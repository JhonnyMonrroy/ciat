<?php if (!defined('BASEPATH')) exit('No se permite el acceso directo al script');

class Menu extends CI_Controller
{
    public $filtro;

    //funciones que queremos implementar en Miclase.
    function const_menu($f)
    {
        // $this->load->model('menu_modelo');
        // $this->filtro = $f;
        if($this->session->userdata('fun_id')!=null){
            $this->load->model('menu_modelo');
            $this->filtro = $f;
        }else{
            redirect('/','refresh');
        }
    }

    public function get_enlaces()
    {
        $enlaces = $this->menu_modelo->get_Modulos($this->filtro);
        return $enlaces;
    }

    public function get_sub_enlaces()
    {
        $enlaces = $this->menu_modelo->get_Modulos($this->filtro);
        for ($i = 0; $i < count($enlaces); $i++) {
            $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        return $subenlaces;
    }
}
?>