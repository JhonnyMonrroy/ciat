<?php

//EJECUCION PRESUPUESTARIA DEL SIGEP
class Cejec_pres_sigep extends CI_Controller
{
    var $gestion;
    public $rol = array('1' => '2','2' => '6');
    function __construct()
    {
        parent:: __construct();
        $this->load->model('registro_ejec/mejec_sigep');
        $this->load->model('users_model','',true);
        //llamar a mi menu
        $this->load->library('menu');
        if($this->rolfun($this->rol)){
            $this->menu->const_menu(3);
            $this->gestion = $this->session->userData('gestion');
        }
        else{
          redirect('admin/dashboard');
        } 
        
    }

    //LISTA DE PROYECTO EJECUCION GASTO
    function lista_proy_ejec()
    {
        $gestion = $this->gestion;
        $mes_id = $this->session->userdata('mes');
        $lista_proy = $this->mejec_sigep->lista_sigep($gestion, $mes_id);
        $tabla = '';
        $cont = 1;
        foreach ($lista_proy AS $row) {
            if ($row['proy_codigo'] == 0 || $row['par_codigo'] == 0 || $row['ff_codigo'] == 0 || $row['of_codigo'] == 0 || $row['pem_devengado'] == 0) {
                $color = "#d55555";
            } else {
                $color = "#3d9850";
            }
            $tabla .= '<tr style="background-color: ' . $color . ';color: #ffffff"">';
            $tabla .= '<td>' . $cont . '</td>';
            $tabla .= '<td>' . $row['proy_codigo'] . '</td>';
            $tabla .= '<td>' . $row['proy_nombre'] . '</td>';
            $tabla .= '<td>' . $row['aper_programa'] . "-" . $row['aper_proyecto'] . "-" . $row['aper_actividad'] . '</td>';
            $tabla .= '<td>' . $row['par_codigo'] . '</td>';
            $tabla .= '<td>' . $row['ff_codigo'] . '</td>';
            $tabla .= '<td>' . $row['of_codigo'] . '</td>';
            $tabla .= '<td>' . $row['pem_ppto_inicial'] . '</td>';
            $tabla .= '<td>' . $row['pem_modif_aprobadas'] . '</td>';
            $tabla .= '<td>' . $row['pem_ppto_vigente'] . '</td>';
            $tabla .= '<td>' . $row['pem_devengado'] . '</td>';
            $tabla .= '<td>' . $row['m_descripcion'] . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        $data['tabla_proy_ejec'] = $tabla;
        $ruta = 'registro_ejec/registro_sigep/vlista_proy_ejec';
        $this->construir_vista($ruta, $data);
    }

    //lista de archivos .csv para la ejecucion presupuestaria
    function lista_ejec_pres_csv()
    {
        $data['gestion'] = $this->gestion;
        $data['reg_ejec_programa_js'] = '<SCRIPT src="' . base_url() . 'mis_js/registro_ejecucion/reg_ejec_sigep.js" type="text/javascript"></SCRIPT>';
        $data['lista_archivos'] = $this->mejec_sigep->lista_arc_segip();
        $ruta = 'registro_ejec/registro_sigep/vejec_csv_sigep';

        //-------------------------
        $gestion = $this->gestion;
        $mes_id = $this->session->userdata('mes');
        $lista_proy = $this->mejec_sigep->lista_proy_ejec($gestion, $mes_id);
        $tabla = '';
        $cont = 1;
        foreach ($lista_proy AS $row) {
            if ($row['proy_codigo'] == 0 || $row['par_codigo'] == 0 || $row['ff_codigo'] == 0 || $row['of_codigo'] == 0 || $row['ega_devengado'] == 0) {
                $color = "#d55555";
            } else {
                $color = "#3d9850";
            }
            $tabla .= '<tr style="background-color: ' . $color . ';color: #ffffff"">';
            $tabla .= '<td>' . $cont . '</td>';
            $tabla .= '<td>' . $row['aper_cod_csv'] . '</td>';
            $tabla .= '<td>' . $row['proy_codigo'] . '</td>';
            $tabla .= '<td>' . $row['proy_nombre'] . '</td>';
            $tabla .= '<td>' . $row['par_cod_csv'] . '</td>';
            $tabla .= '<td>' . $row['par_codigo'] . '</td>';
            $tabla .= '<td>' . $row['ff_cod_csv'] . '</td>';
            $tabla .= '<td>' . $row['ff_codigo'] . '</td>';
            $tabla .= '<td>' . $row['of_cod_csv'] . '</td>';
            $tabla .= '<td>' . $row['of_codigo'] . '</td>';
            $tabla .= '<td>' . $row['ega_ppto_inicial'] . '</td>';
            $tabla .= '<td>' . $row['ega_modif_aprobadas'] . '</td>';
            $tabla .= '<td>' . $row['ega_ppto_vigente'] . '</td>';
            $tabla .= '<td>' . $row['ega_devengado'] . '</td>';
            $tabla .= '<td>' . $row['m_descripcion'] . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        $data['tabla_proy_ejec'] = $tabla;

        $this->construir_vista($ruta, $data);
    }

    //subir archivo sigep
    function subir_sigep()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $nombre_archivo = $post['nombre'];
            $gestion = $this->gestion;
            $mes = $this->session->userdata('mes');
            //--- SUBIR ARCHIVO
            $filename = $_FILES["file"]["name"];
            $file_basename = substr($filename, 0, strripos($filename, '.'));
            $file_ext = substr($filename, strripos($filename, '.'));
            $filesize = $_FILES["file"]["size"];
            $allowed_file_types = array('.csv');
            if (in_array($file_ext, $allowed_file_types) && ($filesize < 90000000)) {
                $newfilename = 'SIGEP-' . $this->gestion . '-' . substr(md5(uniqid(rand())), 0, 5) . $file_ext;
                if (file_exists("archivos/ejec_pres_sigep/" . $newfilename)) {
                    echo "<script>alert('Ya existe este archivo')</script>";
                } else {
                    move_uploaded_file($_FILES["file"]["tmp_name"], "archivos/ejec_pres_sigep/" . $newfilename);
                    $dato['as_nombre'] = $nombre_archivo;
                    $dato['as_ruta'] = $newfilename;
                    $dato['as_gestion'] = $gestion;
                    $dato['as_mes'] = $mes;
                    $dato['fun_id'] = $this->session->userdata('fun_id');
                    $peticiion = $this->db->INSERT('archivos_sigep', $dato);
                    if ($peticiion) {
                        redirect('reg/c_sigep');
                    } else {
                        echo '<script>alert("ERROR AL GUARDAR")</script>';
                    }
                }
            } elseif (empty($file_basename)) {
                echo "Selecciona un archivo para cargarlo.";
            } elseif ($filesize > 100000000) {
                //redirect('');
            } else {
                $mensaje = "Sólo estos tipos de archivo se permiten para la carga: " . implode(', ', $allowed_file_types);
                echo '<script>alert("' . $mensaje . '")</script>';
            }

        } else {
            show_404();
        }
    }

    //sincronizar sigep
    function sincronizar_sigep()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $post = $this->input->post();
            $ruta = $post['ruta'];
            $as_id = $post['as_id'];
            $gestion = $this->gestion;
            $mes = $this->session->userdata('mes');
            $peticion = $this->mejec_sigep->sincronizar($as_id, $ruta, $gestion, $mes);
            if ($peticion) {
                $data['peticion'] = 'verdadero';
            } else {
                $data['peticion'] = 'falso';
            }
            echo json_encode($data);
        } else {

        }
    }

    //eliminar archivo
    function eliminar_archivo()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $post = $this->input->post();
            $ruta = $post['ruta'];
            $as_id = $post['as_id'];
            $peticion = $this->mejec_sigep->eliminar_archivo($as_id, $ruta);
            if ($peticion) {
                $data['peticion'] = 'verdadero';
            } else {
                $data['peticion'] = 'falso';
            }
            echo json_encode($data);
        } else {
            show_404();
        }
    }


    //GUARDAR EJECUCION FINANCIERA DEL MES
    function guardar_ejecucion()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $as_id = $this->input->post('as_id');
            $mes_id = $this->session->userData('mes');
            $gestion = $this->gestion;
            $peticion = $this->mejec_sigep->guardar_ejecucion($as_id, $mes_id, $gestion);
            if ($peticion) {
                $data['peticion'] = 'verdadero';
            } else {
                $data['peticion'] = 'falso';
            }
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    //NUEVA LISTA SIGEP
    function lista_sigep()
    {
        $gestion = $this->gestion;
        $mes_id = $this->session->userdata('mes');
        $lista_proy = $this->mejec_sigep->lista_sigep($gestion, $mes_id);
        $tabla = '';
        $cont = 1;
        foreach ($lista_proy AS $row) {
            if ($row['proy_codigo'] == 0 || $row['par_codigo'] == 0 || $row['ff_codigo'] == 0 || $row['of_codigo'] == 0 || $row['pem_devengado'] == 0) {
                $color = "#d55555";
            } else {
                $color = "#3d9850";
            }
            $tabla .= '<tr style="background-color: ' . $color . ';color: #ffffff"">';
            $tabla .= '<td>' . $cont . '</td>';
            $tabla .= '<td>' . $row['proy_codigo'] . '</td>';
            $tabla .= '<td>' . $row['proy_nombre'] . '</td>';
            $tabla .= '<td>' . $row['aper_programa'] . "-" . $row['aper_proyecto'] . "-" . $row['aper_actividad'] . '</td>';
            $tabla .= '<td>' . $row['par_codigo'] . '</td>';
            $tabla .= '<td>' . $row['ff_codigo'] . '</td>';
            $tabla .= '<td>' . $row['of_codigo'] . '</td>';
            $tabla .= '<td>' . $row['pem_ppto_inicial'] . '</td>';
            $tabla .= '<td>' . $row['pem_modif_aprobadas'] . '</td>';
            $tabla .= '<td>' . $row['pem_ppto_vigente'] . '</td>';
            $tabla .= '<td>' . $row['pem_devengado'] . '</td>';
            $tabla .= '<td>' . $row['m_descripcion'] . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        $data['tabla_ejec'] = $tabla;
        $ruta = 'registro_ejec/registro_sigep/vlista_ejec';
        $this->construir_vista($ruta, $data);
    }


    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'REGISTRO DE EJECUCIÓN';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }
    function rolfun($rol)
    {
        $valor=false;
        for ($i=1; $i <=count($rol) ; $i++) {
            $data = $this->users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
            if(count($data)!=0){
                $valor=true;
                break;
            }
        }
        return $valor;
    }
}