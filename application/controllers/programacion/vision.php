<?php

class vision extends CI_Controller {
    public $rol = array('1' => '2','2' => '6','3' => '6');
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('programacion/model_vision');
         //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(11);
     }
   
    public function vista_vision()
    {   
       if($this->rolfun($this->rol)){ 
            $data['vision']=$this->model_vision->pei_vision_get();
            $ruta = 'programacion/marco_estrategico/vision';
            $this->construir_vista($ruta,$data);
        }
        else{
            redirect('admin/dashboard');
        }
    }   
   function construir_vista($ruta,$data)
   {
       //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PROGRAMACIÒN ';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');
    }
    public function editar_vision()
    {
        if ($this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
          $post = $this->input->post();
          $nueva_vision = $post['vvision'];
          $data_to_store = array( 
                    'vision' => strtoupper($nueva_vision),
                    'ide' => $this->session->userdata('ide'),
                    'fun_id' => $this->session->userdata('fun_id'),
                    'g_id' => $this->session->userdata('gestion'),
                );
            $this->db->insert('historial_vision', $this->security->xss_clean($data_to_store));

        $this->model_vision->edita_vision($nueva_vision);
        $this->vista_vision();
        }
        else{

        }
    }

    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }
}
    


