<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");

class Creporte_proy extends CI_Controller
{
    var $gestion;

    public function __construct()
    {
        parent:: __construct();
        $this->load->library('pdf');
        $this->load->library('pdf2');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/mreporte_proy');
        $this->load->model('reportes/model_objetivo');
        $this->load->model('programacion/insumos/minsumos');
        $this->load->model('programacion/insumos/minsumos_delegado');
        $this->load->model('menu_modelo');
        $this->gestion = $this->session->userData('gestion');
    }

    function reporte_ejec_fin($modulo, $fase_activa, $proy_id)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id)[0];//DATOS DE LA FASEs
        /*---------------- Directa (Actividades)-----------------*/
        if ($fase['pfec_ejecucion'] == 1) {
            $this->rep_ejec_fin_directo($modulo, $proy_id);
        } 
        /*---------------- Delegada (Componentes)-----------------*/
        elseif ($fase['pfec_ejecucion'] == 2) {
            $this->rep_ejec_fin_delegado($modulo, $proy_id);
        } else {
            redirect('admin/dashboard');
        }
    }

    /*========================== REPORTE FINANCIERO ANUAL- PULRIANUAL=============*/
     function programacion_financiera($modulo, $fase_activa, $proy_id)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id)[0];//DATOS DE LA FASE
        if($modulo==1){
                redirect('programacion/creporte_proy/reporte_proy_ejecfin/' . $proy_id); ///// Reporte Anual
            }
        elseif ($modulo==4) {
                redirect('programacion/creporte_proy/reporte_proy_ejecfin_pluri/' . $proy_id); ///// Reporte Anual
            }
        else {
            echo "Error";
        }
    }

    //REPORTE EJECUCION FINANCIERA DIRECTO
    function rep_ejec_fin_directo($modulo, $proy_id)
    {
        $enlaces = $this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id);
        $data['mod'] = $modulo;
        $data['nro_fase'] = $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
        $data['id_f'] = $this->model_faseetapa->get_id_fase($proy_id); //// recupera datos de la tabla fase activa
        
        if($modulo==1){
            $data['ruta_rep_pdf'] = base_url("") . 'index.php/programacion/creporte_proy/reporte_proy_ejecfin/' . $proy_id;
        }
        elseif ($modulo==4) {
            $data['ruta_rep_pdf'] = base_url("") . 'index.php/programacion/creporte_proy/reporte_proy_ejecfin_pluri/' . $proy_id;
        }
        $this->load->view('programacion/reporte_proyecto/vreporte_ejec_fin_proy', $data);
    }

    /// REPORTE EJECUCION FINANCIERA DLEGADO
    function rep_ejec_fin_delegado($modulo, $proy_id)
    {
        $enlaces = $this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id);
        $data['mod'] = $modulo;
        $data['nro_fase'] = $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
        $data['id_f'] = $this->model_faseetapa->get_id_fase($proy_id); //// recupera datos de la tabla fase activa
        if($modulo==1){
            $data['ruta_rep_pdf'] = base_url("") . 'index.php/programacion/creporte_proy/reporte_proy_ejecfin/' . $proy_id;
        }
        elseif ($modulo==4) {
            $data['ruta_rep_pdf'] = base_url("") . 'index.php/programacion/creporte_proy/reporte_proy_ejecfin_pluri/' . $proy_id;
        }
        $this->load->view('programacion/reporte_proyecto/vreporte_ejec_fin_proy', $data);
    }

    //GENERAR REPORTE DE EJECUCION FINANCIERA DEL PROYECTO (Directo/Delegado) ANUAL
    public function reporte_proy_ejecfin($proy_id)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id); //// Fase Activa
        //$num_principal = ($dictamen->tp_id == 1) ? 1 : 0 ;
        $html = $this->get_cabecera();//crear cabecera del pdf
        $html .= $this->dato_proyecto($proy_id);//CREAR DATOS DEL PROYECTO
        if($fase[0]['pfec_ejecucion']==1)
        {
            $html .= $this->insumo_actividades($proy_id,$this->gestion); /// InsumosActividades
            $html .= $this->insumo_total($proy_id,$this->gestion); /// Suma Total
        }
        elseif ($fase[0]['pfec_ejecucion']==2) 
        {
            $html .= $this->insumo_componentes($proy_id,$this->gestion); /// InsumosComponentes
            $html .= $this->insumo_total($proy_id,$this->gestion); /// Suma Total
        }

        $html .= $this->get_pie();//crear pie del pdf
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("ejecucion_financiera_proyecto.pdf", array("Attachment" => false));
    }

    //GENERAR REPORTE DE EJECUCION FINANCIERA DEL PROYECTO (Directo/Delegado) PLURI ANUAL
    public function reporte_proy_ejecfin_pluri($proy_id)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id); //// Fase Activa
        //$num_principal = ($dictamen->tp_id == 1) ? 1 : 0 ;
        $html = $this->get_cabecera();//crear cabecera del pdf
        $html .= $this->dato_proyecto($proy_id);//CREAR DATOS DEL PROYECTO
        if($fase[0]['pfec_ejecucion']==1) //// Directo
        {
            $html .= $this->insumo_actividades_pluri($proy_id); /// InsumosActividades
            $html .= $this->insumo_total_pluri($proy_id); /// Suma Total Pluri Anual
        }
        elseif ($fase[0]['pfec_ejecucion']==2) /// Delegado
        {
            $html .= $this->insumo_componentes_pluri($proy_id); /// InsumosComponentes
            $html .= $this->insumo_total_pluri($proy_id); /// Suma Total Pluri Anual
        }

        $html .= $this->get_pie();//crear pie del pdf
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("reporte_proy_ejecfin_pluri.pdf", array("Attachment" => false));
    }

    /*=================================== INSUMO ACTIVIDADES (DIRECTO) ANUAL ============================*/
    function insumo_actividades($proy_id,$gestion)
    {
        $productos = $this->minsumos->lista_productos($proy_id);
        $tabla = '';
        $cont_p=1;
        foreach ($productos as $rowp) {
            $tabla .='<div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:75%;" class="fila_unitaria titulo_dictamen">
                                    <b> '.$cont_p.'.- PRODUCTO : ' .$rowp['prod_producto']. '</b>
                                </td>
                                <td style="width:25%;">'.$rowp['prod_ponderacion']. ' %</td>
                            </tr>
                        </table>
                      </div>';

            $lista_actividad = $this->minsumos->lista_actividades($rowp['prod_id']);
            $cont = 1;
            foreach ($lista_actividad as $row_a) {
               $tabla .='<div class="contenedor_datos">
                            <table class="table_contenedor">
                                <tr class="collapse_t">
                                    <td style="width:10%;background-color:#ac9fae;font-weight: bold;"  class="fila_unitaria">
                                        <span style="color:white;">'.$cont_p.'.'.$cont.'.- ACTIVIDAD </span>
                                    </td>
                                    <td style="width:90%" class="fila_unitaria">
                                        '.$row_a['act_actividad'].'
                                    </td>
                                </tr>
                            </table>
                        </div>';

                        $tabla .='<div class="contenedor_datos">
                                    <table class="table_contenedor">
                                        <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                            <td style="width:2%;">Nro.</td>
                                            <td style="width:13%;">PARTIDA</td>
                                            <td style="width:5%;">F.F.</td>
                                            <td style="width:5%;">O.F.</td>
                                            <td style="width:5%;">E.T.</td>
                                            <td colspan=13></td>
                                        </tr>';
                                $lista_insumos = $this->minsumos->list_insumo_prog($row_a['act_id'],$gestion);
                                $cont_ins=1;
                                foreach ($lista_insumos as $row) {
                                    $cont++;
                                    $tabla .= '<tr class="collapse_t">';
                                    $tabla .= '<td>'.$cont_ins.'</td>';
                                    $tabla .= '<td>'.$row['par_codigo'].'-'.strtoupper($row['par_nombre']) .'</td>';
                                    $tabla .= '<td>'.$row['ff_codigo'].'</td>';
                                    $tabla .= '<td>'.$row['of_codigo'].'</td>';
                                    $tabla .= '<td>'.$row['et_codigo'].'</td>';
                                    $tabla .= '<td colspan=13>';
                                        $tabla .= '<table class="table_contenedor">';
                                            $tabla .= '<tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">';
                                                $tabla .= '<td>P/E</td>';
                                                $tabla .= '<td>TOTAL</td>';
                                                $tabla .= '<td>ENERO</td>';
                                                $tabla .= '<td>FEBRERO</td>';
                                                $tabla .= '<td>MARZO</td>';
                                                $tabla .= '<td>ABRIL</td>';
                                                $tabla .= '<td>MAYO</td>';
                                                $tabla .= '<td>JUNIO</td>';
                                                $tabla .= '<td>JULIO</td>';
                                                $tabla .= '<td>AGOSTO</td>';
                                                $tabla .= '<td>SEPT.</td>';
                                                $tabla .= '<td>OCTUBRE</td>';
                                                $tabla .= '<td>NOVIEMBRE</td>';
                                                $tabla .= '<td>DICIEMBRE</td>';
                                            $tabla .= '</tr>';
                                            $tabla .= '<tr>';
                                                $tabla .= '<td>PROG.</td>';
                                                $tabla .= '<td>'.number_format($row['programado_total'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($row['mes1'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($row['mes2'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($row['mes3'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($row['mes4'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($row['mes5'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($row['mes6'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($row['mes7'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($row['mes8'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($row['mes9'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($row['mes10'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($row['mes11'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($row['mes12'], 2, ',', '.').'</td>';
                                            $tabla .= '</tr>';
                                            $ejecutado = $this->minsumos->get_insumo_ejec($row['ifin_id']);
                                            if(count($ejecutado)!=0)
                                            {
                                               $tabla .= '<tr>';
                                                $tabla .= '<td>EJEC.</td>';
                                                $tabla .= '<td>'.number_format($ejecutado[0]['ejecutado_total'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($ejecutado[0]['mes1'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($ejecutado[0]['mes2'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($ejecutado[0]['mes3'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($ejecutado[0]['mes4'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($ejecutado[0]['mes5'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($ejecutado[0]['mes6'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($ejecutado[0]['mes7'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($ejecutado[0]['mes8'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($ejecutado[0]['mes9'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($ejecutado[0]['mes10'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($ejecutado[0]['mes11'], 2, ',', '.').'</td>';
                                                $tabla .= '<td>'.number_format($ejecutado[0]['mes12'], 2, ',', '.').'</td>';
                                                $tabla .= '</tr>'; 
                                            }
                                            
                                        $tabla .= '</table>';
                                    $tabla .= '</td>';
                                    $tabla .= '</tr>';
                                    $cont_ins++;
                                }
                            $tabla .='
                                    </table>
                                </div>';        
                $cont++;
            }

        $cont_p++;
        }
        return $tabla;
    }

     /*=================================== INSUMO ACTIVIDADES (DIRECTO) PLURI ANUAL ============================*/
    function insumo_actividades_pluri($proy_id)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id); //// Fase Activa
        $productos = $this->minsumos->lista_productos($proy_id);
        $tabla = '';
        $cont_p=1;
        foreach ($productos as $rowp) {
            $tabla .='<div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:75%;" class="fila_unitaria titulo_dictamen">
                                    <b> '.$cont_p.'.- PRODUCTO : ' .$rowp['prod_producto']. '</b>
                                </td>
                                <td style="width:25%;">'.$rowp['prod_ponderacion']. ' %</td>
                            </tr>
                        </table>
                      </div>';

                        for ($i=$fase[0]['pfec_fecha_inicio']; $i <=$fase[0]['pfec_fecha_fin'] ; $i++) 
                        {
                        $lista_actividad = $this->minsumos->lista_actividades($rowp['prod_id']);
                        $cont = 1;
                        foreach ($lista_actividad as $row_a) {
                        $tabla .='<div class="contenedor_datos">
                                    <table class="table_contenedor">
                                        <tr class="collapse_t">
                                            <td style="width:10%;background-color:#ac9fae;font-weight: bold;"  class="fila_unitaria">
                                                <span style="color:white;">'.$cont_p.'.'.$cont.'.- ACTIVIDAD </span>
                                            </td>
                                            <td style="width:90%" class="fila_unitaria">
                                                '.$row_a['act_actividad'].'
                                            </td>
                                        </tr>
                                    </table>
                                </div>';
                                    $tabla .='
                                    <div class="contenedor_datos">
                                    <table class="table_contenedor">
                                        <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                            <td style="width:2%;">Nro.</td>
                                            <td style="width:13%;">PARTIDA</td>
                                            <td style="width:5%;">F.F.</td>
                                            <td style="width:5%;">O.F.</td>
                                            <td style="width:5%;">E.T.</td>
                                            <td colspan=13> GESTI&Oacute;N : '.$i.'</td>
                                        </tr>';
                                    $lista_insumos = $this->minsumos->list_insumo_prog($row_a['act_id'],$i);
                                    if(count($lista_insumos)!=0)
                                    {
                                        $cont_ins=1;
                                            foreach ($lista_insumos as $row) {
                                                $cont++;
                                                $tabla .= '<tr class="collapse_t">';
                                                $tabla .= '<td>'.$cont_ins.'</td>';
                                                $tabla .= '<td>'.$row['par_codigo'].'-'.strtoupper($row['par_nombre']) .'</td>';
                                                $tabla .= '<td>'.$row['ff_codigo'].'</td>';
                                                $tabla .= '<td>'.$row['of_codigo'].'</td>';
                                                $tabla .= '<td>'.$row['et_codigo'].'</td>';
                                                $tabla .= '<td colspan=13>';
                                                    $tabla .= '<table class="table_contenedor">';
                                                        $tabla .= '<tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">';
                                                            $tabla .= '<td>P/E</td>';
                                                            $tabla .= '<td>TOTAL</td>';
                                                            $tabla .= '<td>ENERO</td>';
                                                            $tabla .= '<td>FEBRERO</td>';
                                                            $tabla .= '<td>MARZO</td>';
                                                            $tabla .= '<td>ABRIL</td>';
                                                            $tabla .= '<td>MAYO</td>';
                                                            $tabla .= '<td>JUNIO</td>';
                                                            $tabla .= '<td>JULIO</td>';
                                                            $tabla .= '<td>AGOSTO</td>';
                                                            $tabla .= '<td>SEPT.</td>';
                                                            $tabla .= '<td>OCTUBRE</td>';
                                                            $tabla .= '<td>NOVIEMBRE</td>';
                                                            $tabla .= '<td>DICIEMBRE</td>';
                                                        $tabla .= '</tr>';
                                                        $tabla .= '<tr>';
                                                            $tabla .= '<td>PROG.</td>';
                                                            $tabla .= '<td>'.number_format($row['programado_total'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($row['mes1'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($row['mes2'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($row['mes3'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($row['mes4'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($row['mes5'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($row['mes6'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($row['mes7'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($row['mes8'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($row['mes9'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($row['mes10'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($row['mes11'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($row['mes12'], 2, ',', '.').'</td>';
                                                        $tabla .= '</tr>';
                                                        $ejecutado = $this->minsumos->get_insumo_ejec($row['ifin_id']);
                                                        if(count($ejecutado)!=0)
                                                        {
                                                           $tabla .= '<tr>';
                                                            $tabla .= '<td>EJEC.</td>';
                                                            $tabla .= '<td>'.number_format($ejecutado[0]['ejecutado_total'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes1'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes2'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes3'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes4'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes5'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes6'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes7'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes8'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes9'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes10'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes11'], 2, ',', '.').'</td>';
                                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes12'], 2, ',', '.').'</td>';
                                                            $tabla .= '</tr>'; 
                                                        }
                                                        
                                                    $tabla .= '</table>';
                                                $tabla .= '</td>';
                                                $tabla .= '</tr>';
                                                $cont_ins++;
                                            }
                                    }
                                    else
                                    {
                                        $tabla .= '<tr class="collapse_t">';
                                        $tabla .= '<td colspan=18>No tiene Requerimientos Registrados</td>';
                                        $tabla .= '</tr>';
                                    }
                                }
                          
                            $tabla .='
                            </table>
                        </div>';        
                $cont++;
            }

        $cont_p++;
        }
        return $tabla;
    }

    /*=================================== INSUMO COMPONENTES (DELEGADO) ANUAL ============================*/
    function insumo_componentes($proy_id,$gestion)
    {
        $componentes = $this->mreporte_proy->lista_componete($proy_id);
        
        $tabla = '';
        $cont_c=1;
        foreach ($componentes as $rowc) {
            $tabla .='<div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:75%;" class="fila_unitaria titulo_dictamen">
                                    <b> '.$cont_c.'.- COMPONENTE : ' .$rowc['com_componente']. '</b>
                                </td>
                                <td style="width:25%;">'.$rowc['com_ponderacion']. ' %</td>
                            </tr>
                        </table>
                      </div>';

                $tabla .='<div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                <td style="width:2%;">Nro.</td>
                                <td style="width:13%;">PARTIDA</td>
                                <td style="width:5%;">F.F.</td>
                                <td style="width:5%;">O.F.</td>
                                <td style="width:5%;">E.T.</td>
                                <td colspan=13></td>
                            </tr>';
                    $lista_insumos = $this->minsumos_delegado->list_insumo_prog($rowc['com_id'],$gestion);
                    $cont_ins=1;
                    foreach ($lista_insumos as $row) {
                        $tabla .= '<tr class="collapse_t">';
                        $tabla .= '<td>'.$cont_ins.'</td>';
                        $tabla .= '<td>'.$row['par_codigo'].'-'.strtoupper($row['par_nombre']) .'</td>';
                        $tabla .= '<td>'.$row['ff_codigo'].'</td>';
                        $tabla .= '<td>'.$row['of_codigo'].'</td>';
                        $tabla .= '<td>'.$row['et_codigo'].'</td>';
                        $tabla .= '<td colspan=13>';
                            $tabla .= '<table class="table_contenedor">';
                                $tabla .= '<tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">';
                                    $tabla .= '<td>P/E</td>';
                                    $tabla .= '<td>TOTAL</td>';
                                    $tabla .= '<td>ENERO</td>';
                                    $tabla .= '<td>FEBRERO</td>';
                                    $tabla .= '<td>MARZO</td>';
                                    $tabla .= '<td>ABRIL</td>';
                                    $tabla .= '<td>MAYO</td>';
                                    $tabla .= '<td>JUNIO</td>';
                                    $tabla .= '<td>JULIO</td>';
                                    $tabla .= '<td>AGOSTO</td>';
                                    $tabla .= '<td>SEPT.</td>';
                                    $tabla .= '<td>OCTUBRE</td>';
                                    $tabla .= '<td>NOVIEMBRE</td>';
                                    $tabla .= '<td>DICIEMBRE</td>';
                                $tabla .= '</tr>';
                                $tabla .= '<tr>';
                                    $tabla .= '<td>PROG.</td>';
                                    $tabla .= '<td>'.number_format($row['programado_total'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($row['mes1'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($row['mes2'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($row['mes3'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($row['mes4'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($row['mes5'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($row['mes6'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($row['mes7'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($row['mes8'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($row['mes9'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($row['mes10'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($row['mes11'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($row['mes12'], 2, ',', '.').'</td>';
                                $tabla .= '</tr>';
                                $ejecutado = $this->minsumos->get_insumo_ejec($row['ifin_id']);
                                if(count($ejecutado)!=0)
                                {
                                   $tabla .= '<tr>';
                                    $tabla .= '<td>EJEC.</td>';
                                    $tabla .= '<td>'.number_format($ejecutado[0]['ejecutado_total'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($ejecutado[0]['mes1'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($ejecutado[0]['mes2'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($ejecutado[0]['mes3'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($ejecutado[0]['mes4'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($ejecutado[0]['mes5'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($ejecutado[0]['mes6'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($ejecutado[0]['mes7'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($ejecutado[0]['mes8'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($ejecutado[0]['mes9'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($ejecutado[0]['mes10'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($ejecutado[0]['mes11'], 2, ',', '.').'</td>';
                                    $tabla .= '<td>'.number_format($ejecutado[0]['mes12'], 2, ',', '.').'</td>';
                                    $tabla .= '</tr>'; 
                                }
                                
                            $tabla .= '</table>';
                        $tabla .= '</td>';
                        $tabla .= '</tr>';
                        $cont_ins++;
                    }
                $tabla .='
                </table>
            </div>'; 

        $cont_c++;
        }
        return $tabla;
    }
    /*=================================== INSUMO COMPONENTES (DELEGADO) PLURI ANUAL ============================*/
    function insumo_componentes_pluri($proy_id)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id); //// Fase Activa
        $componentes = $this->mreporte_proy->lista_componete($proy_id);
        
        $tabla = '';
        $cont_c=1;
        foreach ($componentes as $rowc) {
            $tabla .='<div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:75%;" class="fila_unitaria titulo_dictamen">
                                    <b> '.$cont_c.'.- COMPONENTE : ' .$rowc['com_componente']. '</b>
                                </td>
                                <td style="width:25%;">'.$rowc['com_ponderacion']. ' % </td>
                            </tr>
                        </table>
                      </div>';

            for ($i=$fase[0]['pfec_fecha_inicio']; $i <=$fase[0]['pfec_fecha_fin'] ; $i++) 
            { 
             $tabla .='<div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                <td style="width:2%;">Nro.</td>
                                <td style="width:13%;">PARTIDA</td>
                                <td style="width:5%;">F.F.</td>
                                <td style="width:5%;">O.F.</td>
                                <td style="width:5%;">E.T.</td>
                                <td colspan=13> GESTI&Oacute;N : '.$i.'</td>
                            </tr>';
                    $lista_insumos = $this->minsumos_delegado->list_insumo_prog($rowc['com_id'],$i);
                    if(count($lista_insumos)!=0)
                    {
                         $cont_ins=1;
                            foreach ($lista_insumos as $row) {
                                $tabla .= '<tr class="collapse_t">';
                                $tabla .= '<td>'.$cont_ins.'</td>';
                                $tabla .= '<td>'.$row['par_codigo'].'-'.strtoupper($row['par_nombre']) .'</td>';
                                $tabla .= '<td>'.$row['ff_codigo'].'</td>';
                                $tabla .= '<td>'.$row['of_codigo'].'</td>';
                                $tabla .= '<td>'.$row['et_codigo'].'</td>';
                                $tabla .= '<td colspan=13>';
                                    $tabla .= '<table class="table_contenedor">';
                                        $tabla .= '<tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">';
                                            $tabla .= '<td>P/E</td>';
                                            $tabla .= '<td>TOTAL</td>';
                                            $tabla .= '<td>ENERO</td>';
                                            $tabla .= '<td>FEBRERO</td>';
                                            $tabla .= '<td>MARZO</td>';
                                            $tabla .= '<td>ABRIL</td>';
                                            $tabla .= '<td>MAYO</td>';
                                            $tabla .= '<td>JUNIO</td>';
                                            $tabla .= '<td>JULIO</td>';
                                            $tabla .= '<td>AGOSTO</td>';
                                            $tabla .= '<td>SEPT.</td>';
                                            $tabla .= '<td>OCTUBRE</td>';
                                            $tabla .= '<td>NOVIEMBRE</td>';
                                            $tabla .= '<td>DICIEMBRE</td>';
                                        $tabla .= '</tr>';
                                        $tabla .= '<tr>';
                                            $tabla .= '<td>PROG.</td>';
                                            $tabla .= '<td>'.number_format($row['programado_total'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($row['mes1'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($row['mes2'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($row['mes3'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($row['mes4'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($row['mes5'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($row['mes6'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($row['mes7'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($row['mes8'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($row['mes9'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($row['mes10'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($row['mes11'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($row['mes12'], 2, ',', '.').'</td>';
                                        $tabla .= '</tr>';
                                        $ejecutado = $this->minsumos->get_insumo_ejec($row['ifin_id']);
                                        if(count($ejecutado)!=0)
                                        {
                                           $tabla .= '<tr>';
                                            $tabla .= '<td>EJEC.</td>';
                                            $tabla .= '<td>'.number_format($ejecutado[0]['ejecutado_total'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes1'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes2'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes3'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes4'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes5'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes6'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes7'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes8'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes9'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes10'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes11'], 2, ',', '.').'</td>';
                                            $tabla .= '<td>'.number_format($ejecutado[0]['mes12'], 2, ',', '.').'</td>';
                                            $tabla .= '</tr>'; 
                                        }
                                        
                                    $tabla .= '</table>';
                                $tabla .= '</td>';
                                $tabla .= '</tr>';
                                $cont_ins++;
                            }
                          
                    }
                    else
                    {
                        $tabla .= '<tr class="collapse_t">';
                        $tabla .= '<td colspan=18>No tiene Requerimientos Registrados</td>';
                        $tabla .= '</tr>';
                    }
                  $tabla .='
                        </table>
                    </div>';  
            }
                

        $cont_c++;
        }
        return $tabla;
    }

    /*-------------------- TABLA TOTAL ANUAL ------------------------*/
    function tabla_total($proy_id,$gestion)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $ms[0]='total';$ms[1]='mes1';$ms[2]='mes2';$ms[3]='mes3';$ms[4]='mes4';$ms[5]='mes5';$ms[6]='mes6';
        $ms[7]='mes7';$ms[8]='mes8';$ms[9]='mes9';$ms[10]='mes10';$ms[11]='mes11';$ms[12]='mes12';
        
        for($i=1;$i<=12;$i++)
        {
          $totalp[1][$i]=0; //// i
          $totalp[2][$i]=0; //// p
          $totalp[3][$i]=0; //// pa
          $totalp[4][$i]=0; //// %pa
          $totalp[5][$i]=0; //// e
          $totalp[6][$i]=0; //// ea
          $totalp[7][$i]=0; //// %ea
          $totalp[8][$i]=0; //// eficacia
          $totalp[9][$i]=0; //// Menor
          $totalp[10][$i]=0; //// Entre
          $totalp[11][$i]=0; //// Mayor
        }

        if($fase[0]['pfec_ejecucion']==1){
            $programado = $this->minsumos->sum_total_insumo_prog($proy_id,$gestion);
            $ejecutado = $this->minsumos->sum_total_insumo_ejec($proy_id,$gestion);
        }
        elseif ($fase[0]['pfec_ejecucion']==2) {
           $programado = $this->minsumos_delegado->sum_total_insumo_prog($proy_id,$gestion);
           $ejecutado = $this->minsumos_delegado->sum_total_insumo_ejec($proy_id,$gestion);
        }
        
        if(count($programado)!=0)
        {
            $suma_pfin=0; $suma_efin=0;
            for($i=1;$i<=12;$i++)
            {
                $totalp[1][$i]=$i; //// i
                $totalp[2][$i]=$programado[0][$ms[$i]]; /// programado
                $suma_pfin=$suma_pfin+$programado[0][$ms[$i]];
                $totalp[3][$i]=$suma_pfin; ///// Programado acumulado

              if($programado[0][$ms[0]]!=0)
              {
                $totalp[4][$i]=round((($totalp[3][$i]/$programado[0][$ms[0]])*100),2); ////// Programado Acumulado %
              }
              
              if(count($ejecutado)!=0){
                  $totalp[5][$i]=$ejecutado[0][$ms[$i]]; /// Ejecutado
                  $suma_efin=$suma_efin+$ejecutado[0][$ms[$i]];
                  $totalp[6][$i]=$suma_efin; ///// Ejecutado acumulado
                  
                  if($programado[0][$ms[0]]!=0)
                  {
                    $totalp[7][$i]=round((($totalp[6][$i]/$programado[0][$ms[0]])*100),2); ////// Ejecutado Acumulado %
                  }

                  if($totalp[4][$i]!=0)
                  {
                    $totalp[8][$i]=round((($totalp[7][$i]/$totalp[4][$i])*100),2); ////// Eficacia
                  }

                  /*------------------------- eficacia -------------------------*/
                  if($totalp[8][$i]<=75){$totalp[9][$i] = $totalp[8][$i];}
                  if ($totalp[8][$i] >= 76 && $totalp[8][$i] <= 90.9) {$totalp[10][$i] = $totalp[8][$i];}
                  if($totalp[8][$i] >= 91){$totalp[11][$i] = $totalp[8][$i];}
              }
            }  
        }
        return $totalp;
    }

    /*-------------------- TABLA TOTAL PLURI ANUAL ------------------------*/
    function tabla_total_pluri($proy_id)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $ms[0]='total';$ms[1]='mes1';$ms[2]='mes2';$ms[3]='mes3';$ms[4]='mes4';$ms[5]='mes5';$ms[6]='mes6';
        $ms[7]='mes7';$ms[8]='mes8';$ms[9]='mes9';$ms[10]='mes10';$ms[11]='mes11';$ms[12]='mes12';
        
        for($i=1;$i<=12;$i++)
        {
          $totalp[1][$i]=0; //// i
          $totalp[2][$i]=0; //// p
          $totalp[3][$i]=0; //// pa
          $totalp[4][$i]=0; //// %pa
          $totalp[5][$i]=0; //// e
          $totalp[6][$i]=0; //// ea
          $totalp[7][$i]=0; //// %ea
          $totalp[8][$i]=0; //// eficacia
          $totalp[9][$i]=0; //// Menor
          $totalp[10][$i]=0; //// Entre
          $totalp[11][$i]=0; //// Mayor
        }

        if($fase[0]['pfec_ejecucion']==1){
            $programado = $this->minsumos->sum_total_insumo_prog_pluri($proy_id);
            $ejecutado = $this->minsumos->sum_total_insumo_ejec_pluri($proy_id);
        }
        elseif ($fase[0]['pfec_ejecucion']==2) {
           $programado = $this->minsumos_delegado->sum_total_insumo_prog_pluri($proy_id);
           $ejecutado = $this->minsumos_delegado->sum_total_insumo_ejec_pluri($proy_id);
        }
        
        if(count($programado)!=0)
        {
            $suma_pfin=0; $suma_efin=0;
            for($i=1;$i<=12;$i++)
            {
                $totalp[1][$i]=$i; //// i
                $totalp[2][$i]=$programado[0][$ms[$i]]; /// programado
                $suma_pfin=$suma_pfin+$programado[0][$ms[$i]];
                $totalp[3][$i]=$suma_pfin; ///// Programado acumulado

              if($programado[0][$ms[0]]!=0)
              {
                $totalp[4][$i]=round((($totalp[3][$i]/$programado[0][$ms[0]])*100),2); ////// Programado Acumulado %
              }
              
              if(count($ejecutado)!=0){
                  $totalp[5][$i]=$ejecutado[0][$ms[$i]]; /// Ejecutado
                  $suma_efin=$suma_efin+$ejecutado[0][$ms[$i]];
                  $totalp[6][$i]=$suma_efin; ///// Ejecutado acumulado
                  
                  if($programado[0][$ms[0]]!=0)
                  {
                    $totalp[7][$i]=round((($totalp[6][$i]/$programado[0][$ms[0]])*100),2); ////// Ejecutado Acumulado %
                  }

                  if($totalp[4][$i]!=0)
                  {
                    $totalp[8][$i]=round((($totalp[7][$i]/$totalp[4][$i])*100),2); ////// Eficacia
                  }

                  /*------------------------- eficacia -------------------------*/
                  if($totalp[8][$i]<=75){$totalp[9][$i] = $totalp[8][$i];}
                  if ($totalp[8][$i] >= 76 && $totalp[8][$i] <= 90.9) {$totalp[10][$i] = $totalp[8][$i];}
                  if($totalp[8][$i] >= 91){$totalp[11][$i] = $totalp[8][$i];}
              }
            }
        }
        return $totalp;
    }

    /*----------------------------- INSUMO TOTAL ANUAL ------------------------------*/
    function insumo_total($proy_id,$gestion)
    {
        $total= $this->tabla_total($proy_id,$gestion);
        $tabla = '';   
        $tabla .='<div class="contenedor_datos">
                    <table class="table_contenedor">
                        <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                            <td colspan=13>GESTI&Oacute;N '.$this->gestion.'</td>
                        </tr>
                        <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                            <td style="width:2%;">P/E</td>
                            <td style="width:5%;">ENERO</td>
                            <td style="width:5%;">FEBRERO</td>
                            <td style="width:5%;">MARZO</td>
                            <td style="width:5%;">ABRIL</td>
                            <td style="width:5%;">MAYO</td>
                            <td style="width:5%;">JUNIO</td>
                            <td style="width:5%;">JULIO</td>
                            <td style="width:5%;">AGOSTO</td>
                            <td style="width:5%;">SEPTIEMBRE</td>
                            <td style="width:5%;">OCTUBRE</td>
                            <td style="width:5%;">NOVIEMBRE</td>
                            <td style="width:5%;">DICIEMBRE</td>
                        </tr>
                        <tr class="collapse_t">
                            <td>P.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[2][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="collapse_t">
                            <td>P.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[3][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="collapse_t">
                            <td>%P.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[4][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="collapse_t">
                            <td>E</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[5][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="collapse_t">
                            <td>E.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[6][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="collapse_t">
                            <td>%E.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[7][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="collapse_t">
                            <td>EFICACIA</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[8][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>';
                    $tabla .='
                    </table>
                </div>';
     
        return $tabla;
    }

     /*----------------------------- INSUMO TOTAL PLURI ANUAL ------------------------------*/
    function insumo_total_pluri($proy_id)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id); //// Fase Activa
        $total= $this->tabla_total_pluri($proy_id);
        $tabla = '';   
        $tabla .='<div class="contenedor_datos">
                    <table class="table_contenedor">
                        <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                            <td colspan=13>GESTIONES : '.$fase[0]['pfec_fecha_inicio'].' - '.$fase[0]['pfec_fecha_fin'].'</td>
                        </tr>
                        <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                            <td style="width:2%;">P/E</td>
                            <td style="width:5%;">ENERO</td>
                            <td style="width:5%;">FEBRERO</td>
                            <td style="width:5%;">MARZO</td>
                            <td style="width:5%;">ABRIL</td>
                            <td style="width:5%;">MAYO</td>
                            <td style="width:5%;">JUNIO</td>
                            <td style="width:5%;">JULIO</td>
                            <td style="width:5%;">AGOSTO</td>
                            <td style="width:5%;">SEPTIEMBRE</td>
                            <td style="width:5%;">OCTUBRE</td>
                            <td style="width:5%;">NOVIEMBRE</td>
                            <td style="width:5%;">DICIEMBRE</td>
                        </tr>
                        <tr class="collapse_t">
                            <td>P.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[2][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="collapse_t">
                            <td>P.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[3][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="collapse_t">
                            <td>%P.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[4][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="collapse_t">
                            <td>E</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[5][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="collapse_t">
                            <td>E.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[6][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="collapse_t">
                            <td>%E.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[7][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="collapse_t">
                            <td>EFICACIA</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[8][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>';
                    $tabla .='
                    </table>
                </div>';
     
        return $tabla;
    }
    /*=======================================================================================================================*/
    //DATOS DEL PROYECTO
    public function dato_proyecto($proy_id)
    {
        $gestion = $this->session->userData('gestion');
        $dictamen = $this->model_objetivo->get_dictamen($proy_id);
        $dictamen = $dictamen->row();
        $aper_programatica = $this->model_objetivo->get_aper_programatica_dictamen($proy_id, $gestion);
        $aper_programatica = $aper_programatica->row();
        $proyecto = $this->model_proyecto->get_id_proyecto($proy_id); //// Datos del Proyecto
        $dato_proy = '
             <table class="datos_principales" style="table-layout:fixed;" border="1">
                <tr>
                    <td colspan="2">
                        <b>'.$proyecto[0]['proy_nombre'].'</b><br>
                        '.$proyecto[0]['tipo'].'
                    </td>
                </tr>
                <tr>
                    <td style="width:50%; text-align:center;"">
                    '.$proyecto[0]['proy_sisin'].'<br>
                        C&oacute;digo SISIN
                    </td>
                    <td style="width:50%; text-align:center;">
                        <b>'.$proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'].'</b><br>
                        Apertura Programatica
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;text-align:center;">
                        <b>'.$proyecto[0]['ue'].'</b><br>
                        Unidad Ejecutora
                    </td>
                    <td style="width:50%; text-align:center;">
                        <b>'.$proyecto[0]['ur'].'</b><br>
                        Unidad Responable
                    </td>
                </tr>
            </table><br>';
        return $dato_proy;
    }


    //CABECERA DEL PDF
    function get_cabecera()
    {
        $gestion = $this->session->userdata('gestion');
        $html = '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>' . $this->estilo_vertical().$this->session->userdata('franja_decorativa'). '</head>
            <body>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                           <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=50%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> ' . $gestion . '<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N PRESUPUESTARIA DE LA OPERACI&Oacute;N<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="80px">
                        </td>
                    </tr>
                </table>
                <div class="contenedor_principal">';
        return $html;
    }

    //PIE DEL PDF
    function get_pie()
    {
        $html = '
                    <table >
                        <tr>
                            <td class="fila_unitaria">FIRMAS:</td>
                        </tr>
                    </table>
                    <table style="width: 80%;margin: 0 auto;margin-top: 100px;margin-bottom: 30px;">
                        <tr>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                        </tr>
                    </table>
                </div>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
            </body>
        </html>';
        return $html;
    }

    function estilo_vertical()
    {
        $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1.5px;
                text-align: center;
            }
        .mv{font-size:10px;}
        .siipp{width:150px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 8px;
        }
        .datos_principales {
            text-align: center;
            font-size: 8px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;
        }

        .indi_desemp{
            font-size: 10px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:5px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 10px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .lista{
            text-align:left;
            padding-left: 7px;
            list-style-type:square;
            margin:2px;
        }
    </style>';
        return $estilo_vertical;
    }


}