<?php

class Cponderacion_proyecto extends CI_Controller
{
    var $gestion;
    var $rol;
    var $fun_id;

    function __construct()
    {
        parent::__construct();
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('mantenimiento/mapertura_programatica');
        $this->load->model('programacion/ponderacion/mponderacion');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(1);
        $this->gestion = $this->session->userData('gestion');
        $this->rol = $this->session->userData('rol');
        $this->fun_id = $this->session->userData('fun_id');
    }

    //LISTADE PROGRAMAS PARA PROYECTOS
    function lista_red_objetivos_p()
    {
        $ruta = '/prog/pond_proy/';
        $data['lista_poa'] = $this->tabla_red_objetivos($ruta);
        $ruta = 'programacion/ponderacion/proyectos/vred_programa';
        $this->construir_vista($ruta, $data);
    }

    //GENERAR TABLA DE RED DE OBJETIVOS
    function tabla_red_objetivos($ruta)
    {
        $lista_poa = $this->mpoa->lista_poa();
        $cont = 1;
        $tabla = '';
        foreach ($lista_poa as $row) {
            $tabla .= '<tr>';
            $tabla .= '<td >
                            <a href="' . site_url("") . $ruta . $row['aper_id'] . '" class="enviar_poa" name="enviar_poa" id="enviar_poa">
                                <img src="' . base_url() . 'assets/ifinal/2.png" width="40" height="40" class="img-responsive "title="ASIGNAR PONDERACION">
							</a>
					   </td>';
            $tabla .= '<td>' . $row['poa_codigo'] . '</td>';
            $tabla .= '<td>' . $row['aper_programa'] . $row['aper_proyecto'] . $row['aper_actividad'] . " - " . $row['aper_descripcion'] . '</font></td>';
            $tabla .= '<td>' . $row['uni_unidad'] . '</td>';
            $tabla .= '<td>' . $row['poa_fecha_creacion'] . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        return $tabla;
    }

    //LISTA DE PROYECTOS
    function ponderacion_proyectos($aper_id)
    {
        $data['dato_aper'] = $this->mapertura_programatica->dato_apertura($aper_id)[0];
        $dato = $this->tabla_proyetos($aper_id);
        $data['lista_proyecto'] = $dato['tabla'];
        $data['ponderacion'] = $dato['ponderacion'];
        $data['ponderacion_js'] = '<SCRIPT src="' . base_url() . 'mis_js/programacion/ponderacion/ponderacion_proyecto.js" type="text/javascript"></SCRIPT>';
        $ruta = 'programacion/ponderacion/proyectos/vpond_proyecto';
        $this->construir_vista($ruta, $data);
    }

    //GENERAR TABLA DE RED DE OBJETIVOS
    function tabla_proyetos($aper_id)
    {
        //OBTENER MI CODIGO DE PROGRAMA DE LA APERTURA
        $aper_programa = $this->mapertura_programatica->dato_apertura($aper_id);
        $aper_programa = $aper_programa[0]['aper_programa'].'';
        $lista_proyecto = $this->mponderacion->lista_proyectos($aper_programa);
        $tabla = '';
        $cont = 1;
        $ponderacion = 0;
        foreach ($lista_proyecto as $row) {
            $ponderacion += $row['proy_ponderacion'];
            $tabla .= '<tr>';
            $tabla .= '<td>' . $cont . '</td>';
            $tabla .= '<td >
                            <a href="" class="add_pond" data-toggle="modal" data-target="#modal_ponderacion" name="' . $row['proy_id'] . '">
                                <img src="' . base_url() . 'assets/ifinal/form2.png" width="30" height="30" class="img-responsive"
                                title="ASIGNAR PONDERACION">
							</a>
					   </td>';
            $tabla .= '<td>' . $row['proy_codigo'] . '</td>';
            $tabla .= '<td>' . $row['aper_programa'] . $row['aper_proyecto'] . $row['aper_actividad'] . '</font></td>';
            $tabla .= '<td>' . $row['proy_nombre'] . '</td>';
            $tabla .= '<td>' . $row['proy_sisin'] . '</td>';
            $tabla .= '<td>' . $row['pfecg_ppto_total'] . '</td>';
            $tabla .= '<td>' . $row['proy_ponderacion'] . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        $data['tabla'] = $tabla;
        $data['ponderacion'] = $ponderacion;
        return $data;
    }

    //OBTENER DATOS DEL PROYECTO
    function get_proy(){
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $proy_id = $post['proy_id'];
            $get_proy= $this->mponderacion->dato_proyecto($proy_id);
            $result = array(
                'id' => $get_proy -> proy_id,
                'codigo' => $get_proy -> proy_codigo,
                'objetivo' => $get_proy -> proy_nombre,
                'ponderacion' => $get_proy -> proy_ponderacion
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //GUARDAR PONDERACIONÇ
    function guardar_pond(){
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $proy_id = $post['proy_id'];
            $ponderacion = $post['pond'];
            $this->mponderacion->add_pond_proy($proy_id,$ponderacion);
            $data['respuesta'] = 'true';
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    //PONDERACION DE PROYECTOS
    function ponderar_proyectos(){
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $aper_id = $post['aper_id'];
            //OBTENER MI CODIGO DE PROGRAMA DE LA APERTURA
            $aper_programa = $this->mapertura_programatica->dato_apertura($aper_id);
            $aper_programa = $aper_programa[0]['aper_programa'].'';
            $suma_proy =  $this->mponderacion->suma_aper_proy($aper_programa)->suma;//SUMA TOTAL DE LOS PRESUPUESTO ASIGNADOS
            $lista_proyecto = $this->mponderacion->lista_proyectos($aper_programa);
            foreach ($lista_proyecto as $row) {
                    $pond_porcentual = ($row['pfecg_ppto_total']*100)/$suma_proy;
                $this->mponderacion->add_pond_proy($row['proy_id'],$pond_porcentual);
            }
            $data['respuesta'] = 'true';
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PROGRAMACIÓN';
        //-----------------------------------------------------------------------
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }


}