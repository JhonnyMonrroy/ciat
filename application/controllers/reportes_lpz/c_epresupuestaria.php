<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class c_epresupuestaria extends CI_Controller {
    var $gestion;
    var $mes;
    var $rol;
    var $fun_id;

    public function __construct ()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->library('pdf');
            $this->load->library('pdf2');

            $this->load->model('reportes_tarija_das/mdas_complementario');

            $this->load->model('programacion/model_proyecto');
            $this->load->model('programacion/model_producto');
            $this->load->model('programacion/model_faseetapa');
            $this->load->model('programacion/model_componente');
            $this->load->model('programacion/model_actividad');

            $this->load->model('mantenimiento/mapertura_programatica');
            $this->load->model('mantenimiento/munidad_organizacional');
            $this->load->model('reportes_tarija/model_epresupuestaria');
            $this->load->model('reportes_tarija/model_uejec');
            $this->load->model('menu_modelo');
            $this->load->model('Users_model','',true);

            $this->gestion = $this->session->userData('gestion');
            $this->mes = $this->session->userData('mes');
            $this->rol = $this->session->userData('rol');
            $this->fun_id = $this->session->userData('fun_id');

        }else{
            redirect('/','refresh');
        }
    }

    /*======================== EJECUCION DEL PRESUPUESTO GENERAL ==========================*/
    public function presupuestaria_general()
    {
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;

        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['tabla']=$this->genera_tabla_general(1);
        $this->load->view('admin/reportes/ejecucion_presupuestaria/ejecucion_unidad_ejecutora/ejec_ejecutora_gral', $data);
    }

    /*======================== DETALLE EJECUCION DEL PRESUPUESTO GENERAL ==========================*/
    public function detalle_pres_general($tp,$uni_id)
    {
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;

        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        if($tp==1){
            $titulo='ORGANO LEGISLATIVO';
        }
        else{
            $titulo='ORGANO EJECUTIVO';
        }
        $data['organo'] = $titulo;
        $data['tp'] = $tp;
        $data['uni_id'] = $uni_id;
        $data['unidad'] = $this->model_epresupuestaria->get_unidad_ejecutora($uni_id);

        $data['atras']= '' . base_url() . 'index.php/rep/ejec/presupuestaria';

        $data['detalles']=$this->tabla_detalle_pres_general($uni_id,1);
        $this->load->view('admin/reportes/ejecucion_presupuestaria/ejecucion_unidad_ejecutora/ejec_ejecutora_gral_detalle', $data);

    }

    public function tabla_detalle_pres_general($uni_id,$tp)
    {
        if($tp==1){
            $tb='id="dt_basic" class="table table table-bordered" width="100%"';
        }
        elseif ($tp==2) {
            $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        }

        $unidades = $this->mdas_complementario->unidades_ejecutoras();
        $sum=0;
        $tabla ='';
        $tabla.='<table '.$tb.'>';
        $tabla.='<thead>';
        $tabla.='<tr class="modo1">';
        $tabla.='<th style="width:15%;">UNIDAD EJECUTORA</td>';
        $tabla.='<th style="width:15%;">PROYECTO-PROGRAMA.OPERACI&Oacute;N</th>';
        $tabla.='<th style="width:10%;">COSTO TOTAL DE PROYECTO</th>';
        $tabla.='<th style="width:10%;">COSTO TOTAL GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">PRESUPUESTO INICIAL '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">PRESUPUESTO MODIFICADO '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">PRESUPUESTO VIGENTE '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">EJEC. FINANCIERA DE GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">% EJEC. FINANCIERA '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">% EJEC. FINANCIERA DE PROYECTO </th>';
        $tabla.='<th style="width:10%;">% EJEC. F&Iacute;SICA ACUMULADA </th>';
        $tabla.='<th style="width:10%;">ESTADO</th>';
        $tabla.='</tr>';
        $tabla.='</thead>';
        $tabla.='<tbody>';

        foreach($unidades as $rowue)
        {
            $programas = $this->model_epresupuestaria->programas_hijos_general_detalles($this->gestion,$this->mes,$uni_id,$rowue['uni_id']);
            if(count($programas)!=0){
                //  echo "UNI ID ".$rowue['uni_id']." UNIDAD EJECUTORA : ".$rowue['uni_unidad']."<br>";
                //  echo "NRO : ".count($programas)."<br>";
                foreach($programas as $rowp)
                {
                    $ejec=0;
                    if($rowp['pe_pv']!=0){
                        $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                    }

                    $se=$rowp['pe_pe'];
                    $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                    if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                        $se=$se+$suma[0]['total_ejecutado'];
                    }

                    //$suma_prog=$this->mdas_complementario->costo_total_programado($rowp['pfec_id']);
                    $suma_prog=$this->mdas_complementario->costo_total_proyecto($rowp['pfec_id']);
                    $tabla.='<tr class="modo1 derecha">';
                    $tabla.='<td class="izquierda">'.$rowue['uni_unidad'].'</td>';
                    $tabla.='<td class="izquierda">'.$rowp['proy_nombre'].'</td>';
                    $tabla.='<td>'.number_format($suma_prog[0]['total_programado'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pfecg_ppto_total'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pe_pi'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pe_pm'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pe_pv'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pe_pe'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.$ejec.'%</td>';
                    $efin=0;
                    if($suma_prog[0]['total_programado']!=0){
                        $efin=round((($se/$suma_prog[0]['total_programado'])*100),2);
                    }
                    $tabla.='<td>'.$efin.'%</td>';

                    $efis=$this->avance_fisico($rowp['proy_id'],$this->mes);
                    $tabla.='<td bgcolor="#cbe0cb">'.round($efis,2).'%</td>';
                    $estado=$this->mdas_complementario->estado_operacion($rowp['pfec_id'],$this->mes,$this->gestion);
                    $estado_proy='-----';
                    if(count($estado)!=0){
                        $estado_proy=$estado[0]['ep_descripcion'];
                    }
                    $tabla.='<td class="izquierda">'.$estado_proy.'</td>';
                    $tabla.='</tr>';

                }

            }
        }
        $total = $this->model_epresupuestaria->suma_programas_hijos_general($this->gestion,$this->mes,$uni_id);
        $tabla.='</tbody>';
        $tabla.='<tr class="modo1 derecha" bgcolor="#5f6367">';
        $tabla.='<th style="width:10%;"><font color="#ffffff">TOTAL</font></td>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        if(count($total)!=0){
            $tabla.='<th style="width:10%;"><font color="#ffffff">'.number_format($total[0]['pi'], 2, ',', ' ').'</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">'.number_format($total[0]['pm'], 2, ',', ' ').'</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">'.number_format($total[0]['pv'], 2, ',', ' ').'</font></th>';
        }
        else{
            $tabla.='<th style="width:10%;"><font color="#ffffff">0.00</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">0.00</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">0.00</font></th>';
        }
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='</tr>';
        $tabla.='</table><br>';

        /*echo "<br><br>";
        echo "SUMA TOTAL PI : ".$sum."";*/
        return $tabla;
    }

    /*======================== TABLA  EJECUCION DEL PRESUPUESTO GENERAL ==========================*/
    public function genera_tabla_general($tp)
    {
        $tabla = '';
        $tp_ejecucion1=$this->model_epresupuestaria->tp_unidad_ejec1();
        $tp_ejecucion2=$this->model_epresupuestaria->tp_unidad_ejec2();
        $suma_total=$this->model_epresupuestaria->suma_total($this->gestion,$this->mes);

        $pi_total=0;$pm_total=0;$pv_total=0;$pe_total=0;$e_total=0;
        if(count($suma_total)!=0){
            $pi_total=$suma_total[0]['pi'];
            $pm_total=$suma_total[0]['pm'];
            $pv_total=$suma_total[0]['pv'];
            $pe_total=$suma_total[0]['pe'];
            if($pv_total!=0){
                $e_total=round((($pe_total/$pv_total)*100),2);
            }
        }
        // $tabla .= '<table border=1>';
        $tabla .= '<tr bgcolor="#565655" class="modo1 derecha">';
        $tabla .= '<td class="izquierda"><font color ="#ffffff">TOTAL</font></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td><font color ="#ffffff">'.number_format($pi_total, 2, ',', '.').'</font></td>';
        $tabla .= '<td><font color ="#ffffff">'.number_format($pm_total, 2, ',', '.').'</font></td>';
        $tabla .= '<td><font color ="#ffffff">'.number_format($pv_total, 2, ',', '.').'</font></td>';
        $tabla .= '<td><font color ="#ffffff">'.number_format($pe_total, 2, ',', '.').'</font></td>';
        $tabla .= '<td><font color ="#ffffff">'.$e_total.'%</font></td>';
        $tabla .= '</tr>';

        foreach($tp_ejecucion1 as $rowe)
        {
            $suma=$this->model_epresupuestaria->suma_id_ue($this->gestion,$this->mes,$rowe['tue_id']);
            $pi=0;$pm=0;$pv=0;$pe=0;$e=0;
            if(count($suma)!=0){
                $pi=$suma[0]['pi'];
                $pm=$suma[0]['pm'];
                $pv=$suma[0]['pv'];
                $pe=$suma[0]['pe'];
                if($pv!=0){
                    $e=round((($pe/$pv)*100),2);
                }
            }
            $tabla .= '<tr bgcolor="#949090" class="modo1 derecha">';
            $tabla .= '<td class="izquierda">'.$rowe['tue_unidad'].'</td>'; ///// Organo Legislativo
            $tabla .= '<td></td>';
            $tabla .= '<td>'.number_format($pi, 2, ',', '.').'</td>';
            $tabla .= '<td>'.number_format($pm, 2, ',', '.').'</td>';
            $tabla .= '<td>'.number_format($pv, 2, ',', '.').'</td>';
            $tabla .= '<td>'.number_format($pe, 2, ',', '.').'</td>';
            $tabla .= '<td>'.$e.'%</td>';
            $tabla .= '</tr>';
            $ejecutoras=$this->model_epresupuestaria->unidades_ejecutoras($rowe['tue_id']);
            foreach($ejecutoras as $rowejec)
            {
                $lista_aper_padres = $this->mapertura_programatica->lista_aperturas_padres();
                $suma_pi=0;$suma_pm=0;$suma_pv=0;$suma_pe=0;
                foreach($lista_aper_padres as $rowap)
                {
                    $programas=$this->model_epresupuestaria->programas_hijos_general($rowap['aper_programa'],$rowap['aper_gestion'],$this->mes,$rowejec['uni_id']);
                    foreach($programas as $rowp)
                    {
                        $suma_pi=$suma_pi+$rowp['pe_pi'];
                        $suma_pm=$suma_pm+$rowp['pe_pm'];
                        $suma_pv=$suma_pv+$rowp['pe_pv'];
                        $suma_pe=$suma_pe+$rowp['pe_pe'];
                    }
                }
                $ejec=0;
                if($suma_pv!=0){
                    $ejec=round((($suma_pe/$suma_pv)*100),2);
                }
                $tabla .= '<tr class="modo1 derecha">';
                if($tp==1){
                    $tabla .= '<td class="izquierda"><b><a href="' . site_url("") . '/rep/ejec/detalle_presupuestaria/1/'.$rowejec['uni_id'].'" title="DETALLE : '.$rowejec['uni_unidad'].'">'.$rowejec['uni_unidad'].'</a></b></td>';
                }
                elseif ($tp==2) {
                    $tabla .= '<td><b>'.$rowejec['uni_unidad'].'</b></td>';
                }
                $total = $this->model_epresupuestaria->programas_hijos_general_total($this->gestion,$this->mes,1,$rowejec['uni_id']);
                $total_num=$total[0]['total'];
                $tabla .= '<td>'.$total_num.'</td>';
                $tabla .= '<td>'.number_format($suma_pi, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pm, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pv, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pe, 2, ',', '.').'</td>';
                $tabla .= '<td>'.$ejec.'%</td>';
                $tabla .= '</tr>';

            }
        }

        $suma_pre=$this->model_epresupuestaria->suma_pretotal($this->gestion,$this->mes);

        $pi_ptotal=0;$pm_ptotal=0;$pv_ptotal=0;$pe_ptotal=0;$e_ptotal=0;
        if(count($suma_pre)!=0){
            $pi_ptotal=$suma_pre[0]['pi'];
            $pm_ptotal=$suma_pre[0]['pm'];
            $pv_ptotal=$suma_pre[0]['pv'];
            $pe_ptotal=$suma_pre[0]['pe'];
            if($pv_ptotal!=0){
                $e_ptotal=round((($pe_ptotal/$pv_ptotal)*100),2);
            }
        }

        $tabla .= '<tr bgcolor="#949090" class="modo1 derecha">';
        $tabla .= '<td class="izquierda">ORGANO EJECUTIVO</td>';
        $tabla .= '<td></td>';
        $tabla .= '<td>'.number_format($pi_ptotal, 2, ',', '.').'</td>';
        $tabla .= '<td>'.number_format($pm_ptotal, 2, ',', '.').'</td>';
        $tabla .= '<td>'.number_format($pv_ptotal, 2, ',', '.').'</td>';
        $tabla .= '<td>'.number_format($pe_ptotal, 2, ',', '.').'</td>';
        $tabla .= '<td>'.$e_ptotal.'%</td>';
        $tabla .= '</tr>';

        foreach($tp_ejecucion2 as $rowe)
        {
            $suma=$this->model_epresupuestaria->suma_id_ue($this->gestion,$this->mes,$rowe['tue_id']);
            $pi=0;$pm=0;$pv=0;$pe=0;$e=0;
            if(count($suma)!=0){
                $pi=$suma[0]['pi'];
                $pm=$suma[0]['pm'];
                $pv=$suma[0]['pv'];
                $pe=$suma[0]['pe'];
                if($pv!=0){
                    $e=round((($pe/$pv)*100),2);
                }
            }
            $tabla .= '<tr bgcolor="#f4f4f4" class="modo1 derecha">';
            $tabla .= '<td class="izquierda"><b>'.$rowe['tue_unidad'].'</b></td>';
            $tabla .= '<td></td>';
            $tabla .= '<td><b>'.number_format($pi, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pm, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pv, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pe, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.$e.'%</b></td>';
            $tabla .= '</tr>';
            $ejecutoras=$this->model_epresupuestaria->unidades_ejecutoras($rowe['tue_id']);
            foreach($ejecutoras as $rowejec)
            {
                $lista_aper_padres = $this->mapertura_programatica->lista_aperturas_padres();
                $suma_pi=0;$suma_pm=0;$suma_pv=0;$suma_pe=0;
                foreach($lista_aper_padres as $rowap)
                {
                    $programas=$this->model_epresupuestaria->programas_hijos_general($rowap['aper_programa'],$rowap['aper_gestion'],$this->mes,$rowejec['uni_id']);
                    foreach($programas as $rowp)
                    {
                        $suma_pi=$suma_pi+$rowp['pe_pi'];
                        $suma_pm=$suma_pm+$rowp['pe_pm'];
                        $suma_pv=$suma_pv+$rowp['pe_pv'];
                        $suma_pe=$suma_pe+$rowp['pe_pe'];
                    }
                }
                $ejec=0;
                if($suma_pv!=0){
                    $ejec=round((($suma_pe/$suma_pv)*100),2);
                }
                $tabla .= '<tr class="modo1 derecha">';
                if($tp==1){
                    $tabla .= '<td class="izquierda"><b><a href="' . site_url("") . '/rep/ejec/detalle_presupuestaria/2/'.$rowejec['uni_id'].'" title="DETALLE : '.$rowejec['uni_unidad'].'">'.$rowejec['uni_unidad'].'</a></b></td>';
                }
                elseif($tp==2){
                    $tabla .= '<td><b>'.$rowejec['uni_unidad'].'</b></td>';
                }
                $total = $this->model_epresupuestaria->programas_hijos_general_total($this->gestion,$this->mes,2,$rowejec['uni_id']);
                $total_num=$total[0]['total'];
                $tabla .= '<td>'.$total_num.'</td>';
                $tabla .= '<td>'.number_format($suma_pi, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pm, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pv, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pe, 2, ',', '.').'</td>';
                $tabla .= '<td>'.$ejec.'%</td>';
                $tabla .= '</tr>';

            }
        }
        //  $tabla .= '</table>';
        return $tabla;
    }

    /*======================== REPORTE PRESUPUESTO GENERAL ==========================*/
    public function rep_pres_general()
    {
        $tabla=$this->genera_tabla_general(2);
        $mess=$this->get_mes($this->session->userdata("mes"));
        $mes = $mess[1];
        $dias = $mess[2];
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> EJECUCI&Oacute;N DEL PRESUPUESTO GENERAL POR UNIDAD EJECUTORA<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>';

        $html .= '
                 
                  <table border="0" cellpadding="0" cellspacing="0" class="tabla">
                    <tr class="modo1">
                        <th bgcolor="#4c4f53" style="width:20%;">UNIDAD</th>
                        <th bgcolor="#4c4f53" style="width:15%;">PRESUPUESTO INICIAL</th>
                        <th bgcolor="#4c4f53" style="width:15%;">MODIFICACIONES</th>
                        <th bgcolor="#4c4f53" style="width:15%;">PRESUPUESTO VIGENTE</th>
                        <th bgcolor="#4c4f53" style="width:15%;">EJECUCI&Oacute;N</th>
                        <th bgcolor="#4c4f53" style="width:10%;">% EJECUCI&Oacute;N</th>
                    </tr>
                    <tbody id="bdi">'.$tabla.'</tbody>
                  </table>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'portrait');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("rep_pres_general.pdf", array("Attachment" => false));
    }

    /*============ REPORTE TABLA DETALLES GENERAL ============*/
    public function reporte_detalle_pres_general($tp,$uni_id)
    {
        $mess=$this->get_mes($this->session->userdata("mes"));
        $mes = $mess[1];
        $dias = $mess[2];

        if($tp==1){
            $titulo='ORGANO LEGISLATIVO';
        }
        else{
            $titulo='ORGANO EJECUTIVO';
        }

        $unidad = $this->model_epresupuestaria->get_unidad_ejecutora($uni_id);
        $detalles=$this->tabla_detalle_pres_general($uni_id,2);

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> EJECUCI&Oacute;N DEL PRESUPUESTO GENERAL POR UNIDAD EJECUTORA / '.$unidad[0]['uni_unidad'].'<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$detalles.'
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_detalle_pres_general.pdf", array("Attachment" => false));
    }
    /*=================== EJECUCION DEL PRESUPUESTO PROYECTO DE INVERSION ==================*/
    public function presupuestaria_pinversion()
    {
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;

        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['tabla']=$this->genera_tabla_pinversion(1);

        $this->load->view('admin/reportes/ejecucion_presupuestaria/ejecucion_unidad_ejecutora/ejec_ejecutora_pi', $data);
    }

    /*======================== DETALLE EJECUCION DEL PRESUPUESTO PI ==========================*/
    public function detalle_pres_pinversion($tp,$uni_id)
    {
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;

        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        if($tp==1){
            $titulo='ORGANO LEGISLATIVO';
        }
        else{
            $titulo='ORGANO EJECUTIVO';
        }
        $data['organo'] = $titulo;
        $data['tp'] = $tp;
        $data['uni_id'] = $uni_id;
        $data['unidad'] = $this->model_epresupuestaria->get_unidad_ejecutora($uni_id);

        $data['atras']= '' . base_url() . 'index.php/rep/ejec/presupuestaria_pi';

        $data['detalles']=$this->tabla_detalle_pres_pinversion($uni_id,1);
        $this->load->view('admin/reportes/ejecucion_presupuestaria/ejecucion_unidad_ejecutora/ejec_ejecutora_pi_detalle', $data);

    }

    /*================== TABLA EJECUCION DEL PRESUPUESTO PROY INVERSION ====================*/
    public function genera_tabla_pinversion($tp)
    {
        $tabla = '';
        $tp_ejecucion1=$this->model_epresupuestaria->tp_unidad_ejec1();
        $tp_ejecucion2=$this->model_epresupuestaria->tp_unidad_ejec2();
        $suma_total=$this->model_epresupuestaria->suma_total_pi($this->gestion,$this->mes);

        $pi_total=0;$pm_total=0;$pv_total=0;$pe_total=0;$e_total=0;
        if(count($suma_total)!=0){
            $pi_total=$suma_total[0]['pi'];
            $pm_total=$suma_total[0]['pm'];
            $pv_total=$suma_total[0]['pv'];
            $pe_total=$suma_total[0]['pe'];
            if($pv_total!=0){
                $e_total=round((($pe_total/$pv_total)*100),2);
            }
        }
        //  $tabla .= '<table border=1>';
        $tabla .= '<tr bgcolor="#565655" class="modo1 derecha">';
        $tabla .= '<td class="izquierda"><font color ="#ffffff">TOTAL</font></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td><font color ="#ffffff">'.number_format($pi_total, 2, ',', '.').'</font></td>';
        $tabla .= '<td><font color ="#ffffff">'.number_format($pm_total, 2, ',', '.').'</font></td>';
        $tabla .= '<td><font color ="#ffffff">'.number_format($pv_total, 2, ',', '.').'</font></td>';
        $tabla .= '<td><font color ="#ffffff">'.number_format($pe_total, 2, ',', '.').'</font></td>';
        $tabla .= '<td><font color ="#ffffff">'.$e_total.'%</font></td>';
        $tabla .= '</tr>';

        foreach($tp_ejecucion1 as $rowe)
        {
            $suma=$this->model_epresupuestaria->suma_id_ue_ip($this->gestion,$this->mes,$rowe['tue_id']);
            $pi=0;$pm=0;$pv=0;$pe=0;$e=0;
            if(count($suma)!=0){
                $pi=$suma[0]['pi'];
                $pm=$suma[0]['pm'];
                $pv=$suma[0]['pv'];
                $pe=$suma[0]['pe'];
                if($pv!=0){
                    $e=round((($pe/$pv)*100),2);
                }
            }
            $tabla .= '<tr bgcolor="#949090" class="modo1 derecha">';
            $tabla .= '<td class="izquierda"><b>'.$rowe['tue_unidad'].'</b></td>';
            $tabla .= '<td></td>';
            $tabla .= '<td><b>'.number_format($pi, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pm, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pv, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pe, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.$e.'%</b></td>';
            $tabla .= '</tr>';
            $ejecutoras=$this->model_epresupuestaria->unidades_ejecutoras($rowe['tue_id']);
            foreach($ejecutoras as $rowejec)
            {
                $lista_aper_padres = $this->mapertura_programatica->lista_aperturas_padres();
                $suma_pi=0;$suma_pm=0;$suma_pv=0;$suma_pe=0;
                foreach($lista_aper_padres as $rowap)
                {
                    $programas=$this->model_epresupuestaria->programas_hijos_pinversion($rowap['aper_programa'],$rowap['aper_gestion'],$this->mes,$rowejec['uni_id']);
                    foreach($programas as $rowp)
                    {
                        $suma_pi=$suma_pi+$rowp['pe_pi'];
                        $suma_pm=$suma_pm+$rowp['pe_pm'];
                        $suma_pv=$suma_pv+$rowp['pe_pv'];
                        $suma_pe=$suma_pe+$rowp['pe_pe'];
                    }
                }
                $ejec=0;
                if($suma_pv!=0){
                    $ejec=round((($suma_pe/$suma_pv)*100),2);
                }
                $tabla .= '<tr class="modo1 derecha">';
                if($tp==1){
                    $tabla .= '<td class="izquierda"><b><a href="' . site_url("") . '/rep/ejec/detalle_presupuestaria_pi/1/'.$rowejec['uni_id'].'" title="DETALLE : '.$rowejec['uni_unidad'].'">'.$rowejec['uni_unidad'].'</a></b></td>';
                }
                elseif($tp==2){
                    $tabla .= '<td class="izquierda"><b>'.$rowejec['uni_unidad'].'</b></td>';
                }

                $nro_reg=$this->model_epresupuestaria->programas_hijos_pinversion_totales($this->gestion,$this->mes,$rowejec['uni_id']);
                $tabla .= '<td>'.$nro_reg.'</td>';
                $tabla .= '<td>'.number_format($suma_pi, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pm, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pv, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pe, 2, ',', '.').'</td>';
                $tabla .= '<td>'.$ejec.'%</td>';
                $tabla .= '</tr>';

            }
        }

        $suma_pre=$this->model_epresupuestaria->suma_pretotal_pi($this->gestion,$this->mes);

        $pi_ptotal=0;$pm_ptotal=0;$pv_ptotal=0;$pe_ptotal=0;$e_ptotal=0;
        if(count($suma_pre)!=0){
            $pi_ptotal=$suma_pre[0]['pi'];
            $pm_ptotal=$suma_pre[0]['pm'];
            $pv_ptotal=$suma_pre[0]['pv'];
            $pe_ptotal=$suma_pre[0]['pe'];
            if($pv_ptotal!=0){
                $e_ptotal=round((($pe_ptotal/$pv_ptotal)*100),2);
            }
        }

        $tabla .= '<tr bgcolor="#949090" class="modo1 derecha">';
        $tabla .= '<td class="izquierda">ORGANO EJECUTIVO</td>';
        $tabla .= '<td></td>';
        $tabla .= '<td>'.number_format($pi_ptotal, 2, ',', '.').'</td>';
        $tabla .= '<td>'.number_format($pm_ptotal, 2, ',', '.').'</td>';
        $tabla .= '<td>'.number_format($pv_ptotal, 2, ',', '.').'</td>';
        $tabla .= '<td>'.number_format($pe_ptotal, 2, ',', '.').'</td>';
        $tabla .= '<td>'.$e_ptotal.'%</td>';
        $tabla .= '</tr>';

        foreach($tp_ejecucion2 as $rowe)
        {
            $suma=$this->model_epresupuestaria->suma_id_ue_ip($this->gestion,$this->mes,$rowe['tue_id']);
            $pi=0;$pm=0;$pv=0;$pe=0;$e=0;
            if(count($suma)!=0){
                $pi=$suma[0]['pi'];
                $pm=$suma[0]['pm'];
                $pv=$suma[0]['pv'];
                $pe=$suma[0]['pe'];
                if($pv!=0){
                    $e=round((($pe/$pv)*100),2);
                }
            }
            $tabla .= '<tr bgcolor="#f4f4f4" class="modo1 derecha">';
            $tabla .= '<td class="izquierda"><b>'.$rowe['tue_unidad'].'</b></td>';
            $tabla .= '<td></td>';
            $tabla .= '<td><b>'.number_format($pi, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pm, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pv, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pe, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.$e.'%</b></td>';
            $tabla .= '</tr>';
            $ejecutoras=$this->model_epresupuestaria->unidades_ejecutoras($rowe['tue_id']);
            foreach($ejecutoras as $rowejec)
            {
                $lista_aper_padres = $this->mapertura_programatica->lista_aperturas_padres();
                $suma_pi=0;$suma_pm=0;$suma_pv=0;$suma_pe=0;
                foreach($lista_aper_padres as $rowap)
                {
                    $programas=$this->model_epresupuestaria->programas_hijos_pinversion($rowap['aper_programa'],$rowap['aper_gestion'],$this->mes,$rowejec['uni_id']);
                    foreach($programas as $rowp)
                    {
                        $suma_pi=$suma_pi+$rowp['pe_pi'];
                        $suma_pm=$suma_pm+$rowp['pe_pm'];
                        $suma_pv=$suma_pv+$rowp['pe_pv'];
                        $suma_pe=$suma_pe+$rowp['pe_pe'];
                    }
                }
                $ejec=0;
                if($suma_pv!=0){
                    $ejec=round((($suma_pe/$suma_pv)*100),2);
                }
                $tabla .= '<tr class="modo1 derecha">';
                if($tp==1){
                    $tabla .= '<td class="izquierda"><b><a href="' . site_url("") . '/rep/ejec/detalle_presupuestaria_pi/1/'.$rowejec['uni_id'].'" title="DETALLE : '.$rowejec['uni_unidad'].'">'.$rowejec['uni_unidad'].'</a></b></td>';
                }
                elseif($tp==2){
                    $tabla .= '<td><b>'.$rowejec['uni_unidad'].'</b></td>';
                }
                $nro_reg=$this->model_epresupuestaria->programas_hijos_pinversion_totales($this->gestion,$this->mes,$rowejec['uni_id']);
                $tabla .= '<td>'.$nro_reg.'</td>';
                $tabla .= '<td>'.number_format($suma_pi, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pm, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pv, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pe, 2, ',', '.').'</td>';
                $tabla .= '<td>'.$ejec.'%</td>';
                $tabla .= '</tr>';

            }
        }
        //  $tabla .= '</table>';
        return $tabla;
    }

    /*====================== TABLA DETALLE PROYECTOS DE INVERSION ======================*/
    public function tabla_detalle_pres_pinversion($uni_id,$tp)
    {
        if($tp==1){
            $tb='id="dt_basic" class="table table table-bordered" width="100%"';
        }
        elseif ($tp==2) {
            $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        }

        $unidades = $this->mdas_complementario->unidades_ejecutoras();
        $sum=0;
        $tabla ='';
        $tabla.='<table '.$tb.'>';
        $tabla.='<thead>';
        $tabla.='<tr class="modo1">';
        $tabla.='<th style="width:15%;">UNIDAD EJECUTORA</td>';
        $tabla.='<th style="width:15%;">PROYECTO-PROGRAMA.OPERACI&Oacute;N</th>';
        $tabla.='<th style="width:10%;">COSTO TOTAL DE PROYECTO</th>';
        $tabla.='<th style="width:10%;">COSTO TOTAL GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">PRESUPUESTO INICIAL '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">PRESUPUESTO MODIFICADO '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">PRESUPUESTO VIGENTE '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">EJEC. FINANCIERA DE GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">% EJEC. FINANCIERA '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">% EJEC. FINANCIERA DE PROYECTO </th>';
        $tabla.='<th style="width:10%;">% EJEC. F&Iacute;SICA DE PROYECTO </th>';
        $tabla.='<th style="width:10%;">ESTADO</th>';
        $tabla.='</tr>';
        $tabla.='</thead>';
        $tabla.='<tbody>';

        foreach($unidades as $rowue)
        {
            $programas = $this->model_epresupuestaria->programas_hijos_pinversion_detalles($this->gestion,$this->mes,$uni_id,$rowue['uni_id']);
            if(count($programas)!=0){
                //  echo "UNI ID ".$rowue['uni_id']." UNIDAD EJECUTORA : ".$rowue['uni_unidad']."<br>";
                //  echo "NRO : ".count($programas)."<br>";
                foreach($programas as $rowp)
                {
                    $ejec=0;
                    if($rowp['pe_pv']!=0){
                        $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                    }

                    $se=$rowp['pe_pe'];
                    $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                    if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                        $se=$se+$suma[0]['total_ejecutado'];
                    }

                    $suma_prog=$this->mdas_complementario->costo_total_programado($rowp['pfec_id']);

                    $tabla.='<tr class="modo1 derecha">';
                    $tabla.='<td class="izquierda">'.$rowue['uni_unidad'].'</td>';
                    $tabla.='<td class="izquierda">'.$rowp['proy_nombre'].'</td>';
                    $tabla.='<td>'.number_format($suma_prog[0]['total_programado'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pfecg_ppto_total'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pe_pi'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pe_pm'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pe_pv'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pe_pe'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.$ejec.'%</td>';
                    $efin=0;
                    if($suma_prog[0]['total_programado']!=0){
                        $efin=round((($se/$suma_prog[0]['total_programado'])*100),2);
                    }
                    $tabla.='<td>'.$efin.'%</td>';

                    $efis=$this->avance_fisico($rowp['proy_id'],$this->mes);
                    $tabla.='<td>'.round($efis,2).'%</td>';
                    $estado=$this->mdas_complementario->estado_operacion($rowp['pfec_id'],$this->mes,$this->gestion);
                    $estado_proy='-----';
                    if(count($estado)!=0){
                        $estado_proy=$estado[0]['ep_descripcion'];
                    }
                    $tabla.='<td class="izquierda">'.$estado_proy.'</td>';
                    $tabla.='</tr>';

                }

            }
        }
        $total = $this->model_epresupuestaria->suma_programas_hijos_pinversion($this->gestion,$this->mes,$uni_id);
        $tabla.='</tbody>';
        $tabla.='<tr class="modo1" bgcolor="#5f6367">';
        $tabla.='<th style="width:10%;"><font color="#ffffff">TOTAL</font></td>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        if(count($total)!=0){
            $tabla.='<th style="width:10%;"><font color="#ffffff">'.number_format($total[0]['pi'], 2, ',', ' ').'</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">'.number_format($total[0]['pm'], 2, ',', ' ').'</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">'.number_format($total[0]['pv'], 2, ',', ' ').'</font></th>';
        }
        else{
            $tabla.='<th style="width:10%;"><font color="#ffffff">0.00</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">0.00</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">0.00</font></th>';
        }
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='</tr>';
        $tabla.='</table><br>';

        /*echo "<br><br>";
        echo "SUMA TOTAL PI : ".$sum."";*/
        return $tabla;
    }

    /*======================== REPORTE PRESUPUESTO PROYECTOS DE INVERSION ==========================*/
    public function rep_pres_pinversion()
    {
        $tabla=$this->genera_tabla_pinversion(2);
        $mess=$this->get_mes($this->session->userdata("mes"));
        $mes = $mess[1];
        $dias = $mess[2];
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> DETALLE DE EJECUCI&Oacute;N PRESUPUESTARIA POR UNIDAD EJECUTORA<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>';

        $html .= '
                 
                  <table border="0" cellpadding="0" cellspacing="0" class="tabla">
                    <tr class="modo1">
                        <th bgcolor="#4c4f53" style="width:20%;">UNIDAD</th>
                        <th bgcolor="#4c4f53" style="width:15%;">PRESUPUESTO INICIAL</th>
                        <th bgcolor="#4c4f53" style="width:15%;">MODIFICACIONES</th>
                        <th bgcolor="#4c4f53" style="width:15%;">PRESUPUESTO VIGENTE</th>
                        <th bgcolor="#4c4f53" style="width:15%;">EJECUCI&Oacute;N</th>
                        <th bgcolor="#4c4f53" style="width:10%;">% EJECUCI&Oacute;N</th>
                    </tr>
                    <tbody id="bdi">'.$tabla.'</tbody>
                  </table>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'portrait');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("rep_pres_pinversion.pdf", array("Attachment" => false));
    }
    /*============ REPORTE TABLA DETALLES PI ============*/
    public function reporte_detalle_pres_pinversion($tp,$uni_id)
    {
        $mess=$this->get_mes($this->session->userdata("mes"));
        $mes = $mess[1];
        $dias = $mess[2];

        if($tp==1){
            $titulo='ORGANO LEGISLATIVO';
        }
        else{
            $titulo='ORGANO EJECUTIVO';
        }

        $unidad = $this->model_epresupuestaria->get_unidad_ejecutora($uni_id);
        $detalles=$this->tabla_detalle_pres_pinversion($uni_id,2);

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> DETALLE DE EJECUCI&Oacute;N PRESUPUESTARIA DE INVERSI&Oacute;N P&Uacute;BLICA POR UNIDAD EJECUTORA  / '.$unidad[0]['uni_unidad'].'<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$detalles.'
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_detalle_pres_pinversion.pdf", array("Attachment" => false));
    }
    /*========================================================================================*/

    /*=================== EJECUCION DEL PRESUPUESTO DE PROGRAMAS ==================*/
    public function presupuestaria_prog()
    {
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;

        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['tabla']=$this->genera_tabla_programas(1);
        // echo $data['tabla'];

        $this->load->view('admin/reportes/ejecucion_presupuestaria/ejecucion_unidad_ejecutora/ejec_ejecutora_prog', $data);
    }
    /*======================== DETALLE EJECUCION DEL PRESUPUESTO PROG ==========================*/
    public function detalle_pres_prog($tp,$uni_id)
    {
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;

        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        if($tp==1){
            $titulo='ORGANO LEGISLATIVO';
        }
        else{
            $titulo='ORGANO EJECUTIVO';
        }
        $data['organo'] = $titulo;
        $data['tp'] = $tp;
        $data['uni_id'] = $uni_id;
        $data['unidad'] = $this->model_epresupuestaria->get_unidad_ejecutora($uni_id);

        $data['atras']= '' . base_url() . 'index.php/rep/ejec/presupuestaria_prog';

        $data['detalles']=$this->tabla_detalle_pres_prog($uni_id,1);
        $this->load->view('admin/reportes/ejecucion_presupuestaria/ejecucion_unidad_ejecutora/ejec_ejecutora_prog_detalle', $data);

    }

    /*================== TABLA EJECUCION DEL PRESUPUESTO POR PROGRAMAS ====================*/
    public function genera_tabla_programas($tp)
    {
        $tabla = '';
        $tp_ejecucion1=$this->model_epresupuestaria->tp_unidad_ejec1();
        $tp_ejecucion2=$this->model_epresupuestaria->tp_unidad_ejec2();
        $suma_total=$this->model_epresupuestaria->suma_total_pr($this->gestion,$this->mes);

        $pi_total=0;$pm_total=0;$pv_total=0;$pe_total=0;$e_total=0;
        if(count($suma_total)!=0){
            $pi_total=$suma_total[0]['pi'];
            $pm_total=$suma_total[0]['pm'];
            $pv_total=$suma_total[0]['pv'];
            $pe_total=$suma_total[0]['pe'];
            if($pv_total!=0){
                $e_total=round((($pe_total/$pv_total)*100),2);
            }
        }
        //  $tabla .= '<table border=1>';
        $tabla .= '<tr bgcolor="#565655" class="modo1 derecha">';
        $tabla .= '<td class="izquierda"><font color ="#ffffff">TOTAL</font></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td><font color ="#ffffff">'.number_format($pi_total, 2, ',', '.').'</font></td>';
        $tabla .= '<td><font color ="#ffffff">'.number_format($pm_total, 2, ',', '.').'</font></td>';
        $tabla .= '<td><font color ="#ffffff">'.number_format($pv_total, 2, ',', '.').'</font></td>';
        $tabla .= '<td><font color ="#ffffff">'.number_format($pe_total, 2, ',', '.').'</font></td>';
        $tabla .= '<td><font color ="#ffffff">'.$e_total.'%</font></td>';
        $tabla .= '</tr>';

        foreach($tp_ejecucion1 as $rowe)
        {
            $suma=$this->model_epresupuestaria->suma_id_ue_pr($this->gestion,$this->mes,$rowe['tue_id']);
            $pi=0;$pm=0;$pv=0;$pe=0;$e=0;
            if(count($suma)!=0){
                $pi=$suma[0]['pi'];
                $pm=$suma[0]['pm'];
                $pv=$suma[0]['pv'];
                $pe=$suma[0]['pe'];
                if($pv!=0){
                    $e=round((($pe/$pv)*100),2);
                }
            }
            $tabla .= '<tr bgcolor="#949090" class="modo1 derecha">';
            $tabla .= '<td class="izquierda"><b>'.$rowe['tue_unidad'].'</b></td>';
            $tabla .= '<td></td>';
            $tabla .= '<td><b>'.number_format($pi, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pm, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pv, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pe, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.$e.'%</td>';
            $tabla .= '</tr>';
            $ejecutoras=$this->model_epresupuestaria->unidades_ejecutoras($rowe['tue_id']);
            foreach($ejecutoras as $rowejec)
            {
                $lista_aper_padres = $this->mapertura_programatica->lista_aperturas_padres();
                $suma_pi=0;$suma_pm=0;$suma_pv=0;$suma_pe=0;
                foreach($lista_aper_padres as $rowap)
                {
                    $programas=$this->model_epresupuestaria->programas_hijos_programas($rowap['aper_programa'],$rowap['aper_gestion'],$this->mes,$rowejec['uni_id']);
                    foreach($programas as $rowp)
                    {
                        $suma_pi=$suma_pi+$rowp['pe_pi'];
                        $suma_pm=$suma_pm+$rowp['pe_pm'];
                        $suma_pv=$suma_pv+$rowp['pe_pv'];
                        $suma_pe=$suma_pe+$rowp['pe_pe'];
                    }
                }
                $ejec=0;
                if($suma_pv!=0){
                    $ejec=round((($suma_pe/$suma_pv)*100),2);
                }
                $tabla .= '<tr class="modo1 derecha">';
                if($tp==1){
                    $tabla .= '<td class="izquierda"><a href="' . site_url("") . '/rep/ejec/detalle_presupuestaria_prog/1/'.$rowejec['uni_id'].'" title="DETALLE : '.$rowejec['uni_unidad'].'">'.$rowejec['uni_unidad'].'</a></td>';
                }
                elseif ($tp==2) {
                    $tabla .= '<td class="izquierda">'.$rowejec['uni_unidad'].'</td>';
                }
                $nro_progs=$this->model_epresupuestaria->programas_hijos_programas_total($this->gestion,$this->mes,$rowejec['uni_id']);
                $tabla .= '<td>'.$nro_progs.'</td>';
                $tabla .= '<td>'.number_format($suma_pi, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pm, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pv, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pe, 2, ',', '.').'</td>';
                $tabla .= '<td>'.$ejec.'%</td>';
                $tabla .= '</tr>';

            }
        }

        $suma_pre=$this->model_epresupuestaria->suma_pretotal_pr($this->gestion,$this->mes);

        $pi_ptotal=0;$pm_ptotal=0;$pv_ptotal=0;$pe_ptotal=0;$e_ptotal=0;
        if(count($suma_pre)!=0){
            $pi_ptotal=$suma_pre[0]['pi'];
            $pm_ptotal=$suma_pre[0]['pm'];
            $pv_ptotal=$suma_pre[0]['pv'];
            $pe_ptotal=$suma_pre[0]['pe'];
            if($pv_ptotal!=0){
                $e_ptotal=round((($pe_ptotal/$pv_ptotal)*100),2);
            }
        }

        $tabla .= '<tr bgcolor="#949090" class="modo1 derecha">';
        $tabla .= '<td class="izquierda">ORGANO EJECUTIVO</td>';
        $tabla .= '<td></td>';
        $tabla .= '<td>'.number_format($pi_ptotal, 2, ',', '.').'</td>';
        $tabla .= '<td>'.number_format($pm_ptotal, 2, ',', '.').'</td>';
        $tabla .= '<td>'.number_format($pv_ptotal, 2, ',', '.').'</td>';
        $tabla .= '<td>'.number_format($pe_ptotal, 2, ',', '.').'</td>';
        $tabla .= '<td>'.$e_ptotal.'%</td>';
        $tabla .= '</tr>';

        foreach($tp_ejecucion2 as $rowe)
        {
            $suma=$this->model_epresupuestaria->suma_id_ue_pr($this->gestion,$this->mes,$rowe['tue_id']);
            $pi=0;$pm=0;$pv=0;$pe=0;$e=0;
            if(count($suma)!=0){
                $pi=$suma[0]['pi'];
                $pm=$suma[0]['pm'];
                $pv=$suma[0]['pv'];
                $pe=$suma[0]['pe'];
                if($pv!=0){
                    $e=round((($pe/$pv)*100),2);
                }
            }
            $tabla .= '<tr bgcolor="#f4f4f4" class="modo1 derecha">'; //// Tipo de Unidades Ejecutoras
            $tabla .= '<td class="izquierda"><b>'.$rowe['tue_unidad'].'</b></td>';
            $tabla .= '<td></td>';
            $tabla .= '<td><b>'.number_format($pi, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pm, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pv, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.number_format($pe, 2, ',', '.').'</b></td>';
            $tabla .= '<td><b>'.$e.'%</b></td>';
            $tabla .= '</tr>';
            $ejecutoras=$this->model_epresupuestaria->unidades_ejecutoras($rowe['tue_id']);
            foreach($ejecutoras as $rowejec)
            {
                $lista_aper_padres = $this->mapertura_programatica->lista_aperturas_padres();
                $suma_pi=0;$suma_pm=0;$suma_pv=0;$suma_pe=0;
                foreach($lista_aper_padres as $rowap)
                {
                    $programas=$this->model_epresupuestaria->programas_hijos_programas($rowap['aper_programa'],$rowap['aper_gestion'],$this->mes,$rowejec['uni_id']);
                    foreach($programas as $rowp)
                    {
                        $suma_pi=$suma_pi+$rowp['pe_pi'];
                        $suma_pm=$suma_pm+$rowp['pe_pm'];
                        $suma_pv=$suma_pv+$rowp['pe_pv'];
                        $suma_pe=$suma_pe+$rowp['pe_pe'];
                    }
                }
                $ejec=0;
                if($suma_pv!=0){
                    $ejec=round((($suma_pe/$suma_pv)*100),2);
                }
                $tabla .= '<tr class="modo1 derecha">'; //// Unidades Ejecutoras
                if($tp==1){
                    $tabla .= '<td class="izquierda"><a href="' . site_url("") . '/rep/ejec/detalle_presupuestaria_prog/1/'.$rowejec['uni_id'].'" title="DETALLE : '.$rowejec['uni_unidad'].'">'.$rowejec['uni_unidad'].'</a></td>';
                }
                elseif($tp==2){
                    $tabla .= '<td class="izquierda">'.$rowejec['uni_unidad'].'</td>';
                }
                $nro_progs=$this->model_epresupuestaria->programas_hijos_programas_total($this->gestion,$this->mes,$rowejec['uni_id']);
                $tabla .= '<td>'.$nro_progs.'</td>';
                $tabla .= '<td>'.number_format($suma_pi, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pm, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pv, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_pe, 2, ',', '.').'</td>';
                $tabla .= '<td>'.$ejec.'%</td>';
                $tabla .= '</tr>';

            }
        }
        //  $tabla .= '</table>';
        return $tabla;
    }

    /*====================== TABLA DETALLE PROGRAMAS ======================*/
    public function tabla_detalle_pres_prog($uni_id,$tp)
    {
        if($tp==1){
            $tb='id="dt_basic" class="table table table-bordered" width="100%"';
        }
        elseif ($tp==2) {
            $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        }

        $unidades = $this->mdas_complementario->unidades_ejecutoras();
        $sum=0;
        $tabla ='';
        $tabla.='<table '.$tb.'>';
        $tabla.='<thead>';
        $tabla.='<tr class="modo1">';
        $tabla.='<th style="width:15%;">UNIDAD EJECUTORA</td>';
        $tabla.='<th style="width:15%;">PROYECTO-PROGRAMA.OPERACI&Oacute;N</th>';
        $tabla.='<th style="width:10%;">COSTO TOTAL DE PROYECTO</th>';
        $tabla.='<th style="width:10%;">COSTO TOTAL GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">PRESUPUESTO INICIAL '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">PRESUPUESTO MODIFICADO '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">PRESUPUESTO VIGENTE '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">EJEC. FINANCIERA DE GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">% EJEC. FINANCIERA '.$this->session->userdata('gestion').'</th>';
        $tabla.='<th style="width:10%;">% EJEC. FINANCIERA DE PROYECTO </th>';
        $tabla.='<th style="width:10%;">% EJEC. F&Iacute;SICA ACUMULADA </th>';
        $tabla.='<th style="width:10%;">ESTADO</th>';
        $tabla.='</tr>';
        $tabla.='</thead>';
        $tabla.='<tbody>';

        foreach($unidades as $rowue)
        {
            $programas = $this->model_epresupuestaria->programas_hijos_programas_detalles($this->gestion,$this->mes,$uni_id,$rowue['uni_id']);
            if(count($programas)!=0){
                //  echo "UNI ID ".$rowue['uni_id']." UNIDAD EJECUTORA : ".$rowue['uni_unidad']."<br>";
                //  echo "NRO : ".count($programas)."<br>";
                foreach($programas as $rowp)
                {
                    $ejec=0;
                    if($rowp['pe_pv']!=0){
                        $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                    }

                    $se=$rowp['pe_pe'];
                    $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                    if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                        $se=$se+$suma[0]['total_ejecutado'];
                    }

                    $suma_prog=$this->mdas_complementario->costo_total_programado($rowp['pfec_id']);

                    $tabla.='<tr class="modo1 derecha">';
                    $tabla.='<td class="izquierda">'.$rowue['uni_unidad'].'</td>';
                    $tabla.='<td class="izquierda">'.$rowp['proy_nombre'].'</td>';
                    $tabla.='<td>'.number_format($suma_prog[0]['total_programado'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pfecg_ppto_total'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pe_pi'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pe_pm'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pe_pv'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.number_format($rowp['pe_pe'], 2, ',', ' ').'</td>';
                    $tabla.='<td>'.$ejec.'%</td>';
                    $efin=0;
                    if($suma_prog[0]['total_programado']!=0){
                        $efin=round((($se/$suma_prog[0]['total_programado'])*100),2);
                    }
                    $tabla.='<td>'.$efin.'%</td>';

                    $efis=$this->avance_fisico($rowp['proy_id'],$this->mes);
                    $tabla.='<td>'.round($efis,2).'%</td>';
                    $estado=$this->mdas_complementario->estado_operacion($rowp['pfec_id'],$this->mes,$this->gestion);
                    $estado_proy='-----';
                    if(count($estado)!=0){
                        $estado_proy=$estado[0]['ep_descripcion'];
                    }
                    $tabla.='<td class="izquierda">'.$estado_proy.'</td>';
                    $tabla.='</tr>';

                }

            }
        }
        $total = $this->model_epresupuestaria->suma_programas_hijos_programas($this->gestion,$this->mes,$uni_id);
        $tabla.='</tbody>';
        $tabla.='<tr class="modo1" bgcolor="#5f6367">';
        $tabla.='<th style="width:10%;"><font color="#ffffff">TOTAL</font></td>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        if(count($total)!=0){
            $tabla.='<th style="width:10%; text-align:right; font-size:11px;"><font color="#ffffff">'.number_format($total[0]['pi'], 2, ',', ' ').'</font></th>';
            $tabla.='<th style="width:10%; text-align:right; font-size:11px;"><font color="#ffffff">'.number_format($total[0]['pm'], 2, ',', ' ').'</font></th>';
            $tabla.='<th style="width:10%; text-align:right; font-size:11px;"><font color="#ffffff">'.number_format($total[0]['pv'], 2, ',', ' ').'</font></th>';
        }
        else{
            $tabla.='<th style="width:10%;"><font color="#ffffff">0.00</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">0.00</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">0.00</font></th>';
        }

        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='<th style="width:10%;"></th>';
        $tabla.='</tr>';
        $tabla.='</table><br>';

        /*echo "<br><br>";
        echo "SUMA TOTAL PI : ".$sum."";*/
        return $tabla;
    }

    /*======================== REPORTE PRESUPUESTO PROGRAMAS ==========================*/
    public function rep_pres_prog()
    {
        $tabla=$this->genera_tabla_programas(2);
        $mess=$this->get_mes($this->session->userdata("mes"));
        $mes = $mess[1];
        $dias = $mess[2];
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> DETALLE DE EJECUCI&Oacute;N PRESUPUESTARIA DE PROGRAMAS POR UNIDAD EJECUTORA<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>';

        $html .= '
                 
                  <table border="0" cellpadding="0" cellspacing="0" class="tabla">
                    <tr class="modo1">
                        <th bgcolor="#4c4f53" style="width:20%;">UNIDAD</th>
                        <th bgcolor="#4c4f53" style="width:15%;">PRESUPUESTO INICIAL</th>
                        <th bgcolor="#4c4f53" style="width:15%;">MODIFICACIONES</th>
                        <th bgcolor="#4c4f53" style="width:15%;">PRESUPUESTO VIGENTE</th>
                        <th bgcolor="#4c4f53" style="width:15%;">EJECUCI&Oacute;N</th>
                        <th bgcolor="#4c4f53" style="width:10%;">% EJECUCI&Oacute;N</th>
                    </tr>
                    <tbody id="bdi">'.$tabla.'</tbody>
                  </table>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'portrait');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }

    /*============ REPORTE TABLA DETALLES PROGRAMAS ============*/
    public function reporte_detalle_pres_prog($tp,$uni_id)
    {
        $mess=$this->get_mes($this->session->userdata("mes"));
        $mes = $mess[1];
        $dias = $mess[2];

        if($tp==1){
            $titulo='ORGANO LEGISLATIVO';
        }
        else{
            $titulo='ORGANO EJECUTIVO';
        }

        $unidad = $this->model_epresupuestaria->get_unidad_ejecutora($uni_id);
        $detalles=$this->tabla_detalle_pres_prog($uni_id,2);

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> DETALLE DE EJECUCI&Oacute;N PRESUPUESTARIA DE PROGRAMAS POR UNIDAD EJECUTORA  / '.$unidad[0]['uni_unidad'].'<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$detalles.'
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_detalle_pres_prog.pdf", array("Attachment" => false));
    }

    /*======================= EJECUCION FISICA DEL PROYECTO AL MES ACTUAL =============================*/
    public function avance_fisico($proy_id,$id_mes)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $meses=$años*12;

        $ms[0]='programado_total';$ms[1]='enero';$ms[2]='febrero';$ms[3]='marzo';$ms[4]='abril';$ms[5]='mayo';$ms[6]='junio';
        $ms[7]='julio';$ms[8]='agosto';$ms[9]='septiembre';$ms[10]='octubre';$ms[11]='noviembre';$ms[12]='diciembre';

        $componentes = $this->model_componente->componentes_id($fase[0]['id']);
        for($p=1;$p<=$meses;$p++){$cp[1][$p]=0; $cp[2][$p]=0; $cp[3][$p]=0;}
        foreach ($componentes as $rowc)
        {
            $productos = $this->model_producto->list_prod($rowc['com_id']);
            for($p=1;$p<=$meses;$p++){$pp[1][$p]=0; $pp[2][$p]=0;}
            // echo "COMPONENTE : ".$rowc['com_componente']."<br>";
            foreach ($productos as $rowp)
            {
                $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                //  echo "PRODUCTO : ".$rowp['prod_producto']."<br>";
                for($p=1;$p<=$meses;$p++){$ap[1][$p]=0; $ap[2][$p]=0;}
                foreach ($actividad as $rowa)
                {
                    $variablep=1;$variablee=1; $v=12; $nro=1; $sump=0;$sume=0;
                    for($p=1;$p<=$meses;$p++){$a[1][$p]=0; $a[2][$p]=0;$a[3][$p]=0;$a[4][$p]=0;$a[5][$p]=0;$a[6][$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                    for($i=$fase[0]['pfec_fecha_inicio'];$i<=$fase[0]['pfec_fecha_fin'];$i++)
                    {
                        $aprog = $this->model_actividad->actividad_programado($rowa['act_id'],$i);
                        if(count($aprog)!=0){
                            for ($j=1; $j <=12 ; $j++) {
                                $a[1][$variablep]=$aprog[0][$ms[$j]];
                                $variablep++;
                            }
                        }
                        else{
                            $variablep=($v*$nro)+1;
                        }

                        $aejec = $this->model_actividad->actividad_ejecutado($rowa['act_id'],$i);
                        if(count($aejec)!=0){
                            for ($j=1; $j <=12 ; $j++) {
                                $a[4][$variablee]=$aejec[0][$ms[$j]];
                                $variablee++;
                            }
                        }
                        else{
                            $variablee=($v*$nro)+1;
                        }

                        $nro++;
                    }

                    for ($i=1; $i <=$meses ; $i++) {
                        $sump=$sump+$a[1][$i];
                        $a[2][$i]=$sump+$rowa['act_linea_base'];

                        if($rowa['act_meta']!=0)
                        {
                            $a[3][$i]=round(((($sump+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                        }
                        $ap[1][$i]=$ap[1][$i]+(($a[3][$i]*$rowa['act_ponderacion'])/100);

                        $sume=$sume+$a[4][$i];
                        $a[5][$i]=$sume+$rowa['act_linea_base'];

                        if($rowa['act_meta']!=0)
                        {
                            $a[6][$i]=round(((($sume+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                        }
                        $ap[2][$i]=$ap[2][$i]+(($a[6][$i]*$rowa['act_ponderacion'])/100);
                    }

                }

                for ($i=1; $i <=$meses ; $i++) {
                    $pp[1][$i]=$pp[1][$i]+(($ap[1][$i]*$rowp['prod_ponderacion'])/100);
                    $pp[2][$i]=$pp[2][$i]+(($ap[2][$i]*$rowp['prod_ponderacion'])/100);
                }

            }
            for ($i=1; $i <=$meses ; $i++) {
                $cp[1][$i]=$cp[1][$i]+(($pp[1][$i]*$rowc['com_ponderacion'])/100);
                $cp[2][$i]=$cp[2][$i]+(($pp[2][$i]*$rowc['com_ponderacion'])/100);
            }

        }

        for ($i=1; $i <=$meses ; $i++) {
            if($cp[1][$i]!=0){
                $cp[3][$i]=round((($cp[2][$i]/$cp[1][$i])*100),1);
            }
        }

        for ($i=1; $i <=12 ; $i++) {
            $tefic[1][$i]=0;$tefic[2][$i]=0;$tefic[3][$i]=0;
        }
        $i=1;$vmeses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;
        for($p=$fase[0]['pfec_fecha_inicio'];$p<=$fase[0]['pfec_fecha_fin'];$p++)
        {
            $variable=0;
            for($pos=$i;$pos<=$vmeses;$pos++)
            {
                $variable++;
                $tefic[1][$variable]=$cp[1][$pos];
                $tefic[2][$variable]=$cp[2][$pos];
                $tefic[3][$variable]=$cp[3][$pos];
            }
            if($p==$this->session->userdata('gestion'))
            {
                $ejecucion=$tefic[2][$id_mes];
            }

            $i=$vmeses+1;
            $vmeses=$vmeses+12;
            $gestion++; $nro++;
        }

        return $ejecucion;
    }
    /*=================================================================================================*/
    public function get_mes($mes_id)
    {
        $mes[1]='Enero';
        $mes[2]='Febrero';
        $mes[3]='Marzo';
        $mes[4]='Abril';
        $mes[5]='Mayo';
        $mes[6]='Junio';
        $mes[7]='Julio';
        $mes[8]='Agosto';
        $mes[9]='Septiembre';
        $mes[10]='Octubre';
        $mes[11]='Noviembre';
        $mes[12]='Diciembre';

        $dias[1]='31';
        $dias[2]='28';
        $dias[3]='31';
        $dias[4]='30';
        $dias[5]='31';
        $dias[6]='30';
        $dias[7]='31';
        $dias[8]='31';
        $dias[9]='30';
        $dias[10]='31';
        $dias[11]='30';
        $dias[12]='31';

        $valor[1]=$mes[$mes_id];
        $valor[2]=$dias[$mes_id];

        return $valor;
    }
    private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }

        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        .mv{font-size:10px;}
        .siipp{width:120px;}

.titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .tabla {
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 7px;
            width: 100%;

}
        .tabla th {
padding: 5px;
font-size: 7px;
background-color: #83aec0;
background-image: url(fondo_th.png);
background-repeat: repeat-x;
color: #FFFFFF;
border-right-width: 1px;
border-bottom-width: 1px;
border-right-style: solid;
border-bottom-style: solid;
border-right-color: #558FA6;
border-bottom-color: #558FA6;
font-family: "Trebuchet MS", Arial;
text-transform: uppercase;
}
.tabla .modo1 {
font-size: 7px;
font-weight:bold;
background-color: #e2ebef;
background-image: url(fondo_tr01.png);
background-repeat: repeat-x;
color: #34484E;
font-family: "Trebuchet MS", Arial;
}
.tabla .modo1 td {
padding: 5px;
border-right-width: 1px;
border-bottom-width: 1px;
border-right-style: solid;
border-bottom-style: solid;
border-right-color: #A4C4D0;
border-bottom-color: #A4C4D0;

}
    </style>';
}