<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class c_uejec extends CI_Controller { 
    var $gestion;
    var $mes;
    var $rol;
    var $fun_id;

  public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf');
        $this->load->library('pdf2');

        $this->load->model('reportes_tarija_das/mdas_complementario');

        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_producto');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/model_actividad');

        $this->load->model('programacion/model_proyecto');
        $this->load->model('mantenimiento/mapertura_programatica');
        $this->load->model('programacion/model_reporte_tipogasto');
        $this->load->model('reportes_tarija/model_uejec');
        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);

        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
        $this->rol = $this->session->userData('rol');
        $this->fun_id = $this->session->userData('fun_id');

        }else{
            redirect('/','refresh');
        }
    }

    public function unidad_ejecutora()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['mue']=$this->get_matriz_ue();
        $data['tabla']=$this->genera_tabla_tue(1);
        $this->load->view('admin/reportes/ejecucion_presupuestaria/unidad_ejecutora/u_ejecutora', $data);
    }

    public function reporte_unidad_ejecutora_grafico()
    { 
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['mue']=$this->get_matriz_ue();
        $data['tabla']=$this->genera_tabla_tue(1);
        $this->load->view('admin/reportes/ejecucion_presupuestaria/unidad_ejecutora/u_ejecutora_imprimir', $data);
    }

    public function reporte_unidad_ejecutora()
   {
    $mess=$this->get_mes($this->session->userdata("mes"));
    $mes = $mess[1];
    $dias = $mess[2];

    $data['mue']=$this->get_matriz_ue();
    $tabla=$this->genera_tabla_tue(2);

    $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> RESUMEN EJECUCI&Oacute;N PRESPUESTARIA POR GRUPO DE UNIDADES EJECUTORAS (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') <br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <div>
                  <table border="0" cellpadding="0" cellspacing="0" class="tabla">
                        <thead>
                          <tr>
                            <th>UNIDAD</th>
                            <th>PRESUPUESTO INICIAL</th>
                            <th>MODIFICACIONES</th>
                            <th>PRESUPUESTO VIGENTE</th>
                            <th>EJECUCI&Oacute;N</th>
                            <th>% EJECUCI&Oacute;N</th>
                          </tr>
                        </thead>
                        <tbody>
                      '.$tabla.'
                    </tbody>
                  </table>
                </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_rubro_gasto.pdf", array("Attachment" => false));
   }












    public function detalle_unidad_ejecutora($tue_id)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['tue_id'] = $tue_id;
        $data['tp_ejecucion'] = $this->model_uejec->get_tp_unidad_ejec($tue_id);

        $data['atras']= '' . base_url() . 'index.php/rep/ejec/unidad_ejec';

        $data['detalle']=$this->tabla_detalle($tue_id,1);
      
        $this->load->view('admin/reportes/ejecucion_presupuestaria/unidad_ejecutora/u_ejecutora_detalle', $data);
    }

    /*================== TABLA DETALLE TIPO DE UNIDAD EJECUTORA, UNIDAD EJECUTORA ======================*/
    
    public function tabla_detalle($tue_id,$tp)
    {
        if($tp==1){
        $tb='id="dt_basic" class="table table table-bordered" width="100%"';
        }
        elseif ($tp==2) {
          $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        }

        $unidades = $this->mdas_complementario->unidades_ejecutoras();
        $sum=0;
        $tabla ='';
        $tabla.='<table '.$tb.'>';
            $tabla.='<thead>';
            $tabla.='<tr class="modo1">';
              $tabla.='<th style="width:15%;">UNIDAD EJECUTORA</td>';
              $tabla.='<th style="width:15%;">PROYECTO-PROGRAMA.OPERACI&Oacute;N</th>';
              $tabla.='<th style="width:10%;">COSTO TOTAL DE PROYECTO</th>';
              $tabla.='<th style="width:10%;">COSTO TOTAL GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">PRESUPUESTO INICIAL '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">PRESUPUESTO MODIFICADO '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">PRESUPUESTO VIGENTE '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">EJEC. FINANCIERA DE GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">% EJEC. FINANCIERA '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">% EJEC. FINANCIERA DE PROYECTO </th>';
              $tabla.='<th style="width:10%;">% EJEC. F&Iacute;SICA ACUMULADO </th>';
              $tabla.='<th style="width:10%;">ESTADO</th>';
            $tabla.='</tr>';
            $tabla.='</thead>';
            $tabla.='<tbody>';
        foreach($unidades as $rowue)
        {
          $programas = $this->model_uejec->lista_programas_detalle($this->gestion,$this->mes,$tue_id,$rowue['uni_id']);
          if(count($programas)!=0){
          //  echo "UNI ID ".$rowue['uni_id']." UNIDAD EJECUTORA : ".$rowue['uni_unidad']."<br>";
          //  echo "NRO : ".count($programas)."<br>";
            foreach($programas as $rowp)
            {
              $ejec=0;
              if($rowp['pe_pv']!=0){
                $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
              }

              $se=$rowp['pe_pe'];
              $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
              if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                $se=$se+$suma[0]['total_ejecutado'];
              }

              $suma_prog=$this->mdas_complementario->costo_total_programado($rowp['pfec_id']);

              $tabla.='<tr class="modo1 derecha">';
                $tabla.='<td class="izquierda">'.$rowue['uni_unidad'].'</td>';
                $tabla.='<td class="izquierda">'.$rowp['proy_nombre'].'</td>';
                $tabla.='<td>'.number_format($suma_prog[0]['total_programado'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pfecg_ppto_total'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pi'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pm'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pv'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pe'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.$ejec.'%</td>';
                $efin=0;
                if($suma_prog[0]['total_programado']!=0){
                  $efin=round((($se/$suma_prog[0]['total_programado'])*100),2);
                }
                $tabla.='<td>'.$efin.'%</td>';

                $efis=$this->avance_fisico($rowp['proy_id'],$this->mes);
                $tabla.='<td bgcolor="#cbe0cb">'.round($efis,2).'%</td>';
                $estado=$this->mdas_complementario->estado_operacion($rowp['pfec_id'],$this->mes,$this->gestion);
                $estado_proy='-----';
                if(count($estado)!=0){
                  $estado_proy=$estado[0]['ep_descripcion'];
                }
                $tabla.='<td class="izquierda">'.$estado_proy.'</td>';
              $tabla.='</tr>';

            }
          }
        }
        $tabla.='</tbody>';
        $tabla.='</table><br>';
        
        /*echo "<br><br>";
        echo "SUMA TOTAL PI : ".$sum."";*/

        return $tabla;
    }
  /*======================== REPORTE TABLA DETALLES ==========================*/
   public function reporte_detalle_unidad_ejecutora($tue_id)
   {

    $mess=$this->get_mes($this->session->userdata("mes"));
    $mes = $mess[1];
    $dias = $mess[2];

    $tp_ejecucion = $this->model_uejec->get_tp_unidad_ejec($tue_id);
    $detalle=$this->tabla_detalle($tue_id,2);

    $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                      <td width=20%;>
                          <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                      </td>
                      <td width=60%; class="titulo_pdf">
                          <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                          <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                          <b>'.$this->session->userdata('sistema').'</b><br>
                          <b>REPORTE : </b> DETALLE DE EJECUCI&Oacute;N PRESUPUESTARIA GENERAL POR UNIDAD EJECUTORA / '.$tp_ejecucion[0]['tue_unidad'].'<br>
                          (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                      </td>
                      <td width=20%;>
                          <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                      </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$detalle.'
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_detalle_unidad_ejecutora.pdf", array("Attachment" => false));
   }

  /*============================ MATRIZ POR UNIDADES EJECUTORAS =======================================*/
    public function get_matriz_ue()
    {
        $tp_uejectura = $this->model_uejec->tp_unidad_ejec();
        $i=1;

        $sum_total_pi=0;
        $sum_total_pm=0;
        $sum_total_pv=0;
        $sum_total_pe=0;
       // echo "MESSSSS" .$this->mes;
        foreach($tp_uejectura as $rowue)
        {
           // echo "EJEC : ".$rowue['tue_unidad'].'<br>';
            $lista_aper_padres = $this->mapertura_programatica->lista_aperturas_padres();
            $suma_pi=0;$suma_pm=0;$suma_pv=0;$suma_pe=0;
            foreach($lista_aper_padres as $rowap)
            {  
              $lista_aper_hijas = $this->model_uejec->programas_hijos($rowap['aper_programa'],$rowap['aper_gestion'],$this->mes,$rowue['tue_id']);
              foreach($lista_aper_hijas as $row2)
              {
                $suma_pi=$suma_pi+$row2['pe_pi'];
                $suma_pm=$suma_pm+$row2['pe_pm'];
                $suma_pv=$suma_pv+$row2['pe_pv'];
                $suma_pe=$suma_pe+$row2['pe_pe'];

              }
            }
            $montos[$i][1]=$rowue['tue_id']; //// id
            $montos[$i][2]=$rowue['tue_unidad']; /// unidad
            $montos[$i][3]=$suma_pi; /// pi 
            $montos[$i][4]=$suma_pm; /// pm
            $montos[$i][5]=$suma_pv; /// pv 
            $montos[$i][6]=$suma_pe; /// pe
            if($suma_pv!=0){
              $ejec=round((($suma_pe/$suma_pv)*100),2);
            }
            else{
              $ejec=0;
            }
            $montos[$i][7]=$ejec; /// pe

            if($rowue['tue_id']!=0)
            {
              $sum_total_pi=$sum_total_pi+$montos[$i][3];
              $sum_total_pm=$sum_total_pm+$montos[$i][4];
              $sum_total_pv=$sum_total_pv+$montos[$i][5];
              $sum_total_pe=$sum_total_pe+$montos[$i][6];
            }
            $i++;
        }
        $ejecucion=0;
        if($sum_total_pv!=0){
          $ejecucion=round((($sum_total_pe/$sum_total_pv)*100),2);
        }
/*              
echo "<br>=====================<br>";
        for ($p=1; $p <= 5; $p++) { 
           for ($k=1; $k <=7; $k++) { 
              echo "[".$montos[$p][$k]."]";
            }
            echo "<br><br>";
        }
echo "<br>---------------------<br>";
echo "suma PI: " .$sum_total_pi.'- PM: '.$sum_total_pm.'- PV: '.$sum_total_pv.'-PE : '.$sum_total_pe.'-E : '.$ejecucion.'<br>';
*/
        $cont=2;
        for ($i=1; $i <=5 ; $i++) { 
            
            if($i==2)
            {
              $mue[$i][1]=0; //// tp id
              $mue[$i][2]='ORGANO EJECUTIVO'; //// Descripcion
              $mue[$i][3]=$sum_total_pi-$montos[2][3]; //// pi
              $mue[$i][4]=$sum_total_pm-$montos[2][4]; //// pm
              $mue[$i][5]=$sum_total_pv-$montos[2][5]; //// pv
              $mue[$i][6]=$sum_total_pe-$montos[2][6]; //// pe
              $muee=0;
              if($mue[$i][5]!=0){
               $muee=round((($mue[$i][6]/$mue[$i][5])*100),2);
              }
              $mue[$i][7]=$muee; //// e
            }
            else
            {
              $mue[$i][1]=$montos[$cont][1]; //// tp id
              $mue[$i][2]=$montos[$cont][2]; //// Descripcion
              $mue[$i][3]=$montos[$cont][3]; //// pi
              $mue[$i][4]=$montos[$cont][4]; //// pm
              $mue[$i][5]=$montos[$cont][5]; //// pv
              $mue[$i][6]=$montos[$cont][6]; //// pe
              $mue[$i][7]=$montos[$cont][7]; //// e
              $cont++;
            }
        }

          $mue[6][1]=0; //// tp id
          $mue[6][2]='TOTAL'; //// Descripcion
          $mue[6][3]=$sum_total_pi; //// pi
          $mue[6][4]=$sum_total_pm; //// pm
          $mue[6][5]=$sum_total_pv; //// pv
          $mue[6][6]=$sum_total_pe; //// pe
          $mue[6][7]=$ejecucion; //// e

      return $mue;
    }

    /*======================= GENERA TABLA POR UNIDADES EJECUTORAS ========================*/
    public function genera_tabla_tue($tp)
    {
       $tp_uejectura = $this->model_uejec->tp_unidad_ejec();
        $i=1;

        $sum_total_pi=0;
        $sum_total_pm=0;
        $sum_total_pv=0;
        $sum_total_pe=0;
       // echo "MESSSSS" .$this->mes;
        foreach($tp_uejectura as $rowue)
        {
           // echo "EJEC : ".$rowue['tue_unidad'].'<br>';
            $lista_aper_padres = $this->mapertura_programatica->lista_aperturas_padres();
            $suma_pi=0;$suma_pm=0;$suma_pv=0;$suma_pe=0;
            foreach($lista_aper_padres as $rowap)
            {  
              $lista_aper_hijas = $this->model_uejec->programas_hijos($rowap['aper_programa'],$rowap['aper_gestion'],$this->mes,$rowue['tue_id']);
              foreach($lista_aper_hijas as $row2)
              {
                $suma_pi=$suma_pi+$row2['pe_pi'];
                $suma_pm=$suma_pm+$row2['pe_pm'];
                $suma_pv=$suma_pv+$row2['pe_pv'];
                $suma_pe=$suma_pe+$row2['pe_pe'];

              }
            }
            $montos[$i][1]=$rowue['tue_id']; //// id
            $montos[$i][2]=$rowue['tue_unidad']; /// unidad
            $montos[$i][3]=$suma_pi; /// pi 
            $montos[$i][4]=$suma_pm; /// pm
            $montos[$i][5]=$suma_pv; /// pv 
            $montos[$i][6]=$suma_pe; /// pe
            if($suma_pv!=0){
              $ejec=round((($suma_pe/$suma_pv)*100),2);
            }
            else{
              $ejec=0;
            }
            $montos[$i][7]=$ejec; /// pe

            if($rowue['tue_id']!=0)
            {
              $sum_total_pi=$sum_total_pi+$montos[$i][3];
              $sum_total_pm=$sum_total_pm+$montos[$i][4];
              $sum_total_pv=$sum_total_pv+$montos[$i][5];
              $sum_total_pe=$sum_total_pe+$montos[$i][6];
            }
            $i++;
        }
        $ejecucion=0;
        if($sum_total_pv!=0){
          $ejecucion=round((($sum_total_pe/$sum_total_pv)*100),2);
        }
 
        $tabla = '';
       // $tabla .= '<table border=1>';
        $cont=2;
        for ($i=1; $i <=5 ; $i++) { 
            
            if($i==2)
            {
              $tabla .= '<tr bgcolor="#e5e8ec" class="modo1">';
              $tabla .= '<td>ORGANO EJECUTIVO</td>';
              $tabla .= '<td>'.number_format(($sum_total_pi-$montos[2][3]), 2, ',', '.').'</td>';
              $tabla .= '<td>'.number_format(($sum_total_pm-$montos[2][4]), 2, ',', '.').'</td>';
              $tabla .= '<td>'.number_format(($sum_total_pv-$montos[2][5]), 2, ',', '.').'</td>';
              $tabla .= '<td>'.number_format(($sum_total_pe-$montos[2][6]), 2, ',', '.').'</td>';
              $muee=0;
              if(($sum_total_pv-$montos[2][5])!=0){$muee=round(((($sum_total_pe-$montos[2][6])/($sum_total_pv-$montos[2][5]))*100),2);}
              $tabla .= '<td>'.($muee).'%</td>';
              $tabla .= '</tr>';
            }
            else
            {
              $tabla .= '<tr class="modo1">';
              if($tp==1){
                $tabla .= '<td><a href="' . site_url("") . '/rep/ejec/det_unidad_ejec/'.$montos[$cont][1].'" title="DETALLE : '.$montos[$cont][2].'">'.$montos[$cont][2].'</a></td>';
              }
              else{
                $tabla .= '<td>'.$montos[$cont][2].'</a></td>';
              }
              $tabla .= '<td>'.number_format($montos[$cont][3], 2, ',', '.').'</td>';
              $tabla .= '<td>'.number_format($montos[$cont][4], 2, ',', '.').'</td>';
              $tabla .= '<td>'.number_format($montos[$cont][5], 2, ',', '.').'</td>';
              $tabla .= '<td>'.number_format($montos[$cont][6], 2, ',', '.').'</td>';
              $tabla .= '<td>'.$montos[$cont][7].'%</td>';
              $tabla .= '</tr>';
              $cont++;
            }
        }

            $tabla .= '<tr bgcolor="#f7f7f7" class="modo1">';
            $tabla .= '<th >TOTAL</th>';
            $tabla .= '<th >'.number_format($sum_total_pi, 2, ',', '.').'</th>';
            $tabla .= '<th>'.number_format($sum_total_pm, 2, ',', '.').'</th>';
            $tabla .= '<th>'.number_format($sum_total_pv, 2, ',', '.').'</th>';
            $tabla .= '<th>'.number_format($sum_total_pe, 2, ',', '.').'</th>';
            $tabla .= '<th>'.$ejecucion.'%</th>';
            $tabla .= '</tr>';

    //  $tabla .= '</table>';
      return $tabla;
    }


  /*======================= EJECUCION FISICA DEL PROYECTO AL MES ACTUAL =============================*/
  public function avance_fisico($proy_id,$id_mes)
  {  
      $fase = $this->model_faseetapa->get_id_fase($proy_id);
      $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
      $meses=$años*12;

      $ms[0]='programado_total';$ms[1]='enero';$ms[2]='febrero';$ms[3]='marzo';$ms[4]='abril';$ms[5]='mayo';$ms[6]='junio';
      $ms[7]='julio';$ms[8]='agosto';$ms[9]='septiembre';$ms[10]='octubre';$ms[11]='noviembre';$ms[12]='diciembre';

      $componentes = $this->model_componente->componentes_id($fase[0]['id']);
      for($p=1;$p<=$meses;$p++){$cp[1][$p]=0; $cp[2][$p]=0; $cp[3][$p]=0;}
      foreach ($componentes as $rowc)
      {
        $productos = $this->model_producto->list_prod($rowc['com_id']);
        for($p=1;$p<=$meses;$p++){$pp[1][$p]=0; $pp[2][$p]=0;}
       // echo "COMPONENTE : ".$rowc['com_componente']."<br>";
        foreach ($productos as $rowp)
        {
            $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
          //  echo "PRODUCTO : ".$rowp['prod_producto']."<br>";
            for($p=1;$p<=$meses;$p++){$ap[1][$p]=0; $ap[2][$p]=0;}
            foreach ($actividad as $rowa)
            {   
              $variablep=1;$variablee=1; $v=12; $nro=1; $sump=0;$sume=0;
              for($p=1;$p<=$meses;$p++){$a[1][$p]=0; $a[2][$p]=0;$a[3][$p]=0;$a[4][$p]=0;$a[5][$p]=0;$a[6][$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
              for($i=$fase[0]['pfec_fecha_inicio'];$i<=$fase[0]['pfec_fecha_fin'];$i++)
              {
                $aprog = $this->model_actividad->actividad_programado($rowa['act_id'],$i);
                if(count($aprog)!=0){
                  for ($j=1; $j <=12 ; $j++) { 
                    $a[1][$variablep]=$aprog[0][$ms[$j]];
                    $variablep++;
                  }
                }
                else{
                  $variablep=($v*$nro)+1;
                }

                $aejec = $this->model_actividad->actividad_ejecutado($rowa['act_id'],$i);
                if(count($aejec)!=0){
                  for ($j=1; $j <=12 ; $j++) { 
                    $a[4][$variablee]=$aejec[0][$ms[$j]];
                    $variablee++;
                  }
                }
                else{
                  $variablee=($v*$nro)+1;
                }

                $nro++;
              }

              for ($i=1; $i <=$meses ; $i++) { 
                $sump=$sump+$a[1][$i];
                $a[2][$i]=$sump+$rowa['act_linea_base'];
                
                if($rowa['act_meta']!=0)
                {
                  $a[3][$i]=round(((($sump+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                }
                $ap[1][$i]=$ap[1][$i]+(($a[3][$i]*$rowa['act_ponderacion'])/100);

                $sume=$sume+$a[4][$i];
                $a[5][$i]=$sume+$rowa['act_linea_base'];

                if($rowa['act_meta']!=0)
                {
                  $a[6][$i]=round(((($sume+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                }
                $ap[2][$i]=$ap[2][$i]+(($a[6][$i]*$rowa['act_ponderacion'])/100);
              }

            }

              for ($i=1; $i <=$meses ; $i++) {
                $pp[1][$i]=$pp[1][$i]+(($ap[1][$i]*$rowp['prod_ponderacion'])/100);
                $pp[2][$i]=$pp[2][$i]+(($ap[2][$i]*$rowp['prod_ponderacion'])/100);
              } 
              
        }
          for ($i=1; $i <=$meses ; $i++) {
                $cp[1][$i]=$cp[1][$i]+(($pp[1][$i]*$rowc['com_ponderacion'])/100);
                $cp[2][$i]=$cp[2][$i]+(($pp[2][$i]*$rowc['com_ponderacion'])/100);
              }

      }

      for ($i=1; $i <=$meses ; $i++) {
        if($cp[1][$i]!=0){
          $cp[3][$i]=round((($cp[2][$i]/$cp[1][$i])*100),1);
        }
      }

      for ($i=1; $i <=12 ; $i++) { 
        $tefic[1][$i]=0;$tefic[2][$i]=0;$tefic[3][$i]=0;
      }
      $i=1;$vmeses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;
      for($p=$fase[0]['pfec_fecha_inicio'];$p<=$fase[0]['pfec_fecha_fin'];$p++)
      {
          $variable=0;
          for($pos=$i;$pos<=$vmeses;$pos++)
          {
            $variable++;
            $tefic[1][$variable]=$cp[1][$pos];
            $tefic[2][$variable]=$cp[2][$pos];
            $tefic[3][$variable]=$cp[3][$pos];
          }
          if($p==$this->session->userdata('gestion'))
          {
              $ejecucion=$tefic[2][$id_mes];
          }

            $i=$vmeses+1;
            $vmeses=$vmeses+12;
            $gestion++; $nro++;
      }

      return $ejecucion;
  }
  /*=================================================================================================*/
    public function get_mes($mes_id)
    {
      $mes[1]='Enero';
      $mes[2]='Febrero';
      $mes[3]='Marzo';
      $mes[4]='Abril';
      $mes[5]='Mayo';
      $mes[6]='Junio';
      $mes[7]='Julio';
      $mes[8]='Agosto';
      $mes[9]='Septiembre';
      $mes[10]='Octubre';
      $mes[11]='Noviembre';
      $mes[12]='Diciembre';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }

private $estilo_vertical = 
    '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }

        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
      table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        .mv{font-size:10px;}
        .siipp{width:120px;}

      .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .tabla {
      font-family: Verdana, Arial, Helvetica, sans-serif;
      font-size: 7px;
                  width: 100%;

      }
              .tabla th {
      padding: 5px;
      font-size: 7px;
      background-color: #83aec0;
      background-image: url(fondo_th.png);
      background-repeat: repeat-x;
      color: #FFFFFF;
      border-right-width: 1px;
      border-bottom-width: 1px;
      border-right-style: solid;
      border-bottom-style: solid;
      border-right-color: #558FA6;
      border-bottom-color: #558FA6;
      font-family: "Trebuchet MS", Arial;
      text-transform: uppercase;
      }
      .tabla .modo1 {
      font-size: 7px;
      font-weight:bold;

      background-image: url(fondo_tr01.png);
      background-repeat: repeat-x;
      color: #34484E;
      font-family: "Trebuchet MS", Arial;
      }
      .tabla .modo1 td {
      padding: 5px;
      border-right-width: 1px;
      border-bottom-width: 1px;
      border-right-style: solid;
      border-bottom-style: solid;
      border-right-color: #A4C4D0;
      border-bottom-color: #A4C4D0;

      }
    </style>';

}