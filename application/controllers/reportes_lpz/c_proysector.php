<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class c_proysector extends CI_Controller { 
    var $gestion;
    var $mes;
    var $rol;
    var $fun_id;

  public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf');
        $this->load->library('pdf2');

        $this->load->model('reportes_tarija_das/mdas_complementario');

        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_producto');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/model_actividad');

        $this->load->model('mantenimiento/mapertura_programatica');
        $this->load->model('mantenimiento/munidad_organizacional');
        $this->load->model('reportes_tarija/model_uejec');
        $this->load->model('reportes_tarija/model_codsec');
        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);

        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
        $this->rol = $this->session->userData('rol');
        $this->fun_id = $this->session->userData('fun_id');

        }else{
            redirect('/','refresh');
        }
    }
   
    public function pinversion_unidad_ejecutora()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['mue']=$this->get_matriz_ue_pinv();
        $data['tabla']=$this->genera_tabla_tue_pinv();

        $this->load->view('admin/reportes/ejecucion_presupuestaria/proy_sectores/u_ejecutora_pinv', $data);   
    }

    /*============== reporte Unidades Ejecutoras ==================*/
    public function reporte_pinversion_unidad_ejecutora()
    { 
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['mue']=$this->get_matriz_ue_pinv();
        $data['tabla']=$this->genera_tabla_tue_pinv();
    
        $this->load->view('admin/reportes/ejecucion_presupuestaria/proy_sectores/u_ejecutora_pinv_imprimir', $data);
    }

    public function detalle_pinversion_unidad_ejecutora($tue_id)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['tue_id'] = $tue_id;
        $data['tp_ejecucion'] = $this->model_uejec->get_tp_unidad_ejec($tue_id);

        $data['atras']= '' . base_url() . 'index.php/rep/ejec/unidad_ejec_proy';

        $data['detalle']=$this->tabla_detalle_pinversion_unidad_ejecutora($tue_id,1);
        $this->load->view('admin/reportes/ejecucion_presupuestaria/proy_sectores/u_ejecutora_pinv_detalle', $data);
    }
    /*------------------ Detalle por unidad ejecutoras ---------------------*/
    public function tabla_detalle_pinversion_unidad_ejecutora($tue_id,$tp)
    {
        if($tp==1){
        $tb='id="dt_basic" class="table table table-bordered" width="100%"';
        }
        elseif ($tp==2) {
          $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        }

        $unidades = $this->mdas_complementario->unidades_ejecutoras();
        $sum=0;
        $tabla ='';
        $tabla.='<table '.$tb.'>';
            $tabla.='<thead>';
            $tabla.='<tr class="modo1">';
              $tabla.='<th style="width:15%;">UNIDAD EJECUTORA</td>';
              $tabla.='<th style="width:15%;">PROYECTO-PROGRAMA.OPERACI&Oacute;N</th>';
              $tabla.='<th style="width:10%;">COSTO TOTAL DE PROYECTO</th>';
              $tabla.='<th style="width:10%;">COSTO TOTAL GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">PRESUPUESTO INICIAL '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">PRESUPUESTO MODIFICADO '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">PRESUPUESTO VIGENTE '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">EJEC. FINANCIERA DE GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">% EJEC. FINANCIERA '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">% EJEC. FINANCIERA DE PROYECTO </th>';
              $tabla.='<th style="width:10%;">% EJEC. F&Iacute;SICA ACUMULADA </th>';
              $tabla.='<th style="width:10%;">ESTADO</th>';
            $tabla.='</tr>';
            $tabla.='</thead>';
            $tabla.='<tbody>';
        foreach($unidades as $rowue)
        {
          $programas = $this->model_uejec->programas_hijos_inversionp_detalle($this->gestion,$this->mes,$tue_id,$rowue['uni_id']);
          if(count($programas)!=0){
          //  echo "UNI ID ".$rowue['uni_id']." UNIDAD EJECUTORA : ".$rowue['uni_unidad']."<br>";
          //  echo "NRO : ".count($programas)."<br>";
            foreach($programas as $rowp)
            {
              $ejec=0;
              if($rowp['pe_pv']!=0){
                $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
              }

              $se=$rowp['pe_pe'];
              $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
              if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                $se=$se+$suma[0]['total_ejecutado'];
              }

              $suma_prog=$this->mdas_complementario->costo_total_programado($rowp['pfec_id']);

              $tabla.='<tr class="modo1 derecha">';
                $tabla.='<td class="izquierda">'.$rowue['uni_unidad'].'</td>';
                $tabla.='<td class="izquierda">'.$rowp['proy_nombre'].'</td>';
                $tabla.='<td>'.number_format($suma_prog[0]['total_programado'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pfecg_ppto_total'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pi'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pm'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pv'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pe'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.$ejec.'%</td>';
                $efin=0;
                if($suma_prog[0]['total_programado']!=0){
                  $efin=round((($se/$suma_prog[0]['total_programado'])*100),2);
                }
                $tabla.='<td>'.$efin.'%</td>';

                $efis=$this->avance_fisico($rowp['proy_id'],$this->mes);
                $tabla.='<td bgcolor="#cbe0cb">'.round($efis,2).'%</td>';
                $estado=$this->mdas_complementario->estado_operacion($rowp['pfec_id'],$this->mes,$this->gestion);
                $estado_proy='-----';
                if(count($estado)!=0){
                  $estado_proy=$estado[0]['ep_descripcion'];
                }
                $tabla.='<td class="izquierda">'.$estado_proy.'</td>';
              $tabla.='</tr>';

            }
          } 
        }

        $tabla.='</tbody>';
        $tabla.='</table>';
        /*echo "<br><br>";
        echo "SUMA TOTAL PI : ".$sum."";*/
        return $tabla;
    }

    /*==================== MATRIZ POR UNIDADES EJECUTORAS PROYECTOS DE INVERSION ==============*/
    public function get_matriz_ue_pinv()
    {
        $tp_uejectura = $this->model_uejec->tp_unidad_ejec2();
        $i=1;

        $sum_total_pi=0;
        $sum_total_pm=0;
        $sum_total_pv=0;
        $sum_total_pe=0;
       // echo "MESSSSS" .$this->mes;
        foreach($tp_uejectura as $rowue)
        {
         // echo "UE : ".$rowue['tue_unidad'].'<br>';
            $lista_aper_padres = $this->mapertura_programatica->lista_aperturas_padres();
            $suma_pi=0;$suma_pm=0;$suma_pv=0;$suma_pe=0;
            foreach($lista_aper_padres as $rowap)
            {  
              $lista_aper_hijas = $this->model_uejec->programas_hijos_inversionp($rowap['aper_programa'],$rowap['aper_gestion'],$this->mes,$rowue['tue_id']);
              foreach($lista_aper_hijas as $row2)
              {
                $suma_pi=$suma_pi+$row2['pe_pi'];
                $suma_pm=$suma_pm+$row2['pe_pm'];
                $suma_pv=$suma_pv+$row2['pe_pv'];
                $suma_pe=$suma_pe+$row2['pe_pe'];

              }
            }
            $montos[$i][1]=$rowue['tue_id']; //// id
            $montos[$i][2]=$rowue['tue_unidad']; /// unidad
            $montos[$i][3]=$suma_pi; /// pi 
            $montos[$i][4]=$suma_pm; /// pm
            $montos[$i][5]=$suma_pv; /// pv 
            $montos[$i][6]=$suma_pe; /// pe


            $sum_total_pi=$sum_total_pi+$montos[$i][3];
            $sum_total_pm=$sum_total_pm+$montos[$i][4];
            $sum_total_pv=$sum_total_pv+$montos[$i][5];
            $sum_total_pe=$sum_total_pe+$montos[$i][6];
            
            $i++;
        }

          $montos[4][1]=0; //// id
          $montos[4][2]='TOTAL'; /// unidad
          $montos[4][3]=$sum_total_pi; /// pi 
          $montos[4][4]=$sum_total_pm; /// pm
          $montos[4][5]=$sum_total_pv; /// pv 
          $montos[4][6]=$sum_total_pe; /// pe

      return $montos;
    }

    /*==================== GENERA TABLA POR UNIDADES EJECUTORAS PROYECTOS DE INVERSION ================*/
    public function genera_tabla_tue_pinv()
    {
        $tp_uejectura = $this->model_uejec->tp_unidad_ejec2();
        $i=1;

        $sum_total_pi=0;
        $sum_total_pm=0;
        $sum_total_pv=0;
        $sum_total_pe=0;
       // echo "MESSSSS" .$this->mes;
        $tabla = '';
       // $tabla .= '<table border=1>';
        foreach($tp_uejectura as $rowue)
        {
         // echo "UE : ".$rowue['tue_unidad'].'<br>';
            $lista_aper_padres = $this->mapertura_programatica->lista_aperturas_padres();
            $suma_pi=0;$suma_pm=0;$suma_pv=0;$suma_pe=0;
            foreach($lista_aper_padres as $rowap)
            {  
              $lista_aper_hijas = $this->model_uejec->programas_hijos_inversionp($rowap['aper_programa'],$rowap['aper_gestion'],$this->mes,$rowue['tue_id']);
              foreach($lista_aper_hijas as $row2)
              {
                $suma_pi=$suma_pi+$row2['pe_pi'];
                $suma_pm=$suma_pm+$row2['pe_pm'];
                $suma_pv=$suma_pv+$row2['pe_pv'];
                $suma_pe=$suma_pe+$row2['pe_pe'];

              }
            }

              $tabla .= '<tr class="even_row">';
              $tabla .= '<td><a href="' . site_url("") . '/rep/ejec/det_unidad_ejec_proy/'.$rowue['tue_id'].'" title="DETALLE : '.$rowue['tue_unidad'].'">'.$rowue['tue_unidad'].'</a></td>';
              $tabla .= '<td>'.number_format($suma_pi, 2, ',', '.').'</td>';
              $tabla .= '<td>'.number_format($suma_pm, 2, ',', '.').'</td>';
              $tabla .= '<td>'.number_format($suma_pv, 2, ',', '.').'</td>';
              $tabla .= '<td>'.number_format($suma_pe, 2, ',', '.').'</td>';
              $ejec=0;
              if($suma_pv!=0){
                $ejec=round((($suma_pe/$suma_pv)*100),2);
              }
              $tabla .= '<td>'.$ejec.'%</td>';
              $tabla .= '</tr>';


            $sum_total_pi=$sum_total_pi+$suma_pi;
            $sum_total_pm=$sum_total_pm+$suma_pm;
            $sum_total_pv=$sum_total_pv+$suma_pv;
            $sum_total_pe=$sum_total_pe+$suma_pe;
            
            $i++;
        }

            $tabla .= '<tr bgcolor="#e5e8ec">';
            $tabla .= '<td>TOTAL</td>';
            $tabla .= '<td>'.number_format($sum_total_pi, 2, ',', '.').'</td>';
            $tabla .= '<td>'.number_format($sum_total_pm, 2, ',', '.').'</td>';
            $tabla .= '<td>'.number_format($sum_total_pv, 2, ',', '.').'</td>';
            $tabla .= '<td>'.number_format($sum_total_pe, 2, ',', '.').'</td>';
            $tejec=0;
            if($sum_total_pv!=0){
              $tejec=round((($sum_total_pe/$sum_total_pv)*100),2);
            }
            $tabla .= '<td>'.$tejec.'%</td>';

            $tabla .= '</tr>';
        //  $tabla .= '</table>';

      return $tabla;
    }

    /*============ REPORTE TABLA DETALLES POR UNIDADES EJECUTORAS PROYECTOS DE INVERSION ============*/
   public function reporte_detalle_pinversion_unidad_ejecutora($tue_id)
   {
    $mess=$this->get_mes($this->session->userdata("mes"));
    $mes = $mess[1];
    $dias = $mess[2];

    $tp_ejecucion = $this->model_uejec->get_tp_unidad_ejec($tue_id);
    $detalle=$this->tabla_detalle_pinversion_unidad_ejecutora($tue_id,2);

    $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="assets/img/logoTarija.JPG" width="120px"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> RESUMEN DE EJECUCI&Oacute;N PRESPUESTARIA DE INVERSI&Oacute;N P&Uacute;BLICA POR UNIDAD EJECUTORA / '.$tp_ejecucion[0]['tue_unidad'].'<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <img src="assets/img/ltarija2.JPG" alt="" width="150px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$detalle.'
                  </table>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_detalle_pinversion_unidad_ejecutora.pdf", array("Attachment" => false));
   }

    /*============================ PROYECTOS DE INVERSION POR SECTOR ECONOMICO =========================*/
    public function pi_sector_economico()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['nro']=count($this->model_codsec->list_sectores())+1;
        $data['sec']=$this->get_matriz_codsec_proy();

        $data['tabla']=$this->genera_tabla($data['sec'],$data['nro'],1);
        $this->load->view('admin/reportes/ejecucion_presupuestaria/proy_sectores/codsec_pinv', $data);   
    }

    /*============== reporte Sector Economico Proyectos de Inversion ==================*/
    public function reporte_pi_sector_economico_grafico()
    { 
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['nro']=count($this->model_codsec->list_sectores())+1;
        $data['sec']=$this->get_matriz_codsec_proy();

        $data['tabla']=$this->genera_tabla($data['sec'],$data['nro'],1);
    
        $this->load->view('admin/reportes/ejecucion_presupuestaria/proy_sectores/codsec_pinv_imprimir', $data); 
    }

  /*======================== REPORTE TABLA DETALLES ==========================*/
   public function reporte_pi_sector_economico()
   {
    $mess=$this->get_mes($this->session->userdata("mes"));
    $mes = $mess[1];
    $dias = $mess[2];

    $data['nro']=count($this->model_codsec->list_sectores())+1;
    $data['sec']=$this->get_matriz_codsec_proy();

    $tabla=$this->genera_tabla($data['sec'],$data['nro'],2);

    $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                      <td width=20%;>
                          <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                      </td>
                      <td width=60%; class="titulo_pdf">
                          <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                          <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                          <b>'.$this->session->userdata('sistema').'</b><br>
                          <b>REPORTE : </b> DISTRIBUCI&Oacute;N PRESUPUESTARIA DE INVERSI&Oacute;N P&Uacute;BLICA POR SECTORES ECONOMICOS <br>
                          (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                      </td>
                      <td width=20%;>
                          <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                      </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                 <div>
                  <table border="0" cellpadding="0" cellspacing="0" class="tabla">
                    <thead>
                        <tr bgcolor="#e5e8ec" class="modo1">
                        <th>C&Oacute;DIGO</th>
                        <th>SECTOR</th>
                        <th>PRESUPUESTO INICIAL</th>
                        <th>MODIFICACIONES</th>
                        <th>PRESUPUESTO VIGENTE</th>
                        <th>%PART.</th>
                        <th>EJECUCI&Oacute;N</th>
                        <th>%EJECUCI&Oacute;N</th>
                        <th>%PART.</th>
                      </tr>
                    </thead>
                    <tbody>
                      '.$tabla.'
                    </tbody>
                  </table>
                </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_pi_sector_economico.pdf", array("Attachment" => false));
   }

    /*---------------- Detalle pi sector Economico ---------------*/
    public function detalle_pi_sector_economico($codsectorial)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['codsectorial'] = $codsectorial;
        $data['tp_sectorial'] = $this->model_codsec->get_codigo_sectorial($codsectorial);
        $data['atras']= '' . base_url() . 'index.php/rep/ejec/sector_proy';

        $data['detalle']=$this->tabla_detalle_pi_sector_economico($codsectorial,1);
        $this->load->view('admin/reportes/ejecucion_presupuestaria/proy_sectores/codsec_pinv_detalle', $data);
    }

    public function tabla_detalle_pi_sector_economico($codsectorial,$tp)
    {
        if($tp==1){
        $tb='id="dt_basic" class="table table table-bordered" width="100%"';
        }
        elseif ($tp==2) {
          $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        }

        $unidades = $this->mdas_complementario->unidades_ejecutoras();
        $sum=0;
        $tabla ='';
        $tabla.='<table '.$tb.'>';
            $tabla.='<thead>';
            $tabla.='<tr class="modo1">';
              $tabla.='<th style="width:15%;">UNIDAD EJECUTORA</td>';
              $tabla.='<th style="width:15%;">PROYECTO-PROGRAMA.OPERACI&Oacute;N</th>';
              $tabla.='<th style="width:10%;">COSTO TOTAL DE PROYECTO</th>';
              $tabla.='<th style="width:10%;">COSTO TOTAL GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">PRESUPUESTO INICIAL '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">PRESUPUESTO MODIFICADO '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">PRESUPUESTO VIGENTE '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">EJEC. FINANCIERA DE GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">% EJEC. FINANCIERA '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">% EJEC. FINANCIERA DE PROYECTO </th>';
              $tabla.='<th style="width:10%;">% EJEC. F&Iacute;SICA DE PROYECTO </th>';
              $tabla.='<th style="width:10%;">ESTADO</th>';
            $tabla.='</tr>';
            $tabla.='</thead>';
            $tabla.='<tbody>';
        foreach($unidades as $rowue)
        {
          $programas = $this->model_codsec->programas_hijos_inversionp_detalles($this->gestion,$this->mes,$codsectorial,$rowue['uni_id']);
          if(count($programas)!=0){
          //  echo "UNI ID ".$rowue['uni_id']." UNIDAD EJECUTORA : ".$rowue['uni_unidad']."<br>";
          //  echo "NRO : ".count($programas)."<br>";
            foreach($programas as $rowp)
            {
              $ejec=0;
              if($rowp['pe_pv']!=0){
                $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
              }

              $se=$rowp['pe_pe'];
              $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
              if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                $se=$se+$suma[0]['total_ejecutado'];
              }

              $suma_prog=$this->mdas_complementario->costo_total_programado($rowp['pfec_id']);

              $tabla.='<tr class="modo1 derecha">';
                $tabla.='<td class="izquierda">'.$rowue['uni_unidad'].'</td>';
                $tabla.='<td class="izquierda">'.$rowp['proy_nombre'].'</td>';
                $tabla.='<td>'.number_format($suma_prog[0]['total_programado'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pfecg_ppto_total'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pi'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pm'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pv'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pe'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.$ejec.'%</td>';
                $efin=0;
                if($suma_prog[0]['total_programado']!=0){
                  $efin=round((($se/$suma_prog[0]['total_programado'])*100),2);
                }
                $tabla.='<td>'.$efin.'%</td>';

                $efis=$this->avance_fisico($rowp['proy_id'],$this->mes);
                $tabla.='<td>'.round($efis,2).'%</td>';
                $estado=$this->mdas_complementario->estado_operacion($rowp['pfec_id'],$this->mes,$this->gestion);
                $estado_proy='-----';
                if(count($estado)!=0){
                  $estado_proy=$estado[0]['ep_descripcion'];
                }
                $tabla.='<td class="izquierda">'.$estado_proy.'</td>';
              $tabla.='</tr>';

            }
          }
        }

        $tabla.='</tbody>';
        $tabla.='</table><br>';
        
        /*echo "<br><br>";
        echo "SUMA TOTAL PI : ".$sum."";*/
        return $tabla;
    }

    /*============ REPORTE TABLA DETALLES POR SECTORES ECONOMICOS ============*/
   public function reporte_detalle_pi_sector_economico($codsectorial)
   {
    $mess=$this->get_mes($this->session->userdata("mes"));
    $mes = $mess[1];
    $dias = $mess[2];

    $tp_sectorial = $this->model_codsec->get_codigo_sectorial($codsectorial);
    $detalles=$this->tabla_detalle_pi_sector_economico($codsectorial,2);

    $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> DISTRIBUCI&Oacute;N PRESUPUESTARIA DE INVERSI&Oacute;N P&Uacute;BLICA POR SECTORES ECONOMICOS / '.$tp_sectorial[0]['sector'].'<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$detalles.'
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_detalle_pinversion_unidad_ejecutora.pdf", array("Attachment" => false));
   }
    /*==================== MATRIZ SECTOR ECONOMICO .PI ==============*/
    public function get_matriz_codsec_proy()
    {
        $list_codsec = $this->model_codsec->list_sectores();
        $i=1;
        $sum_total_pi=0;
        $sum_total_pm=0;
        $sum_total_pv=0;
        $sum_total_pe=0;
       // echo "MESSSSS" .$this->mes;
        foreach($list_codsec as $rowsec)
        {
         // echo "SEC : ".$rowsec['sector'].'<br>';
            $lista_aper_padres = $this->mapertura_programatica->lista_aperturas_padres();
            $suma_pi=0;$suma_pm=0;$suma_pv=0;$suma_pe=0;
            foreach($lista_aper_padres as $rowap)
            {  
              $lista_aper_hijas = $this->model_codsec->programas_hijos_inversionp($rowap['aper_programa'],$rowap['aper_gestion'],$this->mes,$rowsec['codsectorial']);
              foreach($lista_aper_hijas as $row2)
              {
                $suma_pi=$suma_pi+$row2['pe_pi'];
                $suma_pm=$suma_pm+$row2['pe_pm'];
                $suma_pv=$suma_pv+$row2['pe_pv'];
                $suma_pe=$suma_pe+$row2['pe_pe'];

              }
            }
            $montos[$i][1]=$rowsec['codsectorial']; //// codsectorial
            $montos[$i][2]=$rowsec['sector']; /// sector
            $montos[$i][3]=$suma_pi; /// pi 
            $montos[$i][4]=$suma_pm; /// pm
            $montos[$i][5]=$suma_pv; /// pv 
            $montos[$i][6]=$suma_pe; /// pe*/


            $sum_total_pi=$sum_total_pi+$montos[$i][3];
            $sum_total_pm=$sum_total_pm+$montos[$i][4];
            $sum_total_pv=$sum_total_pv+$montos[$i][5];
            $sum_total_pe=$sum_total_pe+$montos[$i][6];
          
            $i++;
        }

        $suma_part_pv=0;
        $suma_part_pe=0;
        for ($p=1; $p <= $i-1; $p++){
            $sec[$p][1]=$montos[$p][1]; /// Cod sec
            $sec[$p][2]=$montos[$p][2]; /// Sec
            $sec[$p][3]=$montos[$p][3]; /// pi
            $sec[$p][4]=$montos[$p][4]; /// pm
            $sec[$p][5]=$montos[$p][5]; /// pv
            
            $part_pv=0;
            if($sum_total_pv!=0){
                $part_pv=round((($sec[$p][5]/$sum_total_pv)*100),2);
            }
            $sec[$p][6]=$part_pv; /// % part pv
            $suma_part_pv=$suma_part_pv+$sec[$p][6];
            $sec[$p][7]=$montos[$p][6]; /// pe
            $ejecucion=0;
            $part_pe=0;
            if($sum_total_pe!=0){
                $ejecucion=round((($sec[$p][7]/$sum_total_pe)*100),2);
                $part_pe=round((($sec[$p][7]/$sum_total_pe)*100),2);
            }
            $sec[$p][8]=$ejecucion; /// % ejecucion
            $sec[$p][9]=$part_pe; /// % part ejec
            $suma_part_pe=$suma_part_pe+$sec[$p][9];
        }

        $aux_cod=0; /// Cod
        $aux_sec=0; /// Sector
        $aux_pi=0; /// pi
        $aux_pm=0; /// pm
        $aux_pv=0; /// pv
        $aux_part_pv=0; /// part pv
        $aux_pe=0; /// pe
        $aux_ejec=0; /// ejec
        $aux_part_pe=0; /// part pe

        for ($p=1; $p <=$i-1 ; $p++) { 
            for ($k=$p+1; $k <=$i-1 ; $k++) {

                if($sec[$k][9]>$sec[$p][9])
                {
                    $aux_cod=$sec[$p][1];
                    $aux_sec=$sec[$p][2];
                    $aux_pi=$sec[$p][3];
                    $aux_pm=$sec[$p][4];
                    $aux_pv=$sec[$p][5];
                    $aux_part_pv=$sec[$p][6];
                    $aux_pe=$sec[$p][7];
                    $aux_ejec=$sec[$p][8];
                    $aux_part_pe=$sec[$p][9];
                    
                // echo "aux tp ".$aux_tp."- PI".$aux_pi."- PM".$aux_pm;

                    $sec[$p][1]=$sec[$k][1];
                    $sec[$p][2]=$sec[$k][2];
                    $sec[$p][3]=$sec[$k][3];
                    $sec[$p][4]=$sec[$k][4];
                    $sec[$p][5]=$sec[$k][5];
                    $sec[$p][6]=$sec[$k][6];
                    $sec[$p][7]=$sec[$k][7];
                    $sec[$p][8]=$sec[$k][8];
                    $sec[$p][9]=$sec[$k][9];

                    $sec[$k][1]=$aux_cod;
                    $sec[$k][2]=$aux_sec;
                    $sec[$k][3]=$aux_pi;
                    $sec[$k][4]=$aux_pm;
                    $sec[$k][5]=$aux_pv;
                    $sec[$k][6]=$aux_part_pv;
                    $sec[$k][7]=$aux_pe;
                    $sec[$k][8]=$aux_ejec;
                    $sec[$k][9]=$aux_part_pe;
                }
              }
            }
            
            $sec[$i][1]='';
            $sec[$i][2]='TOTAL';
            $sec[$i][3]=$sum_total_pi;
            $sec[$i][4]=$sum_total_pm;
            $sec[$i][5]=$sum_total_pv;
            $sec[$i][6]=$suma_part_pv;
            $sec[$i][7]=$sum_total_pe;
            $ejec=0;
            if($sec[$i][5]!=0){
              $ejec=round((($sec[$i][7]/$sec[$i][5])*100),2);
            }
            $sec[$i][8]=$ejec;
            $sec[$i][9]=$suma_part_pe;

        return $sec;
    }

    public function genera_tabla($codsec,$nro,$tp)
    {
      $tabla = '';
      
      for ($p=1; $p <= $nro-1; $p++) { 
        $tabla .= '<tr class="modo1">';
        for ($k=1; $k <=9; $k++) { 
          
          if($k==3 || $k==4 || $k==5 || $k==7){
            $tabla .= '<td>'.number_format($codsec[$p][$k], 2, ',', '.').'</td>';
          }
          elseif($k==6 || $k==8 || $k==9){
            $tabla .= '<td>'.$codsec[$p][$k].'%</td>';
          }
          else{
            if($tp==1){
              $tabla .= '<td><b><a href="' . site_url("") . '/rep/ejec/detalle_sector_proy/'.$codsec[$p][1].'" title="DETALLE : '.$codsec[$p][$k].'">'.$codsec[$p][$k].'</a></b></td>';
            }
            else{
              $tabla .= '<td><b>'.$codsec[$p][$k].'</a></b></td>';
            }
            
          }
          
        }
        $tabla .= '</tr>';
      }
      $tabla .= '<tr bgcolor="#f4f4f4" class="modo1">';
        for ($k=1; $k <=9; $k++) { 
          if($k==3 || $k==4 || $k==5 || $k==7){
            $tabla .= '<td>'.number_format($codsec[$p][$k], 2, ',', '.').'</td>';
          }
          elseif($k==6 || $k==8 || $k==9){
            $tabla .= '<td>'.$codsec[$p][$k].'%</td>';
          }
          else{
            $tabla .= '<td><b>'.$codsec[$p][$k].'</b></td>';
          }
        }
        $tabla .= '</tr>';
      
      return $tabla;
    }

  /*======================= EJECUCION FISICA DEL PROYECTO AL MES ACTUAL =============================*/
  public function avance_fisico($proy_id,$id_mes)
  {  
      $fase = $this->model_faseetapa->get_id_fase($proy_id);
      $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
      $meses=$años*12;

      $ms[0]='programado_total';$ms[1]='enero';$ms[2]='febrero';$ms[3]='marzo';$ms[4]='abril';$ms[5]='mayo';$ms[6]='junio';
      $ms[7]='julio';$ms[8]='agosto';$ms[9]='septiembre';$ms[10]='octubre';$ms[11]='noviembre';$ms[12]='diciembre';

      $componentes = $this->model_componente->componentes_id($fase[0]['id']);
      for($p=1;$p<=$meses;$p++){$cp[1][$p]=0; $cp[2][$p]=0; $cp[3][$p]=0;}
      foreach ($componentes as $rowc)
      {
        $productos = $this->model_producto->list_prod($rowc['com_id']);
        for($p=1;$p<=$meses;$p++){$pp[1][$p]=0; $pp[2][$p]=0;}
       // echo "COMPONENTE : ".$rowc['com_componente']."<br>";
        foreach ($productos as $rowp)
        {
            $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
          //  echo "PRODUCTO : ".$rowp['prod_producto']."<br>";
            for($p=1;$p<=$meses;$p++){$ap[1][$p]=0; $ap[2][$p]=0;}
            foreach ($actividad as $rowa)
            {   
              $variablep=1;$variablee=1; $v=12; $nro=1; $sump=0;$sume=0;
              for($p=1;$p<=$meses;$p++){$a[1][$p]=0; $a[2][$p]=0;$a[3][$p]=0;$a[4][$p]=0;$a[5][$p]=0;$a[6][$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
              for($i=$fase[0]['pfec_fecha_inicio'];$i<=$fase[0]['pfec_fecha_fin'];$i++)
              {
                $aprog = $this->model_actividad->actividad_programado($rowa['act_id'],$i);
                if(count($aprog)!=0){
                  for ($j=1; $j <=12 ; $j++) { 
                    $a[1][$variablep]=$aprog[0][$ms[$j]];
                    $variablep++;
                  }
                }
                else{
                  $variablep=($v*$nro)+1;
                }

                $aejec = $this->model_actividad->actividad_ejecutado($rowa['act_id'],$i);
                if(count($aejec)!=0){
                  for ($j=1; $j <=12 ; $j++) { 
                    $a[4][$variablee]=$aejec[0][$ms[$j]];
                    $variablee++;
                  }
                }
                else{
                  $variablee=($v*$nro)+1;
                }

                $nro++;
              }

              for ($i=1; $i <=$meses ; $i++) { 
                $sump=$sump+$a[1][$i];
                $a[2][$i]=$sump+$rowa['act_linea_base'];
                
                if($rowa['act_meta']!=0)
                {
                  $a[3][$i]=round(((($sump+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                }
                $ap[1][$i]=$ap[1][$i]+(($a[3][$i]*$rowa['act_ponderacion'])/100);

                $sume=$sume+$a[4][$i];
                $a[5][$i]=$sume+$rowa['act_linea_base'];

                if($rowa['act_meta']!=0)
                {
                  $a[6][$i]=round(((($sume+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                }
                $ap[2][$i]=$ap[2][$i]+(($a[6][$i]*$rowa['act_ponderacion'])/100);
              }

            }

              for ($i=1; $i <=$meses ; $i++) {
                $pp[1][$i]=$pp[1][$i]+(($ap[1][$i]*$rowp['prod_ponderacion'])/100);
                $pp[2][$i]=$pp[2][$i]+(($ap[2][$i]*$rowp['prod_ponderacion'])/100);
              } 
              
        }
          for ($i=1; $i <=$meses ; $i++) {
                $cp[1][$i]=$cp[1][$i]+(($pp[1][$i]*$rowc['com_ponderacion'])/100);
                $cp[2][$i]=$cp[2][$i]+(($pp[2][$i]*$rowc['com_ponderacion'])/100);
              }

      }

      for ($i=1; $i <=$meses ; $i++) {
        if($cp[1][$i]!=0){
          $cp[3][$i]=round((($cp[2][$i]/$cp[1][$i])*100),1);
        }
      }

      for ($i=1; $i <=12 ; $i++) { 
        $tefic[1][$i]=0;$tefic[2][$i]=0;$tefic[3][$i]=0;
      }
      $i=1;$vmeses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;
      for($p=$fase[0]['pfec_fecha_inicio'];$p<=$fase[0]['pfec_fecha_fin'];$p++)
      {
          $variable=0;
          for($pos=$i;$pos<=$vmeses;$pos++)
          {
            $variable++;
            $tefic[1][$variable]=$cp[1][$pos];
            $tefic[2][$variable]=$cp[2][$pos];
            $tefic[3][$variable]=$cp[3][$pos];
          }
          if($p==$this->session->userdata('gestion'))
          {
              $ejecucion=$tefic[2][$id_mes];
          }

            $i=$vmeses+1;
            $vmeses=$vmeses+12;
            $gestion++; $nro++;
      }

      return $ejecucion;
  }
  /*=================================================================================================*/
    public function get_mes($mes_id)
    {
      $mes[1]='Enero';
      $mes[2]='Febrero';
      $mes[3]='Marzo';
      $mes[4]='Abril';
      $mes[5]='Mayo';
      $mes[6]='Junio';
      $mes[7]='Julio';
      $mes[8]='Agosto';
      $mes[9]='Septiembre';
      $mes[10]='Octubre';
      $mes[11]='Noviembre';
      $mes[12]='Diciembre';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }

private $estilo_vertical = 
    '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }

        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
      table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        .mv{font-size:10px;}
        .siipp{width:120px;}

      .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .tabla {
      font-family: Verdana, Arial, Helvetica, sans-serif;
      font-size: 7px;
                  width: 100%;

      }
              .tabla th {
      padding: 5px;
      font-size: 7px;
      background-color: #83aec0;
      background-image: url(fondo_th.png);
      background-repeat: repeat-x;
      color: #FFFFFF;
      border-right-width: 1px;
      border-bottom-width: 1px;
      border-right-style: solid;
      border-bottom-style: solid;
      border-right-color: #558FA6;
      border-bottom-color: #558FA6;
      font-family: "Trebuchet MS", Arial;
      text-transform: uppercase;
      }
      .tabla .modo1 {
      font-size: 7px;
      font-weight:bold;

      background-image: url(fondo_tr01.png);
      background-repeat: repeat-x;
      color: #34484E;
      font-family: "Trebuchet MS", Arial;
      }
      .tabla .modo1 td {
      padding: 5px;
      border-right-width: 1px;
      border-bottom-width: 1px;
      border-right-style: solid;
      border-bottom-style: solid;
      border-right-color: #A4C4D0;
      border-bottom-color: #A4C4D0;

      }
    </style>';
}