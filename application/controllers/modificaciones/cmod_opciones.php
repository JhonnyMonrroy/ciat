<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Cmod_opciones extends CI_Controller {
  public $rol = array('1' => '3','2' => '4','3' => '5');  
  public function __construct (){
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf2');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_actividad');
        $this->load->model('programacion/model_producto');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/model_mantenimiento');
        $this->load->model('ejecucion/model_ejecucion');
        $this->load->model('modificacion/model_mod');
        $this->load->model('mantenimiento/model_funcionario');
        $this->load->model('menu_modelo');

        $this->gestion = $this->session->userData('gestion'); /// Gestion
        $this->fun_id = $this->session->userData('fun_id'); /// Fun id

        }else{
            redirect('/','refresh');
        }
    }

    /*----- OPCION 1 - MODIFICAR PLAZO ---------*/
    public function opciones_modificar($mod,$fase_id,$proy_id,$tp_mod){
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id); ////// DATOS DEL PROYECTO
        $data['id_f'] = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
        $data['mod']=1;

        if(count($data['proyecto'])!=0 and count($data['id_f'])!=0){
          if($tp_mod==1){
            $data['plazos']=$this->historial_plazo($fase_id);
            $this->load->view('admin/modificacion/poa/opciones/mod1_plazo', $data);
          }
          elseif ($tp_mod==2) {
            $data['list_componente']=$this->list_componentes2($fase_id);
            $this->load->view('admin/modificacion/poa/opciones/mod2/mod2_meta_total', $data);
          }
          elseif ($tp_mod==3) {
            $data['list_componente']=$this->list_componentes3($fase_id);
            $this->load->view('admin/modificacion/poa/opciones/mod3/mod3_meta_anual', $data);
          }
          elseif ($tp_mod==4) {
            $data['presupuesto']=$this->presupuesto($proy_id);
            $this->load->view('admin/modificacion/poa/opciones/mod4_presupuesto', $data);
          }
          elseif ($tp_mod==6) {
            $data['componentes']=$this->modidicar_presupuesto_componentes($proy_id);
            $this->load->view('admin/modificacion/poa/opciones/mod6/mod6_list_componentes', $data);
          }
          else{
            echo "Trabajando";
          }
        }
        else{
            redirect('admin/dashboard');
        }
    }

/*------------ ADICIONA TECHO PRESUPUESTO - LISTA DE COMPONENTES --------------*/
    public function modidicar_presupuesto_componentes($proy_id){
      $tabla='';
      $fase = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
      $componente = $this->model_componente->componentes_id($fase[0]['id']); /// LISTA DE COMPONENTE

      if($fase[0]['pfec_ejecucion']==1){
      $nro=0;
      $tabla.=' 
            <article class="col-sm-12 col-md-12 col-lg-12">
              <section id="widget-grid" class="well">
                <div class="widget-body no-padding">
                  <div class="panel-group smart-accordion-default" id="accordion-2">';
                    foreach($componente  as $rowc){
                      $productos=$this->model_producto->list_productos_actividad($rowc['com_id']);
                      $nro++;
                      $panel='panel-collapse collapse';
                      $colapsed='collapsed';
                      if($nro==1){
                        $panel='panel-collapse collapse in';
                        $colapsed='';
                      }
                      $tabla.='<div class="panel panel-default">
                                <div class="panel-heading">
                                  <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#'.$nro.'" class="'.$colapsed.'"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i>'.$nro.'.- '.$rowc['com_componente'].'</a></h4>
                                </div>
                                <div id="'.$nro.'" class="'.$panel.'">
                                  <div class="panel-body">
                                    <b>LISTA DE PRODUCTOS - ACTIVIDADES</b><br> 
                                      <table class="table table-bordered table-condensed">
                                      <thead>
                                        <tr>
                                          <th style="width:1%;">Nro</th>
                                          <th style="width:20%;">PRODUCTO</th>
                                          <th style="width:5%;">PONDERACI&Oacute;N</th>
                                          <th style="width:20%;">ACTIVIDAD</th>
                                          <th style="width:5%;">PONDERACI&Oacute;N</th>
                                          <th style="width:5%;"></th>
                                        </tr>
                                      </thead>
                                      <tbody>';
                                      $nro_p=0;
                                        foreach($productos  as $rowp){
                                          $nro_p++;
                                          $tabla.=
                                          '<tr>
                                            <td>'.$nro_p.'</td>
                                            <td>'.$rowp['prod_producto'].'</td>
                                            <td>'.$rowp['prod_ponderacion'].'%</td>
                                            <td>'.$rowp['act_actividad'].'</td>
                                            <td>'.$rowp['act_ponderacion'].'%</td>
                                            <td>
                                              <br><a href="'.site_url("").'/mod/partidas/'.$rowp['act_id'].'" class="btn btn-primary" title="MODIFICAR">MODIFICAR PTTO.</a>
                                            </td>
                                          </tr>';
                                        }
                                        $tabla.='
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>';
                   
                $tabla.='
                  </div>
                </div>
              </section>
            </article>';
          }
        
      }
      else{
        $tabla.='
          <article class="col-sm-12 col-md-12 col-lg-12">
          <section id="widget-grid" class="well">
            <div class="">
                <form action="#" id="form_nuevo" name="form_nuevo" class="smart-form" method="post">
                     <h3 class="text-primary">COMPONENTES</h3>';
                      $nro=0;
                      foreach($componente  as $rowc){
                        $nro++;
                        $tabla.='<div class="alert alert-info">'.$nro.'.- '.$rowc['com_componente'].'</div><br>';
                        $tabla.=$this->mis_partidas($rowc['com_id'],2).'<br>';
                      }
                    $tabla.='            
                  <footer>
                    <div id="but" style="display:none;">
                      <button type="button" name="add_form" id="add_form" class="btn btn-primary">MODIFICAR MONTO PARTIDAS</button>
                    </div>          
                  </footer>
                </form>
              </div>
          </section>
          </article>';

      }

      return $tabla;
    }


    /*------------- LISTA DE PARTIDAS -------------*/
    public function mod_partidas($act_id){
      $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
      $data['enlaces'] = $enlaces;      
      $data['act']=$this->model_actividad->get_actividad_id($act_id);
      $data['prod']=$this->model_producto->get_producto_id($data['act'][0]['prod_id']);
      $data['comp']=$this->model_componente->get_componente($data['prod'][0]['com_id']);
      $fase=$this->model_faseetapa->get_fase($data['comp'][0]['pfec_id']);

      $data['nro_fase']= $this->model_faseetapa->nro_fase($fase[0]['proy_id']); /// nro de fases y etapas registrados
      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($fase[0]['proy_id']); ////// DATOS DEL PROYECTO
      $data['id_f'] = $this->model_faseetapa->get_id_fase($fase[0]['proy_id']); //// DATOS DE LA FASE ACTIVA
      $data['mod']=1;

      $data['partidas']=$this->mis_partidas($act_id,$data['id_f'][0]['pfec_ejecucion']);
      $this->load->view('admin/modificacion/poa/opciones/mod6/mod6_partidas', $data);
    }


    /*------------ MODIFICA PARTIDA --------------*/
    public function mis_partidas($tp,$tp_ejec){
      // tp 1: Actividad
      // tp 2: Componente
      $partidas=$this->model_mod->list_partida_insumo($tp,$tp_ejec);
      $total=$this->model_mod->suma_monto_partida($tp,$tp_ejec);
      $tabla='';
      if(count($partidas)!=0){
         $tabla.='<b>LISTA DE PARTIDAS</b><br> 
                  <table class="table table-bordered table-condensed" style="width:100%;">
                    <thead>
                      <tr style="font-size: 11px;">
                        <th style="width:1%;">Nro</th>
                        <th style="width:10%;">JUSTIFICACI&Oacute;N</th>
                        <th style="width:10%;">PARTIDA</th>
                        <th style="width:5%;">FUENTE DE FINANCIAMIENTO</th>
                        <th style="width:5%;">ORGANISMO FINANCIADOR</th>
                        <th style="width:5%;">MONTO INICIAL</th>
                        <th style="width:5%;">MONTO MODIFICADO</th>
                        <th style="width:5%;">MONTO VIGENTE</th>
                        <th style="width:3%;">MODIFICAR</th>
                      </tr>
                    </thead>
                    <tbody>';
                    $nro=0;
                  foreach ($partidas as $row) {
                    $nro++;
                    $tabla.='<tr style="font-size: 11px;">';
                      $tabla.='<td>'.$nro.'</td>';
                      $tabla.='<td>'.$row['insp_justificacion'].'</td>';
                      $tabla.='<td>'.$row['par_codigo'].' - '.$row['par_nombre'].'</td>';
                      $tabla.='<td>'.$row['ff_codigo'].' - '.$row['ff_descripcion'].'</td>';
                      $tabla.='<td>'.$row['of_codigo'].' - '.$row['of_descripcion'].'</td>';
                      $tabla.='<td align=center>'.$row['insp_inicial'].'</td>';
                      $tabla.='<td align=center>'.$row['insp_modifi'].'</td>';
                      $tabla.='<td align=center>'.$row['insp_actual'].'</td>';
                      $tabla.='<td align=center><a href="'.site_url("").'/mod/mod_par/'.$row['insp_id'].'/'.$tp_ejec.'"  title="MODIFICAR DATOS PARTIDA" class="btn btn-primary btn-lg" >MODIFICAR</a></td>';
                    $tabla.='</tr>';
                  }
                  $tabla.='</tbody>
                  </table>';
      }
      else{
        $tabla.='Sin Partidas Registradas';
      }

      return $tabla;
    }



    /*------------ MODIFICA PRESUPUESTO --------------*/
    public function presupuesto($proy_id){
      $proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
      $fase = $this->model_faseetapa->get_id_fase($proy_id);
      $ptto_fase =$this->model_faseetapa->ptto_fase($fase[0]['id']);
      $programado=$this->model_faseetapa->sum_ptto_fase($fase[0]['id']);
      $tabla ='';
      $tabla .='
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-custombutton="false">
        <header>
          <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
          <h2>MODIFICAR PRESUPUESTO</h2>       
        </header>
        <div>
          <div class="jarviswidget-editbox">
          </div>
          <div class="widget-body no-padding">
            
            <form action="'.site_url('').'/modificaciones/cmod_opciones/update_ppto" id="form_nuevo3" name="form_nuev3" class="smart-form" method="post">
              <input type="hidden" name="proy_id" id="proy_id" value='.$proy_id.'>
              <input type="hidden" name="pfec_id" id="pfec_id" value='.$fase[0]['id'].'>
              <input type="hidden" name="nro" id="nro" value='.count($ptto_fase).'>

              <header>
                <center><div class="alert alert-info">MODIFICAR PRESUPUESTO '.$fase[0]['pfec_fecha_inicio'].' - '.$fase[0]['pfec_fecha_fin'].'</div></center>
              </header>
              <fieldset>
                <section>
                  <div class="row">
                    <label class="label col col-2"><b>PRESUPUESTO TOTAL FASE :</b></label>
                    <div class="col col-4">
                      <label class="input"> <i class="icon-append fa fa-user"></i>
                        <input type="text" name="ptto" id="ptto" value='.$fase[0]['pfec_ptto_fase'].' onkeyup="suma_presupuesto();">
                      </label>
                    </div>
                  </div>
                </section>
              </fieldset>
              <fieldset>
                <div class="row">';
                $nro=0;
                foreach ($ptto_fase as $row) {
                  $nro++;
                  $tabla .='
                  <section class="col col-2">
                    <label class="input"><b>GESTI&Oacute;N '.$row['g_id'].'</b>
                      <input type="hidden" name="fgp_id[]" value='.$row['ptofecg_id'].'>
                      <input type="text" name="monto[]" id="fgp_id'.$nro.'" value='.$row['pfecg_ppto_total'].' onkeyup="suma_presupuesto();">
                    </label>
                  </section>';
                }
              $tabla .='
                </div>
              </fieldset>
              <fieldset>
                <section>
                  <div class="row">
                    <label class="label col col-2"><b>SALDO POR PROGRAMAR:</b></label>
                    <div class="col col-4">
                      <label class="input">
                        <input type="text" name="saldo" id="saldo" value='.($fase[0]['pfec_ptto_fase']-$programado[0]['programado']).' disabled>
                      </label>
                    </div>
                    <div id="tit">';
                      if(($fase[0]['pfec_ptto_fase']-$programado[0]['programado'])==0){
                        $tabla.='<font color="#42F990"> SIN SALDO</font>';
                      }
                      elseif (($fase[0]['pfec_ptto_fase']-$programado[0]['programado'])>0) {
                        $tabla.='<font color="red"> SALDO PENDIENTE</font>';
                      }
                      elseif (($fase[0]['pfec_ptto_fase']-$programado[0]['programado'])<0) {
                        $tabla.='<font color="red"> SALDO SOBREGIRADO</font>';
                      }
                    $tabla.='  
                    </div>
                  </div>
                </section>
              </fieldset>
              <footer>
                <button type="button" name="add_form3" id="add_form3" class="btn btn-primary">MODIFICAR PRESUPUESTO</button>
                <a href="'.site_url("").'/mod/opcion_fis/1/'.$fase[0]['id'].'/'.$fase[0]['proy_id'].'" title="CANCELAR" class="btn btn-default">CANCELAR</a>
              </footer>
              <center><img id="load3" style="display: none" src="'.base_url().'/assets/img/loading.gif" width="20" height="20"></center>
            </form>
          </div>
        </div>
      </div>
      </article>';

      return $tabla;
    }

    /*-------- VALIDA MODIFICACION DE PRESUPUESTO ----------*/
    function update_ppto(){
      if($this->input->post()){
          $this->form_validation->set_rules('proy_id', 'Proyecto', 'required|trim');
          $this->form_validation->set_rules('pfec_id', 'Fase', 'required|trim');
          if($this->form_validation->run()) {
            $proy_id=$this->security->xss_clean($this->input->post('proy_id'));
            $pfec_id=$this->security->xss_clean($this->input->post('pfec_id'));

            $ptto_fase =$this->model_faseetapa->ptto_fase($pfec_id); //// Presupuestos de la fase activa

            $data = array(
              'pfec_id' => $pfec_id,
              'num_ip' => $this->input->ip_address(), 
              'nom_ip' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
              'fun_id' => $this->session->userdata("fun_id"),
            );
            $this->db->insert('_ptto_fase_modificado',$data);
            $pttom_id=$this->db->insert_id();

            foreach ($ptto_fase as $row) {
                $data2 = array(
                'pttom_id' => $pttom_id,
                'ptofecg_id' => $row['ptofecg_id'],
                'pfecg_ppto_total' => $row['pfecg_ppto_total'],
                'g_id' => $row['g_id'],
              );
              $this->db->insert('_ptto_fase_gestion_historial',$data2);
            }

            if (!empty($_POST["fgp_id"]) && is_array($_POST["fgp_id"]) ) {
              foreach ( array_keys($_POST["fgp_id"]) as $como  ) {

                $update_ptto = array(
                  'pfecg_ppto_total' => $_POST["monto"][$como],
                  'estado' => 2,
                  'fun_id' => $this->fun_id,
                );
                $this->db->where('pfec_id', $pfec_id);
                $this->db->where('ptofecg_id', $_POST["fgp_id"][$como]);
                $this->db->update('ptto_fase_gestion', $update_ptto);
              }
            }

            $this->session->set_flashdata('success','SE MODIFICO CORRECTAMENTE EL PRESUPUESTO PROGRAMADO DE GESTIONES DE LA FASE ACTIVA');
            redirect(site_url("").'/mod/opciones/1/'.$pfec_id.'/'.$proy_id.'/4');

          }
          else{
            $this->session->set_flashdata('danger','ERROR AL MODIFICAR EL PRESUPUESTO');
            redirect(site_url("").'/mod/opciones/1/'.$pfec_id.'/'.$proy_id.'/4');
          }
      
      }
      else{
        $this->session->set_flashdata('danger','ERROR AL MODIFICAR EL PRESUPUESTO');
            redirect(site_url("").'/mod/opciones/1/'.$pfec_id.'/'.$proy_id.'/4');
      }
    }

    /*------------ LISTA DE COMPONENTES (3)------------*/
      public function list_componentes3($pfec_id){
        $tabla='';
        $comp=$this->model_componente->componentes_id($pfec_id);
        $tabla.='<table class="table table-bordered">';
        $nro=0;
        foreach($comp as $row){
          $nro++;
          $tabla.='<tr>';
            $tabla.='<td style="width:1%;">'.$nro.'</td>';
            $tabla.='<td style="width:19%;">'.$row['com_componente'].'</td>';
            $tabla.='<td style="width:80%;">';
              $productos = $this->model_producto->list_prod($row['com_id']);
              if(count($productos)!=0){
                $tabla.=''.$this->list_productos_plurianual($pfec_id,$productos,$nro);
              }

            $tabla.='</td>';
          $tabla.='</tr>';
        }
        $tabla.='</table>';

        return $tabla;
      }

      /*------------ LISTA DE PRODUCTOS PLURIANUAL--------------*/
      public function list_productos_plurianual($pfec_id,$productos,$nro){
        $tabla='';
        $tabla.='
          <div class="jarviswidget jarviswidget-color-darken" >
            <header>
              <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
            </header>
              <div>
                <div class="widget-body ">
                  <table id="dt_basic'.$nro.'" class="table table-bordered" style="width:100%;">';
                  $tabla.='
                        <thead>
                        <tr align=center>
                          <th style="width:1%;">#</th>
                          <th style="width:10%;">PRODUCTO</th>
                          <th style="width:10%;">ACTIVIDAD</th>
                          <th style="width:5%;">TIPO</th>
                          <th style="width:5%;">LINEA BASE</th>
                          <th style="width:5%;">META</th>
                          <th style="width:10%;">MEDIO DE VERIFICACI&Oacute;N</th>
                          <th style="width:4.16%;">ENE.</th>
                          <th style="width:4.16%;">FEB.</th>
                          <th style="width:4.16%;">MAR.</th>
                          <th style="width:4.16%;">ABR.</th>
                          <th style="width:4.16%;">MAY.</th>
                          <th style="width:4.16%;">JUN.</th>
                          <th style="width:4.16%;">JUL.</th>
                          <th style="width:4.16%;">AGO.</th>
                          <th style="width:4.16%;">SEPT.</th>
                          <th style="width:4.16%;">OCT.</th>
                          <th style="width:4.16%;">NOV.</th>
                          <th style="width:4.16%;">DIC.</th>
                          <th style="width:4%;"></th>
                        </tr>
                        </thead>
                        <tbody>';
                  $nro_p=0;
                  foreach($productos as $row){
                    $temp=$this->temporalizacion_productos($row['prod_id']);
                    $nro_p++;
                    $tabla.='<tr>';
                      $tabla.='<td>'.$nro_p.'</td>';
                      $tabla.='<td>'.$row['prod_producto'].'</td>';
                      $tabla.='<td></td>';
                      $tabla.='<td>'.$row['indi_abreviacion'].'</td>';
                      $tabla.='<td>'.$row['prod_linea_base'].'</td>';
                      $tabla.='<td>'.$row['prod_meta'].'</td>';
                      $tabla.='<td>'.$row['prod_fuente_verificacion'].'</td>';
                      for ($i=1; $i <=12 ; $i++) { 
                        $tabla.='<td bgcolor="#e5fde5">'.$temp[1][$i].'</td>';
                      }
                      $tabla.='<td><a href="' . site_url("").'/mod/prod_mtotal/'.$pfec_id.'/'.$row['prod_id'].'" class="btn btn-primary" title="MODIFICAR META TOTAL PRODUCTO">MODIFICAR META</a></td>';
                    $tabla.='</tr>';
                    $actividad=$this->model_actividad->list_act_anual($row['prod_id']);
                    
                    if(count($actividad)!=0){
                      /*--------------- Lista de Actividades --------------*/
                      $nro_a=0;
                      foreach($actividad as $rowa){
                        $temp=$this->temporalizacion_productos($row['prod_id']);
                        $nro_a++;
                        $tabla.='<tr bgcolor="#d6eff9" title="ACTIVIDAD">';
                          $tabla.='<td>'.$nro_p.'.'.$nro_a.'</td>';
                          $tabla.='<td></td>';
                          $tabla.='<td>'.$rowa['act_actividad'].'</td>';
                          $tabla.='<td>'.$rowa['indi_abreviacion'].'</td>';
                          $tabla.='<td>'.$rowa['act_linea_base'].'</td>';
                          $tabla.='<td>'.$rowa['act_meta'].'</td>';
                          $tabla.='<td>'.$rowa['act_fuente_verificacion'].'</td>';
                          for ($i=1; $i <=12 ; $i++) { 
                            $tabla.='<td bgcolor="#e5fde5">'.$temp[1][$i].'</td>';
                          }
                      $tabla.='<td><a href="' . site_url("").'/mod/act_mtotal/'.$pfec_id.'/'.$rowa['act_id'].'" class="btn btn-primary" title="MODIFICAR META TOTAL ACTIVIDAD">MODIFICAR META</a></td>';
                        $tabla.='</tr>';
                      }
                      /*---------------------------------------------------*/
                    }
                  }
                $tabla.='
              </tbody>
            </table>
            </div>
            </div>
          </div>';

        return $tabla;
      }

      /*------------ LISTA DE COMPONENTES (2)------------*/
      public function list_componentes2($pfec_id){
        $tabla='';
        $comp=$this->model_componente->componentes_id($pfec_id);
        $tabla.='<table class="table table-bordered">';
        $nro=0;
        foreach($comp as $row){
          $nro++;
          $tabla.='<tr>';
            $tabla.='<td style="width:1%;">'.$nro.'</td>';
            $tabla.='<td style="width:19%;">'.$row['com_componente'].'</td>';
            $tabla.='<td style="width:80%;">';
              $productos = $this->model_producto->list_prod($row['com_id']);
              if(count($productos)!=0){
                $tabla.=''.$this->list_productos($pfec_id,$productos,$nro);
              }

            $tabla.='</td>';
          $tabla.='</tr>';
        }
        $tabla.='</table>';

        return $tabla;
      }

      /*------------ LISTA DE PRODUCTOS --------------*/
      public function list_productos($pfec_id,$productos,$nro){
        $tabla='';
        $tabla.='
          <div class="jarviswidget jarviswidget-color-darken" >
            <header>
              <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
            </header>
              <div>
                <div class="widget-body ">
                  <table id="dt_basic'.$nro.'" class="table table-bordered" style="width:100%;">';
                  $tabla.='
                        <thead>
                        <tr align=center>
                          <th style="width:1%;">#</th>
                          <th style="width:10%;">PRODUCTO</th>
                          <th style="width:10%;">ACTIVIDAD</th>
                          <th style="width:5%;">TIPO</th>
                          <th style="width:5%;">LINEA BASE</th>
                          <th style="width:5%;">META</th>
                          <th style="width:10%;">MEDIO DE VERIFICACI&Oacute;N</th>
                          <th style="width:4.16%;">ENE.</th>
                          <th style="width:4.16%;">FEB.</th>
                          <th style="width:4.16%;">MAR.</th>
                          <th style="width:4.16%;">ABR.</th>
                          <th style="width:4.16%;">MAY.</th>
                          <th style="width:4.16%;">JUN.</th>
                          <th style="width:4.16%;">JUL.</th>
                          <th style="width:4.16%;">AGO.</th>
                          <th style="width:4.16%;">SEPT.</th>
                          <th style="width:4.16%;">OCT.</th>
                          <th style="width:4.16%;">NOV.</th>
                          <th style="width:4.16%;">DIC.</th>
                          <th style="width:4%;"></th>
                        </tr>
                        </thead>
                        <tbody>';
                  $nro_p=0;
                  foreach($productos as $row){
                    $temp=$this->temporalizacion_productos($row['prod_id']);
                    $nro_p++;
                    $tabla.='<tr>';
                      $tabla.='<td>'.$nro_p.'</td>';
                      $tabla.='<td>'.$row['prod_producto'].'</td>';
                      $tabla.='<td></td>';
                      $tabla.='<td>'.$row['indi_abreviacion'].'</td>';
                      $tabla.='<td>'.$row['prod_linea_base'].'</td>';
                      $tabla.='<td>'.$row['prod_meta'].'</td>';
                      $tabla.='<td>'.$row['prod_fuente_verificacion'].'</td>';
                      for ($i=1; $i <=12 ; $i++) { 
                        $tabla.='<td bgcolor="#e5fde5">'.$temp[1][$i].'</td>';
                      }
                      $tabla.='<td><a href="' . site_url("").'/mod/prod_mtotal/'.$pfec_id.'/'.$row['prod_id'].'" class="btn btn-primary" title="MODIFICAR META TOTAL PRODUCTO">MODIFICAR META</a></td>';
                    $tabla.='</tr>';
                    $actividad=$this->model_actividad->list_act_anual($row['prod_id']);
                    
                    if(count($actividad)!=0){
                      /*--------------- Lista de Actividades --------------*/
                      $nro_a=0;
                      foreach($actividad as $rowa){
                        $temp=$this->temporalizacion_productos($row['prod_id']);
                        $nro_a++;
                        $tabla.='<tr bgcolor="#d6eff9" title="ACTIVIDAD">';
                          $tabla.='<td>'.$nro_p.'.'.$nro_a.'</td>';
                          $tabla.='<td></td>';
                          $tabla.='<td>'.$rowa['act_actividad'].'</td>';
                          $tabla.='<td>'.$rowa['indi_abreviacion'].'</td>';
                          $tabla.='<td>'.$rowa['act_linea_base'].'</td>';
                          $tabla.='<td>'.$rowa['act_meta'].'</td>';
                          $tabla.='<td>'.$rowa['act_fuente_verificacion'].'</td>';
                          for ($i=1; $i <=12 ; $i++) { 
                            $tabla.='<td bgcolor="#e5fde5">'.$temp[1][$i].'</td>';
                          }
                      $tabla.='<td><a href="' . site_url("").'/mod/act_mtotal/'.$pfec_id.'/'.$rowa['act_id'].'" class="btn btn-primary" title="MODIFICAR META TOTAL ACTIVIDAD">MODIFICAR META</a></td>';
                        $tabla.='</tr>';
                      }
                      /*---------------------------------------------------*/
                    }
                  }
                $tabla.='
              </tbody>
            </table>
            </div>
            </div>
          </div>';

        return $tabla;
      }

      /*------------ UPDATE META TOTAL PRODUCTO --------------*/
      public function mod_prod_mtotal($pfec_id,$prod_id){
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $fase=$this->model_faseetapa->get_fase($pfec_id);
        $data['nro_fase']= $this->model_faseetapa->nro_fase($fase[0]['proy_id']); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($fase[0]['proy_id']); ////// DATOS DEL PROYECTO
        $data['id_f'] = $this->model_faseetapa->get_id_fase($fase[0]['proy_id']); //// DATOS DE LA FASE ACTIVA
        $data['mod']=1;

        $data['producto'] = $this->model_producto->get_producto_id($prod_id);
        $data['componente'] = $this->model_componente->get_componente($data['producto'][0]['com_id']);

        $this->load->view('admin/modificacion/poa/opciones/mod2/edit_prod_mtotal', $data);
      }

      /*------------ VALIDA UPDATE META TOTAL PRODUCTO --------------*/
      public function valida_update_mtotal(){
        if ($this->input->server('REQUEST_METHOD') === 'POST'){
            $this->form_validation->set_rules('prod_id', 'Prod Id', 'required|trim');

            if ($this->form_validation->run()){
                $producto = $this->model_producto->get_producto_id($this->input->post('prod_id'));
                $this->copia_producto($producto);
                $this->model_producto->delete_producto_p($this->input->post('prod_id'));

                $update_prod = array(
                  'prod_linea_base' => $this->input->post('lb'),
                  'prod_meta' => $this->input->post('met'),
                  'estado' => '2',
                  'fun_id' => $this->fun_id,
                );
                $this->db->where('prod_id', $this->input->post('prod_id'));
                $this->db->update('_productos', $update_prod);

                $gestion=$this->input->post('gest');
                if ( !empty($_POST["m1"]) && is_array($_POST["m1"])){
                    foreach ( array_keys($_POST["m1"]) as $como){
                      if($_POST["m1"][$como]!=0){
                          $this->model_producto->add_prod_gest($this->input->post('prod_id'),$gestion,1,$_POST["m1"][$como]);
                      }
                      if($_POST["m2"][$como]!=0){
                          $this->model_producto->add_prod_gest($this->input->post('prod_id'),$gestion,2,$_POST["m2"][$como]);
                      }
                      if($_POST["m3"][$como]!=0){
                          $this->model_producto->add_prod_gest($this->input->post('prod_id'),$gestion,3,$_POST["m3"][$como]);
                      }
                      if($_POST["m4"][$como]!=0){
                          $this->model_producto->add_prod_gest($this->input->post('prod_id'),$gestion,4,$_POST["m4"][$como]);
                      }
                      if($_POST["m5"][$como]!=0){
                          $this->model_producto->add_prod_gest($this->input->post('prod_id'),$gestion,5,$_POST["m5"][$como]);
                      }
                      if($_POST["m6"][$como]!=0){
                          $this->model_producto->add_prod_gest($this->input->post('prod_id'),$gestion,6,$_POST["m6"][$como]);
                      }
                      if($_POST["m7"][$como]!=0){
                          $this->model_producto->add_prod_gest($this->input->post('prod_id'),$gestion,7,$_POST["m7"][$como]);
                      }
                      if($_POST["m8"][$como]!=0){
                          $this->model_producto->add_prod_gest($this->input->post('prod_id'),$gestion,8,$_POST["m8"][$como]);
                      }
                      if($_POST["m9"][$como]!=0){
                          $this->model_producto->add_prod_gest($this->input->post('prod_id'),$gestion,9,$_POST["m9"][$como]);
                      }
                      if($_POST["m10"][$como]!=0){
                          $this->model_producto->add_prod_gest($this->input->post('prod_id'),$gestion,10,$_POST["m10"][$como]);
                      }
                      if($_POST["m11"][$como]!=0){
                          $this->model_producto->add_prod_gest($this->input->post('prod_id'),$gestion,11,$_POST["m11"][$como]);
                      }
                      if($_POST["m12"][$como]!=0){
                          $this->model_producto->add_prod_gest($this->input->post('prod_id'),$gestion,12,$_POST["m12"][$como]);
                      }
                      
                      $gestion++;
                    }
                }

              $componente=$this->model_componente->get_componente($producto[0]['com_id']);
              redirect('mod/opciones/1/'.$componente[0]['pfec_id']."/".$this->input->post('proy_id').'/2');
            }
            else{ 
              redirect('mod/prod_mtotal/'.$componente[0]['pfec_id']."/".$produto[0]['prod_id'].'/false');
            }
        }
    }

    /*------ COPIA PRODUCTO ------*/
    public function copia_producto($producto){
      $data_to_store = array(
        'prod_id' => $producto[0]['prod_id'],
        'prod_producto' => $producto[0]['prod_producto'],
        'indi_id' => $producto[0]['indi_id'],
        'prod_indicador' => $producto[0]['prod_indicador'],
        'prod_linea_base' => $producto[0]['prod_linea_base'],
        'prod_meta' => $producto[0]['prod_meta'],
        'fun_id' => $this->session->userdata("fun_id"),
      );
      $this->db->insert('_hproductos', $data_to_store);
      $hprod_id=$this->db->insert_id();
      /*====================================*/
      $temp=$this->model_producto->list_temporalidad($producto[0]['prod_id']);
      /*===== GUARDA TEMPORALIDAD ======*/
      foreach($temp as $row){
        $temporalidad = array(
        'hprod_id' => $hprod_id,
        'm_id' => $row['m_id'],
        'pg_fis' => $row['pg_fis'],
        'pg_fin' => $row['pg_fin'],
        'g_id' => $row['g_id'],
        );
        $this->db->insert('hprod_programado_mensual', $temporalidad); 
      }
    }
    

    /*------ TEMPORALIZACION DE PRODUCTOS ------*/
    public function temporalizacion_productos($prod_id){
      $prod_prog= $this->model_producto->producto_programado($prod_id,$this->gestion);//// Temporalidad Programado

      $mp[1]='enero';
      $mp[2]='febrero';
      $mp[3]='marzo';
      $mp[4]='abril';
      $mp[5]='mayo';
      $mp[6]='junio';
      $mp[7]='julio';
      $mp[8]='agosto';
      $mp[9]='septiembre';
      $mp[10]='octubre';
      $mp[11]='noviembre';
      $mp[12]='diciembre';

      for ($i=1; $i <=12 ; $i++) { 
        $matriz[1][$i]=0; /// Programado
      }
      
      if(count($prod_prog)!=0){
        for ($i=1; $i <=12 ; $i++) { 
          $matriz[1][$i]=$prod_prog[0][$mp[$i]];
        }
      }

      return $matriz;
    }


      /*------------ UPDATE META TOTAL ACTIVIDAD --------------*/
      public function mod_act_mtotal($pfec_id,$act_id){
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $fase=$this->model_faseetapa->get_fase($pfec_id);
        $data['nro_fase']= $this->model_faseetapa->nro_fase($fase[0]['proy_id']); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($fase[0]['proy_id']); ////// DATOS DEL PROYECTO
        $data['id_f'] = $this->model_faseetapa->get_id_fase($fase[0]['proy_id']); //// DATOS DE LA FASE ACTIVA
        $data['mod']=1;

        $data['actividad']=$this->model_actividad->get_actividad_id($act_id);
        $data['producto'] = $this->model_producto->get_producto_id($data['actividad'][0]['prod_id']);
        $data['componente'] = $this->model_componente->get_componente($data['producto'][0]['com_id']);
        $data['programado'] = $this->model_actividad->suma_programado($act_id); //// SUMA PROGRAMADO

        $this->load->view('admin/modificacion/poa/opciones/mod2/edit_act_mtotal', $data);
      }

      /*------------ VALIDA UPDATE META TOTAL ACTIVIDAD --------------*/
      public function valida_update_mtotal_act(){
        if ($this->input->server('REQUEST_METHOD') === 'POST'){
            $this->form_validation->set_rules('act_id', 'Act Id', 'required|trim');

            if ($this->form_validation->run()){
                $actividad = $this->model_actividad->get_actividad_id($this->input->post('act_id'));
                $this->copia_actividad($actividad);
                $this->model_actividad->delete_act_gest($this->input->post('act_id'));

                $update_act = array(
                  'act_linea_base' => $this->input->post('lb'),
                  'act_meta' => $this->input->post('met'),
                  'estado' => '2',
                  'fun_id' => $this->fun_id,
                );
                $this->db->where('act_id', $this->input->post('act_id'));
                $this->db->update('_actividades', $update_act);

                $gestion=$this->input->post('gest');
                if ( !empty($_POST["m1"]) && is_array($_POST["m1"])){
                    foreach ( array_keys($_POST["m1"]) as $como){
                      if($_POST["m1"][$como]!=0){
                          $this->model_actividad->add_act_gest($this->input->post('act_id'),$gestion,1,$_POST["m1"][$como],($_POST["m1"][$como]*$actividad[0]['act_costo_uni']));
                      }
                      if($_POST["m2"][$como]!=0){
                          $this->model_actividad->add_act_gest($this->input->post('act_id'),$gestion,2,$_POST["m2"][$como],($_POST["m2"][$como]*$actividad[0]['act_costo_uni']));
                      }
                      if($_POST["m3"][$como]!=0){
                          $this->model_actividad->add_act_gest($this->input->post('act_id'),$gestion,3,$_POST["m3"][$como],($_POST["m3"][$como]*$actividad[0]['act_costo_uni']));
                      }
                      if($_POST["m4"][$como]!=0){
                          $this->model_actividad->add_act_gest($this->input->post('act_id'),$gestion,4,$_POST["m4"][$como],($_POST["m4"][$como]*$actividad[0]['act_costo_uni']));
                      }
                      if($_POST["m5"][$como]!=0){
                          $this->model_actividad->add_act_gest($this->input->post('act_id'),$gestion,5,$_POST["m5"][$como],($_POST["m5"][$como]*$actividad[0]['act_costo_uni']));
                      }
                      if($_POST["m6"][$como]!=0){
                          $this->model_actividad->add_act_gest($this->input->post('act_id'),$gestion,6,$_POST["m6"][$como],($_POST["m6"][$como]*$actividad[0]['act_costo_uni']));
                      }
                      if($_POST["m7"][$como]!=0){
                          $this->model_actividad->add_act_gest($this->input->post('act_id'),$gestion,7,$_POST["m7"][$como],($_POST["m7"][$como]*$actividad[0]['act_costo_uni']));
                      }
                      if($_POST["m8"][$como]!=0){
                          $this->model_actividad->add_act_gest($this->input->post('act_id'),$gestion,8,$_POST["m8"][$como],($_POST["m8"][$como]*$actividad[0]['act_costo_uni']));
                      }
                      if($_POST["m9"][$como]!=0){
                          $this->model_actividad->add_act_gest($this->input->post('act_id'),$gestion,9,$_POST["m9"][$como],($_POST["m9"][$como]*$actividad[0]['act_costo_uni']));
                      }
                      if($_POST["m10"][$como]!=0){
                          $this->model_actividad->add_act_gest($this->input->post('act_id'),$gestion,10,$_POST["m10"][$como],($_POST["m10"][$como]*$actividad[0]['act_costo_uni']));
                      }
                      if($_POST["m11"][$como]!=0){
                          $this->model_actividad->add_act_gest($this->input->post('act_id'),$gestion,11,$_POST["m11"][$como],($_POST["m11"][$como]*$actividad[0]['act_costo_uni']));
                      }
                      if($_POST["m12"][$como]!=0){
                          $this->model_actividad->add_act_gest($this->input->post('act_id'),$gestion,12,$_POST["m12"][$como],($_POST["m12"][$como]*$actividad[0]['act_costo_uni']));
                      }
                      
                      $gestion++;
                    }
                }

              $producto = $this->model_producto->get_producto_id($actividad[0]['prod_id']);
              $componente=$this->model_componente->get_componente($producto[0]['com_id']);
              
              redirect('mod/opciones/1/'.$componente[0]['pfec_id']."/".$this->input->post('proy_id').'/2');
            }
            else{ 
              redirect('mod/act_mtotal/'.$componente[0]['pfec_id']."/".$actividad[0]['act_id'].'/false');
            }
        }
    }

    /*------ COPIA ACTIVIDAD ------*/
    public function copia_actividad($actividad){
      $data_to_store = array(
        'act_id' => $actividad[0]['act_id'],
        'act_linea_base' => $actividad[0]['act_linea_base'],
        'act_meta' => $actividad[0]['act_meta'],
        'fun_id' => $this->session->userdata("fun_id"),
      );
      $this->db->insert('_hactividades', $data_to_store);
      $hact_id=$this->db->insert_id();
      /*====================================*/
      $temp=$this->model_actividad->list_temporalidad($actividad[0]['act_id']);
      /*===== GUARDA TEMPORALIDAD ======*/
      foreach($temp as $row){
        $temporalidad = array(
        'hact_id' => $hact_id,
        'm_id' => $row['m_id'],
        'pg_fis' => $row['pg_fis'],
        'pg_fin' => $row['pg_fin'],
        'g_id' => $row['g_id'],
        );
        $this->db->insert('hact_programado_mensual', $temporalidad); 
      }
    }


    /*------ TEMPORALIZACION DE ACTIVIDAD ------*/
    public function temporalizacion_actividad($prod_id){
      $prod_prog= $this->model_producto->actividad_programado($act_id,$this->gestion);//// Temporalidad Programado

      $mp[1]='enero';
      $mp[2]='febrero';
      $mp[3]='marzo';
      $mp[4]='abril';
      $mp[5]='mayo';
      $mp[6]='junio';
      $mp[7]='julio';
      $mp[8]='agosto';
      $mp[9]='septiembre';
      $mp[10]='octubre';
      $mp[11]='noviembre';
      $mp[12]='diciembre';

      for ($i=1; $i <=12 ; $i++) { 
        $matriz[1][$i]=0; /// Programado
      }
      
      if(count($prod_prog)!=0){
        for ($i=1; $i <=12 ; $i++) { 
          $matriz[1][$i]=$prod_prog[0][$mp[$i]];
        }
      }

      return $matriz;
    }


      /*------------ VALIDA MODIFICAR PLAZO ------------*/
      function update_plazo(){
        if($this->input->server('REQUEST_METHOD') === 'POST'){
            $this->form_validation->set_rules('pfec_id', 'Fase id ', 'required|trim');

            $pfec_id=$this->security->xss_clean($this->input->post('pfec_id'));
             if ($this->form_validation->run()){
              $f_inicio=$this->security->xss_clean($this->input->post('f_inicio'));
              $f_final=$this->security->xss_clean($this->input->post('f_final'));

              $get_fase=$this->model_faseetapa->get_fase($pfec_id);

                $data_to_store2 = array(
                'pfec_id' => $pfec_id,
                'pfec_fecha_inicio_ddmmaaa' => $get_fase[0]['pfec_fecha_inicio_ddmmaaa'],
                'pfec_fecha_fin_ddmmaaa' => $get_fase[0]['pfec_fecha_fin_ddmmaaa'],
                'num_ip' => $this->input->ip_address(), 
                'nom_ip' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                'fun_id' => $this->fun_id,
                );
                $this->db->insert('historial_proyectofaseetapacomponente', $data_to_store2);



                $update_ptto_plazo = array(  
                'pfec_fecha_inicio_ddmmaaa' => $f_inicio,
                'pfec_fecha_fin_ddmmaaa' => $f_final,
                'estado' => 2,
                'fun_id' => $this->fun_id
                );

                $this->db->where('pfec_id', $pfec_id);
                $this->db->update('_proyectofaseetapacomponente', $update_ptto_plazo);


                $fechas = $this->model_faseetapa->fechas_fase($pfec_id);
                  $update_fase = array(  
                    'pfec_fecha_inicio' => $fechas[0]['inicio'],
                    'pfec_fecha_fin' => $fechas[0]['final']);

                  $this->db->where('pfec_id', $pfec_id);
                  $this->db->update('_proyectofaseetapacomponente', $update_fase);


              for($i=$fechas[0]['inicio'];$i<=$fechas[0]['final'];$i++){
                if(count($this->model_faseetapa->fase_etapa_gestion($pfec_id,$i))==0){
                    $data_to_store2 = array(
                      'pfec_id' => $pfec_id,
                      'g_id' => $i,
                      'fun_id' => $this->fun_id,
                    );
                    $this->db->insert('ptto_fase_gestion', $data_to_store2);
                }
                
              }

              $this->session->set_flashdata('success','DATOS DE LAS FASE SE MODIFICARON CORRECTAMENTE');
              redirect('mod/opciones/1/'.$pfec_id.'/'.$get_fase[0]['proy_id'].'/1');

            }
            else{
              echo "Error";
            }
          }
        else{
          echo "error !!! ";
        }
      }

    /*Historial Plazo -FaseEtapa*/
    public function historial_plazo($pfec_id){
        $tabla='';
        $hfases=$this->model_mod->historial_plazo($pfec_id);
        if (count($hfases)!=0) {
            $tabla.='<table class="table table-bordered">';
                $tabla.='<thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">FECHA INICIAL</th>
                              <th scope="col">FECHA FINAL</th>
                            </tr>
                          </thead>
                          <tbody>';
                        $nro=0;
                        foreach($hfases as $row){
                            $nro++;
                            $tabla.='<tr>
                                <td>'.$nro.'</td>
                                <td>'.date('d/m/Y',strtotime($row['pfec_fecha_inicio_ddmmaaa'])).'</td>
                                <td>'.date('d/m/Y',strtotime($row['pfec_fecha_fin_ddmmaaa'])).'</td>
                            </tr>';
                        }
            $tabla.='</tbody></table>';
        }

        return $tabla;
    }
}