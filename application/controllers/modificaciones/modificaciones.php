<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Modificaciones extends CI_Controller {
  public $rol = array('1' => '3','2' => '4','3' => '5');  
  public function __construct ()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf2');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_actividad');
        $this->load->model('programacion/model_producto');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/model_mantenimiento');
        $this->load->model('mantenimiento/mindicador');
        $this->load->model('ejecucion/model_ejecucion');
        $this->load->model('modificacion/model_modificacion');
        $this->load->model('mantenimiento/model_funcionario');
        $this->load->model('programacion/marco_estrategico/mobjetivos');
        $this->load->model('programacion/prog_poa/mp_terminal');
        $this->load->model('menu_modelo');

        }else{
            redirect('/','refresh');
        }
    }

    /*============================================ ACCIONES APROBADOS ==================================================*/
    public function acciones_aprobadas()
    {
        $enlaces=$this->menu_modelo->get_Modulos(2);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['lista_aper_padres'] = $this->model_proyecto->list_prog();//lista de aperturas padres

        //load the view
        $this->load->view('admin/modificacion/mis_acciones', $data);
    }
    /*====================================================================================================================*/
    /*============================================ PROYECTO A MODIFICAR ==================================================*/
    public function proyecto($id_p)
    {
        $enlaces=$this->menu_modelo->get_Modulos(2);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
        if($data['proyecto'][0]['tp_id']==1) {
            $titulo_proy='PROYECTO DE INVERSI&Oacute;N';
        }
        elseif ($data['proyecto'][0]['tp_id']==2 || $data['proyecto'][0]['tp_id']==3){
            $titulo_proy='PROGRAMA';
        }
        elseif ($data['proyecto'][0]['tp_id']==4) {
            $titulo_proy='ACCI&Oacute;N DE FUNCIONAMIENTO';
        }
        $data['titulo_proy'] = $titulo_proy;
        //load the view
        $this->load->view('admin/modificacion/proyecto/ver_proyecto', $data);
    }
    /*====================================================================================================================*/
   /*========================================= VALIDAR NUEVA FASE ETAPA 1 ================================================================================*/
    public function modificar()
    { 
        if($this->input->server('REQUEST_METHOD') === 'POST') 
        {
            if($this->input->post('meta')=='on'){$meta=1;}else{$meta=0;}
            if($this->input->post('plazo')=='on'){$plazo=1;}else{$plazo=0;}
            if($this->input->post('presupuesto')=='on'){$pr=1;}else{$pr=0;}

            redirect('admin/mod/proyecto_mod/'.$this->input->post('id').'/'.$meta.'/'.$plazo."/".$pr.'');
        }
    }
 /*============================================ PROYECTO A MODIFICAR ==================================================*/
    public function redireccionar_modicacion($id_p,$meta,$plazo,$pr)
    {

            $enlaces=$this->menu_modelo->get_Modulos(2);
            $data['enlaces'] = $enlaces;
            for($i=0;$i<count($enlaces);$i++) 
            {
              $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
            $data['fase'] = $this->model_faseetapa->get_id_fase($id_p);
            
            if($data['proyecto'][0]['tp_id']==1) {
                $titulo_proy='PROYECTO DE INVERSI&Oacute;N';
            }
            elseif ($data['proyecto'][0]['tp_id']==2 || $data['proyecto'][0]['tp_id']==3){
                $titulo_proy='PROGRAMA';
            }
            elseif ($data['proyecto'][0]['tp_id']==4) {
                $titulo_proy='ACCI&Oacute;N DE FUNCIONAMIENTO';
            }
            $data['titulo_proy'] = $titulo_proy;

            $this->load->view('admin/modificacion/proyecto/111', $data);
    
    }

    

    /*====================================================================================================================*/
    /*============================================ MODIFICAR  PRODUCTO ==================================================*/
    public function modificar_producto($id_p,$id_pr,$v1,$v2,$v3)
    {
        $enlaces=$this->menu_modelo->get_Modulos(2);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); 
        $data['producto'] = $this->model_producto->get_producto_id($id_pr);
        $meta_gest=$this->model_producto->meta_prod_gest($id_pr);
        $data['meta_gest']=$meta_gest[0]['meta_gest'];    
        $data['indi']= $this->model_proyecto->indicador(); /// indicador
        if($data['proyecto'][0]['tp_id']==1) {
            $titulo_proy='PROYECTO DE INVERSI&Oacute;N';
        }
        elseif ($data['proyecto'][0]['tp_id']==2 || $data['proyecto'][0]['tp_id']==3){
            $titulo_proy='PROGRAMA';
        }
        elseif ($data['proyecto'][0]['tp_id']==4) {
            $titulo_proy='ACCI&Oacute;N DE FUNCIONAMIENTO';
        }
        $data['titulo_proy'] = $titulo_proy;
        $data['v1'] = $v1;
        $data['v2'] = $v2;
        $data['v3'] = $v3;
        //load the view
        $this->load->view('admin/modificacion/proyecto/producto', $data);
    }
    /*====================================================================================================================*/
    /*========================================= VALIDA LA MODIFICACION DEL PRODUCTO ======================================*/
    public function valida_producto()
    {
        if ($this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('id_p', 'Proyecto', 'required|trim');
            $this->form_validation->set_rules('id_pr', 'Producto', 'required|trim');

            if ($this->form_validation->run())
            {       /*============================= INSERTA AUDI PRODUCTOS ===============================*/
                if(($this->input->post('met_i')!=$this->input->post('met_m')) || ($this->input->post('prod1')!=$this->input->post('prod2')))
                {
                    $data_to_store = array(
                    'prod_id' => $this->input->post('id_pr'),
                    'prod_producto1' => strtoupper($this->input->post('prod1')),
                    'prod_producto2' => strtoupper($this->input->post('prod2')),
                    'prod_meta1' => $this->input->post('met_i'),
                    'prod_gest1' => $this->input->post('mg'),
                    'prod_meta2' => $this->input->post('met_m'),
                    'num_ip' => $this->input->ip_address(), 
                    'nom_ip' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'fun_id' => $this->session->userdata("fun_id"),
                    );
                    $this->db->insert('_prod_audi', $data_to_store);
                    $id_pu=$this->db->insert_id();  
                    /*=====================================================================================*/
                    /*============================= UPDATE PRODUCTOS ===============================*/
                    $update_prod = array(
                        'prod_meta' => $this->input->post('met_m'),
                        'prod_producto' => strtoupper($this->input->post('prod2')),
                        'estado' => '2',
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                    $this->db->where('prod_id', $this->input->post('id_pr'));
                    $this->db->update('_productos', $update_prod);
                    /*==============================================================================*/
                }
                    /*============================= UPDATE PGESTION RODUCTOS ===============================*/
                       $gestion=$this->input->post('gest');
                       $this->model_producto->delete_prod_gest($this->input->post('id_pr'));

                    if ( !empty($_POST["m1"]) && is_array($_POST["m1"]) ) 
                        {
                            foreach ( array_keys($_POST["m1"]) as $como  )
                            {
                                if($_POST["m1"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,1,$_POST["m1"][$como]);
                                }
                                if($_POST["m2"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,2,$_POST["m2"][$como]);
                                }
                                if($_POST["m3"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,3,$_POST["m3"][$como]);
                                }
                                if($_POST["m4"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,4,$_POST["m4"][$como]);
                                }
                                if($_POST["m5"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,5,$_POST["m5"][$como]);
                                }
                                if($_POST["m6"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,6,$_POST["m6"][$como]);
                                }
                                if($_POST["m7"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,7,$_POST["m7"][$como]);
                                }
                                if($_POST["m8"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,8,$_POST["m8"][$como]);
                                }
                                if($_POST["m9"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,9,$_POST["m9"][$como]);
                                }
                                if($_POST["m10"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,10,$_POST["m10"][$como]);
                                }
                                if($_POST["m11"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,11,$_POST["m11"][$como]);
                                }
                                if($_POST["m12"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,12,$_POST["m12"][$como]);
                                }
                                    $gestion++;
                            }
                        }
                    /*============================= END UPDATE PGESTION RODUCTOS ===============================*/
                    $meta_gest_nuevo=$this->model_producto->meta_prod_gest($this->input->post('id_pr'));
                    if($this->input->post('met_i')!=$this->input->post('met_m'))
                        {
                            /*============================= UPDATE AUDI ACTIVIDAD ===============================*/
                            $update_audi_prod = array(
                                'prod_gest2' => $meta_gest_nuevo[0]['meta_gest'],
                            );
                            $this->db->where('ap_id',$id_pu);
                            $this->db->update('_prod_audi', $update_audi_prod);
                            /*==============================================================================*/
                        }
                    redirect('admin/mod/proyecto_mod/'.$this->input->post('id_p').'/'.$this->input->post('v1').'/'.$this->input->post('v2').'/'.$this->input->post('v3').'/'.$this->input->post('id_pr').'/true');
            }
            else
            {
                  redirect('admin/mod/producto/'.$this->input->post('id_p').'/'.$this->input->post('id_pr').'/false');
            }
        }
   }
    /*=========================================================================================================================*/
    /*============================================ MODIFICAR  ACTIVIDAD ==================================================*/
    public function modificar_actividad($id_p,$id_act,$v1,$v2,$v3)
    {
        $enlaces=$this->menu_modelo->get_Modulos(2);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); 
        $data['actividad'] = $this->model_actividad->get_actividad_id($id_act); //// DATOS ACTIVIDAD
        $meta_gest=$this->model_actividad->meta_act_gest($id_act);
        $data['meta_gest']=$meta_gest[0]['meta_gest'];
        $data['indi']= $this->model_proyecto->indicador(); /// indicador
        if($data['proyecto'][0]['tp_id']==1) {
            $titulo_proy='PROYECTO DE INVERSI&Oacute;N';
        }
        elseif ($data['proyecto'][0]['tp_id']==2 || $data['proyecto'][0]['tp_id']==3){
            $titulo_proy='PROGRAMA';
        }
        elseif ($data['proyecto'][0]['tp_id']==4) {
            $titulo_proy='ACCI&Oacute;N DE FUNCIONAMIENTO';
        }
        $data['titulo_proy'] = $titulo_proy;
        $data['v1'] = $v1;
        $data['v2'] = $v2;
        $data['v3'] = $v3;
        //load the view
        $this->load->view('admin/modificacion/proyecto/actividad', $data);
    }
    /*====================================================================================================================*/
    /*========================================= VALIDA LA MODIFICACION DEL PRODUCTO ======================================*/
    public function valida_actividad()
    {
        if ($this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('id_p', 'Proyecto', 'required|trim');
            $this->form_validation->set_rules('id_act', 'Actividad', 'required|trim');

            if ($this->form_validation->run())
            {       /*============================= INSERTA AUDI PRODUCTOS ===============================*/
                if(($this->input->post('met_i')!=$this->input->post('met_m'))|| ($this->input->post('act1')!=$this->input->post('act2')))
                {
                    $data_to_store = array(
                    'act_id' => $this->input->post('id_act'),
                    'act_actividad1' => strtoupper($this->input->post('act1')),
                    'act_actividad2' => strtoupper($this->input->post('act2')),
                    'act_meta1' => $this->input->post('met_i'),
                    'act_gest1' => $this->input->post('mg'),
                    'act_meta2' => $this->input->post('met_m'),
                    'num_ip' => $this->input->ip_address(), 
                    'nom_ip' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'fun_id' => $this->session->userdata("fun_id"),
                    );
                    $this->db->insert('_act_audi', $data_to_store);
                    $id_au=$this->db->insert_id(); 
                    /*=====================================================================================*/
                    /*============================= UPDATE PRODUCTOS ===============================*/
                    $update_act = array(
                        'act_meta' => $this->input->post('met_m'),
                        'act_actividad' => strtoupper($this->input->post('act2')),
                        'estado' => '2',
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                    $this->db->where('act_id', $this->input->post('id_act'));
                    $this->db->update('_actividades', $update_act);
                    /*==============================================================================*/
                }
                    /*============================= UPDATE PGESTION RODUCTOS ===============================*/
                       $gestion=$this->input->post('gest');
                       $this->model_actividad->delete_act_gest($this->input->post('id_act'));

                        if ( !empty($_POST["m1"]) && is_array($_POST["m1"]) ) 
                        {
                            foreach ( array_keys($_POST["m1"]) as $como  )
                            {
                                if($_POST["m1"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_act'),$gestion,1,$_POST["m1"][$como],($_POST["m1"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m2"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_act'),$gestion,2,$_POST["m2"][$como],($_POST["m2"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m3"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_act'),$gestion,3,$_POST["m3"][$como],($_POST["m3"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m4"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_act'),$gestion,4,$_POST["m4"][$como],($_POST["m4"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m5"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_act'),$gestion,5,$_POST["m5"][$como],($_POST["m5"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m6"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_act'),$gestion,6,$_POST["m6"][$como],($_POST["m6"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m7"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_act'),$gestion,7,$_POST["m7"][$como],($_POST["m7"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m8"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_act'),$gestion,8,$_POST["m8"][$como],($_POST["m8"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m9"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_act'),$gestion,9,$_POST["m9"][$como],($_POST["m9"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m10"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_act'),$gestion,10,$_POST["m10"][$como],($_POST["m10"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m11"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_act'),$gestion,11,$_POST["m11"][$como],($_POST["m11"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m12"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_act'),$gestion,12,$_POST["m12"][$como],($_POST["m12"][$como]*$this->input->post('cost_uni')));
                                }
                                
                                $gestion++;
                            }
                        }
                    /*============================= END UPDATE PGESTION RODUCTOS ===============================*/
                    $meta_gest_nuevo=$this->model_actividad->meta_act_gest($this->input->post('id_act'));
                     if($this->input->post('met_i')!=$this->input->post('met_m'))
                        {
                            /*============================= UPDATE AUDI ACTIVIDAD ===============================*/
                            $update_audi_act = array(
                                'act_gest2' => $meta_gest_nuevo[0]['meta_gest'],
                            );
                            $this->db->where('ap_id',$id_au);
                            $this->db->update('_act_audi', $update_audi_act);
                            /*==============================================================================*/
                        }
                    redirect('admin/mod/proyecto_mod/'.$this->input->post('id_p').'/'.$this->input->post('v1').'/'.$this->input->post('v2').'/'.$this->input->post('v3').'/'.$this->input->post('id_act').'/true');
            }
            else
            {
                  redirect('admin/mod/actividad/'.$this->input->post('id_p').'/'.$this->input->post('id_act').'/false');
            }
        }
   }
    /*=========================================================================================================================*/
    //============================================= VALIDA MODIFICACION DEL PLAZO================================================
    function valida_plazo(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('id_f', 'Id fase ', 'required|trim');
            $this->form_validation->set_rules('fi1', 'Fecha Inicial 1', 'required|trim');
            $this->form_validation->set_rules('fi2', 'Fecha Inicial 2', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');

            if ($this->form_validation->run() ) {
                $id_p=  $this->input->post('id_p');
                $id_f=  $this->input->post('id_f');
                $fi1=  $this->input->post('fi1');
                $ff1=  $this->input->post('ff1');
                $fi2 =  $this->input->post('fi2');
                $ff2 =  $this->input->post('ff2');
                //=================enviar  evitar codigo malicioso ==========
                $id_p = $this->security->xss_clean($id_p);
                $id_f = $this->security->xss_clean($id_f);
                $fi1 = $this->security->xss_clean($fi1);
                $ff1 = $this->security->xss_clean($ff1);
                $fi2 = $this->security->xss_clean($fi2);
                $ff2 = $this->security->xss_clean($ff2);
                //======================= MODIFICAR=
                
                    $data = array(
                        'pfec_id' => $id_f,
                        'fecha_ini1' => $fi1,
                        'fecha_ini2' => $fi2,
                        'fecha_fin1' => $ff1,
                        'fecha_fin2' => $ff2,
                        'num_ip' => $this->input->ip_address(), 
                        'nom_ip' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                    $this->db->insert('_fe_audi',$data);

                    
                    /*============================= UPDATE FASE ETAPA ===============================*/
                    $update_fase= array(
                        'pfec_fecha_inicio_ddmmaaa' => $fi2,
                        'pfec_fecha_fin_ddmmaaa' => $ff2,
                    );
                    $this->db->where('pfec_id', $id_f);
                    $this->db->update('_proyectofaseetapacomponente', $update_fase);
                    /*==============================================================================*/
                    $datos = $this->model_faseetapa->datos_fase_etapa($id_f,$id_p);
                    /*============================= UPDATE FASE ETAPA ===============================*/
                    $update_fase= array(
                        'pfec_fecha_inicio' => $datos[0]['actual'],
                        'pfec_fecha_fin' => $datos[0]['final'],
                        'estado' => '2',
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                    $this->db->where('pfec_id', $id_f);
                    $this->db->update('_proyectofaseetapacomponente', $update_fase);
                    /*==============================================================================*/

                
            } else {
                echo'DATOS ERRONEOS';
            }
        }else{
            show_404();
        }
    }
   /*==================================================================================================================================*/
    /*============================================ MODIFICAR  PRESUPUESTO ==================================================*/
    public function modificar_presupuesto($id_p,$v1,$v2,$v3)
    {
        $enlaces=$this->menu_modelo->get_Modulos(2);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
        $data['fase_proyecto'] = $this->model_faseetapa->get_id_fase($id_p);
        $datos = $this->model_faseetapa->datos_fase_etapa($data['fase_proyecto'][0]['id'],$id_p);  // desvuelve las fechas del proyecto
        $data['años']=$datos[0]['final']-$datos[0]['actual']+1;
        $data['fecha']=$datos[0]['actual']; 
        $años=$datos[0]['final']-$datos[0]['actual']+1;
        $data['gestiones']=$años;
        $fecha=$datos[0]['actual'];
        $data['fecha']=$fecha;
        
        if($data['proyecto'][0]['tp_id']==1) {
            $titulo_proy='PROYECTO DE INVERSI&Oacute;N';
        }
        elseif ($data['proyecto'][0]['tp_id']==2 || $data['proyecto'][0]['tp_id']==3){
            $titulo_proy='PROGRAMA';
        }
        elseif ($data['proyecto'][0]['tp_id']==4) {
            $titulo_proy='ACCI&Oacute;N DE FUNCIONAMIENTO';
        }
        $data['titulo_proy'] = $titulo_proy;
        if($años>=1)
          {
            $data['fase_gestion1']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha);
            if($años>=2)
            {
              $data['fase_gestion2']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+1);
              if($años>=3)
              {
                $data['fase_gestion3']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+2);
                if($años>=4)
                {
                  $data['fase_gestion4']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+3);
                  if($años>=5)
                  {
                    $data['fase_gestion5']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+4);
                    if($años>=6)
                    {
                      $data['fase_gestion6']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+5);
                      if($años>=7)
                      {
                        $data['fase_gestion7']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+6);
                        if($años>=8)
                          {
                            $data['fase_gestion8']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+7);
                            if($años>=9)
                            {
                                $data['fase_gestion9']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+8);
                                if($años>=10)
                                {
                                    $data['fase_gestion10']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+9);
                                    if($años>=11)
                                    {
                                        $data['fase_gestion11']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+10);
                                        if($años>=12)
                                        {
                                            $data['fase_gestion12']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+11);
                                            if($años>=13)
                                            {
                                                $data['fase_gestion13']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+12);
                                                if($años>=14)
                                                {
                                                    $data['fase_gestion14']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+13);
                                                    if($años>=15)
                                                    {
                                                    $data['fase_gestion15']=$this->model_faseetapa->fase_etapa_gestion($data['fase_proyecto'][0]['id'],$fecha+15);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                          }
                      }
                    }
                  }
                }
              }
            }
          }
        $data['v1'] = $v1;
        $data['v2'] = $v2;
        $data['v3'] = $v3;
        //load the view
        $this->load->view('admin/modificacion/proyecto/presupuesto', $data);
    }
    /*====================================================================================================================*/
    /*======================================== VALIDAR MODIFICACION DEL PRESUPUESTO =================================================================================*/
  function valida_presupuesto()
  {
         if($this->input->server('REQUEST_METHOD') === 'POST') 
          {
            $this->form_validation->set_rules('id_f', 'Id de la fase', 'required|trim');
            $this->form_validation->set_rules('id_p', 'Id del Proyecto', 'required|trim');
            
            $variacion=$this->input->post('ppt')-$this->input->post('ppt1');
            if($variacion<0){$variacion=$variacion*-1;}
                    
                    $data_to_store = array(
                    'pfec_id' => $this->input->post('id_f'),
                    'pfec_ptto_fase1' => $this->input->post('ppt1'),
                    'pfec_variacion' => $variacion,
                    'pfec_ptto_fase2' => $this->input->post('ppt'),
                    'num_ip' => $this->input->ip_address(), 
                    'nom_ip' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'fun_id' => $this->session->userdata("fun_id"),
                    );
                    $this->db->insert('_ptto_audi', $data_to_store);
                    /*=====================================================================================*/

                    $update_fasee = array('pfec_ptto_fase' => $this->input->post('ppt'));

                    $this->db->where('pfec_id', $this->input->post('id_f'));
                    $this->db->where('proy_id', $this->input->post('id_p'));
                    $this->db->update('_proyectofaseetapacomponente', $update_fasee);

            if ($this->form_validation->run())
            {
                  if($this->input->post('gest')>=1)
                  {
                    $update_fase = array('pfecg_ppto_total' => $this->input->post('pt1'),
                                         'estado' => 2,
                                         'fun_id' => $this->session->userdata("fun_id"));

                        $this->db->where('g_id', $this->input->post('gestion'));
                        $this->db->where('pfec_id', $this->input->post('id_f'));
                        $this->db->update('ptto_fase_gestion', $update_fase);

                      if($this->input->post('gest')>=2)
                      {
                        $update_fase = array('pfecg_ppto_total' => $this->input->post('pt2'),
                                             'estado' => 2,
                                             'fun_id' => $this->session->userdata("fun_id"));

                            $this->db->where('g_id', $this->input->post('gestion')+1);
                            $this->db->where('pfec_id', $this->input->post('id_f'));
                            $this->db->update('ptto_fase_gestion', $update_fase);

                          if($this->input->post('gest')>=3)
                          {
                            $update_fase = array('pfecg_ppto_total' => $this->input->post('pt3'),
                                                 'estado' => 2,
                                                 'fun_id' => $this->session->userdata("fun_id"));

                                $this->db->where('g_id', $this->input->post('gestion')+2);
                                $this->db->where('pfec_id', $this->input->post('id_f'));
                                $this->db->update('ptto_fase_gestion', $update_fase);


                              if($this->input->post('gest')>=4)
                              {
                                $update_fase = array('pfecg_ppto_total' => $this->input->post('pt4'),
                                                     'estado' => 2,
                                                     'fun_id' => $this->session->userdata("fun_id"));

                                    $this->db->where('g_id', $this->input->post('gestion')+3);
                                    $this->db->where('pfec_id', $this->input->post('id_f'));
                                    $this->db->update('ptto_fase_gestion', $update_fase);

                                  if($this->input->post('gest')>=5)
                                  {
                                    $update_fase = array('pfecg_ppto_total' => $this->input->post('pt5'),
                                                         'estado' => 2,
                                                         'fun_id' => $this->session->userdata("fun_id"));

                                        $this->db->where('g_id', $this->input->post('gestion')+4);
                                        $this->db->where('pfec_id', $this->input->post('id_f'));
                                        $this->db->update('ptto_fase_gestion', $update_fase);

                                      if($this->input->post('gest')>=6)
                                      {
                                        $update_fase = array('pfecg_ppto_total' => $this->input->post('pt6'),
                                                             'estado' => 2,
                                                             'fun_id' => $this->session->userdata("fun_id"));

                                            $this->db->where('g_id', $this->input->post('gestion')+5);
                                            $this->db->where('pfec_id', $this->input->post('id_f'));
                                            $this->db->update('ptto_fase_gestion', $update_fase);

                                            if($this->input->post('gest')>=7)
                                            {
                                              $update_fase = array('pfecg_ppto_total' => $this->input->post('pt7'),
                                                                   'estado' => 2,
                                                                   'fun_id' => $this->session->userdata("fun_id"));

                                                  $this->db->where('g_id', $this->input->post('gestion')+6);
                                                  $this->db->where('pfec_id', $this->input->post('id_f'));
                                                  $this->db->update('ptto_fase_gestion', $update_fase);

                                                  if($this->input->post('gest')>=8)
                                                  {
                                                    $update_fase = array('pfecg_ppto_total' => $this->input->post('pt8'),
                                                                         'estado' => 2,
                                                                         'fun_id' => $this->session->userdata("fun_id"));

                                                        $this->db->where('g_id', $this->input->post('gestion')+7);
                                                        $this->db->where('pfec_id', $this->input->post('id_f'));
                                                        $this->db->update('ptto_fase_gestion', $update_fase);

                                                        if($this->input->post('gest')>=9)
                                                          {
                                                            $update_fase = array('pfecg_ppto_total' => $this->input->post('pt9'),
                                                                                 'estado' => 2,
                                                                                 'fun_id' => $this->session->userdata("fun_id"));

                                                                $this->db->where('g_id', $this->input->post('gestion')+8);
                                                                $this->db->where('pfec_id', $this->input->post('id_f'));
                                                                $this->db->update('ptto_fase_gestion', $update_fase);
                                                          }
                                                  }
                                            }
                                      }
                                  }
                              }
                          }
                      }
                  }
           
                redirect('admin/mod/proyecto_mod/'.$this->input->post('id_p').'/'.$this->input->post('v1').'/'.$this->input->post('v2').'/'.$this->input->post('v3').'/true');
                
            }
            else
            {  
               redirect('admin/mod/presupuesto/'.$this->input->post('id_p').'/'.$this->input->post('v1').'/'.$this->input->post('v2').'/'.$this->input->post('v3').'/false');
                
            }
        }
  }
/*=========================================================================================================================*/
/*========================================= TECHO PRESUPUESTARIO DE LA FASE ==============================================================*/
  public function modificar_techo_presupuesto($id_p,$v1,$v2,$v3) 
  {
    $enlaces=$this->menu_modelo->get_Modulos(2);
    $data['enlaces'] = $enlaces;
    for($i=0;$i<count($enlaces);$i++) 
    {
        $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
    $data['fase'] = $this->model_faseetapa->get_id_fase($id_p); ///// datos fase encendida

    $data['fase_gest'] = $this->model_faseetapa->fase_gestion($data['fase'][0]['id'],$this->session->userdata("gestion")); //// fase presupuesto de la gestion actual
    
    $data['nro'] = $this->model_faseetapa->verif_presupuesto_activo($data['fase_gest'][0]['ptofecg_id']);
    $data['techo']=$this->model_faseetapa->techo_presupuestario($data['fase_gest'][0]['ptofecg_id']);

    $data['fase_asig'] = $this->model_faseetapa->fase_presupuesto_id($data['fase_gest'][0]['ptofecg_id']); //// lista del presupuesto asignado
    $data['ffi'] = $this->model_faseetapa->fuentefinanciamiento(); ///// fuente financiamiento
    $data['fof'] = $this->model_faseetapa->organismofinanciador(); ///// organismo financiador

    if($data['proyecto'][0]['tp_id']==1) {
        $titulo_proy='PROYECTO DE INVERSI&Oacute;N';
    }
    elseif ($data['proyecto'][0]['tp_id']==2 || $data['proyecto'][0]['tp_id']==3){
        $titulo_proy='PROGRAMA';
    }
    elseif ($data['proyecto'][0]['tp_id']==4) {
        $titulo_proy='ACCI&Oacute;N DE FUNCIONAMIENTO';
    }
    $data['titulo_proy'] = $titulo_proy;

    $data['v1'] = $v1;
    $data['v2'] = $v2;
    $data['v3'] = $v3;
        //load the view
    $this->load->view('admin/modificacion/proyecto/techo_presupuesto', $data);
  }
/*===============================================================================================================================================*/
  //=========================== VALIDA UN NUEVO TECHO PRESUPUESTARIO =============================
    function validar_techo_ptto(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('id_fg', 'id fase gestion', 'required|trim');
            $this->form_validation->set_rules('ff_id', 'ff_id', 'required|trim|integer');
            $this->form_validation->set_rules('of_id', 'of_id', 'required|trim|integer');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            $this->form_validation->set_message('integer', 'El campo  debe poseer solo numeros enteros');
            
            if ($this->form_validation->run() ) {
                
                
                $inicial=  $this->input->post('inicial');
                $vigente=  $this->input->post('vigente');

                $id_fg=  $this->input->post('id_fg');
                $ff_id=  $this->input->post('ff_id');
                $of_id =  $this->input->post('of_id');
                $ffofet_monto =  $this->input->post('ffofet_monto');
                //=================enviar  evitar codigo malicioso ==========
                $inicial = $this->security->xss_clean($inicial);
                $vigente = $this->security->xss_clean($vigente);

                $id_fg = $this->security->xss_clean($id_fg);
                $ff_id = $this->security->xss_clean($ff_id);
                $of_id = $this->security->xss_clean($of_id);
                $ffofet_monto = $this->security->xss_clean($ffofet_monto);
                //======================= MODIFICAR=

                $variacion=$vigente-$inicial;
                if($variacion<0){$variacion=$variacion*-1;}

                    $data = array(
                        'ptofecg_id' => $id_fg,
                        'ff_id' => $ff_id,
                        'of_id' =>$of_id,
                        'ffofet_monto' =>$ffofet_monto,
                    );
                    $this->db->insert('_ffofet',$data);
                    $id=$this->db->insert_id(); 

                    $data_to_store = array(
                    'ffofet_id' => $id,
                    'ptofecg_id' => $id_fg,
                    'ffofet_monto1' => $ffofet_monto,
                    'ffofet_monto2' => $ffofet_monto,
                    'num_ip' => $this->input->ip_address(), 
                    'nom_ip' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'fun_id' => $this->session->userdata("fun_id"),

                    'ptto_inicial' => $inicial,
                    'ptto_dif' => $variacion,
                    'ptto_vigente' => $vigente,
                    );
                    $this->db->insert('_ptto_techo_audi', $data_to_store);

                
            } else {
                echo'DATOS ERRONEOS';
            }
        }else{
            show_404();
        }
    }
/*====================================================================================================================*/
/*===================================== UPDATE LOS DATOS DEL TECHO PRESUPUESTO =======================================*/
      function update_techo_ptto()
      {
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('ffofet_id', 'id de la meta', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            /*if ($this->form_validation->run() ) {*/
                $post = $this->input->post();
                $inicial = $post['inicial'];
                $vigente = $post['vigente'];
                $id_fg=  $this->input->post('id_fg');

                $ffofet_id = $post['ffofet_id'];
                $ff_id = $post['ff_id'];
                $of_id = $post['of_id'];
                $valor_anterior = $post['valor_anterior'];
                $ffofet_monto = $post['ffofet_monto'];
                //================ evitar enviar codigo malicioso ==========
                $inicial= $this->security->xss_clean($inicial);
                $vigente= $this->security->xss_clean($vigente);

                $id_fg= $this->security->xss_clean($id_fg);
                $ffofet_id= $this->security->xss_clean($ffofet_id);
                $ff_id= $this->security->xss_clean($ff_id);
                $of_id= $this->security->xss_clean($of_id);
                $valor_anterior= $this->security->xss_clean($valor_anterior);
                $ffofet_monto= $this->security->xss_clean($ffofet_monto);
                
                $variacion=$ffofet_monto-$valor_anterior;
                if($variacion<0){$variacion=$variacion*-1;}

                $variacion_ptto=$vigente-$inicial;
                if($variacion_ptto<0){$variacion_ptto=$variacion_ptto*-1;}

                $data_to_store = array(
                    'ffofet_id' => $ffofet_id,
                    'ptofecg_id' => $id_fg,
                    'ffofet_monto1' => $valor_anterior,
                    'ffofet_variacion' => $variacion,
                    'ffofet_monto2' => $ffofet_monto,
                    'num_ip' => $this->input->ip_address(), 
                    'nom_ip' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'fun_id' => $this->session->userdata("fun_id"),
                    'ptto_inicial' => $inicial,
                    'ptto_dif' => $variacion_ptto,
                    'ptto_vigente' => $vigente,
                    );
                $this->db->insert('_ptto_techo_audi', $data_to_store);

                $update_techo = array(
                        'ff_id' => $ff_id,
                        'of_id' => $of_id,
                        'ffofet_monto' => $ffofet_monto,
                    );
                $this->db->where('ffofet_id', $ffofet_id);
                $this->db->update('_ffofet', $update_techo);
        }else{
            show_404();
        }
    }
    /*====================================================================================================================*/

    /*============================================ OBJETIVOS DE GESTION Y PRODUCTOS TERMINALES ==================================*/
    public function cprog_red_objetivos() 
      {
        $enlaces=$this->menu_modelo->get_Modulos(2);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        // $data['lista_poa'] = $this->model_modificacion->lista_poa();
        // $this->load->view('admin/modificacion/mis_objetivos', $data);
        $data['lista_objetivos'] = $this->mobjetivos->lista_obje_poa_total();
        $data['poa_id']=397;
        $this->load->view('admin/modificacion/objetivo/list_obj', $data);

      }

    /*============================================ LISTA DE OBJETIVOS ESTRATEGICOS ==================================================*/
    public function objetivos_estrategicos($poa_id) 
    {
        $enlaces=$this->menu_modelo->get_Modulos(2);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++) 
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['poa'] = $this->model_modificacion->poa_id($poa_id);
        $data['lista_objetivos'] = $this->mobjetivos->lista_obje_poa_total();
        $data['poa_id']=$poa_id;
// var_dump($data['lista_objetivos']);  die;
        $this->load->view('admin/modificacion/objetivo/list_obj', $data);
    }

    //LISTA DE MODIFICACIONES
    function lista_mes_ejec($o_id)
    {
        
        $enlaces=$this->menu_modelo->get_Modulos(2);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++) 
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['o_id'] = $o_id;
        $lista = $this->model_modificacion->get_modificacion_xobjetivo($o_id);
        $dto_obje = $this->model_modificacion->get_ogestion($o_id);
        $data['obje'] = $dto_obje[0];
               
        $tabla = '';
        $cont = 1;
        $ruta_reg = site_url("") . '/admin/mod/ver_mod/' . $o_id . '/' . $o_id . '/';//TODO Ver si los registros son los mismos
        $ruta_img_reg = base_url() . 'assets/ifinal/modificar.png';//ruta imagen registro
        $ruta_img_arc = base_url() . 'assets/ifinal/doc.jpg';//ruta imagen ARCHIVOS
        $ruta_arc = site_url("") . '/reg/list_arc/';//ruta de archivos
        $ruta_img_rev = base_url() . 'assets/ifinal/rever.png';
        $ruta_img_no_rev = base_url() . 'assets/ifinal/4.png';//para el caso de no revertir
        foreach ($lista AS $row) {
            $tabla .= '<tr>';
            $tabla .= '<td>' . $cont . '</td>';
            $tabla .= '<td>';
            $tabla .= '<a href="' . $ruta_reg . $row['objetivosgestion_mod_id'] . '" title="MODIFICACION">
                            <img src="' . $ruta_img_reg . '" width="30" height="30" class="img-responsive" title="REGISTRO DE MODIFICACION">
                       </a>';
            $tabla .= '</td>';
            // $tabla .= '<td>';
            // $tabla .= '<a href="' . $ruta_arc . $poa_id . '/' . $aper_id . '/' . $row['reg_id'] . '/' . $row['reg_mes'] . '" title="LISTA DE ARCHIVOS">
            //                 <img src="' . $ruta_img_arc . '" width="30" height="30" class="img-responsive" title="LISTA DE ARCHIVOS">
            //            </a>';
            // $tabla .= '</td>';
            //SEGUNDA COLUMNA COMENTADA
            // $tabla .= '<td>';
            // if ($row['reg_validar'] == 2) {
            //     $tabla .= '<a title="NO PUEDE REVERTIR EL REGISTRO" >
            //                 <img src="' . $ruta_img_no_rev . '" width="30" height="30" class="img-responsive" title="NO PUEDE REVERTIR EL REGISTRO">
            //            </a>';
            // } else {
            //     $tabla .= '<a title="REVERTIR REGISTRO DE EJECUCIÓN" id="' . $row['reg_mes'] . '" name="' . $row['reg_id'] . '" class="revertir">
            //                 <img src="' . $ruta_img_rev . '" width="30" height="30" class="img-responsive" title="REVERTIR REGISTRO DE EJECUCIÓN">
            //            </a>';
            // }
            // $tabla .= '</td>';

            $tabla .= '<td>' . $row['o_codigo'] . '</td>';
            $tabla .= '<td>' . $row['o_objetivo'] . '</td>';
            $tabla .= '<td>' . $row['indi_id'] . '</td>';
            $tabla .= '<td>' . $row['o_indicador'] . '</td>';
            $tabla .= '<td>' . $row['o_indicador'] . '</td>';
            // $tabla .= '<td>' . $row['fun_nombre'] . $row['fun_paterno'] . $row['fun_materno'] . '</td>';
            $tabla .= '<td>' . date('d-m-Y', strtotime($row['fecha_mod'])) . '</td>';
            $tabla .= '<td>' . $row['o_estado'] . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        $data['tabla_ejec_mes'] = $tabla;
        // $data['reg_ejec_programa_js'] = '<SCRIPT src="' . base_url() . 'mis_js/registro_ejecucion/reg_ejec_programa.js" type="text/javascript"></SCRIPT>';
        $ruta = 'registro_ejec/ejec_ogestion_pterminal/vlista_meses_ejec';
        // $this->construir_vista($ruta, $data);
        $this->load->view('admin/modificacion/objetivo/vlista_modificacion', $data);
    }

    /*============================================ VER MODIFICACION OBJETIVOS DE GESTION Y PRODUCTOS TERMINALES ==================================*/

    /*===================================== VISTA MODIFICAR OBJETIVO DE GESTION ===============================================================*/
    public function ver_modificar_objetivo($poa_id,$obje_id,$o_id)
    {
        
        $enlaces=$this->menu_modelo->get_Modulos(2);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++) 
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        // $data['poa'] = $this->model_modificacion->poa_id($poa_id);
        $data['obje'] = $this->model_modificacion->get_modificado($o_id);
        $data['programado'] = $this->model_modificacion->obj_prog_mensual($o_id);
        $data['list_indicador'] = $this->mindicador->get_indicador();
// var_dump($data['obje']);  die;
        $data['list_funcionario'] = $this->model_funcionario->get_funcionarios();
        // $data['poa_id']=$poa_id;
        $data['obje_id']=$obje_id;
        $data['o_id']=$o_id;

        $this->load->view('admin/modificacion/objetivo/objetivo_gestion_mod', $data);
    }

    /*============================================ LISTA DE OBJETIVOS DE GESTION Y PRODUCTOS TERMINALES ==================================*/

    /*===================================== MODIFICAR OBJETIVO DE GESTION ===============================================================*/
    public function modificar_objetivo_gestion($poa_id,$obje_id,$o_id)
    {
        
        $enlaces=$this->menu_modelo->get_Modulos(2);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++) 
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['poa'] = $this->model_modificacion->poa_id($poa_id);
        $data['obje'] = $this->model_modificacion->get_ogestion($o_id);
        $data['programado'] = $this->model_modificacion->obj_prog_mensual($o_id);
        $data['list_indicador'] = $this->mindicador->get_indicador();
// var_dump($data['obje']);  die;
        $data['list_funcionario'] = $this->model_funcionario->get_funcionarios();
        $data['poa_id']=$poa_id;
        $data['obje_id']=$obje_id;
        $data['o_id']=$o_id;

        $this->load->view('admin/modificacion/objetivo/objetivo_gestion', $data);
    }
    /*====================================================================================================================*/
    /*========================================= VALIDA MODIFICADO OBJETIVO DE GESTION ====================================*/
    function valida_obj_gestion()
    { 
         if($this->input->server('REQUEST_METHOD') === 'POST') 
          {
            $this->form_validation->set_rules('poa_id', 'poa_id', 'required|trim');
            $this->form_validation->set_rules('obje_id', 'obje_id', 'required|trim');
            $this->form_validation->set_rules('o_id', 'o_id', 'required|trim');

            
            // var_dump($data_copy); die;

            if ($this->form_validation->run())
            {
                  // var_dump($this->input->post());  die;
                   $data_copy = $this->model_modificacion->modifica_ogestion($this->input->post('o_id'),$this->input->post('obje_id'),$this->input->post('sw1'));
                  $data_to_store = array( ///// Tabla _og_audi
                    'o_id' => $this->input->post('o_id'),
                    'obje_objetivo1' => strtoupper($this->input->post('obj1')),
                    'obje_objetivo2' => strtoupper($this->input->post('obj2')),
                    'og_meta1' => $this->input->post('met_i'),
                    'og_meta2' => $this->input->post('met_m'),
                    'num_ip' => $this->input->ip_address(), 
                    'nom_ip' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'fun_id' => $this->session->userdata("fun_id"),
                );
                $this->db->insert('_og_audi', $data_to_store); ///// inserta a _og_audi

                /*============================= UPDATE OG ===============================*/
                $update_og = array(
                    'o_meta' => $this->input->post('met_m'),
                    'o_objetivo' => strtoupper($this->input->post('obj2')),
                    'o_estado' => '2',
                    'fecha_mod' => date("Y-m-d H:i:s"),
                    'fun_id' => $this->input->post('funcionario'),
                    'indi_id' => $this->input->post('tipo_indicador'),
                    'o_indicador' => $this->input->post('oindicador'),
                    'o_linea_base' => $this->input->post('olineabase'),
                    'o_ponderacion' => $this->input->post('oponderacion'),
                    'o_fuente_verificacion' => $this->input->post('ofuenteverificacion'),
                    'o_supuestos' => $this->input->post('osupuesto'),
                    'justifica_m' => $this->input->post('justifica_m'),
                    'norma_m' => $this->input->post('norma_m'),
                );
                $this->db->where('o_id', $this->input->post('o_id'));
                $this->db->update('objetivosgestion', $update_og);
                /*==============================================================================*/

                $this->model_modificacion->delete_og_prog($this->input->post('o_id'));

                if($this->input->post('m1')!=0)
                {
                    $this->model_modificacion->add_og_prog($this->input->post('o_id'),1,$this->input->post('m1'));
                }
                if($this->input->post('m2')!=0)
                {
                    $this->model_modificacion->add_og_prog($this->input->post('o_id'),2,$this->input->post('m2'));
                }
                if($this->input->post('m3')!=0)
                {
                    $this->model_modificacion->add_og_prog($this->input->post('o_id'),3,$this->input->post('m3'));
                }
                if($this->input->post('m4')!=0)
                {
                    $this->model_modificacion->add_og_prog($this->input->post('o_id'),4,$this->input->post('m4'));
                }
                if($this->input->post('m5')!=0)
                {
                    $this->model_modificacion->add_og_prog($this->input->post('o_id'),5,$this->input->post('m5'));
                }
                if($this->input->post('m6')!=0)
                {
                    $this->model_modificacion->add_og_prog($this->input->post('o_id'),6,$this->input->post('m6'));
                }
                if($this->input->post('m7')!=0)
                {
                    $this->model_modificacion->add_og_prog($this->input->post('o_id'),7,$this->input->post('m7'));
                }
                if($this->input->post('m8')!=0)
                {
                    $this->model_modificacion->add_og_prog($this->input->post('o_id'),8,$this->input->post('m8'));
                }
                if($this->input->post('m9')!=0)
                {
                    $this->model_modificacion->add_og_prog($this->input->post('o_id'),9,$this->input->post('m9'));
                }
                if($this->input->post('m10')!=0)
                {
                    $this->model_modificacion->add_og_prog($this->input->post('o_id'),10,$this->input->post('m10'));
                }
                if($this->input->post('m11')!=0)
                {
                    $this->model_modificacion->add_og_prog($this->input->post('o_id'),11,$this->input->post('m11'));
                }
                if($this->input->post('m12')!=0)
                {
                    $this->model_modificacion->add_og_prog($this->input->post('o_id'),12,$this->input->post('m12'));
                }
                $post = $this->input->post();
                //--- SUBIR ARCHIVO
                if ($_FILES["userfile"]["size"] == 0) {
                    $peticion = true;
                } else {
                    $filename = $_FILES["userfile"]["name"];
                    $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
                    $file_ext = substr($filename, strripos($filename, '.')); // get file name
                    $filesize = $_FILES["userfile"]["size"];
                    $allowed_file_types = array('.jpg', '.doc', '.pdf', '.png', '.JPEG','.xlsx','.docx','.odt','.ods');
                    if (in_array($file_ext, $allowed_file_types) && ($filesize < 80000000)) { // Rename file
                        $newfilename = 'MO-2019-' . substr(md5(uniqid(rand())), 0, 5) . $file_ext;
                        //--------------------------------$data_to_store['pa_ruta_archivo'] = "" . $newfilename;
                        if (file_exists("archivos/modificacion/" . $newfilename)) {
                            echo "<script>alert('Ya existe este archivo')</script>";
                        } else {
                            move_uploaded_file($_FILES["userfile"]["tmp_name"], "archivos/modificacion/" . $newfilename);
                            //guardar mi area urbana despues de las validaciones
                            $data['pa_ruta_archivo'] = $newfilename;
                            $data['o_id'] = $this->input->post('o_id');
                            $data['pa_nombre'] = $post['nombre_archivo'];
                            $peticion = $this->db->INSERT('mod_ogestion_adjuntos', $data);
                        }
                    } elseif (empty($file_basename)) {
                        echo "Selecciona un archivo para cargarlo.";
                    } elseif ($filesize > 100000000) {
                        //redirect('');
                    } else {
                        $mensaje = "Sólo estos tipos de archivo se permiten para la carga: " . implode(', ', $allowed_file_types);
                        echo '<script>alert("' . $mensaje . '")</script>';
                        // unlink($_FILES["userfile"]["tmp_name"]);
                    }
                }

                redirect('admin/mod/objetivo/'.$this->input->post('poa_id').'/'.$this->input->post('o_id').'/true');
            }
            else
            {   
                redirect('admin/mod/objetivo/'.$this->input->post('poa_id').'/'.$this->input->post('o_id').'/false');
            }
        }
    }
    /*=========================================================================================================================*/
    /*===================================== MODIFICAR PRODUCTO TERMINAL DE GESTION ===============================================================*/
    public function modificar_producto_terminal($poa_id,$obje_id,$pt_id)
    {
        $enlaces=$this->menu_modelo->get_Modulos(2);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++) 
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['poa'] = $this->model_modificacion->poa_id($poa_id);
        $data['pt'] = $this->model_modificacion->get_pterminal($pt_id);
        $data['programado'] = $this->model_modificacion->pt_prog_mensual($pt_id);

        $data['poa_id']=$poa_id;
        $data['obje_id']=$obje_id;
        $data['pt_id']=$pt_id;

        $this->load->view('admin/modificacion/objetivo/producto_terminal', $data);
    }
    /*====================================================================================================================*/
    /*========================================= VALIDA MODIFICADO PRODUCTO TERMINAL ====================================*/
    public function valida_pt()
    { 
         if($this->input->server('REQUEST_METHOD') === 'POST') 
          {
            $this->form_validation->set_rules('poa_id', 'poa_id', 'required|trim');
            $this->form_validation->set_rules('obje_id', 'obje_id', 'required|trim');
            $this->form_validation->set_rules('pt_id', 'pt_id', 'required|trim');

            if ($this->form_validation->run())
            {
                  $data_to_store = array( ///// Tabla _og_audi
                    'pt_id' => $this->input->post('pt_id'),
                    'pt_objetivo1' => strtoupper($this->input->post('pt1')),
                    'pt_objetivo2' => strtoupper($this->input->post('pt2')),
                    'pt_meta1' => $this->input->post('met_i'),
                    'pt_meta2' => $this->input->post('met_m'),
                    'num_ip' => $this->input->ip_address(), 
                    'nom_ip' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'fun_id' => $this->session->userdata("fun_id"),
                );
                $this->db->insert('_pt_audi', $data_to_store); ///// inserta a _og_audi

                /*============================= UPDATE OG ===============================*/
                $update_pt = array(
                    'pt_meta' => $this->input->post('met_m'),
                    'pt_objetivo' => strtoupper($this->input->post('pt2')),
                    'pt_estado' => '2',
                    'fun_id' => $this->session->userdata("fun_id"),
                );
                $this->db->where('pt_id', $this->input->post('pt_id'));
                $this->db->update('_productoterminal', $update_pt);
                /*==============================================================================*/

                $this->model_modificacion->delete_pt_prog($this->input->post('pt_id'));

                if($this->input->post('m1')!=0)
                {
                    $this->model_modificacion->add_pt_prog($this->input->post('pt_id'),1,$this->input->post('m1'));
                }
                if($this->input->post('m2')!=0)
                {
                    $this->model_modificacion->add_pt_prog($this->input->post('pt_id'),2,$this->input->post('m2'));
                }
                if($this->input->post('m3')!=0)
                {
                    $this->model_modificacion->add_pt_prog($this->input->post('pt_id'),3,$this->input->post('m3'));
                }
                if($this->input->post('m4')!=0)
                {
                    $this->model_modificacion->add_pt_prog($this->input->post('pt_id'),4,$this->input->post('m4'));
                }
                if($this->input->post('m5')!=0)
                {
                    $this->model_modificacion->add_pt_prog($this->input->post('pt_id'),5,$this->input->post('m5'));
                }
                if($this->input->post('m6')!=0)
                {
                    $this->model_modificacion->add_pt_prog($this->input->post('pt_id'),6,$this->input->post('m6'));
                }
                if($this->input->post('m7')!=0)
                {
                    $this->model_modificacion->add_pt_prog($this->input->post('pt_id'),7,$this->input->post('m7'));
                }
                if($this->input->post('m8')!=0)
                {
                    $this->model_modificacion->add_pt_prog($this->input->post('pt_id'),8,$this->input->post('m8'));
                }
                if($this->input->post('m9')!=0)
                {
                    $this->model_modificacion->add_pt_prog($this->input->post('pt_id'),9,$this->input->post('m9'));
                }
                if($this->input->post('m10')!=0)
                {
                    $this->model_modificacion->add_pt_prog($this->input->post('pt_id'),10,$this->input->post('m10'));
                }
                if($this->input->post('m11')!=0)
                {
                    $this->model_modificacion->add_pt_prog($this->input->post('pt_id'),11,$this->input->post('m11'));
                }
                if($this->input->post('m12')!=0)
                {
                    $this->model_modificacion->add_pt_prog($this->input->post('pt_id'),12,$this->input->post('m12'));
                }
                redirect('admin/mod/objetivo/'.$this->input->post('poa_id').'/'.$this->input->post('pt_id').'/true');
            }
            else
            {   
                redirect('admin/mod/objetivo/'.$this->input->post('poa_id').'/'.$this->input->post('pt_id').'/false');
            }
        }
    }
    /*=========================================================================================================================*/
    public function reporte_objetivos_estrategicos($poa_id)
    {
        //$num_principal = ($dictamen->tp_id == 1) ? 1 : 0 ;
         $html = $this->list_objetivos_estrategicos($poa_id);//Reporte Partida por procesos
        
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','512M');
        ini_set('max_execution_time', 900);
        $dompdf->render();
        $dompdf->stream("reporte_proyecto_partidas_total.pdf", array("Attachment" => false));
    }
    function list_objetivos_estrategicos($poa_id)
    {
        $gestion = $this->session->userdata('gestion');
        $html = '
        <html>
          <head>' . $this->estilo_vertical() . $this->session->userdata('franja_decorativa').'
           <style>
             @page { margin: 130px 20px; }
             #header { position: fixed; left: 0px; top: -110px; right: 0px; height: 20px; background-color: #fff; text-align: center; }
             #footer { position: fixed; left: 0px; bottom: -125px; right: 0px; height: 110px;}
             #footer .page:after { content: counter(page, upper-roman); }
           </style>

          <body>
           <div id="header">
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> MODIFICACIONES DE OBJETIVOS DE GESTI&Oacute;N Y PRODUCTOS TERMINALES <br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="80px">
                        </td>
                    </tr>
                </table>
           </div>
           <div id="footer">
             <table border="0" cellpadding="0" cellspacing="0" class="tabla">
                <tr class="modo1" bgcolor=#DDDEDE>
                    <td width=33%;>Jefatura de Unidad o Area / Direcci&oacute;n de Establecimiento / Responsable de Area Regionales / Administraci&oacute;n Central</td>
                    <td width=33%;>Jefaturas de Departamento / Servicios Generales Regional / Medica Regional</td>
                    <td width=33%;>Gerencia General / Gerencias de Area /Administraci&oacute;n Regional</td>
                </tr>
                <tr class="modo1">
                    <td><br><br><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br><br><br></td>
                </tr>
                <tr>
                    <td colspan=2><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                    <td><p class="page">P&aacute;gina </p></td>
                </tr>
            </table>
           </div>
           <div id="content">
             <p>
              
             </p>
             
           </div>
         </body>
         </html>';
        return $html;
    }


    function estilo_vertical()
    {
        $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        .mv{font-size:10px;}
        .siipp{width:120px;}

        .titulo_pdf {
            text-align: left;
            font-size: 8px;
        }
        .tabla {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 8px;
        width: 100%;

        }
        .tabla th {
        padding: 2px;
        font-size: 7px;
        background-color: #1c7368;
        background-repeat: repeat-x;
        color: #FFFFFF;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-right-style: solid;
        border-bottom-style: solid;
        border-right-color: #558FA6;
        border-bottom-color: #558FA6;
        font-family: "Trebuchet MS", Arial;
        text-transform: uppercase;
        }
        .tabla .modo1 {
        font-size: 7px;
        font-weight:bold;
       
        background-image: url(fondo_tr01.png);
        background-repeat: repeat-x;
        color: #34484E;
        font-family: "Trebuchet MS", Arial;
        }
        .tabla .modo1 td {
        padding: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-right-style: solid;
        border-bottom-style: solid;
        border-right-color: #A4C4D0;
        border-bottom-color: #A4C4D0;
        }
    </style>';
        return $estilo_vertical;
    }
}