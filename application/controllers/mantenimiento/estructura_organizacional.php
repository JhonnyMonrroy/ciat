<?php

class estructura_organizacional extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('mantenimiento/model_configuracion');
        $this->load->model('mantenimiento/model_estructura_org');
         //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(9);
     }
   
    public function estruc_org()
    {   
       
        $data['estructura'] = $this->model_estructura_org->estructura_org();
        $ruta = 'mantenimiento/vlista_estructura_org';
        $this->construir_vista($ruta,$data);
    }
    /*public function add_estructura_org()
    {           
          $uni_id=$this->input->post('uni_codigo');
          $uni_unidad=$this->input->post('uni_unidad');
          $uni_depende=$this->input->post('dependiente');
        
        $this->model_estructura_org->add_estrucura_org($uni_id,$uni_depende,$uni_unidad);
    }*/
   

   
   function construir_vista($ruta,$data){

        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'MANTENIMIENTO';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');

    }
    function verificar_cod_uni(){
        if($this->input->is_ajax_request() && $this->input->post('uni_codigo'))
        {
            $post = $this->input->post();
            $cod = $post['uni_codigo'];
            $cod = $this->security->xss_clean($cod);
            $data = $this->model_estructura_org->verificar_unicod($cod);
            if(count($data)== 0){
                echo '1';
            }else{
                echo '0';
            }
        }else{
            show_404();
        }
    }
    function ad_estructuraorganizacional(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('uni_unidad', 'unidad', 'required|trim');
            $this->form_validation->set_rules('uni_codigo', 'codigo', 'required|trim|integer');
            // $this->form_validation->set_rules('padre', 'Seleccione una opcion', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            if ($this->form_validation->run() ) {
                $uni_unidad = $this->input->post('uni_unidad');
                $uni_codigo=  $this->input->post('uni_codigo');
                $padre =  $this->input->post('padre');
                //=================enviar  evitar codigo malicioso ==========
                $uni_unidad= $this->security->xss_clean(trim($uni_unidad));
                $uni_codigo = $this->security->xss_clean($uni_codigo);
                //======================= MODIFICAR=
                if(isset($_REQUEST['modificar'])){
                    $uni_id = $this->input->post('modificar');
                    if($padre == ''){
                        $padre=0;
                        $this->model_estructura_org->mod_uni($uni_id,$uni_unidad,$padre);
                    }else{
                        $this->model_estructura_org->mod_uni($uni_id,$uni_unidad,$padre);
                    }
                }else{
                    if($padre == ''){
                        $this->model_estructura_org->add_uni_independiente($uni_codigo,$uni_unidad);
                    }else{
                        $this->model_estructura_org->add_uni_dependiente($uni_codigo,$uni_unidad,$padre);
                    }
                }
                echo "true";
            } else {
                echo'DATOS ERRONEOS';
            }
        }else{
            show_404();
        }
    }
    ////////modificar//////////
     function get_uni(){
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $cod = $post['id_uni'];
            $id = $this->security->xss_clean($cod);
            $dato_uni= $this->model_estructura_org->get_uni($id);
            $padre='NINGUNO';
            if($dato_uni[0]['uni_depende']!=0){
                $uni_padre = $this->model_estructura_org->get_uni($dato_uni[0]['uni_depende']);
                $padre =$uni_padre[0]['uni_unidad'];
            }

            $result = array(
                'uni_id' => $dato_uni[0]['uni_id'],
                "uni_unidad" =>$dato_uni[0]['uni_unidad'],
                "padre" => $padre,
                "uni_depende" => $dato_uni[0]['uni_depende'],
            );
            echo json_encode($result);
        }else{
            show_404();
        }
    }
    //////leiminar///////
    function del_uni(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $postid = $post['postid'];
            $sql = 'UPDATE unidadorganizacional SET uni_estado = 0 WHERE uni_id = '.$postid;
            if($this->db->query($sql)){
                echo $postid;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }




   }
    


