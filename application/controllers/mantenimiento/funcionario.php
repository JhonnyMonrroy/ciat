<?php

class Funcionario extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('encrypt');
        // $this->load->library('MY_Encrypt');
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('mantenimiento/model_funcionario');
         //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(9);
    }
    
    public function roles_html($id)
    {
        $html_roles_fun = '';
        $lista_rol = $this->model_funcionario->get_rol($id);
        foreach ($lista_rol as $fila) {
            $html_roles_fun .= '
                <li>'.$fila['r_nombre'].'</li>
            ';
        }
        return $html_roles_fun;
    }

    /**
        * Funcion list_usuarios()
        * Para el rol administrador, lista de usuarios activos, ABM para usuarios(funcionarios)
        *
    **/
    public function list_usuarios()
    {
        $lista_fun = $this->model_funcionario->get_funcionarios();
        $tabla = '';
        $cont = 1;
        foreach ($lista_fun as $row) {
            $nombres = $row['fun_nombre'] . ' ' . $row['fun_paterno'] . ' ' . $row['fun_materno'];
            $dato_activo = '';
            if ($row['fun_estado'] == 1 or $row['fun_estado'] == 2) {
                $dato_activo = '
                    <span class="btn btn-success del_fun" name="'.$row['fun_id'].'" data-toggle="tooltip" title="Click para desactivar">
                        <i class="fa fa-check"></i>
                        ACTIVO
                    </span>'
                ;
            } else {
                $dato_activo = '
                    <span class="btn btn-danger act_fun" name="'.$row['fun_id'].'" data-toggle="tooltip" title="Click para activar">
                        <i class="fa fa-circle"></i>
                        INACTIVO
                    </span>'
                ;
            }
            
            $tabla .= '
                <tr id="tr'.$row['fun_id'].'">
                    <td>'.$cont.'</td>
                    <td>' . $nombres . '</td>
                    <td>' . $row['fun_ci'] . '</td>
                    <td>' . $row['fun_telefono'] . '</td>
                    <td>' . $row['car_cargo'] . '</td>
                    <td>' . $row['uni_unidad'] . '</td>
                    <td>' . $row['fun_usuario'] . '</td>
                    <td style="padding-left: 0;">
                        <ul style="text-align:left; padding-left: 1;list-style-type:square; margin:2px;">
                            '.$this->roles_html($row['fun_id']).'
                        </ul>
                    </td>
                    <td>
                        '.$dato_activo.'
                    </td>
                    <td>
                        <BUTTON data-toggle="modal" data-target="#modal_mod_fun" class="btn btn-xs botones dos mod_fun"  name="'.$row['fun_id'].'" id="enviar_mod">
                            <img src="'.base_url().'assets/ifinal/modificar.png" WIDTH="40" HEIGHT="40"/>
                        </BUTTON>
                    </td>
                </tr>';
            $cont++;
        }
        $data['fun'] = $tabla;
        /*$data['lis_rol']   = $this->model_funcionario->get_rol_vi();*/
        $data['lista_car'] = $this->model_funcionario->get_cargo();
        $data['lista_uni'] = $this->model_funcionario->get_uni_o();
        $data['listas_rol'] = $this->model_funcionario->get_add_rol();
        $ruta = 'admin/mantenimiento/funcionario/vlist_fun';
        $this->construir_vista($ruta,$data);
    }

    public function mantenimiento_roles()
    {
        $data['lista_rol'] = $this->model_funcionario->get_roles();
        $ruta = 'admin/mantenimiento/funcionario/vlist_roles';
        $this->construir_vista($ruta,$data);
    }

    public function construir_vista($ruta,$data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'MANTENIMIENTO';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);
        $this->load->view('includes/footer');
    }

    public function add_funcionario()
    {
        $fun_nombre = strtoupper ($this->input->post('fun_nombre'));
        $fun_paterno = strtoupper ( $this->input->post('fun_paterno'));
        $fun_materno = strtoupper ( $this->input->post('fun_materno'));
        $fun_ci = strtoupper ( $this->input->post('fun_ci'));
        $fun_telefono = strtoupper ( $this->input->post('fun_telefono'));
        $fun_cargo = strtoupper ( $this->input->post('fun_cargo'));
        $fun_domicilio = strtoupper ( $this->input->post('fun_domicilio'));
        $fun_usuario = strtoupper ($this->input->post('fun_usuario'));
        $fun_password = $this->encrypt->encode($this->input->post('fun_password'));
        $uni_id	= strtoupper ($this->input->post('uni_id'));
        $car_id = strtoupper ($this->input->post('car_id'));
        $roles = strtoupper($this->input->post('rol[]'));
        $usuario = $this->model_funcionario->verificar_fun($fun_usuario);
       
        if ($usuario == 0) {
            $this->model_funcionario->add_fun($fun_nombre,$fun_paterno, $fun_materno,$fun_ci,$fun_telefono,$fun_cargo,$fun_domicilio,$fun_usuario,$fun_password,$uni_id,$car_id,$roles);
        } else { 
            echo "<script>alert('usuario ya existente');</script>";
            redirect('admin/mnt/list_usu', 'refresh');
        }
    }
    
    public function del_fun()
    {
        $fun_id=$this->input->post('del_fun');
        $this->model_funcionario->del_fun($fun_id);
    }

    public function act_fun()
    {
        $fun_id=$this->input->post('del_fun');
        $data = array(
            'fun_estado' => 1
        );
        $boolean = $this->model_funcionario->update_fun($fun_id, $data);
        $result = array(
            'boolean' => $boolean
        );
        echo json_encode($result);
    }
    
    public function nueva_contra()
    {
        $this->load->view('admin/mod_contrase');
    }

    public function vista()
    {
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'MANTENIMIENTO';
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view('includes/footer');
    }

    public function mod_cont()
    {
        $fun_id = $this->input->post('fun_id');
        $apassword = $this->input->post('apassword');
        $password = $this->input->post('password');
        $password = $this->encrypt->encode($password);
        $verifica = (($this->encrypt->decode($this->model_funcionario->verificar_password($fun_id))) == $apassword) ? true : false ;
        if($verifica){
            $this->model_funcionario->mod_password($fun_id,$password);
            echo "
                <script>
                    alert('Se Cambio la Contraseña Correctamente');
                </script>
            ";
            $this->vista();
        }
        else{
            echo "
                <script>
                    alert('La Contraseña Anterior No Coincide');
                </script>
            ";
            $this->nueva_contra();
        }
    }

    public function mod_funs()
    {
        $fun_id = $this->input->post('fun_id');
        $dato_fun = $this->model_funcionario->get_funcionario($fun_id);
        $roles_actuales = $this->model_funcionario->get_rol($fun_id);
        $rol = array();
        foreach ($roles_actuales as $fila) {
            array_push($rol,$fila['r_id']);
        }
        $edit_pass = $this->encrypt->decode($dato_fun[0]['fun_password']);
        $result = array(
            'fun_id' => $dato_fun[0]['fun_id'],
            'uni_id' => $dato_fun[0]['uni_id'],
            'car_id' => $dato_fun[0]['car_id'],
            "fun_nombre" => $dato_fun[0]['fun_nombre'],
            "fun_paterno" => $dato_fun[0]['fun_paterno'],
            "fun_materno" => $dato_fun[0]['fun_materno'],
            "fun_telefono" => $dato_fun[0]['fun_telefono'],
            "fun_cargo" => $dato_fun[0]['fun_cargo'],
            "fun_domicilio" => $dato_fun[0]['fun_domicilio'],
            "fun_dni" => $dato_fun[0]['fun_ci'],
            "fun_usuario" => $dato_fun[0]['fun_usuario'],
            "fun_password" => $edit_pass,
            "fun_roles" => $rol
        );
        echo json_encode($result);
    }
    
    public function mod_funcionario()
    {
        $fun_nombre=$this->input->post('modfun_nombre');
        $fun_paterno=$this->input->post('modfun_paterno');
        $fun_materno=$this->input->post('modfun_materno');
        $fun_cargo=$this->input->post('modfun_cargo');
        $fun_ci=$this->input->post('modfun_dni');
        $fun_telefono=$this->input->post('modfun_telefono');
        $fun_domicilio=$this->input->post('modfun_domicilio');
        $fun_usuario=$this->input->post('modfun_usuario');
        $fun_password = $this->encrypt->encode($this->input->post('modfun_password'));
        $uni_id=$this->input->post('moduni_id');
        $car_id=$this->input->post('modcar_id');
        $fun_id=$this->input->post('modfun_id');
        $roles = $this->input->post('modrol');
        $this->model_funcionario->mod_funcionario($fun_nombre,$fun_paterno,$fun_materno,$fun_cargo,$fun_ci,$fun_telefono,$fun_domicilio,$fun_usuario,$uni_id,$car_id,$fun_id,$roles,$fun_password);
    }
}