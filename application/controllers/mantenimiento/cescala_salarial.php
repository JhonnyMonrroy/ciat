<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Cescala_salarial extends CI_Controller {
    public $rol = array('1' => '1');
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->model('Users_model','',true);
            if($this->rolfun($this->rol)){ 
                $this->load->library('pdf');
                $this->load->library('pdf2');
                $this->load->model('Users_model','',true);
                $this->load->model('menu_modelo');
                $this->load->model('mantenimiento/model_configuracion');
                $this->load->model('mantenimiento/model_escala_salarial');
                $this->load->library("security");
            }
            else{
                redirect('admin/dashboard');
            }
        }else{
                redirect('/','refresh');
            }
     }


    public function lista_escala()
    {   
        $data['menu']=$this->menu(9);
        $escala = $this->model_escala_salarial->lista_escala_salarial();
        $data['cargos'] = $this->model_escala_salarial->lista_escala_salarial();
        $data['list_car_padre'] = $this->model_escala_salarial->list_car_padre(); ///// la consulta
        $tabla = '';
        $nro=0;
        foreach($escala as $row){
            $nro++;
            $tabla .= '<tr>';
                $tabla .= '<td>'.$nro.'</td>';
                $tabla .= '<td>'.$row['car_id'].'</td>';
                $tabla .= '<td>'.$row['depende'].'</td>';
                $tabla .= '<td>'.$row['car_cargo'].'</td>';
                $tabla .= '<td>'.number_format($row['car_sueldo'], 2, ',', '.').'</td>';
                $tabla .='<td align="center">
                <div class="btn-group">
                 <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> ACCIONES <span class="caret"></span></button>
                 <ul class="dropdown-menu" role="menu">
                 <li><a href="#" data-toggle="modal" data-target="#modal_mod_car" class="mod_car" title="MODIFICAR ESCALA SALARIAL" name="'.$row['car_id'].'"><i class="glyphicon glyphicon-pencil"></i> Modificar</a></li>
                 <li><a href="#" data-toggle="modal" data-target="#modal_act_ff" class="act_ff" title="ELIMINAR ESCALA SALARIAL" name="'.$row['car_id'].'"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
                 </ul>
                </div>
                </td>';
            $tabla .= '</tr>';
        }
        $data['escala']=$tabla;
        $this->load->view('admin/mantenimiento/escala_salarial/list_escala', $data);
    }
   

   function verif_codigo_escala(){
    if($this->input->is_ajax_request()) 
    {
        $post = $this->input->post();
        $car_codigo = $post['car_codigo'];
        $variable= $this->model_escala_salarial->verif_cod_gestion($car_codigo);
        
        if(count($variable)==0){
            echo "true";
        }
        else{
          echo "false";
        } 
    }
    else{
        show_404();
        }
        
    }

    function valida_cargo(){
    if($this->input->is_ajax_request()) 
    {
        $post = $this->input->post();
        $car_nombre = $post['car_nombre'];
        $car_sueldo = $post['car_sueldo'];
        $padre = $post['padre'];
        $car_codigo = $post['car_codigo'];

        if($padre==''){
            $padre=0;
        }
        else{
            $padre=$padre;   
        }
        $query=$this->db->query('set datestyle to DMY');
        $data_to_store = array( 
            'car_id' => $car_codigo,
            'car_depende' => $padre,
            'car_cargo' => strtoupper($car_nombre),
            'car_estado' => 1,
            'car_sueldo' => $car_sueldo,
            'fun_id' => $this->session->userdata("fun_id"),
            'g_id' => $this->session->userdata("gestion"),
        );
        $this->db->insert('cargo', $data_to_store);

        echo "true";
    }
    else{
        show_404();
        }
    }

    function update_cargo(){
    if($this->input->is_ajax_request()) 
    {
        $post = $this->input->post();
        $car_nombre = $post['car_nombre'];
        $car_sueldo = $post['car_sueldo'];
        $padre = $post['padre'];
        $car_codigo = $post['car_codigo'];

        if($padre==''){
            $padre=0;
        }
        else{
            $padre=$padre;   
        }

            $query=$this->db->query('set datestyle to DMY');
                $update_proy = array(
                'car_depende' => $padre,
                'car_cargo' => strtoupper($car_nombre),
                'car_estado' => 1,
                'car_sueldo' => $car_sueldo,
                'fun_id' => $this->session->userdata("fun_id"));

            $this->db->where('car_id', $car_codigo);
            $this->db->update('cargo', $update_proy);

        echo "true";
    }
    else{
        show_404();
        }
    }

    function del_car()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $car_id = $post['car_id'];

            $update_car = array(
                'car_estado' => 0, /// Eliminado
                'fun_id' => $this->session->userdata("fun_id")  
                );
            $this->db->where('car_id', $car_id);
            $this->db->update('cargo', $update_car);

            $result = array(
                    'respuesta' => 'correcto'
                );
            
            echo json_encode($result);

        } else {
            echo 'DATOS ERRONEOS';
        }
    }

      private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }

        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        .mv{font-size:10px;}
        .siipp{width:120px;}

.titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .tabla {
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 7px;
            width: 100%;

}
        .tabla th {
padding: 5px;
font-size: 7px;
background-color: #83aec0;
background-image: url(fondo_th.png);
background-repeat: repeat-x;
color: #FFFFFF;
border-right-width: 1px;
border-bottom-width: 1px;
border-right-style: solid;
border-bottom-style: solid;
border-right-color: #558FA6;
border-bottom-color: #558FA6;
font-family: "Trebuchet MS", Arial;
text-transform: uppercase;
}
.tabla .modo1 {
font-size: 7px;
font-weight:bold;
background-color: #e2ebef;
background-image: url(fondo_tr01.png);
background-repeat: repeat-x;
color: #34484E;
font-family: "Trebuchet MS", Arial;
}
.tabla .modo1 td {
padding: 5px;
border-right-width: 1px;
border-bottom-width: 1px;
border-right-style: solid;
border-bottom-style: solid;
border-right-color: #A4C4D0;
border-bottom-color: #A4C4D0;

}
    </style>';


/*==================================== REPORTE - ESCALA SALARIAL ===================================================*/
    public function rep_escala_salarial()
    {
        $gestion = $this->session->userdata('gestion');
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> ESCALA SALARIAL '.$gestion.'<br>
                        </td>
                        <td width=20%;>
                           <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="80px">
                        </td>
                    </tr>
                </table>

                
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                
                <br>
                <div class="contenedor_principal">
                    <table border="0" cellpadding="0" cellspacing="0" class="tabla">
                      <tr class="modo1">
                        <th style="width:2%">C&Oacute;DIGO</th>
                        <th style="width:15%">DEPENDE</th>
                        <th style="width:15%">CARGO</th>
                        <th style="width:4%">SUELDO</th>
                      </tr>';
                        $cargos = $this->model_escala_salarial->lista_escala_salarial();
                        $cont = 1;
                        foreach ($cargos as $row) {

                            $html .= '<tr class="modo1">';
                            $html .= '<td >'.$row['car_id'].'</td>';
                            $html .= '<td >'.$row['depende'].'</td>';
                            $html .= '<td >'.$row['car_cargo'].'</td>';
                            $html .= '<td >'.$row['car_sueldo'].'</td>';
                            $html .= '</tr>';
                        }
                        $html .= '
                    </table>

                    
                </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'portrait');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }

        /*------------------------------------- MENU -----------------------------------*/
        function menu($mod){
            $enlaces=$this->menu_modelo->get_Modulos($mod);
            for($i=0;$i<count($enlaces);$i++)
            {
              $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }

            $tabla ='';
            for($i=0;$i<count($enlaces);$i++)
            {
                if(count($subenlaces[$enlaces[$i]['o_child']])>0)
                {
                    $tabla .='<li>';
                        $tabla .='<a href="#">';
                            $tabla .='<i class="'.$enlaces[$i]['o_image'].'"></i> <span class="menu-item-parent">'.$enlaces[$i]['o_titulo'].'</span></a>';    
                            $tabla .='<ul>';    
                                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
                                $tabla .='<li><a href="'.base_url($item['o_url']).'">'.$item['o_titulo'].'</a></li>';
                            }
                            $tabla .='</ul>';
                    $tabla .='</li>';
                }
            }

            return $tabla;
        }
    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }
}
    


