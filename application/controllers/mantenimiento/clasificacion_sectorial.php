<?php
class Clasificacion_sectorial extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('mantenimiento/model_clasificacionsectorial');
        $this->load->library('menu');
        $this->menu->const_menu(9);
    }
    public function lista_clasificadores_sectores()
    {
        $gestion = $this->session->userdata('gestion');
        $data['titulo'] = 'Clasificadores Sectoriales "Sectores"';
        $data['sectores'] = $this->model_clasificacionsectorial->get_sectores_gestion($gestion);
        $ruta = 'mantenimiento/vlista_clasificacionsectorial';
        $this->construir_vista($ruta,$data);
    }
    public function construir_vista($ruta,$data)
    {
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'MANTENIMIENTO';
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);
        $this->load->view('includes/footer');
    }
    public function lista_clasificadores_subsectores($codsector,$sector)
    {
        $gestion = $this->session->userdata('gestion');
        $data['subsectores'] = $this->model_clasificacionsectorial->get_subsectores_gestion($gestion,$codsector);
        $datos = $this->model_clasificacionsectorial->datos_sector($gestion,$codsector);
        $data['titulo'] = 'Lista Subsectores y Actividades Economicas del Sector '.$datos->descclasificadorsectorial;
        $data['sector'] = '"'.$datos->descclasificadorsectorial.'"';
        $data['codsectorial'] = $datos->codsectorial;
        $data['codsec'] = $datos->codsec;
        $v = explode( "-", $data['codsectorial']);
        $data['pk1'] = $v[0];
        $ruta = 'mantenimiento/vlista_cs_subsectores';
        $this->construir_vista($ruta,$data);
    }
    public function valida_codigosector()
    {
        $codsectorial = $this->input->post('codsectorial');
        $gestion = $this->session->userdata('gestion');
        $resp = $this->model_clasificacionsectorial->verfica_nuevosector($codsectorial,$gestion);
        if(!$resp){
            $respuesta = array(
                'respuesta' => $resp,
                'mensaje' => 'Se puede Utilizar el Codigo'
            );
        } else {
            $respuesta = array(
                'respuesta' => $resp,
                'mensaje' => 'Codigo ya esta en uso'
            );
        }
        echo json_encode($respuesta);
    }
    public function agregar_nuevosector()
    {
        $nombre = $this->input->post('clas_nombre');
        $abreviacion = $this->input->post('clas_abre');
        $gestion = $this->session->userdata('gestion');
        $cod = $this->input->post('clas_cod');
        if($cod){
            $codsectorial = ($cod >= 10) ? $cod.'-0-00' : '0'.$cod.'-0-00';
            $codsec = ($cod >= 10) ? $cod.'000' : '0'.$cod.'000';
            // $v = explode('-',$codsectorial);
            $data = array(
                'codsectorial' => $codsectorial,
                'descclasificadorsectorial' => $nombre,
                'codsec' => $codsec,
                'sector' => $nombre,
                'abrevsector' => $abreviacion,
                'codsubsec' => $codsec,
                'subsector' => $nombre,
                'codgruposectorial' => 4,
                'nivel' => 1,
                'codsectorialduf' => 'Sector',
                'gestion' => $gestion
            );
            $this->model_clasificacionsectorial->inserta_nuevosector($data);
            redirect('clasificacion_sectorial','refresh');
        }
    }
    public function modificar_sector()
    {
        $codsectorial = $this->input->post('codsectorial');
        $gestion = $this->session->userdata('gestion');
        $data = $this->model_clasificacionsectorial->get_clasificador_sectorial($codsectorial,$gestion);
        $result = array(
            'codsectorial' => $data->codsectorial,
            'gestion' => $data->gestion,
            'descripcion' => $data->descclasificadorsectorial,
            'abreviacion' => $data->abrevsector,
            'codsec' => $data->codsec,
            'gestion' => $data->gestion
            );
        echo json_encode($result);
    }
    public function modifica_sector()
    {
        $codsectorial = $this->input->post('codsectorial_mod');
        $nombre = $this->input->post('clas_mod_nombre');
        $abreviacion = $this->input->post('clas_mod_abre');
        $gestion = $this->input->post('mod_gestion_sector');
        $codsec = $this->input->post('mod_codsec');
        $data = array(
            'descclasificadorsectorial' => $nombre,
            'sector' => $nombre,
            'abrevsector' => $abreviacion,
            'subsector' => $nombre
        );
        if($codsectorial){
            $this->model_clasificacionsectorial->modificar_datos_sector($data,$codsectorial,$gestion);
            if($codsec){
                $subsectores = array(
                    'sector' => $nombre,
                    'abrevsector' => $abreviacion,
                );
                $this->model_clasificacionsectorial->modificar_datos_subsectores($subsectores,$codsec,$gestion);
            }
            redirect('clasificacion_sectorial','refresh');
        }
    }
    public function eliminar_sector()
    {
        $codsec = $this->input->post('codsec');
        $gestion = $this->input->post('gest');
        $data = array(
            'codsec' => $codsec,
            'gestion' => $gestion,
            'bool' => true
        );
        if($data['codsec']) {
            $bool = $this->model_clasificacionsectorial->elimina_sector($data);
            $data['bool'] = true;
        } else {
            $data['bool'] = false;
        }
        echo json_encode($data);
    }
}