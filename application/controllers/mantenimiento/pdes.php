<?php

class pdes extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('mantenimiento/mpdes');   
         //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(9);
   	 }
   
    public function lista_pdes()
    {
        $data['list_pdes_pilar'] =$this->mpdes->listar_pedes_pilar();
        //$data['list_pdes_meta'] =$this->mpdes->listar_pedes_meta();
        //$data['list_pdes_resultado'] =$this->mpdes->listar_pedes_resultado();
        //$data['list_pdes_accion'] =$this->mpdes->listar_pedes_accion();
        $ruta = 'mantenimiento/vlista_pdes';
        $this->construir_vista($ruta,$data);
    }

   function construir_vista($ruta,$data){

        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'MANTENIMIENTO';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');

    }
    function verificar_pdes()
    {
        if($this->input->is_ajax_request() && $this->input->post('pdes_codigo'))
            {
                $post = $this->input->post();
                $pdes_codigo = $post['pdes_codigo'];
                $pdes_codigo = $this->security->xss_clean($pdes_codigo);
                $data = $this->mpdes->verificar_pdes($pdes_codigo);
                if(count($data)== 0){
                    echo '1';
                }else{
                    echo '0';
                }
            }else{
                show_404();
            }
    }
    function add_pdes_pilar()
    {
         
                $pdes_codigo = $this->input->post['pdes_codigo'];
                $pdes_gestion=  $this->input->post['pdes_gestion'];
                $pdes_descripcion=  $this->input->post['pdes_descripcion'];
                $this->mpdes->add_pilar_pdes($pdes_codigo,$pdes_gestion,$pdes_descripcion);          
    }

    function mostrar_pilar()
    {
        $pdes_id=$this->input->post('pdes_id');
            $dato_pdes=$this->mpdes->mostrar_pilar_pdes($pdes_id);
            $result = array(
                'pdes_codigo' => $dato_pdes[0]['pdes_codigo'],
                'pdes_gestion' => $dato_pdes[0]['pdes_gestion'],
                'pdes_descripcion' => $dato_pdes[0]['pdes_descripcion'],
                'pdes_id' => $dato_pdes[0]['pdes_id'],
                );
            echo json_encode($result);
    }
    function mod_pilar_pdes()
    {
        $pdes_codigo=$this->input->post('modpdes_codigo');
        $pdes_gestion=$this->input->post('modpdes_gestion');
        $pdes_descripcion=$this->input->post('modpdes_descripcion');
        $pdes_id=$this->input->post('modpdes_id');

        $this->mpdes->mod_pilar_pdes($pdes_id,$pdes_codigo,$pdes_gestion,$pdes_descripcion);
        
    }
    function verificar_pilar()
    {
        
            $post = $this->input->post();
            $cod = $post['pilar_codigo'];
          

          if($this->input->is_ajax_request() && $this->input->post('pilar_codigo'))
            {
                $post = $this->input->post();
                $cod = $post['pilar_codigo'];
                $cod = $this->security->xss_clean($cod);
                $data = $this->mpdes->verificar_pilar($cod);
                if(count($data)== 0){
                    echo '1';
                }else{
                    echo '0';
                }
            }else{
                show_404();
            }
        }
            function pdes_pilar_add()
            {

                                echo "hola";
                 //$this->mpdes->add_pdes_pilar($pdes_codigo,$pdes_gestion,$pdes_descripcion);

            }

    }
