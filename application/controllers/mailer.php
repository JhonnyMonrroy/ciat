<?php

class Mailer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
		
		 $config = array(
			 'protocol' => 'smtp',
			 'smtp_host' => 'ssl://smtp.gmail.com',
			 'smtp_port' => 465,
			 'smtp_user' => 'SIPSyE.CIAT@gmail.com',
			 'smtp_pass' => 'GobernacionLPZ2018',
			 'mailtype' => 'html',
			 'charset' => 'utf-8',
			 'newline' => "\r\n",
		 );  		
		
		$nombre = $this->input->post("nombre");
		$referencia = $this->input->post("referencia");
		$unidad = $this->input->post("unidad");
		$asunto = $this->input->post("asunto");
		$detalle = $this->input->post("detalle");

		$this->load->library('email');
		$this->email->initialize($config);

		$this->email->to('SIPSyE.soporte@lapaz.gob.bo');
		$this->email->cc('SIPSyE.CIAT@gmail.com');
		$this->email->from('SIPSyE.CIAT@gmail.com', 'SIPSyE Mailer');
		$this->email->subject($asunto);
		$this->email->message('<h4><strong><span style="color:#596; text-transform: uppercase;">'.$asunto.'</span></strong><br><span style="color:#596;">Nombre funcionario:</span> '.$nombre.' - <span style="color:#596;">Unidad :</span> '.$unidad.'<br><small><span style="color:#596;">Referencia: </span>'.$referencia.'</small></h4><p>'.$detalle.'</p>');

		
		$this->form_validation->set_rules('nombre','Nombre','trim|required');
		$this->form_validation->set_rules('referencia','Referencia','trim|required');
		$this->form_validation->set_rules('unidad','Unidad','trim|required');
		$this->form_validation->set_rules('asunto','Asunto','trim|required');
		$this->form_validation->set_rules('detalle','Detalle','trim|required');
	    $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback_recaptcha');
		
		echo "Datos RECIBIDOS: ".$nombre." : ".$asunto." : ".$detalle;
//		if ($this->email->send(FALSE) && $this->form_validation->run())//PARA DEPURACION
		if($this->form_validation->run()){
			if ($this->email->send(FALSE))
			{
//				echo "<h2>Datos enviados<h2>";
				$this->session->set_flashdata('mensaje_mail', 'Datos enviados correctamente. Pronto nuestros asistentes se pondran en contacto con usted');
			}else{
//				echo "<h2>Datos NO enviados<h2>";
				$this->session->set_flashdata('mensaje_mail', 'Oops!! Ocurrio un error al enviar el mensaje');
			}
	//		echo $this->email->print_debugger(array('headers')); PARA DEPURACION
		}else{
//			echo "Datos invalidos, ingrese los datos nuevamente";
			$this->session->set_flashdata('mensaje_mail', 'Datos invalidos, escriba nuevamente su mensaje al equipo de asistencia t&eacute;cnica');
		}
		redirect('/');
	}
	
	
  public function recaptcha($str='')
  {
    $google_url="https://www.google.com/recaptcha/api/siteverify";
    $secret='6LdhhlMUAAAAAHm35wwfMWRamAHRSQ6tIAB4dahA';
    $ip=$_SERVER['REMOTE_ADDR'];
    $url=$google_url."?secret=".$secret."&response=".$str."&remoteip=".$ip;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
    $res = curl_exec($curl);
    curl_close($curl);
    $res= json_decode($res, true);
    //reCaptcha success check
    if($res['success'])
    {
      return TRUE;
    }
    else
    {
      $this->form_validation->set_message('recaptcha', 'The reCAPTCHA field is telling me that you are a robot. Shall we give it another try?');
      return FALSE;
    }
  }

}