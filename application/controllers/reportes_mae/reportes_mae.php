<?php ob_start();
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Reportes_mae extends CI_Controller {
    public function __construct ()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->library('pdf');
            $this->load->library('pdf2');
            $this->load->library('excel');
            $this->load->model('reportes/taqpacha/model_taqpacha');
            $this->load->model('programacion/model_proyecto');
            $this->load->model('programacion/model_producto');
            $this->load->model('programacion/model_faseetapa');
            $this->load->model('programacion/model_componente');
            $this->load->model('programacion/model_actividad');
            $this->load->model('reportes_nfisfin/model_nfisfin');
            $this->load->model('model_rmae/mmae');
            $this->load->model('ejecucion/model_ejecucion');
            $this->load->model('reportes_tarija_das/mdas_complementario');
            $this->load->model('reportes_tarija/model_epresupuestaria');
            $this->load->model('programacion/model_reporte');
            $this->load->model('reportes/seguimiento/model_reporte_seguimiento');
            $this->load->model('modificacion/model_modificacion');
            $this->load->model('reportes/model_objetivo');
            $this->load->model('mantenimiento/mpoa');
            $this->load->model('programacion/prog_poa/mp_terminal');
            $this->load->model('menu_modelo');
            $this->load->model('Users_model','',true);

            $this->gestion = $this->session->userData('gestion');
            $this->mes = $this->session->userData('mes');
            $this->total_pi='E49A15';
            $this->total_pnr='8AB054';
            $this->total_pr='E4BE7C';
            $this->total_of='B2CC96';
            $this->total_general='5F6266';
            $this->total_multi='82E4FF';
        }else{
            redirect('/','refresh');
        }
    }
    private $estilo_horizontal = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 6px;
                color: #ffffff;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 6px;
            border: 1px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1px;
                text-align: center;
                font-size: 6px;
            }
        .mv{font-size:10px;}
        .siipp{width:190px;}
        .header_table {
            color: #000000;
            text-align: left;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #ffffff;
            align-content: center;
        }

        .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }

        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 1px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 8px;
            border-color: black;
        }
        .jarviswidget > div {
            font-size:1.3em;
		}
		.titulo, .titulo_s{
            margin:15px 0 15px 0;
			padding:0 0 5px 0;
			border-bottom:2px solid #ccc;
			text-align:center;
		}
		.titulo_s{
            border-bottom:0;
		}

		.border1{
            border:2px solid #bbb;
			background-color:rgb(245,245,255);
			padding-top:5px;
			padding-bottom:5px;
		}
		.bg-green{
            background-color:#b9e5a9;
		}
		.bg-warning{
            background-color:#ffe88c;
		}
		.bg-info{
            background-color:#9bc1ff;
		}
		.bg-gray{
            background-color:#ccc;
		}
		.bg-gray2{
            background-color:#ddd;
		}
		.bg-primary{
            background-color:#4786ad;
		}
		        .table{
		  border-color:#777 !important;
		  border: 1px solid #777;
		  border-spacing: 0;
		  border-collapse: collapse;
          padding: 0.3em;
        }
table.tabla_avance.table-condensed.table > tbody > tr > td, .table-condensed.table > tbody > tr > th, .table-condensed.table > tfoot > tr > td, .table-condensed.table > tfoot > tr > th, .table-condensed.table > thead > tr > td, .table-condensed.table > thead > tr > th {
    padding: 0.3em;
    
}		
        th{
		  font-weight:bold;
		  font-size:10px;
		  border-color:#777 !important;
        }
		td{
		  font-size:9px;
		  border-color:#777 !important;
		}
		tr.head_componente th{
			background-color:#ccc;
			color:#333;
		  border-color:#777 !important;
		}
		tr.head_producto td{
			background-color:#eee;
			color:#333;
			font-weight:bold;
		  border-color:#777 !important;
		}
		tr.row_actividad > td{
			background-color:#fff;
			color:#333;
		  border-color:#777 !important;
		}
		tr.head_tabla > th{
			background-color:#aaa;
			color:#fff;
		  border-color:#777 !important;
		}
		.bg-green{
			background-color:#b9e5a9;
		}
		.bg-warning{
			background-color:#ffe88c;
		}
		.bg-info{
			background-color:#9bc1ff;
		}
		.bg-gray{
			background-color:#ccc;
		}
		.bg-gray2{
			background-color:#ddd;
		}
		.bg-primary{
			background-color:#4786ad;
		}
		.text-right{
		    text-align:right;
		}
		/****************************/
		 table{font-size: 10px;
                    width: 100%;
                    max-width:1550px;;
                    overflow-x: scroll;
                }
                th{
                    padding: 1.4px;
                    text-align: center;
                    font-size: 11px;
                }
                th.cabecera{
                    background-color:#C7BB78;
                }
                th.cabecera_acum{
                    background-color:#F0EAC7;
                }
                th.derecha{text-align:right; font-size:11px;}
                th.izquierda{text-align:left; font-size:11px;}
                th.derecha{text-align:right; font-size:11px;}
                tr.derecha{text-align:right; font-size:11px;}
                tr.multiple{background-color:#EAD4E5; text-align:right;}
                td{font-size:11px;}
                td.izquierda{text-align:left;}
                td.centro{text-align:center;}
                td.pi{background-color:#E49A15; text-align:center;}
                td.pr{background-color:#E4BE7C; text-align:center;}
                td.pnr{background-color:#8AB054; text-align:center;}
                td.of{background-color:#B2CC96; text-align:center;}
                tr.total_pi{background-color:#E49A15; text-align:right;font-weight:bold; font-size:11px;}
                tr.total_pnr{background-color:#E4BE7C; text-align:right;font-weight:bold; font-size:11px;}
                tr.total_pr{background-color:#8AB054; text-align:right;font-weight:bold; font-size:11px;}
                tr.total_of{background-color:#B2CC96; text-align:right;font-weight:bold; font-size:11px;}
                tr.total_general{background-color:#5F6266; color:#fff; text-align:right; font-weight:bold;}
    </style>';
    private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 8px;
                color: #ffffff;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1px;
                text-align: center;
                font-size: 8px;
            }
        .mv{font-size:10px;}
        .siipp{width:200px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .datos_principales {
            text-align: center;
            font-size: 9px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }

        .indi_desemp{
            font-size: 14px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 10px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 1px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .lista{
            text-align:left;
            padding-left: 8;
            list-style-type:square;
            margin:2px;
        }
        .tabla {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 7px;
        width: 95%;
        }
        .tabla th {
        padding: 5px;
        font-size: 7px;
        background-color: #83aec0;
        background-image: url(fondo_th.png);
        background-repeat: repeat-x;
        color: #FFFFFF;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-right-style: solid;
        border-bottom-style: solid;
        border-right-color: #558FA6;
        border-bottom-color: #558FA6;
        font-family: "Trebuchet MS", Arial;
        text-transform: uppercase;
        }
        .tabla .modo1 {
        font-size: 7px;
        font-weight:bold;
        background-color: #e2ebef;
        background-image: url(fondo_tr01.png);
        background-repeat: repeat-x;
        color: #34484E;
        font-family: "Trebuchet MS", Arial;
        }
        .tabla .modo1 td {
        padding: 5px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-right-style: solid;
        border-bottom-style: solid;
        border-right-color: #A4C4D0;
        border-bottom-color: #A4C4D0;
        
        }
    </style>';
    /*===================================== Ejecución de acciones operativas por región ===========================================*/
    public function acciones_por_region()
    {
        $enlaces=$this->menu_modelo->get_Modulos(10);
        $data['enlaces'] = $enlaces;

        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['regiones'] = $this->model_taqpacha->get_regiones();
        $data['meses'] = $this->model_ejecucion->list_meses(); /// Lista de Meses
        $this->load->view('reportes/reportes_mae/select_op_region', $data);
    }
    public function valida_acciones_por_region()
    {

        if($this->input->server('REQUEST_METHOD') === 'POST')
        {

            $sql_in='';
            if($this->input->post('p1')=='on'){$p1=1;$tp_1=1;}else{$p1=0;$tp_1=0;}
            if($this->input->post('p2')=='on'){$p2=1;$tp_2=2;}else{$p2=0;$tp_2=0;}
            if($this->input->post('p3')=='on'){$p3=1;$tp_3=3;}else{$p3=0;$tp_3=0;}
            if($this->input->post('p4')=='on'){$p4=1;$tp_4=4;}else{$p4=0;$tp_4=0;}
            $sql_in='in ('.$tp_1.','.$tp_2.','.$tp_3.','.$tp_4.')';

            $enlaces = $this->menu_modelo->get_Modulos(10);
            $data['enlaces'] = $enlaces;
            for ($i = 0; $i < count($enlaces); $i++) {
                $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;

            $meses=$this->get_mes($this->input->post('meses_id'));
            $mes = $meses[1];
            $dias = $meses[2];
            $mes_id=$this->input->post('meses_id');
            $data['id_mes'] = $mes_id;
            $data['mes'] = $mes;
            $data['dias'] = date('d');//$dias;


            $pr1='';$pr2='';$pr3='';$pr4='';
            if($p1==1){$pr1='<span class="pi_t">PROYECTOS DE INVERSI&Oacute;N</span>';}
            if($p2==1){$pr2=' - <span class="pr_t">PROGRAMAS RECURRENTES</span>';}
            if($p3==1){$pr3=' - <span class="pnr_t">PROGRAMAS NO RECURRENTES</span>';}
            if($p4==1){$pr4=' - <span class="of_t">OPERACI&Oacute;N DE FUNCIONAMIENTO</span>';}

            $data['pr1']=$pr1;
            $data['pr2']=$pr2;
            $data['pr3']=$pr3;
            $data['pr4']=$pr4;

            $data['p1']=$p1;
            $data['p2']=$p2;
            $data['p3']=$p3;
            $data['p4']=$p4;

            $data['region_id']=$this->input->post('region_id');

            if($this->input->post('region_id')==-1){
                $data['proyectos']=$this->list_ejecucion_financiera_regiones_todos($p1,$p2,$p3,$p4,$sql_in,1,$mes_id);
                $data['title']=' Todas las regiones';
                $data['municipio']='';
                $this->load->view('reportes/reportes_mae/op_regiones', $data);
            }
            else{
                $data['proyectos']=$this->list_ejecucion_financiera_x_region($p1,$p2,$p3,$p4,$sql_in,$this->input->post('region_id'),1,$this->input->post('meses_id'));
                $data['title']=' Por Región';
                $municipio = $this->model_taqpacha->get_region($this->input->post('region_id'));
                $data['municipio']='<br>REGIÓN - '.strtoupper($municipio[0]['reg_region']);
                $this->load->view('reportes/reportes_mae/op_regiones', $data);
            }
        }
        else{
            $this->session->set_flashdata('danger','Problemas al realizar la consulta, contactese con el Web Master');
            redirect('reportes_mae/region');
        }
    }
    public function reporte_acciones_por_region($p1,$p2,$p3,$p4,$region_id,$mes_id)
    {
        $mess=$this->get_mes($mes_id);
        $mes = $mess[1];
        $dias = date('d');//$mess[2];
        $sql_in='';
        if($p1==1){$tp_1=1;}else{$tp_1=0;}
        if($p2==1){$tp_2=2;}else{$tp_2=0;}
        if($p3==1){$tp_3=3;}else{$tp_3=0;}
        if($p4==1){$tp_4=4;}else{$tp_4=0;}
        $sql_in='in ('.$tp_1.','.$tp_2.','.$tp_3.','.$tp_4.')';
        if($region_id==-1){
            $detalles=$this->list_ejecucion_financiera_regiones_todos($p1,$p2,$p3,$p4,$sql_in,2,$mes_id);
            $region_nombre='(TODAS)';
        }
        else{
            $detalles=$this->list_ejecucion_financiera_x_region($p1,$p2,$p3,$p4,$sql_in,$region_id,2,$mes_id);
            $region = $this->model_taqpacha->get_region($region_id);
            $region_nombre=strtoupper($region[0]['reg_region']);
        }

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_resumen.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> EJECUCI&Oacute;N DE ACCIONES OPERATIVAS POR REGI&Oacute;N / '.$region_nombre.'<br>
                            (Al '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$detalles.'
            </body>
        </html>';

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("ejecucion_acciones_operativas_regional.pdf", array("Attachment" => false));
    }
    function list_ejecucion_financiera_regiones_todos($p1,$p2,$p3,$p4,$sql_in,$tp_rep,$mes_id){
        if($tp_rep==1){
            $tb='id="dt_basic_float" class="table table-bordered"';
        }
        elseif ($tp_rep==2) {
            $tb='border="1" cellpadding="0" cellspacing="0" class="table table table-bordered" width="100%"';
        }

        $nro=0;
        $tabla ='';
        if($tp_rep==1) {
            $tabla .= '<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h3 class="alert alert-success"><center>EJECUCI&Oacute;N DE ACCIONES OPERATÍVAS POR REGIÓN </center></h3>
                  </article>
                  <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                        <header>
                          <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                          <h2 class="font-md"><strong>TODAS LAS REGIONES</strong></h2>  
                        </header>
                	<div>
                  <div class="widget-body no-padding">
                  <div class="table-responsive">';
        }
        $regiones = $this->model_taqpacha->get_regiones_sinMulti();
        if(count($regiones)!=0){
            $nro++;
            $tabla .='<table '.$tb.'>';
            $tabla .='<thead>';
            $tabla .='<tr><th class="titulo_cabecera" style="width:1%;" rowspan="2">#</th>
				          <th class="titulo_cabecera" style="width:15%;" rowspan="2">REGIÓN</th>';
            if($p1==1)
                $tabla .='<th class="titulo_cabecera" style="width:15%;" rowspan="2">NRO DE PROYECTOS</th>';
            if($p2==1)
                $tabla.='<th class="titulo_cabecera" style="width:20%;" rowspan="2">NRO DE PROGRAMAS RECURRENTES</th>';
            if($p3==1)
                $tabla.='<th class="titulo_cabecera" style="width:10%;" rowspan="2">NRO DE PROGRAMAS NO RECURRENTES</th>';
            if($p4==1)
                $tabla .='<th class="titulo_cabecera" rowspan="2">NRO. OP. DE FUNCIONAMIENTO </th>';

            $tabla .='<th colspan="5" class="cabecera">PRESUPUESTO GESTI&Oacute;N '.$this->gestion.'</th></tr>';
            $tabla .='<tr class="modo1">';
            $tabla .='<th class="cabecera">INICIAL </th>';
            $tabla .='<th class="cabecera">MODIFICACIONES </th>';
            $tabla .='<th class="cabecera">VIGENTE </th>';
            $tabla .='<th class="cabecera">EJECUTADO </th>';
            $tabla .='<th class="cabecera">% DE EJECUCIÓN PRESUPUESTARIA </th>';
            $tabla .='</tr>';
            $tabla .='</thead>';
            $nrop=0;
            $tabla .='<tbody>';
            $total_pi=0;$total_pr=0;$total_pnr=0;$total_of=0;
            $total_mm_pi=0;$total_mm_pr=0;$total_mm_pnr=0;$total_mm_of=0;
            $total_pemm_pini=0;$total_pemm_pmod=0;$total_pemm_pvig=0;$total_pemm_peje=0;
            $total_gral_pe_pini=0;$total_gral_pe_pmod=0;$total_gral_pe_pvig=0;$total_gral_pe_peje=0;
            $proy_mult[]=array();
            foreach($regiones as $row_reg)
            {
                $nrop++;
                $tabla .='<tr class="derecha">';
                $tabla .='<td class="centro">'.$nrop.'</td>';
                $tabla .='<td class="izquierda">'.$row_reg['reg_region'].'</td>';
                $proyectos=$this->model_taqpacha->proyectos_x_region($row_reg['reg_id'],$this->gestion,$mes_id,$sql_in);
                if(count($proyectos)!=0){
                    $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                    $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
                    foreach($proyectos as $rowp){
                        $muni_proy=$this->model_taqpacha->regiones_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                        if($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']>1){//Multiregional
                            if(count($proy_mult)<=0)
                                $proy_mult[]=$rowp['proy_id'];

                            $encontrado=0;
                            for($i=0;$i<count($proy_mult);$i++){
                                if($proy_mult[$i]==$rowp['proy_id']){
                                    $encontrado=1;
                                    break;
                                }
                            }
                            if ($encontrado==0){
                                $proy_mult[]=$rowp['proy_id'];
                                $total_pemm_pini=$total_pemm_pini+$rowp['pe_pi'];
                                $total_pemm_pmod=$total_pemm_pmod+$rowp['pe_pm'];
                                $total_pemm_pvig=$total_pemm_pvig+$rowp['pe_pv'];
                                $total_pemm_peje=$total_pemm_peje+$rowp['pe_pe'];
                                if($rowp['tp_id']==1){//Proyecto de Inversión
                                    $total_mm_pi=$total_mm_pi+1;
                                }elseif($rowp['tp_id']==2){//Programa Recurrente
                                    $total_mm_pr=$total_mm_pr+1;
                                }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                                    $total_mm_pnr=$total_mm_pnr+1;
                                }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                                    $total_mm_of=$total_mm_of+1;
                                }
                            }
                        }else{ //Unica región
                            $total_pe_pini=$total_pe_pini+$rowp['pe_pi'];
                            $total_pe_pmod=$total_pe_pmod+$rowp['pe_pm'];
                            $total_pe_pvig=$total_pe_pvig+$rowp['pe_pv'];
                            $total_pe_peje=$total_pe_peje+$rowp['pe_pe'];
                            if($rowp['pe_pv']!=0){
                                $total_pe_porc=$rowp['pe_pe']/$rowp['pe_pv']*100;
                            }
                            if($rowp['tp_id']==1){//Proyecto de Inversión
                                $tot_pi=$tot_pi+1;
                            }elseif($rowp['tp_id']==2){//Programa Recurrente
                                $tot_pr=$tot_pr+1;
                            }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                                $tot_pnr=$tot_pnr+1;
                            }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                                $tot_of=$tot_of+1;
                            }
                        }
                    }
                }else{
                    $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                    $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
                }
                $total_pe_porc=0;
                if($total_pe_pvig!=0){
                    $total_pe_porc=$total_pe_peje/$total_pe_pvig*100;
                }
                if($p1==1)
                    $tabla .='<td class="pi">'.$tot_pi.'</td>';
                if($p2==1)
                    $tabla .='<td class="pr">'.$tot_pr.'</td>';
                if($p3==1)
                    $tabla .='<td class="pnr">'.$tot_pnr.'</td>';
                if($p4==1)
                    $tabla .='<td class="of">'.$tot_of.'</td>';

                $tabla .='<td>'.number_format($total_pe_pini, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_pmod, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_pvig, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_peje, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_porc, 2, ',', '.').'%</td>';
                $tabla .='</tr>';
                $total_pi =$total_pi+$tot_pi;
                $total_pr =$total_pr+$tot_pr;
                $total_pnr=$total_pnr+$tot_pnr;
                $total_of =$total_of+$tot_of;
                $total_gral_pe_pini=$total_gral_pe_pini+$total_pe_pini;
                $total_gral_pe_pmod=$total_gral_pe_pmod+$total_pe_pmod;
                $total_gral_pe_pvig=$total_gral_pe_pvig+$total_pe_pvig;
                $total_gral_pe_peje=$total_gral_pe_peje+$total_pe_peje;
            }
            /*Linea multiregional*/
            $proyectos=$this->model_taqpacha->proyectos_x_region(8,$this->gestion,$mes_id,$sql_in);
            if(count($proyectos)>=1){
                $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                foreach($proyectos as $rowp){
                    $muni_proy=$this->model_taqpacha->regiones_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                    if($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']==1){//Multiregional
                        $total_pemm_pini=$total_pemm_pini+$rowp['pe_pi'];
                        $total_pemm_pmod=$total_pemm_pmod+$rowp['pe_pm'];
                        $total_pemm_pvig=$total_pemm_pvig+$rowp['pe_pv'];
                        $total_pemm_peje=$total_pemm_peje+$rowp['pe_pe'];
                        if($rowp['tp_id']==1){//Proyecto de Inversión
                            $total_mm_pi=$total_mm_pi+1;
                        }elseif($rowp['tp_id']==2){//Programa Recurrente
                            $total_mm_pr=$total_mm_pr+1;
                        }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                            $total_mm_pnr=$total_mm_pnr+1;
                        }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                            $total_mm_of=$total_mm_of+1;
                        }
                    }
                }
            }
            $nrop++;
            $total_pemm_porc=0;
            if($total_pemm_pvig!=0){
                $total_pemm_porc=$total_pemm_peje/$total_pemm_pvig*100;
            }

            $tabla .='<tr class="multiple">';
            $tabla .='<td>'.$nrop.'</td>';
            $tabla .='<td class="izquierda">Multiregional</td>';
            if($p1==1)
                $tabla .='<td class="centro">'.$total_mm_pi.'</td>';
            if($p2==1)
                $tabla .='<td class="centro">'.$total_mm_pr.'</td>';
            if($p3==1)
                $tabla .='<td class="centro">'.$total_mm_pnr.'</td>';
            if($p4==1)
                $tabla .='<td class="centro">'.$total_mm_of.'</td>';

            $tabla .='<td>'.number_format($total_pemm_pini, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_pemm_pmod, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_pemm_pvig, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_pemm_peje, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_pemm_porc, 2, ',', '.').'%</td>';
            $tabla .='</tr>';

            $total_pi =$total_pi+$tot_pi+$total_mm_pi;
            $total_pr =$total_pr+$tot_pr+$total_mm_pr;
            $total_pnr=$total_pnr+$tot_pnr+$total_mm_pnr;
            $total_of =$total_of+$tot_of+$total_mm_of;


            $total_gral_pe_pini=$total_gral_pe_pini+$total_pemm_pini;
            $total_gral_pe_pmod=$total_gral_pe_pmod+$total_pemm_pmod;
            $total_gral_pe_pvig=$total_gral_pe_pvig+$total_pemm_pvig;
            $total_gral_pe_peje=$total_gral_pe_peje+$total_pemm_peje;
            $total_gral_pe_porc=0;
            if($total_gral_pe_pvig!=0){$total_gral_pe_porc=$total_gral_pe_peje/$total_gral_pe_pvig*100;}

            $tabla .='</tbody>';
            $tabla .='<tr class="total_general">';
            $tabla .='<td colspan="2" class="izquierda"><b>TOTAL : '.$nrop.'</b></td>';
            if($p1==1)
                $tabla .='<td class="centro">'.$total_pi.'</td>';
            if($p2==1)
                $tabla .='<td class="centro">'.$total_pr.'</td>';
            if($p3==1)
                $tabla .='<td class="centro">'.$total_pnr.'</td>';
            if($p4==1)
                $tabla .='<td class="centro">'.$total_of.'</td>';

            $tabla .='<td >'.number_format($total_gral_pe_pini, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pmod, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pvig, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_peje, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_porc, 2, ',', '.').'%</td>';
            $tabla .='</tr>';
            $tabla .='</table>';
            if($tp_rep==1) {
                $tabla .= '</div>
                </div>
              </div>
            </article>';
            }
        }

        return $tabla;
    }
    function list_ejecucion_financiera_x_region($p1,$p2,$p3,$p4,$sql_in,$reg_id,$tp,$mes_id)
    {
        if($tp==1){
            $tb='id="dt_basic_float" class="table table-bordered"';
        }
        elseif ($tp==2) {
            $tb='border="1" cellpadding="0" cellspacing="0" class="table table-bordered" width="100%"';
        }
        $sum=0;
        $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
        $acum_pi=0;$acum_pr=0;$acum_pnr=0;$acum_of=0;
        $acum_pi_fis=0;$acum_pr_fis=0;$acum_pnr_fis=0;$acum_of_fis=0;
        $tot_cost_pi=0;$tot_cost_pr=0;$tot_cost_pnr=0;$tot_cost_of=0;
        $tot_mod_pi=0;$tot_mod_pr=0;$tot_mod_pnr=0;$tot_mod_of=0;
        $tot_ctv_pi=0;$tot_ctv_pr=0;$tot_ctv_pnr=0;$tot_ctv_of=0;
        $tot_epa_pi=0;$tot_epa_pr=0;$tot_epa_pnr=0;$tot_epa_of=0;
        $tot_pi_pi=0;$tot_pi_pr=0;$tot_pi_pnr=0;$tot_pi_of=0;
        $tot_pm_pi=0;$tot_pm_pr=0;$tot_pm_pnr=0;$tot_pm_of=0;
        $tot_pv_pi=0;$tot_pv_pr=0;$tot_pv_pnr=0;$tot_pv_of=0;
        $tot_pe_pi=0;$tot_pe_pr=0;$tot_pe_pnr=0;$tot_pe_of=0;

        $tot_pi_m=0;$tot_pr_m=0;$tot_pnr_m=0;$tot_of_m=0;
        $tot_cost_pi_m=0;$tot_cost_pr_m=0;$tot_cost_pnr_m=0;$tot_cost_of_m=0;
        $tot_mod_pi_m=0;$tot_mod_pr_m=0;$tot_mod_pnr_m=0;$tot_mod_of_m=0;
        $tot_ctv_pi_m=0;$tot_ctv_pr_m=0;$tot_ctv_pnr_m=0;$tot_ctv_of_m=0;
        $tot_epa_pi_m=0;$tot_epa_pr_m=0;$tot_epa_pnr_m=0;$tot_epa_of_m=0;
        $tot_pi_pi_m=0;$tot_pi_pr_m=0;$tot_pi_pnr_m=0;$tot_pi_of_m=0;
        $tot_pm_pi_m=0;$tot_pm_pr_m=0;$tot_pm_pnr_m=0;$tot_pm_of_m=0;
        $tot_pv_pi_m=0;$tot_pv_pr_m=0;$tot_pv_pnr_m=0;$tot_pv_of_m=0;
        $tot_pe_pi_m=0;$tot_pe_pr_m=0;$tot_pe_pnr_m=0;$tot_pe_of_m=0;

        $acumulado=0;$acumulado_multi=0;
        $acumulado_efis=0;$acumulado_multi_efis=0;
        $tabla ='';
        $tab_aux='';
        $tabla.='<table '.$tb.'>';
        $tabla.='<thead>';
        $tabla.='<tr>
				<th class="titulo_cabecera" style="width:1%;" rowspan="2">Nro.</th>
				<th class="titulo_cabecera" style="width:8%;" rowspan="2">NOMBRE</th>
				<th class="titulo_cabecera" style="width:5%;" rowspan="2">PROVINCIA <br>Y<br> MUNICIPIO(S)</th>
				<th class="titulo_cabecera" style="width:4%;" rowspan="2">TIPO</th>
				<th class="cabecera" colspan="3">COSTO TOTAL DE LA OPERACI&Oacute;N</th>
				<th class="cabecera_acum" colspan="3">EJECUCIÓN ACUMULADA</th>
	    		<th class="cabecera" colspan="5">PRESUPUESTO GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>
	    		<th class="cabecera_acum" colspan="3">EJECUCI&Oacute;N FÍSICA '.$this->session->userdata('gestion').'</th>
	    		<th class="titulo_cabecera" style="width:5%;" rowspan="2">ESTADO</th>
	    		<th class="titulo_cabecera" style="width:6%;" rowspan="2">UNIDAD EJECUTORA</th>
	    		<th class="titulo_cabecera" style="width:6%;" rowspan="2">RESPONSABLE</th>
			</tr>';

        $tabla.='<tr class="modo1">
				<th class="cabecera" style="width:6%;">INICIAL</th>
				<th class="cabecera" style="width:5%;">MODIFICACIONES</th>
				<th class="cabecera" style="width:6%;">VIGENTE</th>
				<th class="cabecera_acum" style="width:5%;">EJECUCIÓN FINANCIERA</th>
				<th class="cabecera_acum" style="width:5%;">% EJEC. FINANCIERA</th>
				<th class="cabecera_acum" style="width:5%;">% DE AVANCE FÍSICO</th>';
        $tabla.='<th class="cabecera" style="width:5%;"> INICIAL </th>';
        $tabla.='<th class="cabecera" style="width:5%;"> MODIFICADO </th>';
        $tabla.='<th class="cabecera" style="width:5%;"> VIGENTE </th>';
        $tabla.='<th class="cabecera" style="width:5%;">EJEC. FINANCIERA </th>';
        $tabla.='<th class="cabecera" style="width:5%;">% EJEC. FINANCIERA </th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">PROG. %</th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">EJEC %</th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">% EJEC. ANUAL</th>';
        $tabla.='</tr>';
        $tabla.='</thead>';
        $tabla.='<tbody>';

        $programas = $this->model_epresupuestaria->ejecucion_proyecto_programas_x_region($sql_in,$this->gestion,$mes_id,$reg_id);
        if(count($programas)!=0){
            $nro=0;
            $nro_mult=0;
            foreach($programas as $rowp)
            {
                if($reg_id!=8){
                    $muni_proy=$this->model_taqpacha->regiones_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                    $color_mult='';
                    if($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']>1){//Multiregional
                        $color_mult='style="background-color:#EDFBFF"';
                    }

                    $se=$rowp['pe_pe'];
                    $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                    if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                        $se=$se+$suma[0]['total_ejecutado'];
                    }

                    $suma_prog=$this->mdas_complementario->costo_total_proyecto($rowp['pfec_id']);
                    $toal_modif = $this->mdas_complementario->modificaciones_costo_total($rowp['pfec_id']);
                    $estado = $this->mdas_complementario->estado_operacion($rowp['pfec_id'],$mes_id,$this->gestion);
                    $efis = $this->avance_fisico($rowp['proy_id'],$mes_id);

                    if($color_mult=='') {
                        $tabla .= '<tr class="modo1 derecha">';
                        $nro = $nro + 1;
                        $tabla .= '<td class="centro">' . $nro . '</td>';
                        $tabla .= '<td class="izquierda"><a href="javascript:abreVentana(\'' . site_url("reportes_mae") . '/resumen_ejec/' . $rowp['proy_id'] . '/' . $mes_id . '\');"' . '>' . $rowp['proy_nombre'] . '</a></td>';
                        $tabla .= '<td class="izquierda">';
                        $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);
                        if((count($prov)>3) and ($tp == 1)){
                            $tabla .= '<p class="btn btn-default" href="#" class="form_pond" title="';
                        }elseif($tp ==2){
                            $tabla .= '<div style="font-size: 5px;">';
                        }
                        foreach($prov as $row_prov){
                            $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                            if (count($prov) > 3) {
                                if ($tp == 1) {
                                    foreach ($muni as $muni) {
                                        $tabla .= $row_prov['prov_provincia'] .'&#09;'. $muni['muni_municipio'] . '&#13;';
                                    }
                                } else {
                                    $tabla .='<ul>'.$row_prov['prov_provincia'];
                                    foreach ($muni as $muni) {
                                        $tabla .= '<li>'.$muni['muni_municipio'] . '</li>';
                                    }
                                    $tabla .= '</ul>';
                                }
                            } else {
                                $tabla .= '<ul><strong>'.$row_prov['prov_provincia'].'</strong>';
                                foreach ($muni as $muni) {
                                    $tabla .= '<li>'.$muni['muni_municipio'].'</li>';
                                }
                                $tabla .= '</ul>';
                            }
                        }
                        if((count($prov) > 3) and ($tp == 1)){
                            $tabla .= '"><i class="glyphicon glyphicon-search"></i>Ver...</p>';
                        }elseif($tp==2){
                            $tabla .= '</div>';
                        }
                        $tabla .= '</td>';
                        $tabla .= '<td class="izquierda">' . $rowp['tp_tipo'] . '</td>';
                        $tabla .= '<td>'.number_format($suma_prog[0]['total_programado'],2,',','.') . '</td>';

                        $total_mod = 0;
                        if ($toal_modif[0]['total_modif'] != '' || $toal_modif[0]['total_modif'] != 0) {
                            $total_mod = $toal_modif[0]['total_modif'];
                        }
                        $tabla .= '<td>' . number_format($total_mod,2,',','.') . '</td>';
                        $ctv = $suma_prog[0]['total_programado'] + $total_mod;
                        $tabla .= '<td>' . number_format($ctv,2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($se,2,',','.') . '</td>';

                        $efin = 0;
                        if ($suma_prog[0]['total_programado'] != 0) {
                            $efin = round((($se / $suma_prog[0]['total_programado']) * 100),2);
                        }
                        $acumulado_efis=$acumulado_efis+$efis;
                        $tabla .= '<td>' . $efin . '%</td>';
                        $tabla .= '<td>' . round($efis,2) . '%</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pi'],2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pm'],2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pv'],2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pe'],2,',','.') . '</td>';

                        $ejec = 0;
                        if ($rowp['pe_pv'] != 0) {
                            $ejec = round((($rowp['pe_pe'] / $rowp['pe_pv']) * 100),2);
                        }
                        $tabla .= '<td>' . $ejec . '%</td>';
                        $estado_proy = 'No registrado';
                        if (count($estado) != 0) {
                            $estado_proy = $estado[0]['ep_descripcion'];
                        }
                        $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));

                        $tabla .= '<td class="derecha">'.$ejecutado[0].'</td>';
                        $tabla .= '<td class="derecha">'.$ejecutado[1].'</td>';
                        $tabla .= '<td class="derecha">'.$ejecutado[2].'</td>';
                        $acumulado=$acumulado+$ejecutado[2];
                        $tabla .= '<td class="izquierda">' . $estado_proy . '</td>';
                        $tabla .= '<td class="izquierda">' . $rowp['uni_unidad'] . '</td>';
                        $tabla .= '<td class="izquierda">' . $rowp['responsable'] . '<br><font color="red">Telf: ' . $rowp['fun_telefono'] . '</font></td>';
                        $tabla .= '</tr>';

                        if ($rowp['tp_id'] == 1) {//Proyecto de Inversión
                            $tot_pi = $tot_pi + 1;
                            $acum_pi=$acum_pi+$ejecutado[2];
                            $acum_pi_fis=$acum_pi_fis+$efis;
                            $tot_cost_pi = $tot_cost_pi + $suma_prog[0]['total_programado'];
                            $tot_mod_pi = $tot_mod_pi + $total_mod;
                            $tot_ctv_pi = $tot_ctv_pi + $ctv;
                            $tot_epa_pi = $tot_epa_pi + $se;
                            $tot_pi_pi = $tot_pi_pi + $rowp['pe_pi'];
                            $tot_pm_pi = $tot_pm_pi + $rowp['pe_pm'];
                            $tot_pv_pi = $tot_pv_pi + $rowp['pe_pv'];
                            $tot_pe_pi = $tot_pe_pi + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 2) {//Programa Recurrente
                            $tot_pr = $tot_pr + 1;
                            $acum_pr=$acum_pr+$ejecutado[2];
                            $acum_pr_fis=$acum_pr_fis+$efis;
                            $tot_cost_pr = $tot_cost_pr + $suma_prog[0]['total_programado'];
                            $tot_mod_pr = $tot_mod_pr + $total_mod;
                            $tot_ctv_pr = $tot_ctv_pr + $ctv;
                            $tot_epa_pr = $tot_epa_pr + $se;
                            $tot_pi_pr = $tot_pi_pr + $rowp['pe_pi'];
                            $tot_pm_pr = $tot_pm_pr + $rowp['pe_pm'];
                            $tot_pv_pr = $tot_pv_pr + $rowp['pe_pv'];
                            $tot_pe_pr = $tot_pe_pr + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 3) {//Programa No  Recurrente
                            $tot_pnr = $tot_pnr + 1;
                            $acum_pnr=$acum_pnr+$ejecutado[2];
                            $acum_pnr_fis=$acum_pnr_fis+$efis;
                            $tot_cost_pnr = $tot_cost_pnr + $suma_prog[0]['total_programado'];
                            $tot_mod_pnr = $tot_mod_pnr + $total_mod;
                            $tot_ctv_pnr = $tot_ctv_pnr + $ctv;
                            $tot_epa_pnr = $tot_epa_pnr + $se;
                            $tot_pi_pnr = $tot_pi_pnr + $rowp['pe_pi'];
                            $tot_pm_pnr = $tot_pm_pnr + $rowp['pe_pm'];
                            $tot_pv_pnr = $tot_pv_pnr + $rowp['pe_pv'];
                            $tot_pe_pnr = $tot_pe_pnr + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 4) {//Operacion de Funcionamiento
                            $tot_of = $tot_of + 1;
                            $acum_of=$acum_of+$ejecutado[2];
                            $acum_of_fis=$acum_of_fis+$efis;
                            $tot_cost_of = $tot_cost_of + $suma_prog[0]['total_programado'];
                            $tot_mod_of = $tot_mod_of + $total_mod;
                            $tot_ctv_of = $tot_ctv_of + $ctv;
                            $tot_epa_of = $tot_epa_of + $se;
                            $tot_pi_of = $tot_pi_of + $rowp['pe_pi'];
                            $tot_pm_of = $tot_pm_of + $rowp['pe_pm'];
                            $tot_pv_of = $tot_pv_of + $rowp['pe_pv'];
                            $tot_pe_of = $tot_pe_of + $rowp['pe_pe'];
                        }
                    }else{
                        /**Multi**/
                        $nro_mult=$nro_mult+1;
                        $tab_aux .='<tr class="modo1 derecha" ' . $color_mult . '>';
                        $tab_aux .= '<td class="centro">' . $nro_mult . '</td>';
                        $tab_aux .= '<td class="izquierda"><a href="javascript:abreVentana(\'' . site_url("reportes_mae") . '/resumen_ejec/' . $rowp['proy_id'] . '/' . $mes_id . '\');"' . '>' . $rowp['proy_nombre'] . '</a></td>';
                        $tab_aux .= '<td class="izquierda">';
                        $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);
                        if((count($prov) > 3) and ($tp == 1)){
                            $tab_aux .= '<p class="btn btn-default" href="#" class="form_pond" title="';
                        }elseif($tp ==2){
                            $tab_aux .= '<div style="font-size: 4px;">';
                        }
                        foreach($prov as $row_prov){
                            $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                            if (count($prov) > 3) {
                                if ($tp == 1) {
                                    foreach ($muni as $muni) {
                                        $tab_aux .= $row_prov['prov_provincia'] .'&#09;'. $muni['muni_municipio'] . '&#13;';
                                    }
                                } else {
                                    $tab_aux .='<ul>'.$row_prov['prov_provincia'];
                                    foreach ($muni as $muni) {
                                        $tab_aux .= '<li>'.$muni['muni_municipio'].'</li>';
                                    }
                                    $tab_aux .= '</ul>';
                                }
                            } else {
                                $tab_aux .= '<ul><strong>'.$row_prov['prov_provincia'].'</strong>';
                                foreach ($muni as $muni) {
                                    $tab_aux .= '<li>'.$muni['muni_municipio'].'</li>';
                                }
                                $tab_aux .= '</ul>';
                            }
                        }
                        if((count($prov) > 3) and ($tp == 1)){
                            $tab_aux .= '"><i class="glyphicon glyphicon-search"></i>Ver...</p>';
                        }elseif($tp==2){
                            $tab_aux .= '</div>';
                        }

                        $tab_aux .= '</td>';
                        $tab_aux .= '<td class="izquierda">' . $rowp['tp_tipo'] . '</td>';
                        $tab_aux .= '<td>' . number_format($suma_prog[0]['total_programado'],2,',','.') . '</td>';

                        $total_mod = 0;
                        if ($toal_modif[0]['total_modif'] != '' || $toal_modif[0]['total_modif'] != 0) {
                            $total_mod = $toal_modif[0]['total_modif'];
                        }
                        $tab_aux .= '<td>' . number_format($total_mod,2,',','.') . '</td>';
                        $ctv = $suma_prog[0]['total_programado'] + $total_mod;
                        $tab_aux .= '<td>' . number_format($ctv,2,',','.') . '</td>';
                        $tab_aux .= '<td>' . number_format($se,2,',','.') . '</td>';

                        $efin = 0;
                        if ($suma_prog[0]['total_programado'] != 0) {
                            $efin = round((($se / $suma_prog[0]['total_programado']) * 100),2);
                        }
                        $acumulado_multi_efis=$acumulado_multi_efis+$efis;
                        $tab_aux .= '<td>' . $efin . '%</td>';
                        $tab_aux .= '<td>' . round($efis,2) . '%</td>';
                        $tab_aux .= '<td>' . number_format($rowp['pe_pi'],2,',','.') . '</td>';
                        $tab_aux .= '<td>' . number_format($rowp['pe_pm'],2,',','.') . '</td>';
                        $tab_aux .= '<td>' . number_format($rowp['pe_pv'],2,',','.') . '</td>';
                        $tab_aux .= '<td>' . number_format($rowp['pe_pe'],2,',','.') . '</td>';

                        $ejec = 0;
                        if ($rowp['pe_pv'] != 0) {
                            $ejec = round((($rowp['pe_pe'] / $rowp['pe_pv']) * 100),2);
                        }
                        $tab_aux .= '<td>' . $ejec . '%</td>';
                        $estado_proy = 'No registrado';
                        if (count($estado) != 0) {
                            $estado_proy = $estado[0]['ep_descripcion'];
                        }
                        $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));
                        $tab_aux .= '<td class="derecha">'.$ejecutado[0].'</td>';
                        $tab_aux .= '<td class="derecha">'.$ejecutado[1].'</td>';
                        $tab_aux .= '<td class="derecha">'.$ejecutado[2].'</td>';
                        $acumulado_multi=$acumulado_multi+$ejecutado[2];
                        $tab_aux .= '<td class="izquierda">' . $estado_proy . '</td>';
                        $tab_aux .= '<td class="izquierda">' . $rowp['uni_unidad'] . '</td>';
                        $tab_aux .= '<td class="izquierda">' . $rowp['responsable'] . '<br><font color="red">Telf: ' . $rowp['fun_telefono'] . '</font></td>';
                        $tab_aux .= '</tr>';
                        if ($rowp['tp_id'] == 1) {//Proyecto de Inversión
                            $tot_pi_m = $tot_pi_m + 1;
                            $tot_cost_pi_m = $tot_cost_pi_m + $suma_prog[0]['total_programado'];
                            $tot_mod_pi_m = $tot_mod_pi_m + $total_mod;
                            $tot_ctv_pi_m = $tot_ctv_pi_m + $ctv;
                            $tot_epa_pi_m = $tot_epa_pi_m + $se;
                            $tot_pi_pi_m = $tot_pi_pi_m + $rowp['pe_pi'];
                            $tot_pm_pi_m = $tot_pm_pi_m + $rowp['pe_pm'];
                            $tot_pv_pi_m = $tot_pv_pi_m + $rowp['pe_pv'];
                            $tot_pe_pi_m = $tot_pe_pi_m + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 2) {//Programa Recurrente
                            $tot_pr_m = $tot_pr_m + 1;
                            $tot_cost_pr_m = $tot_cost_pr_m + $suma_prog[0]['total_programado'];
                            $tot_mod_pr_m = $tot_mod_pr_m + $total_mod;
                            $tot_ctv_pr_m = $tot_ctv_pr_m + $ctv;
                            $tot_epa_pr_m = $tot_epa_pr_m + $se;
                            $tot_pi_pr_m = $tot_pi_pr_m + $rowp['pe_pi'];
                            $tot_pm_pr_m = $tot_pm_pr_m + $rowp['pe_pm'];
                            $tot_pv_pr_m = $tot_pv_pr_m + $rowp['pe_pv'];
                            $tot_pe_pr_m = $tot_pe_pr_m + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 3) {//Programa No  Recurrente
                            $tot_pnr_m = $tot_pnr_m + 1;
                            $tot_cost_pnr_m = $tot_cost_pnr_m + $suma_prog[0]['total_programado'];
                            $tot_mod_pnr_m = $tot_mod_pnr_m + $total_mod;
                            $tot_ctv_pnr_m = $tot_ctv_pnr_m + $ctv;
                            $tot_epa_pnr_m = $tot_epa_pnr_m + $se;
                            $tot_pi_pnr_m = $tot_pi_pnr_m + $rowp['pe_pi'];
                            $tot_pm_pnr_m = $tot_pm_pnr_m + $rowp['pe_pm'];
                            $tot_pv_pnr_m = $tot_pv_pnr_m + $rowp['pe_pv'];
                            $tot_pe_pnr_m = $tot_pe_pnr_m + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 4) {//Operacion de Funcionamiento
                            $tot_of_m = $tot_of_m + 1;
                            $tot_cost_of_m = $tot_cost_of_m + $suma_prog[0]['total_programado'];
                            $tot_mod_of_m = $tot_mod_of_m + $total_mod;
                            $tot_ctv_of_m = $tot_ctv_of_m + $ctv;
                            $tot_epa_of_m = $tot_epa_of_m + $se;
                            $tot_pi_of_m = $tot_pi_of_m + $rowp['pe_pi'];
                            $tot_pm_of_m = $tot_pm_of_m + $rowp['pe_pm'];
                            $tot_pv_of_m = $tot_pv_of_m + $rowp['pe_pv'];
                            $tot_pe_of_m = $tot_pe_of_m + $rowp['pe_pe'];
                        }
                    }
                }else{
                    $muni_proy=$this->model_taqpacha->regiones_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                    $color_mult='';
                    if(($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']>1) ||($rowp['region']==8)){//Multiregional
                        $se=$rowp['pe_pe'];
                        $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                        if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                            $se=$se+$suma[0]['total_ejecutado'];
                        }

                        $suma_prog=$this->mdas_complementario->costo_total_proyecto($rowp['pfec_id']);
                        $toal_modif = $this->mdas_complementario->modificaciones_costo_total($rowp['pfec_id']);
                        $estado = $this->mdas_complementario->estado_operacion($rowp['pfec_id'],$mes_id,$this->gestion);
                        $efis = $this->avance_fisico($rowp['proy_id'],$mes_id);
                        $tabla .= '<tr class="modo1 derecha">';
                        $nro = $nro + 1;
                        $tabla .= '<td class="centro">' . $nro . '</td>';
                        $tabla .= '<td class="izquierda"><a href="javascript:abreVentana(\'' . site_url("reportes_mae") . '/resumen_ejec/' . $rowp['proy_id'] . '/' . $mes_id . '\');"' . '>' . $rowp['proy_nombre'] . '</a></td>';
                        $tabla .= '<td class="izquierda">';
                        $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);
                        if((count($prov)>3) and ($tp == 1)){
                            $tabla .= '<p class="btn btn-default" href="#" class="form_pond" title="';
                        }elseif($tp ==2){
                            $tabla .= '<div style="font-size: 5px;">';
                        }
                        foreach($prov as $row_prov){
                            $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                            if (count($prov) > 3) {
                                if ($tp == 1) {
                                    foreach ($muni as $muni) {
                                        $tabla .= $row_prov['prov_provincia'] .'&#09;'. $muni['muni_municipio'] . '&#13;';
                                    }
                                } else {
                                    $tabla .='<ul>'.$row_prov['prov_provincia'];
                                    foreach ($muni as $muni) {
                                        $tabla .= '<li>'.$muni['muni_municipio'] . '</li>';
                                    }
                                    $tabla .= '</ul>';
                                }
                            } else {
                                $tabla .= '<ul><strong>'.$row_prov['prov_provincia'].'</strong>';
                                foreach ($muni as $muni) {
                                    $tabla .= '<li>'.$muni['muni_municipio'].'</li>';
                                }
                                $tabla .= '</ul>';
                            }
                        }
                        if((count($prov) > 3) and ($tp == 1)){
                            $tabla .= '"><i class="glyphicon glyphicon-search"></i>Ver...</p>';
                        }elseif($tp==2){
                            $tabla .= '</div>';
                        }
                        $tabla .= '</td>';
                        $tabla .= '<td class="izquierda">' . $rowp['tp_tipo'] . '</td>';
                        $tabla .= '<td>'.number_format($suma_prog[0]['total_programado'],2,',','.') . '</td>';

                        $total_mod = 0;
                        if ($toal_modif[0]['total_modif'] != '' || $toal_modif[0]['total_modif'] != 0) {
                            $total_mod = $toal_modif[0]['total_modif'];
                        }
                        $tabla .= '<td>' . number_format($total_mod,2,',','.') . '</td>';
                        $ctv = $suma_prog[0]['total_programado'] + $total_mod;
                        $tabla .= '<td>' . number_format($ctv,2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($se,2,',','.') . '</td>';

                        $efin = 0;
                        if ($suma_prog[0]['total_programado'] != 0) {
                            $efin = round((($se / $suma_prog[0]['total_programado']) * 100),2);
                        }
                        $tabla .= '<td>' . $efin . '%</td>';
                        $tabla .= '<td>' . round($efis,2) . '%</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pi'],2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pm'],2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pv'],2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pe'],2,',','.') . '</td>';

                        $ejec = 0;
                        if ($rowp['pe_pv'] != 0) {
                            $ejec = round((($rowp['pe_pe'] / $rowp['pe_pv']) * 100),2);
                        }
                        $tabla .= '<td>' . $ejec . '%</td>';
                        $estado_proy = 'No registrado';
                        if (count($estado) != 0) {
                            $estado_proy = $estado[0]['ep_descripcion'];
                        }
                        $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));
                        $tabla .= '<td class="izquierda">'.$ejecutado[0].'</td>';
                        $tabla .= '<td class="izquierda">'.$ejecutado[1].'</td>';
                        $tabla .= '<td class="izquierda">'.$ejecutado[2].'</td>';
                        $tabla .= '<td class="izquierda">' . $estado_proy . '</td>';
                        $tabla .= '<td class="izquierda">' . $rowp['uni_unidad'] . '</td>';
                        $tabla .= '<td class="izquierda">' . $rowp['responsable'] . '<br><font color="red">Telf: ' . $rowp['fun_telefono'] . '</font></td>';
                        $tabla .= '</tr>';

                        if ($rowp['tp_id'] == 1) {//Proyecto de Inversión
                            $tot_pi = $tot_pi + 1;
                            $tot_cost_pi = $tot_cost_pi + $suma_prog[0]['total_programado'];
                            $tot_mod_pi = $tot_mod_pi + $total_mod;
                            $tot_ctv_pi = $tot_ctv_pi + $ctv;
                            $tot_epa_pi = $tot_epa_pi + $se;
                            $tot_pi_pi = $tot_pi_pi + $rowp['pe_pi'];
                            $tot_pm_pi = $tot_pm_pi + $rowp['pe_pm'];
                            $tot_pv_pi = $tot_pv_pi + $rowp['pe_pv'];
                            $tot_pe_pi = $tot_pe_pi + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 2) {//Programa Recurrente
                            $tot_pr = $tot_pr + 1;
                            $tot_cost_pr = $tot_cost_pr + $suma_prog[0]['total_programado'];
                            $tot_mod_pr = $tot_mod_pr + $total_mod;
                            $tot_ctv_pr = $tot_ctv_pr + $ctv;
                            $tot_epa_pr = $tot_epa_pr + $se;
                            $tot_pi_pr = $tot_pi_pr + $rowp['pe_pi'];
                            $tot_pm_pr = $tot_pm_pr + $rowp['pe_pm'];
                            $tot_pv_pr = $tot_pv_pr + $rowp['pe_pv'];
                            $tot_pe_pr = $tot_pe_pr + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 3) {//Programa No  Recurrente
                            $tot_pnr = $tot_pnr + 1;
                            $tot_cost_pnr = $tot_cost_pnr + $suma_prog[0]['total_programado'];
                            $tot_mod_pnr = $tot_mod_pnr + $total_mod;
                            $tot_ctv_pnr = $tot_ctv_pnr + $ctv;
                            $tot_epa_pnr = $tot_epa_pnr + $se;
                            $tot_pi_pnr = $tot_pi_pnr + $rowp['pe_pi'];
                            $tot_pm_pnr = $tot_pm_pnr + $rowp['pe_pm'];
                            $tot_pv_pnr = $tot_pv_pnr + $rowp['pe_pv'];
                            $tot_pe_pnr = $tot_pe_pnr + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 4) {//Operacion de Funcionamiento
                            $tot_of = $tot_of + 1;
                            $tot_cost_of = $tot_cost_of + $suma_prog[0]['total_programado'];
                            $tot_mod_of = $tot_mod_of + $total_mod;
                            $tot_ctv_of = $tot_ctv_of + $ctv;
                            $tot_epa_of = $tot_epa_of + $se;
                            $tot_pi_of = $tot_pi_of + $rowp['pe_pi'];
                            $tot_pm_of = $tot_pm_of + $rowp['pe_pm'];
                            $tot_pv_of = $tot_pv_of + $rowp['pe_pv'];
                            $tot_pe_of = $tot_pe_of + $rowp['pe_pe'];
                        }
                    }
                }
            }
        }
        $tot_porc_pi=0;$tot_porc_pnr=0;$tot_porc_pr=0;$tot_porc_of=0;
        if($tot_pv_pi!=0){$tot_porc_pi  =$tot_pe_pi/$tot_pv_pi*100;}
        if($tot_pv_pnr!=0){$tot_porc_pnr=$tot_pe_pnr/$tot_pv_pnr*100;}
        if($tot_pv_pr!=0){$tot_porc_pr  =$tot_pe_pr/$tot_pv_pr*100;}
        if($tot_pv_of!=0){$tot_porc_of  =$tot_pe_of/$tot_pv_of*100;}

        $tot_pora_pi=0;$tot_pora_pnr=0;$tot_pora_pr=0;$tot_pora_of=0;
        if($tot_ctv_pi!=0){$tot_pora_pi=$tot_epa_pi/$tot_ctv_pi*100;}
        if($tot_ctv_pnr!=0){$tot_pora_pnr=$tot_epa_pnr/$tot_ctv_pnr*100;}
        if($tot_ctv_pr!=0){$tot_pora_pr=$tot_epa_pr/$tot_ctv_pr*100;}
        if($tot_ctv_of!=0){$tot_pora_of=$tot_epa_of/$tot_ctv_of*100;}

        $total_gral_op  =$tot_pi+$tot_pnr+$tot_pr+$tot_of;
        $total_gral_cost=$tot_cost_pi+$tot_cost_pnr+$tot_cost_pr+$tot_cost_of;
        $total_gral_mod =$tot_mod_pi+$tot_mod_pnr+$tot_mod_pr+$tot_mod_of;
        $total_gral_ctv =$tot_ctv_pi+$tot_ctv_pnr+$tot_ctv_pr+$tot_ctv_of;
        $total_gral_epa =$tot_epa_pi+$tot_epa_pnr+$tot_epa_pr+$tot_epa_of;
        $total_gral_pora=0;
        if($total_gral_ctv!=0){$total_gral_pora=$total_gral_epa/$total_gral_ctv*100;}

        $total_gral_pi  =$tot_pi_pi+$tot_pi_pnr+$tot_pi_pr+$tot_pi_of;
        $total_gral_pm  =$tot_pm_pi+$tot_pm_pnr+$tot_pm_pr+$tot_pm_of;
        $total_gral_pv  =$tot_pv_pi+$tot_pv_pnr+$tot_pv_pr+$tot_pv_of;
        $total_gral_pe  =$tot_pe_pi+$tot_pe_pnr+$tot_pe_pr+$tot_pe_of;
        $total_gral_porc=0;
        if($total_gral_pv!=0){$total_gral_porc=$total_gral_pe/$total_gral_pv*100;}

        /**Multiregionales**/
        $tot_porc_pi_m=0;$tot_porc_pnr_m=0;$tot_porc_pr_m=0;$tot_porc_of_m=0;
        if($tot_pv_pi_m!=0){$tot_porc_pi_m  =$tot_pe_pi_m/$tot_pv_pi_m*100;}
        if($tot_pv_pnr_m!=0){$tot_porc_pnr_m=$tot_pe_pnr_m/$tot_pv_pnr_m*100;}
        if($tot_pv_pr_m!=0){$tot_porc_pr_m  =$tot_pe_pr_m/$tot_pv_pr_m*100;}
        if($tot_pv_of_m!=0){$tot_porc_of_m  =$tot_pe_of_m/$tot_pv_of_m*100;}

        $tot_pora_pi_m=0;$tot_pora_pnr_m=0;$tot_pora_pr_m=0;$tot_pora_of_m=0;
        if($tot_ctv_pi_m!=0){$tot_pora_pi_m  =$tot_epa_pi_m/$tot_ctv_pi_m*100;}
        if($tot_ctv_pnr_m!=0){$tot_pora_pnr_m=$tot_epa_pnr_m/$tot_ctv_pnr_m*100;}
        if($tot_ctv_pr_m!=0){$tot_pora_pr_m  =$tot_epa_pr_m/$tot_ctv_pr_m*100;}
        if($tot_ctv_of_m!=0){$tot_pora_of_m  =$tot_epa_of_m/$tot_ctv_of_m*100;}

        $total_gral_op_m  =$tot_pi_m+$tot_pnr_m+$tot_pr_m+$tot_of_m;
        $total_gral_cost_m=$tot_cost_pi_m+$tot_cost_pnr_m+$tot_cost_pr_m+$tot_cost_of;
        $total_gral_mod_m =$tot_mod_pi_m+$tot_mod_pnr_m+$tot_mod_pr_m+$tot_mod_of_m;
        $total_gral_ctv_m =$tot_ctv_pi_m+$tot_ctv_pnr_m+$tot_ctv_pr_m+$tot_ctv_of_m;
        $total_gral_epa_m =$tot_epa_pi_m+$tot_epa_pnr_m+$tot_epa_pr_m+$tot_epa_of_m;
        $total_gral_pora_m=0;
        if($total_gral_ctv_m!=0){$total_gral_pora_m=$total_gral_epa_m/$total_gral_ctv_m*100;}

        $total_gral_pi_m  =$tot_pi_pi_m+$tot_pi_pnr_m+$tot_pi_pr_m+$tot_pi_of_m;
        $total_gral_pm_m  =$tot_pm_pi_m+$tot_pm_pnr_m+$tot_pm_pr_m+$tot_pm_of_m;
        $total_gral_pv_m  =$tot_pv_pi_m+$tot_pv_pnr_m+$tot_pv_pr_m+$tot_pv_of_m;
        $total_gral_pe_m  =$tot_pe_pi_m+$tot_pe_pnr_m+$tot_pe_pr_m+$tot_pe_of_m;
        $total_gral_porc_m=0;
        if($total_gral_pv_m!=0){$total_gral_porc_m=$total_gral_pe_m/$total_gral_pv_m*100;}
        $tabla.='</tbody>';
        if($p1==1) {
            $total_acum_pi=0;$total_acum_pi_fis=0;
            if($tot_pi!=0){
                $total_acum_pi=number_format($acum_pi/$tot_pi,2,',','.');
                $total_acum_pi_fis=number_format($acum_pi_fis/$tot_pi,2,',','.');
            }
            $tabla .= '<tr class="total_pi">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROYECTOS DE INVERSIÓN</td>';
            $tabla .= '<th >' . $tot_pi . '</th><th class="derecha">' . number_format($tot_cost_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pi,2,',','.') . ' %</th><th class="derecha">'.$total_acum_pi_fis.'</th>
                <th class="derecha">' . number_format($tot_pi_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pi,2,',','.') . '%</th><th ></th><th ></th><th class="derecha">'.$total_acum_pi.'</th><th colspan="3"></th></tr>';
        }
        if($p2==1) {
            $total_acum_pr=0;$total_acum_pr_fis=0;
            if($tot_pr!=0){
                $total_acum_pr=number_format($acum_pr/$tot_pr,2,',','.');
                $total_acum_pr_fis=number_format($acum_pr_fis/$tot_pr,2,',','.');
            }
            $tabla .= '<tr class="total_pr">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROGRAMAS RECURRENTES</td>';
            $tabla .= '<th >' . $tot_pr . '</th><th class="derecha">' . number_format($tot_cost_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pr,2,',','.') . ' %</th><th class="derecha">'.$total_acum_pr_fis.'</th>
                 <th class="derecha">' . number_format($tot_pi_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pr,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_pr.'</th><th colspan="3"></th></tr>';
        }
        if($p3==1) {
            $total_acum_pnr=0;$total_acum_prn_fis=0;
            if($tot_pnr!=0){
                $total_acum_pnr=number_format($acum_pnr/$tot_pnr,2,',','.');
                $total_acum_prn_fis=number_format($acum_pnr_fis/$tot_pnr,2,',','.');
            }

            $tabla .= '<tr class="total_pnr">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROGRAMAS NO RECURRENTES</td>';
            $tabla .= '<th >' . $tot_pnr . '</th><th class="derecha">' . number_format($tot_cost_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pnr,2,',','.') . ' %</th><th class="derecha">'.$total_acum_prn_fis.'</th>
                 <th class="derecha">' . number_format($tot_pi_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pnr,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_pnr.'</th><th colspan="3"></th></tr>';
        }
        if($p4==1) {
            $total_acum_of=0;$total_acum_of_fis=0;
            if($tot_of!=0){
                $total_acum_of=number_format($acum_of/$tot_of,2,',','.');
                $total_acum_of_fis=number_format($acum_of_fis/$tot_of,2,',','.');
            }

            $tabla .= '<tr class="total_of">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL OPERACIONES DE FUNCIONAMIENTO</td>';
            $tabla .= '<th >' . $tot_of . '</th><th class="derecha">' . number_format($tot_cost_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_of,2,',','.') . ' %</th><th class="derecha">'.$total_acum_of_fis.'</th>
                 <th class="derecha">' . number_format($tot_pi_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_of,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_of.'</th><th colspan="3"></th></tr>';
        }
        $total_acum=0;$total_acum_fis=0;
        if($total_gral_op!=0){
            $total_acum=number_format($acumulado/$total_gral_op, 2, ',', '.');
            $total_acum_fis=number_format($acumulado_efis/$total_gral_op, 2, ',', '.');
        }

        $tabla.='<tr class="total_general">';
        $tabla.='<th colspan="3" class="izquierda">TOTAL DE LA REGIÓN</td>';
        $tabla.='<th >'.$total_gral_op.'</th><th class="derecha">'.number_format($total_gral_cost, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_mod, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_ctv, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_epa, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pora, 2, ',', '.').' %</th><th class="derecha" >'.$total_acum_fis.'</th>
                 <th class="derecha">'.number_format($total_gral_pi, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pm, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pv, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pe, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_porc, 2, ',', '.').' %</th><th ></th><th ></th><th class="derecha">'.$total_acum.'</th><th colspan="3"></th></tr>';
        if($tab_aux!=''){
            $total_acum_multi=0;$total_acum_multi_fis=0;
            if($total_gral_op_m!=0){
                $total_acum_multi=number_format($acumulado_multi/$total_gral_op_m, 2, ',', '.');
                $total_acum_multi_fis=number_format($acumulado_multi_efis/$total_gral_op_m, 2, ',', '.');
            }

            $tabla.='<tr><br>
				<td colspan="17"><strong><center><h2>OPERACIONES MULTIREGIÓN</h2></center></strong></td></tr>'.$tab_aux;
            $tabla.='<tr class="total_multi"> <th colspan="3" class="izquierda">TOTAL MULTIREGIONAL</th>';
            $tabla.='<th >'.$total_gral_op_m.'</th><th class="derecha">'.number_format($total_gral_cost_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_mod_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_ctv_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_epa_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pora_m, 2, ',', '.').' %</th><th class="derecha">'.$total_acum_multi_fis.'</th>
                 <th class="derecha">'.number_format($total_gral_pi_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pm_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pv_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pe_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_porc_m, 2, ',', '.').' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_multi.'</th><th colspan="3"></th></tr>';

            $tabla.='<tr class="total_general">';
            $tabla.='<th colspan="3" class="izquierda">TOTAL GENERAL</th>';
            $gran_total=$total_gral_op+$total_gral_op_m;
            $gran_total_por_acum=0;
            if(($total_gral_ctv+$total_gral_ctv_m)!=0) {$gran_total_por_acum=($total_gral_epa+$total_gral_epa_m)/($total_gral_ctv+$total_gral_ctv_m)*100;}
            $gran_total_porc=0;
            if(($total_gral_pv+$total_gral_pv_m)!=0) {$gran_total_porc=($total_gral_pe+$total_gral_pe_m)/($total_gral_pv+$total_gral_pv_m)*100;}
            $gran_total_media=0;$gran_total_media_fis=0;
            if($gran_total!=0){
                $gran_total_media=number_format(($acumulado_multi+$acumulado)/$gran_total, 2, ',', '.');
                $gran_total_media_fis=number_format(($acumulado_multi_efis+$acumulado_efis)/$gran_total, 2, ',', '.');
            }

            $tabla.='<th >'.$gran_total.'</th><th class="derecha">'.number_format($total_gral_cost+$total_gral_cost_m, 2, ',', '.').'</th>
					<th class="derecha">'.number_format($total_gral_mod+$total_gral_mod_m, 2, ',', '.').'</th>
					<th class="derecha">'.number_format($total_gral_ctv+$total_gral_ctv_m, 2, ',', '.').'</th>
					<th class="derecha">'.number_format($total_gral_epa+$total_gral_epa_m, 2, ',', '.').'</th>
					<th class="derecha">'.number_format($gran_total_por_acum, 2, ',', '.').' %</th>
					<th class="derecha">'.$gran_total_media_fis.'</th>
                    <th class="derecha">'.number_format($total_gral_pi+$total_gral_pi_m, 2, ',', '.').'</th>
                    <th class="derecha">'.number_format($total_gral_pm+$total_gral_pm_m, 2, ',', '.').'</th>
                    <th class="derecha">'.number_format($total_gral_pv+$total_gral_pv_m, 2, ',', '.').'</th>
                    <th class="derecha">'.number_format($total_gral_pe+$total_gral_pe_m, 2, ',', '.').'</th>
                    <th class="derecha">'.number_format($gran_total_porc, 2, ',', '.').' %</th>
                    <th ></th><th ></th><th class="derecha">'.$gran_total_media.'</th><th colspan="3"></th></tr>';
        }
        $tabla.='</table><br>';
        return $tabla;
    }
    function excel_acciones_por_region($p1,$p2,$p3,$p4,$region_id,$mes_id){
        //$mess=$this->get_mes($mes_id);
        //$mes = $mess[1];
        //$dias = date('d');//$mess[2];
        $sql_in='';
        if($p1==1){$tp_1=1;}else{$tp_1=0;}
        if($p2==1){$tp_2=2;}else{$tp_2=0;}
        if($p3==1){$tp_3=3;}else{$tp_3=0;}
        if($p4==1){$tp_4=4;}else{$tp_4=0;}
        $sql_in='in ('.$tp_1.','.$tp_2.','.$tp_3.','.$tp_4.')';
        if($region_id==-1){
            $region_nombre="(TODAS)";
            $this->excel_region_todos($region_nombre,$p1,$p2,$p3,$p4,$sql_in,$mes_id);
        }
        else{
            $region = $this->model_taqpacha->get_region($region_id);
            $region_nombre=strtoupper($region[0]['reg_region']);
            $this->excel_x_region($region_nombre,$p1,$p2,$p3,$p4,$sql_in,$region_id,$mes_id);
        }
    }
    function excel_region_todos($region_nombre,$p1,$p2,$p3,$p4,$sql_in,$mes_id){
        $mess=$this->get_mes($mes_id);
        $mes = $mess[1];
        $nombre_mes='(AL MES DE '.$mes.' DE '.$this->gestion.') (Expresado en Bolivianos)';
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('REGIONES');
        $nro_fila=1;
        $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, 'EJECUCIÓN DE ACCIONES OPERATIVAS POR REGIÓN /'.$region_nombre);
        $nro_fila++;
        $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, $nombre_mes);$nro_fila++;
        $nro_fila++;

        $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, '#');
        $this->excel->getActiveSheet()->setCellValue('B'.$nro_fila, 'REGIÓN');
        $this->excel->getActiveSheet()->setCellValue('C'.$nro_fila, 'NRO DE PROYECTOS');
        $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, 'NRO DE PROGRAMAS RECURRENTES');
        $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, 'NRO DE PROGRAMAS NO RECURRENTES');
        $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, 'NRO. OP. DE FUNCIONAMIENTO');
        $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':K'.$nro_fila)->getFont()->setSize(11);
        $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':K'.$nro_fila)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A'.$nro_fila.':A5');
        $this->excel->getActiveSheet()->mergeCells('B'.$nro_fila.':B5');
        $this->excel->getActiveSheet()->mergeCells('C'.$nro_fila.':C5');
        $this->excel->getActiveSheet()->mergeCells('D'.$nro_fila.':D5');
        $this->excel->getActiveSheet()->mergeCells('E'.$nro_fila.':E5');
        $this->excel->getActiveSheet()->mergeCells('F'.$nro_fila.':F5');
        $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, 'PRESUPUESTO GESTIÓN '.$this->gestion);
        $this->excel->getActiveSheet()->mergeCells('G'.$nro_fila.':K'.$nro_fila);
        $nro_fila++;
        $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, 'INICIAL');
        $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, 'MODIFICACIONES');
        $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, 'VIGENTE');
        $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, 'EJECUTADO');
        $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, '% DE EJECUCIÓN PRESUPUESTARIA');
        $this->excel->getActiveSheet()->getStyle('G'.$nro_fila.':K'.$nro_fila)->getFont()->setSize(11);
        $this->excel->getActiveSheet()->getStyle('G'.$nro_fila.':K'.$nro_fila)->getFont()->setBold(true);


                 $regiones = $this->model_taqpacha->get_regiones_sinMulti();
                if(count($regiones)!=0){
                    $nrop=0;
                    $total_pi=0;$total_pr=0;$total_pnr=0;$total_of=0;
                    $total_mm_pi=0;$total_mm_pr=0;$total_mm_pnr=0;$total_mm_of=0;
                    $total_pemm_pini=0;$total_pemm_pmod=0;$total_pemm_pvig=0;$total_pemm_peje=0;
                    $total_gral_pe_pini=0;$total_gral_pe_pmod=0;$total_gral_pe_pvig=0;$total_gral_pe_peje=0;
                    $proy_mult[]=array();
                    foreach($regiones as $row_reg)
                    {
                        $nrop++;
                        $nro_fila++;
                        $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, $nrop);
                        $this->excel->getActiveSheet()->setCellValue('B'.$nro_fila, $row_reg['reg_region']);

                        $proyectos=$this->model_taqpacha->proyectos_x_region($row_reg['reg_id'],$this->gestion,$mes_id,$sql_in);
                        if(count($proyectos)!=0){
                            $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                            $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
                            foreach($proyectos as $rowp){
                                $muni_proy=$this->model_taqpacha->regiones_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                                if($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']>1){//Multiregional
                                    if(count($proy_mult)<=0)
                                        $proy_mult[]=$rowp['proy_id'];

                                    $encontrado=0;
                                    for($i=0;$i<count($proy_mult);$i++){
                                        if($proy_mult[$i]==$rowp['proy_id']){
                                            $encontrado=1;
                                            break;
                                        }
                                    }
                                    if ($encontrado==0){
                                        $proy_mult[]=$rowp['proy_id'];
                                        $total_pemm_pini=$total_pemm_pini+$rowp['pe_pi'];
                                        $total_pemm_pmod=$total_pemm_pmod+$rowp['pe_pm'];
                                        $total_pemm_pvig=$total_pemm_pvig+$rowp['pe_pv'];
                                        $total_pemm_peje=$total_pemm_peje+$rowp['pe_pe'];
                                        if($rowp['tp_id']==1){//Proyecto de Inversión
                                            $total_mm_pi=$total_mm_pi+1;
                                        }elseif($rowp['tp_id']==2){//Programa Recurrente
                                            $total_mm_pr=$total_mm_pr+1;
                                        }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                                            $total_mm_pnr=$total_mm_pnr+1;
                                        }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                                            $total_mm_of=$total_mm_of+1;
                                        }
                                    }
                                }else{ //Unica región
                                    $total_pe_pini=$total_pe_pini+$rowp['pe_pi'];
                                    $total_pe_pmod=$total_pe_pmod+$rowp['pe_pm'];
                                    $total_pe_pvig=$total_pe_pvig+$rowp['pe_pv'];
                                    $total_pe_peje=$total_pe_peje+$rowp['pe_pe'];
                                    if($rowp['pe_pv']!=0){
                                        $total_pe_porc=$rowp['pe_pe']/$rowp['pe_pv']*100;
                                    }
                                    if($rowp['tp_id']==1){//Proyecto de Inversión
                                        $tot_pi=$tot_pi+1;
                                    }elseif($rowp['tp_id']==2){//Programa Recurrente
                                        $tot_pr=$tot_pr+1;
                                    }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                                        $tot_pnr=$tot_pnr+1;
                                    }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                                        $tot_of=$tot_of+1;
                                    }
                                }
                            }
                        }else{
                            $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                            $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
                        }
                        $total_pe_porc=0;
                        if($total_pe_pvig!=0){
                            $total_pe_porc=$total_pe_peje/$total_pe_pvig*100;
                        }
                        $this->excel->getActiveSheet()->setCellValue('C'.$nro_fila, $tot_pi);
                        $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, $tot_pr);
                        $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, $tot_pnr);
                        $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, $tot_of);

                        $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, $total_pe_pini);
                        $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, $total_pe_pmod);
                        $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, $total_pe_pvig);
                        $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, $total_pe_peje);
                        $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, $total_pe_porc);

                        $total_pi =$total_pi+$tot_pi;
                        $total_pr =$total_pr+$tot_pr;
                        $total_pnr=$total_pnr+$tot_pnr;
                        $total_of =$total_of+$tot_of;
                        $total_gral_pe_pini=$total_gral_pe_pini+$total_pe_pini;
                        $total_gral_pe_pmod=$total_gral_pe_pmod+$total_pe_pmod;
                        $total_gral_pe_pvig=$total_gral_pe_pvig+$total_pe_pvig;
                        $total_gral_pe_peje=$total_gral_pe_peje+$total_pe_peje;
                    }

                    $proyectos=$this->model_taqpacha->proyectos_x_region(8,$this->gestion,$mes_id,$sql_in);
                    if(count($proyectos)>=1){
                        $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                        foreach($proyectos as $rowp){
                            $muni_proy=$this->model_taqpacha->regiones_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                            if($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']==1){//Multiregional
                                $total_pemm_pini=$total_pemm_pini+$rowp['pe_pi'];
                                $total_pemm_pmod=$total_pemm_pmod+$rowp['pe_pm'];
                                $total_pemm_pvig=$total_pemm_pvig+$rowp['pe_pv'];
                                $total_pemm_peje=$total_pemm_peje+$rowp['pe_pe'];
                                if($rowp['tp_id']==1){//Proyecto de Inversión
                                    $total_mm_pi=$total_mm_pi+1;
                                }elseif($rowp['tp_id']==2){//Programa Recurrente
                                    $total_mm_pr=$total_mm_pr+1;
                                }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                                    $total_mm_pnr=$total_mm_pnr+1;
                                }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                                    $total_mm_of=$total_mm_of+1;
                                }
                            }
                        }
                    }
                    $nrop++;
                    $total_pemm_porc=0;
                    if($total_pemm_pvig!=0){
                        $total_pemm_porc=$total_pemm_peje/$total_pemm_pvig*100;
                    }

                    $nro_fila++;
                    $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, $nrop);
                    $this->excel->getActiveSheet()->setCellValue('B'.$nro_fila, 'Multiregional');
                    $this->excel->getActiveSheet()->setCellValue('C'.$nro_fila, $total_mm_pi);
                    $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, $total_mm_pr);
                    $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, $total_mm_pr);

                    $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, $total_mm_of);
                    $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, $total_pemm_pini);
                    $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, $total_pemm_pmod);
                    $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, $total_pemm_pvig);
                    $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, $total_pemm_peje);
                    $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, $total_pemm_porc);

                    $total_pi =$total_pi+$tot_pi+$total_mm_pi;
                    $total_pr =$total_pr+$tot_pr+$total_mm_pr;
                    $total_pnr=$total_pnr+$tot_pnr+$total_mm_pnr;
                    $total_of =$total_of+$tot_of+$total_mm_of;


                    $total_gral_pe_pini=$total_gral_pe_pini+$total_pemm_pini;
                    $total_gral_pe_pmod=$total_gral_pe_pmod+$total_pemm_pmod;
                    $total_gral_pe_pvig=$total_gral_pe_pvig+$total_pemm_pvig;
                    $total_gral_pe_peje=$total_gral_pe_peje+$total_pemm_peje;
                    $total_gral_pe_porc=0;
                    if($total_gral_pe_pvig!=0){$total_gral_pe_porc=$total_gral_pe_peje/$total_gral_pe_pvig*100;}
                    $nro_fila++;
                    $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila,'TOTAL :'.$nrop);
                    $this->excel->getActiveSheet()->setCellValue('C'.$nro_fila, $total_pi);
                    $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, $total_pr);
                    $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, $total_pnr);
                    $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, $total_of);
                    $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, $total_gral_pe_pini);
                    $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, $total_gral_pe_pmod);
                    $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, $total_gral_pe_pvig);
                    $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, $total_gral_pe_peje);
                    $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, $total_gral_pe_porc);
                    if($p1!=1)
                        $this->excel->getActiveSheet()->getColumnDimension('C')->setVisible(false);
                    if($p2!=1)
                        $this->excel->getActiveSheet()->getColumnDimension('D')->setVisible(false);
                    if($p3!=1)
                        $this->excel->getActiveSheet()->getColumnDimension('E')->setVisible(false);
                    if($p4!=1)
                        $this->excel->getActiveSheet()->getColumnDimension('F')->setVisible(false);

                    $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':K'.$nro_fila)->getFont()->setBold(true);
                }
        $nombre="resumen_operaciones_x_region".strftime( "%Y-%m-%d",time()).'.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$nombre);
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // Forzamos a la descarga
        ob_end_clean();
        $objWriter->save('php://output');
    }
    function excel_x_region($region_nombre,$p1,$p2,$p3,$p4,$sql_in,$reg_id,$mes_id)
    {
        $sum=0;
        $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
        $acum_pi=0;$acum_pr=0;$acum_pnr=0;$acum_of=0;
        $acum_pi_fis=0;$acum_pr_fis=0;$acum_pnr_fis=0;$acum_of_fis=0;
        $tot_cost_pi=0;$tot_cost_pr=0;$tot_cost_pnr=0;$tot_cost_of=0;
        $tot_mod_pi=0;$tot_mod_pr=0;$tot_mod_pnr=0;$tot_mod_of=0;
        $tot_ctv_pi=0;$tot_ctv_pr=0;$tot_ctv_pnr=0;$tot_ctv_of=0;
        $tot_epa_pi=0;$tot_epa_pr=0;$tot_epa_pnr=0;$tot_epa_of=0;
        $tot_pi_pi=0;$tot_pi_pr=0;$tot_pi_pnr=0;$tot_pi_of=0;
        $tot_pm_pi=0;$tot_pm_pr=0;$tot_pm_pnr=0;$tot_pm_of=0;
        $tot_pv_pi=0;$tot_pv_pr=0;$tot_pv_pnr=0;$tot_pv_of=0;
        $tot_pe_pi=0;$tot_pe_pr=0;$tot_pe_pnr=0;$tot_pe_of=0;

        $tot_pi_m=0;$tot_pr_m=0;$tot_pnr_m=0;$tot_of_m=0;
        $tot_cost_pi_m=0;$tot_cost_pr_m=0;$tot_cost_pnr_m=0;$tot_cost_of_m=0;
        $tot_mod_pi_m=0;$tot_mod_pr_m=0;$tot_mod_pnr_m=0;$tot_mod_of_m=0;
        $tot_ctv_pi_m=0;$tot_ctv_pr_m=0;$tot_ctv_pnr_m=0;$tot_ctv_of_m=0;
        $tot_epa_pi_m=0;$tot_epa_pr_m=0;$tot_epa_pnr_m=0;$tot_epa_of_m=0;
        $tot_pi_pi_m=0;$tot_pi_pr_m=0;$tot_pi_pnr_m=0;$tot_pi_of_m=0;
        $tot_pm_pi_m=0;$tot_pm_pr_m=0;$tot_pm_pnr_m=0;$tot_pm_of_m=0;
        $tot_pv_pi_m=0;$tot_pv_pr_m=0;$tot_pv_pnr_m=0;$tot_pv_of_m=0;
        $tot_pe_pi_m=0;$tot_pe_pr_m=0;$tot_pe_pnr_m=0;$tot_pe_of_m=0;

        $acumulado=0;$acumulado_multi=0;
        $acumulado_efis=0;$acumulado_multi_efis=0;
        $mess=$this->get_mes($mes_id);
        $mes = $mess[1];
        $nombre_mes='(AL MES DE '.$mes.' DE '.$this->gestion.') (Expresado en Bolivianos)';
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('REGIONES');
        $nro_fila=1;
        $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, 'EJECUCIÓN DE ACCIONES OPERATIVAS POR REGIÓN /'.$region_nombre);
        $nro_fila++;
        $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, $nombre_mes);$nro_fila++;
        $nro_fila++;

        $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, '#');
        $this->excel->getActiveSheet()->setCellValue('B'.$nro_fila, 'NOMBRE');
        $this->excel->getActiveSheet()->setCellValue('C'.$nro_fila, 'PROVINCIA Y MUNICIPIO(S)');
        $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, 'TIPO');
        $this->excel->getActiveSheet()->mergeCells('A'.$nro_fila.':A5');
        $this->excel->getActiveSheet()->mergeCells('B'.$nro_fila.':B5');
        $this->excel->getActiveSheet()->mergeCells('C'.$nro_fila.':C5');
        $this->excel->getActiveSheet()->mergeCells('D'.$nro_fila.':D5');

        $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, 'COSTO TOTAL DE LA OPERACIÓN');
        $this->excel->getActiveSheet()->mergeCells('E'.$nro_fila.':G'.$nro_fila);

        $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, 'EJECUCIÓN ACUMULADA');
        $this->excel->getActiveSheet()->mergeCells('H'.$nro_fila.':J'.$nro_fila);

        $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, 'PRESUPUESTO GESTIÓN '.$this->session->userdata('gestion'));
        $this->excel->getActiveSheet()->mergeCells('K'.$nro_fila.':O'.$nro_fila);

        $this->excel->getActiveSheet()->setCellValue('P'.$nro_fila, 'EJECUCIÓN FÍSICA '.$this->session->userdata('gestion'));
        $this->excel->getActiveSheet()->mergeCells('P'.$nro_fila.':R'.$nro_fila);

        $this->excel->getActiveSheet()->setCellValue('S'.$nro_fila, 'ESTADO');
        $this->excel->getActiveSheet()->setCellValue('T'.$nro_fila, 'UNIDAD EJECUTORA');
        $this->excel->getActiveSheet()->setCellValue('U'.$nro_fila, 'RESPONSABLE');
        $this->excel->getActiveSheet()->mergeCells('S'.$nro_fila.':S5');
        $this->excel->getActiveSheet()->mergeCells('T'.$nro_fila.':T5');
        $this->excel->getActiveSheet()->mergeCells('U'.$nro_fila.':U5');

        $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFont()->setSize(11);
        $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFont()->setBold(true);

        $nro_fila++;
        $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, 'INICIAL');
        $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, 'MODIFICACIONES');
        $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, 'VIGENTE');
        $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, 'EJECUCIÓN FINANCIERA');
        $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, '% EJEC. FINANCIERA');
        $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, '% DE AVANCE FÍSICO');
        $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, 'INICIAL');
        $this->excel->getActiveSheet()->setCellValue('L'.$nro_fila, 'MODIFICADO');
        $this->excel->getActiveSheet()->setCellValue('M'.$nro_fila, 'VIGENTE');
        $this->excel->getActiveSheet()->setCellValue('N'.$nro_fila, 'EJEC. FINANCIERA');
        $this->excel->getActiveSheet()->setCellValue('O'.$nro_fila, '% EJEC. FINANCIERA');
        $this->excel->getActiveSheet()->setCellValue('P'.$nro_fila, 'PROG. %');
        $this->excel->getActiveSheet()->setCellValue('Q'.$nro_fila, 'EJEC %');
        $this->excel->getActiveSheet()->setCellValue('R'.$nro_fila, '% EJEC. ANUAL');

        $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFont()->setSize(11);
        $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFont()->setBold(true);

        $tab_aux='';

        $programas = $this->model_epresupuestaria->ejecucion_proyecto_programas_x_region($sql_in,$this->gestion,$mes_id,$reg_id);
        if(count($programas)!=0){
            $nro=0;
            $nro_fila++;
            $nro_mult=0;
            if($reg_id!=8){
                foreach ($programas as $rowp){
                    $muni_proy=$this->model_taqpacha->regiones_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                    $color_mult='';
                    if($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']>1){//Multiregional
                        $color_mult='style="background-color:#EDFBFF"';
                    }

                    $se=$rowp['pe_pe'];
                    $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                    if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                        $se=$se+$suma[0]['total_ejecutado'];
                    }

                    $suma_prog=$this->mdas_complementario->costo_total_proyecto($rowp['pfec_id']);
                    $toal_modif = $this->mdas_complementario->modificaciones_costo_total($rowp['pfec_id']);
                    $estado = $this->mdas_complementario->estado_operacion($rowp['pfec_id'],$mes_id,$this->gestion);
                    $efis = $this->avance_fisico($rowp['proy_id'],$mes_id);

                    if($color_mult=='') {
                        $nro++;
                        $nro_fila++;
                        $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, $nro);
                        $this->excel->getActiveSheet()->setCellValue('B'.$nro_fila, $rowp['proy_nombre']);
                        $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);

                        $tabla_mun='';
                        foreach($prov as $row_prov){
                            $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                            foreach ($muni as $muni) {
                                $tabla_mun .= $row_prov['prov_provincia'] .'    '. $muni['muni_municipio'] . PHP_EOL;
                            }
                        }
                        $this->excel->getActiveSheet()->setCellValue('C'.$nro_fila, substr($tabla_mun,0,-2));
                        $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, $rowp['tp_tipo']);
                        $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, $suma_prog[0]['total_programado']);

                        $total_mod = 0;
                        if ($toal_modif[0]['total_modif'] != '' || $toal_modif[0]['total_modif'] != 0) {
                            $total_mod = $toal_modif[0]['total_modif'];
                        }
                        $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, $total_mod);
                        $ctv = $suma_prog[0]['total_programado'] + $total_mod;
                        $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, $ctv);
                        $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, $se);
                        $efin = 0;
                        if ($suma_prog[0]['total_programado'] != 0) {
                            $efin = round((($se / $suma_prog[0]['total_programado']) * 100),2);
                        }
                        $acumulado_efis=$acumulado_efis+$efis;
                        $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, $efin);
                        $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, $efis);
                        $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, $rowp['pe_pi']);
                        $this->excel->getActiveSheet()->setCellValue('L'.$nro_fila, $rowp['pe_pm']);
                        $this->excel->getActiveSheet()->setCellValue('M'.$nro_fila, $rowp['pe_pv']);
                        $this->excel->getActiveSheet()->setCellValue('N'.$nro_fila, $rowp['pe_pe']);
                        $ejec = 0;
                        if ($rowp['pe_pv'] != 0) {
                            $ejec = round((($rowp['pe_pe'] / $rowp['pe_pv']) * 100),2);
                        }
                        $this->excel->getActiveSheet()->setCellValue('O'.$nro_fila, $ejec);
                        $estado_proy = 'No registrado';
                        if (count($estado) != 0) {
                            $estado_proy = $estado[0]['ep_descripcion'];
                        }
                        $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));

                        $this->excel->getActiveSheet()->setCellValue('P'.$nro_fila, $ejecutado[0]);
                        $this->excel->getActiveSheet()->setCellValue('Q'.$nro_fila, $ejecutado[1]);
                        $this->excel->getActiveSheet()->setCellValue('R'.$nro_fila, $ejecutado[2]);
                        $acumulado=$acumulado+$ejecutado[2];
                        $this->excel->getActiveSheet()->setCellValue('S'.$nro_fila, $estado_proy);
                        $this->excel->getActiveSheet()->setCellValue('T'.$nro_fila, $rowp['uni_unidad']);
                        $this->excel->getActiveSheet()->setCellValue('U'.$nro_fila, $rowp['responsable'].PHP_EOL.'Telf: '.$rowp['fun_telefono']);

                        if ($rowp['tp_id'] == 1) {//Proyecto de Inversión
                            $tot_pi = $tot_pi + 1;
                            $acum_pi=$acum_pi+$ejecutado[2];
                            $acum_pi_fis=$acum_pi_fis+$efis;
                            $tot_cost_pi = $tot_cost_pi + $suma_prog[0]['total_programado'];
                            $tot_mod_pi = $tot_mod_pi + $total_mod;
                            $tot_ctv_pi = $tot_ctv_pi + $ctv;
                            $tot_epa_pi = $tot_epa_pi + $se;
                            $tot_pi_pi = $tot_pi_pi + $rowp['pe_pi'];
                            $tot_pm_pi = $tot_pm_pi + $rowp['pe_pm'];
                            $tot_pv_pi = $tot_pv_pi + $rowp['pe_pv'];
                            $tot_pe_pi = $tot_pe_pi + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 2) {//Programa Recurrente
                            $tot_pr = $tot_pr + 1;
                            $acum_pr=$acum_pr+$ejecutado[2];
                            $acum_pr_fis=$acum_pr_fis+$efis;
                            $tot_cost_pr = $tot_cost_pr + $suma_prog[0]['total_programado'];
                            $tot_mod_pr = $tot_mod_pr + $total_mod;
                            $tot_ctv_pr = $tot_ctv_pr + $ctv;
                            $tot_epa_pr = $tot_epa_pr + $se;
                            $tot_pi_pr = $tot_pi_pr + $rowp['pe_pi'];
                            $tot_pm_pr = $tot_pm_pr + $rowp['pe_pm'];
                            $tot_pv_pr = $tot_pv_pr + $rowp['pe_pv'];
                            $tot_pe_pr = $tot_pe_pr + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 3) {//Programa No  Recurrente
                            $tot_pnr = $tot_pnr + 1;
                            $acum_pnr=$acum_pnr+$ejecutado[2];
                            $acum_pnr_fis=$acum_pnr_fis+$efis;
                            $tot_cost_pnr = $tot_cost_pnr + $suma_prog[0]['total_programado'];
                            $tot_mod_pnr = $tot_mod_pnr + $total_mod;
                            $tot_ctv_pnr = $tot_ctv_pnr + $ctv;
                            $tot_epa_pnr = $tot_epa_pnr + $se;
                            $tot_pi_pnr = $tot_pi_pnr + $rowp['pe_pi'];
                            $tot_pm_pnr = $tot_pm_pnr + $rowp['pe_pm'];
                            $tot_pv_pnr = $tot_pv_pnr + $rowp['pe_pv'];
                            $tot_pe_pnr = $tot_pe_pnr + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 4) {//Operacion de Funcionamiento
                            $tot_of = $tot_of + 1;
                            $acum_of=$acum_of+$ejecutado[2];
                            $acum_of_fis=$acum_of_fis+$efis;
                            $tot_cost_of = $tot_cost_of + $suma_prog[0]['total_programado'];
                            $tot_mod_of = $tot_mod_of + $total_mod;
                            $tot_ctv_of = $tot_ctv_of + $ctv;
                            $tot_epa_of = $tot_epa_of + $se;
                            $tot_pi_of = $tot_pi_of + $rowp['pe_pi'];
                            $tot_pm_of = $tot_pm_of + $rowp['pe_pm'];
                            $tot_pv_of = $tot_pv_of + $rowp['pe_pv'];
                            $tot_pe_of = $tot_pe_of + $rowp['pe_pe'];
                        }
                    }else{$tab_aux='..';}
                }
            }else{
                foreach($programas as $rowp) {
                        $muni_proy=$this->model_taqpacha->regiones_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                        if(($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']>1) ||($rowp['region']==8)){//Multiregional
                            $se=$rowp['pe_pe'];
                            $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                            if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                                $se=$se+$suma[0]['total_ejecutado'];
                            }

                            $suma_prog=$this->mdas_complementario->costo_total_proyecto($rowp['pfec_id']);
                            $toal_modif = $this->mdas_complementario->modificaciones_costo_total($rowp['pfec_id']);
                            $estado = $this->mdas_complementario->estado_operacion($rowp['pfec_id'],$mes_id,$this->gestion);
                            $efis = $this->avance_fisico($rowp['proy_id'],$mes_id);
                            $tabla .= '<tr class="modo1 derecha">';
                            $nro++;
                            $nro_fila++;
                            $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, $nro);
                            $this->excel->getActiveSheet()->setCellValue('B'.$nro_fila, $rowp['proy_nombre']);
                            $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);

                            $tabla_mun='';
                            foreach($prov as $row_prov){
                                $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                                foreach ($muni as $muni) {
                                    $tabla_mun .= $row_prov['prov_provincia'] .'    '. $muni['muni_municipio'] . PHP_EOL;
                                }
                            }
                            $this->excel->getActiveSheet()->setCellValue('C'.$nro_fila, substr($tabla_mun,0,-2));
                            $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, $rowp['tp_tipo']);
                            $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, $suma_prog[0]['total_programado']);

                            $total_mod = 0;
                            if ($toal_modif[0]['total_modif'] != '' || $toal_modif[0]['total_modif'] != 0) {
                                $total_mod = $toal_modif[0]['total_modif'];
                            }
                            $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, $total_mod);
                            $ctv = $suma_prog[0]['total_programado'] + $total_mod;
                            $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, $ctv);
                            $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, $se);
                            $efin = 0;
                            if ($suma_prog[0]['total_programado'] != 0) {
                                $efin = round((($se / $suma_prog[0]['total_programado']) * 100),2);
                            }
                            $acumulado_efis=$acumulado_efis+$efis;
                            $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, $efin);
                            $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, $efis);
                            $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, $rowp['pe_pi']);
                            $this->excel->getActiveSheet()->setCellValue('L'.$nro_fila, $rowp['pe_pm']);
                            $this->excel->getActiveSheet()->setCellValue('M'.$nro_fila, $rowp['pe_pv']);
                            $this->excel->getActiveSheet()->setCellValue('N'.$nro_fila, $rowp['pe_pe']);
                            $ejec = 0;
                            if ($rowp['pe_pv'] != 0) {
                                $ejec = round((($rowp['pe_pe'] / $rowp['pe_pv']) * 100),2);
                            }
                            $this->excel->getActiveSheet()->setCellValue('O'.$nro_fila, $ejec);
                            $estado_proy = 'No registrado';
                            if (count($estado) != 0) {
                                $estado_proy = $estado[0]['ep_descripcion'];
                            }
                            $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));

                            $this->excel->getActiveSheet()->setCellValue('P'.$nro_fila, $ejecutado[0]);
                            $this->excel->getActiveSheet()->setCellValue('Q'.$nro_fila, $ejecutado[1]);
                            $this->excel->getActiveSheet()->setCellValue('R'.$nro_fila, $ejecutado[2]);
                            $acumulado=$acumulado+$ejecutado[2];
                            $this->excel->getActiveSheet()->setCellValue('S'.$nro_fila, $estado_proy);
                            $this->excel->getActiveSheet()->setCellValue('T'.$nro_fila, $rowp['uni_unidad']);
                            $this->excel->getActiveSheet()->setCellValue('U'.$nro_fila, $rowp['responsable'].PHP_EOL.'Telf: '.$rowp['fun_telefono']);

                            if ($rowp['tp_id'] == 1) {//Proyecto de Inversión
                                $tot_pi = $tot_pi + 1;
                                $acum_pi=$acum_pi+$ejecutado[2];
                                $acum_pi_fis=$acum_pi_fis+$efis;
                                $tot_cost_pi = $tot_cost_pi + $suma_prog[0]['total_programado'];
                                $tot_mod_pi = $tot_mod_pi + $total_mod;
                                $tot_ctv_pi = $tot_ctv_pi + $ctv;
                                $tot_epa_pi = $tot_epa_pi + $se;
                                $tot_pi_pi = $tot_pi_pi + $rowp['pe_pi'];
                                $tot_pm_pi = $tot_pm_pi + $rowp['pe_pm'];
                                $tot_pv_pi = $tot_pv_pi + $rowp['pe_pv'];
                                $tot_pe_pi = $tot_pe_pi + $rowp['pe_pe'];
                            } elseif ($rowp['tp_id'] == 2) {//Programa Recurrente
                                $tot_pr = $tot_pr + 1;
                                $acum_pr=$acum_pr+$ejecutado[2];
                                $acum_pr_fis=$acum_pr_fis+$efis;
                                $tot_cost_pr = $tot_cost_pr + $suma_prog[0]['total_programado'];
                                $tot_mod_pr = $tot_mod_pr + $total_mod;
                                $tot_ctv_pr = $tot_ctv_pr + $ctv;
                                $tot_epa_pr = $tot_epa_pr + $se;
                                $tot_pi_pr = $tot_pi_pr + $rowp['pe_pi'];
                                $tot_pm_pr = $tot_pm_pr + $rowp['pe_pm'];
                                $tot_pv_pr = $tot_pv_pr + $rowp['pe_pv'];
                                $tot_pe_pr = $tot_pe_pr + $rowp['pe_pe'];
                            } elseif ($rowp['tp_id'] == 3) {//Programa No  Recurrente
                                $tot_pnr = $tot_pnr + 1;
                                $acum_pnr=$acum_pnr+$ejecutado[2];
                                $acum_pnr_fis=$acum_pnr_fis+$efis;
                                $tot_cost_pnr = $tot_cost_pnr + $suma_prog[0]['total_programado'];
                                $tot_mod_pnr = $tot_mod_pnr + $total_mod;
                                $tot_ctv_pnr = $tot_ctv_pnr + $ctv;
                                $tot_epa_pnr = $tot_epa_pnr + $se;
                                $tot_pi_pnr = $tot_pi_pnr + $rowp['pe_pi'];
                                $tot_pm_pnr = $tot_pm_pnr + $rowp['pe_pm'];
                                $tot_pv_pnr = $tot_pv_pnr + $rowp['pe_pv'];
                                $tot_pe_pnr = $tot_pe_pnr + $rowp['pe_pe'];
                            } elseif ($rowp['tp_id'] == 4) {//Operacion de Funcionamiento
                                $tot_of = $tot_of + 1;
                                $acum_of=$acum_of+$ejecutado[2];
                                $acum_of_fis=$acum_of_fis+$efis;
                                $tot_cost_of = $tot_cost_of + $suma_prog[0]['total_programado'];
                                $tot_mod_of = $tot_mod_of + $total_mod;
                                $tot_ctv_of = $tot_ctv_of + $ctv;
                                $tot_epa_of = $tot_epa_of + $se;
                                $tot_pi_of = $tot_pi_of + $rowp['pe_pi'];
                                $tot_pm_of = $tot_pm_of + $rowp['pe_pm'];
                                $tot_pv_of = $tot_pv_of + $rowp['pe_pv'];
                                $tot_pe_of = $tot_pe_of + $rowp['pe_pe'];
                            }
                        }

                }
            }

        }
        $tot_porc_pi=0;$tot_porc_pnr=0;$tot_porc_pr=0;$tot_porc_of=0;
        if($tot_pv_pi!=0){$tot_porc_pi  =$tot_pe_pi/$tot_pv_pi*100;}
        if($tot_pv_pnr!=0){$tot_porc_pnr=$tot_pe_pnr/$tot_pv_pnr*100;}
        if($tot_pv_pr!=0){$tot_porc_pr  =$tot_pe_pr/$tot_pv_pr*100;}
        if($tot_pv_of!=0){$tot_porc_of  =$tot_pe_of/$tot_pv_of*100;}

        $tot_pora_pi=0;$tot_pora_pnr=0;$tot_pora_pr=0;$tot_pora_of=0;
        if($tot_ctv_pi!=0){$tot_pora_pi=$tot_epa_pi/$tot_ctv_pi*100;}
        if($tot_ctv_pnr!=0){$tot_pora_pnr=$tot_epa_pnr/$tot_ctv_pnr*100;}
        if($tot_ctv_pr!=0){$tot_pora_pr=$tot_epa_pr/$tot_ctv_pr*100;}
        if($tot_ctv_of!=0){$tot_pora_of=$tot_epa_of/$tot_ctv_of*100;}

        $total_gral_op  =$tot_pi+$tot_pnr+$tot_pr+$tot_of;
        $total_gral_cost=$tot_cost_pi+$tot_cost_pnr+$tot_cost_pr+$tot_cost_of;
        $total_gral_mod =$tot_mod_pi+$tot_mod_pnr+$tot_mod_pr+$tot_mod_of;
        $total_gral_ctv =$tot_ctv_pi+$tot_ctv_pnr+$tot_ctv_pr+$tot_ctv_of;
        $total_gral_epa =$tot_epa_pi+$tot_epa_pnr+$tot_epa_pr+$tot_epa_of;
        $total_gral_pora=0;
        if($total_gral_ctv!=0){$total_gral_pora=$total_gral_epa/$total_gral_ctv*100;}

        $total_gral_pi  =$tot_pi_pi+$tot_pi_pnr+$tot_pi_pr+$tot_pi_of;
        $total_gral_pm  =$tot_pm_pi+$tot_pm_pnr+$tot_pm_pr+$tot_pm_of;
        $total_gral_pv  =$tot_pv_pi+$tot_pv_pnr+$tot_pv_pr+$tot_pv_of;
        $total_gral_pe  =$tot_pe_pi+$tot_pe_pnr+$tot_pe_pr+$tot_pe_of;
        $total_gral_porc=0;
        if($total_gral_pv!=0){$total_gral_porc=$total_gral_pe/$total_gral_pv*100;}


        if($p1==1) {
            $nro_fila++;
            $total_acum_pi=0;$total_acum_pi_fis=0;
            if($tot_pi!=0){
                $total_acum_pi=$acum_pi/$tot_pi;
                $total_acum_pi_fis=$acum_pi_fis/$tot_pi;
            }
            $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, 'TOTAL PROYECTOS DE INVERSIÓN');
            $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, $tot_pi);
            $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, $tot_cost_pi);
            $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, $tot_mod_pi);
            $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, $tot_ctv_pi);
            $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, $tot_epa_pi);
            $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, $tot_pora_pi);
            $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, $total_acum_pi_fis);
            $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, $tot_pi_pi);
            $this->excel->getActiveSheet()->setCellValue('L'.$nro_fila, $tot_pm_pi);
            $this->excel->getActiveSheet()->setCellValue('M'.$nro_fila, $tot_pv_pi);
            $this->excel->getActiveSheet()->setCellValue('N'.$nro_fila, $tot_pe_pi);
            $this->excel->getActiveSheet()->setCellValue('O'.$nro_fila, $tot_porc_pi);
            $this->excel->getActiveSheet()->setCellValue('R'.$nro_fila, $total_acum_pi);
            $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => $this->total_pi)));
        }
        if($p2==1) {
            $nro_fila++;
            $total_acum_pr=0;$total_acum_pr_fis=0;
            if($tot_pr!=0){
                $total_acum_pr=$acum_pr/$tot_pr;
                $total_acum_pr_fis=$acum_pr_fis/$tot_pr;
            }
            $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, 'TOTAL PROGRAMAS RECURRENTES');
            $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, $tot_pr);
            $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, $tot_cost_pr);
            $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, $tot_mod_pr);
            $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, $tot_ctv_pr);
            $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, $tot_epa_pr);
            $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, $tot_pora_pr);
            $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, $total_acum_pr_fis);
            $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, $tot_pi_pr);
            $this->excel->getActiveSheet()->setCellValue('L'.$nro_fila, $tot_pm_pr);
            $this->excel->getActiveSheet()->setCellValue('M'.$nro_fila, $tot_pv_pr);
            $this->excel->getActiveSheet()->setCellValue('N'.$nro_fila, $tot_pe_pr);
            $this->excel->getActiveSheet()->setCellValue('O'.$nro_fila, $tot_porc_pr);
            $this->excel->getActiveSheet()->setCellValue('R'.$nro_fila, $total_acum_pr);
            $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => $this->total_pr)));
        }
        if($p3==1) {
            $nro_fila++;
            $total_acum_pnr=0;$total_acum_prn_fis=0;
            if($tot_pnr!=0){
                $total_acum_pnr=$acum_pnr/$tot_pnr;
                $total_acum_prn_fis=$acum_pnr_fis/$tot_pnr;
            }
            $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, 'TOTAL PROGRAMAS NO RECURRENTES');
            $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, $tot_pnr);
            $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, $tot_cost_pnr);
            $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, $tot_mod_pnr);
            $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, $tot_ctv_pnr);
            $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, $tot_epa_pnr);
            $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, $tot_pora_pnr);
            $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, $total_acum_prn_fis);
            $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, $tot_pi_pnr);
            $this->excel->getActiveSheet()->setCellValue('L'.$nro_fila, $tot_pm_pnr);
            $this->excel->getActiveSheet()->setCellValue('M'.$nro_fila, $tot_pv_pnr);
            $this->excel->getActiveSheet()->setCellValue('N'.$nro_fila, $tot_pe_pnr);
            $this->excel->getActiveSheet()->setCellValue('O'.$nro_fila, $tot_porc_pnr);
            $this->excel->getActiveSheet()->setCellValue('R'.$nro_fila, $total_acum_pnr);
            $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => $this->total_pnr)));
        }
        if($p4==1) {
            $nro_fila++;
            $total_acum_of=0;$total_acum_of_fis=0;
            if($tot_of!=0){
                $total_acum_of=$acum_of/$tot_of;
                $total_acum_of_fis=$acum_of_fis/$tot_of;
            }
            $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, 'TOTAL OPERACIONES DE FUNCIONAMIENTO');
            $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, $tot_of);
            $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, $tot_cost_of);
            $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, $tot_mod_of);
            $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, $tot_ctv_of);
            $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, $tot_epa_of);
            $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, $tot_pora_of);
            $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, $total_acum_of_fis);
            $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, $tot_pi_of);
            $this->excel->getActiveSheet()->setCellValue('L'.$nro_fila, $tot_pm_of);
            $this->excel->getActiveSheet()->setCellValue('M'.$nro_fila, $tot_pv_of);
            $this->excel->getActiveSheet()->setCellValue('N'.$nro_fila, $tot_pe_of);
            $this->excel->getActiveSheet()->setCellValue('O'.$nro_fila, $tot_porc_of);
            $this->excel->getActiveSheet()->setCellValue('R'.$nro_fila, $total_acum_of);
            $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => $this->total_of)));
         }
        $nro_fila++;
        $total_acum=0;$total_acum_fis=0;
        if($total_gral_op!=0){
            $total_acum=$acumulado/$total_gral_op;
            $total_acum_fis=$acumulado_efis/$total_gral_op;
        }

        $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, 'TOTAL DE LA REGIÓN');
        $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, $total_gral_op);
        $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, $total_gral_cost);
        $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, $total_gral_mod);
        $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, $total_gral_ctv);
        $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, $total_gral_epa);
        $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, $total_gral_pora);
        $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, $total_acum_fis);
        $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, $total_gral_pi);
        $this->excel->getActiveSheet()->setCellValue('L'.$nro_fila, $total_gral_pm);
        $this->excel->getActiveSheet()->setCellValue('M'.$nro_fila, $total_gral_pv);
        $this->excel->getActiveSheet()->setCellValue('N'.$nro_fila, $total_gral_pe);
        $this->excel->getActiveSheet()->setCellValue('O'.$nro_fila, $total_gral_porc);
        $this->excel->getActiveSheet()->setCellValue('R'.$nro_fila, $total_acum);
        $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => $this->total_general)));


        /*Proyectos multiregion*/

        if($tab_aux!=''){
            $nro_fila++;
            $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, 'OPERACIONES MULTIREGIÓN');
            $this->excel->getActiveSheet()->mergeCells('A'.$nro_fila.':U'.$nro_fila);

            $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFont()->setSize(14);
            $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFont()->setBold(true);

            if($reg_id!=8){
                foreach ($programas as $rowp){
                    $muni_proy=$this->model_taqpacha->regiones_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                    $color_mult='';
                    if($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']>1){//Multiregional
                        $color_mult='style="background-color:#EDFBFF"';
                    }

                    $se=$rowp['pe_pe'];
                    $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                    if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                        $se=$se+$suma[0]['total_ejecutado'];
                    }

                    $suma_prog=$this->mdas_complementario->costo_total_proyecto($rowp['pfec_id']);
                    $toal_modif = $this->mdas_complementario->modificaciones_costo_total($rowp['pfec_id']);
                    $estado = $this->mdas_complementario->estado_operacion($rowp['pfec_id'],$mes_id,$this->gestion);
                    $efis = $this->avance_fisico($rowp['proy_id'],$mes_id);

                    if($color_mult!='') {
                        $nro_mult++;
                        $nro_fila++;
                        $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, $nro_mult);
                        $this->excel->getActiveSheet()->setCellValue('B'.$nro_fila, $rowp['proy_nombre']);
                        $tabla_mun='';
                        $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);

                        foreach($prov as $row_prov){
                            $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                            foreach ($muni as $muni) {
                                $tabla_mun .= $row_prov['prov_provincia'] .'    '. $muni['muni_municipio'] . PHP_EOL;
                            }
                        }
                        $this->excel->getActiveSheet()->setCellValue('C'.$nro_fila, substr($tabla_mun, 0, -2));
                        $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, $rowp['tp_tipo']);
                        $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, $suma_prog[0]['total_programado']);

                        $total_mod = 0;
                        if ($toal_modif[0]['total_modif'] != '' || $toal_modif[0]['total_modif'] != 0) {
                            $total_mod = $toal_modif[0]['total_modif'];
                        }
                        $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, $total_mod);
                        $ctv = $suma_prog[0]['total_programado'] + $total_mod;
                        $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, $ctv);
                        $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, $se);

                        $efin = 0;
                        if ($suma_prog[0]['total_programado'] != 0) {
                            $efin = round((($se / $suma_prog[0]['total_programado']) * 100),2);
                        }
                        $acumulado_multi_efis=$acumulado_multi_efis+$efis;
                        $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, $efin);
                        $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, $efis);
                        $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, $rowp['pe_pi']);
                        $this->excel->getActiveSheet()->setCellValue('L'.$nro_fila, $rowp['pe_pm']);
                        $this->excel->getActiveSheet()->setCellValue('M'.$nro_fila, $rowp['pe_pv']);
                        $this->excel->getActiveSheet()->setCellValue('N'.$nro_fila, $rowp['pe_pe']);
                        $ejec = 0;
                        if ($rowp['pe_pv'] != 0) {
                            $ejec = round((($rowp['pe_pe'] / $rowp['pe_pv']) * 100),2);
                        }
                        $this->excel->getActiveSheet()->setCellValue('O'.$nro_fila, $ejec);
                        $estado_proy = 'No registrado';
                        if (count($estado) != 0) {
                            $estado_proy = $estado[0]['ep_descripcion'];
                        }
                        $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));
                        $this->excel->getActiveSheet()->setCellValue('P'.$nro_fila, $ejecutado[0]);
                        $this->excel->getActiveSheet()->setCellValue('Q'.$nro_fila, $ejecutado[1]);
                        $this->excel->getActiveSheet()->setCellValue('R'.$nro_fila, $ejecutado[2]);
                        $acumulado_multi=$acumulado_multi+$ejecutado[2];
                        $this->excel->getActiveSheet()->setCellValue('S'.$nro_fila, $estado_proy);
                        $this->excel->getActiveSheet()->setCellValue('T'.$nro_fila, $rowp['uni_unidad']);
                        $this->excel->getActiveSheet()->setCellValue('U'.$nro_fila, $rowp['responsable'].PHP_EOL.'Telf: '.$rowp['fun_telefono']);
                        if ($rowp['tp_id'] == 1) {//Proyecto de Inversión
                            $tot_pi_m = $tot_pi_m + 1;
                            $tot_cost_pi_m = $tot_cost_pi_m + $suma_prog[0]['total_programado'];
                            $tot_mod_pi_m = $tot_mod_pi_m + $total_mod;
                            $tot_ctv_pi_m = $tot_ctv_pi_m + $ctv;
                            $tot_epa_pi_m = $tot_epa_pi_m + $se;
                            $tot_pi_pi_m = $tot_pi_pi_m + $rowp['pe_pi'];
                            $tot_pm_pi_m = $tot_pm_pi_m + $rowp['pe_pm'];
                            $tot_pv_pi_m = $tot_pv_pi_m + $rowp['pe_pv'];
                            $tot_pe_pi_m = $tot_pe_pi_m + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 2) {//Programa Recurrente
                            $tot_pr_m = $tot_pr_m + 1;
                            $tot_cost_pr_m = $tot_cost_pr_m + $suma_prog[0]['total_programado'];
                            $tot_mod_pr_m = $tot_mod_pr_m + $total_mod;
                            $tot_ctv_pr_m = $tot_ctv_pr_m + $ctv;
                            $tot_epa_pr_m = $tot_epa_pr_m + $se;
                            $tot_pi_pr_m = $tot_pi_pr_m + $rowp['pe_pi'];
                            $tot_pm_pr_m = $tot_pm_pr_m + $rowp['pe_pm'];
                            $tot_pv_pr_m = $tot_pv_pr_m + $rowp['pe_pv'];
                            $tot_pe_pr_m = $tot_pe_pr_m + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 3) {//Programa No  Recurrente
                            $tot_pnr_m = $tot_pnr_m + 1;
                            $tot_cost_pnr_m = $tot_cost_pnr_m + $suma_prog[0]['total_programado'];
                            $tot_mod_pnr_m = $tot_mod_pnr_m + $total_mod;
                            $tot_ctv_pnr_m = $tot_ctv_pnr_m + $ctv;
                            $tot_epa_pnr_m = $tot_epa_pnr_m + $se;
                            $tot_pi_pnr_m = $tot_pi_pnr_m + $rowp['pe_pi'];
                            $tot_pm_pnr_m = $tot_pm_pnr_m + $rowp['pe_pm'];
                            $tot_pv_pnr_m = $tot_pv_pnr_m + $rowp['pe_pv'];
                            $tot_pe_pnr_m = $tot_pe_pnr_m + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 4) {//Operacion de Funcionamiento
                            $tot_of_m = $tot_of_m + 1;
                            $tot_cost_of_m = $tot_cost_of_m + $suma_prog[0]['total_programado'];
                            $tot_mod_of_m = $tot_mod_of_m + $total_mod;
                            $tot_ctv_of_m = $tot_ctv_of_m + $ctv;
                            $tot_epa_of_m = $tot_epa_of_m + $se;
                            $tot_pi_of_m = $tot_pi_of_m + $rowp['pe_pi'];
                            $tot_pm_of_m = $tot_pm_of_m + $rowp['pe_pm'];
                            $tot_pv_of_m = $tot_pv_of_m + $rowp['pe_pv'];
                            $tot_pe_of_m = $tot_pe_of_m + $rowp['pe_pe'];
                        }
                    }
                }
            }
            /**Multiregionales**/
            $tot_porc_pi_m=0;$tot_porc_pnr_m=0;$tot_porc_pr_m=0;$tot_porc_of_m=0;
            if($tot_pv_pi_m!=0){$tot_porc_pi_m  =$tot_pe_pi_m/$tot_pv_pi_m*100;}
            if($tot_pv_pnr_m!=0){$tot_porc_pnr_m=$tot_pe_pnr_m/$tot_pv_pnr_m*100;}
            if($tot_pv_pr_m!=0){$tot_porc_pr_m  =$tot_pe_pr_m/$tot_pv_pr_m*100;}
            if($tot_pv_of_m!=0){$tot_porc_of_m  =$tot_pe_of_m/$tot_pv_of_m*100;}

            $tot_pora_pi_m=0;$tot_pora_pnr_m=0;$tot_pora_pr_m=0;$tot_pora_of_m=0;
            if($tot_ctv_pi_m!=0){$tot_pora_pi_m  =$tot_epa_pi_m/$tot_ctv_pi_m*100;}
            if($tot_ctv_pnr_m!=0){$tot_pora_pnr_m=$tot_epa_pnr_m/$tot_ctv_pnr_m*100;}
            if($tot_ctv_pr_m!=0){$tot_pora_pr_m  =$tot_epa_pr_m/$tot_ctv_pr_m*100;}
            if($tot_ctv_of_m!=0){$tot_pora_of_m  =$tot_epa_of_m/$tot_ctv_of_m*100;}

            $total_gral_op_m  =$tot_pi_m+$tot_pnr_m+$tot_pr_m+$tot_of_m;
            $total_gral_cost_m=$tot_cost_pi_m+$tot_cost_pnr_m+$tot_cost_pr_m+$tot_cost_of;
            $total_gral_mod_m =$tot_mod_pi_m+$tot_mod_pnr_m+$tot_mod_pr_m+$tot_mod_of_m;
            $total_gral_ctv_m =$tot_ctv_pi_m+$tot_ctv_pnr_m+$tot_ctv_pr_m+$tot_ctv_of_m;
            $total_gral_epa_m =$tot_epa_pi_m+$tot_epa_pnr_m+$tot_epa_pr_m+$tot_epa_of_m;
            $total_gral_pora_m=0;
            if($total_gral_ctv_m!=0){$total_gral_pora_m=$total_gral_epa_m/$total_gral_ctv_m*100;}

            $total_gral_pi_m  =$tot_pi_pi_m+$tot_pi_pnr_m+$tot_pi_pr_m+$tot_pi_of_m;
            $total_gral_pm_m  =$tot_pm_pi_m+$tot_pm_pnr_m+$tot_pm_pr_m+$tot_pm_of_m;
            $total_gral_pv_m  =$tot_pv_pi_m+$tot_pv_pnr_m+$tot_pv_pr_m+$tot_pv_of_m;
            $total_gral_pe_m  =$tot_pe_pi_m+$tot_pe_pnr_m+$tot_pe_pr_m+$tot_pe_of_m;
            $total_gral_porc_m=0;
            if($total_gral_pv_m!=0){$total_gral_porc_m=$total_gral_pe_m/$total_gral_pv_m*100;}
            $total_acum_multi=0;$total_acum_multi_fis=0;
            if($total_gral_op_m!=0){
                $total_acum_multi=$acumulado_multi/$total_gral_op_m;
                $total_acum_multi_fis=$acumulado_multi_efis/$total_gral_op_m;
            }

            $nro_fila++;
            $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, 'TOTAL MULTIREGIONAL');
            $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, $total_gral_op_m);
            $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, $total_gral_cost_m);
            $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, $total_gral_mod_m);
            $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, $total_gral_ctv_m);
            $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, $total_gral_epa_m);
            $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, $total_gral_pora_m);
            $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, $total_acum_multi_fis);
            $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, $total_gral_pi_m);
            $this->excel->getActiveSheet()->setCellValue('L'.$nro_fila, $total_gral_pm_m);
            $this->excel->getActiveSheet()->setCellValue('M'.$nro_fila, $total_gral_pv_m);
            $this->excel->getActiveSheet()->setCellValue('N'.$nro_fila, $total_gral_pe_m);
            $this->excel->getActiveSheet()->setCellValue('O'.$nro_fila, $total_gral_porc_m);
            $this->excel->getActiveSheet()->setCellValue('R'.$nro_fila, $total_acum_multi);
            $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => $this->total_multi)));

            $gran_total=$total_gral_op+$total_gral_op_m;
            $gran_total_por_acum=0;
            if(($total_gral_ctv+$total_gral_ctv_m)!=0) {$gran_total_por_acum=($total_gral_epa+$total_gral_epa_m)/($total_gral_ctv+$total_gral_ctv_m)*100;}
            $gran_total_porc=0;
            if(($total_gral_pv+$total_gral_pv_m)!=0) {$gran_total_porc=($total_gral_pe+$total_gral_pe_m)/($total_gral_pv+$total_gral_pv_m)*100;}
            $gran_total_media=0;$gran_total_media_fis=0;
            if($gran_total!=0){
                $gran_total_media=($acumulado_multi+$acumulado)/$gran_total;
                $gran_total_media_fis=($acumulado_multi_efis+$acumulado_efis)/$gran_total;
            }

            $nro_fila++;
            $this->excel->getActiveSheet()->setCellValue('A'.$nro_fila, 'TOTAL GENERAL');
            $this->excel->getActiveSheet()->setCellValue('D'.$nro_fila, $gran_total);
            $this->excel->getActiveSheet()->setCellValue('E'.$nro_fila, $total_gral_cost+$total_gral_cost_m);
            $this->excel->getActiveSheet()->setCellValue('F'.$nro_fila, $total_gral_mod+$total_gral_mod_m);
            $this->excel->getActiveSheet()->setCellValue('G'.$nro_fila, $total_gral_ctv+$total_gral_ctv_m);
            $this->excel->getActiveSheet()->setCellValue('H'.$nro_fila, $total_gral_ctv+$total_gral_ctv_m);
            $this->excel->getActiveSheet()->setCellValue('I'.$nro_fila, $gran_total_por_acum);
            $this->excel->getActiveSheet()->setCellValue('J'.$nro_fila, $gran_total_media_fis);
            $this->excel->getActiveSheet()->setCellValue('K'.$nro_fila, $total_gral_pi+$total_gral_pi_m);
            $this->excel->getActiveSheet()->setCellValue('L'.$nro_fila, $total_gral_pm+$total_gral_pm_m);
            $this->excel->getActiveSheet()->setCellValue('M'.$nro_fila, $total_gral_pv+$total_gral_pv_m);
            $this->excel->getActiveSheet()->setCellValue('N'.$nro_fila, $total_gral_pe+$total_gral_pe_m);
            $this->excel->getActiveSheet()->setCellValue('O'.$nro_fila, $gran_total_porc);
            $this->excel->getActiveSheet()->setCellValue('R'.$nro_fila, $gran_total_media);
            $this->excel->getActiveSheet()->getStyle('A'.$nro_fila.':U'.$nro_fila)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => $this->total_general)));
        }
        $nombre="operaciones_x_region".strftime( "%Y-%m-%d",time()).'.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$nombre);
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // Forzamos a la descarga
        ob_end_clean();
        $objWriter->save('php://output');
    }
    /*---------------------------------- Ejecución de acciones operativas por provincia -----------------------------*/
    public function acciones_por_provincia(){
        $enlaces=$this->menu_modelo->get_Modulos(10);
        $data['enlaces'] = $enlaces;

        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['meses'] = $this->model_ejecucion->list_meses(); /// Lista de Meses
        $data['provincias'] = $this->model_taqpacha->get_provincias();
        $this->load->view('reportes/reportes_mae/select_op_provincia', $data);
    }
    public function valida_acciones_por_provincia()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {

            $sql_in='';
            if($this->input->post('p1')=='on'){$p1=1;$tp_1=1;}else{$p1=0;$tp_1=0;}
            if($this->input->post('p2')=='on'){$p2=1;$tp_2=2;}else{$p2=0;$tp_2=0;}
            if($this->input->post('p3')=='on'){$p3=1;$tp_3=3;}else{$p3=0;$tp_3=0;}
            if($this->input->post('p4')=='on'){$p4=1;$tp_4=4;}else{$p4=0;$tp_4=0;}
            $sql_in='in ('.$tp_1.','.$tp_2.','.$tp_3.','.$tp_4.')';

            $enlaces = $this->menu_modelo->get_Modulos(10);
            $data['enlaces'] = $enlaces;
            for ($i = 0; $i < count($enlaces); $i++) {
                $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;

            $meses=$this->get_mes($this->input->post("meses_id"));
            $mes = $meses[1];
            $dias = $meses[2];
            $mes_id=$this->input->post("meses_id");
            $data['id_mes'] = $mes_id;
            $data['mes'] = $mes;
            $data['dias'] = $dias;



            $pr1='';$pr2='';$pr3='';$pr4='';
            if($p1==1){$pr1='<span class="pi_t">PROYECTOS DE INVERSI&Oacute;N</span>';}
            if($p2==1){$pr2=' - <span class="pr_t">PROGRAMAS RECURRENTES</span>';}
            if($p3==1){$pr3=' - <span class="pnr_t">PROGRAMAS NO RECURRENTES</span>';}
            if($p4==1){$pr4=' - <span class="of_t">OPERACI&Oacute;N DE FUNCIONAMIENTO</span>';}

            $data['pr1']=$pr1;
            $data['pr2']=$pr2;
            $data['pr3']=$pr3;
            $data['pr4']=$pr4;

            $data['p1']=$p1;
            $data['p2']=$p2;
            $data['p3']=$p3;
            $data['p4']=$p4;

            $data['prov_id']=$this->input->post('provinci_id');

            if($this->input->post('provinci_id')==-1){// mostrar reporetes
                $data['proyectos']=$this->list_ejecucion_financiera_provincias_todos($p1,$p2,$p3,$p4,$sql_in,1,$mes_id);
                $data['title']=' Todas las provincias';
                $data['municipio']='';
                $this->load->view('reportes/reportes_mae/op_provincias', $data);
                //$this->load->view('admin/reportes/gerencial/nfinanciero/operaciones', $data);
            }
            else{
                $data['proyectos']=$this->list_ejecucion_financiera_x_provincia($p1,$p2,$p3,$p4,$sql_in,$this->input->post('provinci_id'),1,$mes_id);
                $data['title']=' Por provincia';
                $municipio = $this->model_taqpacha->get_provincia($this->input->post('provinci_id'));
                $data['municipio']='<br>PROVINCIA - '.$municipio[0]['prov_provincia'];
                $this->load->view('reportes/reportes_mae/op_provincias', $data);
            }
        }
        else{
            $this->session->set_flashdata('danger','Problemas al realizar la consulta, contactese con el Web Master');
            redirect('reportes_mae/provincia');
        }
    }
    public function reporte_acciones_por_provincia($p1,$p2,$p3,$p4,$prov_id,$mes_id)
    {
        $mess=$this->get_mes($mes_id);
        $mes = $mess[1];
        $dias = date('d');//$mess[2];
        $sql_in='';
        if($p1==1){$tp_1=1;}else{$tp_1=0;}
        if($p2==1){$tp_2=2;}else{$tp_2=0;}
        if($p3==1){$tp_3=3;}else{$tp_3=0;}
        if($p4==1){$tp_4=4;}else{$tp_4=0;}
        $sql_in='in ('.$tp_1.','.$tp_2.','.$tp_3.','.$tp_4.')';
        if($prov_id==-1){
            $detalles=$this->list_ejecucion_financiera_provincias_todos($p1,$p2,$p3,$p4,$sql_in,2,$mes_id);
            $prov_nombre='(TODAS)';
        }
        else{
            $detalles=$this->list_ejecucion_financiera_x_provincia($p1,$p2,$p3,$p4,$sql_in,$prov_id,2,$mes_id);
            $provincia = $this->model_taqpacha->get_provincia($prov_id);
            $prov_nombre=strtoupper($provincia[0]['prov_provincia']);
        }

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_resumen.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> EJECUCI&Oacute;N DE ACCIONES OPERATIVAS POR PROVINCIA / '.$prov_nombre.'<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$detalles.'
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("ejecucion_acciones_operativas_provincial.pdf", array("Attachment" => false));
    }
    function list_ejecucion_financiera_provincias_todos($p1,$p2,$p3,$p4,$sql_in,$tp_rep,$mes_id){
        if($tp_rep==1){
            $tb='id="dt_basic_float" class="table table-bordered"';
        }
        elseif ($tp_rep==2) {
            $tb='border="1" cellpadding="0" cellspacing="0" class="table table table-bordered" width="100%"';
        }

        $nro=0;
        $tabla ='';
        if($tp_rep==1) {
            $tabla .= '<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h2 class="alert alert-success"><center>EJECUCI&Oacute;N DE ACCIONES OPERATÍVAS POR PROVINCIA </center></h2>
                  </article>                
                  <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                        <header>
                          <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                          <h2 class="font-md"><strong>TODAS LAS PROVINCIAS</strong></h2>  
                        </header>
                <div>
                  <div class="widget-body no-padding">
                  <div class="table-responsive">';
        }
        $provincias = $this->model_taqpacha->get_provincias_sinMulti();
        if(count($provincias)!=0){
            $nro++;
            $tabla .='<table '.$tb.'>';
            $tabla .='<thead>';
            $tabla .='<tr><th class="titulo_cabecera" style="width:1%;" rowspan="2">#</th>
					      <th class="titulo_cabecera" style="width:15%;" rowspan="2">PROVINCIA</th>';
            if($p1==1)
                $tabla .='<th class="titulo_cabecera" style="width:15%;" rowspan="2">NRO DE PROYECTOS</th>';
            if($p2==1)
                $tabla .='<th class="titulo_cabecera" style="width:20%;" rowspan="2">NRO DE PROGRAMAS RECURRENTES</th>';
            if($p3==1)
                $tabla .='<th class="titulo_cabecera" style="width:10%;" rowspan="2">NRO DE PROGRAMAS NO RECURRENTES</th>';
            if($p4==1)
                $tabla .='<th class="titulo_cabecera" rowspan="2">NRO. OP. DE FUNCIONAMIENTO </th>';

            $tabla .='<th colspan="5" class="cabecera">PRESUPUESTO GESTI&Oacute;N '.$this->gestion.'</th></tr>';
            $tabla .='<tr class="modo1">';
            $tabla .='<th class="cabecera">INICIAL </th>';
            $tabla .='<th class="cabecera">MODIFICACIONES </th>';
            $tabla .='<th class="cabecera">VIGENTE </th>';
            $tabla .='<th class="cabecera">EJECUTADO </th>';
            $tabla .='<th class="cabecera">% DE EJECUCIÓN PRESUPUESTARIA </th>';
            $tabla .='</tr>';
            $tabla .='</thead>';
            $nrop=0;
            $tabla .='<tbody>';
            $total_pi=0;$total_pr=0;$total_pnr=0;$total_of=0;
            $total_mm_pi=0;$total_mm_pr=0;$total_mm_pnr=0;$total_mm_of=0;
            $total_pemm_pini=0;$total_pemm_pmod=0;$total_pemm_pvig=0;$total_pemm_peje=0;
            $total_gral_pe_pini=0;$total_gral_pe_pmod=0;$total_gral_pe_pvig=0;$total_gral_pe_peje=0;
            $proy_mult[]=array();
            foreach($provincias as $row_prov)
            {
                $nrop++;
                $tabla .='<tr class="derecha">';
                $tabla .='<td class="centro">'.$nrop.'</td>';
                $tabla .='<td class="izquierda">'.$row_prov['prov_provincia'].'</td>';

                $proyectos=$this->model_taqpacha->proyectos_x_provincia($row_prov['prov_id'],$this->gestion,$mes_id,$sql_in);
                if(count($proyectos)!=0){
                    $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                    $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
                    foreach($proyectos as $rowp){
                        $prov_proy=$this->model_taqpacha->provincias_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                        if($prov_proy[0]['total_prov']!='' && $prov_proy[0]['total_prov']>1){//Multiprovincial
                            if(count($proy_mult)<=0)
                                $proy_mult[]=$rowp['proy_id'];
                            $encontrado=0;
                            for($i=0;$i<count($proy_mult);$i++){
                                if($proy_mult[$i]==$rowp['proy_id']){
                                    $encontrado=1;
                                    break;
                                }
                            }
                            if ($encontrado==0){
                                $proy_mult[]=$rowp['proy_id'];
                                $total_pemm_pini=$total_pemm_pini+$rowp['pe_pi'];
                                $total_pemm_pmod=$total_pemm_pmod+$rowp['pe_pm'];
                                $total_pemm_pvig=$total_pemm_pvig+$rowp['pe_pv'];
                                $total_pemm_peje=$total_pemm_peje+$rowp['pe_pe'];
                                if($rowp['tp_id']==1){//Proyecto de Inversión
                                    $total_mm_pi=$total_mm_pi+1;
                                }elseif($rowp['tp_id']==2){//Programa Recurrente
                                    $total_mm_pr=$total_mm_pr+1;
                                }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                                    $total_mm_pnr=$total_mm_pnr+1;
                                }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                                    $total_mm_of=$total_mm_of+1;
                                }
                            }
                        }else{ //Unica provincia
                            $total_pe_pini=$total_pe_pini+$rowp['pe_pi'];
                            $total_pe_pmod=$total_pe_pmod+$rowp['pe_pm'];
                            $total_pe_pvig=$total_pe_pvig+$rowp['pe_pv'];
                            $total_pe_peje=$total_pe_peje+$rowp['pe_pe'];
                            if($rowp['pe_pv']!=0){
                                $total_pe_porc=$rowp['pe_pe']/$rowp['pe_pv']*100;
                            }
                            if($rowp['tp_id']==1){//Proyecto de Inversión
                                $tot_pi=$tot_pi+1;
                            }elseif($rowp['tp_id']==2){//Programa Recurrente
                                $tot_pr=$tot_pr+1;
                            }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                                $tot_pnr=$tot_pnr+1;
                            }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                                $tot_of=$tot_of+1;
                            }
                        }
                    }
                }else{
                    $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                    $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
                }
                $total_pe_porc=0;
                if($total_pe_pvig!=0){
                    $total_pe_porc=$total_pe_peje/$total_pe_pvig*100;
                }
                if($p1==1)
                    $tabla .='<td class="pi">'.$tot_pi.'</td>';
                if($p2==1)
                    $tabla .='<td class="pr">'.$tot_pr.'</td>';
                if($p3==1)
                    $tabla .='<td class="pnr">'.$tot_pnr.'</td>';
                if($p4==1)
                    $tabla .='<td class="of">'.$tot_of.'</td>';

                $tabla .='<td>'.number_format($total_pe_pini, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_pmod, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_pvig, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_peje, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_porc, 2, ',', '.').'%</td>';
                $tabla .='</tr>';
                $total_pi =$total_pi+$tot_pi;
                $total_pr =$total_pr+$tot_pr;
                $total_pnr=$total_pnr+$tot_pnr;
                $total_of =$total_of+$tot_of;
                $total_gral_pe_pini=$total_gral_pe_pini+$total_pe_pini;
                $total_gral_pe_pmod=$total_gral_pe_pmod+$total_pe_pmod;
                $total_gral_pe_pvig=$total_gral_pe_pvig+$total_pe_pvig;
                $total_gral_pe_peje=$total_gral_pe_peje+$total_pe_peje;

            }
            /*Linea multiprovincial*/
            $proyectos=$this->model_taqpacha->proyectos_x_provincia(906,$this->gestion,$mes_id,$sql_in);
            if(count($proyectos)>=1){
                $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                foreach($proyectos as $rowp){
                    $prov_proy=$this->model_taqpacha->provincias_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                    if($prov_proy[0]['total_prov']!='' && $prov_proy[0]['total_prov']==1){//Multiprovincial
                        $total_pemm_pini=$total_pemm_pini+$rowp['pe_pi'];
                        $total_pemm_pmod=$total_pemm_pmod+$rowp['pe_pm'];
                        $total_pemm_pvig=$total_pemm_pvig+$rowp['pe_pv'];
                        $total_pemm_peje=$total_pemm_peje+$rowp['pe_pe'];
                        if($rowp['tp_id']==1){//Proyecto de Inversión
                            $total_mm_pi=$total_mm_pi+1;
                        }elseif($rowp['tp_id']==2){//Programa Recurrente
                            $total_mm_pr=$total_mm_pr+1;
                        }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                            $total_mm_pnr=$total_mm_pnr+1;
                        }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                            $total_mm_of=$total_mm_of+1;
                        }
                    }
                }
            }
            $nrop++;
            $total_pemm_porc=0;
            if($total_pemm_pvig!=0){
                $total_pemm_porc=$total_pemm_peje/$total_pemm_pvig*100;
            }

            $tabla .='<tr class="multiple">';
            $tabla .='<td>'.$nrop.'</td>';
            $tabla .='<td class="izquierda">Multiprovincial</td>';
            if($p1==1)
                $tabla .='<td class="centro">'.$total_mm_pi.'</td>';
            if($p2==1)
                $tabla .='<td class="centro">'.$total_mm_pr.'</td>';
            if($p3==1)
                $tabla .='<td class="centro">'.$total_mm_pnr.'</td>';
            if($p4==1)
                $tabla .='<td class="centro">'.$total_mm_of.'</td>';

            $tabla .='<td>'.number_format($total_pemm_pini, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_pemm_pmod, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_pemm_pvig, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_pemm_peje, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_pemm_porc, 2, ',', '.').'%</td>';
            $tabla .='</tr>';

            $total_pi =$total_pi+$tot_pi+$total_mm_pi;
            $total_pr =$total_pr+$tot_pr+$total_mm_pr;
            $total_pnr=$total_pnr+$tot_pnr+$total_mm_pnr;
            $total_of =$total_of+$tot_of+$total_mm_of;


            $total_gral_pe_pini=$total_gral_pe_pini+$total_pemm_pini;
            $total_gral_pe_pmod=$total_gral_pe_pmod+$total_pemm_pmod;
            $total_gral_pe_pvig=$total_gral_pe_pvig+$total_pemm_pvig;
            $total_gral_pe_peje=$total_gral_pe_peje+$total_pemm_peje;
            $total_gral_pe_porc=0;
            if($total_gral_pe_pvig!=0){$total_gral_pe_porc=$total_gral_pe_peje/$total_gral_pe_pvig*100;}

            $tabla .='</tbody>';
            $tabla .='<tr class="total_general">';
            $tabla .='<td colspan="2" class="izquierda"><b>TOTAL : '.$nrop.'</b></td>';
            if($p1==1)
                $tabla .='<td class="centro">'.$total_pi.'</td>';
            if($p2==1)
                $tabla .='<td class="centro">'.$total_pr.'</td>';
            if($p3==1)
                $tabla .='<td class="centro">'.$total_pnr.'</td>';
            if($p4==1)
                $tabla .='<td class="centro">'.$total_of.'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pini, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pmod, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pvig, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_peje, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_porc, 2, ',', '.').'%</td>';
            $tabla .='</tr>';
            $tabla .='</table>';
            if($tp_rep==1) {
                $tabla .= '</div>
                </div>
              </div>
            </article>';
            };
        }

        return $tabla;
    }
    function list_ejecucion_financiera_x_provincia($p1,$p2,$p3,$p4,$sql_in,$prov_id,$tp,$mes_id)
    {
        if($tp==1){
            $tb='id="dt_basic_float" class="table table-bordered"';
        }
        elseif ($tp==2) {
            $tb='border="1" cellpadding="0" cellspacing="0" class="table table table-bordered" width="100%"';
        }

        $sum=0;
        $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
        $acum_pi=0;$acum_pr=0;$acum_pnr=0;$acum_of=0;
        $acum_pi_fis=0;$acum_pr_fis=0;$acum_pnr_fis=0;$acum_of_fis=0;
        $tot_cost_pi=0;$tot_cost_pr=0;$tot_cost_pnr=0;$tot_cost_of=0;
        $tot_mod_pi=0;$tot_mod_pr=0;$tot_mod_pnr=0;$tot_mod_of=0;
        $tot_ctv_pi=0;$tot_ctv_pr=0;$tot_ctv_pnr=0;$tot_ctv_of=0;
        $tot_epa_pi=0;$tot_epa_pr=0;$tot_epa_pnr=0;$tot_epa_of=0;
        $tot_pi_pi=0;$tot_pi_pr=0;$tot_pi_pnr=0;$tot_pi_of=0;
        $tot_pm_pi=0;$tot_pm_pr=0;$tot_pm_pnr=0;$tot_pm_of=0;
        $tot_pv_pi=0;$tot_pv_pr=0;$tot_pv_pnr=0;$tot_pv_of=0;
        $tot_pe_pi=0;$tot_pe_pr=0;$tot_pe_pnr=0;$tot_pe_of=0;

        $tot_pi_m=0;$tot_pr_m=0;$tot_pnr_m=0;$tot_of_m=0;
        $tot_cost_pi_m=0;$tot_cost_pr_m=0;$tot_cost_pnr_m=0;$tot_cost_of_m=0;
        $tot_mod_pi_m=0;$tot_mod_pr_m=0;$tot_mod_pnr_m=0;$tot_mod_of_m=0;
        $tot_ctv_pi_m=0;$tot_ctv_pr_m=0;$tot_ctv_pnr_m=0;$tot_ctv_of_m=0;
        $tot_epa_pi_m=0;$tot_epa_pr_m=0;$tot_epa_pnr_m=0;$tot_epa_of_m=0;
        $tot_pi_pi_m=0;$tot_pi_pr_m=0;$tot_pi_pnr_m=0;$tot_pi_of_m=0;
        $tot_pm_pi_m=0;$tot_pm_pr_m=0;$tot_pm_pnr_m=0;$tot_pm_of_m=0;
        $tot_pv_pi_m=0;$tot_pv_pr_m=0;$tot_pv_pnr_m=0;$tot_pv_of_m=0;
        $tot_pe_pi_m=0;$tot_pe_pr_m=0;$tot_pe_pnr_m=0;$tot_pe_of_m=0;

        $acumulado=0;$acumulado_multi=0;
        $acumulado_efis=0;$acumulado_multi_efis=0;
        $tabla ='';
        $tab_aux='';
        $tabla.='<table '.$tb.'>';
        $tabla.='<thead>';
        $tabla.='<tr>
				<th class="titulo_cabecera" style="width:1%;" rowspan="2">Nro.</th>
				<th class="titulo_cabecera" style="width:8%;" rowspan="2">NOMBRE</th>
				<th class="titulo_cabecera" style="width:5%;" rowspan="2">PROVINCIA <br>Y <br>MUNICIPIO(S)</th>
				<th class="titulo_cabecera" style="width:4%;" rowspan="2">TIPO</th>
				<th class="cabecera" colspan="3">COSTO TOTAL DE LA OPERACI&Oacute;N</th>
				<th class="cabecera_acum" colspan="3">EJECUCIÓN ACUMULADA</th>
	    		<th class="cabecera" colspan="5">PRESUPUESTO GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>
	    		<th class="cabecera_acum" colspan="3">EJECUCI&Oacute;N FÍSICA '.$this->session->userdata('gestion').'</th>
	    		<th class="titulo_cabecera" style="width:5%;" rowspan="2">ESTADO</th>
	    		<th class="titulo_cabecera" style="width:6%;" rowspan="2">UNIDAD EJECUTORA</th>
	    		<th class="titulo_cabecera" style="width:6%;" rowspan="2">RESPONSABLE</th>
			</tr>';

        $tabla.='<tr class="modo1">
				<th class="cabecera" style="width:6%;">INICIAL</th>
				<th class="cabecera" style="width:5%;">MODIFICACIONES</th>
				<th class="cabecera" style="width:6%;">VIGENTE</th>
				<th class="cabecera_acum" style="width:5%;">EJECUCIÓN FINANCIERA</th>
				<th class="cabecera_acum" style="width:5%;">% EJEC. FINANCIERA</th>
				<th class="cabecera_acum" style="width:5%;">% DE AVANCE FÍSICO</th>';
        $tabla.='<th class="cabecera" style="width:5%;"> INICIAL </th>';
        $tabla.='<th class="cabecera" style="width:5%;"> MODIFICADO </th>';
        $tabla.='<th class="cabecera" style="width:5%;"> VIGENTE </th>';
        $tabla.='<th class="cabecera" style="width:5%;">EJEC. FINANCIERA </th>';
        $tabla.='<th class="cabecera" style="width:5%;">% EJEC. FINANCIERA </th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">PROG. %</th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">EJEC %</th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">% EJEC. ANUAL</th>';
        $tabla.='</tr>';
        $tabla.='</thead>';
        $tabla.='<tbody>';

        $programas = $this->model_epresupuestaria->ejecucion_proyecto_programas_x_provincia($sql_in,$this->gestion,$mes_id,$prov_id);
        if(count($programas)!=0){
            $nro=0;
            $nro_mult=0;
            foreach($programas as $rowp)
            {
                if($prov_id!=906){
                    $prov_proy=$this->model_taqpacha->provincias_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                    $color_mult='';
                    if($prov_proy[0]['total_prov']!='' && $prov_proy[0]['total_prov']>1){
                        $color_mult='style="background-color:#EDFBFF"';
                    }

                    $se=$rowp['pe_pe'];
                    $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                    if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                        $se=$se+$suma[0]['total_ejecutado'];
                    }

                    $suma_prog=$this->mdas_complementario->costo_total_proyecto($rowp['pfec_id']);
                    $toal_modif=$this->mdas_complementario->modificaciones_costo_total($rowp['pfec_id']);
                    $estado=$this->mdas_complementario->estado_operacion($rowp['pfec_id'],$mes_id,$this->gestion);
                    $efis=$this->avance_fisico($rowp['proy_id'],$mes_id);
                    if($color_mult==''){
                        $tabla.='<tr class="modo1 derecha">';
                        $nro=$nro+1;
                        $tabla.='<td class="centro">'.$nro.'</td>';
                        $tabla.='<td class="izquierda"><a href="javascript:abreVentana(\''.site_url("reportes_mae").'/resumen_ejec/'.$rowp['proy_id'].'/'.$mes_id.'\');"'.'>'.$rowp['proy_nombre'].'</a></td>';
                        $tabla.='<td class="izquierda">';
                        $prov=$this->mmae->proyecto_provincia($rowp['proy_id']);
                        if((count($prov)>3) and ($tp == 1)){
                            $tabla .= '<p class="btn btn-default" href="#" class="form_pond" title="';
                        }elseif($tp ==2){
                            $tabla .= '<div style="font-size: 5px;">';
                        }
                        foreach($prov as $row_prov){
                            $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                            if (count($prov) > 3) {
                                if ($tp == 1) {
                                    foreach ($muni as $muni) {
                                        $tabla .= $row_prov['prov_provincia'] .'&#09;'. $muni['muni_municipio'] . '&#13;';
                                    }
                                } else {
                                    $tabla .='<ul>'.$row_prov['prov_provincia'];
                                    foreach ($muni as $muni) {
                                        $tabla .= '<li>'.$muni['muni_municipio'] . '</li>';
                                    }
                                    $tabla .= '</ul>';
                                }
                            } else {
                                $tabla .= '<ul><strong>'.$row_prov['prov_provincia'].'</strong>';
                                foreach ($muni as $muni) {
                                    $tabla .= '<li>'.$muni['muni_municipio'].'</li>';
                                }
                                $tabla .= '</ul>';
                            }
                        }
                        if((count($prov) > 3) and ($tp == 1)){
                            $tabla .= '"><i class="glyphicon glyphicon-search"></i>Ver...</p>';
                        }elseif($tp==2){
                            $tabla .= '</div>';
                        }
                        $tabla .='</td>';
                        $tabla.='<td class="izquierda">'.$rowp['tp_tipo'].'</td>';
                        $tabla.='<td>'.number_format($suma_prog[0]['total_programado'], 2, ',', '.').'</td>';

                        $total_mod=0;
                        if($toal_modif[0]['total_modif']!='' || $toal_modif[0]['total_modif']!=0){
                            $total_mod=$toal_modif[0]['total_modif'];
                        }
                        $tabla.='<td>'.number_format($total_mod, 2, ',', '.').'</td>';
                        $ctv=$suma_prog[0]['total_programado']+$total_mod;
                        $tabla.='<td>'.number_format($ctv, 2, ',', '.').'</td>';
                        $tabla.='<td>'.number_format($se, 2, ',', '.').'</td>';

                        $efin=0;
                        if($suma_prog[0]['total_programado']!=0){
                            $efin=round((($se/$suma_prog[0]['total_programado'])*100),2);
                        }
                        $acumulado_efis=$acumulado_efis+$efis;
                        $tabla.='<td>'.$efin.'%</td>';
                        $tabla.='<td>'.round($efis,2).'%</td>';
                        $tabla.='<td>'.number_format($rowp['pe_pi'], 2, ',', '.').'</td>';
                        $tabla.='<td>'.number_format($rowp['pe_pm'], 2, ',', '.').'</td>';
                        $tabla.='<td>'.number_format($rowp['pe_pv'], 2, ',', '.').'</td>';
                        $tabla.='<td>'.number_format($rowp['pe_pe'], 2, ',', '.').'</td>';

                        $ejec=0;
                        if($rowp['pe_pv']!=0){
                            $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                        }
                        $tabla.='<td>'.$ejec.'%</td>';
                        $estado_proy='No registrado';
                        if(count($estado)!=0){
                            $estado_proy=$estado[0]['ep_descripcion'];
                        }
                        $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));

                        $tabla .= '<td class="derecha">'.$ejecutado[0].'</td>';
                        $tabla .= '<td class="derecha">'.$ejecutado[1].'</td>';
                        $tabla .= '<td class="derecha">'.$ejecutado[2].'</td>';
                        $acumulado=$acumulado+$ejecutado[2];
                        $tabla.='<td class="izquierda">'.$estado_proy.'</td>';
                        $tabla.='<td class="izquierda">'.$rowp['uni_unidad'].'</td>';
                        $tabla.='<td class="izquierda">'.$rowp['responsable'].'<br><font color="red">Telf: '.$rowp['fun_telefono'].'</font></td>';
                        $tabla.='</tr>';

                        if($rowp['tp_id']==1){//Proyecto de Inversión
                            $tot_pi=$tot_pi+1;
                            $acum_pi=$acum_pi+$ejecutado[2];
                            $acum_pi_fis=$acum_pi_fis+$efis;
                            $tot_cost_pi=$tot_cost_pi+$suma_prog[0]['total_programado'];
                            $tot_mod_pi=$tot_mod_pi+$total_mod;
                            $tot_ctv_pi=$tot_ctv_pi+$ctv;
                            $tot_epa_pi=$tot_epa_pi+$se;
                            $tot_pi_pi=$tot_pi_pi+$rowp['pe_pi'];
                            $tot_pm_pi=$tot_pm_pi+$rowp['pe_pm'];
                            $tot_pv_pi=$tot_pv_pi+$rowp['pe_pv'];
                            $tot_pe_pi=$tot_pe_pi+$rowp['pe_pe'];
                        }elseif($rowp['tp_id']==2){//Programa Recurrente
                            $tot_pr=$tot_pr+1;
                            $acum_pr=$acum_pr+$ejecutado[2];
                            $acum_pr_fis=$acum_pr_fis+$efis;
                            $tot_cost_pr=$tot_cost_pr+$suma_prog[0]['total_programado'];
                            $tot_mod_pr=$tot_mod_pr+$total_mod;
                            $tot_ctv_pr=$tot_ctv_pr+$ctv;
                            $tot_epa_pr=$tot_epa_pr+$se;
                            $tot_pi_pr=$tot_pi_pr+$rowp['pe_pi'];
                            $tot_pm_pr=$tot_pm_pr+$rowp['pe_pm'];
                            $tot_pv_pr=$tot_pv_pr+$rowp['pe_pv'];
                            $tot_pe_pr=$tot_pe_pr+$rowp['pe_pe'];
                        }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                            $tot_pnr=$tot_pnr+1;
                            $acum_pnr=$acum_pnr+$ejecutado[2];
                            $acum_pnr_fis=$acum_pnr_fis+$efis;
                            $tot_cost_pnr=$tot_cost_pnr+$suma_prog[0]['total_programado'];
                            $tot_mod_pnr=$tot_mod_pnr+$total_mod;
                            $tot_ctv_pnr=$tot_ctv_pnr+$ctv;
                            $tot_epa_pnr=$tot_epa_pnr+$se;
                            $tot_pi_pnr=$tot_pi_pnr+$rowp['pe_pi'];
                            $tot_pm_pnr=$tot_pm_pnr+$rowp['pe_pm'];
                            $tot_pv_pnr=$tot_pv_pnr+$rowp['pe_pv'];
                            $tot_pe_pnr=$tot_pe_pnr+$rowp['pe_pe'];
                        }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                            $tot_of=$tot_of+1;
                            $acum_of=$acum_of+$ejecutado[2];
                            $acum_of_fis=$acum_of_fis+$efis;
                            $tot_cost_of=$tot_cost_of+$suma_prog[0]['total_programado'];
                            $tot_mod_of=$tot_mod_of+$total_mod;
                            $tot_ctv_of=$tot_ctv_of+$ctv;
                            $tot_epa_of=$tot_epa_of+$se;
                            $tot_pi_of=$tot_pi_of+$rowp['pe_pi'];
                            $tot_pm_of=$tot_pm_of+$rowp['pe_pm'];
                            $tot_pv_of=$tot_pv_of+$rowp['pe_pv'];
                            $tot_pe_of=$tot_pe_of+$rowp['pe_pe'];
                        }
                    }else{
                        /**Multi**/
                        $nro_mult=$nro_mult+1;
                        $tab_aux.='<tr class="modo1 derecha" '.$color_mult.'>';
                        $tab_aux.='<td class="centro">'.$nro_mult.'</td>';
                        $tab_aux.='<td class="izquierda"><a href="javascript:abreVentana(\''.site_url("reportes_mae").'/resumen_ejec/'.$rowp['proy_id'].'/'.$mes_id.'\');"'.'>'.$rowp['proy_nombre'].'</a></td>';
                        $tab_aux.='<td class="izquierda">';
                        $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);
                        if((count($prov) > 3) and ($tp == 1)){
                            $tab_aux .= '<p class="btn btn-default" href="#" class="form_pond" title="';
                        }elseif($tp ==2){
                            $tab_aux .= '<div style="font-size: 4px;">';
                        }
                        foreach($prov as $row_prov){
                            $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                            if (count($prov) > 3) {
                                if ($tp == 1) {
                                    foreach ($muni as $muni) {
                                        $tab_aux .= $row_prov['prov_provincia'] .'&#09;'. $muni['muni_municipio'] . '&#13;';
                                    }
                                } else {
                                    $tab_aux .='<ul>'.$row_prov['prov_provincia'];
                                    foreach ($muni as $muni) {
                                        $tab_aux .= '<li>'.$muni['muni_municipio'].'</li>';
                                    }
                                    $tab_aux .= '</ul>';
                                }
                            } else {
                                $tab_aux .= '<ul><strong>'.$row_prov['prov_provincia'].'</strong>';
                                foreach ($muni as $muni) {
                                    $tab_aux .= '<li>'.$muni['muni_municipio'].'</li>';
                                }
                                $tab_aux .= '</ul>';
                            }
                        }
                        if((count($prov) > 3) and ($tp == 1)){
                            $tab_aux .= '"><i class="glyphicon glyphicon-search"></i>Ver...</p>';
                        }elseif($tp==2){
                            $tab_aux .= '</div>';
                        }

                        $tab_aux .='</td>';
                        $tab_aux.='<td class="izquierda">'.$rowp['tp_tipo'].'</td>';
                        $tab_aux.='<td>'.number_format($suma_prog[0]['total_programado'], 2, ',', '.').'</td>';

                        $total_mod=0;
                        if($toal_modif[0]['total_modif']!='' || $toal_modif[0]['total_modif']!=0){
                            $total_mod=$toal_modif[0]['total_modif'];
                        }
                        $tab_aux.='<td>'.number_format($total_mod, 2, ',', '.').'</td>';
                        $ctv=$suma_prog[0]['total_programado']+$total_mod;
                        $tab_aux.='<td>'.number_format($ctv, 2, ',', '.').'</td>';
                        $tab_aux.='<td>'.number_format($se, 2, ',', '.').'</td>';

                        $efin=0;
                        if($suma_prog[0]['total_programado']!=0){
                            $efin=round((($se/$suma_prog[0]['total_programado'])*100),2);
                        }
                        $acumulado_multi_efis=$acumulado_multi_efis+$efis;
                        $tab_aux.='<td>'.$efin.'%</td>';
                        $tab_aux.='<td>'.round($efis,2).'%</td>';
                        $tab_aux.='<td>'.number_format($rowp['pe_pi'], 2, ',', '.').'</td>';
                        $tab_aux.='<td>'.number_format($rowp['pe_pm'], 2, ',', '.').'</td>';
                        $tab_aux.='<td>'.number_format($rowp['pe_pv'], 2, ',', '.').'</td>';
                        $tab_aux.='<td>'.number_format($rowp['pe_pe'], 2, ',', '.').'</td>';

                        $ejec=0;
                        if($rowp['pe_pv']!=0){
                            $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                        }
                        $tab_aux.='<td>'.$ejec.'%</td>';
                        $estado_proy='No registrado';
                        if(count($estado)!=0){
                            $estado_proy=$estado[0]['ep_descripcion'];
                        }
                        $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));
                        $tab_aux .= '<td class="derecha">'.$ejecutado[0].'</td>';
                        $tab_aux .= '<td class="derecha">'.$ejecutado[1].'</td>';
                        $tab_aux .= '<td class="derecha">'.$ejecutado[2].'</td>';
                        $acumulado_multi=$acumulado_multi+$ejecutado[2];
                        $tab_aux.='<td class="izquierda">'.$estado_proy.'</td>';
                        $tab_aux.='<td class="izquierda">'.$rowp['uni_unidad'].'</td>';
                        $tab_aux.='<td class="izquierda">'.$rowp['responsable'].'<br><font color="red">Telf: '.$rowp['fun_telefono'].'</font></td>';
                        $tab_aux.='</tr>';
                        if ($rowp['tp_id'] == 1) {//Proyecto de Inversión
                            $tot_pi_m = $tot_pi_m + 1;
                            $tot_cost_pi_m = $tot_cost_pi_m + $suma_prog[0]['total_programado'];
                            $tot_mod_pi_m = $tot_mod_pi_m + $total_mod;
                            $tot_ctv_pi_m = $tot_ctv_pi_m + $ctv;
                            $tot_epa_pi_m = $tot_epa_pi_m + $se;
                            $tot_pi_pi_m = $tot_pi_pi_m + $rowp['pe_pi'];
                            $tot_pm_pi_m = $tot_pm_pi_m + $rowp['pe_pm'];
                            $tot_pv_pi_m = $tot_pv_pi_m + $rowp['pe_pv'];
                            $tot_pe_pi_m = $tot_pe_pi_m + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 2) {//Programa Recurrente
                            $tot_pr_m = $tot_pr_m + 1;
                            $tot_cost_pr_m = $tot_cost_pr_m + $suma_prog[0]['total_programado'];
                            $tot_mod_pr_m = $tot_mod_pr_m + $total_mod;
                            $tot_ctv_pr_m = $tot_ctv_pr_m + $ctv;
                            $tot_epa_pr_m = $tot_epa_pr_m + $se;
                            $tot_pi_pr_m = $tot_pi_pr_m + $rowp['pe_pi'];
                            $tot_pm_pr_m = $tot_pm_pr_m + $rowp['pe_pm'];
                            $tot_pv_pr_m = $tot_pv_pr_m + $rowp['pe_pv'];
                            $tot_pe_pr_m = $tot_pe_pr_m + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 3) {//Programa No  Recurrente
                            $tot_pnr_m = $tot_pnr_m + 1;
                            $tot_cost_pnr_m = $tot_cost_pnr_m + $suma_prog[0]['total_programado'];
                            $tot_mod_pnr_m = $tot_mod_pnr_m + $total_mod;
                            $tot_ctv_pnr_m = $tot_ctv_pnr_m + $ctv;
                            $tot_epa_pnr_m = $tot_epa_pnr_m + $se;
                            $tot_pi_pnr_m = $tot_pi_pnr_m + $rowp['pe_pi'];
                            $tot_pm_pnr_m = $tot_pm_pnr_m + $rowp['pe_pm'];
                            $tot_pv_pnr_m = $tot_pv_pnr_m + $rowp['pe_pv'];
                            $tot_pe_pnr_m = $tot_pe_pnr_m + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 4) {//Operacion de Funcionamiento
                            $tot_of_m = $tot_of_m + 1;
                            $tot_cost_of_m = $tot_cost_of_m + $suma_prog[0]['total_programado'];
                            $tot_mod_of_m = $tot_mod_of_m + $total_mod;
                            $tot_ctv_of_m = $tot_ctv_of_m + $ctv;
                            $tot_epa_of_m = $tot_epa_of_m + $se;
                            $tot_pi_of_m = $tot_pi_of_m + $rowp['pe_pi'];
                            $tot_pm_of_m = $tot_pm_of_m + $rowp['pe_pm'];
                            $tot_pv_of_m = $tot_pv_of_m + $rowp['pe_pv'];
                            $tot_pe_of_m = $tot_pe_of_m + $rowp['pe_pe'];
                        }
                    }
                }else{
                    $prov_proy=$this->model_taqpacha->provincias_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                    $color_mult='';
                    if(($prov_proy[0]['total_prov']!='' && $prov_proy[0]['total_prov']>1) || ($rowp['provincia']==906)){//Multiprovincial
                        $se=$rowp['pe_pe'];
                        $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                        if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                            $se=$se+$suma[0]['total_ejecutado'];
                        }

                        $suma_prog=$this->mdas_complementario->costo_total_proyecto($rowp['pfec_id']);
                        $toal_modif = $this->mdas_complementario->modificaciones_costo_total($rowp['pfec_id']);
                        $estado = $this->mdas_complementario->estado_operacion($rowp['pfec_id'],$mes_id,$this->gestion);
                        $efis=$this->avance_fisico($rowp['proy_id'],$mes_id);

                        $tabla.='<tr class="modo1 derecha">';
                        $nro=$nro+1;
                        $tabla.='<td class="centro">'.$nro.'</td>';
                        $tabla.='<td class="izquierda"><a href="javascript:abreVentana(\''.site_url("reportes_mae").'/resumen_ejec/'.$rowp['proy_id'].'/'.$mes_id.'\');"'.'>'.$rowp['proy_nombre'].'</a></td>';
                        $tabla.='<td class="izquierda">';
                        $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);
                        if((count($prov)>3) and ($tp == 1)){
                            $tabla .= '<p class="btn btn-default" href="#" class="form_pond" title="';
                        }elseif($tp ==2){
                            $tabla .= '<div style="font-size: 5px;">';
                        }
                        foreach($prov as $row_prov){
                            $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                            if (count($prov) > 3) {
                                if ($tp == 1) {
                                    foreach ($muni as $muni) {
                                        $tabla .= $row_prov['prov_provincia'] .'&#09;'. $muni['muni_municipio'] . '&#13;';
                                    }
                                } else {
                                    $tabla .='<ul>'.$row_prov['prov_provincia'];
                                    foreach ($muni as $muni) {
                                        $tabla .= '<li>'.$muni['muni_municipio'] . '</li>';
                                    }
                                    $tabla .= '</ul>';
                                }
                            } else {
                                $tabla .= '<ul><strong>'.$row_prov['prov_provincia'].'</strong>';
                                foreach ($muni as $muni) {
                                    $tabla .= '<li>'.$muni['muni_municipio'].'</li>';
                                }
                                $tabla .= '</ul>';
                            }
                        }
                        if((count($prov) > 3) and ($tp == 1)){
                            $tabla .= '"><i class="glyphicon glyphicon-search"></i>Ver...</p>';
                        }elseif($tp==2){
                            $tabla .= '</div>';
                        }
                        $tabla .='</td>';
                        $tabla.='<td class="izquierda">'.$rowp['tp_tipo'].'</td>';
                        $tabla.='<td>'.number_format($suma_prog[0]['total_programado'], 2, ',', '.').'</td>';

                        $total_mod=0;
                        if($toal_modif[0]['total_modif']!='' || $toal_modif[0]['total_modif']!=0){
                            $total_mod=$toal_modif[0]['total_modif'];
                        }
                        $tabla.='<td>'.number_format($total_mod, 2, ',', '.').'</td>';
                        $ctv=$suma_prog[0]['total_programado']+$total_mod;
                        $tabla.='<td>'.number_format($ctv, 2, ',', '.').'</td>';
                        $tabla.='<td>'.number_format($se, 2, ',', '.').'</td>';

                        $efin=0;
                        if($suma_prog[0]['total_programado']!=0){
                            $efin=round((($se/$suma_prog[0]['total_programado'])*100),2);
                        }
                        $tabla.='<td>'.$efin.'%</td>';
                        $tabla.='<td>'.round($efis,2).'%</td>';
                        $tabla.='<td>'.number_format($rowp['pe_pi'], 2, ',', '.').'</td>';
                        $tabla.='<td>'.number_format($rowp['pe_pm'], 2, ',', '.').'</td>';
                        $tabla.='<td>'.number_format($rowp['pe_pv'], 2, ',', '.').'</td>';
                        $tabla.='<td>'.number_format($rowp['pe_pe'], 2, ',', '.').'</td>';

                        $ejec=0;
                        if($rowp['pe_pv']!=0){
                            $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                        }
                        $tabla.='<td>'.$ejec.'%</td>';
                        $estado_proy='No registrado';
                        if(count($estado)!=0){
                            $estado_proy=$estado[0]['ep_descripcion'];
                        }
                        $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));
                        $tabla .= '<td class="izquierda">'.$ejecutado[0].'</td>';
                        $tabla .= '<td class="izquierda">'.$ejecutado[1].'</td>';
                        $tabla .= '<td class="izquierda">'.$ejecutado[2].'</td>';
                        //$acumulado_multi=$acumulado_multi+$ejecutado[2];
                        $tabla.='<td class="izquierda">'.$estado_proy.'</td>';
                        $tabla.='<td class="izquierda">'.$rowp['uni_unidad'].'</td>';
                        $tabla.='<td class="izquierda">'.$rowp['responsable'].'<br><font color="red">Telf: '.$rowp['fun_telefono'].'</font></td>';
                        $tabla.='</tr>';
                        if($rowp['tp_id']==1){//Proyecto de Inversión
                            $tot_pi=$tot_pi+1;
                            $tot_cost_pi=$tot_cost_pi+$suma_prog[0]['total_programado'];
                            $tot_mod_pi=$tot_mod_pi+$total_mod;
                            $tot_ctv_pi=$tot_ctv_pi+$ctv;
                            $tot_epa_pi=$tot_epa_pi+$se;
                            $tot_pi_pi=$tot_pi_pi+$rowp['pe_pi'];
                            $tot_pm_pi=$tot_pm_pi+$rowp['pe_pm'];
                            $tot_pv_pi=$tot_pv_pi+$rowp['pe_pv'];
                            $tot_pe_pi=$tot_pe_pi+$rowp['pe_pe'];
                        }elseif($rowp['tp_id']==2){//Programa Recurrente
                            $tot_pr=$tot_pr+1;
                            $tot_cost_pr=$tot_cost_pr+$suma_prog[0]['total_programado'];
                            $tot_mod_pr=$tot_mod_pr+$total_mod;
                            $tot_ctv_pr=$tot_ctv_pr+$ctv;
                            $tot_epa_pr=$tot_epa_pr+$se;
                            $tot_pi_pr=$tot_pi_pr+$rowp['pe_pi'];
                            $tot_pm_pr=$tot_pm_pr+$rowp['pe_pm'];
                            $tot_pv_pr=$tot_pv_pr+$rowp['pe_pv'];
                            $tot_pe_pr=$tot_pe_pr+$rowp['pe_pe'];
                        }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                            $tot_pnr=$tot_pnr+1;
                            $tot_cost_pnr=$tot_cost_pnr+$suma_prog[0]['total_programado'];
                            $tot_mod_pnr=$tot_mod_pnr+$total_mod;
                            $tot_ctv_pnr=$tot_ctv_pnr+$ctv;
                            $tot_epa_pnr=$tot_epa_pnr+$se;
                            $tot_pi_pnr=$tot_pi_pnr+$rowp['pe_pi'];
                            $tot_pm_pnr=$tot_pm_pnr+$rowp['pe_pm'];
                            $tot_pv_pnr=$tot_pv_pnr+$rowp['pe_pv'];
                            $tot_pe_pnr=$tot_pe_pnr+$rowp['pe_pe'];
                        }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                            $tot_of=$tot_of+1;
                            $tot_cost_of=$tot_cost_of+$suma_prog[0]['total_programado'];
                            $tot_mod_of=$tot_mod_of+$total_mod;
                            $tot_ctv_of=$tot_ctv_of+$ctv;
                            $tot_epa_of=$tot_epa_of+$se;
                            $tot_pi_of=$tot_pi_of+$rowp['pe_pi'];
                            $tot_pm_of=$tot_pm_of+$rowp['pe_pm'];
                            $tot_pv_of=$tot_pv_of+$rowp['pe_pv'];
                            $tot_pe_of=$tot_pe_of+$rowp['pe_pe'];
                        }
                    }
                }
            }
        }
        $tot_porc_pi=0;$tot_porc_pnr=0;$tot_porc_pr=0;$tot_porc_of=0;
        if($tot_pv_pi!=0){$tot_porc_pi  =$tot_pe_pi/$tot_pv_pi*100;}
        if($tot_pv_pnr!=0){$tot_porc_pnr=$tot_pe_pnr/$tot_pv_pnr*100;}
        if($tot_pv_pr!=0){$tot_porc_pr  =$tot_pe_pr/$tot_pv_pr*100;}
        if($tot_pv_of!=0){$tot_porc_of  =$tot_pe_of/$tot_pv_of*100;}

        $tot_pora_pi=0;$tot_pora_pnr=0;$tot_pora_pr=0;$tot_pora_of=0;
        if($tot_ctv_pi!=0){$tot_pora_pi=$tot_epa_pi/$tot_ctv_pi*100;}
        if($tot_ctv_pnr!=0){$tot_pora_pnr=$tot_epa_pnr/$tot_ctv_pnr*100;}
        if($tot_ctv_pr!=0){$tot_pora_pr=$tot_epa_pr/$tot_ctv_pr*100;}
        if($tot_ctv_of!=0){$tot_pora_of=$tot_epa_of/$tot_ctv_of*100;}
        $total_gral_op  =$tot_pi+$tot_pnr+$tot_pr+$tot_of;
        $total_gral_cost=$tot_cost_pi+$tot_cost_pnr+$tot_cost_pr+$tot_cost_of;
        $total_gral_mod =$tot_mod_pi+$tot_mod_pnr+$tot_mod_pr+$tot_mod_of;
        $total_gral_ctv =$tot_ctv_pi+$tot_ctv_pnr+$tot_ctv_pr+$tot_ctv_of;
        $total_gral_epa =$tot_epa_pi+$tot_epa_pnr+$tot_epa_pr+$tot_epa_of;
        $total_gral_pora=0;
        if($total_gral_ctv!=0){$total_gral_pora=$total_gral_epa/$total_gral_ctv*100;}

        $total_gral_pi  =$tot_pi_pi+$tot_pi_pnr+$tot_pi_pr+$tot_pi_of;
        $total_gral_pm  =$tot_pm_pi+$tot_pm_pnr+$tot_pm_pr+$tot_pm_of;
        $total_gral_pv  =$tot_pv_pi+$tot_pv_pnr+$tot_pv_pr+$tot_pv_of;
        $total_gral_pe  =$tot_pe_pi+$tot_pe_pnr+$tot_pe_pr+$tot_pe_of;
        $total_gral_porc=0;
        if($total_gral_pv!=0){$total_gral_porc=$total_gral_pe/$total_gral_pv*100;}

        /**Multiprovinciales**/
        $tot_porc_pi_m=0;$tot_porc_pnr_m=0;$tot_porc_pr_m=0;$tot_porc_of_m=0;
        if($tot_pv_pi_m!=0){$tot_porc_pi_m  =$tot_pe_pi_m/$tot_pv_pi_m*100;}
        if($tot_pv_pnr_m!=0){$tot_porc_pnr_m=$tot_pe_pnr_m/$tot_pv_pnr_m*100;}
        if($tot_pv_pr_m!=0){$tot_porc_pr_m  =$tot_pe_pr_m/$tot_pv_pr_m*100;}
        if($tot_pv_of_m!=0){$tot_porc_of_m  =$tot_pe_of_m/$tot_pv_of_m*100;}

        $tot_pora_pi_m=0;$tot_pora_pnr_m=0;$tot_pora_pr_m=0;$tot_pora_of_m=0;
        if($tot_ctv_pi_m!=0){$tot_pora_pi_m  =$tot_epa_pi_m/$tot_ctv_pi_m*100;}
        if($tot_ctv_pnr_m!=0){$tot_pora_pnr_m=$tot_epa_pnr_m/$tot_ctv_pnr_m*100;}
        if($tot_ctv_pr_m!=0){$tot_pora_pr_m  =$tot_epa_pr_m/$tot_ctv_pr_m*100;}
        if($tot_ctv_of_m!=0){$tot_pora_of_m  =$tot_epa_of_m/$tot_ctv_of_m*100;}

        $total_gral_op_m  =$tot_pi_m+$tot_pnr_m+$tot_pr_m+$tot_of_m;
        $total_gral_cost_m=$tot_cost_pi_m+$tot_cost_pnr_m+$tot_cost_pr_m+$tot_cost_of;
        $total_gral_mod_m =$tot_mod_pi_m+$tot_mod_pnr_m+$tot_mod_pr_m+$tot_mod_of_m;
        $total_gral_ctv_m =$tot_ctv_pi_m+$tot_ctv_pnr_m+$tot_ctv_pr_m+$tot_ctv_of_m;
        $total_gral_epa_m =$tot_epa_pi_m+$tot_epa_pnr_m+$tot_epa_pr_m+$tot_epa_of_m;
        $total_gral_pora_m=0;
        if($total_gral_ctv_m!=0){$total_gral_pora_m=$total_gral_epa_m/$total_gral_ctv_m*100;}

        $total_gral_pi_m  =$tot_pi_pi_m+$tot_pi_pnr_m+$tot_pi_pr_m+$tot_pi_of_m;
        $total_gral_pm_m  =$tot_pm_pi_m+$tot_pm_pnr_m+$tot_pm_pr_m+$tot_pm_of_m;
        $total_gral_pv_m  =$tot_pv_pi_m+$tot_pv_pnr_m+$tot_pv_pr_m+$tot_pv_of_m;
        $total_gral_pe_m  =$tot_pe_pi_m+$tot_pe_pnr_m+$tot_pe_pr_m+$tot_pe_of_m;
        $total_gral_porc_m=0;
        if($total_gral_pv_m!=0){$total_gral_porc_m=$total_gral_pe_m/$total_gral_pv_m*100;}
        $tabla.='</tbody>';
        if($p1==1) {
            $total_acum_pi=0;$total_acum_pi_fis=0;
            if($tot_pi!=0){
                $total_acum_pi=number_format($acum_pi/$tot_pi,2,',','.');
                $total_acum_pi_fis=number_format($acum_pi_fis/$tot_pi,2,',','.');
            }
            $tabla .= '<tr class="total_pi">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROYECTOS DE INVERSIÓN</td>';
            $tabla .= '<th >' . $tot_pi . '</th><th class="derecha">' . number_format($tot_cost_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pi,2,',','.') . ' %</th><th class="derecha">'.$total_acum_pi_fis.'</th>
                <th class="derecha">' . number_format($tot_pi_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pi,2,',','.') . '%</th><th ></th><th ></th><th class="derecha">'.$total_acum_pi.'</th><th colspan="3"></th></tr>';
        }
        if($p2==1) {
            $total_acum_pr=0;$total_acum_pr_fis=0;
            if($tot_pr!=0){
                $total_acum_pr=number_format($acum_pr/$tot_pr,2,',','.');
                $total_acum_pr_fis=number_format($acum_pr_fis/$tot_pr,2,',','.');
            }
            $tabla .= '<tr class="total_pr">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROGRAMAS RECURRENTES</td>';
            $tabla .= '<th >' . $tot_pr . '</th><th class="derecha">' . number_format($tot_cost_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pr,2,',','.') . ' %</th><th class="derecha">'.$total_acum_pr_fis.'</th>
                 <th class="derecha">' . number_format($tot_pi_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pr,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_pr.'</th><th colspan="3"></th></tr>';
        }
        if($p3==1) {
            $total_acum_pnr=0;$total_acum_prn_fis=0;
            if($tot_pnr!=0){
                $total_acum_pnr=number_format($acum_pnr/$tot_pnr,2,',','.');
                $total_acum_prn_fis=number_format($acum_pnr_fis/$tot_pnr,2,',','.');
            }
            $tabla .= '<tr class="total_pnr">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROGRAMAS NO RECURRENTES</td>';
            $tabla .= '<th >' . $tot_pnr . '</th><th class="derecha">' . number_format($tot_cost_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pnr,2,',','.') . ' %</th><th class="derecha">'.$total_acum_prn_fis.'</th>
                 <th class="derecha">' . number_format($tot_pi_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pnr,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_pnr.'</th><th colspan="3"></th></tr>';
        }
        if($p4==1) {
            $total_acum_of=0;$total_acum_of_fis=0;
            if($tot_of!=0){
                $total_acum_of=number_format($acum_of/$tot_of,2,',','.');
                $total_acum_of_fis=number_format($acum_of_fis/$tot_of,2,',','.');
            }
            $tabla .= '<tr class="total_of">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL OPERACIONES DE FUNCIONAMIENTO</td>';
            $tabla .= '<th >' . $tot_of . '</th><th class="derecha">' . number_format($tot_cost_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_of,2,',','.') . ' %</th><th class="derecha">'.$total_acum_of_fis.'</th>
                 <th class="derecha">' . number_format($tot_pi_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_of,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_of.'</th><th colspan="3"></th></tr>';
        }
        $total_acum=0;$total_acum_fis=0;
        if($total_gral_op!=0){
            $total_acum=number_format($acumulado/$total_gral_op, 2, ',', '.');
            $total_acum_fis=number_format($acumulado_efis/$total_gral_op, 2, ',', '.');
        }
        $tabla.='<tr class="total_general">';
        $tabla.='<th colspan="3" class="izquierda">TOTAL DE LA PROVINCIA</td>';
        $tabla.='<th >'.$total_gral_op.'</th><th class="derecha">'.number_format($total_gral_cost, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_mod, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_ctv, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_epa, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pora, 2, ',', '.').' %</th><th class="derecha" >'.$total_acum_fis.'</th>
                 <th class="derecha">'.number_format($total_gral_pi, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pm, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pv, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pe, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_porc, 2, ',', '.').' %</th><th ></th><th ></th><th class="derecha">'.$total_acum.'</th><th colspan="3"></th></tr>';
        if($tab_aux!=''){
            $total_acum_multi=0;$total_acum_multi_fis=0;
            if($total_gral_op_m!=0){
                $total_acum_multi=number_format($acumulado_multi/$total_gral_op_m, 2, ',', '.');
                $total_acum_multi_fis=number_format($acumulado_multi_efis/$total_gral_op_m, 2, ',', '.');
            }
            $tabla.='<tr><br>
				<td colspan="17"><strong><center><h2>OPERACIONES MULTIPROVINCIA</h2></center></strong></td></tr>'.$tab_aux;
            $tabla.='<tr class="total_multi"> <th colspan="3" class="izquierda">TOTAL MULTIPROVINCIAL</th>';
            $tabla.='<th >'.$total_gral_op_m.'</th><th class="derecha">'.number_format($total_gral_cost_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_mod_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_ctv_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_epa_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pora_m, 2, ',', '.').' %</th><th class="derecha">'.$total_acum_multi_fis.'</th>
                 <th class="derecha">'.number_format($total_gral_pi_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pm_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pv_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pe_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_porc_m, 2, ',', '.').' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_multi.'</th><th colspan="3"></th></tr>';

            $tabla.='<tr class="total_general">';
            $tabla.='<th colspan="3" class="izquierda">TOTAL GENERAL</th>';
            $gran_total=$total_gral_op+$total_gral_op_m;
            $gran_total_por_acum=0;
            if(($total_gral_ctv+$total_gral_ctv_m)!=0) {$gran_total_por_acum=($total_gral_epa+$total_gral_epa_m)/($total_gral_ctv+$total_gral_ctv_m)*100;}
            $gran_total_porc=0;
            if(($total_gral_pv+$total_gral_pv_m)!=0) {$gran_total_porc=($total_gral_pe+$total_gral_pe_m)/($total_gral_pv+$total_gral_pv_m)*100;}
            if($gran_total!=0){
                $gran_total_media=number_format(($acumulado_multi+$acumulado)/$gran_total, 2, ',', '.');
                $gran_total_media_fis=number_format(($acumulado_multi_efis+$acumulado_efis)/$gran_total, 2, ',', '.');
            }
            $tabla.='<th >'.$gran_total.'</th><th class="derecha">'.number_format($total_gral_cost+$total_gral_cost_m, 2, ',', '.').'</th>
					<th class="derecha">'.number_format($total_gral_mod+$total_gral_mod_m, 2, ',', '.').'</th>
					<th class="derecha">'.number_format($total_gral_ctv+$total_gral_ctv_m, 2, ',', '.').'</th>
					<th class="derecha">'.number_format($total_gral_epa+$total_gral_epa_m, 2, ',', '.').'</th>
					<th class="derecha">'.number_format($gran_total_por_acum, 2, ',', '.').' %</th>
					<th class="derecha">'.$gran_total_media_fis.'</th>
                    <th class="derecha">'.number_format($total_gral_pi+$total_gral_pi_m, 2, ',', '.').'</th>
                    <th class="derecha">'.number_format($total_gral_pm+$total_gral_pm_m, 2, ',', '.').'</th>
                    <th class="derecha">'.number_format($total_gral_pv+$total_gral_pv_m, 2, ',', '.').'</th>
                    <th class="derecha">'.number_format($total_gral_pe+$total_gral_pe_m, 2, ',', '.').'</th>
                    <th class="derecha">'.number_format($gran_total_porc, 2, ',', '.').' %</th>
                    <th ></th><th ></th><th class="derecha">'.$gran_total_media.'</th><th colspan="3"></th></tr>';
        }
        $tabla.='</table><br>';
        return $tabla;
    }

    /*===================================== Ejecución de acciones operativas por municipio ===========================================*/
    public function acciones_por_municipio()
    {
        $enlaces=$this->menu_modelo->get_Modulos(10);
        $data['enlaces'] = $enlaces;

        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['municipios'] = $this->model_taqpacha->get_municipios();
        $data['meses'] = $this->model_ejecucion->list_meses(); /// Lista de Meses
        $this->load->view('reportes/reportes_mae/select_op_municipio', $data);
    }
    public function valida_acciones_por_muni()
    {

        if($this->input->server('REQUEST_METHOD') === 'POST')
        {

            $sql_in='';
            if($this->input->post('p1')=='on'){$p1=1;$tp_1=1;}else{$p1=0;$tp_1=0;}
            if($this->input->post('p2')=='on'){$p2=1;$tp_2=2;}else{$p2=0;$tp_2=0;}
            if($this->input->post('p3')=='on'){$p3=1;$tp_3=3;}else{$p3=0;$tp_3=0;}
            if($this->input->post('p4')=='on'){$p4=1;$tp_4=4;}else{$p4=0;$tp_4=0;}
            $sql_in='in ('.$tp_1.','.$tp_2.','.$tp_3.','.$tp_4.')';

            $enlaces = $this->menu_modelo->get_Modulos(10);
            $data['enlaces'] = $enlaces;
            for ($i = 0; $i < count($enlaces); $i++) {
                $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;

            $meses=$this->get_mes($this->input->post('meses_id'));
            $mes = $meses[1];
            $dias = $meses[2];
            $mes_id=$this->input->post('meses_id');
            $data['id_mes'] = $mes_id;
            $data['mes'] = $mes;
            $data['dias'] = $dias;



            $pr1='';$pr2='';$pr3='';$pr4='';
            if($p1==1){$pr1='<span class="pi_t">PROYECTOS DE INVERSI&Oacute;N</span>';}
            if($p2==1){$pr2=' - <span class="pr_t">PROGRAMAS RECURRENTES</span>';}
            if($p3==1){$pr3=' - <span class="pnr_t">PROGRAMAS NO RECURRENTES</span>';}
            if($p4==1){$pr4=' - <span class="of_t">OPERACI&Oacute;N DE FUNCIONAMIENTO</span>';}

            $data['pr1']=$pr1;
            $data['pr2']=$pr2;
            $data['pr3']=$pr3;
            $data['pr4']=$pr4;

            $data['p1']=$p1;
            $data['p2']=$p2;
            $data['p3']=$p3;
            $data['p4']=$p4;

            $data['mun_id']=$this->input->post('prov_id');

            //$v=$this->datos_tp($p1,$p2,$p3,$p4); /// validando para la consulta sql

            if($this->input->post('prov_id')==-1){// mostrar reporetes
                $data['proyectos']=$this->list_ejecucion_financiera_municipios_todos($p1,$p2,$p3,$p4,$sql_in,1,$mes_id);
                $data['title']=' Todos los municipios';
                $data['municipio']='';
                $this->load->view('reportes/reportes_mae/op_municipios', $data);
            }
            else{
                $data['proyectos']=$this->list_ejecucion_financiera_x_municipio($p1,$p2,$p3,$p4,$sql_in,$this->input->post('prov_id'),1,$mes_id);
                $data['title']=' Por Municipio';
                $municipio = $this->model_taqpacha->get_municipio($this->input->post('prov_id'));
                $data['municipio']='<br>MUNICIPIO - '.$municipio[0]['muni_municipio'];
                $this->load->view('reportes/reportes_mae/op_municipios', $data);
            }
        }
        else{
            $this->session->set_flashdata('danger','Problemas al realizar la consulta, contactese con el Web Master');
            redirect('reportes_mae/municipio');
        }
    }
    public function reporte_acciones_por_municipio($p1,$p2,$p3,$p4,$mun_id,$mes_id)
    {
        $mess=$this->get_mes($mes_id);
        $mes = $mess[1];
        $dias = date('d');//$mess[2];
        $sql_in='';
        if($p1==1){$tp_1=1;}else{$tp_1=0;}
        if($p2==1){$tp_2=2;}else{$tp_2=0;}
        if($p3==1){$tp_3=3;}else{$tp_3=0;}
        if($p4==1){$tp_4=4;}else{$tp_4=0;}
        $sql_in='in ('.$tp_1.','.$tp_2.','.$tp_3.','.$tp_4.')';
        if($mun_id==-1){
            $detalles=$this->list_ejecucion_financiera_municipios_todos($p1,$p2,$p3,$p4,$sql_in,2,$mes_id);
            $prov_nombre='(TODOS)';
        }
        else{
            $detalles=$this->list_ejecucion_financiera_x_municipio($p1,$p2,$p3,$p4,$sql_in,$mun_id,2,$mes_id);
            $provincia = $this->model_taqpacha->get_municipio($mun_id);
            $prov_nombre=strtoupper($provincia[0]['muni_municipio']);
        }

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_resumen.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> EJECUCI&Oacute;N DE ACCIONES OPERATIVAS POR MUNICIPIOS / '.$prov_nombre.'<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$detalles.'
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("ejecucion_acciones_operativas_municipal.pdf", array("Attachment" => false));
    }
    function list_ejecucion_financiera_municipios_todos($p1,$p2,$p3,$p4,$sql_in,$tp_rep,$mes_id){
        if($tp_rep==1){
            $tb='id="dt_basic_float" class="table table-bordered"';
        }
        elseif ($tp_rep==2) {
            $tb='border="1" cellpadding="0" cellspacing="0" class="table table table-bordered" width="100%"';
        }

        $nro=0;
        $tabla ='';
        if($tp_rep==1) {
            $tabla .= '<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h3 class="alert alert-success"><center>EJECUCI&Oacute;N DE ACCIONES OPERATÍVAS POR MUNICIPIO </center></h3>
                  </article>
                  <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                        <header>
                          <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                          <h2 class="font-md"><strong>TODOS LOS MUNICIPIOS</strong></h2>  
                        </header>
                	<div>
                  <div class="widget-body no-padding">
                  <div class="table-responsive">';
        }

        $municipios = $this->model_taqpacha->get_municipios_sinMulti();
        if(count($municipios)!=0){
            $nro++;
            $tabla .='<table '.$tb.'>';
            $tabla .='<thead>';
            $tabla .='<tr><th class="titulo_cabecera" style="width:1%;" rowspan="2">#</th>
						  <th class="titulo_cabecera" style="width:15%;" rowspan="2">MUNICIPIO</th>';

            if($p1==1)
                $tabla .='<th class="titulo_cabecera" style="width:15%;" rowspan="2">NRO DE PROYECTOS</th>';
            if($p2==1)
                $tabla.='<th class="titulo_cabecera" style="width:20%;" rowspan="2">NRO DE PROGRAMAS RECURRENTES</th>';
            if($p3==1)
                $tabla.='<th class="titulo_cabecera" style="width:10%;" rowspan="2">NRO DE PROGRAMAS NO RECURRENTES</th>';
            if($p4==1)
                $tabla.='<th class="titulo_cabecera" rowspan="2">NRO. OP. DE FUNCIONAMIENTO </th>';
            $tabla .='<th colspan="5" class="cabecera">PRESUPUESTO GESTI&Oacute;N '.$this->gestion.'</th></tr>';
            $tabla .='<tr class="modo1">';
            $tabla .='<th class="cabecera">INICIAL </th>';
            $tabla .='<th class="cabecera">MODIFICACIONES </th>';
            $tabla .='<th class="cabecera">VIGENTE </th>';
            $tabla .='<th class="cabecera">EJECUTADO </th>';
            $tabla .='<th class="cabecera">% DE EJECUCIÓN PRESUPUESTARIA </th>';
            $tabla .='</tr>';
            $tabla .='</thead>';
            $nrop=0;
            $tabla .='<tbody>';
            $total_pi=0;$total_pr=0;$total_pnr=0;$total_of=0;
            $total_mm_pi=0;$total_mm_pr=0;$total_mm_pnr=0;$total_mm_of=0;
            $total_pemm_pini=0;$total_pemm_pmod=0;$total_pemm_pvig=0;$total_pemm_peje=0;
            $total_gral_pe_pini=0;$total_gral_pe_pmod=0;$total_gral_pe_pvig=0;$total_gral_pe_peje=0;
            $proy_mult[]=array();
            foreach($municipios as $row_muni)
            {
                $nrop++;
                $tabla .='<tr class="derecha">';
                $tabla .='<td class="centro">'.$nrop.'</td>';
                $tabla .='<td class="izquierda">'.$row_muni['muni_municipio'].'</td>';
                $proyectos=$this->model_taqpacha->proyectos_x_municipio($row_muni['muni_id'],$this->gestion,$mes_id,$sql_in);
                if(count($proyectos)!=0){
                    $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                    $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
                    foreach($proyectos as $rowp){
                        $muni_proy=$this->model_taqpacha->municipios_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                        if($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']>1){//Multimunicipal
                            if(count($proy_mult)<=0)
                                $proy_mult[]=$rowp['proy_id'];

                            $encontrado=0;
                            for($i=0;$i<count($proy_mult);$i++){
                                if($proy_mult[$i]==$rowp['proy_id']){
                                    $encontrado=1;
                                    break;
                                }
                            }
                            if ($encontrado==0){
                                $proy_mult[]=$rowp['proy_id'];
                                $total_pemm_pini=$total_pemm_pini+$rowp['pe_pi'];
                                $total_pemm_pmod=$total_pemm_pmod+$rowp['pe_pm'];
                                $total_pemm_pvig=$total_pemm_pvig+$rowp['pe_pv'];
                                $total_pemm_peje=$total_pemm_peje+$rowp['pe_pe'];
                                if($rowp['tp_id']==1){//Proyecto de Inversión
                                    $total_mm_pi=$total_mm_pi+1;
                                }elseif($rowp['tp_id']==2){//Programa Recurrente
                                    $total_mm_pr=$total_mm_pr+1;
                                }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                                    $total_mm_pnr=$total_mm_pnr+1;
                                }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                                    $total_mm_of=$total_mm_of+1;
                                }
                            }

                        }else{ //Unico municipio
                            $total_pe_pini=$total_pe_pini+$rowp['pe_pi'];
                            $total_pe_pmod=$total_pe_pmod+$rowp['pe_pm'];
                            $total_pe_pvig=$total_pe_pvig+$rowp['pe_pv'];
                            $total_pe_peje=$total_pe_peje+$rowp['pe_pe'];
                            if($rowp['pe_pv']!=0){
                                $total_pe_porc=$rowp['pe_pe']/$rowp['pe_pv']*100;
                            }
                            if($rowp['tp_id']==1){//Proyecto de Inversión
                                $tot_pi=$tot_pi+1;
                            }elseif($rowp['tp_id']==2){//Programa Recurrente
                                $tot_pr=$tot_pr+1;
                            }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                                $tot_pnr=$tot_pnr+1;
                            }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                                $tot_of=$tot_of+1;
                            }
                        }
                    }
                }else{
                    $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                    $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
                }

                $total_pe_porc=0;
                if($total_pe_pvig!=0){
                    $total_pe_porc=$total_pe_peje/$total_pe_pvig*100;
                }
                if($p1==1)
                    $tabla .='<td class="pi">'.$tot_pi.'</td>';
                if($p2==1)
                    $tabla .='<td class="pr">'.$tot_pr.'</td>';
                if($p3==1)
                    $tabla .='<td class="pnr">'.$tot_pnr.'</td>';
                if($p4==1)
                    $tabla .='<td class="of">'.$tot_of.'</td>';

                $tabla .='<td>'.number_format($total_pe_pini, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_pmod, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_pvig, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_peje, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_porc, 2, ',', '.').'%</td>';
                $tabla .='</tr>';
                $total_pi =$total_pi+$tot_pi;
                $total_pr =$total_pr+$tot_pr;
                $total_pnr=$total_pnr+$tot_pnr;
                $total_of =$total_of+$tot_of;
                $total_gral_pe_pini=$total_gral_pe_pini+$total_pe_pini;
                $total_gral_pe_pmod=$total_gral_pe_pmod+$total_pe_pmod;
                $total_gral_pe_pvig=$total_gral_pe_pvig+$total_pe_pvig;
                $total_gral_pe_peje=$total_gral_pe_peje+$total_pe_peje;

            }
            /*Linea multimunicipal*/
            $proyectos=$this->model_taqpacha->proyectos_x_municipio(90504,$this->gestion,$mes_id,$sql_in);
            if(count($proyectos)>=1){
                $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                foreach($proyectos as $rowp){
                    $muni_proy=$this->model_taqpacha->municipios_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                    if($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']==1){//Multimunicipal
                        $total_pemm_pini=$total_pemm_pini+$rowp['pe_pi'];
                        $total_pemm_pmod=$total_pemm_pmod+$rowp['pe_pm'];
                        $total_pemm_pvig=$total_pemm_pvig+$rowp['pe_pv'];
                        $total_pemm_peje=$total_pemm_peje+$rowp['pe_pe'];
                        if($rowp['tp_id']==1){//Proyecto de Inversión
                            $total_mm_pi=$total_mm_pi+1;
                        }elseif($rowp['tp_id']==2){//Programa Recurrente
                            $total_mm_pr=$total_mm_pr+1;
                        }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                            $total_mm_pnr=$total_mm_pnr+1;
                        }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                            $total_mm_of=$total_mm_of+1;
                        }
                    }
                }
            }
            $nrop++;
            $total_pemm_porc=0;
            if($total_pemm_pvig!=0){
                $total_pemm_porc=$total_pemm_peje/$total_pemm_pvig*100;
            }

            $tabla .='<tr class="multiple">';
            $tabla .='<td>'.$nrop.'</td>';
            $tabla .='<td class="izquierda">Multimunicipal</td>';
            if($p1==1)
                $tabla .='<td class="centro">'.$total_mm_pi.'</td>';
            if($p2==1)
                $tabla .='<td class="centro">'.$total_mm_pr.'</td>';
            if($p3==1)
                $tabla .='<td class="centro">'.$total_mm_pnr.'</td>';
            if($p4==1)
                $tabla .='<td class="centro">'.$total_mm_of.'</td>';

            $tabla .='<td>'.number_format($total_pemm_pini, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_pemm_pmod, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_pemm_pvig, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_pemm_peje, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_pemm_porc, 2, ',', '.').'%</td>';
            $tabla .='</tr>';

            $total_pi =$total_pi+$tot_pi+$total_mm_pi;
            $total_pr =$total_pr+$tot_pr+$total_mm_pr;
            $total_pnr=$total_pnr+$tot_pnr+$total_mm_pnr;
            $total_of =$total_of+$tot_of+$total_mm_of;


            $total_gral_pe_pini=$total_gral_pe_pini+$total_pemm_pini;
            $total_gral_pe_pmod=$total_gral_pe_pmod+$total_pemm_pmod;
            $total_gral_pe_pvig=$total_gral_pe_pvig+$total_pemm_pvig;
            $total_gral_pe_peje=$total_gral_pe_peje+$total_pemm_peje;
            $total_gral_pe_porc=0;
            if($total_gral_pe_pvig!=0){$total_gral_pe_porc=$total_gral_pe_peje/$total_gral_pe_pvig*100;}

            $tabla .='</tbody>';
            $tabla .='<tr class="total_general">';
            $tabla .='<td colspan="2" class="izquierda"><b>TOTAL : '.$nrop.'</b></td>';
            if($p1==1)
                $tabla .='<td class="centro"><b>'.$total_pi.'</td>';
            if($p2==1)
                $tabla .='<td class="centro">'.$total_pr.'</td>';
            if($p3==1)
                $tabla .='<td class="centro">'.$total_pnr.'</td>';
            if($p4==1)
                $tabla .='<td class="centro">'.$total_of.'</td>';

            $tabla .='<td>'.number_format($total_gral_pe_pini, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pmod, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pvig, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_peje, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_porc, 2, ',', '.').'%</td>';
            $tabla .='</tr>';
            $tabla .='</table>';
            if($tp_rep==1) {
                $tabla .= '</div>
                </div>
              </div>
            </article>';
            }
        }

        return $tabla;
    }
    function list_ejecucion_financiera_x_municipio($p1,$p2,$p3,$p4,$sql_in,$mun_id,$tp,$mes_id)
    {
        if($tp==1){
            $tb='id="dt_basic_float" class="table table-bordered" width="100%"';
        }
        elseif ($tp==2) {
            $tb='border="1" cellpadding="0" cellspacing="0" class="table table table-bordered" width="100%"';
        }
        $sum=0;
        $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
        $acum_pi=0;$acum_pr=0;$acum_pnr=0;$acum_of=0;
        $acum_pi_fis=0;$acum_pr_fis=0;$acum_pnr_fis=0;$acum_of_fis=0;
        $tot_cost_pi=0;$tot_cost_pr=0;$tot_cost_pnr=0;$tot_cost_of=0;
        $tot_mod_pi=0;$tot_mod_pr=0;$tot_mod_pnr=0;$tot_mod_of=0;
        $tot_ctv_pi=0;$tot_ctv_pr=0;$tot_ctv_pnr=0;$tot_ctv_of=0;
        $tot_epa_pi=0;$tot_epa_pr=0;$tot_epa_pnr=0;$tot_epa_of=0;
        $tot_pi_pi=0;$tot_pi_pr=0;$tot_pi_pnr=0;$tot_pi_of=0;
        $tot_pm_pi=0;$tot_pm_pr=0;$tot_pm_pnr=0;$tot_pm_of=0;
        $tot_pv_pi=0;$tot_pv_pr=0;$tot_pv_pnr=0;$tot_pv_of=0;
        $tot_pe_pi=0;$tot_pe_pr=0;$tot_pe_pnr=0;$tot_pe_of=0;

        $tot_pi_m=0;$tot_pr_m=0;$tot_pnr_m=0;$tot_of_m=0;
        $tot_cost_pi_m=0;$tot_cost_pr_m=0;$tot_cost_pnr_m=0;$tot_cost_of_m=0;
        $tot_mod_pi_m=0;$tot_mod_pr_m=0;$tot_mod_pnr_m=0;$tot_mod_of_m=0;
        $tot_ctv_pi_m=0;$tot_ctv_pr_m=0;$tot_ctv_pnr_m=0;$tot_ctv_of_m=0;
        $tot_epa_pi_m=0;$tot_epa_pr_m=0;$tot_epa_pnr_m=0;$tot_epa_of_m=0;
        $tot_pi_pi_m=0;$tot_pi_pr_m=0;$tot_pi_pnr_m=0;$tot_pi_of_m=0;
        $tot_pm_pi_m=0;$tot_pm_pr_m=0;$tot_pm_pnr_m=0;$tot_pm_of_m=0;
        $tot_pv_pi_m=0;$tot_pv_pr_m=0;$tot_pv_pnr_m=0;$tot_pv_of_m=0;
        $tot_pe_pi_m=0;$tot_pe_pr_m=0;$tot_pe_pnr_m=0;$tot_pe_of_m=0;

        $acumulado=0;$acumulado_multi=0;
        $acumulado_efis=0;$acumulado_multi_efis=0;
        $tabla ='';
        $tab_aux='';
        $tabla.='<table '.$tb.'>';
        $tabla.='<thead>';
        $tabla.='<tr>
				<th class="titulo_cabecera" style="width:1%;" rowspan="2">Nro.</th>
				<th class="titulo_cabecera" style="width:8%;" rowspan="2">NOMBRE</th>
				<th class="titulo_cabecera" style="width:5%;" rowspan="2">PROVINCIA <br>Y<br> MUNICIPIO(S)</th>
				<th class="titulo_cabecera" style="width:4%;" rowspan="2">TIPO</th>
				<th class="cabecera" colspan="3">COSTO TOTAL DE LA OPERACI&Oacute;N</th>
				<th class="cabecera_acum" colspan="3">EJECUCIÓN ACUMULADA</th>
	    		<th class="cabecera" colspan="5">PRESUPUESTO GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>
	    		<th class="cabecera_acum" colspan="3">EJECUCI&Oacute;N FÍSICA '.$this->session->userdata('gestion').'</th>
	    		<th class="titulo_cabecera" style="width:5%;" rowspan="2">ESTADO</th>
	    		<th class="titulo_cabecera" style="width:6%;" rowspan="2">UNIDAD EJECUTORA</th>
	    		<th class="titulo_cabecera" style="width:6%;" rowspan="2">RESPONSABLE</th>
			</tr>';
        $tabla.='<tr class="modo1">
				<th class="cabecera" style="width:6%;">INICIAL</th>
				<th class="cabecera" style="width:5%;">MODIFICACIONES</th>
				<th class="cabecera" style="width:6%;">VIGENTE</th>
				<th class="cabecera_acum" style="width:5%;">EJECUCIÓN FINANCIERA</th>
				<th class="cabecera_acum" style="width:5%;">% EJEC. FINANCIERA</th>
				<th class="cabecera_acum" style="width:5%;">% DE AVANCE FÍSICO</th>';
        $tabla.='<th class="cabecera" style="width:5%;"> INICIAL </th>';
        $tabla.='<th class="cabecera" style="width:5%;"> MODIFICADO </th>';
        $tabla.='<th class="cabecera" style="width:5%;"> VIGENTE </th>';
        $tabla.='<th class="cabecera" style="width:5%;">EJEC. FINANCIERA </th>';
        $tabla.='<th class="cabecera" style="width:5%;">% EJEC. FINANCIERA </th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">PROG. %</th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">EJEC %</th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">% EJEC. ANUAL</th>';
        $tabla.='</tr>';
        $tabla.='</thead>';
        $tabla.='<tbody>';

        $programas = $this->model_epresupuestaria->ejecucion_proyecto_programas_x_municipio($sql_in,$this->gestion,$mes_id,$mun_id);
        if(count($programas)!=0){
            $nro=0;
            $nro_mult=0;
            foreach($programas as $rowp)
            {
                if($mun_id!=90504) {
                    $muni_proy = $this->model_taqpacha->municipios_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                    $color_mult = '';
                    if ($muni_proy[0]['total_muni'] != '' && $muni_proy[0]['total_muni'] > 1) {//Multimunicipal
                        $color_mult = 'style="background-color:#EDFBFF"';
                    }
                    $se = $rowp['pe_pe'];
                    $suma = $this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                    if ($suma[0]['total_ejecutado'] != '' || $suma[0]['total_ejecutado'] != 0) {
                        $se = $se + $suma[0]['total_ejecutado'];
                    }

                    $suma_prog = $this->mdas_complementario->costo_total_proyecto($rowp['pfec_id']);
                    $toal_modif = $this->mdas_complementario->modificaciones_costo_total($rowp['pfec_id']);
                    $estado = $this->mdas_complementario->estado_operacion($rowp['pfec_id'],$mes_id,$this->gestion);
                    $efis = $this->avance_fisico($rowp['proy_id'],$mes_id);
                    if($color_mult=='') {
                        $tabla .= '<tr class="modo1 derecha">';
                        $nro = $nro + 1;
                        $tabla .= '<td class="centro">' . $nro . '</td>';
                        $tabla .= '<td class="izquierda"><a href="javascript:abreVentana(\'' . site_url("reportes_mae") . '/resumen_ejec/' . $rowp['proy_id'] . '/' . $mes_id . '\');"' . '>' . $rowp['proy_nombre'] . '</a></td>';
                        $tabla .= '<td class="izquierda">';
                        $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);
                        if((count($prov)>3) and ($tp == 1)){
                            $tabla .= '<p class="btn btn-default" href="#" class="form_pond" title="';
                        }elseif($tp ==2){
                            $tabla .= '<div style="font-size: 5px;">';
                        }
                        foreach($prov as $row_prov){
                            $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                            if (count($prov) > 3) {
                                if ($tp == 1) {
                                    foreach ($muni as $muni) {
                                        $tabla .= $row_prov['prov_provincia'] .'&#09;'. $muni['muni_municipio'] . '&#13;';
                                    }
                                } else {
                                    $tabla .='<ul>'.$row_prov['prov_provincia'];
                                    foreach ($muni as $muni) {
                                        $tabla .= '<li>'.$muni['muni_municipio'] . '</li>';
                                    }
                                    $tabla .= '</ul>';
                                }
                            } else {
                                $tabla .= '<ul><strong>'.$row_prov['prov_provincia'].'</strong>';
                                foreach ($muni as $muni) {
                                    $tabla .= '<li>'.$muni['muni_municipio'].'</li>';
                                }
                                $tabla .= '</ul>';
                            }
                        }
                        if((count($prov) > 3) and ($tp == 1)){
                            $tabla .= '"><i class="glyphicon glyphicon-search"></i>Ver...</p>';
                        }elseif($tp==2){
                            $tabla .= '</div>';
                        }
                        $tabla .= '</td>';
                        $tabla .= '<td class="izquierda">' . $rowp['tp_tipo'] . '</td>';
                        $tabla .= '<td>' . number_format($suma_prog[0]['total_programado'],2,',','.') . '</td>';

                        $total_mod = 0;
                        if ($toal_modif[0]['total_modif'] != '' || $toal_modif[0]['total_modif'] != 0) {
                            $total_mod = $toal_modif[0]['total_modif'];
                        }
                        $tabla .= '<td>' . number_format($total_mod,2,',','.') . '</td>';
                        $ctv = $suma_prog[0]['total_programado'] + $total_mod;
                        $tabla .= '<td>' . number_format($ctv,2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($se,2,',','.') . '</td>';

                        $efin = 0;
                        if ($suma_prog[0]['total_programado'] != 0) {
                            $efin = round((($se / $suma_prog[0]['total_programado']) * 100),2);
                        }
                        $acumulado_efis=$acumulado_efis+$efis;
                        $tabla .= '<td>' . $efin . '%</td>';
                        $tabla .= '<td>' . round($efis,2) . '%</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pi'],2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pm'],2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pv'],2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pe'],2,',','.') . '</td>';

                        $ejec = 0;
                        if ($rowp['pe_pv'] != 0) {
                            $ejec = round((($rowp['pe_pe'] / $rowp['pe_pv']) * 100),2);
                        }
                        $tabla .= '<td>' . $ejec . '%</td>';
                        $estado_proy = 'No registrado';
                        if (count($estado) != 0) {
                            $estado_proy = $estado[0]['ep_descripcion'];
                        }
                        $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));

                        $tabla .= '<td class="derecha">'.$ejecutado[0].'</td>';
                        $tabla .= '<td class="derecha">'.$ejecutado[1].'</td>';
                        $tabla .= '<td class="derecha">'.$ejecutado[2].'</td>';
                        $acumulado=$acumulado+$ejecutado[2];
                        $tabla .= '<td class="izquierda">' . $estado_proy . '</td>';
                        $tabla .= '<td class="izquierda">' . $rowp['uni_unidad'] . '</td>';
                        $tabla .= '<td class="izquierda">' . $rowp['responsable'] . '<br><font color="red">Telf: ' . $rowp['fun_telefono'] . '</font></td>';
                        $tabla .= '</tr>';
                        if ($rowp['tp_id'] == 1) {//Proyecto de Inversión
                            $tot_pi = $tot_pi + 1;
                            $acum_pi=$acum_pi+$ejecutado[2];
                            $acum_pi_fis=$acum_pi_fis+$efis;
                            $tot_cost_pi = $tot_cost_pi + $suma_prog[0]['total_programado'];
                            $tot_mod_pi = $tot_mod_pi + $total_mod;
                            $tot_ctv_pi = $tot_ctv_pi + $ctv;
                            $tot_epa_pi = $tot_epa_pi + $se;
                            $tot_pi_pi = $tot_pi_pi + $rowp['pe_pi'];
                            $tot_pm_pi = $tot_pm_pi + $rowp['pe_pm'];
                            $tot_pv_pi = $tot_pv_pi + $rowp['pe_pv'];
                            $tot_pe_pi = $tot_pe_pi + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 2) {//Programa Recurrente
                            $tot_pr = $tot_pr + 1;
                            $acum_pr=$acum_pr+$ejecutado[2];
                            $acum_pr_fis=$acum_pr_fis+$efis;
                            $tot_cost_pr = $tot_cost_pr + $suma_prog[0]['total_programado'];
                            $tot_mod_pr = $tot_mod_pr + $total_mod;
                            $tot_ctv_pr = $tot_ctv_pr + $ctv;
                            $tot_epa_pr = $tot_epa_pr + $se;
                            $tot_pi_pr = $tot_pi_pr + $rowp['pe_pi'];
                            $tot_pm_pr = $tot_pm_pr + $rowp['pe_pm'];
                            $tot_pv_pr = $tot_pv_pr + $rowp['pe_pv'];
                            $tot_pe_pr = $tot_pe_pr + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 3) {//Programa No  Recurrente
                            $tot_pnr = $tot_pnr + 1;
                            $acum_pnr=$acum_pnr+$ejecutado[2];
                            $acum_pnr_fis=$acum_pnr_fis+$efis;
                            $tot_cost_pnr = $tot_cost_pnr + $suma_prog[0]['total_programado'];
                            $tot_mod_pnr = $tot_mod_pnr + $total_mod;
                            $tot_ctv_pnr = $tot_ctv_pnr + $ctv;
                            $tot_epa_pnr = $tot_epa_pnr + $se;
                            $tot_pi_pnr = $tot_pi_pnr + $rowp['pe_pi'];
                            $tot_pm_pnr = $tot_pm_pnr + $rowp['pe_pm'];
                            $tot_pv_pnr = $tot_pv_pnr + $rowp['pe_pv'];
                            $tot_pe_pnr = $tot_pe_pnr + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 4) {//Operacion de Funcionamiento
                            $tot_of = $tot_of + 1;
                            $acum_of=$acum_of+$ejecutado[2];
                            $acum_of_fis=$acum_of_fis+$efis;
                            $tot_cost_of = $tot_cost_of + $suma_prog[0]['total_programado'];
                            $tot_mod_of = $tot_mod_of + $total_mod;
                            $tot_ctv_of = $tot_ctv_of + $ctv;
                            $tot_epa_of = $tot_epa_of + $se;
                            $tot_pi_of = $tot_pi_of + $rowp['pe_pi'];
                            $tot_pm_of = $tot_pm_of + $rowp['pe_pm'];
                            $tot_pv_of = $tot_pv_of + $rowp['pe_pv'];
                            $tot_pe_of = $tot_pe_of + $rowp['pe_pe'];
                        }
                    }else{
                        /**Multi**/
                        $nro_mult=$nro_mult+1;
                        $tab_aux .='<tr class="modo1 derecha" ' . $color_mult . '>';
                        $tab_aux .= '<td class="centro">' . $nro_mult . '</td>';
                        $tab_aux .= '<td class="izquierda"><a href="javascript:abreVentana(\'' . site_url("reportes_mae") . '/resumen_ejec/' . $rowp['proy_id'] . '/' . $mes_id . '\');"' . '>' . $rowp['proy_nombre'] . '</a></td>';
                        $tab_aux .= '<td class="izquierda">';
                        $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);
                        if((count($prov)>3) and ($tp == 1)){
                            $tab_aux .= '<p class="btn btn-default" href="#" class="form_pond" title="';
                        }elseif($tp ==2){
                            $tab_aux .= '<div style="font-size: 5px;">';
                        }
                        foreach($prov as $row_prov){
                            $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                            if (count($prov) > 3) {
                                if ($tp == 1) {
                                    foreach ($muni as $muni) {
                                        $tab_aux .= $row_prov['prov_provincia'] .'&#09;'. $muni['muni_municipio'] . '&#13;';
                                    }
                                } else {
                                    $tab_aux .='<ul>'.$row_prov['prov_provincia'];
                                    foreach ($muni as $muni) {
                                        $tab_aux .= '<li>'.$muni['muni_municipio'] . '</li>';
                                    }
                                    $tab_aux .= '</ul>';
                                }
                            } else {
                                $tab_aux .= '<ul><strong>'.$row_prov['prov_provincia'].'</strong>';
                                foreach ($muni as $muni) {
                                    $tab_aux .= '<li>'.$muni['muni_municipio'].'</li>';
                                }
                                $tab_aux .= '</ul>';
                            }
                        }
                        if((count($prov) > 3) and ($tp == 1)){
                            $tab_aux .= '"><i class="glyphicon glyphicon-search"></i>Ver...</p>';
                        }elseif($tp==2){
                            $tab_aux .= '</div>';
                        }

                        $tab_aux .= '</td>';
                        $tab_aux .= '<td class="izquierda">' . $rowp['tp_tipo'] . '</td>';
                        $tab_aux .= '<td>' . number_format($suma_prog[0]['total_programado'],2,',','.') . '</td>';

                        $total_mod = 0;
                        if ($toal_modif[0]['total_modif'] != '' || $toal_modif[0]['total_modif'] != 0) {
                            $total_mod = $toal_modif[0]['total_modif'];
                        }
                        $tab_aux .= '<td>' . number_format($total_mod,2,',','.') . '</td>';
                        $ctv = $suma_prog[0]['total_programado'] + $total_mod;
                        $tab_aux .= '<td>' . number_format($ctv,2,',','.') . '</td>';
                        $tab_aux .= '<td>' . number_format($se,2,',','.') . '</td>';

                        $efin = 0;
                        if ($suma_prog[0]['total_programado'] != 0) {
                            $efin = round((($se / $suma_prog[0]['total_programado']) * 100),2);
                        }
                        $acumulado_multi_efis=$acumulado_multi_efis+$efis;
                        $tab_aux .= '<td>' . $efin . '%</td>';
                        $tab_aux .= '<td>' . round($efis,2) . '%</td>';
                        $tab_aux .= '<td>' . number_format($rowp['pe_pi'],2,',','.') . '</td>';
                        $tab_aux .= '<td>' . number_format($rowp['pe_pm'],2,',','.') . '</td>';
                        $tab_aux .= '<td>' . number_format($rowp['pe_pv'],2,',','.') . '</td>';
                        $tab_aux .= '<td>' . number_format($rowp['pe_pe'],2,',','.') . '</td>';

                        $ejec = 0;
                        if ($rowp['pe_pv'] != 0) {
                            $ejec = round((($rowp['pe_pe'] / $rowp['pe_pv']) * 100),2);
                        }
                        $tab_aux .= '<td>' . $ejec . '%</td>';
                        $estado_proy = 'No registrado';
                        if (count($estado) != 0) {
                            $estado_proy = $estado[0]['ep_descripcion'];
                        }
                        $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));

                        $tab_aux .= '<td class="derecha">'.$ejecutado[0].'</td>';
                        $tab_aux .= '<td class="derecha">'.$ejecutado[1].'</td>';
                        $tab_aux .= '<td class="derecha">'.$ejecutado[2].'</td>';
                        $acumulado_multi=$acumulado_multi+$ejecutado[2];
                        $tab_aux .= '<td class="izquierda">' . $estado_proy . '</td>';
                        $tab_aux .= '<td class="izquierda">' . $rowp['uni_unidad'] . '</td>';
                        $tab_aux .= '<td class="izquierda">' . $rowp['responsable'] . '<br><font color="red">Telf: ' . $rowp['fun_telefono'] . '</font></td>';
                        $tab_aux .= '</tr>';
                        if ($rowp['tp_id'] == 1) {//Proyecto de Inversión
                            $tot_pi_m = $tot_pi_m + 1;
                            $tot_cost_pi_m = $tot_cost_pi_m + $suma_prog[0]['total_programado'];
                            $tot_mod_pi_m = $tot_mod_pi_m + $total_mod;
                            $tot_ctv_pi_m = $tot_ctv_pi_m + $ctv;
                            $tot_epa_pi_m = $tot_epa_pi_m + $se;
                            $tot_pi_pi_m = $tot_pi_pi_m + $rowp['pe_pi'];
                            $tot_pm_pi_m = $tot_pm_pi_m + $rowp['pe_pm'];
                            $tot_pv_pi_m = $tot_pv_pi_m + $rowp['pe_pv'];
                            $tot_pe_pi_m = $tot_pe_pi_m + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 2) {//Programa Recurrente
                            $tot_pr_m = $tot_pr_m + 1;
                            $tot_cost_pr_m = $tot_cost_pr_m + $suma_prog[0]['total_programado'];
                            $tot_mod_pr_m = $tot_mod_pr_m + $total_mod;
                            $tot_ctv_pr_m = $tot_ctv_pr_m + $ctv;
                            $tot_epa_pr_m = $tot_epa_pr_m + $se;
                            $tot_pi_pr_m = $tot_pi_pr_m + $rowp['pe_pi'];
                            $tot_pm_pr_m = $tot_pm_pr_m + $rowp['pe_pm'];
                            $tot_pv_pr_m = $tot_pv_pr_m + $rowp['pe_pv'];
                            $tot_pe_pr_m = $tot_pe_pr_m + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 3) {//Programa No  Recurrente
                            $tot_pnr_m = $tot_pnr_m + 1;
                            $tot_cost_pnr_m = $tot_cost_pnr_m + $suma_prog[0]['total_programado'];
                            $tot_mod_pnr_m = $tot_mod_pnr_m + $total_mod;
                            $tot_ctv_pnr_m = $tot_ctv_pnr_m + $ctv;
                            $tot_epa_pnr_m = $tot_epa_pnr_m + $se;
                            $tot_pi_pnr_m = $tot_pi_pnr_m + $rowp['pe_pi'];
                            $tot_pm_pnr_m = $tot_pm_pnr_m + $rowp['pe_pm'];
                            $tot_pv_pnr_m = $tot_pv_pnr_m + $rowp['pe_pv'];
                            $tot_pe_pnr_m = $tot_pe_pnr_m + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 4) {//Operacion de Funcionamiento
                            $tot_of_m = $tot_of_m + 1;
                            $tot_cost_of_m = $tot_cost_of_m + $suma_prog[0]['total_programado'];
                            $tot_mod_of_m = $tot_mod_of_m + $total_mod;
                            $tot_ctv_of_m = $tot_ctv_of_m + $ctv;
                            $tot_epa_of_m = $tot_epa_of_m + $se;
                            $tot_pi_of_m = $tot_pi_of_m + $rowp['pe_pi'];
                            $tot_pm_of_m = $tot_pm_of_m + $rowp['pe_pm'];
                            $tot_pv_of_m = $tot_pv_of_m + $rowp['pe_pv'];
                            $tot_pe_of_m = $tot_pe_of_m + $rowp['pe_pe'];
                        }
                    }
                }else{
                    $muni_proy = $this->model_taqpacha->municipios_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                    $color_mult = '';
                    if(($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']>1) ||($rowp['municipio']==90504)){//Multimunicipal
                        $se=$rowp['pe_pe'];
                        $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                        if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                            $se=$se+$suma[0]['total_ejecutado'];
                        }

                        $suma_prog=$this->mdas_complementario->costo_total_proyecto($rowp['pfec_id']);
                        $toal_modif = $this->mdas_complementario->modificaciones_costo_total($rowp['pfec_id']);
                        $estado = $this->mdas_complementario->estado_operacion($rowp['pfec_id'],$mes_id,$this->gestion);
                        $efis = $this->avance_fisico($rowp['proy_id'],$mes_id);
                        $tabla .= '<tr class="modo1 derecha">';
                        $nro = $nro + 1;
                        $tabla .= '<td class="centro">' . $nro . '</td>';
                        $tabla .= '<td class="izquierda"><a href="javascript:abreVentana(\'' . site_url("reportes_mae") . '/resumen_ejec/' . $rowp['proy_id'] . '/' . $mes_id . '\');"' . '>' . $rowp['proy_nombre'] . '</a></td>';
                        $tabla .= '<td class="izquierda">';
                        $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);
                        if((count($prov)>3) and ($tp == 1)){
                            $tabla .= '<p class="btn btn-default" href="#" class="form_pond" title="';
                        }elseif($tp ==2){
                            $tabla .= '<div style="font-size: 5px;">';
                        }
                        foreach($prov as $row_prov){
                            $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                            if (count($prov) > 3) {
                                if ($tp == 1) {
                                    foreach ($muni as $muni) {
                                        $tabla .= $row_prov['prov_provincia'] .'&#09;'. $muni['muni_municipio'] . '&#13;';
                                    }
                                } else {
                                    $tabla .='<ul>'.$row_prov['prov_provincia'];
                                    foreach ($muni as $muni) {
                                        $tabla .= '<li>'.$muni['muni_municipio'] . '</li>';
                                    }
                                    $tabla .= '</ul>';
                                }
                            } else {
                                $tabla .= '<ul><strong>'.$row_prov['prov_provincia'].'</strong>';
                                foreach ($muni as $muni) {
                                    $tabla .= '<li>'.$muni['muni_municipio'].'</li>';
                                }
                                $tabla .= '</ul>';
                            }
                        }
                        if((count($prov) > 3) and ($tp == 1)){
                            $tabla .= '"><i class="glyphicon glyphicon-search"></i>Ver...</p>';
                        }elseif($tp==2){
                            $tabla .= '</div>';
                        }
                        $tabla .= '</td>';
                        $tabla .= '<td class="izquierda">' . $rowp['tp_tipo'] . '</td>';
                        $tabla .= '<td>'.number_format($suma_prog[0]['total_programado'],2,',','.') . '</td>';

                        $total_mod = 0;
                        if ($toal_modif[0]['total_modif'] != '' || $toal_modif[0]['total_modif'] != 0) {
                            $total_mod = $toal_modif[0]['total_modif'];
                        }
                        $tabla .= '<td>' . number_format($total_mod,2,',','.') . '</td>';
                        $ctv = $suma_prog[0]['total_programado'] + $total_mod;
                        $tabla .= '<td>' . number_format($ctv,2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($se,2,',','.') . '</td>';

                        $efin = 0;
                        if ($suma_prog[0]['total_programado'] != 0) {
                            $efin = round((($se / $suma_prog[0]['total_programado']) * 100),2);
                        }
                        $tabla .= '<td>' . $efin . '%</td>';
                        $tabla .= '<td>' . round($efis,2) . '%</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pi'],2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pm'],2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pv'],2,',','.') . '</td>';
                        $tabla .= '<td>' . number_format($rowp['pe_pe'],2,',','.') . '</td>';

                        $ejec = 0;
                        if ($rowp['pe_pv'] != 0) {
                            $ejec = round((($rowp['pe_pe'] / $rowp['pe_pv']) * 100),2);
                        }
                        $tabla .= '<td>' . $ejec . '%</td>';
                        $estado_proy = 'No registrado';
                        if (count($estado) != 0) {
                            $estado_proy = $estado[0]['ep_descripcion'];
                        }
                        $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));
                        $tabla .= '<td class="izquierda">'.$ejecutado[0].'</td>';
                        $tabla .= '<td class="izquierda">'.$ejecutado[1].'</td>';
                        $tabla .= '<td class="izquierda">'.$ejecutado[2].'</td>';
                        $tabla .= '<td class="izquierda">' . $estado_proy . '</td>';
                        $tabla .= '<td class="izquierda">' . $rowp['uni_unidad'] . '</td>';
                        $tabla .= '<td class="izquierda">' . $rowp['responsable'] . '<br><font color="red">Telf: ' . $rowp['fun_telefono'] . '</font></td>';
                        $tabla .= '</tr>';

                        if ($rowp['tp_id'] == 1) {//Proyecto de Inversión
                            $tot_pi = $tot_pi + 1;
                            $tot_cost_pi = $tot_cost_pi + $suma_prog[0]['total_programado'];
                            $tot_mod_pi = $tot_mod_pi + $total_mod;
                            $tot_ctv_pi = $tot_ctv_pi + $ctv;
                            $tot_epa_pi = $tot_epa_pi + $se;
                            $tot_pi_pi = $tot_pi_pi + $rowp['pe_pi'];
                            $tot_pm_pi = $tot_pm_pi + $rowp['pe_pm'];
                            $tot_pv_pi = $tot_pv_pi + $rowp['pe_pv'];
                            $tot_pe_pi = $tot_pe_pi + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 2) {//Programa Recurrente
                            $tot_pr = $tot_pr + 1;
                            $tot_cost_pr = $tot_cost_pr + $suma_prog[0]['total_programado'];
                            $tot_mod_pr = $tot_mod_pr + $total_mod;
                            $tot_ctv_pr = $tot_ctv_pr + $ctv;
                            $tot_epa_pr = $tot_epa_pr + $se;
                            $tot_pi_pr = $tot_pi_pr + $rowp['pe_pi'];
                            $tot_pm_pr = $tot_pm_pr + $rowp['pe_pm'];
                            $tot_pv_pr = $tot_pv_pr + $rowp['pe_pv'];
                            $tot_pe_pr = $tot_pe_pr + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 3) {//Programa No  Recurrente
                            $tot_pnr = $tot_pnr + 1;
                            $tot_cost_pnr = $tot_cost_pnr + $suma_prog[0]['total_programado'];
                            $tot_mod_pnr = $tot_mod_pnr + $total_mod;
                            $tot_ctv_pnr = $tot_ctv_pnr + $ctv;
                            $tot_epa_pnr = $tot_epa_pnr + $se;
                            $tot_pi_pnr = $tot_pi_pnr + $rowp['pe_pi'];
                            $tot_pm_pnr = $tot_pm_pnr + $rowp['pe_pm'];
                            $tot_pv_pnr = $tot_pv_pnr + $rowp['pe_pv'];
                            $tot_pe_pnr = $tot_pe_pnr + $rowp['pe_pe'];
                        } elseif ($rowp['tp_id'] == 4) {//Operacion de Funcionamiento
                            $tot_of = $tot_of + 1;
                            $tot_cost_of = $tot_cost_of + $suma_prog[0]['total_programado'];
                            $tot_mod_of = $tot_mod_of + $total_mod;
                            $tot_ctv_of = $tot_ctv_of + $ctv;
                            $tot_epa_of = $tot_epa_of + $se;
                            $tot_pi_of = $tot_pi_of + $rowp['pe_pi'];
                            $tot_pm_of = $tot_pm_of + $rowp['pe_pm'];
                            $tot_pv_of = $tot_pv_of + $rowp['pe_pv'];
                            $tot_pe_of = $tot_pe_of + $rowp['pe_pe'];
                        }
                    }

                }
            }
        }
        $tot_porc_pi=0;$tot_porc_pnr=0;$tot_porc_pr=0;$tot_porc_of=0;
        if($tot_pv_pi!=0){$tot_porc_pi  =$tot_pe_pi/$tot_pv_pi*100;}
        if($tot_pv_pnr!=0){$tot_porc_pnr=$tot_pe_pnr/$tot_pv_pnr*100;}
        if($tot_pv_pr!=0){$tot_porc_pr  =$tot_pe_pr/$tot_pv_pr*100;}
        if($tot_pv_of!=0){$tot_porc_of  =$tot_pe_of/$tot_pv_of*100;}

        $tot_pora_pi=0;$tot_pora_pnr=0;$tot_pora_pr=0;$tot_pora_of=0;
        if($tot_ctv_pi!=0){$tot_pora_pi=$tot_epa_pi/$tot_ctv_pi*100;}
        if($tot_ctv_pnr!=0){$tot_pora_pnr=$tot_epa_pnr/$tot_ctv_pnr*100;}
        if($tot_ctv_pr!=0){$tot_pora_pr=$tot_epa_pr/$tot_ctv_pr*100;}
        if($tot_ctv_of!=0){$tot_pora_of=$tot_epa_of/$tot_ctv_of*100;}

        $total_gral_op  =$tot_pi+$tot_pnr+$tot_pr+$tot_of;
        $total_gral_cost=$tot_cost_pi+$tot_cost_pnr+$tot_cost_pr+$tot_cost_of;
        $total_gral_mod =$tot_mod_pi+$tot_mod_pnr+$tot_mod_pr+$tot_mod_of;
        $total_gral_ctv =$tot_ctv_pi+$tot_ctv_pnr+$tot_ctv_pr+$tot_ctv_of;
        $total_gral_epa =$tot_epa_pi+$tot_epa_pnr+$tot_epa_pr+$tot_epa_of;
        $total_gral_pora=0;
        if($total_gral_ctv!=0){$total_gral_pora=$total_gral_epa/$total_gral_ctv*100;}

        $total_gral_pi  =$tot_pi_pi+$tot_pi_pnr+$tot_pi_pr+$tot_pi_of;
        $total_gral_pm  =$tot_pm_pi+$tot_pm_pnr+$tot_pm_pr+$tot_pm_of;
        $total_gral_pv  =$tot_pv_pi+$tot_pv_pnr+$tot_pv_pr+$tot_pv_of;
        $total_gral_pe  =$tot_pe_pi+$tot_pe_pnr+$tot_pe_pr+$tot_pe_of;
        $total_gral_porc=0;
        if($total_gral_pv!=0){$total_gral_porc=$total_gral_pe/$total_gral_pv*100;}

        /**Multimunicipales**/
        $tot_porc_pi_m=0;$tot_porc_pnr_m=0;$tot_porc_pr_m=0;$tot_porc_of_m=0;
        if($tot_pv_pi_m!=0){$tot_porc_pi_m  =$tot_pe_pi_m/$tot_pv_pi_m*100;}
        if($tot_pv_pnr_m!=0){$tot_porc_pnr_m=$tot_pe_pnr_m/$tot_pv_pnr_m*100;}
        if($tot_pv_pr_m!=0){$tot_porc_pr_m  =$tot_pe_pr_m/$tot_pv_pr_m*100;}
        if($tot_pv_of_m!=0){$tot_porc_of_m  =$tot_pe_of_m/$tot_pv_of_m*100;}

        $tot_pora_pi_m=0;$tot_pora_pnr_m=0;$tot_pora_pr_m=0;$tot_pora_of_m=0;
        if($tot_ctv_pi_m!=0){$tot_pora_pi_m  =$tot_epa_pi_m/$tot_ctv_pi_m*100;}
        if($tot_ctv_pnr_m!=0){$tot_pora_pnr_m=$tot_epa_pnr_m/$tot_ctv_pnr_m*100;}
        if($tot_ctv_pr_m!=0){$tot_pora_pr_m  =$tot_epa_pr_m/$tot_ctv_pr_m*100;}
        if($tot_ctv_of_m!=0){$tot_pora_of_m  =$tot_epa_of_m/$tot_ctv_of_m*100;}

        $total_gral_op_m  =$tot_pi_m+$tot_pnr_m+$tot_pr_m+$tot_of_m;
        $total_gral_cost_m=$tot_cost_pi_m+$tot_cost_pnr_m+$tot_cost_pr_m+$tot_cost_of;
        $total_gral_mod_m =$tot_mod_pi_m+$tot_mod_pnr_m+$tot_mod_pr_m+$tot_mod_of_m;
        $total_gral_ctv_m =$tot_ctv_pi_m+$tot_ctv_pnr_m+$tot_ctv_pr_m+$tot_ctv_of_m;
        $total_gral_epa_m =$tot_epa_pi_m+$tot_epa_pnr_m+$tot_epa_pr_m+$tot_epa_of_m;
        $total_gral_pora_m=0;
        if($total_gral_ctv_m!=0){$total_gral_pora_m=$total_gral_epa_m/$total_gral_ctv_m*100;}

        $total_gral_pi_m  =$tot_pi_pi_m+$tot_pi_pnr_m+$tot_pi_pr_m+$tot_pi_of_m;
        $total_gral_pm_m  =$tot_pm_pi_m+$tot_pm_pnr_m+$tot_pm_pr_m+$tot_pm_of_m;
        $total_gral_pv_m  =$tot_pv_pi_m+$tot_pv_pnr_m+$tot_pv_pr_m+$tot_pv_of_m;
        $total_gral_pe_m  =$tot_pe_pi_m+$tot_pe_pnr_m+$tot_pe_pr_m+$tot_pe_of_m;
        $total_gral_porc_m=0;
        if($total_gral_pv_m!=0){$total_gral_porc_m=$total_gral_pe_m/$total_gral_pv_m*100;}

        $tabla.='</tbody>';
        if($p1==1) {
            $total_acum_pi=0;$total_acum_pi_fis=0;
            if($tot_pi!=0){
                $total_acum_pi=number_format($acum_pi/$tot_pi,2,',','.');
                $total_acum_pi_fis=number_format($acum_pi_fis/$tot_pi,2,',','.');
            }
            $tabla .= '<tr class="total_pi">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROYECTOS DE INVERSIÓN</td>';
            $tabla .= '<th >' . $tot_pi . '</th><th class="derecha">' . number_format($tot_cost_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pi,2,',','.') . ' %</th><th class="derecha">'.$total_acum_pi_fis.'</th>
                <th class="derecha">' . number_format($tot_pi_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pi,2,',','.') . '%</th><th ></th><th ></th><th class="derecha">'.$total_acum_pi.'</th><th colspan="3"></th></tr>';
        }
        if($p2==1) {
            $total_acum_pr=0;$total_acum_pr_fis=0;
            if($tot_pr!=0){
                $total_acum_pr=number_format($acum_pr/$tot_pr,2,',','.');
                $total_acum_pr_fis=number_format($acum_pr_fis/$tot_pr,2,',','.');
            }
            $tabla .= '<tr class="total_pr">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROGRAMAS RECURRENTES</td>';
            $tabla .= '<th >' . $tot_pr . '</th><th class="derecha">' . number_format($tot_cost_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pr,2,',','.') . ' %</th><th class="derecha">'.$total_acum_pr_fis.'</th>
                 <th class="derecha">' . number_format($tot_pi_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pr,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_pr.'</th><th colspan="3"></th></tr>';
        }
        if($p3==1) {
            $total_acum_pnr=0;$total_acum_prn_fis=0;
            if($tot_pnr!=0){
                $total_acum_pnr=number_format($acum_pnr/$tot_pnr,2,',','.');
                $total_acum_prn_fis=number_format($acum_pnr_fis/$tot_pnr,2,',','.');
            }
            $tabla .= '<tr class="total_pnr">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROGRAMAS NO RECURRENTES</td>';
            $tabla .= '<th >' . $tot_pnr . '</th><th class="derecha">' . number_format($tot_cost_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pnr,2,',','.') . ' %</th><th class="derecha">'.$total_acum_prn_fis.'</th>
                 <th class="derecha">' . number_format($tot_pi_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pnr,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_pnr.'</th><th colspan="3"></th></tr>';
        }
        if($p4==1) {
            $total_acum_of=0;$total_acum_of_fis=0;
            if($tot_of!=0){
                $total_acum_of=number_format($acum_of/$tot_of,2,',','.');
                $total_acum_of_fis=number_format($acum_of_fis/$tot_of,2,',','.');
            }
            $tabla .= '<tr class="total_of">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL OPERACIONES DE FUNCIONAMIENTO</td>';
            $tabla .= '<th >' . $tot_of . '</th><th class="derecha">' . number_format($tot_cost_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_of,2,',','.') . ' %</th><th class="derecha">'.$total_acum_of_fis.'</th>
                 <th class="derecha">' . number_format($tot_pi_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_of,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_of.'</th><th colspan="3"></th></tr>';
        }
        $total_acum=0;$total_acum_fis=0;
        if($total_gral_op!=0){
            $total_acum=number_format($acumulado/$total_gral_op, 2, ',', '.');
            $total_acum_fis=number_format($acumulado_efis/$total_gral_op, 2, ',', '.');
        }

        $tabla.='<tr class="total_general">';
        $tabla.='<th colspan="3" class="izquierda">TOTAL DEL MUNICIPIO</td>';
        $tabla.='<th >'.$total_gral_op.'</th><th class="derecha">'.number_format($total_gral_cost, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_mod, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_ctv, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_epa, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pora, 2, ',', '.').' %</th><th class="derecha" >'.$total_acum_fis.'</th>
                 <th class="derecha">'.number_format($total_gral_pi, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pm, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pv, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pe, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_porc, 2, ',', '.').' %</th><th ></th><th ></th><th class="derecha">'.$total_acum.'</th><th colspan="3"></th></tr>';
        if($tab_aux!=''){
            $total_acum_multi=0;$total_acum_multi_fis=0;
            if($total_gral_op_m!=0){
                $total_acum_multi=number_format($acumulado_multi/$total_gral_op_m, 2, ',', '.');
                $total_acum_multi_fis=number_format($acumulado_multi_efis/$total_gral_op_m, 2, ',', '.');
            }

            $tabla.='<tr><br>
				<td colspan="17"><strong><center><h2>OPERACIONES MULTIMUNICIPAL</h2></center></strong></td></tr>'.$tab_aux;
            $tabla.='<tr class="total_multi"> <th colspan="3" class="izquierda">TOTAL MULTIMUNICIPAL</th>';
            $tabla.='<th >'.$total_gral_op_m.'</th><th class="derecha">'.number_format($total_gral_cost_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_mod_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_ctv_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_epa_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pora_m, 2, ',', '.').' %</th><th class="derecha">'.$total_acum_multi_fis.'</th>
                 <th class="derecha">'.number_format($total_gral_pi_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pm_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pv_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pe_m, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_porc_m, 2, ',', '.').' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_multi.'</th><th colspan="3"></th></tr>';

            $tabla.='<tr class="total_general">';
            $tabla.='<th colspan="3" class="izquierda">TOTAL GENERAL</th>';
            $gran_total=$total_gral_op+$total_gral_op_m;
            $gran_total_por_acum=0;
            if(($total_gral_ctv+$total_gral_ctv_m)!=0) {$gran_total_por_acum=($total_gral_epa+$total_gral_epa_m)/($total_gral_ctv+$total_gral_ctv_m)*100;}
            $gran_total_porc=0;
            if(($total_gral_pv+$total_gral_pv_m)!=0) {$gran_total_porc=($total_gral_pe+$total_gral_pe_m)/($total_gral_pv+$total_gral_pv_m)*100;}
            $gran_total_media=0;$gran_total_media_fis=0;
            if($gran_total!=0){
                $gran_total_media=number_format(($acumulado_multi+$acumulado)/$gran_total, 2, ',', '.');
                $gran_total_media_fis=number_format(($acumulado_multi_efis+$acumulado_efis)/$gran_total, 2, ',', '.');
            }
            $tabla.='<th >'.$gran_total.'</th><th class="derecha">'.number_format($total_gral_cost+$total_gral_cost_m, 2, ',', '.').'</th>
					<th class="derecha">'.number_format($total_gral_mod+$total_gral_mod_m, 2, ',', '.').'</th>
					<th class="derecha">'.number_format($total_gral_ctv+$total_gral_ctv_m, 2, ',', '.').'</th>
					<th class="derecha">'.number_format($total_gral_epa+$total_gral_epa_m, 2, ',', '.').'</th>
					<th class="derecha">'.number_format($gran_total_por_acum, 2, ',', '.').' %</th>
					<th class="derecha">'.$gran_total_media_fis.'</th>
                    <th class="derecha">'.number_format($total_gral_pi+$total_gral_pi_m, 2, ',', '.').'</th>
                    <th class="derecha">'.number_format($total_gral_pm+$total_gral_pm_m, 2, ',', '.').'</th>
                    <th class="derecha">'.number_format($total_gral_pv+$total_gral_pv_m, 2, ',', '.').'</th>
                    <th class="derecha">'.number_format($total_gral_pe+$total_gral_pe_m, 2, ',', '.').'</th>
                    <th class="derecha">'.number_format($gran_total_porc, 2, ',', '.').' %</th>
                    <th ></th><th ></th><th class="derecha">'.$gran_total_media.'</th><th colspan="3"></th></tr>';
        }
        $tabla.='</table><br>';
        return $tabla;
    }

    /*===================================== Ejecución de acciones operativas por unidad ejecutora =====================================*/
    public function acciones_por_unidad_ejecutora(){
        $data['menu']=$this->menu(10);

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['ue'] = $this->mmae->unidades_ejecutoras();
        $data['meses'] = $this->model_ejecucion->list_meses(); /// Lista de Meses
        $this->load->view('reportes/reportes_mae/select_op_ue', $data);
    }
    public function validar_acciones_por_unidad_ejecutora(){
        if($this->input->server('REQUEST_METHOD') === 'POST'){
            $sql_in='';
            if($this->input->post('p1')=='on'){$p1=1;$tp_1=1;}else{$p1=0;$tp_1=0;}
            if($this->input->post('p2')=='on'){$p2=1;$tp_2=2;}else{$p2=0;$tp_2=0;}
            if($this->input->post('p3')=='on'){$p3=1;$tp_3=3;}else{$p3=0;$tp_3=0;}
            if($this->input->post('p4')=='on'){$p4=1;$tp_4=4;}else{$p4=0;$tp_4=0;}
            $sql_in='in ('.$tp_1.','.$tp_2.','.$tp_3.','.$tp_4.')';

            $enlaces = $this->menu_modelo->get_Modulos(10);
            $data['enlaces'] = $enlaces;
            for ($i = 0; $i < count($enlaces); $i++) {
                $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;

            $meses=$this->get_mes($this->input->post('meses_id'));
            $mes = $meses[1];
            $dias = $meses[2];
            $mes_id=$this->input->post('meses_id');
            $data['id_mes'] = $mes_id;
            $data['mes'] = $mes;
            $data['dias'] = $dias;

            $pr1='';$pr2='';$pr3='';$pr4='';
            if($p1==1){$pr1='<span class="pi_t"> PROYECTOS DE INVERSI&Oacute;N</span>';}
            if($p2==1){$pr2=' - <span class="pr_t">PROGRAMA RECURRENTE</span>';}
            if($p3==1){$pr3=' - <span class="pnr_t">PROGRAMA NO RECURRENTE</span>';}
            if($p4==1){$pr4=' - <span class="of_t">OPERACI&Oacute;N DE FUNCIONAMIENTO</span>';}

            $data['pr1']=$pr1;
            $data['pr2']=$pr2;
            $data['pr3']=$pr3;
            $data['pr4']=$pr4;

            $data['p1']=$p1;
            $data['p2']=$p2;
            $data['p3']=$p3;
            $data['p4']=$p4;
            $data['ue_id']=$this->input->post('ue_id');

            if($this->input->post('ue_id')==0){
                $data['proyectos']=$this->list_ejecucion_fisica_todos($p1,$p2,$p3,$p4,$sql_in,1,$mes_id);
                $data['title']=' Todas las UE';
                $data['municipio']='';
            }
            else{
                $data['proyectos']=$this->list_ejecucion_fisica_ue($p1,$p2,$p3,$p4,$sql_in,$this->input->post('ue_id'),1,$mes_id);
                $data['title']=' Por Unidad Ejecutora';
                $municipio = $this->model_taqpacha->get_unidad_ejec($this->input->post('ue_id'));
                $data['municipio']='<br>U.E. - '.strtoupper($municipio[0]['uni_unidad']);
            }

            $this->load->view('reportes/reportes_mae/op_ue', $data);
        }
        else{
            $this->session->set_flashdata('danger','Problemas al realizar la consulta, contactese con el Web Master');
            redirect('reportes_mae/unidad_ejec');
        }
    }
    public function reporte_acciones_por_unidad_ejecutora($p1,$p2,$p3,$p4,$ue_id,$mes_id)
    {
        $mess=$this->get_mes($mes_id);
        $mes = $mess[1];
        $dias = date('d');//$mess[2];
        $sql_in='';
        if($p1==1){$tp_1=1;}else{$tp_1=0;}
        if($p2==1){$tp_2=2;}else{$tp_2=0;}
        if($p3==1){$tp_3=3;}else{$tp_3=0;}
        if($p4==1){$tp_4=4;}else{$tp_4=0;}
        $sql_in='in ('.$tp_1.','.$tp_2.','.$tp_3.','.$tp_4.')';
        if($ue_id==0){
            $detalles=$this->list_ejecucion_fisica_todos($p1,$p2,$p3,$p4,$sql_in,2,$mes_id);
            $prov_nombre='(TODAS)';
        }
        else{
            $detalles=$this->list_ejecucion_fisica_ue($p1,$p2,$p3,$p4,$sql_in,$ue_id,2,$mes_id);
            $provincia = $this->model_taqpacha->get_unidad_ejec($ue_id);
            $prov_nombre=strtoupper($provincia[0]['uni_unidad']);
        }

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_resumen.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> EJECUCI&Oacute;N DE ACCIONES OPERATIVAS POR U.E. / '.$prov_nombre.'<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$detalles.'
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("ejecucion_acciones_operativas_ue.pdf", array("Attachment" => false));
    }
    function list_ejecucion_fisica_todos($p1,$p2,$p3,$p4,$sql_in,$tp_rep,$mes_id){

        if($tp_rep==1){
            $tb='id="dt_basic_float" class="table table-bordered" width="100%"';
        }
        elseif ($tp_rep==2) {
            $tb='border="1" cellpadding="0" cellspacing="0" class="table table table-bordered" width="100%"';
        }

        $nro=0;
        $tabla ='';
        if($tp_rep==1) {
            $tabla .= '<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h3 class="alert alert-success"><center>EJECUCI&Oacute;N DE ACCIONES OPERATIVAS POR UNIDAD EJECUTORA </center></h3>
                  </article>
                  <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                        <header>
                          <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                          <h2 class="font-md"><strong>TODAS LAS UNIDADES EJECUTORAS</strong></h2>  
                        </header>
                	<div>
                  <div class="widget-body no-padding">
                  <div class="table-responsive">';
        }

        $unid_ej = $this->mmae->unidades_ejecutoras();

        if(count($unid_ej)!=0){
            $nro++;
            $tabla .='<table '.$tb.'>';
            $tabla .='<thead>';
            $tabla .='<tr><th class="titulo_cabecera" style="width:1%;" rowspan="2">#</th>
				<th class="titulo_cabecera" style="width:15%;" rowspan="2">UNIDAD EJECUTORA</th>';

            if($p1==1)
                $tabla .='<th class="titulo_cabecera" style="width:15%;" rowspan="2">NRO DE PROYECTOS</th>';
            if($p2==1)
                $tabla .='<th class="titulo_cabecera" style="width:20%;" rowspan="2">NRO DE PROGRAMAS RECURRENTES</th>';
            if($p3==1)
                $tabla .='<th class="titulo_cabecera" style="width:10%;" rowspan="2">NRO DE PROGRAMAS NO RECURRENTES</th>';
            if($p4==1)
                $tabla .='<th class="titulo_cabecera" rowspan="2">NRO. OP. DE FUNCIONAMIENTO </th>';
            $tabla .='<th colspan="5" class="cabecera">PRESUPUESTO GESTI&Oacute;N '.$this->gestion.'</th></tr>';
            $tabla .='<tr class="modo1">';
            $tabla .='<th class="cabecera">INICIAL </th>';
            $tabla .='<th class="cabecera">MODIFICACIONES </th>';
            $tabla .='<th class="cabecera">VIGENTE </th>';
            $tabla .='<th class="cabecera">EJECUTADO </th>';
            $tabla .='<th class="cabecera">% DE EJECUCIÓN PRESUPUESTARIA </th>';
            $tabla .='</tr>';
            $tabla .='</thead>';
            $nrop=0;
            $tabla .='<tbody>';
            $total_pi=0;$total_pr=0;$total_pnr=0;$total_of=0;
            $total_mm_pi=0;$total_mm_pr=0;$total_mm_pnr=0;$total_mm_of=0;
            $total_pemm_pini=0;$total_pemm_pmod=0;$total_pemm_pvig=0;$total_pemm_peje=0;
            $total_gral_pe_pini=0;$total_gral_pe_pmod=0;$total_gral_pe_pvig=0;$total_gral_pe_peje=0;
            foreach($unid_ej as $row_unid_ej)
            {

                $proyectos = $this->mmae->get_proyectos_ue($row_unid_ej['uni_id'],$mes_id,$sql_in);
                if(count($proyectos)!=0){
                    $nrop++;
                    $tabla .='<tr class="derecha">';
                    $tabla .='<td class="centro">'.$nrop.'</td>';
                    $tabla .='<td class="izquierda">'.$row_unid_ej['uni_unidad'].'</td>';
                    $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                    $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
                    foreach($proyectos as $rowp){
                        $total_pe_pini=$total_pe_pini+$rowp['pe_pi'];
                        $total_pe_pmod=$total_pe_pmod+$rowp['pe_pm'];
                        $total_pe_pvig=$total_pe_pvig+$rowp['pe_pv'];
                        $total_pe_peje=$total_pe_peje+$rowp['pe_pe'];
                        if($rowp['pe_pv']!=0){
                            $total_pe_porc=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                        }
                        if($rowp['tp_id']==1){//Proyecto de Inversión
                            $tot_pi=$tot_pi+1;
                        }elseif($rowp['tp_id']==2){//Programa Recurrente
                            $tot_pr=$tot_pr+1;
                        }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                            $tot_pnr=$tot_pnr+1;
                        }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                            $tot_of=$tot_of+1;
                        }
                    }
                    /*}else{
					$tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
					$total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
				}*/

                    $total_pe_porc=0;
                    if($total_pe_pvig!=0){
                        $total_pe_porc=$total_pe_peje/$total_pe_pvig*100;
                    }
                    if($p1==1)
                        $tabla .='<td class="pi">'.$tot_pi.'</td>';
                    if($p2==1)
                        $tabla .='<td class="pr">'.$tot_pr.'</td>';
                    if($p3==1)
                        $tabla .='<td class="pnr">'.$tot_pnr.'</td>';
                    if($p4==1)
                        $tabla .='<td class="of">'.$tot_of.'</td>';
                    $tabla .='<td>'.number_format($total_pe_pini, 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($total_pe_pmod, 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($total_pe_pvig, 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($total_pe_peje, 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($total_pe_porc, 2, ',', '.').'%</td>';
                    $tabla .='</tr>';
                }else{$tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                    $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;}

                $total_pi =$total_pi+$tot_pi;
                $total_pr =$total_pr+$tot_pr;
                $total_pnr=$total_pnr+$tot_pnr;
                $total_of =$total_of+$tot_of;
                $total_gral_pe_pini=$total_gral_pe_pini+$total_pe_pini;
                $total_gral_pe_pmod=$total_gral_pe_pmod+$total_pe_pmod;
                $total_gral_pe_pvig=$total_gral_pe_pvig+$total_pe_pvig;
                $total_gral_pe_peje=$total_gral_pe_peje+$total_pe_peje;

            }
            $total_gral_pe_porc=0;
            if($total_gral_pe_pvig!=0){$total_gral_pe_porc=$total_gral_pe_peje/$total_gral_pe_pvig*100;}

            $tabla .='</tbody>';
            $tabla .='<tr class="total_general">';
            $tabla .='<td colspan="2" class="izquierda"><b>TOTAL : '.$nrop.'</b></td>';
            if($p1==1)
                $tabla .='<td class="centro">'.$total_pi.'</td>';
            if($p2==1)
                $tabla .='<td class="centro">'.$total_pr.'</td>';
            if($p3==1)
                $tabla .='<td class="centro">'.$total_pnr.'</td>';
            if($p4==1)
                $tabla .='<td class="centro">'.$total_of.'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pini, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pmod, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pvig, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_peje, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_porc, 2, ',', '.').'%</td>';
            $tabla .='</tr>';
            $tabla .='</table>';
            if($tp_rep==1) {
                $tabla .= '</div>
                </div>
              </div>
            </article>';
            }
        }
        return $tabla;
    }
    function list_ejecucion_fisica_ue($p1,$p2,$p3,$p4,$sql_in,$ue_id,$tp_rep,$mes_id){

        if($tp_rep==1){
            $tb='id="dt_basic_float" class="table table-bordered"';
        }
        elseif ($tp_rep==2) {
            $tb='border="1" cellpadding="0" cellspacing="0" class="table table table-bordered" width="100%"';
        }
        $nro=0;
        $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
        $acum_pi=0;$acum_pr=0;$acum_pnr=0;$acum_of=0;
        $acum_pi_fis=0;$acum_pr_fis=0;$acum_pnr_fis=0;$acum_of_fis=0;
        $tot_cost_pi=0;$tot_cost_pr=0;$tot_cost_pnr=0;$tot_cost_of=0;
        $tot_mod_pi=0;$tot_mod_pr=0;$tot_mod_pnr=0;$tot_mod_of=0;
        $tot_ctv_pi=0;$tot_ctv_pr=0;$tot_ctv_pnr=0;$tot_ctv_of=0;
        $tot_epa_pi=0;$tot_epa_pr=0;$tot_epa_pnr=0;$tot_epa_of=0;
        $tot_pi_pi=0;$tot_pi_pr=0;$tot_pi_pnr=0;$tot_pi_of=0;
        $tot_pm_pi=0;$tot_pm_pr=0;$tot_pm_pnr=0;$tot_pm_of=0;
        $tot_pv_pi=0;$tot_pv_pr=0;$tot_pv_pnr=0;$tot_pv_of=0;
        $tot_pe_pi=0;$tot_pe_pr=0;$tot_pe_pnr=0;$tot_pe_of=0;

        $acumulado=0;$acumulado_efis=0;
        $tabla ='';
        $tabla.='<table '.$tb.'>';
        $tabla.='<thead>';
        $tabla.='<tr>
				<th class="titulo_cabecera" style="width:1%;" rowspan="2">Nro.</th>
				<th class="titulo_cabecera" style="width:8%;" rowspan="2">NOMBRE</th>
				<th class="titulo_cabecera" style="width:5%;" rowspan="2">PROVINCIA <br>Y<br> MUNICIPIO(S)</th>
				<th class="titulo_cabecera" style="width:4%;" rowspan="2">TIPO</th>
				<th class="cabecera" colspan="3">COSTO TOTAL DE LA OPERACI&Oacute;N</th>
				<th class="cabecera_acum" colspan="3">EJECUCIÓN ACUMULADA</th>
	    		<th class="cabecera" colspan="5">PRESUPUESTO GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>
	    		<th class="cabecera_acum" colspan="3">EJECUCI&Oacute;N FÍSICA '.$this->session->userdata('gestion').'</th>
	    		<th class="titulo_cabecera" style="width:5%;" rowspan="2">ESTADO</th>
	    		<th class="titulo_cabecera" style="width:6%;" rowspan="2">UNIDAD EJECUTORA</th>
	    		<th class="titulo_cabecera" style="width:6%;" rowspan="2">RESPONSABLE</th>
			</tr>';

        $tabla.='<tr class="modo1">
				<th class="cabecera" style="width:6%;">INICIAL</th>
				<th class="cabecera" style="width:5%;">MODIFICACIONES</th>
				<th class="cabecera" style="width:6%;">VIGENTE</th>
				<th class="cabecera_acum" style="width:5%;">EJECUCIÓN FINANCIERA</th>
				<th class="cabecera_acum" style="width:5%;">% EJEC. FINANCIERA</th>
				<th class="cabecera_acum" style="width:5%;">% DE AVANCE FÍSICO</th>';
        $tabla.='<th class="cabecera" style="width:5%;"> INICIAL </th>';
        $tabla.='<th class="cabecera" style="width:5%;"> MODIFICADO </th>';
        $tabla.='<th class="cabecera" style="width:5%;"> VIGENTE </th>';
        $tabla.='<th class="cabecera" style="width:5%;">EJEC. FINANCIERA </th>';
        $tabla.='<th class="cabecera" style="width:5%;">% EJEC. FINANCIERA </th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">PROG. %</th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">EJEC %</th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">% EJEC. ANUAL</th>';
        $tabla.='</tr>';
        $tabla.='</thead>';
        $tabla.='<tbody>';
        $proy = $this->mmae->get_proyectos_ue($ue_id,$mes_id,$sql_in);
        if(count($proy)!=0){
            $nro=0;
            foreach($proy as $rowp){
                $muni_proy=$this->model_taqpacha->regiones_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                $color_mult='';
                if($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']>1){//Multiregional
                    $color_mult='style="background-color:#EDFBFF"';
                }
                $se=$rowp['pe_pe'];
                $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                    $se=$se+$suma[0]['total_ejecutado'];
                }

                $suma_prog=$this->mdas_complementario->costo_total_proyecto($rowp['pfec_id']);
                $tabla .='<tr class="modo1 derecha" '.$color_mult.'>';
                $nro=$nro+1;
                $tabla .='<td class="centro">'.$nro.'</td>';
                $tabla.='<td class="izquierda"><a href="javascript:abreVentana(\''.site_url("reportes_mae").'/resumen_ejec/'.$rowp['proy_id'].'/'.$mes_id.'\');"'.'>'.$rowp['proy_nombre'].'</a></td>';
                $tabla.='<td class="izquierda">';
                $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);
                if((count($prov)>3) and ($tp_rep == 1)){
                    $tabla .= '<p class="btn btn-default" href="#" class="form_pond" title="';
                }elseif($tp_rep ==2){
                    $tabla .= '<div style="font-size: 5px;">';
                }
                foreach($prov as $row_prov){
                    $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                    if (count($prov) > 3) {
                        if ($tp_rep == 1) {
                            foreach ($muni as $muni) {
                                $tabla .= $row_prov['prov_provincia'] .'&#09;'. $muni['muni_municipio'] . '&#13;';
                            }
                        } else {
                            $tabla .='<ul>'.$row_prov['prov_provincia'];
                            foreach ($muni as $muni) {
                                $tabla .= '<li>'.$muni['muni_municipio'] . '</li>';
                            }
                            $tabla .= '</ul>';
                        }
                    } else {
                        $tabla .= '<ul><strong>'.$row_prov['prov_provincia'].'</strong>';
                        foreach ($muni as $muni) {
                            $tabla .= '<li>'.$muni['muni_municipio'].'</li>';
                        }
                        $tabla .= '</ul>';
                    }
                }
                if((count($prov) > 3) and ($tp_rep == 1)){
                    $tabla .= '"><i class="glyphicon glyphicon-search"></i>Ver...</p>';
                }elseif($tp_rep==2){
                    $tabla .= '</div>';
                }
                $tabla .='</td>';
                $tabla.='<td class="izquierda">'.$rowp['tp_tipo'].'</td>';
                $tabla.='<td>'.number_format($suma_prog[0]['total_programado'], 2, ',', '.').'</td>';
                $toal_modif=$this->mdas_complementario->modificaciones_costo_total($rowp['pfec_id']);
                $total_mod=0;
                if($toal_modif[0]['total_modif']!='' || $toal_modif[0]['total_modif']!=0){
                    $total_mod=$toal_modif[0]['total_modif'];
                }
                $tabla.='<td>'.number_format($total_mod, 2, ',', '.').'</td>';
                $ctv=$suma_prog[0]['total_programado']+$total_mod;
                $tabla.='<td>'.number_format($ctv, 2, ',', '.').'</td>';
                $tabla.='<td>'.number_format($se, 2, ',', '.').'</td>';

                $efin=0;
                if($suma_prog[0]['total_programado']!=0){
                    $efin=round((($se/$suma_prog[0]['total_programado'])*100),2);
                }

                $tabla.='<td>'.$efin.'%</td>';

                $efis=$this->avance_fisico($rowp['proy_id'],$mes_id);
                $acumulado_efis=$acumulado_efis+$efis;
                $tabla.='<td>'.round($efis,2).'%</td>';
                $tabla.='<td>'.number_format($rowp['pe_pi'], 2, ',', '.').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pm'], 2, ',', '.').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pv'], 2, ',', '.').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pe'], 2, ',', '.').'</td>';

                $ejec=0;
                if($rowp['pe_pv']!=0){
                    $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                }
                $tabla.='<td>'.$ejec.'%</td>';

                $estado=$this->mdas_complementario->estado_operacion($rowp['pfec_id'],$mes_id,$this->gestion);
                $estado_proy='No registrado';
                if(count($estado)!=0){
                    $estado_proy=$estado[0]['ep_descripcion'];
                }
                $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));

                $tabla .= '<td class="derecha">'.$ejecutado[0].'</td>';
                $tabla .= '<td class="derecha">'.$ejecutado[1].'</td>';
                $tabla .= '<td class="derecha">'.$ejecutado[2].'</td>';
                $acumulado=$acumulado+$ejecutado[2];
                $tabla.='<td class="izquierda">'.$estado_proy.'</td>';
                $tabla.='<td class="izquierda">'.$rowp['uni_unidad'].'</td>';
                $tabla.='<td class="izquierda">'.$rowp['responsable'].'<br><font color="red">Telf: '.$rowp['fun_telefono'].'</font></td>';
                $tabla.='</tr>';
                if($rowp['tp_id']==1){//Proyecto de Inversión
                    $tot_pi=$tot_pi+1;
                    $acum_pi=$acum_pi+$ejecutado[2];
                    $acum_pi_fis=$acum_pi_fis+$efis;
                    $tot_cost_pi=$tot_cost_pi+$suma_prog[0]['total_programado'];
                    $tot_mod_pi=$tot_mod_pi+$total_mod;
                    $tot_ctv_pi=$tot_ctv_pi+$ctv;
                    $tot_epa_pi=$tot_epa_pi+$se;
                    $tot_pi_pi=$tot_pi_pi+$rowp['pe_pi'];
                    $tot_pm_pi=$tot_pm_pi+$rowp['pe_pm'];
                    $tot_pv_pi=$tot_pv_pi+$rowp['pe_pv'];
                    $tot_pe_pi=$tot_pe_pi+$rowp['pe_pe'];
                }elseif($rowp['tp_id']==2){//Programa Recurrente
                    $tot_pr=$tot_pr+1;
                    $acum_pr=$acum_pr+$ejecutado[2];
                    $acum_pr_fis=$acum_pr_fis+$efis;
                    $tot_cost_pr=$tot_cost_pr+$suma_prog[0]['total_programado'];
                    $tot_mod_pr=$tot_mod_pr+$total_mod;
                    $tot_ctv_pr=$tot_ctv_pr+$ctv;
                    $tot_epa_pr=$tot_epa_pr+$se;
                    $tot_pi_pr=$tot_pi_pr+$rowp['pe_pi'];
                    $tot_pm_pr=$tot_pm_pr+$rowp['pe_pm'];
                    $tot_pv_pr=$tot_pv_pr+$rowp['pe_pv'];
                    $tot_pe_pr=$tot_pe_pr+$rowp['pe_pe'];
                }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                    $tot_pnr=$tot_pnr+1;
                    $acum_pnr=$acum_pnr+$ejecutado[2];
                    $acum_pnr_fis=$acum_pnr_fis+$efis;
                    $tot_cost_pnr=$tot_cost_pnr+$suma_prog[0]['total_programado'];
                    $tot_mod_pnr=$tot_mod_pnr+$total_mod;
                    $tot_ctv_pnr=$tot_ctv_pnr+$ctv;
                    $tot_epa_pnr=$tot_epa_pnr+$se;
                    $tot_pi_pnr=$tot_pi_pnr+$rowp['pe_pi'];
                    $tot_pm_pnr=$tot_pm_pnr+$rowp['pe_pm'];
                    $tot_pv_pnr=$tot_pv_pnr+$rowp['pe_pv'];
                    $tot_pe_pnr=$tot_pe_pnr+$rowp['pe_pe'];
                }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                    $tot_of=$tot_of+1;
                    $acum_of=$acum_of+$ejecutado[2];
                    $acum_of_fis=$acum_of_fis+$efis;
                    $tot_cost_of=$tot_cost_of+$suma_prog[0]['total_programado'];
                    $tot_mod_of=$tot_mod_of+$total_mod;
                    $tot_ctv_of=$tot_ctv_of+$ctv;
                    $tot_epa_of=$tot_epa_of+$se;
                    $tot_pi_of=$tot_pi_of+$rowp['pe_pi'];
                    $tot_pm_of=$tot_pm_of+$rowp['pe_pm'];
                    $tot_pv_of=$tot_pv_of+$rowp['pe_pv'];
                    $tot_pe_of=$tot_pe_of+$rowp['pe_pe'];
                }

            }
        }
        $tot_porc_pi=0;$tot_porc_pnr=0;$tot_porc_pr=0;$tot_porc_of=0;
        if($tot_pv_pi!=0){$tot_porc_pi  =$tot_pe_pi/$tot_pv_pi*100;}
        if($tot_pv_pnr!=0){$tot_porc_pnr=$tot_pe_pnr/$tot_pv_pnr*100;}
        if($tot_pv_pr!=0){$tot_porc_pr  =$tot_pe_pr/$tot_pv_pr*100;}
        if($tot_pv_of!=0){$tot_porc_of  =$tot_pe_of/$tot_pv_of*100;}

        $tot_pora_pi=0;$tot_pora_pnr=0;$tot_pora_pr=0;$tot_pora_of=0;
        if($tot_ctv_pi!=0){$tot_pora_pi=$tot_epa_pi/$tot_ctv_pi*100;}
        if($tot_ctv_pnr!=0){$tot_pora_pnr=$tot_epa_pnr/$tot_ctv_pnr*100;}
        if($tot_ctv_pr!=0){$tot_pora_pr=$tot_epa_pr/$tot_ctv_pr*100;}
        if($tot_ctv_of!=0){$tot_pora_of=$tot_epa_of/$tot_ctv_of*100;}

        $tabla.='</tbody>';
        if($p1==1) {
            $total_acum_pi=0;$total_acum_pi_fis=0;
            if($tot_pi!=0){
                $total_acum_pi=number_format($acum_pi/$tot_pi,2,',','.');
                $total_acum_pi_fis=number_format($acum_pi_fis/$tot_pi,2,',','.');
            }
            $tabla .= '<tr class="total_pi">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROYECTOS DE INVERSIÓN</td>';
            $tabla .= '<th >' . $tot_pi . '</th><th class="derecha">' . number_format($tot_cost_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pi,2,',','.') . ' %</th><th class="derecha">'.$total_acum_pi_fis.'</th>
                    <th class="derecha">' . number_format($tot_pi_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pi,2,',','.') . '%</th><th ></th><th ></th><th class="derecha">'.$total_acum_pi.'</th><th colspan="3"></th></tr>';
        }
        if($p2==1) {
            $total_acum_pr=0;$total_acum_pr_fis=0;
            if($tot_pr!=0){
                $total_acum_pr=number_format($acum_pr/$tot_pr,2,',','.');
                $total_acum_pr_fis=number_format($acum_pr_fis/$tot_pr,2,',','.');
            }
            $tabla .= '<tr class="total_pr">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROGRAMAS RECURRENTES</td>';
            $tabla .= '<th >' . $tot_pr . '</th><th class="derecha">' . number_format($tot_cost_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pr,2,',','.') . ' %</th><th class="derecha">'.$total_acum_pr_fis.'</th>
                     <th class="derecha">' . number_format($tot_pi_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pr,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_pr.'</th><th colspan="3"></th></tr>';
        }
        if($p3==1) {
            $total_acum_pnr=0;$total_acum_prn_fis=0;
            if($tot_pnr!=0){
                $total_acum_pnr=number_format($acum_pnr/$tot_pnr,2,',','.');
                $total_acum_prn_fis=number_format($acum_pnr_fis/$tot_pnr,2,',','.');
            }
            $tabla .= '<tr class="total_pnr">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROGRAMAS NO RECURRENTES</td>';
            $tabla .= '<th >' . $tot_pnr . '</th><th class="derecha">' . number_format($tot_cost_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pnr,2,',','.') . ' %</th><th class="derecha">'.$total_acum_prn_fis.'</th>
                     <th class="derecha">' . number_format($tot_pi_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pnr,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_pnr.'</th><th colspan="3"></th></tr>';
        }
        if($p4==1) {
            $total_acum_of=0;$total_acum_of_fis=0;
            if($tot_of!=0){
                $total_acum_of=number_format($acum_of/$tot_of,2,',','.');
                $total_acum_of_fis=number_format($acum_of_fis/$tot_of,2,',','.');
            }
            $tabla .= '<tr class="total_of">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL OPERACIONES DE FUNCIONAMIENTO</td>';
            $tabla .= '<th >' . $tot_of . '</th><th class="derecha">' . number_format($tot_cost_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_of,2,',','.') . ' %</th><th class="derecha">'.$total_acum_of_fis.'</th>
                     <th class="derecha">' . number_format($tot_pi_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_of,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_of.'</th><th colspan="3"></th></tr>';
        }

        $total_gral_op  =$tot_pi+$tot_pnr+$tot_pr+$tot_of;
        $total_gral_cost=$tot_cost_pi+$tot_cost_pnr+$tot_cost_pr+$tot_cost_of;
        $total_gral_mod =$tot_mod_pi+$tot_mod_pnr+$tot_mod_pr+$tot_mod_of;
        $total_gral_ctv =$tot_ctv_pi+$tot_ctv_pnr+$tot_ctv_pr+$tot_ctv_of;
        $total_gral_epa =$tot_epa_pi+$tot_epa_pnr+$tot_epa_pr+$tot_epa_of;
        $total_gral_pora=0;
        if($total_gral_ctv!=0){$total_gral_pora=$total_gral_epa/$total_gral_ctv*100;}

        $total_gral_pi  =$tot_pi_pi+$tot_pi_pnr+$tot_pi_pr+$tot_pi_of;
        $total_gral_pm  =$tot_pm_pi+$tot_pm_pnr+$tot_pm_pr+$tot_pm_of;
        $total_gral_pv  =$tot_pv_pi+$tot_pv_pnr+$tot_pv_pr+$tot_pv_of;
        $total_gral_pe  =$tot_pe_pi+$tot_pe_pnr+$tot_pe_pr+$tot_pe_of;
        $total_gral_porc=0;
        if($total_gral_pv!=0){$total_gral_porc=$total_gral_pe/$total_gral_pv*100;}
        $total_acum=0;$total_acum_fis=0;
        if($total_gral_op!=0){
            $total_acum=number_format($acumulado/$total_gral_op, 2, ',', '.');
            $total_acum_fis=number_format($acumulado_efis/$total_gral_op, 2, ',', '.');
        }
        $tabla.='<tr class="total_general">';
        $tabla.='<th colspan="3" class="izquierda">TOTAL DE LA U.E.</td>';
        $tabla.='<th >'.$total_gral_op.'</th><th class="derecha">'.number_format($total_gral_cost, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_mod, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_ctv, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_epa, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pora, 2, ',', '.').' %</th><th class="derecha" >'.$total_acum_fis.'</th>
                 <th class="derecha">'.number_format($total_gral_pi, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pm, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pv, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pe, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_porc, 2, ',', '.').' %</th><th ></th><th ></th><th class="derecha">'.$total_acum.'</th><th colspan="3"></th></tr>';

        $tabla .='</table><br>';
        return $tabla;
    }

    /*---------------------------------- Ejecución de acciones operativas por estado -----------------------------*/
    public function acciones_operativas_por_estado(){
        $data['menu']=$this->menu(10);
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['estados'] = $this->mmae->list_estados();
        $data['meses'] = $this->model_ejecucion->list_meses(); /// Lista de Meses
        $this->load->view('reportes/reportes_mae/select_opciones_estado', $data);
    }
    public function valida_acciones_operativas_por_estado(){
        if($this->input->server('REQUEST_METHOD') === 'POST'){
            $sql_in='';
            if($this->input->post('p1')=='on'){$p1=1;$tp_1=1;}else{$p1=0;$tp_1=0;}
            if($this->input->post('p2')=='on'){$p2=1;$tp_2=2;}else{$p2=0;$tp_2=0;}
            if($this->input->post('p3')=='on'){$p3=1;$tp_3=3;}else{$p3=0;$tp_3=0;}
            if($this->input->post('p4')=='on'){$p4=1;$tp_4=4;}else{$p4=0;$tp_4=0;}
            $sql_in='in ('.$tp_1.','.$tp_2.','.$tp_3.','.$tp_4.')';

            $enlaces = $this->menu_modelo->get_Modulos(10);
            $data['enlaces'] = $enlaces;
            for ($i = 0; $i < count($enlaces); $i++) {
                $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;

            $meses=$this->get_mes($this->input->post('meses_id'));
            $mes = $meses[1];
            $dias = $meses[2];
            $mes_id=$this->input->post('meses_id');
            $data['id_mes'] = $mes_id;
            $data['mes'] = $mes;
            $data['dias'] = $dias;

            $pr1='';$pr2='';$pr3='';$pr4='';
            if($p1==1){$pr1='<span class="pi_t"> PROYECTOS DE INVERSI&Oacute;N</span>';}
            if($p2==1){$pr2=' - <span class="pr_t">PROGRAMA RECURRENTE</span>';}
            if($p3==1){$pr3=' - <span class="pnr_t">PROGRAMA NO RECURRENTE</span>';}
            if($p4==1){$pr4=' - <span class="of_t">OPERACI&Oacute;N DE FUNCIONAMIENTO</span>';}

            $data['pr1']=$pr1;
            $data['pr2']=$pr2;
            $data['pr3']=$pr3;
            $data['pr4']=$pr4;

            $data['p1']=$p1;
            $data['p2']=$p2;
            $data['p3']=$p3;
            $data['p4']=$p4;

            $data['est_id']=$this->input->post('estado_id');

            if($this->input->post('estado_id')==0){
                $data['proyectos']=$this->list_ejecucion_fisica_estado_todos($p1,$p2,$p3,$p4,$sql_in,1,$mes_id);
                $data['title']=' Todos los estados';
                $data['municipio']='';
            }
            else{
                $data['proyectos']=$this->list_ejecucion_fisica_estado($p1,$p2,$p3,$p4,$sql_in,$this->input->post('estado_id'),1,$mes_id);
                $data['title']=' Por Estado';
                $municipio = $this->model_taqpacha->get_estado($this->input->post('estado_id'));
                $data['municipio']='<br>ESTADO - '.strtoupper($municipio[0]['ep_descripcion']);
            }
            $this->load->view('reportes/reportes_mae/op_estado', $data);
        }
        else{
            $this->session->set_flashdata('danger','Problemas al realizar la consulta, contactese con el Web Master');
            redirect('reportes_mae/estado');
        }
    }
    public function reporte_acciones_por_estado($p1,$p2,$p3,$p4,$est_id,$mes_id)
    {
        $mess=$this->get_mes($mes_id);
        $mes = $mess[1];
        $dias = date('d');//$mess[2];
        $sql_in='';
        if($p1==1){$tp_1=1;}else{$tp_1=0;}
        if($p2==1){$tp_2=2;}else{$tp_2=0;}
        if($p3==1){$tp_3=3;}else{$tp_3=0;}
        if($p4==1){$tp_4=4;}else{$tp_4=0;}
        $sql_in='in ('.$tp_1.','.$tp_2.','.$tp_3.','.$tp_4.')';
        if($est_id==0){
            $detalles=$this->list_ejecucion_fisica_estado_todos($p1,$p2,$p3,$p4,$sql_in,2,$mes_id);
            $prov_nombre='(TODOS)';
        }
        else{
            $detalles=$this->list_ejecucion_fisica_estado($p1,$p2,$p3,$p4,$sql_in,$est_id,2,$mes_id);
            $provincia = $this->model_taqpacha->get_estado($est_id);
            $prov_nombre=strtoupper($provincia[0]['ep_descripcion']);
        }

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_resumen.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> EJECUCI&Oacute;N DE ACCIONES OPERATIVAS POR ESTADO / '.$prov_nombre.'<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$detalles.'
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("ejecucion_acciones_operativas_estado.pdf", array("Attachment" => false));
    }
    function list_ejecucion_fisica_estado_todos($p1,$p2,$p3,$p4,$sql_in,$tp_rep,$mes_id){

        if($tp_rep==1){
            $tb='id="dt_basic_float" class="table table-bordered"';
        }
        elseif ($tp_rep==2) {
            $tb='border="1" cellpadding="0" cellspacing="0" class="table table table-bordered" width="100%"';
        }

        $nro=0;
        $tabla ='';
        if($tp_rep==1) {
            $tabla .= '<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h3 class="alert alert-success"><center>EJECUCI&Oacute;N DE ACCIONES OPERATIVAS POR ESTADO</center></h3>
                  </article>
                  <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                        <header>
                          <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                          <h2 class="font-md"><strong>TODOS LOS ESTADOS</strong></h2>  
                        </header>
                	<div>
                  <div class="widget-body no-padding">
                  <div class="table-responsive">';
        }
        $estado_proy = $this->mmae->list_estados();

        if(count($estado_proy)!=0){
            $nro++;
            $tabla .='<table '.$tb.'>';
            $tabla .='<thead>';
            $tabla .='<tr><th class="titulo_cabecera" style="width:1%;" rowspan="2">#</th>
				<th class="titulo_cabecera" style="width:15%;" rowspan="2">ESTADO DEL PROYECTO</th>';
            if($p1==1)
                $tabla .='<th class="titulo_cabecera" style="width:15%;" rowspan="2">NRO DE PROYECTOS</th>';
            if($p2==1)
                $tabla .='<th class="titulo_cabecera" style="width:20%;" rowspan="2">NRO DE PROGRAMAS RECURRENTES</th>';
            if($p3==1)
                $tabla .='<th class="titulo_cabecera" style="width:10%;" rowspan="2">NRO DE PROGRAMAS NO RECURRENTES</th>';
            if($p4==1)
                $tabla .='<th class="titulo_cabecera" rowspan="2">NRO. OP. DE FUNCIONAMIENTO </th>';

            $tabla .='<th colspan="5" class="cabecera">PRESUPUESTO GESTI&Oacute;N '.$this->gestion.'</th></tr>';
            $tabla .='<tr class="modo1">';
            $tabla .='<th class="cabecera">INICIAL </th>';
            $tabla .='<th class="cabecera">MODIFICACIONES </th>';
            $tabla .='<th class="cabecera">VIGENTE </th>';
            $tabla .='<th class="cabecera">EJECUTADO </th>';
            $tabla .='<th class="cabecera">% DE EJECUCIÓN PRESUPUESTARIA </th>';
            $tabla .='</tr>';
            $tabla .='</thead>';
            $nrop=0;
            $tabla .='<tbody>';
            $total_pi=0;$total_pr=0;$total_pnr=0;$total_of=0;
            $total_mm_pi=0;$total_mm_pr=0;$total_mm_pnr=0;$total_mm_of=0;
            $total_pemm_pini=0;$total_pemm_pmod=0;$total_pemm_pvig=0;$total_pemm_peje=0;
            $total_gral_pe_pini=0;$total_gral_pe_pmod=0;$total_gral_pe_pvig=0;$total_gral_pe_peje=0;
            foreach($estado_proy as $row_estado)
            {
                $nrop++;
                $tabla .='<tr class="derecha">';
                $tabla .='<td class="centro">'.$nrop.'</td>';
                $tabla .='<td class="izquierda">'.$row_estado['ep_descripcion'].'</td>';

                $proyectos = $this->mmae->get_proyectos_estado($row_estado['ep_id'],$mes_id,$sql_in);
                if(count($proyectos)!=0){
                    $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                    $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
                    foreach($proyectos as $rowp){
                        $total_pe_pini=$total_pe_pini+$rowp['pe_pi'];
                        $total_pe_pmod=$total_pe_pmod+$rowp['pe_pm'];
                        $total_pe_pvig=$total_pe_pvig+$rowp['pe_pv'];
                        $total_pe_peje=$total_pe_peje+$rowp['pe_pe'];
                        if($rowp['pe_pv']!=0){
                            $total_pe_porc=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                        }

                        if($rowp['tp_id']==1){//Proyecto de Inversión
                            $tot_pi=$tot_pi+1;
                        }elseif($rowp['tp_id']==2){//Programa Recurrente
                            $tot_pr=$tot_pr+1;
                        }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                            $tot_pnr=$tot_pnr+1;
                        }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                            $tot_of=$tot_of+1;
                        }
                    }
                }else{
                    $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                    $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
                }

                $total_pe_porc=0;
                if($total_pe_pvig!=0){
                    $total_pe_porc=$total_pe_peje/$total_pe_pvig*100;
                }
                if($p1==1)
                    $tabla .='<td class="pi">'.$tot_pi.'</td>';
                if($p2==1)
                    $tabla .='<td class="pr">'.$tot_pr.'</td>';
                if($p3==1)
                    $tabla .='<td class="pnr">'.$tot_pnr.'</td>';
                if($p4==1)
                    $tabla .='<td class="of">'.$tot_of.'</td>';
                $tabla .='<td>'.number_format($total_pe_pini, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_pmod, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_pvig, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_peje, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_porc, 2, ',', '.').'%</td>';
                $tabla .='</tr>';
                $total_pi =$total_pi+$tot_pi;
                $total_pr =$total_pr+$tot_pr;
                $total_pnr=$total_pnr+$tot_pnr;
                $total_of =$total_of+$tot_of;
                $total_gral_pe_pini=$total_gral_pe_pini+$total_pe_pini;
                $total_gral_pe_pmod=$total_gral_pe_pmod+$total_pe_pmod;
                $total_gral_pe_pvig=$total_gral_pe_pvig+$total_pe_pvig;
                $total_gral_pe_peje=$total_gral_pe_peje+$total_pe_peje;

            }
            $total_gral_pe_porc=0;
            if($total_gral_pe_pvig!=0){$total_gral_pe_porc=$total_gral_pe_peje/$total_gral_pe_pvig*100;}

            $tabla .='</tbody>';
            $tabla .='<tr class="total_general">';
            $tabla .='<td colspan="2" class="izquierda"><b>TOTAL : '.$nrop.'</b></td>';
            if($p1==1)
                $tabla .='<td class="centro">'.$total_pi.'</td>';
            if($p2==1)
                $tabla .='<td class="centro">'.$total_pr.'</td>';
            if($p3==1)
                $tabla .='<td class="centro">'.$total_pnr.'</td>';
            if($p4==1)
                $tabla .='<td class="centro">'.$total_of.'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pini, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pmod, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pvig, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_peje, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_porc, 2, ',', '.').'%</td>';
            $tabla .='</tr>';
            $tabla .='</table>';
            if($tp_rep==1) {
                $tabla .= '</div>
                </div>
              </div>
            </article>';
            }
        }
        return $tabla;
    }
    function list_ejecucion_fisica_estado($p1,$p2,$p3,$p4,$sql_in,$estado_id,$tp_rep,$mes_id){

        if($tp_rep==1){
            $tb='id="dt_basic_float" class="table table-bordered"';
        }
        elseif ($tp_rep==2) {
            $tb='border="1" cellpadding="0" cellspacing="0" class="table table table-bordered" width="100%"';
        }
        $nro=0;
        $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
        $acum_pi=0;$acum_pr=0;$acum_pnr=0;$acum_of=0;
        $acum_pi_fis=0;$acum_pr_fis=0;$acum_pnr_fis=0;$acum_of_fis=0;
        $tot_cost_pi=0;$tot_cost_pr=0;$tot_cost_pnr=0;$tot_cost_of=0;
        $tot_mod_pi=0;$tot_mod_pr=0;$tot_mod_pnr=0;$tot_mod_of=0;
        $tot_ctv_pi=0;$tot_ctv_pr=0;$tot_ctv_pnr=0;$tot_ctv_of=0;
        $tot_epa_pi=0;$tot_epa_pr=0;$tot_epa_pnr=0;$tot_epa_of=0;
        $tot_pi_pi=0;$tot_pi_pr=0;$tot_pi_pnr=0;$tot_pi_of=0;
        $tot_pm_pi=0;$tot_pm_pr=0;$tot_pm_pnr=0;$tot_pm_of=0;
        $tot_pv_pi=0;$tot_pv_pr=0;$tot_pv_pnr=0;$tot_pv_of=0;
        $tot_pe_pi=0;$tot_pe_pr=0;$tot_pe_pnr=0;$tot_pe_of=0;

        $acumulado=0;$acumulado_efis=0;
        $tabla ='';
        $tabla.='<table '.$tb.'>';
        $tabla.='<thead>';
        $tabla.='<tr>
				<th class="titulo_cabecera" style="width:1%;" rowspan="2">Nro.</th>
				<th class="titulo_cabecera" style="width:8%;" rowspan="2">NOMBRE</th>
				<th class="titulo_cabecera" style="width:5%;" rowspan="2">PROVINCIA <br>Y<br>MUNICIPIO(S)</th>
				<th class="titulo_cabecera" style="width:4%;" rowspan="2">TIPO</th>
				<th class="cabecera" colspan="3">COSTO TOTAL DE LA OPERACI&Oacute;N</th>
				<th class="cabecera_acum" colspan="3">EJECUCIÓN ACUMULADA</th>
	    		<th class="cabecera" colspan="5">PRESUPUESTO GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>
	    		<th class="cabecera_acum" colspan="3">EJECUCI&Oacute;N FÍSICA '.$this->session->userdata('gestion').'</th>
	    		<th class="titulo_cabecera" style="width:5%;" rowspan="2">ESTADO</th>
	    		<th class="titulo_cabecera" style="width:6%;" rowspan="2">UNIDAD EJECUTORA</th>
	    		<th class="titulo_cabecera" style="width:6%;" rowspan="2">RESPONSABLE</th>
			</tr>';

        $tabla.='<tr class="modo1">
				<th class="cabecera" style="width:6%;">INICIAL</th>
				<th class="cabecera" style="width:5%;">MODIFICACIONES</th>
				<th class="cabecera" style="width:6%;">VIGENTE</th>
				<th class="cabecera_acum" style="width:5%;">EJECUCIÓN FINANCIERA</th>
				<th class="cabecera_acum" style="width:5%;">% EJEC. FINANCIERA</th>
				<th class="cabecera_acum" style="width:5%;">% DE AVANCE FÍSICO</th>';
        $tabla.='<th class="cabecera" style="width:5%;"> INICIAL </th>';
        $tabla.='<th class="cabecera" style="width:5%;"> MODIFICADO </th>';
        $tabla.='<th class="cabecera" style="width:5%;"> VIGENTE </th>';
        $tabla.='<th class="cabecera" style="width:5%;">EJEC. FINANCIERA </th>';
        $tabla.='<th class="cabecera" style="width:5%;">% EJEC. FINANCIERA </th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">PROG. %</th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">EJEC %</th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">% EJEC. ANUAL</th>';
        $tabla.='</tr>';
        $tabla.='</thead>';
        $tabla.='<tbody>';
        $proy = $this->mmae->get_proyectos_estado($estado_id,$mes_id,$sql_in);
        if(count($proy)!=0){
            $nro=0;
            foreach($proy as $rowp){
                $muni_proy=$this->model_taqpacha->regiones_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                $color_mult='';
                if($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']>1){//Multiregional
                    $color_mult='style="background-color:#EDFBFF"';
                }
                $se=$rowp['pe_pe'];
                $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                    $se=$se+$suma[0]['total_ejecutado'];
                }

                $suma_prog=$this->mdas_complementario->costo_total_proyecto($rowp['pfec_id']);
                $tabla .='<tr class="modo1 derecha" '.$color_mult.'>';
                $nro=$nro+1;
                $tabla .='<td class="centro">'.$nro.'</td>';
                $tabla.='<td class="izquierda"><a href="javascript:abreVentana(\''.site_url("reportes_mae").'/resumen_ejec/'.$rowp['proy_id'].'/'.$mes_id.'\');"'.'>'.$rowp['proy_nombre'].'</a></td>';
                $tabla.='<td class="izquierda">';
                $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);
                if((count($prov)>3) and ($tp_rep == 1)){
                    $tabla .= '<p class="btn btn-default" href="#" class="form_pond" title="';
                }elseif($tp_rep ==2){
                    $tabla .= '<div style="font-size: 5px;">';
                }
                foreach($prov as $row_prov){
                    $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                    if (count($prov) > 3) {
                        if ($tp_rep == 1) {
                            foreach ($muni as $muni) {
                                $tabla .= $row_prov['prov_provincia'] .'&#09;'. $muni['muni_municipio'] . '&#13;';
                            }
                        } else {
                            $tabla .='<ul>'.$row_prov['prov_provincia'];
                            foreach ($muni as $muni) {
                                $tabla .= '<li>'.$muni['muni_municipio'] . '</li>';
                            }
                            $tabla .= '</ul>';
                        }
                    } else {
                        $tabla .= '<ul><strong>'.$row_prov['prov_provincia'].'</strong>';
                        foreach ($muni as $muni) {
                            $tabla .= '<li>'.$muni['muni_municipio'].'</li>';
                        }
                        $tabla .= '</ul>';
                    }
                }
                if((count($prov) > 3) and ($tp_rep == 1)){
                    $tabla .= '"><i class="glyphicon glyphicon-search"></i>Ver...</p>';
                }elseif($tp_rep==2){
                    $tabla .= '</div>';
                }
                $tabla .='</td>';
                $tabla.='<td class="izquierda">'.$rowp['tp_tipo'].'</td>';
                $tabla.='<td>'.number_format($suma_prog[0]['total_programado'], 2, ',', '.').'</td>';
                $toal_modif=$this->mdas_complementario->modificaciones_costo_total($rowp['pfec_id']);
                $total_mod=0;
                if($toal_modif[0]['total_modif']!='' || $toal_modif[0]['total_modif']!=0){
                    $total_mod=$toal_modif[0]['total_modif'];
                }
                $tabla.='<td>'.number_format($total_mod, 2, ',', '.').'</td>';
                $ctv=$suma_prog[0]['total_programado']+$total_mod;
                $tabla.='<td>'.number_format($ctv, 2, ',', '.').'</td>';
                $tabla.='<td>'.number_format($se, 2, ',', '.').'</td>';

                $efin=0;
                if($suma_prog[0]['total_programado']!=0){
                    $efin=round((($se/$suma_prog[0]['total_programado'])*100),2);
                }
                $tabla.='<td>'.$efin.'%</td>';

                $efis=$this->avance_fisico($rowp['proy_id'],$mes_id);
                $acumulado_efis=$acumulado_efis+$efis;
                $tabla.='<td>'.round($efis,2).'%</td>';

                $tabla.='<td>'.number_format($rowp['pe_pi'], 2, ',', '.').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pm'], 2, ',', '.').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pv'], 2, ',', '.').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pe'], 2, ',', '.').'</td>';

                $ejec=0;
                if($rowp['pe_pv']!=0){
                    $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                }
                $tabla.='<td>'.$ejec.'%</td>';
                $estado=$this->mdas_complementario->estado_operacion($rowp['pfec_id'],$mes_id,$this->gestion);
                $estado_proy='No registrado';
                if(count($estado)!=0){
                    $estado_proy=$estado[0]['ep_descripcion'];
                }
                $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));

                $tabla .= '<td class="derecha">'.$ejecutado[0].'</td>';
                $tabla .= '<td class="derecha">'.$ejecutado[1].'</td>';
                $tabla .= '<td class="derecha">'.$ejecutado[2].'</td>';
                $acumulado=$acumulado+$ejecutado[2];
                $tabla.='<td class="izquierda">'.$estado_proy.'</td>';
                $tabla.='<td class="izquierda">'.$rowp['uni_unidad'].'</td>';
                $tabla.='<td class="izquierda">'.$rowp['responsable'].'<br><font color="red">Telf: '.$rowp['fun_telefono'].'</font></td>';
                $tabla.='</tr>';
                if($rowp['tp_id']==1){//Proyecto de Inversión
                    $tot_pi=$tot_pi+1;
                    $acum_pi=$acum_pi+$ejecutado[2];
                    $acum_pi_fis=$acum_pi_fis+$efis;
                    $tot_cost_pi=$tot_cost_pi+$suma_prog[0]['total_programado'];
                    $tot_mod_pi=$tot_mod_pi+$total_mod;
                    $tot_ctv_pi=$tot_ctv_pi+$ctv;
                    $tot_epa_pi=$tot_epa_pi+$se;
                    $tot_pi_pi=$tot_pi_pi+$rowp['pe_pi'];
                    $tot_pm_pi=$tot_pm_pi+$rowp['pe_pm'];
                    $tot_pv_pi=$tot_pv_pi+$rowp['pe_pv'];
                    $tot_pe_pi=$tot_pe_pi+$rowp['pe_pe'];
                }elseif($rowp['tp_id']==2){//Programa Recurrente
                    $tot_pr=$tot_pr+1;
                    $acum_pr=$acum_pr+$ejecutado[2];
                    $acum_pr_fis=$acum_pr_fis+$efis;
                    $tot_cost_pr=$tot_cost_pr+$suma_prog[0]['total_programado'];
                    $tot_mod_pr=$tot_mod_pr+$total_mod;
                    $tot_ctv_pr=$tot_ctv_pr+$ctv;
                    $tot_epa_pr=$tot_epa_pr+$se;
                    $tot_pi_pr=$tot_pi_pr+$rowp['pe_pi'];
                    $tot_pm_pr=$tot_pm_pr+$rowp['pe_pm'];
                    $tot_pv_pr=$tot_pv_pr+$rowp['pe_pv'];
                    $tot_pe_pr=$tot_pe_pr+$rowp['pe_pe'];
                }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                    $tot_pnr=$tot_pnr+1;
                    $acum_pnr=$acum_pnr+$ejecutado[2];
                    $acum_pnr_fis=$acum_pnr_fis+$efis;
                    $tot_cost_pnr=$tot_cost_pnr+$suma_prog[0]['total_programado'];
                    $tot_mod_pnr=$tot_mod_pnr+$total_mod;
                    $tot_ctv_pnr=$tot_ctv_pnr+$ctv;
                    $tot_epa_pnr=$tot_epa_pnr+$se;
                    $tot_pi_pnr=$tot_pi_pnr+$rowp['pe_pi'];
                    $tot_pm_pnr=$tot_pm_pnr+$rowp['pe_pm'];
                    $tot_pv_pnr=$tot_pv_pnr+$rowp['pe_pv'];
                    $tot_pe_pnr=$tot_pe_pnr+$rowp['pe_pe'];
                }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                    $tot_of=$tot_of+1;
                    $acum_of=$acum_of+$ejecutado[2];
                    $acum_of_fis=$acum_of_fis+$efis;
                    $tot_cost_of=$tot_cost_of+$suma_prog[0]['total_programado'];
                    $tot_mod_of=$tot_mod_of+$total_mod;
                    $tot_ctv_of=$tot_ctv_of+$ctv;
                    $tot_epa_of=$tot_epa_of+$se;
                    $tot_pi_of=$tot_pi_of+$rowp['pe_pi'];
                    $tot_pm_of=$tot_pm_of+$rowp['pe_pm'];
                    $tot_pv_of=$tot_pv_of+$rowp['pe_pv'];
                    $tot_pe_of=$tot_pe_of+$rowp['pe_pe'];
                }

            }
        }
        $tot_porc_pi=0;$tot_porc_pnr=0;$tot_porc_pr=0;$tot_porc_of=0;
        if($tot_pv_pi!=0){$tot_porc_pi  =$tot_pe_pi/$tot_pv_pi*100;}
        if($tot_pv_pnr!=0){$tot_porc_pnr=$tot_pe_pnr/$tot_pv_pnr*100;}
        if($tot_pv_pr!=0){$tot_porc_pr  =$tot_pe_pr/$tot_pv_pr*100;}
        if($tot_pv_of!=0){$tot_porc_of  =$tot_pe_of/$tot_pv_of*100;}

        $tot_pora_pi=0;$tot_pora_pnr=0;$tot_pora_pr=0;$tot_pora_of=0;
        if($tot_ctv_pi!=0){$tot_pora_pi=$tot_epa_pi/$tot_ctv_pi*100;}
        if($tot_ctv_pnr!=0){$tot_pora_pnr=$tot_epa_pnr/$tot_ctv_pnr*100;}
        if($tot_ctv_pr!=0){$tot_pora_pr=$tot_epa_pr/$tot_ctv_pr*100;}
        if($tot_ctv_of!=0){$tot_pora_of=$tot_epa_of/$tot_ctv_of*100;}

        $tabla .='</tbody>';
        if($p1==1) {
            $total_acum_pi=0;$total_acum_pi_fis=0;
            if($tot_pi!=0){
                $total_acum_pi=number_format($acum_pi/$tot_pi,2,',','.');
                $total_acum_pi_fis=number_format($acum_pi_fis/$tot_pi,2,',','.');
            }
            $tabla .= '<tr class="total_pi">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROYECTOS DE INVERSIÓN</td>';
            $tabla .= '<th >' . $tot_pi . '</th><th class="derecha">' . number_format($tot_cost_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pi,2,',','.') . ' %</th><th class="derecha">'.$total_acum_pi_fis.'</th>
                        <th class="derecha">' . number_format($tot_pi_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pi,2,',','.') . '%</th><th ></th><th ></th><th class="derecha">'.$total_acum_pi.'</th><th colspan="3"></th></tr>';
        }
        if($p2==1) {
            $total_acum_pr=0;$total_acum_pr_fis=0;
            if($tot_pr!=0){
                $total_acum_pr=number_format($acum_pr/$tot_pr,2,',','.');
                $total_acum_pr_fis=number_format($acum_pr_fis/$tot_pr,2,',','.');
            }
            $tabla .= '<tr class="total_pr">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROGRAMAS RECURRENTES</td>';
            $tabla .= '<th >' . $tot_pr . '</th><th class="derecha">' . number_format($tot_cost_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pr,2,',','.') . ' %</th><th class="derecha">'.$total_acum_pr_fis.'</th>
                         <th class="derecha">' . number_format($tot_pi_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pr,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_pr.'</th><th colspan="3"></th></tr>';
        }
        if($p3==1) {
            $total_acum_pnr=0;$total_acum_prn_fis=0;
            if($tot_pnr!=0){
                $total_acum_pnr=number_format($acum_pnr/$tot_pnr,2,',','.');
                $total_acum_prn_fis=number_format($acum_pnr_fis/$tot_pnr,2,',','.');
            }
            $tabla .= '<tr class="total_pnr">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROGRAMAS NO RECURRENTES</td>';
            $tabla .= '<th >' . $tot_pnr . '</th><th class="derecha">' . number_format($tot_cost_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pnr,2,',','.') . ' %</th><th class="derecha">'.$total_acum_prn_fis.'</th>
                         <th class="derecha">' . number_format($tot_pi_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pnr,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_pnr.'</th><th colspan="3"></th></tr>';
        }
        if($p4==1) {
            $total_acum_of=0;$total_acum_of_fis=0;
            if($tot_of!=0){
                $total_acum_of=number_format($acum_of/$tot_of,2,',','.');
                $total_acum_of_fis=number_format($acum_of_fis/$tot_of,2,',','.');
            }
            $tabla .= '<tr class="total_of">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL OPERACIONES DE FUNCIONAMIENTO</td>';
            $tabla .= '<th >' . $tot_of . '</th><th class="derecha">' . number_format($tot_cost_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_of,2,',','.') . ' %</th><th class="derecha">'.$total_acum_of_fis.'</th>
                         <th class="derecha">' . number_format($tot_pi_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_of,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_of.'</th><th colspan="3"></th></tr>';
        }

        $total_gral_op  =$tot_pi+$tot_pnr+$tot_pr+$tot_of;
        $total_gral_cost=$tot_cost_pi+$tot_cost_pnr+$tot_cost_pr+$tot_cost_of;
        $total_gral_mod =$tot_mod_pi+$tot_mod_pnr+$tot_mod_pr+$tot_mod_of;
        $total_gral_ctv =$tot_ctv_pi+$tot_ctv_pnr+$tot_ctv_pr+$tot_ctv_of;
        $total_gral_epa =$tot_epa_pi+$tot_epa_pnr+$tot_epa_pr+$tot_epa_of;
        $total_gral_pora=0;
        if($total_gral_ctv!=0){$total_gral_pora=$total_gral_epa/$total_gral_ctv*100;}

        $total_gral_pi  =$tot_pi_pi+$tot_pi_pnr+$tot_pi_pr+$tot_pi_of;
        $total_gral_pm  =$tot_pm_pi+$tot_pm_pnr+$tot_pm_pr+$tot_pm_of;
        $total_gral_pv  =$tot_pv_pi+$tot_pv_pnr+$tot_pv_pr+$tot_pv_of;
        $total_gral_pe  =$tot_pe_pi+$tot_pe_pnr+$tot_pe_pr+$tot_pe_of;
        $total_gral_porc=0;
        if($total_gral_pv!=0){$total_gral_porc=$total_gral_pe/$total_gral_pv*100;}
        $total_acum=0;$total_acum_fis=0;
        if($total_gral_op!=0){
            $total_acum=number_format($acumulado/$total_gral_op, 2, ',', '.');
            $total_acum_fis=number_format($acumulado_efis/$total_gral_op, 2, ',', '.');
        }
        $tabla.='<tr class="total_general">';
        $tabla.='<th colspan="3" class="izquierda">TOTAL POR ESTADO</td>';
        $tabla.='<th >'.$total_gral_op.'</th><th class="derecha">'.number_format($total_gral_cost, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_mod, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_ctv, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_epa, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pora, 2, ',', '.').' %</th><th class="derecha" >'.$total_acum_fis.'</th>
                 <th class="derecha">'.number_format($total_gral_pi, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pm, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pv, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pe, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_porc, 2, ',', '.').' %</th><th ></th><th ></th><th class="derecha">'.$total_acum.'</th><th colspan="3"></th></tr>';

        $tabla .='</table><br>';
        return $tabla;
    }
    /*----------------------------------- Ejecución de acciones operativas por sector ----------------------------*/
    public function acciones_operativas_por_sector(){
        $data['menu']=$this->menu(10);

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['cod'] = $this->mmae->list_sectores();
        $data['meses'] = $this->model_ejecucion->list_meses(); /// Lista de Meses
        $this->load->view('reportes/reportes_mae/select_opciones_sector', $data);
    }
    public function valida_acciones_operativas_por_sector(){
        if($this->input->server('REQUEST_METHOD') === 'POST'){
            $sql_in='';
            if($this->input->post('p1')=='on'){$p1=1;$tp_1=1;}else{$p1=0;$tp_1=0;}
            if($this->input->post('p2')=='on'){$p2=1;$tp_2=2;}else{$p2=0;$tp_2=0;}
            if($this->input->post('p3')=='on'){$p3=1;$tp_3=3;}else{$p3=0;$tp_3=0;}
            if($this->input->post('p4')=='on'){$p4=1;$tp_4=4;}else{$p4=0;$tp_4=0;}
            $sql_in='in ('.$tp_1.','.$tp_2.','.$tp_3.','.$tp_4.')';

            $enlaces = $this->menu_modelo->get_Modulos(10);
            $data['enlaces'] = $enlaces;
            for ($i = 0; $i < count($enlaces); $i++) {
                $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;

            $meses=$this->get_mes($this->input->post('meses_id'));
            $mes = $meses[1];
            $dias = $meses[2];
            $mes_id=$this->input->post('meses_id');
            $data['id_mes'] = $mes_id;
            $data['mes'] = $mes;
            $data['dias'] = $dias;

            $pr1='';$pr2='';$pr3='';$pr4='';
            if($p1==1){$pr1='<span class="pi_t"> PROYECTOS DE INVERSI&Oacute;N</span>';}
            if($p2==1){$pr2=' - <span class="pr_t">PROGRAMA RECURRENTE</span>';}
            if($p3==1){$pr3=' - <span class="pnr_t">PROGRAMA NO RECURRENTE</span>';}
            if($p4==1){$pr4=' - <span class="of_t">OPERACI&Oacute;N DE FUNCIONAMIENTO</span>';}

            $data['pr1']=$pr1;
            $data['pr2']=$pr2;
            $data['pr3']=$pr3;
            $data['pr4']=$pr4;

            $data['p1']=$p1;
            $data['p2']=$p2;
            $data['p3']=$p3;
            $data['p4']=$p4;

            $data['est_id']=$this->input->post('cod_sector');

            if($this->input->post('cod_sector')==-1){
                $data['proyectos']=$this->list_ejecucion_fisica_sector_todos($p1,$p2,$p3,$p4,$sql_in,1,$mes_id);
                $data['title']=' Todos los Sectores';
                $data['municipio']='';
            }
            else{
                $data['proyectos']=$this->list_ejecucion_fisica_sector($p1,$p2,$p3,$p4,$sql_in,$this->input->post('cod_sector'),1,$mes_id);
                $data['title']=' Por Sectore Económico';
                $municipio = $this->mmae->get_sector($this->input->post('cod_sector'));
                $data['municipio']='<br>SECTOR - '.strtoupper($municipio[0]['sector']);
            }

            $this->load->view('reportes/reportes_mae/op_sector', $data);
        }
        else{
            $this->session->set_flashdata('danger','Problemas al realizar la consulta, contactese con el Web Master');
            redirect('reportes_mae/sector');
        }
    }
    public function reporte_acciones_por_sector($p1,$p2,$p3,$p4,$est_id,$mes_id)
    {
        $mess=$this->get_mes($mes_id);
        $mes = $mess[1];
        $dias = date('d');//$mess[2];
        $sql_in='';
        if($p1==1){$tp_1=1;}else{$tp_1=0;}
        if($p2==1){$tp_2=2;}else{$tp_2=0;}
        if($p3==1){$tp_3=3;}else{$tp_3=0;}
        if($p4==1){$tp_4=4;}else{$tp_4=0;}
        $sql_in='in ('.$tp_1.','.$tp_2.','.$tp_3.','.$tp_4.')';
        if($est_id==-1){
            $detalles=$this->list_ejecucion_fisica_sector_todos($p1,$p2,$p3,$p4,$sql_in,2,$mes_id);
            $prov_nombre='(TODOS)';
        }
        else{
            $detalles=$this->list_ejecucion_fisica_sector($p1,$p2,$p3,$p4,$sql_in,$est_id,2,$mes_id);
            $provincia = $this->mmae->get_sector($est_id);
            $prov_nombre=strtoupper($provincia[0]['sector']);
        }

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_resumen.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> EJECUCI&Oacute;N DE ACCIONES OPERATIVAS POR SECTOR / '.$prov_nombre.'<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$detalles.'
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("ejecucion_acciones_operativas_estado.pdf", array("Attachment" => false));
    }
    function list_ejecucion_fisica_sector_todos($p1,$p2,$p3,$p4,$sql_in,$tp_rep,$mes_id){

        if($tp_rep==1){
            $tb='id="dt_basic_float" class="table table-bordered"';
        }
        elseif ($tp_rep==2) {
            $tb='border="1" cellpadding="0" cellspacing="0" class="table table table-bordered" width="100%"';
        }

        $nro=0;
        $tabla ='';
        if($tp_rep==1) {
            $tabla .= '<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h3 class="alert alert-success"><center>EJECUCI&Oacute;N DE ACCIONES OPERATIVAS POR SECTOR</center></h3>
                  </article>
                  <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                        <header>
                          <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                          <h2 class="font-md"><strong>TODOS LOS SECTORES</strong></h2>  
                        </header>
                	<div>
                  <div class="widget-body no-padding">
                  <div class="table-responsive">';
        }
        $sector_eco = $this->mmae->list_sectores();

        if(count($sector_eco)!=0){
            $nro++;
            $tabla .='<table '.$tb.'>';
            $tabla .='<thead>';
            $tabla .='<tr><th class="titulo_cabecera" style="width:1%;" rowspan="2">#</th>
				<th class="titulo_cabecera" style="width:15%;" rowspan="2">SECTOR ECONÓMICO</th>';
            if($p1==1)
                $tabla .='<th class="titulo_cabecera" style="width:15%;" rowspan="2">NRO DE PROYECTOS</th>';
            if($p2==1)
                $tabla .='<th class="titulo_cabecera" style="width:20%;" rowspan="2">NRO DE PROGRAMAS RECURRENTES</th>';
            if($p3==1)
                $tabla .='<th class="titulo_cabecera" style="width:10%;" rowspan="2">NRO DE PROGRAMAS NO RECURRENTES</th>';
            if($p4==1)
                $tabla .='<th class="titulo_cabecera" rowspan="2">NRO. OP. DE FUNCIONAMIENTO </th>';
            $tabla .='<th colspan="5" class="cabecera">PRESUPUESTO GESTI&Oacute;N '.$this->gestion.'</th></tr>';
            $tabla .='<tr class="modo1">';
            $tabla .='<th class="cabecera">INICIAL </th>';
            $tabla .='<th class="cabecera">MODIFICACIONES </th>';
            $tabla .='<th class="cabecera">VIGENTE </th>';
            $tabla .='<th class="cabecera">EJECUTADO </th>';
            $tabla .='<th class="cabecera">% DE EJECUCIÓN PRESUPUESTARIA </th>';
            $tabla .='</tr>';
            $tabla .='</thead>';
            $nrop=0;
            $tabla .='<tbody>';
            $total_pi=0;$total_pr=0;$total_pnr=0;$total_of=0;
            $total_mm_pi=0;$total_mm_pr=0;$total_mm_pnr=0;$total_mm_of=0;
            $total_pemm_pini=0;$total_pemm_pmod=0;$total_pemm_pvig=0;$total_pemm_peje=0;
            $total_gral_pe_pini=0;$total_gral_pe_pmod=0;$total_gral_pe_pvig=0;$total_gral_pe_peje=0;
            foreach($sector_eco as $row_sector)
            {
                $nrop++;
                $tabla .='<tr class="derecha">';
                $tabla .='<td class="centro">'.$nrop.'</td>';
                $tabla .='<td class="izquierda">'.$row_sector['sector'].'</td>';

                $proyectos = $this->mmae->get_proyectos_sector($row_sector['codsectorial'],$mes_id,$sql_in);
                if(count($proyectos)!=0){
                    $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                    $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
                    foreach($proyectos as $rowp){
                        $total_pe_pini=$total_pe_pini+$rowp['pe_pi'];
                        $total_pe_pmod=$total_pe_pmod+$rowp['pe_pm'];
                        $total_pe_pvig=$total_pe_pvig+$rowp['pe_pv'];
                        $total_pe_peje=$total_pe_peje+$rowp['pe_pe'];
                        if($rowp['pe_pv']!=0){
                            $total_pe_porc=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                        }

                        if($rowp['tp_id']==1){//Proyecto de Inversión
                            $tot_pi=$tot_pi+1;
                        }elseif($rowp['tp_id']==2){//Programa Recurrente
                            $tot_pr=$tot_pr+1;
                        }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                            $tot_pnr=$tot_pnr+1;
                        }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                            $tot_of=$tot_of+1;
                        }
                    }
                }else{
                    $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
                    $total_pe_pini=0;$total_pe_pmod=0;$total_pe_pvig=0;$total_pe_peje=0;$total_pe_porc=0;
                }

                $total_pe_porc=0;
                if($total_pe_pvig!=0){
                    $total_pe_porc=$total_pe_peje/$total_pe_pvig*100;
                }
                if($p1==1)
                    $tabla .='<td class="pi">'.$tot_pi.'</td>';
                if($p2==1)
                    $tabla .='<td class="pr">'.$tot_pr.'</td>';
                if($p3==1)
                    $tabla .='<td class="pnr">'.$tot_pnr.'</td>';
                if($p4==1)
                    $tabla .='<td class="of">'.$tot_of.'</td>';
                $tabla .='<td>'.number_format($total_pe_pini, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_pmod, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_pvig, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_peje, 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($total_pe_porc, 2, ',', '.').'%</td>';
                $tabla .='</tr>';
                $total_pi =$total_pi+$tot_pi;
                $total_pr =$total_pr+$tot_pr;
                $total_pnr=$total_pnr+$tot_pnr;
                $total_of =$total_of+$tot_of;
                $total_gral_pe_pini=$total_gral_pe_pini+$total_pe_pini;
                $total_gral_pe_pmod=$total_gral_pe_pmod+$total_pe_pmod;
                $total_gral_pe_pvig=$total_gral_pe_pvig+$total_pe_pvig;
                $total_gral_pe_peje=$total_gral_pe_peje+$total_pe_peje;

            }
            $total_gral_pe_porc=0;
            if($total_gral_pe_pvig!=0){$total_gral_pe_porc=$total_gral_pe_peje/$total_gral_pe_pvig*100;}

            $tabla .='</tbody>';
            $tabla .='<tr class="total_general">';
            $tabla .='<td colspan="2" class="izquierda"><b>TOTAL : '.$nrop.'</b></td>';
            if($p1==1)
                $tabla .='<td><b>'.$total_pi.'</b></td>';
            if($p2==1)
                $tabla .='<td><b>'.$total_pr.'</b></td>';
            if($p3==1)
                $tabla .='<td><b>'.$total_pnr.'</b></td>';
            if($p4==1)
                $tabla .='<td><b>'.$total_of.'</b></td>';
            $tabla .='<td>'.number_format($total_gral_pe_pini, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pmod, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_pvig, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_peje, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($total_gral_pe_porc, 2, ',', '.').'%</td>';
            $tabla .='</tr>';
            $tabla .='</table>';
            if($tp_rep==1) {
                $tabla .= '</div>
                </div>
              </div>
            </article>';
            }
        }
        return $tabla;
    }
    function list_ejecucion_fisica_sector($p1,$p2,$p3,$p4,$sql_in,$cod_sector,$tp_rep,$mes_id){

        if($tp_rep==1){
            $tb='id="dt_basic_float" class="table table-bordered"';
        }
        elseif ($tp_rep==2) {
            $tb='border="1" cellpadding="0" cellspacing="0" class="table table table-bordered" width="100%"';
        }

        $nro=0;
        $tot_pi=0;$tot_pr=0;$tot_pnr=0;$tot_of=0;
        $acum_pi=0;$acum_pr=0;$acum_pnr=0;$acum_of=0;
        $acum_pi_fis=0;$acum_pr_fis=0;$acum_pnr_fis=0;$acum_of_fis=0;
        $tot_cost_pi=0;$tot_cost_pr=0;$tot_cost_pnr=0;$tot_cost_of=0;
        $tot_mod_pi=0;$tot_mod_pr=0;$tot_mod_pnr=0;$tot_mod_of=0;
        $tot_ctv_pi=0;$tot_ctv_pr=0;$tot_ctv_pnr=0;$tot_ctv_of=0;
        $tot_epa_pi=0;$tot_epa_pr=0;$tot_epa_pnr=0;$tot_epa_of=0;
        $tot_pi_pi=0;$tot_pi_pr=0;$tot_pi_pnr=0;$tot_pi_of=0;
        $tot_pm_pi=0;$tot_pm_pr=0;$tot_pm_pnr=0;$tot_pm_of=0;
        $tot_pv_pi=0;$tot_pv_pr=0;$tot_pv_pnr=0;$tot_pv_of=0;
        $tot_pe_pi=0;$tot_pe_pr=0;$tot_pe_pnr=0;$tot_pe_of=0;

        $acumulado=0;$acumulado_efis=0;
        $tabla ='';
        $tabla.='<table '.$tb.'>';
        $tabla.='<thead>';
        $tabla.='<tr>
				<th class="titulo_cabecera" style="width:1%;" rowspan="2">Nro.</th>
				<th class="titulo_cabecera" style="width:8%;" rowspan="2">NOMBRE</th>
				<th class="titulo_cabecera" style="width:5%;" rowspan="2">PROVINCIA <br>Y<br>MUNICIPIO(S)</th>
				<th class="titulo_cabecera" style="width:4%;" rowspan="2">TIPO</th>
				<th class="cabecera" colspan="3">COSTO TOTAL DE LA OPERACI&Oacute;N</th>
				<th class="cabecera_acum" colspan="3">EJECUCIÓN ACUMULADA</th>
	    		<th class="cabecera" colspan="5">PRESUPUESTO GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>
	    		<th class="cabecera_acum" colspan="3">EJECUCI&Oacute;N FÍSICA '.$this->session->userdata('gestion').'</th>
	    		<th class="titulo_cabecera" style="width:5%;" rowspan="2">ESTADO</th>
	    		<th class="titulo_cabecera" style="width:6%;" rowspan="2">UNIDAD EJECUTORA</th>
	    		<th class="titulo_cabecera" style="width:6%;" rowspan="2">RESPONSABLE</th>
			</tr>';
        $tabla.='<tr class="modo1">
				<th class="cabecera" style="width:6%;">INICIAL</th>
				<th class="cabecera" style="width:5%;">MODIFICACIONES</th>
				<th class="cabecera" style="width:6%;">VIGENTE</th>
				<th class="cabecera_acum" style="width:5%;">EJECUCIÓN FINANCIERA</th>
				<th class="cabecera_acum" style="width:5%;">% EJEC. FINANCIERA</th>
				<th class="cabecera_acum" style="width:5%;">% DE AVANCE FÍSICO</th>';
        $tabla.='<th class="cabecera" style="width:5%;"> INICIAL </th>';
        $tabla.='<th class="cabecera" style="width:5%;"> MODIFICADO </th>';
        $tabla.='<th class="cabecera" style="width:5%;"> VIGENTE </th>';
        $tabla.='<th class="cabecera" style="width:5%;">EJEC. FINANCIERA </th>';
        $tabla.='<th class="cabecera" style="width:5%;">% EJEC. FINANCIERA </th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">PROG. %</th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">EJEC %</th>';
        $tabla.='<th class="cabecera_acum" style="width:5%;">% EJEC. ANUAL</th>';
        $tabla.='</tr>';
        $tabla.='</thead>';
        $tabla.='<tbody>';


        $proy = $this->mmae->get_proyectos_sector($cod_sector,$mes_id,$sql_in);
        if(count($proy)!=0){
            $nro=0;
            foreach($proy as $rowp){
                $muni_proy=$this->model_taqpacha->regiones_en_proyecto($rowp['proy_id'],$this->gestion,$mes_id);
                $color_mult='';
                if($muni_proy[0]['total_muni']!='' && $muni_proy[0]['total_muni']>1){//Multiregional
                    $color_mult='style="background-color:#EDFBFF"';
                }
                $se=$rowp['pe_pe'];
                $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
                if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                    $se=$se+$suma[0]['total_ejecutado'];
                }

                $suma_prog=$this->mdas_complementario->costo_total_proyecto($rowp['pfec_id']);
                $tabla .='<tr class="modo1 derecha" '.$color_mult.'>';
                $nro=$nro+1;
                $tabla .='<td class="centro">'.$nro.'</td>';
                $tabla.='<td class="izquierda"><a href="javascript:abreVentana(\''.site_url("reportes_mae").'/resumen_ejec/'.$rowp['proy_id'].'/'.$mes_id.'\');"'.'>'.$rowp['proy_nombre'].'</a></td>';
                $tabla.='<td class="izquierda">';
                $prov = $this->mmae->proyecto_provincia($rowp['proy_id']);
                if((count($prov)>3) and ($tp_rep == 1)){
                    $tabla .= '<p class="btn btn-default" href="#" class="form_pond" title="';
                }elseif($tp_rep ==2){
                    $tabla .= '<div style="font-size: 5px;">';
                }
                foreach($prov as $row_prov){
                    $muni = $this->mmae->proyecto_municipio($rowp['proy_id'],$row_prov['prov_id']);
                    if (count($prov) > 3) {
                        if ($tp_rep == 1) {
                            foreach ($muni as $muni) {
                                $tabla .= $row_prov['prov_provincia'] .'&#09;'. $muni['muni_municipio'] . '&#13;';
                            }
                        } else {
                            $tabla .='<ul>'.$row_prov['prov_provincia'];
                            foreach ($muni as $muni) {
                                $tabla .= '<li>'.$muni['muni_municipio'] . '</li>';
                            }
                            $tabla .= '</ul>';
                        }
                    } else {
                        $tabla .= '<ul><strong>'.$row_prov['prov_provincia'].'</strong>';
                        foreach ($muni as $muni) {
                            $tabla .= '<li>'.$muni['muni_municipio'].'</li>';
                        }
                        $tabla .= '</ul>';
                    }
                }
                if((count($prov) > 3) and ($tp_rep == 1)){
                    $tabla .= '"><i class="glyphicon glyphicon-search"></i>Ver...</p>';
                }elseif($tp_rep==2){
                    $tabla .= '</div>';
                }
                $tabla .='</td>';
                $tabla.='<td class="izquierda">'.$rowp['tp_tipo'].'</td>';
                $tabla.='<td>'.number_format($suma_prog[0]['total_programado'], 2, ',', '.').'</td>';
                $toal_modif=$this->mdas_complementario->modificaciones_costo_total($rowp['pfec_id']);
                $total_mod=0;
                if($toal_modif[0]['total_modif']!='' || $toal_modif[0]['total_modif']!=0){
                    $total_mod=$toal_modif[0]['total_modif'];
                }
                $tabla.='<td>'.number_format($total_mod, 2, ',', '.').'</td>';
                $ctv=$suma_prog[0]['total_programado']+$total_mod;
                $tabla.='<td>'.number_format($ctv, 2, ',', '.').'</td>';
                $tabla.='<td>'.number_format($se, 2, ',', '.').'</td>';

                $efin=0;
                if($suma_prog[0]['total_programado']!=0){
                    $efin=round((($se/$suma_prog[0]['total_programado'])*100),2);
                }
                $tabla.='<td>'.$efin.'%</td>';

                $efis=$this->avance_fisico($rowp['proy_id'],$mes_id);
                $acumulado_efis=$acumulado_efis+$efis;
                $tabla.='<td>'.round($efis,2).'%</td>';
                $tabla.='<td>'.number_format($rowp['pe_pi'], 2, ',', '.').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pm'], 2, ',', '.').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pv'], 2, ',', '.').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pe'], 2, ',', '.').'</td>';

                $ejec=0;
                if($rowp['pe_pv']!=0){
                    $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                }
                $tabla.='<td>'.$ejec.'%</td>';
                $estado=$this->mdas_complementario->estado_operacion($rowp['pfec_id'],$mes_id,$this->gestion);
                $estado_proy='No registrado';
                if(count($estado)!=0){
                    $estado_proy=$estado[0]['ep_descripcion'];
                }
                $ejecutado=explode("|",$this->progr_ejec_fisica_acumulada(1,$rowp['pfec_id'],$rowp['proy_id']));

                $tabla .= '<td class="derecha">'.$ejecutado[0].'</td>';
                $tabla .= '<td class="derecha">'.$ejecutado[1].'</td>';
                $tabla .= '<td class="derecha">'.$ejecutado[2].'</td>';
                $acumulado=$acumulado+$ejecutado[2];
                $tabla.='<td class="izquierda">'.$estado_proy.'</td>';
                $tabla.='<td class="izquierda">'.$rowp['uni_unidad'].'</td>';
                $tabla.='<td class="izquierda">'.$rowp['responsable'].'<br><font color="red">Telf: '.$rowp['fun_telefono'].'</font></td>';
                $tabla.='</tr>';

                if($rowp['tp_id']==1){//Proyecto de Inversión
                    $tot_pi=$tot_pi+1;
                    $acum_pi=$acum_pi+$ejecutado[2];
                    $acum_pi_fis=$acum_pi_fis+$efis;
                    $tot_cost_pi=$tot_cost_pi+$suma_prog[0]['total_programado'];
                    $tot_mod_pi=$tot_mod_pi+$total_mod;
                    $tot_ctv_pi=$tot_ctv_pi+$ctv;
                    $tot_epa_pi=$tot_epa_pi+$se;
                    $tot_pi_pi=$tot_pi_pi+$rowp['pe_pi'];
                    $tot_pm_pi=$tot_pm_pi+$rowp['pe_pm'];
                    $tot_pv_pi=$tot_pv_pi+$rowp['pe_pv'];
                    $tot_pe_pi=$tot_pe_pi+$rowp['pe_pe'];
                }elseif($rowp['tp_id']==2){//Programa Recurrente
                    $tot_pr=$tot_pr+1;
                    $acum_pr=$acum_pr+$ejecutado[2];
                    $acum_pr_fis=$acum_pr_fis+$efis;
                    $tot_cost_pr=$tot_cost_pr+$suma_prog[0]['total_programado'];
                    $tot_mod_pr=$tot_mod_pr+$total_mod;
                    $tot_ctv_pr=$tot_ctv_pr+$ctv;
                    $tot_epa_pr=$tot_epa_pr+$se;
                    $tot_pi_pr=$tot_pi_pr+$rowp['pe_pi'];
                    $tot_pm_pr=$tot_pm_pr+$rowp['pe_pm'];
                    $tot_pv_pr=$tot_pv_pr+$rowp['pe_pv'];
                    $tot_pe_pr=$tot_pe_pr+$rowp['pe_pe'];
                }elseif($rowp['tp_id']==3){//Programa No  Recurrente
                    $tot_pnr=$tot_pnr+1;
                    $acum_pnr=$acum_pnr+$ejecutado[2];
                    $acum_pnr_fis=$acum_pnr_fis+$efis;
                    $tot_cost_pnr=$tot_cost_pnr+$suma_prog[0]['total_programado'];
                    $tot_mod_pnr=$tot_mod_pnr+$total_mod;
                    $tot_ctv_pnr=$tot_ctv_pnr+$ctv;
                    $tot_epa_pnr=$tot_epa_pnr+$se;
                    $tot_pi_pnr=$tot_pi_pnr+$rowp['pe_pi'];
                    $tot_pm_pnr=$tot_pm_pnr+$rowp['pe_pm'];
                    $tot_pv_pnr=$tot_pv_pnr+$rowp['pe_pv'];
                    $tot_pe_pnr=$tot_pe_pnr+$rowp['pe_pe'];
                }elseif($rowp['tp_id']==4){//Operacion de Funcionamiento
                    $tot_of=$tot_of+1;
                    $acum_of=$acum_of+$ejecutado[2];
                    $acum_of_fis=$acum_of_fis+$efis;
                    $tot_cost_of=$tot_cost_of+$suma_prog[0]['total_programado'];
                    $tot_mod_of=$tot_mod_of+$total_mod;
                    $tot_ctv_of=$tot_ctv_of+$ctv;
                    $tot_epa_of=$tot_epa_of+$se;
                    $tot_pi_of=$tot_pi_of+$rowp['pe_pi'];
                    $tot_pm_of=$tot_pm_of+$rowp['pe_pm'];
                    $tot_pv_of=$tot_pv_of+$rowp['pe_pv'];
                    $tot_pe_of=$tot_pe_of+$rowp['pe_pe'];
                }

            }
        }
        $tot_porc_pi=0;$tot_porc_pnr=0;$tot_porc_pr=0;$tot_porc_of=0;
        if($tot_pv_pi!=0){$tot_porc_pi  =$tot_pe_pi/$tot_pv_pi*100;}
        if($tot_pv_pnr!=0){$tot_porc_pnr=$tot_pe_pnr/$tot_pv_pnr*100;}
        if($tot_pv_pr!=0){$tot_porc_pr  =$tot_pe_pr/$tot_pv_pr*100;}
        if($tot_pv_of!=0){$tot_porc_of  =$tot_pe_of/$tot_pv_of*100;}

        $tot_pora_pi=0;$tot_pora_pnr=0;$tot_pora_pr=0;$tot_pora_of=0;
        if($tot_ctv_pi!=0){$tot_pora_pi=$tot_epa_pi/$tot_ctv_pi*100;}
        if($tot_ctv_pnr!=0){$tot_pora_pnr=$tot_epa_pnr/$tot_ctv_pnr*100;}
        if($tot_ctv_pr!=0){$tot_pora_pr=$tot_epa_pr/$tot_ctv_pr*100;}
        if($tot_ctv_of!=0){$tot_pora_of=$tot_epa_of/$tot_ctv_of*100;}

        $tabla .='</tbody>';
        if($p1==1) {
            $total_acum_pi=0;$total_acum_pi_fis=0;
            if($tot_pi!=0){
                $total_acum_pi=number_format($acum_pi/$tot_pi,2,',','.');
                $total_acum_pi_fis=number_format($acum_pi_fis/$tot_pi,2,',','.');
            }
            $tabla .= '<tr class="total_pi">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROYECTOS DE INVERSIÓN</td>';
            $tabla .= '<th >' . $tot_pi . '</th><th class="derecha">' . number_format($tot_cost_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pi,2,',','.') . ' %</th><th class="derecha">'.$total_acum_pi_fis.'</th>
                        <th class="derecha">' . number_format($tot_pi_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pi,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pi,2,',','.') . '%</th><th ></th><th ></th><th class="derecha">'.$total_acum_pi.'</th><th colspan="3"></th></tr>';
        }
        if($p2==1) {
            $total_acum_pr=0;$total_acum_pr_fis=0;
            if($tot_pr!=0){
                $total_acum_pr=number_format($acum_pr/$tot_pr,2,',','.');
                $total_acum_pr_fis=number_format($acum_pr_fis/$tot_pr,2,',','.');
            }
            $tabla .= '<tr class="total_pr">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROGRAMAS RECURRENTES</td>';
            $tabla .= '<th >' . $tot_pr . '</th><th class="derecha">' . number_format($tot_cost_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pr,2,',','.') . ' %</th><th class="derecha">'.$total_acum_pr_fis.'</th>
                         <th class="derecha">' . number_format($tot_pi_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pr,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pr,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_pr.'</th><th colspan="3"></th></tr>';
        }
        if($p3==1) {
            $total_acum_pnr=0;$total_acum_prn_fis=0;
            if($tot_pnr!=0){
                $total_acum_pnr=number_format($acum_pnr/$tot_pnr,2,',','.');
                $total_acum_prn_fis=number_format($acum_pnr_fis/$tot_pnr,2,',','.');
            }
            $tabla .= '<tr class="total_pnr">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL PROGRAMAS NO RECURRENTES</td>';
            $tabla .= '<th >' . $tot_pnr . '</th><th class="derecha">' . number_format($tot_cost_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_pnr,2,',','.') . ' %</th><th class="derecha">'.$total_acum_prn_fis.'</th>
                         <th class="derecha">' . number_format($tot_pi_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_pnr,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_pnr,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_pnr.'</th><th colspan="3"></th></tr>';
        }
        if($p4==1) {
            $total_acum_of=0;$total_acum_of_fis=0;
            if($tot_of!=0){
                $total_acum_of=number_format($acum_of/$tot_of,2,',','.');
                $total_acum_of_fis=number_format($acum_of_fis/$tot_of,2,',','.');
            }
            $tabla .= '<tr class="total_of">';
            $tabla .= '<th colspan="3" class="izquierda">TOTAL OPERACIONES DE FUNCIONAMIENTO</td>';
            $tabla .= '<th >' . $tot_of . '</th><th class="derecha">' . number_format($tot_cost_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_mod_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_ctv_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_epa_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pora_of,2,',','.') . ' %</th><th class="derecha">'.$total_acum_of_fis.'</th>
                         <th class="derecha">' . number_format($tot_pi_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pm_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pv_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_pe_of,2,',','.') . '</th><th class="derecha">' . number_format($tot_porc_of,2,',','.') . ' %</th><th ></th><th ></th><th class="derecha">'.$total_acum_of.'</th><th colspan="3"></th></tr>';
        }


        $total_gral_op  =$tot_pi+$tot_pnr+$tot_pr+$tot_of;
        $total_gral_cost=$tot_cost_pi+$tot_cost_pnr+$tot_cost_pr+$tot_cost_of;
        $total_gral_mod =$tot_mod_pi+$tot_mod_pnr+$tot_mod_pr+$tot_mod_of;
        $total_gral_ctv =$tot_ctv_pi+$tot_ctv_pnr+$tot_ctv_pr+$tot_ctv_of;
        $total_gral_epa =$tot_epa_pi+$tot_epa_pnr+$tot_epa_pr+$tot_epa_of;
        $total_gral_pora=0;
        if($total_gral_ctv!=0){$total_gral_pora=$total_gral_epa/$total_gral_ctv*100;}

        $total_gral_pi  =$tot_pi_pi+$tot_pi_pnr+$tot_pi_pr+$tot_pi_of;
        $total_gral_pm  =$tot_pm_pi+$tot_pm_pnr+$tot_pm_pr+$tot_pm_of;
        $total_gral_pv  =$tot_pv_pi+$tot_pv_pnr+$tot_pv_pr+$tot_pv_of;
        $total_gral_pe  =$tot_pe_pi+$tot_pe_pnr+$tot_pe_pr+$tot_pe_of;
        $total_gral_porc=0;
        if($total_gral_pv!=0){$total_gral_porc=$total_gral_pe/$total_gral_pv*100;}
        $total_acum=0;$total_acum_fis=0;
        if($total_gral_op!=0){
            $total_acum=number_format($acumulado/$total_gral_op, 2, ',', '.');
            $total_acum_fis=number_format($acumulado_efis/$total_gral_op, 2, ',', '.');
        }
        $tabla.='<tr class="total_general">';
        $tabla.='<th colspan="3" class="izquierda">TOTAL POR SECTOR</td>';
        $tabla.='<th >'.$total_gral_op.'</th><th class="derecha">'.number_format($total_gral_cost, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_mod, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_ctv, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_epa, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pora, 2, ',', '.').' %</th><th class="derecha" >'.$total_acum_fis.'</th>
                 <th class="derecha">'.number_format($total_gral_pi, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pm, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pv, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_pe, 2, ',', '.').'</th><th class="derecha">'.number_format($total_gral_porc, 2, ',', '.').' %</th><th ></th><th ></th><th class="derecha">'.$total_acum.'</th><th colspan="3"></th></tr>';

        $tabla .='</table><br>';

        return $tabla;
    }
    /*===================================== Ejecución de acciones operativas ===========================================*/
    public function accion_operativas()
    {
        $enlaces=$this->menu_modelo->get_Modulos(10);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];
        $id_mes=$this->session->userdata("mes");
        $data['id_mes'] = $id_mes;
        $data['mes'] = $mes;
        $data['dias'] = $dias;

        $lista_poa = $this->mpoa->lista_poa();
        $tabla = '';
        foreach($lista_poa as $row)
        {
            $tabla .= '<tr>';
            $tabla .= '<td><center><a href="javascript:abreVentana(\''.site_url("admin").'/reporte/reporte_acciones/'.$row['aper_programa'].'/'.$row['poa_id'].'\');" title="LISTA DE OBJETIVOS DE GESTION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/></a></center></td>';
            $tabla .= '<td>'.$row['aper_programa'].$row['aper_proyecto'].$row['aper_actividad']." - ".$row['aper_descripcion'].'</td>';
            $tabla .= '<td>'.$row['uni_unidad'].'</td>';
            $tabla .= '</tr>';
        }

        $data['aprog'] = $tabla;
        $this->load->view('admin/reportes/formularios_poa/accion/list_programas', $data);
    }
    public function mis_operaciones(){
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(10);
        $data['menu']=$this->menu(10); //// genera menu

        $data['proyectos']= $this->mis_operaciones_aprobadas(1,5,1);
        $data['precurrentes']=$this->mis_operaciones_aprobadas(1,5,2);
        $data['pnrecurrentes']=$this->mis_operaciones_aprobadas(1,5,3);
        $data['operaciones']=$this->mis_operaciones_aprobadas(1,5,4);
        $this->load->view('reportes/reportes_mae/mis_operaciones', $data);

    }
    public function mis_operaciones_aprobadas($mod,$proy_estado,$tp_id){
        $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres
        $tabla ='';
        $nro=0;
        $color='#ffffff';
        foreach($lista_aper_padres  as $rowa){
            $proyectos=$this->model_proyecto->list_proyectos_registrados($mod,$rowa['aper_programa'],$proy_estado,1,$tp_id);
            if(count($proyectos)!=0){
                foreach($proyectos  as $row){
                    $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);
                    $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
                    $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
                    $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']); //// calcula Anual/Plurianual
                    $tabla .= '<tr bgcolor='.$color.'>';
                    $nro++;
                    $tabla .='<td>'.$nro.'</td>';
                    $tabla .= '<td style="padding:5px;">';
                    $tabla .= '<div class="btn-group-vertical">';
                    $tabla .='<a class="btn btn-default" href="" class="form_pond" data-toggle="modal" data-target="#'.$row['proy_id'].'a">
                            <i class="glyphicon glyphicon-stats"></i> REPORTES
                          </a>
                            <div class="modal fade bs-example-modal-lg" tabindex="-1" id="'.$row['proy_id'].'a"  role="dialog" aria-labelledby="myLargeModalLabel">
                              <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                                      &times;
                                    </button>
                                            <h4 class="modal-title">
                                                <img src="'.base_url().'assets/img/logo.png" style="width:300px;" alt="SIIPP">
                                            </h4>
                                        </div>
										<br>
                                        <h2 class="text-center modal-title" id="myModalLabel">'.$row['proy_nombre'].'</h2>
										<br>
                                        <div class="modal-body no-padding">
                                          <div class="well">
                                            <table class="table table-hover table-bordered">
                                              <tr>
                                                <td><b>IDENTIFICACI&Oacute;N DE LA ETAPA</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/identificacion_proy/'.$row['proy_id'].'\');" title="IDENTIFICACION DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Identificación</a></center></td>
                                                <td></td>
                                              </tr>
                                              <tr>
                                                <td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA DE LA ETAPA</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/ejecucion_pfisico/1/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FISICA ANUAL DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/ejecucion_pfisico_m/4/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FISICA PLURI-ANUAL DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N FINANCIERA DE LA ETAPA</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/rep_financiero/1/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FINANCIERA ANUAL DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/rep_financiero/4/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FINANCIERA PLURI-ANUAL DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>REGISTRO DE CONTRATOS</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/reporte_contrato/'.$row['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Contratos</a></center></td>
                                                <td></td>
                                              </tr>
                                              <tr>
                                                <td><b>INFORMES DE SUPERVISI&Oacute;N Y EVALUACI&Oacute;N</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/reporte_supervision/'.$row['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE SUPERVISIÓN"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Supervisión</a></center></td>
                                                <td></td>
                                              </tr>
                                              <tr>
                                                <td><b>CURVA "S" AVANCE F&Iacute;SICO </b></td>
                                                <td><center><a href="javascript:abreVentanaFis(\''.site_url('admin').'/imprimir_curva_fis/1/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="GRAFICO AVANCE FISICO ANUAL"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentanaFis(\''.site_url('admin').'/imprimir_curva_fis/4/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="GRAFICO AVANCE FISICO PLURI-ANUAL"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>CURVA "S" AVANCE FINANCIERO</b></td>
                                                <td><center><a href="javascript:abreVentanaFin(\''.site_url("").'/prog/rep_curva_fin/1/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="GRAFICO AVANCE FINANCIERO ANUAL"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentanaFin(\''.site_url("").'/prog/rep_curva_fin/4/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="GRAFICO AVANCE FINANCIERO PLURI-ANUAL"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Pluri Anual</a></center></td>
                                              </tr>
                                            </table>
                                          </div>
                                        </div>
                                  </div>
                                </div>
                              </div>
                            </div>';
                    $tabla .= '</div>';
                    $tabla .='<a href="javascript:abreVentana(\''.site_url("reportes_mae").'/resumen_ejec/'.$row['proy_id'].'/'.$this->mes.'\');"'.' class="btn btn-primary form_pond">
                            <i class="glyphicon glyphicon-print"></i> RESUMEN</a>';

                    $tabla .='</td>';
                    if($tp_id==1){
                        $tabla .= '<td>'.$row['proy_sisin'].'</td>';}
                    $tabla .= '<td>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</td>';
                    $tabla .= '<td>'.$row['proy_nombre'].'</td>';
                    $tabla .= '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
                    $tabla .= '<td>'.$row['ue'].'</td>';
                    $tabla .= '<td>'.$nc.'</td>';
                    $tabla .= '<td>'.$ap.'</td>';
                    $tabla .= '<td>';
                    $nombre='';
                    if(isset($fase_gest[0]['pfecg_ppto_total']))
                        $nombre=number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.');
                    else
                        $nombre='<a data-toggle="modal" data-target="#modal_mod_aper2" class="btn btn-warning mod_aper2" name="." id="2"><i class="glyphicon glyphicon-info-sign"></i> ERROR EN FECHA <br>DE FASE</a>';
                    $tabla .= $nombre.'</td>';
                    $tabla .= '</tr>';
                }
            }
        }
        return $tabla;
    }
    public function resumen_ejecucion($id,$mes_id){
        $mess=$this->get_mes($mes_id);
        $mes = $mess[1];
        $dias = $mess[2];
        $proy_id = $id;
        $gestion = $this->session->userdata('gestion');
        $dictamen = $this->model_objetivo->get_dictamen($proy_id);
        $unidad_responsable=$this->model_proyecto->responsable_proy($proy_id,1);
        $dictamen = $dictamen->row();
        $aper_programatica = $this->model_objetivo->get_aper_programatica_dictamen($proy_id,$gestion);
        $aper_programatica = $aper_programatica->row();
        $data_fase_activa = array(
            'pfec_id' => $dictamen->pfec_id,
            'fas_fase' => $dictamen->fas_fase,
            'eta_etapa' => $dictamen->eta_etapa,
            'pfec_descripcion' => $dictamen->pfec_descripcion,
            'fecha_inicial' => $dictamen->fecha_inicial,
            'fecha_final' => $dictamen->fecha_final,
            'ejecucion' => $dictamen->ejecucion,
            'anual_plurianual' => $dictamen->anual_plurianual,
            'tp_id' => $dictamen->tp_id
        );
        $num_principal = ($dictamen->tp_id == 1) ? 1 : 0 ;
        $nomb_op ='';
        if($dictamen->tp_id==1)
        {$nomb_op = 'Proyecto';}
        elseif ($dictamen->tp_id==2 || $dictamen->tp_id==3)
        {$nomb_op = 'Programa';}
        elseif ($dictamen->tp_id==4)
        {$nomb_op = 'Acci&oacute;n de Funcionamiento';}

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_resumen.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=70%; class="titulo_pdf">
                            <b>'.$this->session->userdata('entidad').'</b><br>
                            <b>UNIDAD EJECUTORA: </b>'.$unidad_responsable[0]['uresp'].'<br>
                            <b>FORMULARIO RESUMEN EJECUCI&Oacute;N F&Iacute;SICA - FINANCIERA </b><br>
                            <b>'.$dias.' DE '.$mes.' DE '.$gestion.'</b><br>
                        </td>
                        <td width=10%;>
                            
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <br>
                <div class="contenedor_principal">
                <div class="titulo_dictamen"> 1. DATOS GENERALES </div>
                <div class="contenedor_datos">
                <table class="table_contenedor" border="1">
                	<tr class="collapse_t">
                        <td colspan="3" style="width:40%;" class="titulo_campos"><b>Nombre del '.$nomb_op.'</b></td>
                        <td style="width:20%;" class="titulo_campos"><b>C&oacute;digo SISIN</b></td>
                        <td rowspan="4">';
        $html .='<table>
					<tr>
					<td colspan="2" class="titulo_campos"><b>Fase/Etapa</b></td>
					<td colspan="2">'.$data_fase_activa['fas_fase'].'</td>
					</tr>
					<tr>
					<td class="titulo_campos"><b>Fecha de término actual</b></td>
					<td>'.date_format(date_create($data_fase_activa['fecha_final']),"d/m/Y").'</td>
					<td class="titulo_campos"><b>Número de ampliaciones de plazo</b></td>
					<td></td>
					</tr>
					<tr>
					<td colspan="2" class="titulo_campos"><b>N° de modificaciones realizadas al costo total</b></td>
					<td colspan="2"></td>
					</tr>
					</table>';

        $html .='</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="width:40%; text-align:left;">
                            '.$dictamen->proy_nombre.'
                        <br></td>
                        <td>';
        if($dictamen->tp_id==1)
        {$html .= ''.$dictamen->proy_sisin.'';}
        $html .='</td>
					
                    </tr>
                    <tr>
                    	<td class="titulo_campos"><b>Fecha de inicio</b></td>
                    	<td>'.date_format(date_create($data_fase_activa['fecha_inicial']),"d/m/Y").'</td>
                        <td class="titulo_campos"><b>Fecha de término inicial</b></td>
                        <td>'.date_format(date_create($data_fase_activa['fecha_final']),"d/m/Y").'</td>
                    </tr>
                    <tr>
                    	<td class="titulo_campos"><b>Consto inicial del proyecto (Bs.)</b></td>
                    	<td>100.000,21</td>
                        <td class="titulo_campos"><b>Consto actual del proyecto (Bs.)</b></td>
                        <td>100.000,21</td>
                    </tr>
                </table>
                </div>
                <div class="titulo_dictamen"> 2. EJECUCI&Oacute;N F&Iacute;SICA DEL '.strtoupper($nomb_op).' </div>
                <div class="contenedor_datos">';
        $html .='<table class="table_contenedor" border="1">
					<tr class="collapse_t">
					<td class="titulo_campos" style="width:15%;"><b>Ejecuci&oacute;n f&iacute;sica acumulada del '.$nomb_op.' (En %)</b></td>
					<td></td>
					<td class="titulo_campos" style="width:15%;">Ejecuci&oacute;n f&iacute;sica del '.$nomb_op.' solo gesti&oacute;n '.$this->gestion.' (En %)</b></td>
					<td></td>
					</tr>
				</table>
                </div>';
        $html .= '<div class="titulo_dictamen"> 3. EJECUCI&Oacute;N FINANCIERA DEL '.strtoupper ($nomb_op).' </div>
                <div class="contenedor_datos">';
        $html .='<table class="table_contenedor" border="1">
					<tr>
						<td colspan="2" class="titulo_campos" style="width:70%;"><b>Ejecuci&oacute;n financiera acumulada del '.$nomb_op.' (En Bs.)</b></td>
						<td class="titulo_campos" style="width:30%;">Ejecuci&oacute;n financiera del '.$nomb_op.' solo gesti&oacute;n '.$this->gestion.' (En Bs.)</b></td>
					</tr>
					<tr>
						<td>Comp i</td>
						<td>1000</td>
						<td>5000</td>
					</tr>
					<tr>
						<td>Comp ii</td>
						<td>1000</td>
						<td>5000</td>
					</tr>
					<tr>
						<td>Comp iii</td>
						<td>1000</td>
						<td>5000</td>
					</tr>

				</table>
                </div>  
                    '.$this->fase_etapa_activa_proyecto($data_fase_activa, $num_principal).'
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. DATOS GENERALES </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%" class="fila_unitaria">Tipo</td>
                                <td class="fila_unitaria">'.$dictamen->tp_tipo.'</td>
                            </tr>
                        </table>
                    </div>
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. INDICADORES METAS </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">'.$this->indicadores_metas($proy_id).'</table>
                    </div>
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. RESPONSABLES </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%" class="fila_unitaria">Responsable T&eacute;cnico</td>
                                <td class="fila_unitaria">'.$dictamen->resp_tecnico.'</td>
                            </tr>
                            <tr class="collapse_t">
                                <td class="fila_unitaria">Validador POA</td>
                                <td class="fila_unitaria">'.$dictamen->val_poa.'</td>
                            </tr>
                            <tr class="collapse_t">
                                <td class="fila_unitaria">Validador Financiero</td>
                                <td class="fila_unitaria">'.$dictamen->val_fin.'</td>
                            </tr>
                        </table>
                    </div>
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. LOCALIZACI&Oacute;N </div>
                    <div class="contenedor_datos">'.$this->localizacion($proy_id).'</div>
                    
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. CLASIFICACI&Oacute;N SECTORIAL</div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%;text-align:center;" class="fila_unitaria">Clasificaci&oacute;n Sectorial</td>
                                <td class="fila_unitaria">
                                    <ul class="lista">
                                        <li><b>SECTOR: </b>'.$dictamen->c_sect_sector.'</li>
                                        <li><b>SUBSECTOR: </b>'.$dictamen->c_sect_subsector.'</li>
                                        <li><b>ACTIVIDAD: </b>'.$dictamen->c_sect_actividad.'</li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </div>
                   
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. RESUMEN T&Eacute;CNICO DEL PROYECTO</div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%" class="fila_unitaria">Descripci&oacute;n del Proyecto</td>
                                <td class="fila_unitaria">'.$dictamen->proy_descripcion_proyecto.'</td>
                            </tr>
                        </table>
                    </div>';
        $fase=$this->model_faseetapa->fase_etapa($dictamen->pfec_id,$proy_id);
        $html .= '<div class="titulo_dictamen"> '.($num_principal+=1).'. PRESUPUESTO</div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                <td style="width:21%" class="fila_unitaria">TOTAL FASE PRESUPUESTO</td>
                                <td class="fila_unitaria">'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>
                                <td style="width:21%" class="fila_unitaria">PRESUPUESTO EJECUTADO</td>
                                <td class="fila_unitaria">'.number_format($fase[0]['pfec_ptto_fase_e'], 2, ',', '.').' Bs.</td>
                            </tr>';
        $ptto_fase=$this->model_faseetapa->ptto_fase($dictamen->pfec_id);
        foreach($ptto_fase as $row)
        {
            $html .= '
                                <tr class="collapse_t">
                                    <td style="width:21%" class="fila_unitaria">PRESUPUESTO '.$row['g_id'].'</td>
                                    <td class="fila_unitaria">'.number_format($row['pfecg_ppto_total'], 2, ',', '.').' Bs.</td>
                                    <td style="width:21%" class="fila_unitaria">EJECUCI&Oacute;N '.$row['g_id'].'</td>
                                    <td class="fila_unitaria">'.number_format($row['pfecg_ppto_ejecutado'], 2, ',', '.').' Bs.</td>
                                </tr>';
        }
        $html .= '    
                        </table>
                    </div>
                    '.$this->componente($proy_id, $dictamen->pfec_id,$gestion,$num_principal).'
                    <br>
                
                    <table class="table_contenedor" style="margin-top: 10px;>
                        <tr>
                            <td class="fila_unitaria">FIRMAS:</td>
                        </tr>
                    </table>
                    <table style="width: 80%;margin: 0 auto;margin-top: 100px;margin-bottom: 50px;">
                        <tr>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                        </tr>
                    </table>

                </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');//portrait
        $dompdf->render();
        $dompdf->stream("resumen_ejecucion.pdf", array("Attachment" => false));
    }

    public function fase_etapa_activa_proyecto($data, $num_principal)
    {
        $fase_etapa_activa = '';
        $fase_etapa_activa .= '';
        if($data['pfec_id'] != -1){
            if($data['tp_id']==1){
                $fase_etapa_activa .= '
                    <div class="titulo_dictamen"> 1. FASE ETAPA ACTIVA </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="background-color:#ac9fae;">
                                <td>FASE</td>
                                <td>ETAPA</td>
                                <td>DESCRIPCI&Oacute;N</td>
                                <td>F. INICIAL</td>
                                <td>F. FINAL</td>
                                <td>EJECUCI&Oacute;N</td>
                                <td>ANUAL/PLURIANUAL</td>
                            </tr>
                            <tr class="collapse_t">
                                <td>'.$data['fas_fase'].'</td>
                                <td>'.$data['eta_etapa'].'</td>
                                <td>'.$data['pfec_descripcion'].'</td>
                                <td>'.$data['fecha_inicial'].'</td>
                                <td>'.$data['fecha_final'].'</td>
                                <td>'.$data['ejecucion'].'</td>
                                <td>'.$data['anual_plurianual'].'</td>
                            </tr>
                        </table>    
                    </div>
                ';
            }
            else{
                $fase_etapa_activa .= '';
            }
        }else{
            $fase_etapa_activa .= '
                <div class="titulo_dictamen"> 1. FASE ETAPA ACTIVA </div>
                <div class="contenedor_datos">
                    <table class="table_contenedor">
                        <tr>
                            <td>Sin Fase Etapa Activa</td>
                        </tr>
                    </table>    
                </div>
            ';
        };
        return $fase_etapa_activa;
    }
    public function indicadores_metas($id)
    {
        $proy_id = $id;
        $query = $this->model_objetivo->get_indicadores_metas($proy_id);
        $count = $query->num_rows();
        $query = $query->result_array();
        $html_metas = '';
        if( $count >= 1 ){
            $html_metas ='
                <tr class="collapse_t" style="background-color:#ac9fae;">
                    <td>#</td>
                    <td>DESCRIPCI&Oacute;N META</td>
                    <td>META</td>
                    <td>EJECUCI&Oacute;N</td>
                    <td>EFICACIA</td>
                </tr>';
            $n=0;
            foreach($query as $fila){
                $n++;
                $html_metas .='
                <tr class="collapse_t">
                    <td>'.$n.'</td>
                    <td>'.$fila['meta_descripcion'].'</td>
                    <td>'.$fila['meta_meta'].'</td>
                    <td>'.$fila['meta_ejec'].'</td>
                    <td>'.$fila['meta_efic'].'</td>
                </tr>';
            };
        }else{
            $html_metas = '
                <tr class="collapse_t">
                    <td>Sin Metas Registradas</td>
                </tr>
            ';
        }
        return $html_metas;
    }
    public function localizacion($id)
    {
        $proy_id = $id;
        $query = $this->model_objetivo->get_localizacion_dictamen($proy_id);
        $n = $query->num_rows();
        $html_localizacion = '';
        if($n>0){
            $query = $query->result_array();
            $html_localizacion .= '';
            foreach($query as $fila){
                $html_localizacion .='
                <table class="table_contenedor">
                    <tr class="collapse_t">
                        <td style="width:18%">
                            '.$fila['dep_departamento'].'
                        </td>
                        <td style="width:18%">
                            Provincia: '.$fila['prov_provincia'].'
                        </td>
                        <td>
                            <table class="table_contenedor">
                                <tr style="font-size:7px;background-color:#ac9fae;">
                                    <td style="height:10px;">REGI&Oacute;N</td>
                                    <td>MUNICIPIO</td>
                                    <td>%P</td>
                                    <td>POB. HOMBRES</td>
                                    <td>POB. MUJERES</td>
                                    <td style="width:15%;">Cantones</td>
                                </tr>
                                <tr>
                                    <td>'.$fila['reg_region'].'</td>
                                    <td>'.$fila['muni_municipio'].'</td>
                                    <td>'.$fila['pm_pondera'].'%</td>
                                    <td>'.$fila['muni_poblacion_hombres'].'</td>
                                    <td>'.$fila['muni_polacion_mujeres'].'</td>
                                    <td>'.$fila['cantones'].'</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>';
            };
        }else{
            $html_localizacion .='
                <table class="table_contenedor">
                    <tr class="collapse_t" style="font-weight: bold;">
                        <td> Sin Localizaci�n</td>
                    </tr>
                </table>';
        }
        return $html_localizacion;
    }
    public function componente($id, $fase_activa, $gestion,$num_principal)
    {
        $proy_id = $id;
        if($fase_activa != -1){
            $componentes = $this->model_objetivo->get_componentes_dictamen($proy_id);
            $n = $componentes->num_rows();
            $div_componente = '<div class="titulo_dictamen"> '.($num_principal+=2).'. COMPONENTES</div>';
            $tbl_componente = '';
            if($n>0){
                $componentes = $componentes->result_array();
                $n_componente = 0;
                foreach($componentes as $fila){
                    $n_componente++;
                    $tbl_componente .='
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:75%;" class="fila_unitaria titulo_dictamen">
                                    <b> '.$n_componente.'. COMPONENTE</b>
                                </td>
                                <td style="width:25%;"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%;background-color:#ac9fae;font-weight: bold;">
                                    <b>COMPONENTE</b>
                                </td>
                                <td class="fila_unitaria">'.$fila['com_componente'].'</td>
                                <td style="width:18%;background-color:#ac9fae;font-weight: bold;"> 
                                    <b>PONDERACI&Oacute;N %</b>
                                </td>
                                <td class="fila_unitaria">'.$fila['com_ponderacion'].'</td>
                            </tr>
                        </table>
                    </div>
                    ';
                };
                $div_componente .= $tbl_componente;
            }else{
                $tbl_componente .='
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="font-weight: bold;">
                                <td>Sin Componentes</td>
                            </tr>
                        </table>
                    </div>';
                $div_componente .= $tbl_componente;
            }
        }else{
            $div_componente = '';
        }
        return $div_componente;
    }
    public function producto($id, $gestion,$n_componente)
    {
        $com_id = $id;
        $productos = $this->model_objetivo->get_productos_dictamen($com_id);
        if ($productos->num_rows()>0) {
            $productos = $productos->result_array();
            $div_producto = '<div class="titulo_dictamen"> 12. PRODUCTOS</div>';
            $div_producto = '';
            $tbl_producto = '';
            $n_producto = 0;
            $div_producto .= '';
            foreach ($productos as $fila) {
                $n_producto++;
                // $titulo_producto .='';
                $tbl_producto .='
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:50%;" class="fila_unitaria titulo_dictamen">
                                    <b> '.$n_componente.'.'.$n_producto.'. PRODUCTO</b>
                                </td>
                                <td style="width:50%;"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                <td style="width:4%;">Nro.</td>
                                <td>OBJETIVO DEL PRODUCTO</td>
                                <td style="width:4%;">T.I.</td>
                                <td>INDICADOR</td>
                                <td>L/B</td>
                                <td>META</td>
                                <td>%P</td>
                                <td style="width:50%;">TEMPORALIZACI�N DE LA META '.$fila['prod_id'].'</td>
                            </tr>
                            <tr class="collapse_t">
                                <td>'.$n_producto.'</td>
                                <td>'.$fila['pro_producto'].'</td>
                                <td>'.$fila['indi_abreviacion'].'</td>
                                <td>'.$fila['prod_indicador'].'</td>
                                <td>'.$fila['prod_linea_base'].'</td>
                                <td>'.$fila['prod_meta'].'</td>
                                <td>'.$fila['prod_ponderacion'].'</td>
                                <td> '.$this->temporalizacion_metas_producto($fila['prod_id'],$gestion).'</td>
                            </tr>
                        </table>
                    </div>
                    '.$this->actividad($fila['prod_id'],$gestion,$n_componente,$n_producto).'';
            };
            $div_producto .= $tbl_producto;
        }else{
            $div_producto = '';
        };

        return $div_producto;
    }
    private $estilo_resumen = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 45px 35px 17px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 8px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 7px;
                color: #ffffff;
                height: 20px;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -70px;
            right: 0px;
            height: 80px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 6px;
            width: 100%;
            background-color:#fff;
        }
        td {
                padding: 1px;
                font-size: 6px;
                height: 15px;
            }
        td.titulo_campos{
			background-color:#ac9fae;
			text-align:center;
		}
        .mv{font-size:8px;}
        .siipp{width:140px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 7px;
        }
        .datos_principales {
            text-align: center;
            font-size: 6px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }

        .indi_desemp{
            font-size: 8px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 6px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 1px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .lista{
            text-align:left;
            padding-left: 6;
            list-style-type:square;
            margin:2px;
        }
        th.cabecera{
                    background-color:#C7BB78;
                    color: #000000;
                    border: solid 1px #000000;
        }
        th.cabecera_acum{
                    background-color:#F0EAC7;
                    color: #000000;
        }
        th.titulo_cabecera{color: #000000; border-width:1px;}
        th.derecha{text-align:right; font-size:6px; height: 20px;}
        th.izquierda{text-align:left; font-size:6px;}
        tr.derecha{text-align:right; font-size:6px;}
        tr.multiple{background-color:#EAD4E5; text-align:right;}
        td.izquierda{text-align:left;}
        td.centro{text-align:center;}
        td.pi{background-color:#E49A15; text-align:center;}
        td.pr{background-color:#E4BE7C; text-align:center;}
        td.pnr{background-color:#8AB054; text-align:center;}
        td.of{background-color:#B2CC96; text-align:center;}
        tr.total_pi{background-color:#E49A15; text-align:right;font-weight:bold; font-size:11px; height: 15px;}
        tr.total_pnr{background-color:#8AB054; text-align:right;font-weight:bold; font-size:11px; height: 15px;}
        tr.total_pr{background-color:#E4BE7C; text-align:right;font-weight:bold; font-size:11px; height: 15px;}
        tr.total_of{background-color:#B2CC96; text-align:right;font-weight:bold; font-size:11px; height: 20px;}
        tr.total_general{background-color:#5F6266; color:#ffffff; text-align:right; font-weight:bold;}
        tr.total_multi{background-color:#82E4FF; color:#000000; text-align:right; font-weight:bold;}
        a {text-decoration: none; color:#000000;} 
        ul{ margin-left: 0; padding-left: 0; list-style:none;}
        ul li{ margin-left: 1em;}
    </style>';






    /*==========================*/
    function list_ejecucion_financiera_da($f1,$f2,$p1,$p3,$p4,$p5,$valor1,$valor2,$da_id,$tp_rep){
        $da = $this->model_nfisfin->get_unidad_organizacional($da_id);
        if($tp_rep==1){
            $tb='id="dt_basic" class="table table table-bordered" width="100%"';
        }
        elseif ($tp_rep==2) {
            $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        }

        $nro=0;
        $tabla ='';
        $tabla .='<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h2 class="alert alert-success"><center>EJECUCI&Oacute;N FINANCIERO% </center></h2>
                  </article>';

        $proy = $this->model_nfisfin->get_proyectos_prog_fin($f1,$f2,$p1,$p3,$p4,$p5,$da_id);
        if(count($proy)!=0){
            $nro++;
            $tabla .='
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                        <header>
                          <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                          <h2 class="font-md"><strong>'.$da[0]['uni_unidad'].'</strong></h2>  
                        </header>
                <div>
                  <div class="widget-body no-padding">
                  <div class="table-responsive">';
            $tabla .='<table '.$tb.'>';
            $tabla .='<thead>';
            $tabla .='<tr class="modo1">';
            $tabla .='<th style="width:1%;">#</th>';
            $tabla .='<th style="width:15%;">DIRECCI&Oacute;N ADMINISTRATIVA</th>';
            $tabla .='<th style="width:15%;">UNIDAD EJECUTORA</th>';
            $tabla .='<th style="width:15%;">PROYECTO_PROGRAMA_OPERACI&Oacute;N</th>';
            $tabla .='<th style="width:10%;">TIPO DE OPERACI&Oacute;N</th>';
            $tabla .='<th>AVANCE FINANCIERO</th>';
            $tabla .='<th>AVANCE F&Iacute;SICO</th>';
            $tabla .='<th>ESTADO</th>';
            $tabla .='</tr>';
            $tabla .='</thead>';
            $nrop=0;
            $tabla .='<tbody>';
            foreach($proy as $rowp)
            {
                $fis=$this->avance_fisico($rowp['proy_id'],$this->mes);
                $estado=$this->estado_proyecto($rowp['proy_id']);
                $efin=0;
                if($rowp['pe_pv']!=0){
                    $efin=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                }

                if($efin>=1 & $efin<=100){
                    $nrop++;
                    $tabla .='<tr class="modo1">';
                    $tabla .='<td>'.$nrop.'</td>';
                    $tabla .='<td>'.$rowp['das'].'</td>';
                    $tabla .='<td>'.$rowp['u_ejec'].'</td>';
                    $tabla .='<td>'.$rowp['proy_nombre'].'</td>';
                    $tabla .='<td>'.$rowp['tp_tipo'].'</td>';
                    $tabla .='<td bgcolor="#c9eac9">'.$efin.'%</td>';
                    $tabla .='<td>'.$fis[2].'%</td>';
                    $tabla .='<td>'.$estado.'</td>';
                    $tabla .='</tr>';
                }
            }
            $tabla .='</tbody>';
            $tabla .='<tr bgcolor="#f5f5f5">';
            $tabla .='<td></td>';
            $tabla .='<td><b>TOTAL : '.$nrop.'</b></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='</tr>';
            $tabla .='</table>';
            $tabla .='</div>

                  </div>
                </div>
              </div>
            </article>';
        }
        return $tabla;
    }
    function datos_tp($p1,$p2,$p3,$p4,$p5){
        if($p1==1 & $p2==1 & $p3==0 & $p4==0 & $p5==0){
            $v[1]=1;$v[2]=1;$v[3]=1;$v[4]=0;$v[5]=0;$v[6]=0;
            // echo "1,1,1,0,0,0";
        }
        elseif($p1==1 & $p2==0 & $p3==0 & $p4==0 & $p5==0){
            $v[1]=1;$v[2]=0;$v[3]=1;$v[4]=0;$v[5]=0;$v[6]=0;
            // echo "1,0,1,0,0,0";
        }
        elseif($p1==0 & $p2==1 & $p3==0 & $p4==0 & $p5==0){
            $v[1]=0;$v[2]=1;$v[3]=1;$v[4]=0;$v[5]=0;$v[6]=0;
            // echo "0,1,1,0,0,0";
        }
        elseif($p1==0 & $p2==0 & $p3==1 & $p4==0 & $p5==0){
            $v[1]=1;$v[2]=0;$v[3]=0;$v[4]=1;$v[5]=0;$v[6]=0;
            // echo "1,0,0,1,0,0";
        }
        elseif($p1==0 & $p2==0 & $p3==1 & $p4==1 & $p5==0){
            $v[1]=1;$v[2]=0;$v[3]=0;$v[4]=1;$v[5]=1;$v[6]=0;
            // echo "1,0,0,1,1,0";
        }
        elseif($p1==0 & $p2==0 & $p3==1 & $p4==0 & $p5==1){
            $v[1]=1;$v[2]=0;$v[3]=0;$v[4]=1;$v[5]=0;$v[6]=1;
            // echo "1,0,0,1,0,1";
        }
        elseif($p1==0 & $p2==0 & $p3==0 & $p4==1 & $p5==0){
            $v[1]=1;$v[2]=0;$v[3]=0;$v[4]=0;$v[5]=1;$v[6]=0;
            // echo "1,0,0,0,1,0";
        }
        elseif($p1==0 & $p2==0 & $p3==0 & $p4==0 & $p5==1){
            $v[1]=1;$v[2]=0;$v[3]=0;$v[4]=0;$v[5]=0;$v[6]=1;
            // echo "1,0,0,0,0,1";
        }
        elseif($p1==0 & $p2==0 & $p3==0 & $p4==1 & $p5==1){
            $v[1]=1;$v[2]=0;$v[3]=0;$v[4]=0;$v[5]=1;$v[6]=1;
            // echo "1,0,0,0,1,1";
        }
        elseif($p1==0 & $p2==0 & $p3==1 & $p4==1 & $p5==1){
            $v[1]=1;$v[2]=0;$v[3]=0;$v[4]=1;$v[5]=1;$v[6]=1;
            // echo "1,0,0,1,1,1";
        }
        elseif($p1==1 & $p2==0 & ($p3==1 || $p4=1 || $p5==1)){
            $v[1]=1;$v[2]=0;$v[3]=1;$v[4]=$p3;$v[5]=$p4;$v[6]=$p5;
            // echo "1,0,1,".$p3.",".$p4.",".$p5."";
        }
        elseif($p1==0 & $p2==1 & ($p3==1 || $p4==1 || $p5==1)){
            $v[1]=1;$v[2]=1;$v[3]=1;$v[4]=$p3;$v[5]=$p4;$v[6]=$p5;
            // echo "1,1,1,".$p3.",".$p4.",".$p5."";
        }
        elseif($p1==1 & $p2==1 & ($p3==1 || $p4==1 || $p5==1)){
            $v[1]=1;$v[2]=1;$v[3]=1;$v[4]=$p3;$v[5]=$p4;$v[6]=$p5;
            // echo "1,1,1,".$p3.",".$p4.",".$p5."";
        }
        if($p1==1 & $p2==1 & $p3==1 & $p4==1 & $p5==1){
            $v[1]=1;$v[2]=1;$v[3]=1;$v[4]=1;$v[5]=1;$v[6]=1;
            // echo "1,1,1,1,1,1";
        }
        return $v;
    }
    function avance_fisico($proy_id,$id_mes)
    {
        $ejecucion=0;
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $meses=$años*12;

        $ms[0]='programado_total';$ms[1]='enero';$ms[2]='febrero';$ms[3]='marzo';$ms[4]='abril';$ms[5]='mayo';$ms[6]='junio';
        $ms[7]='julio';$ms[8]='agosto';$ms[9]='septiembre';$ms[10]='octubre';$ms[11]='noviembre';$ms[12]='diciembre';

        $componentes = $this->model_componente->componentes_id($fase[0]['id']);
        for($p=1;$p<=$meses;$p++){$cp[1][$p]=0; $cp[2][$p]=0; $cp[3][$p]=0;}
        foreach ($componentes as $rowc)
        {
            $productos = $this->model_producto->list_prod($rowc['com_id']);
            for($p=1;$p<=$meses;$p++){$pp[1][$p]=0; $pp[2][$p]=0;}
            // echo "COMPONENTE : ".$rowc['com_componente']."<br>";
            foreach ($productos as $rowp)
            {
                $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                //  echo "PRODUCTO : ".$rowp['prod_producto']."<br>";
                for($p=1;$p<=$meses;$p++){$ap[1][$p]=0; $ap[2][$p]=0;}
                foreach ($actividad as $rowa)
                {
                    $variablep=1;$variablee=1; $v=12; $nro=1; $sump=0;$sume=0;
                    for($p=1;$p<=$meses;$p++){$a[1][$p]=0; $a[2][$p]=0;$a[3][$p]=0;$a[4][$p]=0;$a[5][$p]=0;$a[6][$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                    for($i=$fase[0]['pfec_fecha_inicio'];$i<=$fase[0]['pfec_fecha_fin'];$i++)
                    {
                        $aprog = $this->model_actividad->actividad_programado($rowa['act_id'],$i);
                        if(count($aprog)!=0){
                            for ($j=1; $j <=12 ; $j++) {
                                $a[1][$variablep]=$aprog[0][$ms[$j]];
                                $variablep++;
                            }
                        }
                        else{
                            $variablep=($v*$nro)+1;
                        }

                        $aejec = $this->model_actividad->actividad_ejecutado($rowa['act_id'],$i);
                        if(count($aejec)!=0){
                            for ($j=1; $j <=12 ; $j++) {
                                $a[4][$variablee]=$aejec[0][$ms[$j]];
                                $variablee++;
                            }
                        }
                        else{
                            $variablee=($v*$nro)+1;
                        }

                        $nro++;
                    }

                    for ($i=1; $i <=$meses ; $i++) {
                        $sump=$sump+$a[1][$i];
                        $a[2][$i]=$sump+$rowa['act_linea_base'];

                        if($rowa['act_meta']!=0)
                        {
                            $a[3][$i]=round(((($sump+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                        }
                        $ap[1][$i]=$ap[1][$i]+(($a[3][$i]*$rowa['act_ponderacion'])/100);

                        $sume=$sume+$a[4][$i];
                        $a[5][$i]=$sume+$rowa['act_linea_base'];

                        if($rowa['act_meta']!=0)
                        {
                            $a[6][$i]=round(((($sume+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                        }
                        $ap[2][$i]=$ap[2][$i]+(($a[6][$i]*$rowa['act_ponderacion'])/100);
                    }

                }

                for ($i=1; $i <=$meses ; $i++) {
                    $pp[1][$i]=$pp[1][$i]+(($ap[1][$i]*$rowp['prod_ponderacion'])/100);
                    $pp[2][$i]=$pp[2][$i]+(($ap[2][$i]*$rowp['prod_ponderacion'])/100);
                }

            }
            for ($i=1; $i <=$meses ; $i++) {
                $cp[1][$i]=$cp[1][$i]+(($pp[1][$i]*$rowc['com_ponderacion'])/100);
                $cp[2][$i]=$cp[2][$i]+(($pp[2][$i]*$rowc['com_ponderacion'])/100);
            }

        }

        for ($i=1; $i <=$meses ; $i++) {
            if($cp[1][$i]!=0){
                $cp[3][$i]=round((($cp[2][$i]/$cp[1][$i])*100),1);
            }
        }

        for ($i=1; $i <=12 ; $i++) {
            $tefic[1][$i]=0;$tefic[2][$i]=0;$tefic[3][$i]=0;
        }
        $i=1;$vmeses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;
        for($p=$fase[0]['pfec_fecha_inicio'];$p<=$fase[0]['pfec_fecha_fin'];$p++)
        {
            $variable=0;
            for($pos=$i;$pos<=$vmeses;$pos++)
            {
                $variable++;
                $tefic[1][$variable]=$cp[1][$pos];
                $tefic[2][$variable]=$cp[2][$pos];
                $tefic[3][$variable]=$cp[3][$pos];
            }
            if($p==$this->session->userdata('gestion'))
            {
                $ejecucion=$tefic[2][$id_mes];
            }

            $i=$vmeses+1;
            $vmeses=$vmeses+12;
            $gestion++; $nro++;
        }

        return $ejecucion;
    }
    /*-------------------- Avance Financiero -------------------*/
    public function avance_financiero($proy_id,$mes_id){
        $efin = $this->model_ejecucion->datos_programa_presupuesto($proy_id,$mes_id,$this->gestion);
        $fin=0;
        if(count($efin)!=0){
            if($efin[0]['pe_pv']!=0){
                $fin=round((($efin[0]['pe_pe']/$efin[0]['pe_pv'])*100),2);
            }
        }
        return $fin;
    }
    public function estado_proyecto($proy_id,$mes_id)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $estado=$this->mdas_complementario->estado_operacion($fase[0]['id'],$mes_id,$this->gestion);
        $estado_proy='Sin registro';
        if(count($estado)!=0){
            $estado_proy=$estado[0]['ep_descripcion'];
        }

        return $estado_proy;
    }



    function menu($mod){
        $enlaces=$this->menu_modelo->get_Modulos($mod);
        for($i=0;$i<count($enlaces);$i++){
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }

        $tabla ='';
        for($i=0;$i<count($enlaces);$i++){
            if(count($subenlaces[$enlaces[$i]['o_child']])>0){
                $tabla .='<li>';
                $tabla .='<a href="#">';
                $tabla .='<i class="'.$enlaces[$i]['o_image'].'"></i> <span class="menu-item-parent">'.$enlaces[$i]['o_titulo'].'</span></a>';
                $tabla .='<ul>';
                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
                    $tabla .='<li><a href="'.base_url($item['o_url']).'">'.$item['o_titulo'].'</a></li>';
                }
                $tabla .='</ul>';
                $tabla .='</li>';
            }
        }

        return $tabla;
    }
    public function get_mes($mes_id)
    {
        $mes[1]='ENERO';
        $mes[2]='FEBRERO';
        $mes[3]='MARZO';
        $mes[4]='ABRIL';
        $mes[5]='MAYO';
        $mes[6]='JUNIO';
        $mes[7]='JULIO';
        $mes[8]='AGOSTO';
        $mes[9]='SEPTIEMBRE';
        $mes[10]='OCTUBRE';
        $mes[11]='NOVIEMBRE';
        $mes[12]='DICIEMBRE';

        $dias[1]='31';
        $dias[2]='28';
        $dias[3]='31';
        $dias[4]='30';
        $dias[5]='31';
        $dias[6]='30';
        $dias[7]='31';
        $dias[8]='31';
        $dias[9]='30';
        $dias[10]='31';
        $dias[11]='30';
        $dias[12]='31';

        $valor[1]=$mes[$mes_id];
        $valor[2]=$dias[$mes_id];

        return $valor;
    }

    function progr_ejec_fisica_acumulada($mod,$id_f,$proy_id){
        $programado=0;
        $ejecutado =0;
        $porcentaje=0;
        $proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $a�os=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $meses=$a�os*12;
        for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
        for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
        for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
        //for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
        $nro_c = $this->model_componente->componentes_nro($id_f); //// nro de componente
        if ($nro_c>0){
            for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
            $componentes = $this->model_componente->componentes_id($id_f);
            $nro_c=1;

            foreach ($componentes as $rowc){
                $nrop=0;
                $productos = $this->model_producto->list_prod($rowc['com_id']);
                for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
                /*--------------------------- Producto --------------------------*/
                foreach ($productos as $rowp){
                    $nrop++;
                    /*--------------------------- Actividad --------------------------*/
                    for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                    $nroa=0;
                    $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                    foreach ($actividad as $rowa){
                        $nroa++;
                        $ti='';if($rowa['indi_id']==2){ $ti='%';}
                        $meta_gestion=$this->model_actividad->meta_act_gest($rowa['act_id']);
                        $meta_gest=$meta_gestion[0]['meta_gest']+$rowa['act_linea_base'];

                        $pa=0; $pe=0; $gestion=$fase[0]['pfec_fecha_inicio']; $a=1;$e=1;
                        for($k=1;$k<=$a�os;$k++){
                            $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$gestion); /// programado
                            $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$gestion); /// ejecutado
                            $nro=0;
                            foreach($programado as $row){
                                $nro++;
                                $matriz [1][$nro]=$row['m_id'];
                                $matriz [2][$nro]=$row['pg_fis'];
                            }
                            /*---------------- llenando la matriz vacia --------------*/
                            for($j = 1; $j<=12; $j++){
                                $matriz_r[1][$j]=$j;
                                $matriz_r[2][$j]='0';  //// P
                                $matriz_r[3][$j]='0';  //// PA
                                $matriz_r[4][$j]='0';  //// %PA
                                $matriz_r[5][$j]='0';  //// A
                                $matriz_r[6][$j]='0';  //// B
                                $matriz_r[7][$j]='0';  //// E
                                $matriz_r[8][$j]='0';  //// EA
                                $matriz_r[9][$j]='0';  //// %EA
                                $matriz_r[10][$j]='0'; //// EFICACIA
                            }
                            /*--------------------------------------------------------*/
                            /*--------------------ejecutado gestion ------------------*/
                            $nro_e=0;
                            foreach($ejecutado as $row){
                                $nro_e++;
                                $matriz_e [1][$nro_e]=$row['m_id'];
                                $matriz_e [2][$nro_e]=$row['ejec_fis'];
                                $matriz_e [3][$nro_e]=$row['ejec_fis_a'];
                                $matriz_e [4][$nro_e]=$row['ejec_fis_b'];
                            }
                            /*--------------------------------------------------------*/
                            /*------- asignando en la matriz P, PA, %PA ----------*/
                            for($i = 1 ;$i<=$nro ;$i++){
                                for($j = 1 ;$j<=12 ;$j++){
                                    if($matriz[1][$i]==$matriz_r[1][$j]){
                                        $matriz_r[2][$j]=round($matriz[2][$i],2);
                                    }
                                }
                            }

                            for($j = 1 ;$j<=12 ;$j++){
                                $pa=$pa+$matriz_r[2][$j];
                                $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
                                if($rowa['act_meta']!=0){
                                    $matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                                }

                            }
                            /*-------------------------------------------------*/
                            /*--------------- EJECUCION ----------------------------------*/
                            if($rowa['indi_id']==1){
                                for($i = 1 ;$i<=$nro_e ;$i++){
                                    for($j = 1 ;$j<=12 ;$j++){
                                        if($matriz_e[1][$i]==$matriz_r[1][$j]){
                                            $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                        }
                                    }
                                }
                            }
                            elseif ($rowa['indi_id']==2){
                                for($i = 1 ;$i<=$nro_e ;$i++){
                                    for($j = 1 ;$j<=12 ;$j++){
                                        if($matriz_e[1][$i]==$matriz_r[1][$j]){
                                            $matriz_r[5][$j]=round($matriz_e[3][$i],2);
                                            $matriz_r[6][$j]=round($matriz_e[4][$i],2);
                                            $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                        }
                                    }
                                }
                                /*--------------------------------------------------------*/
                            }
                            /*--------------------matriz E,AE,%AE gestion ------------------*/
                            for($j = 1 ;$j<=12 ;$j++){
                                $pe=$pe+$matriz_r[7][$j];
                                $matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
                                if($rowa['act_meta']!=0){
                                    $matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                                }

                                if($matriz_r[4][$j]==0)
                                {$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),2);}
                                else{
                                    $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),2);
                                }
                            }

                            $ap[$a]=(($matriz_r[4][1]*$rowa['act_ponderacion'])/100); $a++;
                            $ae[$e]=(($matriz_r[9][1]*$rowa['act_ponderacion'])/100); $e++;

                            $ap[$a]=(($matriz_r[4][2]*$rowa['act_ponderacion'])/100); $a++;
                            $ae[$e]=(($matriz_r[9][2]*$rowa['act_ponderacion'])/100); $e++;

                            $ap[$a]=(($matriz_r[4][3]*$rowa['act_ponderacion'])/100); $a++;
                            $ae[$e]=(($matriz_r[9][3]*$rowa['act_ponderacion'])/100); $e++;

                            $ap[$a]=(($matriz_r[4][4]*$rowa['act_ponderacion'])/100); $a++;
                            $ae[$e]=(($matriz_r[9][4]*$rowa['act_ponderacion'])/100); $e++;

                            $ap[$a]=(($matriz_r[4][5]*$rowa['act_ponderacion'])/100); $a++;
                            $ae[$e]=(($matriz_r[9][5]*$rowa['act_ponderacion'])/100); $e++;

                            $ap[$a]=(($matriz_r[4][6]*$rowa['act_ponderacion'])/100); $a++;
                            $ae[$e]=(($matriz_r[9][6]*$rowa['act_ponderacion'])/100); $e++;

                            $ap[$a]=(($matriz_r[4][7]*$rowa['act_ponderacion'])/100); $a++;
                            $ae[$e]=(($matriz_r[9][7]*$rowa['act_ponderacion'])/100); $e++;

                            $ap[$a]=(($matriz_r[4][8]*$rowa['act_ponderacion'])/100); $a++;
                            $ae[$e]=(($matriz_r[9][8]*$rowa['act_ponderacion'])/100); $e++;

                            $ap[$a]=(($matriz_r[4][9]*$rowa['act_ponderacion'])/100); $a++;
                            $ae[$e]=(($matriz_r[9][9]*$rowa['act_ponderacion'])/100); $e++;

                            $ap[$a]=(($matriz_r[4][10]*$rowa['act_ponderacion'])/100); $a++;
                            $ae[$e]=(($matriz_r[9][10]*$rowa['act_ponderacion'])/100); $e++;

                            $ap[$a]=(($matriz_r[4][11]*$rowa['act_ponderacion'])/100); $a++;
                            $ae[$e]=(($matriz_r[9][11]*$rowa['act_ponderacion'])/100); $e++;

                            $ap[$a]=(($matriz_r[4][12]*$rowa['act_ponderacion'])/100); $a++;
                            $ae[$e]=(($matriz_r[9][12]*$rowa['act_ponderacion'])/100); $e++;

                            /*if($gestion==$this->session->userdata('gestion')){
								$tabla .= '';
							}*/

                            $gestion++;
                        }

                        for($k=1;$k<=$meses;$k++){
                            $app[$k]=round(($app[$k]+$ap[$k]),1); //// sumando a nivel de las actividades
                            $aee[$k]=round(($aee[$k]+$ae[$k]),1); //// sumando a nivel de las actividades
                        }

                    }
                    for($kp=1;$kp<=$meses;$kp++){
                        $ppp[$kp]=round(($ppp[$kp]+($app[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
                        $ppe[$kp]=round(($ppe[$kp]+($aee[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
                    }
                    /*--------------------------- End Actividad --------------------------*/
                }
                for($k=1;$k<=$meses;$k++){
                    $cpp[$k]=round(($cpp[$k]+($ppp[$k]*$rowc['com_ponderacion'])/100),0); //// sumando a nivel de los Componentes
                    $cpe[$k]=round(($cpe[$k]+($ppe[$k]*$rowc['com_ponderacion'])/100),0); //// sumando a nivel de los Componentes
                }
                /*--------------------------- End Producto --------------------------*/
            }
            $i=1;$meses=12;$gestion=$fase[0]['pfec_fecha_inicio'];
            //$tabla .='TABLA DE EFICACIA ANUAL';
            for($kp=1;$kp<=$a�os;$kp++){
                if($gestion==$this->session->userdata('gestion')){
                    //'PROGRAMACIÓN ACUMULADA EN ';
                    for($pos=$i;$pos<=$meses;$pos++){
                        $programado = $cpp[$pos];
                    }
                    //'EJECUCIÓN ACUMULADA EN %';
                    for($pos=$i;$pos<=$meses;$pos++){
                        $ejecutado = $cpe[$pos];
                    }
                    // 'EFICACIA';
                    for($pos=$i;$pos<=$meses;$pos++){
                        if($cpp[$pos]==0){$cpp[$pos]=1;}
                        $porcentaje = round((($cpe[$pos]/$cpp[$pos])*100),1);
                    }
                }
                $i=$meses+1;
                $meses=$meses+12;
                $gestion++;
            }
        }
        return $programado.'|'.$ejecutado.'|'.$porcentaje;
    }
}