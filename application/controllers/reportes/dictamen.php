<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Dictamen extends CI_Controller 
{
    private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        footer {
            position: fixed;
            left: 0px;
            bottom: -94px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1.5px;
                text-align: center;
            }
        .mv{font-size:10px;}
        .siipp{width:150px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 9px;
        }
        .datos_principales {
            text-align: center;
            font-size: 9px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }
        .indi_desemp{
            font-size: 14px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:5px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 7px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .lista{
            text-align:left;
            padding-left: 8;
            list-style-type:square;
            margin:2px;
        }
    </style>';
    public function __construct ()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->library('pdf');
            $this->load->library('pdf2');
            $this->load->model('menu_modelo');
            $this->load->model('programacion/insumos/minsumos');
            $this->load->model('programacion/insumos/minsumos_delegado');
            $this->load->model('Users_model','',true);
            $this->load->model('reportes/model_objetivo');
            $this->load->model('programacion/insumos/minsumos');
            $this->load->model('programacion/insumos/minsumos_delegado');
        }else{
            redirect('/','refresh');
        }
    }
    public function busca_pdf($vector_archivos,$nombre_pdf,$ruta_pdfs)
    {
        $bool = false;
        for ($i = 0; $i < count($vector_archivos) ; $i++) {
            if($vector_archivos[$i] == $nombre_pdf){
                $bool = $this->fecha_creacion_pdf($ruta_pdfs,$nombre_pdf);
            }
        }
        return $bool;
    }
    public function fecha_creacion_pdf($ruta_pdfs,$nombre_pdf)
    {
        $bool = true;
        $directorio = opendir($ruta_pdfs);
        $nombre_archivo = $ruta_pdfs.''.$nombre_pdf;
        if (file_exists($nombre_archivo)) {
            $fecha_creacion = date("Yjn", filectime($nombre_archivo));
            $fecha_actual = date("Yjn");
            if($fecha_actual > $fecha_creacion){
                // $nombre_archivo = base_url().''.$nombre_archivo;
                unlink($nombre_archivo);
                $bool = false;
            } else{
                $bool = true;
            }
        } else {
            $bool = false;
        }
        closedir($directorio);
        return $bool;
    }
    public function pdf_dictamen_proyecto($id, $t_insumo)
    {
        $proy_id = $id;
        $gestion = $this->session->userdata('gestion');
        $ruta_pdfs = 'archivos_pdf/';
        $vector_archivos  = scandir($ruta_pdfs);
        $nombre_pdf = $proy_id.'_'.$gestion.'.pdf';
        // $existe_pdf = $this->busca_pdf($vector_archivos,$nombre_pdf,$ruta_pdfs);
        $existe_pdf = false;
        $archivo_pdf = base_url().''.$ruta_pdfs.''.$nombre_pdf;
        if(!$existe_pdf){
            $fecha_creacion_pdf = date('d/m/Y - H:i');
            $dictamen = $this->model_objetivo->get_dictamen($proy_id);
            $dictamen = $dictamen->row();
            $aper_programatica = $this->model_objetivo->get_aper_programatica_dictamen($proy_id,$gestion);
            $aper_programatica = $aper_programatica->row();
            $data_fase_activa = array(
                'pfec_id' => $dictamen->pfec_id, 
                'fas_fase' => $dictamen->fas_fase, 
                'eta_etapa' => $dictamen->eta_etapa,
                'pfec_descripcion' => $dictamen->pfec_descripcion,
                'fecha_inicial' => $dictamen->fecha_inicial,
                'fecha_final' => $dictamen->fecha_final,
                'ejecucion' => $dictamen->ejecucion,
                'anual_plurianual' => $dictamen->anual_plurianual,
                'tp_id' => $dictamen->tp_id
                );
            $num_principal = ($dictamen->tp_id == 1) ? 1 : 0 ;
            // $init = ini_get("memory_limit");
            // ini_set('memory_limit','512M');
            // '.$init.' =>'.$init2.'
            // $init2 = ini_get("memory_limit");
            $html = '';
            $html .= '
            <html>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
                <body>
                    <header>
                    </header>
                    <div class="rojo"></div>
                    <div class="verde"></div>
                    <table style="width:100%;" cellspacing="5">
                        <tr>
                            <td style="border-bottom:1px solid #aaa; padding-bottom:10px;" colspan="2">
                                <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
								<br>
								<span style="font-size:1.8em;">'.$this->session->userdata('entidad').'</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="titulo_pdf" style="width:410px !important; border-right:1px solid #aaa; padding-right:7px;">
<!--                                <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>-->
<!--                                <b>'.$this->session->userdata('sistema').'</b><br>-->
								<span style="font-size:1.6em;">
								<b>FORMULARIO : </b> REGISTRO DEL PROYECTO
								</span><br>
                                <b>PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                                <b>Fecha de Impresi&oacute;n : </b> '.$fecha_creacion_pdf.'<br>
                            </td>
                            <td>
                                <img style="width:310px !important; padding:0 !important; margin:0 !important;" src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                            </td>
                        </tr>
                    </table>
					<hr style="border:0; border-bottom:1px solid #aaa;">
                    <footer>
                        <table class="table table-bordered" width="100%">
                            <tr>
                                <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                                <td><p class="page">P&aacute;gina </p></td>
                            </tr>
                        </table>
                    </footer>
                    <table class="datos_principales" style="table-layout:fixed;" border="1">
                        <tr>
                            <td colspan="2">
                                <b>'.$dictamen->proy_nombre.'</b><br>
                                Proyecto/Actividad
                            </td>
                        </tr>
                        <tr>
                            <td style="width:50%;">
                                <b>'.$dictamen->proy_sisin.'</b><br>
                                SISIN
                            </td>
                            <td style="width:50%; text-align:center;">
                                <b>'.$aper_programatica->programatica.'</b><br>
                                Apertura Programatica
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align:center;">
                                <b>'.$aper_programatica->uni_unidad.'</b><br>
                                Unidad Ejecutora
                            </td>
                        </tr>
                    </table>
                    <br>
                    <div class="contenedor_principal">
                        '.$this->fase_etapa_activa_proyecto($data_fase_activa, $num_principal).'
                        <div class="titulo_dictamen"> '.($num_principal+=1).'. DATOS GENERALES </div>
                        <div class="contenedor_datos">
                            <table class="table_contenedor">
                                <tr class="collapse_t">
                                    <td style="width:18%"  class="fila_unitaria">Tipo</td>
                                    <td class="fila_unitaria">'.$dictamen->tp_tipo.'</td>
                                </tr>
                            </table>
                        </div>
                        <div class="titulo_dictamen"> '.($num_principal+=1).'. INDICADORES METAS </div>
                        <div class="contenedor_datos">
                            <table class="table_contenedor">'.$this->indicadores_metas($proy_id).'</table>
                        </div>
                        <div class="titulo_dictamen"> '.($num_principal+=1).'. RESPONSABLES </div>
                        <div class="contenedor_datos">
                            <table class="table_contenedor">
                                <tr class="collapse_t">
                                    <td style="width:18%" class="fila_unitaria">Responsable Tecnico</td>
                                    <td class="fila_unitaria">'.$dictamen->resp_tecnico.'</td>
                                </tr>
                                <tr class="collapse_t">
                                    <td class="fila_unitaria">Validador POA</td>
                                    <td class="fila_unitaria">'.$dictamen->val_poa.'</td>
                                </tr>
                                <tr class="collapse_t">
                                    <td class="fila_unitaria">Validador Financiero</td>
                                    <td class="fila_unitaria">'.$dictamen->val_fin.'</td>
                                </tr>
                            </table>
                        </div>
                        <div class="titulo_dictamen"> '.($num_principal+=1).'. LOCALIZACIÓN </div>
                        <div class="contenedor_datos">'.$this->localizacion($proy_id).'</div>
                        <div class="titulo_dictamen"> '.($num_principal+=1).'. POBLACIÓN BENEFICIARIA </div>
                        <div class="contenedor_datos">
                            <table class="table_contenedor">
                                <tr class="collapse_t">
                                    <td style="width:18%" class="fila_unitaria">Población Beneficiaria</td>
                                    <td class="fila_unitaria">'.$dictamen->proy_poblac_beneficiaria.'</td>
                                </tr>
                                <tr class="collapse_t">
                                    <td class="fila_unitaria">Area de Influencia</td>
                                    <td class="fila_unitaria">'.$dictamen->area_influencia.'</td>
                                </tr>
                                <tr class="collapse_t">
                                    <td class="fila_unitaria">Latitud</td>
                                    <td class="fila_unitaria">'.$dictamen->latitud.'</td>
                                </tr>
                                <tr class="collapse_t">
                                    <td class="fila_unitaria">Longitud</td>
                                    <td class="fila_unitaria">'.$dictamen->longitud.'</td>
                                </tr>
                                <tr class="collapse_t">
                                    <td class="fila_unitaria">Población</td>
                                    <td class="fila_unitaria">2.719.344</td>
                                </tr>
                                <tr class="collapse_t">
                                    <td class="fila_unitaria">Población Beneficiada</td>
                                    <td class="fila_unitaria">'.$dictamen->proy_poblac_beneficiada.'</td>
                                </tr>
                                <tr class="collapse_t">
                                    <td class="fila_unitaria">% Población Beneficiada</td>
                                    <td class="fila_unitaria">'.$dictamen->proy_porcent_poblac_beneficiada.'</td>
                                </tr>
                            </table>
                        </div>
                        <div class="titulo_dictamen"> '.($num_principal+=1).'. CLASIFICACIÓN </div>
                        <div class="contenedor_datos">
                            <table class="table_contenedor">
                                <tr class="collapse_t">
                                    <td style="width:18%;text-align:center;" class="fila_unitaria">PDES</td>
                                    <td class="fila_unitaria">
                                        <ul class="lista">
                                            <li><b>PILAR: </b>'.$dictamen->pdes_pilar.'</li>
                                            <li><b>META: </b>'.$dictamen->pdes_meta.'</li>
                                            <li><b>RESULTADO: </b>'.$dictamen->pdes_resultado.'</li>
                                            <li><b>ACCIÓN: </b>'.$dictamen->pdes_acccion.'</li>                                    
                                        </ul>
                                    </td>
                                </tr>
                                <tr class="collapse_t">
                                    <td style="width:18%;text-align:center;" class="fila_unitaria">PTDI</td>
                                    <td class="fila_unitaria">
                                        <ul class="lista">
                                            <li><b>EJE PROGRAMATICA: </b>'.$dictamen->ptdi_eje.'</li>
                                            <li><b>POLITICA: </b>'.$dictamen->ptdi_politica.'</li>
                                            <li><b>PROGRAMA: </b>'.$dictamen->ptdi_programa.'</li>                                
                                        </ul>
                                    </td>
                                </tr>
                                <tr class="collapse_t">
                                    <td style="width:18%;text-align:center;" class="fila_unitaria">Clasificación Sectorial</td>
                                    <td class="fila_unitaria">
                                        <ul class="lista">
                                            <li><b>SECTOR: </b>'.$dictamen->c_sect_sector.'</li>
                                            <li><b>SUBSECTOR: </b>'.$dictamen->c_sect_subsector.'</li>
                                            <li><b>ACTIVIDAD: </b>'.$dictamen->c_sect_actividad.'</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr class="collapse_t">
                                    <td style="width:18%;text-align:center;" class="fila_unitaria">Finalidad Función</td>
                                    <td class="fila_unitaria">
                                        <ul class="lista">
                                            <li><b>FINALIDAD: </b>'.$dictamen->fifu_finalidad.'</li>
                                            <li><b>FUNCIÓN: </b>'.$dictamen->fifu_funcion.'</li>
                                            <li><b>CLASE: </b>'.$dictamen->fifu_clase.'</li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="titulo_dictamen"> '.($num_principal+=1).'. MARCO LÓGICO </div>
                        <div class="contenedor_datos">
                            '.$this->control_longitud(1,$dictamen->proy_desc_problema,'Descripción del Problema').'
                            '.$this->control_longitud(1,$dictamen->proy_desc_solucion,'Descripción de la Solución').'
                            '.$this->control_longitud(1,$dictamen->proy_obj_general,'Objetivo General').'
                            '.$this->control_longitud(1,$dictamen->proy_obj_especifico,'Objetivo Específico').'
                        </div>
                        <div class="titulo_dictamen"> '.($num_principal+=1).'. RESUMEN TÉCNICO DEL PROYECTO</div>
                        '.$this->control_longitud(2,$dictamen->proy_descripcion_proyecto,'Descripción del Proyecto').'
                        '.$this->presupuesto($proy_id, $dictamen->pfec_id, $gestion,$num_principal).'
                        '.$this->componente($proy_id, $dictamen->pfec_id,$gestion,$num_principal).'
                        '.$this->dictamen_insumos($proy_id,$t_insumo,$gestion,$num_principal).'
                     
                    </div>
                </body>
            </html>';
            $dompdf = new DOMPDF();
            $dompdf->load_html($html);
            $dompdf->set_paper('letter', 'portrait');
            ini_set('memory_limit','512M');
            ini_set('max_execution_time', 900);
            $dompdf->render();
            file_put_contents('archivos_pdf/'.$nombre_pdf ,$dompdf->output());
            // $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
        }
        header("Content-type:application/pdf");
        header("Content-Disposition:inline; filename='$nombre_pdf");
        readfile($archivo_pdf);
    }

    public function fase_etapa_activa_proyecto($data, $num_principal)
    {
        $fase_etapa_activa = '';
        $fase_etapa_activa .= '';
        if($data['pfec_id'] != -1){
            if($data['tp_id']==1){
                $fase_etapa_activa .= '
                    <div class="titulo_dictamen"> 1. FASE ETAPA ACTIVA </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="background-color:#ac9fae;">
                                <td>FASE</td>
                                <td>ETAPA</td>
                                <td>DESCRIPCIÓN</td>
                                <td>F. INICIAL</td>
                                <td>F. FINAL</td>
                                <td>EJECUCIÓN</td>
                                <td>ANUAL/PLURIANUAL</td>
                            </tr>
                            <tr class="collapse_t">
                                <td>'.$data['fas_fase'].'</td>
                                <td>'.$data['eta_etapa'].'</td>
                                <td>'.$data['pfec_descripcion'].'</td>
                                <td>'.$data['fecha_inicial'].'</td>
                                <td>'.$data['fecha_final'].'</td>
                                <td>'.$data['ejecucion'].'</td>
                                <td>'.$data['anual_plurianual'].'</td>
                            </tr>
                        </table>    
                    </div>
                ';    
            }
            else{
                $fase_etapa_activa .= '';    
            }
        }else{
            $fase_etapa_activa .= '
                <div class="titulo_dictamen"> 1. FASE ETAPA ACTIVA </div>
                <div class="contenedor_datos">
                    <table class="table_contenedor">
                        <tr>
                            <td>Sin Fase Etapa Activa</td>
                        </tr>
                    </table>    
                </div>
            ';
        };
        return $fase_etapa_activa;
    }

    public function indicadores_metas($id)
    {
        $proy_id = $id;
        $query = $this->model_objetivo->get_indicadores_metas($proy_id);
        $count = $query->num_rows();
        $query = $query->result_array();
        $html_metas = '';
        if( $count >= 1 ){
            $html_metas ='
                <tr class="collapse_t" style="background-color:#ac9fae;">
                    <td>#</td>
                    <td>DESCRIPCIÓN META</td>
                    <td>META</td>
                    <td>EJECUCIÓN</td>
                    <td>EFICACIA</td>
                    <td>TIPO</td>
                </tr>';
            $n=0;
            foreach($query as $fila){
                $n++;
                $html_metas .='
                <tr class="collapse_t">
                    <td>'.$n.'</td>
                    <td>'.$fila['meta_descripcion'].'</td>
                    <td>'.$fila['meta_meta'].'</td>
                    <td>'.$fila['meta_ejec'].'</td>
                    <td>'.$fila['meta_efic'].'</td>
                    <td>'.$fila['tipo'].'</td>
                </tr>'; 
            };
        }else{
            $html_metas = '
                <tr class="collapse_t">
                    <td>Sin Metas Registradas</td>
                </tr>
            ';
        }
        return $html_metas;
    }

    public function presupuesto($id, $fase_activa, $gest, $num_principal)
    {
        $proy_id = $id;
        $gestion = $gest;
        if($fase_activa != -1){
            $query = $this->model_objetivo->get_presupuesto_dictamen($proy_id,$gestion);
            $n = $query->num_rows();
            $query = $query->row();
            if($n>0){
                $html_presupuesto = '';
                $html_presupuesto .= '
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. PRESUPUESTO</div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                <td style="width:21%" class="fila_unitaria">COSTO TOTAL</td>
                                <td class="fila_unitaria">'.$query->pfec_ptto_fase.' Bs.</td>
                                <td style="width:21%" class="fila_unitaria">PRESUPUESTO EJECUTADO</td>
                                <td class="fila_unitaria">'.$query->pfec_ptto_fase_e.' Bs.</td>
                            </tr>
                            <tr class="collapse_t">
                                <td style="width:22%" class="fila_unitaria">PRESUPUESTO GESTIÓN '.$gestion.'</td>
                                <td class="fila_unitaria">'.$query->pres_gestion.' Bs.</td>
                                <td style="width:22%" class="fila_unitaria">PRESUPUESTO EJECUTADO '.$gestion.'</td>
                                <td class="fila_unitaria">'.$query->pres_ejecutado_ges.' Bs.</td>
                            </tr>
                        </table>
                    </div>
                ';
            }
            else{
                $html_presupuesto = '';
                $html_presupuesto .= '
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. PRESUPUESTO</div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="font-weight: bold;">
                                <td> Sin Presupuesto para esta gestion</td>
                            </tr>
                        </table>
                    </div>
                ';
            }
        }else{
            $html_presupuesto = '';
        };
        return $html_presupuesto;
    }

    public function localizacion($id)
    {
        $proy_id = $id;
        $query = $this->model_objetivo->get_localizacion_dictamen($proy_id);
        $n = $query->num_rows();
        $html_localizacion = '';
        if($n>0){
            $query = $query->result_array();
            $html_localizacion .= '';
            foreach($query as $fila){
                $html_localizacion .='
                <table class="table_contenedor">
                    <tr class="collapse_t">
                        <td style="width:18%">
                            '.$fila['dep_departamento'].'
                        </td>
                        <td style="width:18%">
                            Provincia: '.$fila['prov_provincia'].'
                        </td>
                        <td>
                            <table class="table_contenedor">
                                <tr style="font-size:7px;background-color:#ac9fae;">
                                    <td style="height:10px;">REGIÓN</td>
                                    <td>MUNICIPIO</td>
                                    <td>%P</td>
                                    <td>POB. HOMBRES</td>
                                    <td>POB. MUJERES</td>
                                    <td style="width:15%;">Cantones</td>
                                </tr>
                                <tr>
                                    <td>'.$fila['reg_region'].'</td>
                                    <td>'.$fila['muni_municipio'].'</td>
                                    <td>'.$fila['pm_pondera'].'%</td>
                                    <td>'.$fila['muni_poblacion_hombres'].'</td>
                                    <td>'.$fila['muni_polacion_mujeres'].'</td>
                                    <td>'.$fila['cantones'].'</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>'; 
            };
        }else{
            $html_localizacion .='
                <table class="table_contenedor">
                    <tr class="collapse_t" style="font-weight: bold;">
                        <td> Sin Localización</td>
                    </tr>
                </table>';
        }
        return $html_localizacion;
    }

    public function componente($id, $fase_activa, $gestion,$num_principal)
    {
        $proy_id = $id;
        if($fase_activa != -1){
            $componentes = $this->model_objetivo->get_componentes_dictamen($proy_id);
            $n = $componentes->num_rows();
            $div_componente = '<div class="titulo_dictamen"> '.($num_principal+=2).'. COMPONENTES</div>';
            $tbl_componente = '';
            if($n>0){
                $componentes = $componentes->result_array();
                $n_componente = 0;
                foreach($componentes as $fila){
                    $n_componente++;
                    $tbl_componente .='
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:75%;" class="fila_unitaria titulo_dictamen">
                                    <b> '.$n_componente.'. COMPONENTE</b>
                                </td>
                                <td style="width:25%;"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%;background-color:#ac9fae;font-weight: bold;">
                                    <b>COMPONENTE</b>
                                </td>
                                <td class="fila_unitaria">'.$fila['com_componente'].'</td>
                                <td style="width:18%;background-color:#ac9fae;font-weight: bold;"> 
                                    <b>PONDERACIÓN %</b>
                                </td>
                                <td class="fila_unitaria">'.$fila['com_ponderacion'].'</td>
                            </tr>
                        </table>
                    </div>
                    '.$this->producto($fila['com_id'],$gestion,$n_componente).' '; 
                };
                $div_componente .= $tbl_componente;
            }else{
                $tbl_componente .='
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="font-weight: bold;">
                                <td>Sin Componentes</td>
                            </tr>
                        </table>
                    </div>';
                $div_componente .= $tbl_componente;
            }
        }else{
            $div_componente = '';
        }
        return $div_componente;
    }

    public function producto($id, $gestion,$n_componente)
    {
        $com_id = $id;
        $productos = $this->model_objetivo->get_productos_dictamen($com_id);
        if ($productos->num_rows()>0) {
            $productos = $productos->result_array();
            $div_producto = '<div class="titulo_dictamen"> 12. PRODUCTOS</div>';
            $div_producto = '';
            $tbl_producto = '';
            $n_producto = 0;
            $div_producto .= '';
            foreach ($productos as $fila) {
                $n_producto++;
                // $titulo_producto .='';
                $tbl_producto .='
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:50%;" class="fila_unitaria titulo_dictamen">
                                    <b> '.$n_componente.'.'.$n_producto.'. PRODUCTO</b>
                                </td>
                                <td style="width:50%;"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                <td style="width:4%;">Nro.</td>
                                <td>OBJETIVO DEL PRODUCTO</td>
                                <td style="width:4%;">T.I.</td>
                                <td>INDICADOR</td>
                                <td>L/B</td>
                                <td>META</td>
                                <td>%P</td>
                                <td style="width:50%;">TEMPORALIZACIÓN DE LA META</td>
                            </tr>
                            <tr class="collapse_t">
                                <td style="width:4%;">'.$n_producto.'</td>
                                <td>'.$fila['pro_producto'].'</td>
                                <td style="width:4%;">'.$fila['indi_abreviacion'].'</td>
                                <td>'.$fila['prod_indicador'].'</td>
                                <td>'.$fila['prod_linea_base'].'</td>
                                <td>'.$fila['prod_meta'].'</td>
                                <td>'.$fila['prod_ponderacion'].'</td>
                                <td style="vertical-align: top;" style="width:50%;"> '.$this->temporalizacion_metas_producto($fila['prod_id'],$gestion).'</td>
                            </tr>
                        </table>
                    </div>
                    '.$this->actividad($fila['prod_id'],$gestion,$n_componente,$n_producto).'';
            };
            $div_producto .= $tbl_producto;
        }else{
            $div_producto = '';
        };     
        
        return $div_producto;
    }

    public function temporalizacion_metas_producto($prod_id, $gestion)
    {

        $tbl_tmp_producto = '';
        $dato = $this->model_objetivo->get_temp_metas_productos($prod_id,$gestion);
        $programado = $dato['programado'];
        $programado = $programado->row();
        $ejecutado = $dato['ejecutado'];
        $ejecutado = $ejecutado->row();
        $tbl_tmp_producto .='
            <table class="table_contenedor">
                <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                    <td colspan="13">Gestión '.$gestion.'</td>
                </tr>
                <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                    <td style="width:3%;">P/E</td>
                    <td>Ene.</td>
                    <td>Feb.</td>
                    <td>Mar.</td>
                    <td>Abr.</td>
                    <td>May.</td>
                    <td>Jun.</td>
                    <td>Jul.</td>
                    <td>Ago.</td>
                    <td>Sep.</td>
                    <td>Oct.</td>
                    <td>Nov.</td>
                    <td>Dic.</td>
                </tr>
                <tr>
                    <td style="background-color:#ac9fae;font-weight: bold;">P</td>
                    <td>'.$programado->enero.'</td>
                    <td>'.$programado->febrero.'</td>
                    <td>'.$programado->marzo.'</td>
                    <td>'.$programado->abril.'</td>
                    <td>'.$programado->mayo.'</td>
                    <td>'.$programado->junio.'</td>
                    <td>'.$programado->julio.'</td>
                    <td>'.$programado->agosto.'</td>
                    <td>'.$programado->septiembre.'</td>
                    <td>'.$programado->octubre.'</td>
                    <td>'.$programado->noviembre.'</td>
                    <td>'.$programado->diciembre.'</td>
                </tr>
                <tr>
                    <td style="background-color:#ac9fae;font-weight: bold;">E</td>
                    <td>'.$ejecutado->enero.'</td>
                    <td>'.$ejecutado->febrero.'</td>
                    <td>'.$ejecutado->marzo.'</td>
                    <td>'.$ejecutado->abril.'</td>
                    <td>'.$ejecutado->mayo.'</td>
                    <td>'.$ejecutado->junio.'</td>
                    <td>'.$ejecutado->julio.'</td>
                    <td>'.$ejecutado->agosto.'</td>
                    <td>'.$ejecutado->septiembre.'</td>
                    <td>'.$ejecutado->octubre.'</td>
                    <td>'.$ejecutado->noviembre.'</td>
                    <td>'.$ejecutado->diciembre.'</td>
                </tr>
            </table>
        ';
        return $tbl_tmp_producto;
    }

    public function actividad($prod_id, $gestion,$n_componente,$n_producto)
    {
        $actividades = $this->model_objetivo->get_actividades_dictamen($prod_id);
        if ($actividades->num_rows()>0) {
            $actividades = $actividades->result_array();
            $div_actividad = '<div class="titulo_dictamen"> 13. ACTIVIDADES</div>';
            $div_actividad = '';
            $tbl_actividad = '';
            $n_actividad = 0;
            foreach ($actividades as $fila) {
                $n_actividad++;
                $tbl_actividad .= '
                <div class="contenedor_datos">
                    <table class="table_contenedor">
                        <tr class="collapse_t">
                            <td style="width:25%;" class="fila_unitaria titulo_dictamen">
                                <b> '.$n_componente.'.'.$n_producto.'.'.$n_actividad.'. ACTIVIDAD</b>
                            </td>
                            <td style="width:75%;"></td>
                        </tr>
                    </table>
                </div>
                <div class="contenedor_datos">
                    <table class="table_contenedor">
                        <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                            <td>OBJETIVO DE LA ACTIVIDAD</td>
                            <td>T.I.</td>
                            <td>F. INICIO</td>
                            <td>F. FINAL</td>
                            <td>DURACIÓN</td>
                            <td>COSTO</td>
                            <td>%P</td>
                            <td>L/B</td>
                            <td>META</td>
                            <td style="width:43%;">TEMPORALIZACIÓN DE LA META '.$fila['act_id'].'</td>
                        </tr>
                        <tr class="collapse_t">
                            <td>'.$fila['act_actividad'].'</td>
                            <td>'.$fila['indi_abreviacion'].'</td>
                            <td>'.$fila['f_inicio'].'</td>
                            <td>'.$fila['f_final'].'</td>
                            <td>'.$fila['act_duracion'].'</td>
                            <td>'.$fila['act_presupuesto'].' Bs.</td>
                            <td>'.$fila['act_ponderacion'].'</td>
                            <td>'.$fila['act_linea_base'].'</td>
                            <td>'.$fila['act_meta'].'</td>
                            <td style="width:43%;vertical-align: top;">'.$this->temporalizacion_metas_actividad($fila['act_id'],$gestion).'</td>
                        </tr>
                    </table>
                </div>';
            };
            $div_actividad .= $tbl_actividad;
        }else{
            $div_actividad = '';
        }
        return $div_actividad;
    }

    public function temporalizacion_metas_actividad($act_id, $gestion)
    {
        $tbl_tmp_actividad = '';
        $dato = $this->model_objetivo->get_temp_metas_actividades($act_id,$gestion);
        $programado = $dato['programado'];
        $programado = $programado->row();
        $ejecutado = $dato['ejecutado'];
        $ejecutado = $ejecutado->row();
        $tbl_tmp_actividad .='
            <table class="table_contenedor">
                <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                    <td colspan="7">Gestión '.$gestion.'</td>
                </tr>
                <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                    <td style="width:3%;">P/E</td>
                    <td>Ene.</td>
                    <td>Feb.</td>
                    <td>Mar.</td>
                    <td>Abr.</td>
                    <td>May.</td>
                    <td>Jun.</td>
                </tr>
                <tr>
                    <td style="background-color:#ac9fae;font-weight: bold;">P</td>
                    <td>'.$programado->enero.'</td>
                    <td>'.$programado->febrero.'</td>
                    <td>'.$programado->marzo.'</td>
                    <td>'.$programado->abril.'</td>
                    <td>'.$programado->mayo.'</td>
                    <td>'.$programado->junio.'</td>
                </tr>
                <tr>
                    <td style="background-color:#ac9fae;font-weight: bold;">E</td>
                    <td>'.$ejecutado->enero.'</td>
                    <td>'.$ejecutado->febrero.'</td>
                    <td>'.$ejecutado->marzo.'</td>
                    <td>'.$ejecutado->abril.'</td>
                    <td>'.$ejecutado->mayo.'</td>
                    <td>'.$ejecutado->junio.'</td>
                </tr>
                <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                    <td>P/E</td>
                    <td>Jul.</td>
                    <td>Ago.</td>
                    <td>Sep.</td>
                    <td>Oct.</td>
                    <td>Nov.</td>
                    <td>Dic.</td>
                </tr>
                <tr>
                    <td style="background-color:#ac9fae;font-weight: bold;">P</td>
                    <td>'.$programado->julio.'</td>
                    <td>'.$programado->agosto.'</td>
                    <td>'.$programado->septiembre.'</td>
                    <td>'.$programado->octubre.'</td>
                    <td>'.$programado->noviembre.'</td>
                    <td>'.$programado->diciembre.'</td>
                </tr>
                <tr>
                    <td style="background-color:#ac9fae;font-weight: bold;">E</td>
                    <td>'.$ejecutado->julio.'</td>
                    <td>'.$ejecutado->agosto.'</td>
                    <td>'.$ejecutado->septiembre.'</td>
                    <td>'.$ejecutado->octubre.'</td>
                    <td>'.$ejecutado->noviembre.'</td>
                    <td>'.$ejecutado->diciembre.'</td>
                </tr>
            </table>
        ';
        return $tbl_tmp_actividad;
    }
    public function control_longitud($tipo,$texto,$titulo)
    {
        $html = '';
        $long_str = strlen($texto);
        if($tipo == 1){
            if($long_str >= 4500) {
                $html = '
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%" class="fila_unitaria">'.$titulo.'</td>
                            </tr>
                        </table>
                        <span style="margin:-10px 10px 0px 10px;font-size:9px;">
                            <p>'.$texto.'</p>
                        </span>
                    </div>
                ';
            } else {
                $html = '
                    <table>
                        <tr class="collapse_t">
                            <td style="width:18%" class="fila_unitaria">'.$titulo.'</td>
                            <td class="fila_unitaria">'.$texto.'</td>
                        </tr>
                    </table>
                ';
            }
        } else {
            if($long_str >= 6500) {
                $html = '
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%" class="fila_unitaria">'.$titulo.'</td>
                            </tr>
                        </table>
                        <span style="margin:-10px 10px 0px 10px;font-size:9px;">
                            <p>'.$texto.'</p>
                        </span>
                    </div>
                ';
            } else {
                $html = '
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%" class="fila_unitaria">'.$titulo.'</td>
                                <td class="fila_unitaria">'.$texto.'</td>
                            </tr>
                        </table>
                    </div>
                ';
            }
        }
        return $html;
    }
    //**************INSUMOS****************//
    public function dictamen_insumos($proy_id, $t_insumo,$gestion,$num_principal)
    {
        $html_insumos = '';
        if ($t_insumo != 0) {
            $num_principal += 3;
            $html_insumos = '<div class="titulo_dictamen"> '.($num_principal).'. INSUMOS</div>';
            if($t_insumo == 1){
                $tbl_insumo = '';
                $lista_productos = $this->minsumos->lista_productos($proy_id, $gestion);
                $cont = 0;
                foreach ($lista_productos as $fila) {
                    $prod_id = $fila['prod_id'];
                    $tbl_act = $this->insumos_directo_productos($proy_id,$prod_id,$gestion);
                    if($tbl_act != ''){
                        $cont++;
                        $enum_insumos = ($num_principal).'.'.$cont;
                        $tbl_insumo .= '
                            <div class="contenedor_datos">
                                <table class="table_contenedor">
                                    <tr class="collapse_t">
                                        <td style="width:15%;" class="fila_unitaria titulo_dictamen">
                                            <b> '.$enum_insumos.'. PRODUCTO</b>
                                        </td>
                                        <td style="width:85%;">'.$fila['prod_producto'].'</td>
                                    </tr>
                                </table>
                            </div>';
                        $tbl_insumo .= $tbl_act;
                    }
                }
                $html_insumos .= $tbl_insumo;
                if($tbl_insumo == ''){
                    $html_insumos .= '
                        <div class="contenedor_datos">
                            <table class="table_contenedor">
                                <tr class="collapse_t">
                                    <td style="width:100%" class="fila_unitaria">
                                        SIN INSUMOS
                                    </td>
                                </tr>
                            </table>
                        </div>';
                }
            } else {
                $tbl_insumos = '';
                $lista_componentes = $this->minsumos_delegado->lista_componentes($proy_id);
                $cont = 0;
                foreach ($lista_componentes AS $row) {
                    $cont++;
                    $enum_insumos = ($num_principal).'.'.$cont;
                    $com_id = $row['com_id'];
                    $dato_com = $this->minsumos_delegado->get_componente($com_id);
                    $tbl_insumos .= '
                        <div class="contenedor_datos">
                            <table class="table_contenedor">
                                <tr class="collapse_t">
                                    <td style="width:45%;" class="fila_unitaria titulo_dictamen">
                                        <b> '.$enum_insumos.'. COMPONENTE</b>
                                    </td>
                                    <td style="width:55%;">'.$dato_com->com_componente.'</td>
                                </tr>
                            </table>
                        </div>
                        '.$this->contenido_insumos($com_id).' ';
                }
                $html_insumos .= $tbl_insumos;
            }
        } else {
            $html_insumos = '';
        }
        return $html_insumos;
    }
    public function contenido_insumos($com_id)
    {
        $cont_insumos = '';
        $lista_af = $this->minsumos_delegado->lista_activos_fijos($com_id);
        foreach ($lista_af as $fila) {
            $cont_insumos .= '
            <div class="contenedor_datos">
                <table class="table_contenedor">
                    <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                        <td class="fila_unitaria">CODIGO</td>
                        <td class="fila_unitaria">FECHA REQUERIMIENTO</td>
                        <td class="fila_unitaria">DESCRIPCIÓN</td>
                        <td class="fila_unitaria">CANTIDAD</td>
                        <td class="fila_unitaria">COSTO UNITARIO</td>
                        <td class="fila_unitaria">COSTO TOTAL</td>
                        <td class="fila_unitaria">PARTIDA</td>
                    </tr>
                    <tr class="collapse_t">
                        <td class="fila_unitaria">'.$fila['ins_codigo'] .'</td>
                        <td class="fila_unitaria">'.$fila['ins_fecha_requerimiento'].'</td>
                        <td class="fila_unitaria">'.$fila['ins_detalle'].'</td>
                        <td class="fila_unitaria">'.$fila['ins_cant_requerida'].'</td>
                        <td class="fila_unitaria">'.$fila['ins_costo_unitario'].'</td>
                        <td class="fila_unitaria">'.$fila['ins_costo_total'].'</td>
                        <td class="fila_unitaria">'.$fila['par_codigo'] .'</td>
                    </tr>
                    <tr class="collapse_t">
                        <td colspan="7">
                            '.$this->get_tabla_ins_progmensual_delegado($fila['ins_id']).'
                        </td>
                    </tr>
                </table>
            </div>
            ';
        }
        return $cont_insumos;
    }

        public function get_tabla_ins_progmensual_delegado($insg_id)
        {
            $list_prog_mensual = $this->minsumos->get_list_insumo_financiamiento($insg_id);
          //  $prog_mensual = $this->minsumos->lista_progmensual_ins($ins_id);
            $tabla = '
                <table class="table_contenedor">
                    <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                        <td colspan="16">
                            PROGRAMACIÓN FINANCIERA MENSUAL
                        </td>
                    </tr>
                    <tr class="collapse_t">
                        <td style="background-color: #0AA699;">MONTO</td>
                        <td style="background-color: #0AA699;">FF</td>
                        <td style="background-color: #0AA699;">OF</td>
                        <td style="background-color: #0AA699;">ET</td>
                        <td style="background-color: #0AA699;">Enero</td>
                        <td style="background-color: #0AA699;">Febrero</td>
                        <td style="background-color: #0AA699;">Marzo</td>
                        <td style="background-color: #0AA699;">Abril</td>
                        <td style="background-color: #0AA699;">Mayo</td>
                        <td style="background-color: #0AA699;">Junio</td>
                        <td style="background-color: #0AA699;">Julio</td>
                        <td style="background-color: #0AA699;">Agosto</td>
                        <td style="background-color: #0AA699;">Septiembre</td>
                        <td style="background-color: #0AA699;">Octubre</td>
                        <td style="background-color: #0AA699;">Noviembre</td>
                        <td style="background-color: #0AA699;">Diciembre</td>
                    </tr>';

                    if(count($list_prog_mensual)!=0){
                        foreach ($list_prog_mensual as $row) {
                            $tabla .= '<tr>';
                            $tabla .= '<td>' . $row['ifin_monto'] . '</td>';
                            $tabla .= '<td>' . $row['ff_codigo'] . '</td>';
                            $tabla .= '<td>' . $row['of_codigo'] . '</td>';
                            $tabla .= '<td>' . $row['et_codigo'] . '</td>';
                            $tabla .= '<td>' . number_format($row['mes1'], 2, ',', '.') . '</td>';
                            $tabla .= '<td>' . number_format($row['mes2'], 2, ',', '.') . '</td>';
                            $tabla .= '<td>' . number_format($row['mes3'], 2, ',', '.') . '</td>';
                            $tabla .= '<td>' . number_format($row['mes4'], 2, ',', '.') . '</td>';
                            $tabla .= '<td>' . number_format($row['mes5'], 2, ',', '.') . '</td>';
                            $tabla .= '<td>' . number_format($row['mes6'], 2, ',', '.') . '</td>';
                            $tabla .= '<td>' . number_format($row['mes7'], 2, ',', '.') . '</td>';
                            $tabla .= '<td>' . number_format($row['mes8'], 2, ',', '.') . '</td>';
                            $tabla .= '<td>' . number_format($row['mes9'], 2, ',', '.') . '</td>';
                            $tabla .= '<td>' . number_format($row['mes10'], 2, ',', '.') . '</td>';
                            $tabla .= '<td>' . number_format($row['mes11'], 2, ',', '.') . '</td>';
                            $tabla .= '<td>' . number_format($row['mes12'], 2, ',', '.') . '</td>';
                            $tabla .= '</tr>';
                        }
                    }
                    
                    $tabla .= '</table>';
                    return $tabla;
    }
    public function insumos_directo_productos($proy_id,$prod_id,$gestion)
    {
        $lista_actividad = $this->minsumos->lista_actividades($prod_id, $gestion);
        $tbl_act = '';
        $n_act = 0;
        foreach ($lista_actividad as $row) {
            $act_id = $row['act_id'];
            $ins_contenido = $this->insumos_directo($proy_id,$prod_id,$act_id);
            if($ins_contenido != ''){
                $n_act++;
                $tbl_act .= '
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:10%;background-color:#ac9fae;font-weight: bold;"  class="fila_unitaria">
                                    <span style="color:white;">'.$n_act.' Actividad</span>
                                </td>
                                <td style="width:90%" class="fila_unitaria">
                                    '.$row['act_actividad'].'
                                </td>
                            </tr>
                        </table>
                    </div>
                    '.$ins_contenido.' ';
            }  
        }
        return $tbl_act;
    }

    public function insumos_directo($proy_id,$prod_id,$act_id)
    {
        $html_directo = '';
        $nombres_tipo = array(
            'Tipos de Insumos Directos',
            'Determinación de Recursos Humanos Permanentes',
            'Determinación de Servicios',
            'Determinación de Pasajes',
            'Determinación de Viáticos',
            'Determinación de Consultorias por Producto',
            'Determinación de Consultorias en Linea',
            'Determinación de Materiales y Suministros',
            'Determinación de Activos Fijos',
            'Determinación de Otros Insumos');
        for ($i=1; $i <=9 ; $i++) {
            $data = $this->minsumos->lista_insumos_tipo($act_id,$i);
            $cont_insumos = '';
            foreach ($data as $fila) {
                $insg = $this->minsumos->get_dato_insumo_gestion_actual($fila['ins_id']);
                if(count($insg)!=0){
                    $tabla_presupuesto_programado=$this->get_tabla_ins_progmensual_directo($insg[0]['insg_id']);
                }
                else{
                    $tabla_presupuesto_programado='';
                }
                $cont_insumos .= '
                <div class="contenedor_datos">
                    <table class="table_contenedor">
                        <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                            <td style="width:18%;" class="fila_unitaria">CODIGO</td>
                            <td style="width:10%;" class="fila_unitaria">FECHA REQUERIMIENTO</td>
                            <td class="fila_unitaria">DESCRIPCIÓN</td>
                            <td style="width:6%;" class="fila_unitaria">CANTIDAD</td>
                            <td class="fila_unitaria">COSTO UNITARIO</td>
                            <td class="fila_unitaria">COSTO TOTAL</td>
                            <td class="fila_unitaria">PARTIDA</td>
                            <td class="fila_unitaria">TIPO</td>
                        </tr>
                        <tr class="collapse_t">
                            <td class="fila_unitaria">'.$fila['ins_codigo'] .'</td>
                            <td class="fila_unitaria">'.$fila['ins_fecha_requerimiento'].'</td>
                            <td class="fila_unitaria">'.$fila['ins_detalle'].'</td>
                            <td class="fila_unitaria">'.$fila['ins_cant_requerida'].'</td>
                            <td class="fila_unitaria">'.$fila['ins_costo_unitario'].'</td>
                            <td class="fila_unitaria">'.$fila['ins_costo_total'].'</td>
                            <td class="fila_unitaria">'.$fila['par_codigo'] .'</td>
                            <td class="fila_unitaria">'.$nombres_tipo[$i].'</td>
                        </tr>
                        <tr class="collapse_t">
                            <td colspan="8">
                            '.$tabla_presupuesto_programado.'
                            </td>
                        </tr>
                    </table>
                </div>
                ';
            }
            $html_directo .= $cont_insumos;
        }
        // if ($html_directo == '') {
        //     $html_directo = '
        //         <div class="contenedor_datos">
        //             <table class="table_contenedor">
        //                 <tr class="collapse_t">
        //                     <td style="width:100%; text-align:center;" class="fila_unitaria">
        //                         Sin Registro de Insumos
        //                     </td>
        //                 </tr>
        //             </table>
        //         </div>
        //     ';
        // }
        return $html_directo;
    }

    public function get_tabla_ins_progmensual_directo($insg_id)
    {

        $list_prog_mensual = $this->minsumos->get_list_insumo_financiamiento($insg_id);
        $tabla = '
            <table class="table_contenedor">
                <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                    <td colspan="16">
                        PROGRAMACIÓN FINANCIERA MENSUAL
                    </td>
                </tr>
                <tr class="collapse_t">
                    <td style="background-color: #0AA699;">MONTO</td>
                    <td style="background-color: #0AA699;">FF</td>
                    <td style="background-color: #0AA699;">OF</td>
                    <td style="background-color: #0AA699;">ET</td>
                    <td style="background-color: #0AA699;">Enero</td>
                    <td style="background-color: #0AA699;">Febrero</td>
                    <td style="background-color: #0AA699;">Marzo</td>
                    <td style="background-color: #0AA699;">Abril</td>
                    <td style="background-color: #0AA699;">Mayo</td>
                    <td style="background-color: #0AA699;">Junio</td>
                    <td style="background-color: #0AA699;">Julio</td>
                    <td style="background-color: #0AA699;">Agosto</td>
                    <td style="background-color: #0AA699;">Septiembre</td>
                    <td style="background-color: #0AA699;">Octubre</td>
                    <td style="background-color: #0AA699;">Noviembre</td>
                    <td style="background-color: #0AA699;">Diciembre</td>
                </tr>';
        if(count($list_prog_mensual)!=0)
        {
            foreach ($list_prog_mensual as $row) {
                $tabla .= '<tr>';
                $tabla .= '<td>' . $row['ifin_monto'] . '</td>';
                $tabla .= '<td>' . $row['ff_codigo'] . '</td>';
                $tabla .= '<td>' . $row['of_codigo'] . '</td>';
                $tabla .= '<td>' . $row['et_codigo'] . '</td>';
                $tabla .= '<td>' . number_format($row['mes1'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes2'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes3'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes4'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes5'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes6'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes7'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes8'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes9'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes10'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes11'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes12'], 2, ',', '.') . '</td>';
                $tabla .= '</tr>';
            }
        }
        
        $tabla .= '</table>';
        return $tabla;
    }
}