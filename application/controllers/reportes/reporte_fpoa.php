<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Reporte_fpoa extends CI_Controller { 
  public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf');
        $this->load->library('pdf2');

        $this->load->model('programacion/model_reporte');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/marco_estrategico/mobjetivos');
        $this->load->model('reportes/seguimiento/model_reporte_seguimiento');
        $this->load->model('modificacion/model_modificacion');
        $this->load->model('reportes/model_objetivo');
        $this->load->model('programacion/model_reporte_fpoa');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/model_producto');
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('programacion/prog_poa/mp_terminal');
        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);
        }else{
            redirect('/','refresh');
        }
    }
        private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 8px;
                color: #ffffff;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1px;
                text-align: center;
                font-size: 8px;
            }
        .mv{font-size:10px;}
        .siipp{width:200px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .datos_principales {
            text-align: center;
            font-size: 9px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }

        .indi_desemp{
            font-size: 14px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 10px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 1px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .lista{
            text-align:left;
            padding-left: 8;
            list-style-type:square;
            margin:2px;
        }
    </style>';
    /*===================================== OBJETIVOS ESTRATEGICOS ===========================================*/
    public function iframe_objetivo_estrategico()
    {
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];
        $id_mes=$this->session->userdata("mes");
        $data['id_mes'] = $id_mes;
        $data['mes'] = $mes;
        $data['dias'] = $dias;

        $this->load->view('admin/reportes/formularios_poa/obj_estrategico/iframe_obj', $data);
    }

/*---------------------------------- REPORTE PDF OBJETIVO ESTRATEGICO -----------------------------*/
    public function pdf_objetivo_estrategico()
    {  
        $gestion=$this->session->userdata('gestion');
        

        $mision_vision = $this->model_objetivo->get_dato_configuracion($gestion);
        $mision_vision = $mision_vision->row();
        $mision = $mision_vision->conf_mision;
        $vision = $mision_vision->conf_vision;
        $gestion_base = $mision_vision->conf_gestion_base;
        $rango = $mision_vision->conf_gestion_desde.' - '.$mision_vision->conf_gestion_hasta;
        

        $lista_objetivos = $this->mobjetivos->lista_objetivos($gestion);

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('name').'</b><br>
                            <b>FORMULARIO POA : </b> OBJETIVO ESTRATEGICO '.$rango.'<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <div class="mv" style="text-align:justify">
                    <b>MISI&Oacute;N: </b> <br>
                    '.$mision.'
                </div><br>
                <div class="mv" style="text-align:justify">
                    <b>VISI&Oacute;N: </b> <br>
                    '.$vision.'
                </div>
                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <br><br>
                ';
                $nro=1;
                foreach($lista_objetivos as $fila)
                {
                  $html .= '
                  <table class="datos_principales" style="table-layout:fixed;" border="1">
                        <tr bgcolor="#696969">
                            <td style="width:1%;" class="header_table">Nro</td>
                            <td style="width:20%;" class="header_table"> PDES. </td>
                            <td style="width:10%;" class="header_table">Objetivo Estrat&eacute;gico</td>
                            <td style="width:6.7%;" class="header_table">Tipo de Indicador</td>
                            <td style="width:6.7%;" class="header_table">Linea Base</td>
                            <td style="width:6.7%;" class="header_table">Meta</td>
                            <td style="width:6.7%;" class="header_table">Ponderaci&oacute;n</td>
                            <td style="width:10.4%;" class="header_table">Responsable</td>
                            <td style="width:10.4%;" class="header_table">Unidad Responsable</td>
                            <td class="header_table">Cronograma de Ejecuci&oacute;n </td>
                        </tr>
                  <tr bgcolor="#F5F5F5" style="width:100%;">
                            <td>'.$nro.'</td>            
                            <td class="td_pdes">
                                            <span class="pdes_titulo">Pilar :</span> '.$fila['pdes_pilar'].'<br>
                                            <span class="pdes_titulo">Meta :</span> '.$fila['pdes_meta'].'<br>
                                            <span class="pdes_titulo">Resultado :</span> '.$fila['pdes_resultado'].'<br>
                                            <span class="pdes_titulo">Acción :</span> '.$fila['pdes_accion'].'<br>
                                        </td>
                            <td>'.$fila['obje_objetivo'].'</td>
                            <td>'.$fila['indi_abrev'].'</td>
                            <td>'.$fila['obje_linea_base'].'</td>
                            <td>'.$fila['obje_meta'].'%</td>
                            <td>'.$fila['obje_ponderacion'].'%</td>
                            <td>'.$fila['fun_nombre'].' '.$fila['fun_paterno'].'</td>
                            <td>'.$fila['get_unidad'].'</td>
                            <td>'.$this->temporalizacion($fila['obje_id'],$mision_vision->conf_gestion_desde).'</td>
                        </tr>
                        <tr bgcolor="#F5F5F5" style="width:100%;">
                            <td colspan="10">
                                <table style="width=100%">
                                    <tr bgcolor="#696969">
                                        <td colspan="8" class="header_subtable">Indicadores Desempeño</td>
                                    </tr>
                                    <tr bgcolor="#696969">
                                        <td colspan="2" class="header_subtable">Eficacia</td>
                                        <td colspan="2" class="header_subtable">Eficiencia</td>
                                        <td colspan="2" class="header_subtable">Eficiencia en el plazo de ejecución</td>
                                        <td colspan="2" class="header_subtable">Eficiencia Fisica</td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#F5F5F5">'.$fila['obje_eficacia'].'</td>
                                        <td bgcolor="#F5F5F5">
                                            <ul>
                                                <li> Mayor 100% Mas Eficiente</li>
                                                <li> Igual 100% Eficiente</li>
                                                <li> Menor 100% Menos Eficiente </li>
                                            </ul>
                                        </td>
                                        <td bgcolor="#F5F5F5">'.$fila['obje_eficiencia'].'</td>
                                        <td bgcolor="#F5F5F5">
                                            <ul>
                                                <li> Mayor 100% Mas Eficiente</li>
                                                <li> Igual 100% Eficiente</li>
                                                <li> Menor 100% Menos Eficiente </li>
                                            </ul>                                
                                        </td>
                                        <td bgcolor="#F5F5F5">'.$fila['obje_eficiencia_pe'].'</td>
                                        <td bgcolor="#F5F5F5">
                                            <ul>
                                                <li> Mayor 100% Mas Eficiente</li>
                                                <li> Igual 100% Eficiente</li>
                                                <li> Menor 100% Menos Eficiente </li>
                                            </ul>                                
                                        </td>
                                        <td bgcolor="#F5F5F5">'.$fila['obje_eficiencia_fi'].'</td>
                                        <td bgcolor="#F5F5F5">
                                            <ul>
                                                <li> Mayor 100% Mas Eficiente</li>
                                                <li> Igual 100% Eficiente</li>
                                                <li> Menor 100% Menos Eficiente </li>
                                            </ul>                                
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </table><hr>
                  ';
                  if($nro==1){$html .= '<br><br><br><br><br>';}
                  elseif($nro==2){$html .= '<br><br><br><br>';}
                  elseif($nro==3){$html .= '<br><br><br><br>';}
                  elseif($nro==4){$html .= '<br><br><br><br>';}
                  elseif($nro==5){$html .= '<br><br><br><br>';}
                  elseif($nro==6){$html .= '<br><br><br><br>';}
                  elseif($nro==7){$html .= '<br><br><br><br>';}
                $nro++;
                }
                
                $html .= '
                <table style="width: 95%;margin: 0 auto;margin-top:30px;margin-bottom: 50px;">
                    <tr>
                        <td style="width:3%;margin-bottom: 10px;">FIRMAS:</td>
                        <td style="width:30%">
                            <hr>
                        </td>
                        <td style="width:3%"></td>
                        <td style="width:30%">
                            <hr>
                        </td>
                        <td style="width:3%"></td>
                        <td style="width:30%">
                            <hr>
                        </td>
                    </tr>
                </table>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }

    public function temporalizacion($oe_id,$gi)
    {
      $obj=$this->model_reporte_fpoa->get_objetivo($oe_id); /// Objetivo Id
      $programado=$this->model_reporte_fpoa->oestrategico_programado($oe_id); /// programado
      $ejecutado=$this->model_reporte_fpoa->oestrategico_ejecutado($oe_id); /// ejecutado
      $nro=0;
      $tr_return = '';
      foreach($programado as $row)
      {
        $nro++;
        $matriz [1][$nro]=$row['g_id'];
        $matriz [2][$nro]=$row['opm_programado'];
      }
      /*---------------- llenando la matriz vacia --------------*/
      for($j = 1; $j<=5; $j++)
      {
        $matriz_r[1][$j]=$gi;
        $matriz_r[2][$j]='0';  //// P
        $matriz_r[3][$j]='0';  //// PA
        $matriz_r[4][$j]='0';  //// %PA
        $matriz_r[5][$j]='0';  //// A
        $matriz_r[6][$j]='0';  //// B
        $matriz_r[7][$j]='0';  //// E
        $matriz_r[8][$j]='0';  //// EA
        $matriz_r[9][$j]='0';  //// %EA
        $matriz_r[10][$j]='0'; //// EFICACIA

        $gi++;
      }
      /*--------------------------------------------------------*/
     /*--------------------ejecutado gestion ------------------*/
      $nro_e=0;
      foreach($ejecutado as $row)
      {
        $nro_e++;
        $matriz_e [1][$nro_e]=$row['g_id'];
        $matriz_e [2][$nro_e]=$row['oem_ejecutado'];
        $matriz_e [3][$nro_e]=$row['oem_ejecutado_a'];
        $matriz_e [4][$nro_e]=$row['oem_ejecutado_a'];
      }
      /*--------------------------------------------------------*/
      /*------- asignando en la matriz P, PA, %PA ----------*/
      for($i = 1 ;$i<=$nro ;$i++)
      {
        for($j = 1 ;$j<=5 ;$j++)
        {
          if($matriz[1][$i]==$matriz_r[1][$j])
          {
              $matriz_r[2][$j]=round($matriz[2][$i],2);
          }
        }
      }

      $pa=0;
      for($j = 1 ;$j<=5 ;$j++){
        $pa=$pa+$matriz_r[2][$j];
        $matriz_r[3][$j]=$pa+$obj[0]['obje_linea_base'];
        if($obj[0]['obje_meta']!=0)
        {
          $matriz_r[4][$j]=round(((($pa+$obj[0]['obje_linea_base'])/$obj[0]['obje_meta'])*100),2);
        }
        
      } 
      /*-------------------------------------------------*/
      /*--------------- EJECUCION ----------------------------------*/
      if($obj[0]['indi_id']==1)
      {
        for($i = 1 ;$i<=$nro_e ;$i++){
          for($j = 1 ;$j<=5 ;$j++)
          {
            if($matriz_e[1][$i]==$matriz_r[1][$j])
            {
                $matriz_r[7][$j]=round($matriz_e[2][$i],2);
            }
          }
        }
      }
      elseif ($obj[0]['indi_id']==2) 
      {
        for($i = 1 ;$i<=$nro_e ;$i++){
            for($j = 1 ;$j<=5 ;$j++)
            {
              if($matriz_e[1][$i]==$matriz_r[1][$j])
              {
                $matriz_r[5][$j]=round($matriz_e[3][$i],2);
                $matriz_r[6][$j]=round($matriz_e[4][$i],2);
                $matriz_r[7][$j]=round($matriz_e[2][$i],2);
              }
            }
          }
        /*--------------------------------------------------------*/
        }
        /*--------------------matriz E,AE,%AE gestion ------------------*/
          $pe=0;
          for($j = 1 ;$j<=5 ;$j++){
            $pe=$pe+$matriz_r[7][$j];
            $matriz_r[8][$j]=$pe+$obj[0]['obje_linea_base'];
            if($obj[0]['obje_meta']!=0)
            {
              $matriz_r[9][$j]=round(((($pe+$obj[0]['obje_linea_base'])/$obj[0]['obje_meta'])*100),2);
            }
            
            if($matriz_r[4][$j]==0)
              {$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),2);}
              else
              {
              $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),2);
              }
          }
      /*------------------------------------------------------------*/

          $tr_return .= '<table border=1>
                          <tr>
                          <td class="header_subtable">P/E</td>';
                          for($i = 1 ;$i<=5 ;$i++)
                          {
                            $tr_return .= '<td class="header_subtable">'.$matriz_r[1][$i].'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>P</td>';
                          for($i = 1 ;$i<=5 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[2][$i].'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>PA</td>';
                          for($i = 1 ;$i<=5 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[3][$i].'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr bgcolor="#F5F5F5">
                          <td>% PA</td>';
                          for($i = 1 ;$i<=5 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[4][$i].'</td>';
                          }
                          $tr_return .= '
                          </tr>';
                          /*---------------------------------------------------*/
                          if($obj[0]['indi_id']==2)
                          {
                             $tr_return .= '
                              <tr>
                              <td>A</td>';
                              for($i = 1 ;$i<=5 ;$i++)
                              {
                                $tr_return .= '<td>'.$matriz_r[5][$i].'</td>';
                              }
                              $tr_return .= '
                              </tr>
                              <tr>
                              <td>B</td>';
                              for($i = 1 ;$i<=5 ;$i++)
                              {
                                $tr_return .= '<td>'.$matriz_r[6][$i].'</td>';
                              }
                              $tr_return .= '
                              </tr>';
                          }
                          /*---------------------------------------------------*/
                          $tr_return .= '  
                          <tr>
                          <td>E</td>';
                          for($i = 1 ;$i<=5 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[7][$i].'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>EA</td>';
                          for($i = 1 ;$i<=5 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[8][$i].'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr bgcolor="#F5F5F5">
                          <td>% EA</td>';
                          for($i = 1 ;$i<=5 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[9][$i].'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>EFICACIA</td>';
                          for($i = 1 ;$i<=5 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[10][$i].'%</td>';
                          }
                          $tr_return .= '
                          </tr>
                        </table>';
                  
      return $tr_return;
    }
    /*========================================================================================================*/
    /*===================================== ANALISIS DE SITUACION ===========================================*/
    public function analisis_programas()
    {
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];
        $id_mes=$this->session->userdata("mes");
        $data['id_mes'] = $id_mes;
        $data['mes'] = $mes;
        $data['dias'] = $dias;

        $lista_poa = $this->mpoa->lista_poa();
        $tabla = '';
        foreach($lista_poa as $row)
        {
          $tabla .= '<tr>';
            $tabla .= '<td><center><a href="javascript:abreVentana(\''.site_url("admin").'/reporte_analisis_situacion/'.$row['poa_id'].'\');" title="LISTA DE OBJETIVOS DE GESTION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/></a></center></td>';
            $tabla .= '<td>'.$row['aper_programa'].$row['aper_proyecto'].$row['aper_actividad']." - ".$row['aper_descripcion'].'</td>';
            $tabla .= '<td>'.$row['uni_unidad'].'</td>';
          $tabla .= '</tr>';
        }

        $data['aprog'] = $tabla;
        $this->load->view('admin/reportes/formularios_poa/analisis/list_programas', $data);
    }
    /*==============================================================================================================*/
    /*===================================== OBJETIVOS DE GESTION-PRODUCTO TERMINAL ===========================================*/
    public function og_pterminal_programas()
    {
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];
        $id_mes=$this->session->userdata("mes");
        $data['id_mes'] = $id_mes;
        $data['mes'] = $mes;
        $data['dias'] = $dias;

        $lista_poa = $this->mpoa->lista_poa();
        $tabla = '';
        foreach($lista_poa as $row)
        {
          $tabla .= '<tr>';
            $tabla .= '<td><center><a href="javascript:abreVentana(\''.site_url("admin").'/reporte/rep_og_pterminal/'.$row['poa_id'].'\');" title="LISTA DE OBJETIVOS DE GESTION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/></a></center></td>';
            $tabla .= '<td>'.$row['aper_programa'].$row['aper_proyecto'].$row['aper_actividad']." - ".$row['aper_descripcion'].'</td>';
            $tabla .= '<td>'.$row['uni_unidad'].'</td>';
          $tabla .= '</tr>';
        }

        $data['aprog'] = $tabla;
        $this->load->view('admin/reportes/formularios_poa/og_pterminal/list_programas', $data);
    }
    /*---------------------------------- REPORTE PDF OBJETIVO DE GESTION PRODUCTO TERMINAL -----------------------------*/
    public function reporte_og_pterminal($poa_id)
    {  
        $gestion=$this->session->userdata('gestion');
        
        $mision_vision = $this->model_objetivo->get_dato_configuracion($gestion);
        $mision_vision = $mision_vision->row();
        $mision = $mision_vision->conf_mision;
        $vision = $mision_vision->conf_vision;
        $gestion_base = $mision_vision->conf_gestion_base;
        $rango = $mision_vision->conf_gestion_desde.' - '.$mision_vision->conf_gestion_hasta;
        
        $poa = $this->model_modificacion->poa_id($poa_id);
        
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO POA : </b> OBJETIVO DE GESTI&Oacute;N Y PRODUCTO TERMINAL <br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <div class="mv" style="text-align:justify">
                    <b>PROGRAMA : </b> '.$poa[0]['aper_programa'].''.$poa[0]['aper_proyecto'].''.$poa[0]['aper_actividad'].' - '.$poa[0]['aper_descripcion'].'
                </div>
                <div class="mv" style="text-align:justify">
                    <b>UNIDAD ORGANIZACIONAL : </b> '.$poa[0]['uni_unidad'].'
                </div>
                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <br>';

                $lista_objetivos = $this->mobjetivos->lista_obje_poa($poa_id);
                foreach($lista_objetivos as $rowoe)
                {
                    $html .= '<div class="mv" style="text-align:justify">
                                <b>OBJETIVO ESTRATEGICO : </b> '.$rowoe['obje_objetivo'].'
                            </div>';

                    $lista_objgestion = $this->model_modificacion->lista_ogestion($rowoe['obje_id'],$poa[0]['aper_id']); //// Lista Objetivos de Gestion
                    $nro=1;
                    foreach ($lista_objgestion as $rowo) 
                    {
                      $html .= '
                      <table>
                            <tr bgcolor="#696969">
                                <th bgcolor="#000000">'.$nro.'</th>
                                <th bgcolor="#000000" colspan=6>OBJETIVO DE GESTI&Oacute;N</th>
                            </tr>
                            <tr>
                                <th style="width:1%;" bgcolor="#000000"></th>
                                <th style="width:20%;" bgcolor="#000000">OBJETIVO DE GESTI&Oacute;N</th>
                                <th style="width:10%;" bgcolor="#000000">TIPO DE INDICADOR</th>
                                <th style="width:6.7%;" bgcolor="#000000">LINEA BASE</th>
                                <th style="width:6.7%;" bgcolor="#000000">META</th>
                                <th style="width:6.7%;" bgcolor="#000000">PONDERACI&Oacute;N</th>
                                <th bgcolor="#000000">CRONOGRAMA DE PROGRAMACI&Oacute;N/EJECUCI&Oacute;N</th>
                            </tr>
                            <tr bgcolor="#F5F5F5" style="width:100%;">
                                <td></td>            
                                <td>'.$rowo['o_objetivo'].'</td>
                                <td>'.$rowo['indi_abreviacion'].'</td>
                                <td>'.$rowo['o_linea_base'].'</td>
                                <td>'.$rowo['o_meta'].'</td>
                                <td>'.$rowo['o_ponderacion'].' %</td>
                                <td>'.$this->temporalizacion_og($rowo['o_id']).'</td>
                            </tr>
                      </table>';
                        $pterminal = $this->model_reporte_seguimiento->list_pterminal($rowo['o_id']); ///// lista de Productos Terminales  
                        $html .= '
                      <table class="table table-bordered" width="100%">
                            <tr >
                                <td style="width:20%;" class="header_table">PRODUCTO TERMINAL</td>
                                <td style="width:10%;" class="header_table">TIPO DE INDICADOR</td>
                                <td style="width:6.7%;" class="header_table">LINEA BASE</td>
                                <td style="width:6.7%;" class="header_table">META</td>
                                <td style="width:6.7%;" class="header_table">PONDERACI&Oacute;N</td>
                                <td class="header_table">CRONOGRAMA DE PROGRAMACI&Oacute;N/EJECUCI&Oacute;N</td>
                            </tr>';
                        $nro_pt=1;
                        foreach ($pterminal as $rowpt) 
                        {
                          $html .= ' 
                           <tr style="width:100%;">         
                                <td>'.$rowpt['pt_objetivo'].'</td>
                                <td>'.$rowpt['indi_descripcion'].'</td>
                                <td>'.$rowpt['pt_linea_base'].'</td>
                                <td>'.$rowpt['pt_meta'].'</td>
                                <td>'.$rowpt['pt_ponderacion'].' %</td>
                                <td>'.$this->temporalizacion_pt($rowpt['pt_id']).'</td>
                            </tr>';
                        $nro_pt++;
                        }
                        $html .= '
                      </table><hr><br><br>';
                    $nro++;
                    }
                }

                $html .= '
                <table style="width: 95%;margin: 0 auto;margin-top:30px;margin-bottom: 50px;">
                    <tr>
                        <td style="width:3%;margin-bottom: 10px;">FIRMAS:</td>
                        <td style="width:30%">
                            <hr>
                        </td>
                        <td style="width:3%"></td>
                        <td style="width:30%">
                            <hr>
                        </td>
                        <td style="width:3%"></td>
                        <td style="width:30%">
                            <hr>
                        </td>
                    </tr>
                </table>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }

    public function temporalizacion_og($o_id)
    {
      $og=$this->model_reporte_seguimiento->get_objetivo($o_id); /// Objetivo gestion Id
      if($og[0]['indi_id']==1){$ti='';}
      elseif($og[0]['indi_id']==2){$ti='%';}
      $tr_return = '';
      $programado=$this->model_reporte_seguimiento->ogestion_programado($o_id); /// programado
          $nro=0;
          foreach($programado as $row)
          {
            $nro++;
            $matriz [1][$nro]=$row['mes_id'];
            $matriz [2][$nro]=$row['opm_fis'];
          }
           /*---------------- llenando la matriz vacia --------------*/
          for($j = 1; $j<=12; $j++)
          {
            $matriz_r[1][$j]=$j;
            $matriz_r[2][$j]='0'; /// P
            $matriz_r[3][$j]='0'; /// PA
            $matriz_r[4][$j]='0'; /// PA%
            $matriz_r[5][$j]='0'; /// A
            $matriz_r[6][$j]='0'; /// B
            $matriz_r[7][$j]='0'; /// E
            $matriz_r[8][$j]='0'; /// EA
            $matriz_r[9][$j]='0'; /// EA%
            $matriz_r[10][$j]='0'; /// EFI
          }
          /*--------------------------------------------------------*/
          /*--------------------- asignando en la matriz P, PA, %PA ----------------------*/
            for($i = 1 ;$i<=$nro ;$i++)
            {
              for($j = 1 ;$j<=12 ;$j++)
              {
                if($matriz[1][$i]==$matriz_r[1][$j])
                {
                  $matriz_r[2][$j]=round($matriz[2][$i],2);
                }
              }
            }
          $pa=0;
          for($j = 1 ;$j<=12 ;$j++){
              $pa=$pa+$matriz_r[2][$j];
              $matriz_r[3][$j]=$pa+$og[0]['o_linea_base'];
              if($og[0]['o_meta']!=0)
              {
                  $matriz_r[4][$j]=round(((($pa+$og[0]['o_linea_base'])/$og[0]['o_meta'])*100),2);
              }
              
            }
          /*--------------------------------------------------------------------------------*/
          /*-------------------------------- EJECUTADO -----------------------------------*/
          if($og[0]['indi_id']==1){
              $ejecutado=$this->model_reporte_seguimiento->ogestion_ejecutado_absoluto($o_id); /// ejecutado abs
              $nro_e=0;
              foreach($ejecutado as $row)
              {
                  $nro_e++;
                  $matriz_e [1][$nro_e]=$row['mes_id'];  //// Mes_id
                  $matriz_e [2][$nro_e]=$row['oem_fis']; //// valor
                  $matriz_e [3][$nro_e]='0';  //// a
                  $matriz_e [4][$nro_e]='0';  //// b
              }
          }
          else{
              $ejecutado=$this->model_reporte_seguimiento->ogestion_ejecutado_relativo($o_id); /// ejecutado relativo
              $nro_e=0;
              foreach($ejecutado as $row)
              {
                  $nro_e++;
                  if($og[0]['o_denominador']==0){
                      $valor=(($og[0]['oer_desfavorable']/$og[0]['oer_favorable'])*$matriz_r[2][$nro_e]);
                  }
                  else{
                      $valor=(($og[0]['oer_desfavorable']/$og[0]['oer_favorable'])*100);
                  }
                  
                  $matriz_e [1][$nro_e]=$row['mes_id'];
                  $matriz_e [2][$nro_e]=$valor;
                  $matriz_e [3][$nro_e]=$og[0]['oer_desfavorable'];  //// a
                  $matriz_e [4][$nro_e]=$og[0]['oer_favorable'];  //// b
              }
          }
          /*--------------------matriz E,AE,%AE gestion ------------------*/
              for($i = 1 ;$i<=$nro_e ;$i++){
                for($j = 1 ;$j<=12 ;$j++)
                {
                  if($matriz_e[1][$i]==$matriz_r[1][$j])
                  {
                    $matriz_r[5][$j]=round($matriz_e[3][$i],1);
                    $matriz_r[6][$j]=round($matriz_e[4][$i],1);
                    $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                  }
                }
              }

            $pe=0;
            for($j = 1 ;$j<=12 ;$j++){
              $pe=$pe+$matriz_r[7][$j];
              $matriz_r[8][$j]=$pe+$og[0]['o_linea_base'];
              if($og[0]['o_meta'])
              {
                 $matriz_r[9][$j]=round(((($pe+$og[0]['o_linea_base'])/$og[0]['o_meta'])*100),2); 
              }
              
              if($matriz_r[4][$j]!=0)
              {$matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),1);}
          }

      /*------------------------------------------------------------*/

          $tr_return .= '<table border=1>
                          <tr>
                          <td class="header_subtable">P/E</td>
                          <td class="header_subtable">Ene.</td>
                          <td class="header_subtable">Feb.</td>
                          <td class="header_subtable">Mar.</td>
                          <td class="header_subtable">Abr.</td>
                          <td class="header_subtable">May.</td>
                          <td class="header_subtable">Jun.</td>
                          <td class="header_subtable">Jul.</td>
                          <td class="header_subtable">Agos.</td>
                          <td class="header_subtable">Sept.</td>
                          <td class="header_subtable">Oct.</td>
                          <td class="header_subtable">Nov.</td>
                          <td class="header_subtable">Dic.</td>
                          </tr>
                          <tr>
                          <td>P</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[2][$i].''.$ti.'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>PA</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[3][$i].''.$ti.'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td bgcolor="#F5F5F5">PA%</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[4][$i].'%</td>';
                          }
                          $tr_return .= '
                          </tr>';
                          /*---------------------------------------------------------------*/
                          if($og[0]['indi_id']==2)
                          {
                            $tr_return .= '
                          <tr>
                          <td>A</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[5][$i].'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>B</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[6][$i].'</td>';
                          }
                          $tr_return .= '
                          </tr>';
                          }
                          
                          /*---------------------------------------------------------------*/
                          $tr_return .= '
                          <tr>
                          <td>E</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[7][$i].''.$ti.'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>EA</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[8][$i].''.$ti.'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr bgcolor="#F5F5F5">
                          <td>EA%</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[9][$i].'%</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>EFICACIA</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[10][$i].'%</td>';
                          }
                          $tr_return .= '
                          </tr>
                        </table>';
                  
      return $tr_return;
    }
    /*----------------------------------- PRODUCTO TERMINAL ----------------------------*/
    public function temporalizacion_pt($pt_id)
    {
      $pt=$this->model_reporte_seguimiento->get_pterminal($pt_id); /// Objetivo gestion Id
      if($pt[0]['indi_id']==1){$ti='';}
      elseif($pt[0]['indi_id']==2){$ti='%';}
      $tr_return = '';
      $programado=$this->model_reporte_seguimiento->pterminal_programado($pt_id); /// pt programado
      $nro=0;
      foreach($programado as $row)
      {
        $nro++;
        $matriz [1][$nro]=$row['mes_id'];
        $matriz [2][$nro]=$row['ppm_fis'];
      }
       /*---------------- llenando la matriz vacia --------------*/
      for($j = 1; $j<=12; $j++)
      {
            $matriz_r[1][$j]=$j;
            $matriz_r[2][$j]='0'; /// P
            $matriz_r[3][$j]='0'; /// PA
            $matriz_r[4][$j]='0'; /// PA%
            $matriz_r[5][$j]='0'; /// A
            $matriz_r[6][$j]='0'; /// B
            $matriz_r[7][$j]='0'; /// E
            $matriz_r[8][$j]='0'; /// EA
            $matriz_r[9][$j]='0'; /// EA%
            $matriz_r[10][$j]='0'; /// EFI
      }
      /*--------------------------------------------------------*/
      /*--------------------- asignando en la matriz P, PA, %PA ----------------------*/
        for($i = 1 ;$i<=$nro ;$i++)
        {
          for($j = 1 ;$j<=12 ;$j++)
          {
            if($matriz[1][$i]==$matriz_r[1][$j])
            {
              $matriz_r[2][$j]=round($matriz[2][$i],2);
            }
          }
        }

      $pa=0;
      for($j = 1 ;$j<=12 ;$j++){
          $pa=$pa+$matriz_r[2][$j];
          $matriz_r[3][$j]=$pa+$pt[0]['pt_linea_base'];
          if($pt[0]['pt_meta']!=0)
          {
            $matriz_r[4][$j]=round(((($pa+$pt[0]['pt_linea_base'])/$pt[0]['pt_meta'])*100),2);
          }
          
        }
      /*--------------------------------------------------------------------------------*/
      /*-------------------------------- EJECUTADO -----------------------------------*/
      if($pt[0]['indi_id']==1){
          $ejecutado=$this->model_reporte_seguimiento->pterminal_ejecutado_absoluto($pt_id); /// ejecutado abs
          $nro_e=0;
          foreach($ejecutado as $row)
          {
              $nro_e++;
              $matriz_e [1][$nro_e]=$row['mes_id'];
              $matriz_e [2][$nro_e]=$row['pem_fis'];
              $matriz_e [3][$nro_e]='0';
              $matriz_e [4][$nro_e]='0';
          }
      }
      else{
          $ejecutado=$this->model_reporte_seguimiento->pterminal_ejecutado_relativo($pt_id); /// ejecutado rel
          $nro_e=0;
          foreach($ejecutado as $row)
          {
              $nro_e++;
              if($pt[0]['pt_denominador']==0){
                  $valor=(($pt[0]['per_desfavorable']/$pt[0]['per_favorable'])*$matriz_r[2][$nro_e]);
              }
              else{
                  $valor=(($pt[0]['per_desfavorable']/$pt[0]['per_favorable'])*100);
              }
              
              $matriz_e [1][$nro_e]=$row['mes_id'];
              $matriz_e [2][$nro_e]=$valor;
              $matriz_e [3][$nro_e]=$pt[0]['per_favorable'];
              $matriz_e [4][$nro_e]=$pt[0]['per_desfavorable'];
          }
      }
      /*--------------------matriz E,AE,%AE gestion ------------------*/
          for($i = 1 ;$i<=$nro_e ;$i++){
            for($j = 1 ;$j<=12 ;$j++)
            {
              if($matriz_e[1][$i]==$matriz_r[1][$j])
              {
                $matriz_r[5][$j]=round($matriz_e[3][$i],1);
                $matriz_r[6][$j]=round($matriz_e[4][$i],1);
                $matriz_r[7][$j]=round($matriz_e[2][$i],1);
              }
            }
          }

        $pe=0;
        for($j = 1 ;$j<=12 ;$j++){
          $pe=$pe+$matriz_r[7][$j];
          $matriz_r[8][$j]=$pe+$pt[0]['pt_linea_base'];
          if($pt[0]['pt_meta']!=0)
          {
            $matriz_r[9][$j]=round(((($pe+$pt[0]['pt_linea_base'])/$pt[0]['pt_meta'])*100),2);
          }
          
          if($matriz_r[4][$j]!=0)
          {$matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),1);}
      }

      /*------------------------------------------------------------*/

          $tr_return .= '<table border=1>
                          <tr>
                          <td class="header_subtable">P/E</td>
                          <td class="header_subtable">Ene.</td>
                          <td class="header_subtable">Feb.</td>
                          <td class="header_subtable">Mar.</td>
                          <td class="header_subtable">Abr.</td>
                          <td class="header_subtable">May.</td>
                          <td class="header_subtable">Jun.</td>
                          <td class="header_subtable">Jul.</td>
                          <td class="header_subtable">Agos.</td>
                          <td class="header_subtable">Sept.</td>
                          <td class="header_subtable">Oct.</td>
                          <td class="header_subtable">Nov.</td>
                          <td class="header_subtable">Dic.</td>
                          </tr>
                          <tr>
                          <td>P</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[2][$i].''.$ti.'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>PA</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[3][$i].''.$ti.'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td bgcolor="#F5F5F5">PA%</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[4][$i].'%</td>';
                          }
                          $tr_return .= '
                          </tr>';
                          /*---------------------------------------------------------------*/
                          if($pt[0]['indi_id']==2)
                          {
                            $tr_return .= '
                          <tr>
                          <td>A</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[5][$i].'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>B</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[6][$i].'</td>';
                          }
                          $tr_return .= '
                          </tr>';
                          }
                          
                          /*---------------------------------------------------------------*/
                          $tr_return .= '
                          <tr>
                          <td>E</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[7][$i].''.$ti.'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>EA</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[8][$i].''.$ti.'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr bgcolor="#F5F5F5">
                          <td>EA%</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[9][$i].'%</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>EFICACIA</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[10][$i].'%</td>';
                          }
                          $tr_return .= '
                          </tr>
                        </table>';
                  
      return $tr_return;
    }
    /*========================================================================================================*/
    /*===================================== ACCION ===========================================*/
    public function accion_programas()
    {
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];
        $id_mes=$this->session->userdata("mes");
        $data['id_mes'] = $id_mes;
        $data['mes'] = $mes;
        $data['dias'] = $dias;

        $lista_poa = $this->mpoa->lista_poa();
        $tabla = '';
        foreach($lista_poa as $row)
        {
          $tabla .= '<tr>';
            $tabla .= '<td><center><a href="javascript:abreVentana(\''.site_url("admin").'/reporte/reporte_acciones/'.$row['aper_programa'].'/'.$row['poa_id'].'\');" title="LISTA DE OBJETIVOS DE GESTION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/></a></center></td>';
            $tabla .= '<td>'.$row['aper_programa'].$row['aper_proyecto'].$row['aper_actividad']." - ".$row['aper_descripcion'].'</td>';
            $tabla .= '<td>'.$row['uni_unidad'].'</td>';
          $tabla .= '</tr>';
        }

        $data['aprog'] = $tabla;
        $this->load->view('admin/reportes/formularios_poa/accion/list_programas', $data);
    }
    /*---------------------------------- REPORTE PDF ACCIONES -----------------------------*/
    public function reporte_acciones($prog,$poa_id)
    {  
        $gestion=$this->session->userdata('gestion');
        
        $mision_vision = $this->model_objetivo->get_dato_configuracion($gestion);
        $mision_vision = $mision_vision->row();
        $mision = $mision_vision->conf_mision;
        $vision = $mision_vision->conf_vision;
        $gestion_base = $mision_vision->conf_gestion_base;
        $rango = $mision_vision->conf_gestion_desde.' - '.$mision_vision->conf_gestion_hasta;
        
        $poa = $this->model_modificacion->poa_id($poa_id);
        $acciones=$this->model_reporte->list_acciones($prog,$gestion);

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO POA : </b> OPERACIONES INSTITUCIONALES <br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <div class="mv" style="text-align:justify">
                    <b>PROGRAMA : </b> '.$poa[0]['aper_programa'].''.$poa[0]['aper_proyecto'].''.$poa[0]['aper_actividad'].' - '.$poa[0]['aper_descripcion'].'
                </div>
                <div class="mv" style="text-align:justify">
                    <b>UNIDAD ORGANIZACIONAL : </b> '.$poa[0]['uni_unidad'].'
                </div>
                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <br>';

                $lista_objetivos = $this->mobjetivos->lista_obje_poa($poa_id);
                foreach($lista_objetivos as $rowoe)
                {
                    $html .= '<div class="mv" style="text-align:justify">
                                <b>OBJETIVO ESTRATEGICO : </b> '.$rowoe['obje_objetivo'].'
                            </div>';

                    $lista_objgestion = $this->model_modificacion->lista_ogestion($rowoe['obje_id'],$poa[0]['aper_id']); //// Lista Objetivos de Gestion
                    $nro=1;
                    foreach ($lista_objgestion as $rowo) 
                    {
                        $html .= '<div class="mv" style="text-align:justify">
                                    <b>OBJETIVO DE GESTI&Oacute;N : </b> '.$rowo['o_objetivo'].'
                                    </div>';
                        $lista_pterminal = $this->mp_terminal->lista_pterminal($rowo['o_id']);

                            foreach ($lista_pterminal as $row)
                            {
                                $html .= '<div class="mv" style="text-align:justify">
                                    <b>PRODUCTO TERMINAL : </b> '.$row['pt_objetivo'].'
                                    </div>';
                            }
                    }
                }

                $nro=1;
                foreach ($acciones as $row) 
                {
                    if($this->model_proyecto->verif_proy($row['proy_id'])!=0)
                    {
                        $fase= $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
                        $nc=$this->model_faseetapa->calcula_nc($row['pfec_fecha_inicio']); //// calcula nuevo/continuo
                        $ap=$this->model_faseetapa->calcula_ap($row['pfec_fecha_inicio'],$row['pfec_fecha_fin']); //// calcula Anual/Plurianual
                      $html .= '
                      <table>
                        <tr>
                            <th bgcolor="#000000" colspan=10>ACCI&Oacute;N : '.$nro.' - CATEGORIA_PROGRAMATICA '.$gestion.' : '.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</th>
                        </tr>
                        <tr>
                            <th bgcolor="#000000">PROYECTO_PROGRAMA_ACTIVIDAD</th>
                            <th bgcolor="#000000">TIPO_DE_ACCI&Oacute;N</th>
                            <th bgcolor="#000000">CODIGO_SISIN</th>
                            <th bgcolor="#000000">RESPONSABLE (UE)</th>
                            <th bgcolor="#000000">UNIDAD_EJECUTORA</th>
                            <th bgcolor="#000000">UNIDAD_RESPONSABLE</th>
                            <th bgcolor="#000000">FASE_ETAPA</th>
                            <th bgcolor="#000000">NUEVO_CONTINUIDAD</th>
                            <th bgcolor="#000000">ANUAL_PLURIANUAL</th>
                            <th bgcolor="#000000">PRESUPUESTO<br>'.$gestion.'</th>
                        </tr>
                        <tr bgcolor="#F5F5F5" style="width:100%;">
                            <td>'.$row['proy_nombre'].'</td>
                            <td>'.$row['tp_tipo'].'</td>
                            <td>'.$row['proy_sisin'].'</td>
                            <td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>
                            <td>'.$row['ue'].'</td>
                            <td>'.$row['ur'].'</td>
                            <td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>
                            <td>'.$nc.'</td>
                            <td>'.$ap.'</td>
                            <td>';
                            $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion")); ////// datos fase activa presupuesto gestion actual
                            if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
                                {   
                                    if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
                                    {
                                        $techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
                                        $html .= ''.number_format($techo[0]['suma_techo'], 2, ',', ' ').'Bs';

                                    }
                                    else{$html .= '<font color=red>S/T</font>';}
                                }
                            
                        $html .= '
                            </td>
                        </tr>
                       </table><br>';
                        $componentes=$this->model_componente->componentes_id($fase[0]['id']);
                        foreach ($componentes as $rowc) 
                        {
                            $producto=$this->model_producto->list_prod($rowc['com_id']);
                            $html .= '
                              <table>
                                <tr>
                                    <th style="width:20%;" class="header_table">PRODUCTO DE LA ACCI&Oacute;N</th>
                                    <th style="width:10%;" class="header_table">TIPO DE INDICADOR</th>
                                    <th style="width:6.7%;" class="header_table">LINEA BASE</th>
                                    <th style="width:6.7%;" class="header_table">META</th>
                                    <th style="width:6.7%;" class="header_table">PONDERACI&Oacute;N</th>
                                    <th class="header_table">CRONOGRAMA DE PROGRAMACI&Oacute;N/EJECUCI&Oacute;N</th>
                                </tr>';
                                $nro_p=1;
                                foreach ($producto as $rowp) 
                                {
                                    $html .= '
                                        <tr>
                                            <td>'.$rowp['prod_producto'].'</td>
                                            <td>'.$rowp['indi_descripcion'].'</td>
                                            <td>'.$rowp['prod_linea_base'].'</td>
                                            <td>'.$rowp['prod_meta'].'</td>
                                            <td>'.$rowp['prod_ponderacion'].'</td>
                                            <td>'.$this->temporalizacion_prod($rowp['prod_id']).'</td>
                                        </tr>';
                                $nro_p++;
                                }
                               $html .= '
                              </table>'; 
                        }
                    $nro++;  
                    }
                    
                }
                
                $html .= '
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }
    /*----------------------------------- PRODUCTOS ----------------------------*/
    public function temporalizacion_prod($prod_id)
    {
      $prod=$this->model_producto->get_producto_id($prod_id); /// Producto Id
      $meta_gest=$this->model_producto->meta_prod_gest($prod_id);
      if($prod[0]['indi_id']==1){$ti='';}
      elseif($prod[0]['indi_id']==2){$ti='%';}
      $tr_return = '';
      $programado = $this->model_producto->prod_prog_mensual($prod_id,$this->session->userdata('gestion'));
      $ejecutado = $this->model_producto->prod_ejec_mensual($prod_id,$this->session->userdata('gestion'));
      if($meta_gest[0]['meta_gest']==''){$meta_gest[0]['meta_gest']='1';}
        $nro=0;
        foreach($programado as $row)
        {
            $nro++;
            $matriz [1][$nro]=$row['m_id'];
            $matriz [2][$nro]=$row['pg_fis'];
        }                        
        /*---------------- llenando la matriz vacia --------------*/
          for($j = 1; $j<=12; $j++)
          {
            $matriz_r[1][$j]=$j;
            $matriz_r[2][$j]='0'; /// P
            $matriz_r[3][$j]='0'; /// PA
            $matriz_r[4][$j]='0'; /// PA%
            $matriz_r[5][$j]='0'; /// A
            $matriz_r[6][$j]='0'; /// B
            $matriz_r[7][$j]='0'; /// E
            $matriz_r[8][$j]='0'; /// EA
            $matriz_r[9][$j]='0'; /// EA%
            $matriz_r[10][$j]='0'; /// EFI
          }
          /*--------------------------------------------------------*/
          /*--------------------ejecutado gestion ------------------*/
            $nro_e=0;
            foreach($ejecutado as $row)
            {
              $nro_e++;
              $matriz_e [1][$nro_e]=$row['m_id'];
              $matriz_e [2][$nro_e]=$row['pejec_fis'];
              $matriz_e [3][$nro_e]=$row['pejec_fis_a'];
              $matriz_e [4][$nro_e]=$row['pejec_fis_b'];
            }
            /*--------------------------------------------------------*/                        
          /*--------------------- asignando en la matriz P, PA, %PA ----------------------*/
            for($i = 1 ;$i<=$nro ;$i++)
            {
              for($j = 1 ;$j<=12 ;$j++)
              {
                if($matriz[1][$i]==$matriz_r[1][$j])
                {
                  $matriz_r[2][$j]=round($matriz[2][$i],2);
                }
              }
            }
            $pa=0;
            for($j = 1 ;$j<=12 ;$j++){
              $pa=$pa+$matriz_r[2][$j];
              $matriz_r[3][$j]=$pa+$prod[0]['prod_linea_base'];
              if($meta_gest[0]['meta_gest']!=0)
              {
                $matriz_r[4][$j]=round(((($pa+$prod[0]['prod_linea_base'])/$meta_gest[0]['meta_gest'])*100),1);
              }
              
            }                   
           /*----------------------------------------------------------------------------------*/
           /*--------------- EJECUCION ----------------------------------*/
            if($prod[0]['indi_id']==1)
            {
              for($i = 1 ;$i<=$nro_e ;$i++){
                for($j = 1 ;$j<=12 ;$j++)
                {
                  if($matriz_e[1][$i]==$matriz_r[1][$j])
                  {
                      $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                  }
                }
              }
            }
            elseif ($prod[0]['indi_id']==2) 
            {
              for($i = 1 ;$i<=$nro_e ;$i++){
                  for($j = 1 ;$j<=12 ;$j++)
                  {
                    if($matriz_e[1][$i]==$matriz_r[1][$j])
                    {
                      $matriz_r[5][$j]=round($matriz_e[3][$i],2);
                      $matriz_r[6][$j]=round($matriz_e[4][$i],2);
                      $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                    }
                  }
                }
              /*--------------------------------------------------------*/
              }
              /*--------------------matriz E,AE,%AE gestion ------------------*/
                $pe=0;
                for($j = 1 ;$j<=12 ;$j++){
                  $pe=$pe+$matriz_r[7][$j];
                  $matriz_r[8][$j]=$pe+$prod[0]['prod_linea_base'];
                  $matriz_r[9][$j]=round(((($pe+$prod[0]['prod_linea_base'])/$meta_gest[0]['meta_gest'])*100),1);
                  if($matriz_r[4][$j]==0)
                    {$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),1);}
                    else
                    {
                    $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),1);
                    }
                }
                                

      /*------------------------------------------------------------*/

          $tr_return .= '<table border=1>
                          <tr>
                          <td class="header_subtable">P/E</td>
                          <td class="header_subtable">Ene.</td>
                          <td class="header_subtable">Feb.</td>
                          <td class="header_subtable">Mar.</td>
                          <td class="header_subtable">Abr.</td>
                          <td class="header_subtable">May.</td>
                          <td class="header_subtable">Jun.</td>
                          <td class="header_subtable">Jul.</td>
                          <td class="header_subtable">Agos.</td>
                          <td class="header_subtable">Sept.</td>
                          <td class="header_subtable">Oct.</td>
                          <td class="header_subtable">Nov.</td>
                          <td class="header_subtable">Dic.</td>
                          </tr>
                          <tr>
                          <td>P</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[2][$i].''.$ti.'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>PA</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[3][$i].''.$ti.'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr bgcolor="#F5F5F5">
                          <td>PA%</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[4][$i].'%</td>';
                          }
                          $tr_return .= '
                          </tr>';
                          /*---------------------------------------------------------------*/
                          if($prod[0]['indi_id']==2)
                          {
                            $tr_return .= '
                          <tr>
                          <td>A</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[5][$i].'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>B</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[6][$i].'</td>';
                          }
                          $tr_return .= '
                          </tr>';
                          }
                          
                          /*---------------------------------------------------------------*/
                          $tr_return .= '
                          <tr>
                          <td>E</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[7][$i].''.$ti.'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>EA</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[8][$i].''.$ti.'</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr bgcolor="#F5F5F5">
                          <td>EA%</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[9][$i].'%</td>';
                          }
                          $tr_return .= '
                          </tr>
                          <tr>
                          <td>EFICACIA</td>';
                          for($i = 1 ;$i<=12 ;$i++)
                          {
                            $tr_return .= '<td>'.$matriz_r[10][$i].'%</td>';
                          }
                          $tr_return .= '
                          </tr>
                        </table>';
                  
      return $tr_return;
    }
    public function get_mes($mes_id)
    {
      $mes[1]='ENERO';
      $mes[2]='FEBRERO';
      $mes[3]='MARZO';
      $mes[4]='ABRIL';
      $mes[5]='MAYO';
      $mes[6]='JUNIO';
      $mes[7]='JULIO';
      $mes[8]='AGOSTO';
      $mes[9]='SEPTIEMBRE';
      $mes[10]='OCTUBRE';
      $mes[11]='NOVIEMBRE';
      $mes[12]='DICIEMBRE';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }
}