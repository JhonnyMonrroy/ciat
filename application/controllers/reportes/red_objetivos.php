<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Red_objetivos extends CI_Controller
{
    private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1.5px;
                text-align: center;
            }
        .mv{font-size:10px;}
        .siipp{width:180px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 8px;
        }
        .datos_principales {
            text-align: center;
            font-size: 8px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }
        .indi_desemp{
            font-size: 9px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:5px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 7px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
            padding: 8;
        }
        .lista{
            text-align:left;
            padding-left: 8;
            list-style-type:square;
            margin:2px;
        }
    </style>';
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->library('pdf2');
        $this->load->model('menu_modelo');
        $this->load->model('reportes/model_objetivo');
        $this->load->model('Users_model', '', true);
        $this->load->model('model_pei');
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('programacion/marco_estrategico/mobjetivos');
        $this->load->model('programacion/prog_poa/mobjetivo_gestion');
        $this->load->model('programacion/prog_poa/mp_terminal');
        $this->load->model('mantenimiento/model_funcionario');
        $this->load->model('mantenimiento/mindicador');
        $this->load->model('registro_ejec/mejec_ogestion_pterminal');
        $this->load->model('reportes/seguimiento/malerta_ogestion');
        $this->load->model('reportes/seguimiento/malerta_pterminal');
    }
    /////////*******RED DE OBJETIVOS --> OBJETIVOS ESTRATEGICOS*******////////
    //OBTENER TEMPORALIZACION DE PROGRMACION
    public function objetivo_estrategico($poa_id)
    {
        $gestion = $this->session->userdata('gestion');
        $dato_poa = $this->mpoa->dato_poa_id($poa_id);
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> ACCI&Oacute;N DE MEDIANO PLAZO<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <div class="mv" style="text-align:justify">
                                <b>APERTURA PROGRAMATICA: </b> '.$dato_poa[0]['aper_programa'].$dato_poa[0]['aper_proyecto'].$dato_poa[0]['aper_actividad'].'-'.$dato_poa[0]['aper_descripcion'].'
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="mv" style="text-align:justify">
                                <b>CODIGO POA: </b> '.$dato_poa[0]['poa_codigo'].'
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="mv" style="text-align:justify">
                                <b>UNIDAD EJECUTORA: </b> '.$dato_poa[0]['uni_unidad'].'
                            </div>
                        </td>
                    </tr>  
                </table>

                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <p>
                    <table style="table-layout:fixed;">
                        '.$this->obj_estrategicos($poa_id,$gestion).'
                    </table>
                </p>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("objetivo_estrategico.pdf", array("Attachment" => false));
    }
    public function obj_estrategicos($poa_id,$gestion)
    {
        $lista_objetivos = $this->mobjetivos->lista_obje_poa($poa_id);
        $temporalizacion = array();
        foreach ($lista_objetivos as $row1) {
            $obje_id = $row1['obje_id'];// id de mi objetivo estrategico
            $gestion_inicial = $row1['obje_gestion_curso'];// id de mi objetivo estrategico
            $linea_base = $row1['obje_linea_base'];
            $meta = $row1['obje_meta']; //variable meta
            $temporalizacion[$obje_id] = $this->progracion_gestion($obje_id, $gestion_inicial, $linea_base, $meta);
        }
        $data['lista_objetivos'] = $lista_objetivos;
        $data['temporalizacion'] = $temporalizacion;
        $html_objetivos = '';
        $n_obj = 0;
        foreach ($data['lista_objetivos'] as $fila) {
            $n_obj++;
            $html_objetivos .= '
                <tr>
                    <td class="sub_table">
                        <table>
                            <tr bgcolor="#696969">
                                <td style="width:3%;" class="header_table">#</td>
                                <td class="header_table"> CÓDIGO </td>
                                <td class="header_table">PDES.</td>
                                <td class="header_table">PTDI.</td>
                                <td class="header_table">OBJETIVO ESTRATÉGICO</td>
                                <td class="header_table">T.I.</td>
                                <td class="header_table">L/B</td>
                                <td class="header_table">META</td>
                                <td class="header_table">FUENTE VERIFICACIÓN</td>
                                <td style="width:30%;" class="header_table">Temporalizaci&oacute;n '.$gestion.'-'.($gestion+4).'</td>
                            </tr>
                            <tr bgcolor="#F5F5F5">
                                <td>'.$n_obj.'</td>
                                <td>'.$fila['obje_codigo'].'</td>
                                <td>
                                    <ul class="lista">
                                        <li><b>PILAR: </b> '.$fila['pdes_pilar'].'</li>
                                        <li><b>META: </b> '.$fila['pdes_meta'].'</li>
                                        <li><b>RESULTADO:</b> '.$fila['pdes_resultado'].'</li>
                                        <li><b>ACCIÓN:</b> '.$fila['pdes_accion'].'</li>                                    
                                    </ul>
                                </td>
                                <td>
                                    <ul class="lista">
                                        <li><b>EJECUCIÓN: </b> '.$fila['ptdi_eje'].'</li>
                                        <li><b>POLÍTICA: </b> '.$fila['ptdi_politica'].'</li>
                                        <li><b>PROGRAMA: </b>'.$fila['ptdi_programa'].'</li>                                    
                                    </ul>
                                </td>
                                <td>'.$fila['obje_objetivo'].'</td>
                                <td>'.$fila['indi_abrev'].'</td>
                                <td>'.$fila['obje_linea_base'].'</td>
                                <td>'.$fila['obje_meta'].'</td>
                                <td>'.$fila['obje_fuente_verificacion'].'</td>
                                <td>'.$this->obj_estrategico_temporalizacion($fila['obje_id'],$gestion,$data['temporalizacion']).'</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            ';
        }
        return $html_objetivos;
    }
    function progracion_gestion($obje_id, $gestion_inicial, $linea_base, $meta)
    {
        $cont = 1;
        $prog_acumulado = $linea_base; //variable de programacion acumulada
        for ($i = $gestion_inicial; $i <= ($gestion_inicial + 4); $i++) {
            $puntero_prog = 'prog' . $cont;//PUNTERO PARA LISTAR LA PROGRAMACION
            $dat_prog = $this->mobjetivos->get_prog_obj($obje_id, $i);
            $dato_programado = $dat_prog[0]['opm_programado'];
            $temporalizacion[$puntero_prog] = $dato_programado;//matriz
            //PROGRAMADO ACUMULADO
            $puntero_prog_acumulado = 'p_acumulado' . $cont;//PUNTERO PARA LA PROGRAMACION ACUMULADA
            $prog_acumulado += $dato_programado;
            $temporalizacion[$puntero_prog_acumulado] = $prog_acumulado;//matriz
            //PROGRAMA ACUMULADO PORECENTUAL
            $puntero_pa_porcentual = 'pa_porc' . $cont;//PUNTERO PARA LA PROGRAMACION ACUMULADA PORCENTUAL
	      if($meta == 0){
                $pa_porcentual = 0;
            }else{
                $pa_porcentual = (($prog_acumulado / $meta) * 100);
            }	           
            $temporalizacion[$puntero_pa_porcentual] = $pa_porcentual;//matriz
            $cont++;
        }
        return $temporalizacion;
    }
    public function obj_estrategico_temporalizacion($dato,$gestion,$temporalizacion)
    {
        $var = '
            <table>
                <thead>
                    <tr bgcolor="#696969">
                        <th class="header_subtable"> Tipo </th>
                        <th class="header_subtable">'.$gestion.'</th>
                        <th class="header_subtable">'.($gestion+1).'</th>
                        <th class="header_subtable">'.($gestion+2).'</th>
                        <th class="header_subtable">'.($gestion+3).'</th>
                        <th class="header_subtable">'.($gestion+4).'</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="header_subtable"> P. </td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['prog1'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['prog2'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['prog3'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['prog4'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['prog5'],0).'%</td>
                    </tr>
                    <tr>
                        <td class="header_subtable"> P.A. </td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['p_acumulado1'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['p_acumulado2'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['p_acumulado3'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['p_acumulado4'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['p_acumulado5'],0).'%</td>
                    </tr>
                    <tr>
                        <td class="header_subtable"> %P.A. </td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['pa_porc1'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['pa_porc2'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['pa_porc3'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['pa_porc4'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['pa_porc5'],0).'%</td>
                    </tr>
                </tbody>
            </table>
        ';
        return $var;
    }
    /////////*******FIN --> OBJETIVOS ESTRATEGICOS*******////////
    
    /////////*******RED DE OBJETIVOS --> OBJETIVOS GESTION*******////////
    public function objetivo_gestion($poa_id, $obje_id)
    {
        $dato_poa = $this->mpoa->dato_poa_id($poa_id);//datos del poa
        $data['dato_poa'] = $dato_poa[0];
        $dato_objest = $this->mobjetivos->dato_objetivo($obje_id);//dato de objetivo estrategico
        // var_dump($dato_objest);  die;
        $data['dato_objest'] = $dato_objest[0];
        $gestion = $this->session->userdata('gestion');
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO POR RESULTADOS - </b> '.$gestion.'<br>
                            <b>FORMULARIO POA-02:</b> ACCIONES DE CORTO PLAZO Y PRODUCTO TERMINAL<br>
                        </td>
                    </tr>
                </table>
                <hr>

                    <table>
                        <tr bgcolor="#F5F5F5" style="width:80%;">
                            <td style="width:7%;" class="header_table"> PDES: </td>
                            <td style="text-align: left; width:9%;" ><span class="pdes_titulo">Pilar :</span> '.$data['dato_objest']['pdes_pilar'].'</td>
                            <td style="text-align: left;"><span class="pdes_titulo">Meta :</span> '.$data['dato_objest']['pdes_meta'].'<br>
                                            <span class="pdes_titulo">Resultado :</span> '.$data['dato_objest']['pdes_resultado'].'<br>
                                            <span class="pdes_titulo">Acción :</span> '.$data['dato_objest']['pdes_accion'].'</td>
                        </tr>
                        <tr bgcolor="#F5F5F5" style="width:80%;">
                            <td style="width:7%;" class="header_table"> PTDI: </td>
                            <td style="text-align: left; width:18%;" ><span class="pdes_titulo">Eje Program&aacute;tico :</span> '.$data['dato_objest']['ptdi_eje'].'</td>
                            <td style="text-align: left;"><span class="pdes_titulo">Pol&iacute;tica :</span> '.$data['dato_objest']['ptdi_politica'].'<br>
                                            <span class="pdes_titulo">Programa :</span> '.$data['dato_objest']['ptdi_programa'].'</td>
                        </tr>
                    </table>


                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <div class="mv" style="text-align:justify">
                                <b>ACCI&Oacute;N DE MEDIANO PLAZO : </b> '.$data['dato_objest']['obje_codigo'] . ' -- ' . $data['dato_objest']['obje_objetivo'].' 
                            </div>
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="2">
                            <div class="mv" style="text-align:justify">
                                <b>PROGRAMA PRESUPUESTARIO: </b> '.$data['dato_poa']['aper_programa'] . $data['dato_poa']['aper_proyecto'] .
                                            $data['dato_poa']['aper_actividad'] . " - " . $data['dato_poa']['aper_descripcion'].' 
                            </div>
                        </td>
                    </tr>
                </table>



                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <p>
                    <table style="table-layout:fixed;">
                        '.$this->obj_gestion($obje_id,($dato_poa[0]['aper_id']),$gestion).'
                    </table>
                </p>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("objetivo_gestion.pdf", array("Attachment" => false));
    }
    //CALCULO DE PROGRAMACION MENSUAL
    function programacion_mensual($obje_id, $linea_base, $meta, $aper_id, $gestion)
    {
        $prog_acumulado = $linea_base; //variable de programacion acumulada
        $ejec_acumulado = $linea_base;
        for ($i = 1; $i <= 12; $i++) {
            $puntero_prog = 'p' . $i;//PUNTERO PARA LISTAR LA PROGRAMACION
            $puntero_ejec = 'e' . $i;
            $dato_programado = $this->mobjetivo_gestion->get_prog_ogestion($obje_id, $i);//lista de programado
            $dato_programado = $dato_programado[0]['opm_fis'];
            //Codigo de la ejecucion
            $ejec = $this->mejec_ogestion_pterminal->get_eje_oabsoluto($i, $obje_id);//dato de lo ejecutado
            $ejecut = (count($ejec) == 0) ? 0 : $ejec->oem_fis;

            $programado[$puntero_prog] = $dato_programado;//matriz
            $programado[$puntero_ejec] = $ejecut;
            //PROGRAMADO ACUMULADO--------------------------------------------------------------------------------
            $puntero_prog_acumulado = 'p_a' . $i;//PUNTERO PARA LA PROGRAMACION ACUMULADA
            $prog_acumulado += $dato_programado;
            $programado[$puntero_prog_acumulado] = $prog_acumulado;//matriz

            $puntero_ejec_acumulado = 'e_a' . $i;//PUNTERO PARA LA PROGRAMACION ACUMULADA
            $ejec_acumulado += $ejecut;
            $programado[$puntero_ejec_acumulado] = $ejec_acumulado;//matriz
            //PROGRAMA ACUMULADO PORECENTUAL----------------------------------------------------------------------
            $puntero_pa_porcentual = 'pa_porc' . $i;//PUNTERO PARA LA PROGRAMACION ACUMULADA PORCENTUAL
            $puntero_ea_porcentual = 'ea_porc' . $i;
            if($meta == 0){
                $pa_porcentual = 0;
                $ea_porcentual = 0;
            }else{
                $pa_porcentual = (($prog_acumulado / $meta) * 100);
                $ea_porcentual = (($ejec_acumulado / $meta) * 100);
            }
            $programado[$puntero_pa_porcentual] = $pa_porcentual;//matriz
            $programado[$puntero_ea_porcentual] = $ea_porcentual;
            $puntero_efi = 'efic' . $i;
            if($pa_porcentual == 0){
                $eficacia = 0;
            }else{
                $eficacia = (($ea_porcentual/$pa_porcentual) * 100);
            }
            $programado[$puntero_efi] = $eficacia;

        }
        return $programado;
    }

    public function obj_gestion($obje_id,$aper_id,$gestion)
    {
        $lista_objgestion = $this->mobjetivo_gestion->lista_ogestion($obje_id,$aper_id);
        var_dump($lista_objgestion);  die;
        $programacion = array();
        foreach ($lista_objgestion as $row) {
            $o_id = $row['o_id'];// id de mi objetivo estrategico
            $linea_base = $row['o_linea_base'];
            $meta = $row['o_meta']; //variable meta
            $programacion[$o_id] = $this->programacion_mensual($o_id, $linea_base, $meta, $aper_id, $gestion);
        }
        // var_dump($programacion);  die;  
        $objetivo_gestion = '';
        
        foreach ($lista_objgestion as $fila) {
            
            //--------LISTA DE PRODUCTOS TERMINALES
            $lista_pterminal = $this->mp_terminal->lista_pterminal($fila['o_id']);
            $metaa = $this->mobjetivo_gestion->get_mbefore($fila['o_id']);
            $meta_a = $metaa[0]['o_meta'];
            $difm = ($fila['o_meta']-$meta_a);
            $programacion_pt = array();
            foreach ($lista_pterminal as $row) {
                $pt_id = $row['pt_id'];// id de mi objetivo estrategico
                $linea_base = $row['pt_linea_base'];
                $meta = $row['pt_meta']; //variable meta
                $programacion_pt[$pt_id] = $this->programacion_mensual_prod_terminal($pt_id,$linea_base,$meta);
            }
            $producto_terminal = '';
            $n_prod = 0;


            $objetivo_gestion .= '
                <tr>
                    <td class="sub_table">
                        <table>
                            <tr bgcolor="#696969">
                                <td class="header_table">CÓD.</td>
                                <td class="header_table">ACCIONES DE CORTO PLAZO</td>
                                <td class="header_table">FECHA<br>INICIO - FIN</td>
                                <td class="header_table">RESPONSABLE</td>
                                <td class="header_table">INDICADORES</td>
                                <td class="header_table">FORMULA</td>
                                <td class="header_table">LINEA<BR>BASE</td>
                                <td class="header_table">META <br>INICIAL</td>
                                <td class="header_table">MODIFI- <br>CACI&Oacute;N</td>
                                <td class="header_table">META <br>ACTUAL</td>
                                <td style="width:30%;" class="header_table">MES</td>
                                <td class="header_table">FUENTE VERIFICACIÓN</td>
                            </tr>
                            <tr bgcolor="#F5F5F5">
                                <td>'.$fila['o_codigo'].'</td>
                                <td>'.$fila['o_objetivo'].'</td>
                                <td>01/01/'.$gestion.' <br>- 31/12/'.$gestion.'</td>
                                <td>'.$fila['fun_nombre'].' '.$fila['fun_paterno'].' '.$fila['fun_materno'].'<br>'.$fila['unidad'].'</td>
                                <td>'.$fila['o_indicador'].'</td>
                                <td>'.$fila['o_formula'].'</td>
                                <td>'.$fila['o_linea_base'].'</td>
                                <td>'.$meta_a.'</td>
                                <td>'.$difm.'</td>
                                <td>'.$fila['o_meta'].'</td>
                                <td style="width:30%;">'.$this->obj_gestion_programado_ejecutado($fila['o_id'],$programacion).'</td>
                                <td>'.$fila['o_fuente_verificacion'].'</td>
                            </tr>
                        </table> <br><b><p align="left">PRODUCTOS TERMINALES</p><b>';
                    foreach ($lista_pterminal as $fila) {
                         $objetivo_gestion .= '
                        <table>
                            <tr>
                                <td class="sub_table">
                                    <table>
                                        <tr bgcolor="#696969">
                                            <td class="header_table">CÓD</td>
                                            <td class="header_table">PRODUCTOS TERMINALES</td>
                                            <td class="header_table">FECHA<br>INICIO - FIN</td>
                                            <td class="header_table">RESPONSABLE</td>
                                            <td class="header_table">INDICADORES</td>
                                            <td class="header_table">FORMULA</td>
                                            <td class="header_table">LINEA<BR>BASE</td>
                                            <td class="header_table">META <br>INICIAL</td>
                                            <td class="header_table">MODIFI- <br>CACI&Oacute;N</td>
                                            <td class="header_table">META <br>ACTUAL</td>
                                            <td style="width:30%;" class="header_table">MES</td>
                                            <td class="header_table">FUENTE VERIFICACIÓN</td>
                                        </tr>
                                        <tr>
                                            <td>'.$fila['pt_codigo'].'</td>
                                            <td>'.$fila['pt_objetivo'].'</td>
                                            <td>01/01/'.$gestion.' <br>- 31/12/'.$gestion.'</td>
                                            <td>'.$fila['fun_nombre'].' '.$fila['fun_paterno'].' '.$fila['fun_materno'].'<br>'.$fila['unidad'].'</td>
                                            <td>'.$fila['pt_indicador'].'</td>
                                            <td>'.$fila['pt_formula'].'</td>
                                            <td>'.$fila['pt_linea_base'].'</td>
                                            <td>'.$fila['pt_meta'].'</td>
                                            <td>0</td>
                                            <td>'.$fila['pt_meta'].'</td>

                                            <td style="width:30%;">'.$this->obj_gestion_programado_ejecutado($fila['pt_id'],$programacion_pt,$o_id).'</td>
                                            <td>'.$fila['pt_fuente_verificacion'].'</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>';

                    }

$objetivo_gestion .= '
                </td>
            </tr>';
        }

        return $objetivo_gestion;
    }
    public function obj_gestion_programado_ejecutado($o_id,$programacion)
    {
        $var = '
            <table border=1 style="font-size:8px;">
                <tr>
                    <th class="header_subtable" style="width:6%">T.</th>
                    <th class="header_subtable">Ene.</th>
                    <th class="header_subtable">Feb.</th>
                    <th class="header_subtable">Mar.</th>
                    <th class="header_subtable">Abr.</th>
                    <th class="header_subtable">May.</th>
                    <th class="header_subtable">Jun.</th>
                    <th class="header_subtable">Jul.</th>
                    <th class="header_subtable">Ago.</th>
                    <th class="header_subtable">Sep.</th>
                    <th class="header_subtable">Oct.</th>
                    <th class="header_subtable">Nov.</th>
                    <th class="header_subtable">Dic.</th>
                </tr>
                <tr>
                    <td class="header_subtable">P.</td>
                    <td>'.round($programacion[$o_id]['p1'],2).'</td>
                    <td>'.round($programacion[$o_id]['p2'],2).'</td>
                    <td>'.round($programacion[$o_id]['p3'],2).'</td>
                    <td>'.round($programacion[$o_id]['p4'],2).'</td>
                    <td>'.round($programacion[$o_id]['p5'],2).'</td>
                    <td>'.round($programacion[$o_id]['p6'],2).'</td>
                    <td>'.round($programacion[$o_id]['p7'],2).'</td>
                    <td>'.round($programacion[$o_id]['p8'],2).'</td>
                    <td>'.round($programacion[$o_id]['p9'],2).'</td>
                    <td>'.round($programacion[$o_id]['p10'],2).'</td>
                    <td>'.round($programacion[$o_id]['p11'],2).'</td>
                    <td>'.round($programacion[$o_id]['p12'],2).'</td>
                </tr>
                <tr>
                    <td class="header_subtable">P.A.</td>
                    <td>'.round($programacion[$o_id]['p_a1'],2).'</td>
                    <td>'.round($programacion[$o_id]['p_a2'],2).'</td>
                    <td>'.round($programacion[$o_id]['p_a3'],2).'</td>
                    <td>'.round($programacion[$o_id]['p_a4'],2).'</td>
                    <td>'.round($programacion[$o_id]['p_a5'],2).'</td>
                    <td>'.round($programacion[$o_id]['p_a6'],2).'</td>
                    <td>'.round($programacion[$o_id]['p_a7'],2).'</td>
                    <td>'.round($programacion[$o_id]['p_a8'],2).'</td>
                    <td>'.round($programacion[$o_id]['p_a9'],2).'</td>
                    <td>'.round($programacion[$o_id]['p_a10'],2).'</td>
                    <td>'.round($programacion[$o_id]['p_a11'],2).'</td>
                    <td>'.round($programacion[$o_id]['p_a12'],2).'</td>
                </tr>
                <tr>
                    <td class="header_subtable">%P.A.</td>
                    <td>'.round($programacion[$o_id]['pa_porc1'],2).'</td>
                    <td>'.round($programacion[$o_id]['pa_porc2'],2).'</td>
                    <td>'.round($programacion[$o_id]['pa_porc3'],2).'</td>
                    <td>'.round($programacion[$o_id]['pa_porc4'],2).'</td>
                    <td>'.round($programacion[$o_id]['pa_porc5'],2).'</td>
                    <td>'.round($programacion[$o_id]['pa_porc6'],2).'</td>
                    <td>'.round($programacion[$o_id]['pa_porc7'],2).'</td>
                    <td>'.round($programacion[$o_id]['pa_porc8'],2).'</td>
                    <td>'.round($programacion[$o_id]['pa_porc9'],2).'</td>
                    <td>'.round($programacion[$o_id]['pa_porc10'],2).'</td>
                    <td>'.round($programacion[$o_id]['pa_porc11'],2).'</td>
                    <td>'.round($programacion[$o_id]['pa_porc12'],2).'</td>
                </tr>
                <tr>
                    <td class="header_subtable">E.</td>
                    <td>'.round($programacion[$o_id]['e1'],2).'</td>
                    <td>'.round($programacion[$o_id]['e2'],2).'</td>
                    <td>'.round($programacion[$o_id]['e3'],2).'</td>
                    <td>'.round($programacion[$o_id]['e4'],2).'</td>
                    <td>'.round($programacion[$o_id]['e5'],2).'</td>
                    <td>'.round($programacion[$o_id]['e6'],2).'</td>
                    <td>'.round($programacion[$o_id]['e7'],2).'</td>
                    <td>'.round($programacion[$o_id]['e8'],2).'</td>
                    <td>'.round($programacion[$o_id]['e9'],2).'</td>
                    <td>'.round($programacion[$o_id]['e10'],2).'</td>
                    <td>'.round($programacion[$o_id]['e11'],2).'</td>
                    <td>'.round($programacion[$o_id]['e12'],2).'</td>
                </tr>
                <tr>
                    <td class="header_subtable">E.A.</td>
                    <td>'.round($programacion[$o_id]['e_a1'],2).'</td>
                    <td>'.round($programacion[$o_id]['e_a2'],2).'</td>
                    <td>'.round($programacion[$o_id]['e_a3'],2).'</td>
                    <td>'.round($programacion[$o_id]['e_a4'],2).'</td>
                    <td>'.round($programacion[$o_id]['e_a5'],2).'</td>
                    <td>'.round($programacion[$o_id]['e_a6'],2).'</td>
                    <td>'.round($programacion[$o_id]['e_a7'],2).'</td>
                    <td>'.round($programacion[$o_id]['e_a8'],2).'</td>
                    <td>'.round($programacion[$o_id]['e_a9'],2).'</td>
                    <td>'.round($programacion[$o_id]['e_a10'],2).'</td>
                    <td>'.round($programacion[$o_id]['e_a11'],2).'</td>
                    <td>'.round($programacion[$o_id]['e_a12'],2).'</td>
                </tr>
                <tr>
                    <td class="header_subtable">%E.A.</td>
                    <td>'.round($programacion[$o_id]['ea_porc1'],2).'</td>
                    <td>'.round($programacion[$o_id]['ea_porc2'],2).'</td>
                    <td>'.round($programacion[$o_id]['ea_porc3'],2).'</td>
                    <td>'.round($programacion[$o_id]['ea_porc4'],2).'</td>
                    <td>'.round($programacion[$o_id]['ea_porc5'],2).'</td>
                    <td>'.round($programacion[$o_id]['ea_porc6'],2).'</td>
                    <td>'.round($programacion[$o_id]['ea_porc7'],2).'</td>
                    <td>'.round($programacion[$o_id]['ea_porc8'],2).'</td>
                    <td>'.round($programacion[$o_id]['ea_porc9'],2).'</td>
                    <td>'.round($programacion[$o_id]['ea_porc10'],2).'</td>
                    <td>'.round($programacion[$o_id]['ea_porc11'],2).'</td>
                    <td>'.round($programacion[$o_id]['ea_porc12'],2).'</td>
                </tr>
                <tr>
                    <td class="header_subtable">Eficacia</td>
                    <td>'.round($programacion[$o_id]['efic1'],2).'</td>
                    <td>'.round($programacion[$o_id]['efic2'],2).'</td>
                    <td>'.round($programacion[$o_id]['efic3'],2).'</td>
                    <td>'.round($programacion[$o_id]['efic4'],2).'</td>
                    <td>'.round($programacion[$o_id]['efic5'],2).'</td>
                    <td>'.round($programacion[$o_id]['efic6'],2).'</td>
                    <td>'.round($programacion[$o_id]['efic7'],2).'</td>
                    <td>'.round($programacion[$o_id]['efic8'],2).'</td>
                    <td>'.round($programacion[$o_id]['efic9'],2).'</td>
                    <td>'.round($programacion[$o_id]['efic10'],2).'</td>
                    <td>'.round($programacion[$o_id]['efic11'],2).'</td>
                    <td>'.round($programacion[$o_id]['efic12'],2).'</td>
                </tr>
                
            </table>
        ';
        return $var;
    }
    /////////*******FIN --> OBJETIVOS GESTION*******////////
    
    /////////*******RED DE OBJETIVOS --> PRODUCTO TERMINAL*******////////
    public function producto_terminal($poa_id, $obje_id, $o_id)
    {
        $gestion = $this->session->userData('gestion');
        $dato_poa = $this->mpoa->dato_poa_id($poa_id);//datos del poa
        $data['dato_poa'] = $dato_poa[0];
        $dato_objest = $this->mobjetivos->dato_objetivo($obje_id);//dato de objetivo estrategico
        $data['dato_objest'] = $dato_objest[0];
        $dato_ogestion = $this->mobjetivo_gestion->get_ogestion($o_id);//dato de objetivo de gestion
        $data['dato_ogestion'] = $dato_ogestion[0];
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> PRODUCTO TERMINAL <br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <hr>
                <table width="100%">
                    <tr>
                        <td>
                            <div class="mv" style="text-align:justify">
                                <b>GESTIÓN: </b>'.$data['dato_poa']['poa_gestion'].'
                            </div>
                        </td>
                        <td>
                            <div class="mv" style="text-align:justify">
                                <b>CÓDIGO POA: </b>'.$data['dato_poa']['poa_codigo'].'
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="mv" style="text-align:justify">
                                <b>APERTURA PROGRAMÁTICA: </b> '.$data['dato_poa']['aper_programa'] . $data['dato_poa']['aper_proyecto'] .
                                            $data['dato_poa']['aper_actividad'] . " - " . $data['dato_poa']['aper_descripcion'].' 
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="mv" style="text-align:justify">
                                <b>ACCI&Oacute;N DE MEDIANO PLAZO: </b> '.$data['dato_objest']['obje_codigo'] . ' -- ' . $data['dato_objest']['obje_objetivo'].' 
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="mv" style="text-align:justify">
                                <b>ACCI&Oacute;N DE CORTO PLAZO: </b>'.$data['dato_ogestion']['o_codigo'] . ' -- ' . $data['dato_ogestion']['o_objetivo'].'
                            </div>
                        </td>
                    </tr>  
                </table>

                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <p>
                    <table style="table-layout:fixed;">
                        '.$this->cont_prod_terminal($o_id,$gestion).'
                    </table>
                </p>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("producto_terminal.pdf", array("Attachment" => false));
    }
    public function cont_prod_terminal($o_id,$gestion)
    {
        //--------LISTA DE PRODUCTOS TERMINALES
        $lista_pterminal = $this->mp_terminal->lista_pterminal($o_id);
        $programacion = array();
        foreach ($lista_pterminal as $row) {
            $pt_id = $row['pt_id'];// id de mi objetivo estrategico
            $linea_base = $row['pt_linea_base'];
            $meta = $row['pt_meta']; //variable meta
            $programacion[$pt_id] = $this->programacion_mensual_prod_terminal($pt_id,$linea_base,$meta);
        }
        $producto_terminal = '';
        $n_prod = 0;
        foreach ($lista_pterminal as $fila) {
            $n_prod++;
            $producto_terminal .= '
                <tr>
                    <td class="sub_table">
                        <table>
                            <tr bgcolor="#696969">
                                <td style="width:3%;" class="header_table">#</td>
                                <td class="header_table">CÓDIGO</td>
                                <td class="header_table">OBJ. PRODUCTO TERMINAL</td>
                                <td class="header_table">RESPONSABLE</td>
                                <td class="header_table">UNIDAD ORGANIZACIONAL</td>
                                <td style="width:3%;" class="header_table">T.I.</td>
                                <td class="header_table">INDICADOR</td>
                                <td class="header_table">L/B</td>
                                <td class="header_table">META</td>
                                <td class="header_table">FUENTE VERIFICACIÓN</td>
                                <td style="width:30%;" class="header_table">PROGRAMACIÓN/EJECUCIÓN '.$gestion.'</td>
                            </tr>
                            <tr>
                                <td style="width:3%;">'.$n_prod.'</td>
                                <td>'.$fila['pt_codigo'].'</td>
                                <td>'.$fila['pt_objetivo'].'</td>
                                <td>'.$fila['fun_nombre'].' '.$fila['fun_paterno'].' '.$fila['fun_materno'].'</td>
                                <td>'.$fila['unidad'].'</td>
                                <td style="width:3%;">'.$fila['indi_abreviacion'].'</td>
                                <td>'.$fila['pt_indicador'].'</td>
                                <td>'.$fila['pt_linea_base'].'</td>
                                <td>'.$fila['pt_meta'].'</td>
                                <td>'.$fila['pt_fuente_verificacion'].'</td>
                                <td style="width:30%;">'.$this->temporalizacion_prod_terminal($fila['pt_id'],$programacion,$o_id).'</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            ';    
        }
        return $producto_terminal;
    }
    function programacion_mensual_prod_terminal($pt_id,$linea_base,$meta){
        $prog_acumulado = $linea_base; //variable de programacion acumulada
        $ejec_acumulado = $linea_base;
        for ($i = 1; $i <= 12 ; $i++) {
            $puntero_prog = 'p'.$i;//PUNTERO PARA LISTAR LA PROGRAMACION
            $puntero_ejec = 'e' . $i;
            $dato_programado = $this->mp_terminal->get_prog_pterminal($pt_id, $i);//lista de programado
            $dato_programado = $dato_programado[0]['ppm_fis'];
            $ejec = $this->mejec_ogestion_pterminal->get_eje_ptabsoluto($i, $pt_id);//dato de lo ejecutado
            $ejecut = (count($ejec) == 0) ? 0 : $ejec->pem_fis;
            $programado[$puntero_prog] = $dato_programado;//matriz
            $programado[$puntero_ejec] = $ejecut;
            //PROGRAMADO ACUMULADO--------------------------------------------------------------------------------
            $puntero_prog_acumulado = 'p_a' . $i;//PUNTERO PARA LA PROGRAMACION ACUMULADA
            $prog_acumulado += $dato_programado;
            $programado[$puntero_prog_acumulado] = $prog_acumulado;//matriz
            $puntero_ejec_acumulado = 'e_a' . $i;//PUNTERO PARA LA PROGRAMACION ACUMULADA
            $ejec_acumulado += $ejecut;
            $programado[$puntero_ejec_acumulado] = $ejec_acumulado;
            //PROGRAMA ACUMULADO PORECENTUAL----------------------------------------------------------------------
            $puntero_pa_porcentual = 'pa_porc' . $i;//PUNTERO PARA LA PROGRAMACION ACUMULADA PORCENTUAL
            $puntero_ea_porcentual = 'ea_porc' . $i;
            if($meta == 0){
                $pa_porcentual = 0;
                $ea_porcentual = 0;
            }else{
                $pa_porcentual = (($prog_acumulado / $meta) * 100);
                $ea_porcentual = (($ejec_acumulado / $meta) * 100);
            }
            $programado[$puntero_pa_porcentual] = $pa_porcentual;//matriz
            $programado[$puntero_ea_porcentual] = $ea_porcentual;
            $puntero_efi = 'efic' . $i;
            if($pa_porcentual == 0){
                $eficacia = 0;
            }else{
                $eficacia = (($ea_porcentual/$pa_porcentual) * 100);
            }
            $programado[$puntero_efi] = $eficacia;
        }
        return $programado;
    }
    public function temporalizacion_prod_terminal($pt_id,$programacion,$o_id)
    {
        $var = '
            <table>
                <tr>
                    <th class="header_subtable" style="width:6%">T.</th>
                    <th class="header_subtable">Ene.</th>
                    <th class="header_subtable">Feb.</th>
                    <th class="header_subtable">Mar.</th>
                    <th class="header_subtable">Abr.</th>
                    <th class="header_subtable">May.</th>
                    <th class="header_subtable">Jun.</th>
                </tr>
                <tr>
                    <td class="header_subtable">P.</td>
                    <td>'.round($programacion[$pt_id]['p1'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p2'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p3'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p4'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p5'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p6'],2).'</td>
                </tr>
                <tr>
                    <td class="header_subtable">P.A.</td>
                    <td>'.round($programacion[$pt_id]['p_a1'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a2'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a3'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a4'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a5'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a6'],2).'</td>
                </tr>
                <tr>
                    <td class="header_subtable">%P.A.</td>
                    <td>'.round($programacion[$pt_id]['pa_porc1'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc2'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc3'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc4'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc5'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc6'],2).'</td>
                </tr>
                <tr bgcolor="#696969">
                    <th class="header_subtable">T.</th>
                    <th class="header_subtable">Jul.</th>
                    <th class="header_subtable">Ago.</th>
                    <th class="header_subtable">Sep.</th>
                    <th class="header_subtable">Oct.</th>
                    <th class="header_subtable">Nov.</th>
                    <th class="header_subtable">Dic.</th>
                </tr>
                <tr>
                    <td class="header_subtable">P.</td>
                    <td>'.round($programacion[$pt_id]['p7'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p8'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p9'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p10'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p11'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p12'],2).'</td>
                </tr>
                <tr>
                    <td class="header_subtable">P.A.</td>
                    <td>'.round($programacion[$pt_id]['p_a7'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a8'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a9'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a10'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a11'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a12'],2).'</td>
                </tr>
                <tr>
                    <td class="header_subtable">%P.A.</td>
                    <td>'.round($programacion[$pt_id]['pa_porc7'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc8'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc9'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc10'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc11'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc12'],2).'</td>
                </tr>
            </table>
        ';
        return $var;
    }
    /////////*******FIN --> PRODUCTO TERMINAL*******////////

    ///************REPORTE DE RED OBJETIVOS ******************//////////////    <------- EDITANDO **************
    public function reporte_red_objetivos($poa_id,$caso)
    {
        $gestion = $this->session->userdata('gestion');
        $dato_poa = $this->mpoa->dato_poa_id($poa_id);
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> ACCI&Oacute;N DE CORTO PLAZO/PRODUCTO TERMINAL<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <div class="mv" style="text-align:justify">
                                <b>PROGRAMA : </b> '.$dato_poa[0]['aper_programa'].$dato_poa[0]['aper_proyecto'].$dato_poa[0]['aper_actividad'].'-'.$dato_poa[0]['aper_descripcion'].'
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="mv" style="text-align:justify">
                                <b>UNIDAD ORGANIZACIONAL: </b> '.$dato_poa[0]['uni_unidad'].'
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="mv" style="text-align:justify">
                                <b>FECHA CREACIÓN POA: </b> '.$dato_poa[0]['poa_fecha_creacion'].'
                            </div>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$this->red_objetivo_estrategico($poa_id,$gestion,$caso).'
                <br>
                <table style="width: 95%;margin: 0 auto;margin-top:30px;margin-bottom: 50px;">
                    <tr>
                        <td style="width:3%;margin-bottom: 10px;">FIRMAS: </td>
                        <td style="width:30%">
                            <hr>
                        </td>
                        <td style="width:3%"></td>
                        <td style="width:30%">
                            <hr>
                        </td>
                        <td style="width:3%"></td>
                        <td style="width:30%">
                            <hr>
                        </td>
                    </tr>
                </table>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("red_de_objetivos.pdf", array("Attachment" => false));
    }
    public function red_objetivo_estrategico($poa_id,$gestion,$caso)
    {
        $lista_objetivos = $this->mobjetivos->lista_obje_poa($poa_id);
        $temporalizacion = array();
        foreach ($lista_objetivos as $row1) {
            $obje_id = $row1['obje_id'];// id de mi objetivo estrategico
            $gestion_inicial = $row1['obje_gestion_curso'];// id de mi objetivo estrategico
            $linea_base = $row1['obje_linea_base'];
            $meta = $row1['obje_meta']; //variable meta
            $temporalizacion[$obje_id] = $this->progracion_gestion($obje_id, $gestion_inicial, $linea_base, $meta);
        }
        $data['lista_objetivos'] = $lista_objetivos;
        $data['temporalizacion'] = $temporalizacion;
        $html_objetivos = '';
        $n_obj = 0;
        if(count($data['lista_objetivos'])>=1){
            foreach ($data['lista_objetivos'] as $fila) {
                $n_obj++;
                $dato_poa = $this->mpoa->dato_poa_id($poa_id);//datos del poa
                $data['dato_poa'] = $dato_poa[0];
                $obje_id = $fila['obje_id'];
                $html_objetivos .= '
                    <table class="table_contenedor">
                        <tr>
                            <td style="border:solid 1px;border-color:black;text-align:left;font-size:10px;width:7%;background-color:#454545;" class="header_table fila_unitaria"> O.E.</td>
                            <td style="width:10%; border:solid 1px;" class="fila_unitaria">'.$fila['obje_codigo'].'</td>
                            <td class="fila_unitaria">'.$fila['obje_objetivo'].'</td>
                        </tr>
                    </table>
                    '.$this->red_objetivo_gestion($obje_id,($dato_poa[0]['aper_id']),$gestion,$n_obj,$poa_id,$caso).'
                ';
            }
        }else{
            $html_objetivos .= '
                <table class="table_contenedor">
                    <tr>
                        <td class="fila_unitaria">SIN ACCIONES DE MEDIANO PLAZO REGISTRADOS</td>
                    </tr>
                </table>
            ';
        }
        return $html_objetivos;
    }
    public function red_objetivo_gestion($obje_id,$aper_id,$gestion,$n_obj,$poa_id,$caso)
    {
        $lista_objgestion = $this->mobjetivo_gestion->lista_ogestion($obje_id,$aper_id);
        $tbl_objetivo_gestion = ($caso==1) ? 'PROGRAMADO/EJECUTADO '.$gestion : 'PROGRAMADO '.$gestion ;
        $programacion = array();
        foreach ($lista_objgestion as $row) {
            $o_id = $row['o_id'];// id de mi objetivo estrategico
            $linea_base = $row['o_linea_base'];
            $meta = $row['o_meta']; //variable meta
            $programacion[$o_id] = $this->programacion_mensual($o_id, $linea_base, $meta);
        }
        $objetivo_gestion = '';
        if(count($lista_objgestion) >= 1){
            $objetivo_gestion = '
                <div class="titulo_dictamen">ACCI&Oacute;N DE CORTO PLAZO</div>
            ';
            $sw = true;
            $n_gest = 0;
            foreach ($lista_objgestion as $fila) {
                $n_gest++;
                $objetivo_gestion .= '
                    <table style="table-layout:fixed;">
                        <tr>
                            <td class="sub_table">
                                <table>
                                    <tr>
                                        <td style="width:3%; background-color:#587fbb;" class="header_table">#</td>
                                        <td class="header_table" style="background-color:#587fbb;">CÓDIGO</td>
                                        <td class="header_table" style="background-color:#587fbb;">RESULTADO DE CORTO PLAZO</td>
                                        <td class="header_table" style="background-color:#587fbb;">RESPONSABLE</td>
                                        <td class="header_table" style="background-color:#587fbb;">UNIDAD ORGANIZACIONAL</td>
                                        <td class="header_table" style="background-color:#587fbb;">INDICADOR DE RESULTADO</td>
                                        <td style="width:3%;background-color:#587fbb;" class="header_table">T.I.</td>
                                        <td class="header_table" style="background-color:#587fbb;">L/B</td>
                                        <td class="header_table" style="background-color:#587fbb;">META</td>
                                        <td class="header_table" style="background-color:#587fbb;">FUENTE VERIFICACIÓN</td>
                                    </tr>
                                    <tr bgcolor="#F5F5F5">
                                        <td style="width:3%;">'.$n_gest.'</td>
                                        <td>'.$fila['o_codigo'].'</td>
                                        <td style="width:35%;">'.$fila['o_objetivo'].'</td>
                                        <td>'.$fila['fun_nombre'].' '.$fila['fun_paterno'].' '.$fila['fun_materno'].'</td>
                                        <td>'.$fila['unidad'].'</td>
                                        <td>'.$fila['o_indicador'].'</td>
                                        <td style="width:3%;">'.$fila['indi_abreviacion'].'</td>
                                        <td>'.$fila['o_linea_base'].'</td>
                                        <td>'.$fila['o_meta'].'</td>
                                        <td>'.$fila['o_fuente_verificacion'].'</td>
                                    </tr>
                                    <tr bgcolor="#F5F5F5">
                                        <td colspan=10>
                                            <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                                <thead>
                                                    <tr>
                                                        <td colspan=13 style="background-color:#587fbb;" class="header_table">'.$tbl_objetivo_gestion.'</td>
                                                    </tr>
                                                    <tr>
                                                        <th bgcolor="#2F4F4F" style="color: #FFFFFF;text-align: center;vertical-align: middle">
                                                            P./E.
                                                        </th>
                                                        <th bgcolor="#2F4F4F" style="color: #FFFFFF">Ene.</th>
                                                        <th bgcolor="#2F4F4F" style="color: #FFFFFF">Feb.</th>
                                                        <th bgcolor="#2F4F4F" style="color: #FFFFFF">Mar.</th>
                                                        <th bgcolor="#2F4F4F" style="color: #FFFFFF">Abr.</th>
                                                        <th bgcolor="#2F4F4F" style="color: #FFFFFF">May.</th>
                                                        <th bgcolor="#2F4F4F" style="color: #FFFFFF">Jun.</th>
                                                        <th bgcolor="#2F4F4F" style="color: #FFFFFF">Jul.</th>
                                                        <th bgcolor="#2F4F4F" style="color: #FFFFFF">Ago.</th>
                                                        <th bgcolor="#2F4F4F" style="color: #FFFFFF">Sep.</th>
                                                        <th bgcolor="#2F4F4F" style="color: #FFFFFF">Oct.</th>
                                                        <th bgcolor="#2F4F4F" style="color: #FFFFFF">Nov.</th>
                                                        <th bgcolor="#2F4F4F" style="color: #FFFFFF">Dic.</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="">
                                                '.$this->ejecutado_gestion($poa_id,$fila['o_id'],$caso).'
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    '.$this->red_objetivo_prod_terminal($poa_id,$fila['o_id'],$gestion,$n_obj,$n_gest,$caso).'
                ';
            }
        }
        else{
            $objetivo_gestion .= '
                <table class="table_contenedor">
                    <tr>
                        <td class="fila_unitaria">SIN OBJETIVOS DE GESTIÓN REGISTRADOS</td>
                    </tr>
                </table>
            ';
        }
        return $objetivo_gestion;
    }
    public function red_objetivo_prod_terminal($poa_id,$o_id,$gestion,$n_obj,$n_gest,$caso)
    {
        //--------LISTA DE PRODUCTOS TERMINALES
        $lista_pterminal = $this->mp_terminal->lista_pterminal($o_id);
        $tbl_prod = ($caso==1) ? 'PROGRAMADO/EJECUTADO '.$gestion : 'PROGRAMADO '.$gestion ;
        $programacion = array();
        foreach ($lista_pterminal as $row) {
            $pt_id = $row['pt_id'];// id de mi objetivo estrategico
            $linea_base = $row['pt_linea_base'];
            $meta = $row['pt_meta']; //variable meta
            $programacion[$pt_id] = $this->programacion_mensual_prod_terminal($pt_id,$linea_base,$meta);
        }
        $producto_terminal = '';
        if(count($lista_pterminal)>=1){
            $sw = true;
            $n_prod = 0;
            foreach ($lista_pterminal as $fila) {
                $n_prod++;
                if($sw){
                    $producto_terminal .= '
                        <table style="table-layout:fixed;">
                            <tr>
                                <td class="sub_table">
                                    <table>
                                        <tr><td colspan="11" class="titulo_dictamen" style="text-align:left; margin-bottom: 2cm;">PRODUCTO TERMINAL</td></tr>
                                        <tr bgcolor="#696969">
                                            <td style="width:3%;background-color:#5fabd3;" class="header_table">#</td>
                                            <td class="header_table" style="background-color:#5fabd3;">CÓDIGO</td>
                                            <td class="header_table" style="background-color:#5fabd3;">OBJ. PRODUCTO TERMINAL</td>
                                            <td class="header_table" style="background-color:#5fabd3;">RESPONSABLE</td>
                                            <td class="header_table" style="background-color:#5fabd3;">UNIDAD ORGANIZACIONAL</td>
                                            <td style="width:3%;background-color:#5fabd3;" class="header_table">T.I.</td>
                                            <td class="header_table" style="background-color:#5fabd3;">INDICADOR</td>
                                            <td class="header_table" style="background-color:#5fabd3;">L/B</td>
                                            <td class="header_table" style="background-color:#5fabd3;">META</td>
                                            <td class="header_table" style="background-color:#5fabd3;">FUENTE VERIFICACIÓN</td>
                                        </tr>
                                        <tr bgcolor="#F5F5F5">
                                            <td style="width:3%;">'.$n_prod.'</td>
                                            <td>'.$fila['pt_codigo'].'</td>
                                            <td style="width:30%;">'.$fila['pt_objetivo'].'</td>
                                            <td>'.$fila['fun_nombre'].' '.$fila['fun_paterno'].' '.$fila['fun_materno'].'</td>
                                            <td>'.$fila['unidad'].'</td>
                                            <td style="width:3%;">'.$fila['indi_abreviacion'].'</td>
                                            <td>'.$fila['pt_indicador'].'</td>
                                            <td>'.$fila['pt_linea_base'].'</td>
                                            <td>'.$fila['pt_meta'].'</td>
                                            <td>'.$fila['pt_fuente_verificacion'].'</td>
                                        </tr>
                                        <tr bgcolor="#F5F5F5">
                                            <td></td>
                                            <td></td>
                                            <td colspan=8>
                                                <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                                    <thead>
                                                        <tr>
                                                            <td colspan=13 style="background-color:#5fabd3;" class="header_table">'.$tbl_prod.'</td>
                                                        </tr>
                                                        <tr>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF;text-align: center;vertical-align: middle">
                                                                P./E.
                                                            </th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Ene.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Feb.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Mar.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Abr.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">May.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Jun.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Jul.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Ago.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Sep.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Oct.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Nov.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Dic.</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="">
                                                    '.$this->ejecutado_producto_terminal($poa_id,$o_id,$fila['pt_id'],$caso).'
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    ';
                    $sw = false;    
                }else{
                    $producto_terminal .= '
                        <table style="table-layout:fixed;">
                            <tr>
                                <td class="sub_table">
                                    <table>
                                        <tr bgcolor="#696969">
                                            <td style="width:3%;background-color:#5fabd3;" class="header_table">#</td>
                                            <td class="header_table" style="background-color:#5fabd3;">CÓDIGO</td>
                                            <td class="header_table" style="background-color:#5fabd3;">OBJ. PRODUCTO TERMINAL</td>
                                            <td class="header_table" style="background-color:#5fabd3;">RESPONSABLE</td>
                                            <td class="header_table" style="background-color:#5fabd3;">UNIDAD ORGANIZACIONAL</td>
                                            <td style="width:3%;background-color:#5fabd3;" class="header_table">T.I.</td>
                                            <td class="header_table" style="background-color:#5fabd3;">INDICADOR</td>
                                            <td class="header_table" style="background-color:#5fabd3;">L/B</td>
                                            <td class="header_table" style="background-color:#5fabd3;">META</td>
                                            <td class="header_table" style="background-color:#5fabd3;">FUENTE VERIFICACIÓN</td>
                                        </tr>
                                        <tr bgcolor="#F5F5F5">
                                            <td style="width:3%;">'.$n_prod.'</td>
                                            <td>'.$fila['pt_codigo'].'</td>
                                            <td style="width:30%;">'.$fila['pt_objetivo'].'</td>
                                            <td>'.$fila['fun_nombre'].' '.$fila['fun_paterno'].' '.$fila['fun_materno'].'</td>
                                            <td>'.$fila['unidad'].'</td>
                                            <td style="width:3%;">'.$fila['indi_abreviacion'].'</td>
                                            <td>'.$fila['pt_indicador'].'</td>
                                            <td>'.$fila['pt_linea_base'].'</td>
                                            <td>'.$fila['pt_meta'].'</td>
                                            <td>'.$fila['pt_fuente_verificacion'].'</td>
                                        </tr>
                                        <tr bgcolor="#F5F5F5">
                                            <td></td>
                                            <td></td>
                                            <td colspan=8>
                                                <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                                    <thead>
                                                        <tr>
                                                            <td colspan=13 style="background-color:#5fabd3;" class="header_table">'.$tbl_prod.'</td>
                                                        </tr>
                                                        <tr>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF;text-align: center;vertical-align: middle">
                                                                P./E.
                                                            </th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Ene.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Feb.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Mar.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Abr.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">May.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Jun.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Jul.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Ago.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Sep.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Oct.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Nov.</th>
                                                            <th bgcolor="#2F4F4F" style="color: #FFFFFF">Dic.</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="">
                                                    '.$this->ejecutado_producto_terminal($poa_id,$o_id,$fila['pt_id'],$caso).'
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    ';
                }
            }
        }else{
            $producto_terminal .= '
                <table class="table_contenedor">
                    <tr>
                        <td class="fila_unitaria">SIN PRODUCTOS TERMINALES REGISTRADOS</td>
                    </tr>
                </table>
            ';
        }
        return $producto_terminal;
    }
    public function temporalizacion_prod_terminal_ejec($pt_id,$programacion,$o_id)
    {
        // $ejecucion_objetivo_gestion = $this->mobjetivos->get_prod_terminal_ejecutado($pt_id);
        $var = '
            <table border=1 style="font-size:8px;">
                <tr>
                    <th class="header_subtable" style="width:6%">T.</th>
                    <th class="header_subtable">Ene.</th>
                    <th class="header_subtable">Feb.</th>
                    <th class="header_subtable">Mar.</th>
                    <th class="header_subtable">Abr.</th>
                    <th class="header_subtable">May.</th>
                    <th class="header_subtable">Jun.</th>
                    <th class="header_subtable">Jul.</th>
                    <th class="header_subtable">Ago.</th>
                    <th class="header_subtable">Sep.</th>
                    <th class="header_subtable">Oct.</th>
                    <th class="header_subtable">Nov.</th>
                    <th class="header_subtable">Dic.</th>
                </tr>
                <tr>
                    <td class="header_subtable">P.</td>
                    <td>'.round($programacion[$pt_id]['p1'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p2'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p3'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p4'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p5'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p6'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p7'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p8'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p9'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p10'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p11'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p12'],2).'</td>
                </tr>
                <tr>
                    <td class="header_subtable">P.A.</td>
                    <td>'.round($programacion[$pt_id]['p_a1'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a2'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a3'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a4'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a5'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a6'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a7'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a8'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a9'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a10'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a11'],2).'</td>
                    <td>'.round($programacion[$pt_id]['p_a12'],2).'</td>
                </tr>
                <tr>
                    <td class="header_subtable">%P.A.</td>
                    <td>'.round($programacion[$pt_id]['pa_porc1'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc2'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc3'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc4'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc5'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc6'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc7'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc8'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc9'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc10'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc11'],2).'</td>
                    <td>'.round($programacion[$pt_id]['pa_porc12'],2).'</td>
                </tr>
                <tr>
                    <td class="header_subtable">E.</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="header_subtable">E.A.</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="header_subtable">%E.A.</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="header_subtable">Ef.</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        ';
        return $var;
    }
    /////////*******FIN RED DE OBJETIVOS*******////////

    ///FUNCIONES ROYER PARA EJECUTADOS//////
    public function ejecutado_gestion($poa_id,$o_id,$caso)
    {
        $data['poa_id'] = $poa_id;
        $data['dato_poa'] = $this->mpoa->dato_poa_id($poa_id)[0];
        $data['dato_ogestion'] = $this->mobjetivo_gestion->get_ogestion($o_id)[0];
        $dato_ogestion = $this->mobjetivo_gestion->get_ogestion($o_id)[0];
        $dato_tabla = $this->tabla_prog_ejec_ogestion($o_id, $dato_ogestion,$caso);
        $data['tabla_prog_ejec'] = $dato_tabla['tabla'];
        return $data['tabla_prog_ejec'];
    }
    public function ejecutado_producto_terminal($poa_id, $o_id, $pt_id,$caso)
    {
        $data['poa_id'] = $poa_id;
        $data['dato_poa'] = $this->mpoa->dato_poa_id($poa_id)[0];
        $data['dato_ogestion'] = $this->mobjetivo_gestion->get_ogestion($o_id)[0];//objetivo de gestion
        $dato_pterminal = $this->mp_terminal->get_pterminal($pt_id)[0];//producto terminal
        $data['dato_pterminal'] = $dato_pterminal;//producto terminal
        $dato_tabla = $this->tabla_prog_ejec_pterminal($pt_id, $dato_pterminal,$caso);
        $data['tabla_prog_ejec'] = $dato_tabla['tabla'];
        return $data['tabla_prog_ejec'];
    }

///------------->PARA GESTION ;;
    //tabla de programacion mensual y ejecucion mensual
    function tabla_prog_ejec_ogestion($o_id, $dato,$caso)
    {
        $meta = $dato['o_meta'];
        $linea_base = $dato['o_linea_base'];
        $indicador = $dato['indi_id'];
        $denominador = $dato['o_denominador'];
        $mat_prog = $this->mat_prog_mes_ogestion($o_id, $meta, $linea_base); //matriz de la programacion
        $mat_ejec = $this->mat_ejec_mes_ogestion($o_id, $meta, $linea_base, $indicador, $denominador, $mat_prog); //matriz de la ejecucion
        $graf_prog = '';//vector para el grafico de programacion
        $graf_ejec = '';//vector para el grafico de ejecucion
        $ef_menor = '';//vector para el grafico de eficacia
        $ef_entre = '';//vector para el grafico de eficacia
        $ef_mayor = '';//vector para el grafico de eficacia
        $tabla = '';
        for ($i = 0; $i < count($mat_prog); $i++) {
            $tabla .= '<tr>';
            for ($j = 0; $j <= 12; $j++) {
                $tabla .= '<td style="background-color: #F5F5DC">';
                if ($j == 0) {
                    $tabla .= $mat_prog[$i][$j];
                } elseif ($i == 2) {
                    $tabla .= number_format(($mat_prog[$i][$j]), 2, ',', '.') . '%';
                    $graf_prog .= round($mat_prog[$i][$j]) . ',';
                } else {
                    $tabla .= number_format(($mat_prog[$i][$j]), 2, ',', '.');
                }
                $tabla .= '</td>';
            }
            $tabla .= '</tr>';
        }
        if($caso==1){
            if ($indicador == 1) {//absoluto
                for ($i = 0; $i < count($mat_ejec) - 1; $i++) {
                    $tabla .= '<tr>';
                    for ($j = 0; $j <= 12; $j++) {
                        $tabla .= '<td style="background:#98FB98 ">';
                        if ($j == 0) {
                            $tabla .= $mat_ejec[$i][$j];
                        } elseif ($i == 2) {
                            $tabla .= number_format(($mat_ejec[$i][$j]), 2, ',', '.') . '%';
                            $graf_ejec .= round($mat_ejec[$i][$j]) . ',';
                        } else {
                            $tabla .= number_format(($mat_ejec[$i][$j]), 2, ',', '.');
                        }
                        $tabla .= '</td>';
                    }
                    $tabla .= '</tr>';
                }
                $cont = 3;//puntero para eficacia
            } else {//relativo
                for ($i = 0; $i < count($mat_ejec) - 1; $i++) {
                    $tabla .= '<tr>';
                    for ($j = 0; $j <= 12; $j++) {
                        $tabla .= '<td style="background:#98FB98 ">';
                        if ($j == 0) {
                            $tabla .= $mat_ejec[$i][$j];
                        } elseif ($i == 4) {
                            $tabla .= number_format(($mat_ejec[$i][$j]), 2, ',', '.') . '%';
                            $graf_ejec .= round($mat_ejec[$i][$j]) . ',';
                        } else {
                            $tabla .= number_format(($mat_ejec[$i][$j]), 2, ',', '.');
                        }
                        $tabla .= '</td>';
                    }
                    $tabla .= '</tr>';
                }
                $cont = 5;//puntero para eficacia
            }
            //EFICACIA
            $tabla .= '<tr>';
            $tabla .= '<td style="background:#B0E0E6">' . $mat_ejec[$cont][0] . '</td>';
            for ($j = 1; $j <= 12; $j++) {
                $tabla .= '<td style="background:#B0E0E6">';
                $tabla .= number_format(($mat_ejec[$cont][$j]), 2, ',', '.') . '%';
                $efi = round($mat_ejec[$cont][$j]);//EFICACIA
                $tabla .= '</td>';
                $data_efi = $this->get_eficacia(round($efi));
                $ef_menor .= $data_efi['menor'];
                $ef_entre .= $data_efi['entre'];
                $ef_mayor .= $data_efi['mayor'];

            }
            $tabla .= '</tr>';
        }
        $dato['tabla'] = $tabla;
        $dato['graf_prog'] = $graf_prog;
        $dato['graf_ejec'] = $graf_ejec;
        $dato['ef_menor'] = $ef_menor;
        $dato['ef_entre'] = $ef_entre;
        $dato['ef_mayor'] = $ef_mayor;
        return $dato;
    }

    function get_eficacia($efi)
    {
        if ($efi <= 75) {
            $d['menor'] = "{y: " . $efi . ", color: 'red'},";
            $d['entre'] = "{y: 0, color: 'yellow'},";
            $d['mayor'] = "{y: 0, color: 'green'},";
        }
        if ($efi >= 76 && $efi <= 90) {
            $d['entre'] = "{y: " . $efi . ", color: 'yellow'},";
            $d['menor'] = "{y: 0, color: 'red'},";
            $d['mayor'] = "{y: 0, color: 'green'},";
        }
        if ($efi >= 91) {
            $d['mayor'] = "{y: " . $efi . ", color: 'green'},";
            $d['entre'] = "{y: 0, color: 'yellow'},";
            $d['menor'] = "{y: 0, color: 'red'},";
        }
        return $d;
    }

    //matriz de la programacion mensual del pbjetiv de gestion
    function mat_prog_mes_ogestion($o_id, $meta, $linea_base)
    {
        $lista_prog = $this->malerta_ogestion->prog_mensual_ogestion($o_id);
        if (count($lista_prog) != 0) {
            $lista_prog = $this->malerta_ogestion->prog_mensual_ogestion($o_id)[0];
            $prog_acumulado = $linea_base;
            $mat_prog[0][0] = 'P.';
            $mat_prog[1][0] = 'P.A.';
            $mat_prog[2][0] = 'P.A.[%]';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_bd = 'fis' . $i;
                $prog_fisica = $lista_prog[$puntero_bd];
                $mat_prog[0][$i] = $prog_fisica;//programacion fisica
                $prog_acumulado += $prog_fisica;
                $mat_prog[1][$i] = $prog_acumulado;//programacion acumulada
                $mat_prog[2][$i] = ($meta == 0) ? 0 : (($prog_acumulado / $meta) * 100);//programacion acumulada en porcentaje
            }
            return $mat_prog;
        } else {
            return 0;
        }

    }

    //verificar si la ejecucion es absoluto o relativo
    function mat_ejec_mes_ogestion($o_id, $meta, $linea_base, $indicador, $denominador, $mat_prog)
    {
        if ($indicador == 1) {
            $matr = $this->mat_ejec_abs($o_id, $linea_base, $meta, $mat_prog);
        } else {
            $matr = $this->mat_ejec_rel($o_id, $linea_base, $meta, $denominador, $mat_prog);
        }
        return $matr;
    }

    //ejecucion mensual de tipo absoluto
    function mat_ejec_abs($o_id, $linea_base, $meta, $mat_prog)
    {
        $lista_ejec = $this->malerta_ogestion->ejec_abs_ogestion($o_id);
        if (count($lista_ejec) != 0) {
            $lista_ejec = $this->malerta_ogestion->ejec_abs_ogestion($o_id)[0];
            $ejec_acumulado = $linea_base;
            $mat_ejec[0][0] = 'E.';
            $mat_ejec[1][0] = 'E.A.';
            $mat_ejec[2][0] = 'E.A.[%]';
            $mat_ejec[3][0] = 'Eficacia';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_bd = 'fis' . $i;
                $ejec_fisica = $lista_ejec[$puntero_bd];
                $mat_ejec[0][$i] = $ejec_fisica;//ejecucion fisica
                $ejec_acumulado += $ejec_fisica;
                $mat_ejec[1][$i] = $ejec_acumulado;//ejecucion acumulada
                $mat_ejec[2][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                $mat_ejec[3][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[2][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
            }
            return $mat_ejec;
        } else {
            return 0;
        }

    }

    //ejecucion mensual de tipo relativo
    function mat_ejec_rel($o_id, $linea_base, $meta, $denominador, $mat_prog)
    {
        $lista_ejec = $this->malerta_ogestion->ejec_rel_ogestion($o_id);
        if (count($lista_ejec) != 0) {
            $lista_ejec = $this->malerta_ogestion->ejec_rel_ogestion($o_id)[0];
            $ejec_acumulado = $linea_base;
            $mat_ejec[0][0] = 'CASO FAVORABLE';
            $mat_ejec[1][0] = 'CASO DESFAVORABLE';
            $mat_ejec[2][0] = 'EJECUCIÓN';
            $mat_ejec[3][0] = 'EJECUCIÓN ACUMUALADA';
            $mat_ejec[4][0] = 'EJECUCIÓN ACUMUALADA PORCENTUAL [%]';
            $mat_ejec[5][0] = 'EFICACIA';
            if ($denominador == 0) {//variable
                for ($i = 1; $i <= 12; $i++) {
                    $punt_fav = 'fav' . $i; //puntero
                    $punt_des = 'des' . $i;
                    $favorable = $lista_ejec[$punt_fav];
                    $desfavorable = $lista_ejec[$punt_des];
                    $mat_ejec[0][$i] = $favorable;
                    $mat_ejec[1][$i] = $desfavorable;
                    $ejec_fisica = ($desfavorable <= 0) ? 0 : ($favorable / $desfavorable) * $mat_prog[0][$i];
                    $mat_ejec[2][$i] = $ejec_fisica;
                    $ejec_acumulado += $ejec_fisica;
                    $mat_ejec[3][$i] = $ejec_acumulado;//ejecucion acumulada
                    $mat_ejec[4][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                    $mat_ejec[5][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[4][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
                }
            } else {//fijo
                for ($i = 1; $i <= 12; $i++) {
                    $punt_fav = 'fav' . $i; //puntero
                    $punt_des = 'des' . $i;
                    $favorable = $lista_ejec[$punt_fav];
                    $desfavorable = $lista_ejec[$punt_des];
                    $mat_ejec[0][$i] = $favorable;
                    $mat_ejec[1][$i] = $desfavorable;
                    $ejec_fisica = ($desfavorable <= 0) ? 0 : ($favorable / $desfavorable) * 100;
                    $mat_ejec[2][$i] = $ejec_fisica;
                    $ejec_acumulado += $ejec_fisica;
                    $mat_ejec[3][$i] = $ejec_acumulado;//ejecucion acumulada
                    $mat_ejec[4][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                    $mat_ejec[5][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[4][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
                }
            }
            return $mat_ejec;
        } else {
            return 0;
        }
    }
///------------->PARA PRODUCTO TERMINAL;;

    //programacion y ejecucion del produto terminal
    function tabla_prog_ejec_pterminal($pt_id, $dato,$caso)
    {
        $meta = $dato['pt_meta'];
        $linea_base = $dato['pt_linea_base'];
        $indicador = $dato['indi_id'];
        $denominador = $dato['pt_denominador'];
        $mat_prog = $this->mat_prog_mes_pterminal_p($pt_id, $meta, $linea_base); //matriz de la programacion
        $mat_ejec = $this->mat_ejec_mes_pterminal_p($pt_id, $meta, $linea_base, $indicador, $denominador, $mat_prog); //matriz de ejecucion
        $graf_prog = '';//vector para el grafico de programacion
        $graf_ejec = '';//vector para el grafico de ejecucion
        $ef_menor = '';//vector para el grafico de eficacia
        $ef_entre = '';//vector para el grafico de eficacia
        $ef_mayor = '';//vector para el grafico de eficacia
        $tabla = '';
        for ($i = 0; $i < count($mat_prog); $i++) {
            $tabla .= '<tr>';
            for ($j = 0; $j <= 12; $j++) {
                $tabla .= '<td style="background-color: #F5F5DC">';
                if ($j == 0) {
                    $tabla .= $mat_prog[$i][$j];
                } elseif ($i == 2) {
                    $tabla .= number_format(($mat_prog[$i][$j]), 2, ',', '.') . '%';
                    $graf_prog .= round($mat_prog[$i][$j]) . ',';
                } else {
                    $tabla .= number_format(($mat_prog[$i][$j]), 2, ',', '.');
                }
                $tabla .= '</td>';
            }
            $tabla .= '</tr>';
        }
        if($caso==1){
            if ($indicador == 1) {//absoluto
                for ($i = 0; $i < count($mat_ejec) - 1; $i++) {
                    $tabla .= '<tr>';
                    for ($j = 0; $j <= 12; $j++) {
                        $tabla .= '<td style="background:#98FB98 ">';
                        if ($j == 0) {
                            $tabla .= $mat_ejec[$i][$j];
                        } elseif ($i == 2) {
                            $tabla .= number_format(($mat_ejec[$i][$j]), 2, ',', '.') . '%';
                            $graf_ejec .= round($mat_ejec[$i][$j]) . ',';
                        } else {
                            $tabla .= number_format(($mat_ejec[$i][$j]), 2, ',', '.');
                        }
                        $tabla .= '</td>';
                    }
                    $tabla .= '</tr>';
                }
                $cont = 3;//puntero para eficacia
            } else {//relativo
                for ($i = 0; $i < count($mat_ejec) - 1; $i++) {
                    $tabla .= '<tr>';
                    for ($j = 0; $j <= 12; $j++) {
                        $tabla .= '<td style="background:#98FB98 ">';
                        if ($j == 0) {
                            $tabla .= $mat_ejec[$i][$j];
                        } elseif ($i == 4) {
                            $tabla .= number_format(($mat_ejec[$i][$j]), 2, ',', '.') . '%';
                            $graf_ejec .= round($mat_ejec[$i][$j]) . ',';
                        } else {
                            $tabla .= number_format(($mat_ejec[$i][$j]), 2, ',', '.');
                        }
                        $tabla .= '</td>';
                    }
                    $tabla .= '</tr>';
                }
                $cont = 5;//contador para eficacia
            }
            //EFICACIA
            $tabla .= '<tr>';
            $tabla .= '<td style="background:#B0E0E6">' . $mat_ejec[$cont][0] . '</td>';
            for ($j = 1; $j <= 12; $j++) {
                $tabla .= '<td style="background:#B0E0E6">';
                $tabla .= number_format(($mat_ejec[$cont][$j]), 2, ',', '.') . '%';
                $efi = round($mat_ejec[$cont][$j]);//EFICACIA
                $tabla .= '</td>';
                $data_efi = $this->get_eficacia($efi);
                $ef_menor .= $data_efi['menor'];
                $ef_entre .= $data_efi['entre'];
                $ef_mayor .= $data_efi['mayor'];

            }
            $tabla .= '</tr>';
        }
        $dato['tabla'] = $tabla;
        $dato['graf_prog'] = $graf_prog;
        $dato['graf_ejec'] = $graf_ejec;
        $dato['ef_menor'] = $ef_menor;
        $dato['ef_entre'] = $ef_entre;
        $dato['ef_mayor'] = $ef_mayor;
        return $dato;
    }

    //matriz de la programacion mensual del producto terminal
    function mat_prog_mes_pterminal_p($pt_id, $meta, $linea_base)
    {
        $lista_prog = $this->malerta_pterminal->prog_mensual_pterminal($pt_id);
        if (count($lista_prog) != 0) {
            $lista_prog = $this->malerta_pterminal->prog_mensual_pterminal($pt_id)[0];
            $prog_acumulado = $linea_base;
            $mat_prog[0][0] = 'P.';
            $mat_prog[1][0] = 'P.A.';
            $mat_prog[2][0] = 'P.A.[%]';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_bd = 'fis' . $i;
                $prog_fisica = $lista_prog[$puntero_bd];
                $mat_prog[0][$i] = $prog_fisica;//programacion fisica
                $prog_acumulado += $prog_fisica;
                $mat_prog[1][$i] = $prog_acumulado;//programacion acumulada
                $mat_prog[2][$i] = ($meta == 0) ? 0 : (($prog_acumulado / $meta) * 100);//programacion acumulada en porcentaje
            }
            return $mat_prog;
        } else {
            return 0;
        }

    }

    //verificar si la ejecucion es absoluto o relativo
    function mat_ejec_mes_pterminal_p($pt_id, $meta, $linea_base, $indicador, $denominador, $mat_prog)
    {
        if ($indicador == 1) {
            $matr = $this->mat_ejec_abs_p($pt_id, $linea_base, $meta, $mat_prog);
        } else {
            $matr = $this->mat_ejec_rel_p($pt_id, $linea_base, $meta, $denominador, $mat_prog);
        }
        return $matr;
    }

    //ejecucion mensual de tipo absoluto
    function mat_ejec_abs_p($pt_id, $linea_base, $meta, $mat_prog)
    {
        $lista_ejec = $this->malerta_pterminal->ejec_abs_pterminal($pt_id);
        if (count($lista_ejec) != 0) {
            $lista_ejec = $this->malerta_pterminal->ejec_abs_pterminal($pt_id)[0];
            $ejec_acumulado = $linea_base;
            $mat_ejec[0][0] = 'E.';
            $mat_ejec[1][0] = 'E.A.';
            $mat_ejec[2][0] = 'E.A.[%]';
            $mat_ejec[3][0] = 'Eficacia';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_bd = 'fis' . $i;
                $ejec_fisica = $lista_ejec[$puntero_bd];
                $mat_ejec[0][$i] = $ejec_fisica;//ejecucion fisica
                $ejec_acumulado += $ejec_fisica;
                $mat_ejec[1][$i] = $ejec_acumulado;//ejecucion acumulada
                $mat_ejec[2][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                $mat_ejec[3][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[2][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
            }
            return $mat_ejec;
        } else {
            return 0;
        }

    }

    //ejecucion mensual de tipo relativo
    function mat_ejec_rel_p($pt_id, $linea_base, $meta, $denominador, $mat_prog)
    {
        $lista_ejec = $this->malerta_pterminal->ejec_rel_pterminal($pt_id);
        if (count($lista_ejec) != 0) {
            $lista_ejec = $this->malerta_pterminal->ejec_rel_pterminal($pt_id)[0];
            $ejec_acumulado = $linea_base;
            $mat_ejec[0][0] = 'Caso Favorable';
            $mat_ejec[1][0] = 'Caso Desfavorable';
            $mat_ejec[2][0] = 'E.';
            $mat_ejec[3][0] = 'E.A.';
            $mat_ejec[4][0] = 'E.A.[%]';
            $mat_ejec[5][0] = 'Eficacia';
            if ($denominador == 0) {//variable
                for ($i = 1; $i <= 12; $i++) {
                    $punt_fav = 'fav' . $i; //puntero
                    $punt_des = 'des' . $i;
                    $favorable = $lista_ejec[$punt_fav];
                    $desfavorable = $lista_ejec[$punt_des];
                    $mat_ejec[0][$i] = $favorable;
                    $mat_ejec[1][$i] = $desfavorable;
                    $ejec_fisica = ($desfavorable <= 0) ? 0 : ($favorable / $desfavorable) * $mat_prog[0][$i];
                    $mat_ejec[2][$i] = $ejec_fisica;
                    $ejec_acumulado += $ejec_fisica;
                    $mat_ejec[3][$i] = $ejec_acumulado;//ejecucion acumulada
                    $mat_ejec[4][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                    $mat_ejec[5][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[4][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
                }
            } else {//fijo
                for ($i = 1; $i <= 12; $i++) {
                    $punt_fav = 'fav' . $i; //puntero
                    $punt_des = 'des' . $i;
                    $favorable = $lista_ejec[$punt_fav];
                    $desfavorable = $lista_ejec[$punt_des];
                    $mat_ejec[0][$i] = $favorable;
                    $mat_ejec[1][$i] = $desfavorable;
                    $ejec_fisica = ($desfavorable <= 0) ? 0 : ($favorable / $desfavorable) * 100;
                    $mat_ejec[2][$i] = $ejec_fisica;
                    $ejec_acumulado += $ejec_fisica;
                    $mat_ejec[3][$i] = $ejec_acumulado;//ejecucion acumulada
                    $mat_ejec[4][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                    $mat_ejec[5][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[4][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
                }
            }
            return $mat_ejec;
        } else {
            return 0;
        }
    }

    public function reporte_acciones($poa_id,$caso)
    {
        $gestion = $this->session->userdata('gestion');
        $dato_poa = $this->mpoa->dato_poa_id($poa_id);
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').' </b><br>
                            <b>FORMULARIO : </b> OBJETIVO DE GESTIÓN/PRODUCTO TERMINAL<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <div class="mv" style="text-align:justify">
                                <h1>'.$poa_id.'</h1>
                                <b>PROGRAMA : </b> '.$dato_poa[0]['aper_programa'].$dato_poa[0]['aper_proyecto'].$dato_poa[0]['aper_actividad'].'-'.$dato_poa[0]['aper_descripcion'].'
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="mv" style="text-align:justify">
                                <b>UNIDAD ORGANIZACIONAL: </b> '.$dato_poa[0]['uni_unidad'].'
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="mv" style="text-align:justify">
                                <b>FECHA CREACIÓN POA: </b> '.$dato_poa[0]['poa_fecha_creacion'].'
                            </div>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$this->acciones_informacion_poa($poa_id,$gestion,$caso).'
                <br>
                <table style="width: 95%;margin: 0 auto;margin-top:30px;margin-bottom: 50px;">
                    <tr>
                        <td style="width:3%;margin-bottom: 10px;">FIRMAS: </td>
                        <td style="width:30%">
                            <hr>
                        </td>
                        <td style="width:3%"></td>
                        <td style="width:30%">
                            <hr>
                        </td>
                        <td style="width:3%"></td>
                        <td style="width:30%">
                            <hr>
                        </td>
                    </tr>
                </table>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("red_de_objetivos.pdf", array("Attachment" => false));
    }

    public function acciones_informacion_poa($poa_id,$gestion,$caso)
    {
        $lista_objetivos = $this->mobjetivos->lista_obje_poa($poa_id);
        $temporalizacion = array();
        foreach ($lista_objetivos as $row1) {
            $obje_id = $row1['obje_id'];// id de mi objetivo estrategico
            $gestion_inicial = $row1['obje_gestion_curso'];// id de mi objetivo estrategico
            $linea_base = $row1['obje_linea_base'];
            $meta = $row1['obje_meta']; //variable meta
            $temporalizacion[$obje_id] = $this->progracion_gestion($obje_id, $gestion_inicial, $linea_base, $meta);
        }
        $data['lista_objetivos'] = $lista_objetivos;
        $data['temporalizacion'] = $temporalizacion;
        $html_objetivos = '';
        $n_obj = 0;
        if(count($data['lista_objetivos'])>=1){
            foreach ($data['lista_objetivos'] as $fila) {
                $n_obj++;
                $dato_poa = $this->mpoa->dato_poa_id($poa_id);//datos del poa
                $data['dato_poa'] = $dato_poa[0];
                $obje_id = $fila['obje_id'];
                $html_objetivos .= '
                    <table class="table_contenedor">
                        <tr>
                            <td style="border:solid 1px;border-color:black;text-align:left;font-size:10px;width:7%;background-color:#454545;" class="header_table fila_unitaria"> O.E.</td>
                            <td style="width:10%; border:solid 1px;" class="fila_unitaria">'.$fila['obje_codigo'].'</td>
                            <td class="fila_unitaria">'.$fila['obje_objetivo'].'</td>
                        </tr>
                    </table>
                    '.$this->acciones_red_objetivo($obje_id,($dato_poa[0]['aper_id']),$gestion,$n_obj,$poa_id,$caso).'
                ';
            }
        }else{
            $html_objetivos .= '
                <table class="table_contenedor">
                    <tr>
                        <td class="fila_unitaria">SIN OBJETIVOS ESTRATEGICOS REGISTRADOS</td>
                    </tr>
                </table>
            ';
        }
        return $html_objetivos;
    }

    public function acciones_red_objetivo($obje_id,$aper_id,$gestion,$n_obj,$poa_id,$caso)
    {
        $lista_objgestion = $this->mobjetivo_gestion->lista_ogestion($obje_id,$aper_id);
        $tbl_objetivo_gestion = ($caso==1) ? 'PROGRAMADO/EJECUTADO '.$gestion : 'PROGRAMADO '.$gestion ;
        $programacion = array();
        foreach ($lista_objgestion as $row) {
            $o_id = $row['o_id'];// id de mi objetivo estrategico
            $linea_base = $row['o_linea_base'];
            $meta = $row['o_meta']; //variable meta
            $programacion[$o_id] = $this->programacion_mensual($o_id, $linea_base, $meta);
        }
        $objetivo_gestion = '';
        if(count($lista_objgestion) >= 1){
            $objetivo_gestion = '
                <div class="titulo_dictamen">PROYECTO INFORMACIÓN ACCIONES </div>
                <table style="table-layout:fixed;">
                    <tr>
                        <td class="sub_table">
                            <table>
                                <tr>
                                    <td style="width:3%; background-color:#217346;" class="header_table">#</td>
                                    <td class="header_table" style="background-color:#217346;">CÓDIGO</td>
                                    <td class="header_table" style="background-color:#217346;">TIPO PROYECTO</td>
                                    <td class="header_table" style="background-color:#217346;">RESPONSABLE</td>
                                    <td class="header_table" style="background-color:#217346;">UNIDAD ORGANIZACIONAL</td>
                                    <td style="width:3%;background-color:#217346;" class="header_table">T.I.</td>
                                    <td class="header_table" style="background-color:#217346;">INDICADOR</td>
                                    <td class="header_table" style="background-color:#217346;">L/B</td>
                                    <td class="header_table" style="background-color:#217346;">META</td>
                                    <td class="header_table" style="background-color:#217346;">FUENTE VERIFICACIÓN</td>
                                </tr>
                                <tr bgcolor="#F5F5F5">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                </table>
            ';
            $sw = true;
            $n_gest = 0;
            foreach ($lista_objgestion as $fila) {
                $n_gest++;
                $objetivo_gestion .= '
                    '.$this->red_objetivo_prod_terminal($poa_id,$fila['o_id'],$gestion,$n_obj,$n_gest,$caso).'
                ';
            }
        }
        else{
            $objetivo_gestion .= '
                <table class="table_contenedor">
                    <tr>
                        <td class="fila_unitaria">SIN OBJETIVOS DE GESTIÓN REGISTRADOS</td>
                    </tr>
                </table>
            ';
        }
        return $objetivo_gestion;
    }

}