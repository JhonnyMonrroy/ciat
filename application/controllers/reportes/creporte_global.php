<?php

class Creporte_global extends CI_Controller
{
    function __construct()
    {
        parent:: __construct();
        $this->load->model('reportes/reporte_gerencial/mreporte_global');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(7);
        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
    }

    function index()
    {
        $data['mes'] = $this->mes_nombre();
        $mes_id = $this->session->userData('mes');
        $data_tabla = $this->tabla_institucional($mes_id, $this->gestion);
        $data['tabla'] = $data_tabla['tabla'];
        /* $data_tabla = $this->tabla_nivel_tipo_proyecto($mes_id, $this->gestion);
        $data['tabla'] = $data_tabla['tabla'];
        $data['pie_proy'] = $data_tabla['pie_proy'];
        $data['col_proy_titulo'] = $data_tabla['col_proy_titulo'];
        $data['col_proy_data'] = $data_tabla['col_proy_data'];*/
        $ruta = 'reportes/reportes_gerenciales/reporte_global/vmain';
        $this->construir_vista($ruta, $data);
    }

    function get_financiamiento()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $mes_id = $this->session->userData('mes');
            $data_tabla = $this->get_institucional_tipo_proy($mes_id, $this->gestion, $post);
            $data = array(
                "tabla" => $data_tabla['tabla']
            );
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function get_vec_tipo_proy($tipo_proy){
        $proy_inv = $tipo_proy['proy_inv'];
        $prog_rec = $tipo_proy['prog_rec'];
        $prog_no_rec = $tipo_proy['prog_no_rec'];
        $accion_fun = $tipo_proy['accion_fun'];

        $proy_inv = ($proy_inv == 'true') ? '1' : '';
        $prog_rec = ($prog_rec == 'true') ? '2' : '';
        $prog_no_rec = ($prog_no_rec == 'true') ? '3' : '';
        $accion_fun = ($accion_fun == 'true') ? '4' : '';

        $cont = 0;
        if (strlen($proy_inv) != 0) {
            $vec_proy_tipo[$cont] = $proy_inv;
            $cont++;
        }
        if (strlen($prog_rec) != 0) {
            $vec_proy_tipo[$cont] = $prog_rec;
            $cont++;
        }
        if (strlen($prog_no_rec) != 0) {
            $vec_proy_tipo[$cont] = $prog_no_rec;
            $cont++;
        }
        if (strlen($accion_fun) != 0) {
            $vec_proy_tipo[$cont] = $accion_fun;
        }

        $tipo = '';
        for ($i = 0; $i < count($vec_proy_tipo); $i++) {
            $tipo .= ($i == (count($vec_proy_tipo)-1)) ? $vec_proy_tipo[$i] : $vec_proy_tipo[$i] . ',';
        }
        return $tipo;
    }

    function get_institucional_tipo_proy($mes_id, $gestion, $tipo_proy)
    {
        $vec = $this->get_vec_tipo_proy($tipo_proy);
        if(strlen($vec) == 0){
            $dato['tabla'] = '<tr></tr>';
        }else{
            $lista = $this->mreporte_global->reporte_institucional_tipo_proy($mes_id, $gestion, $vec);
            $sum_inversion = 0;
            $sum_preinversion = 0;
            $sum_operacion = 0;
            $sum_total = 0;
            $sum_inicial = 0;
            $sum_mod = 0;
            $sum_ppto_vigente = 0;
            $sum_devengado = 0;
            $sum_saldo = 0;
            foreach ($lista as $item) {
                $sum_inversion += $item['inversion'];
                $sum_preinversion += $item['preinversion'];
                $sum_operacion += $item['operacion'];
                $sum_total += $item['total'];
                $sum_inicial += $item['ppto_inicial'];
                $sum_mod += $item['mod_aprobadas'];
                $sum_ppto_vigente += $item['ppto_vigente'];
                $sum_devengado += $item['devengado'];
                $sum_saldo += $item['saldo'];
            }
            $tabla = '';
            foreach ($lista as $item) {
                $tabla .= '<tr>';
                $tabla .= '<td>' . $item['tp_tipo'] . '</td>';
                $tabla .= '<td>' . $item['inversion'] . '</td>';
                $tabla .= '<td>' . $item['preinversion'] . '</td>';
                $tabla .= '<td>' . $item['operacion'] . '</td>';
                $tabla .= '<td>' . number_format($item['total'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['ppto_inicial'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['mod_aprobadas'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['ppto_vigente'], 2, ',', '.') . '</td>';
                $porc = ($sum_ppto_vigente == 0) ? 0 : number_format((($item['ppto_vigente'] / $sum_ppto_vigente) * 100), 2, '.', '');
                $tabla .= '<td>' . $porc . ' %</td>';
                $tabla .= '<td>' . number_format($item['devengado'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['saldo'], 2, ',', '.') . '</td>';
                $porc_ejec = ($item['ppto_vigente'] == 0) ? 0 : number_format((($item['devengado'] / $item['ppto_vigente']) * 100), 2, '.', '');
                $tabla .= '<td>' . $porc_ejec . ' %</td>';
                $tabla .= '</tr>';
            }
            $tabla .= '<tr style="background: #3A3633;color: #fff">';
            $tabla .= '<td style="color: #fff"> TOTAL </td>';
            $tabla .= '<td style="color: #fff">' . $sum_inversion . '</td>';
            $tabla .= '<td style="color: #fff">' . $sum_preinversion . '</td>';
            $tabla .= '<td style="color: #fff">' . $sum_operacion . '</td>';
            $tabla .= '<td style="color: #fff">' . number_format($sum_total, 2, ',', '.') . '</td>';
            $tabla .= '<td style="color: #fff">' . number_format($sum_inicial, 2, ',', '.') . '</td>';
            $tabla .= '<td style="color: #fff">' . number_format($sum_mod, 2, ',', '.') . '</td>';
            $tabla .= '<td style="color: #fff">' . number_format($sum_ppto_vigente, 2, ',', '.') . '</td>';
            $tabla .= '<td style="color: #fff"> 100 %</td>';
            $tabla .= '<td style="color: #fff">' . number_format($sum_devengado, 2, ',', '.') . '</td>';
            $tabla .= '<td style="color: #fff">' . number_format($sum_saldo, 2, ',', '.') . '</td>';
            $porc_ejec = ($sum_ppto_vigente == 0) ? 0 : number_format((($sum_devengado / $sum_ppto_vigente) * 100), 2, '.', '');
            $tabla .= '<td style="color: #fff">' . $porc_ejec . ' %</td>';
            $tabla .= '</tr>';
            $dato['tabla'] = $tabla;
        }


        return $dato;
    }

    function tabla_institucional($mes_id, $gestion)
    {
        $lista = $this->mreporte_global->reporte_institucional($mes_id, $gestion);
        $sum_inversion = 0;
        $sum_preinversion = 0;
        $sum_operacion = 0;
        $sum_total = 0;
        $sum_inicial = 0;
        $sum_mod = 0;
        $sum_ppto_vigente = 0;
        $sum_devengado = 0;
        $sum_saldo = 0;
        foreach ($lista as $item) {
            $sum_inversion += $item['inversion'];
            $sum_preinversion += $item['preinversion'];
            $sum_operacion += $item['operacion'];
            $sum_total += $item['total'];
            $sum_inicial += $item['ppto_inicial'];
            $sum_mod += $item['mod_aprobadas'];
            $sum_ppto_vigente += $item['ppto_vigente'];
            $sum_devengado += $item['devengado'];
            $sum_saldo += $item['saldo'];
        }
        $tabla = '';
        foreach ($lista as $item) {
            $tabla .= '<tr>';
            $tabla .= '<td>' . $item['tp_tipo'] . '</td>';
            $tabla .= '<td>' . $item['inversion'] . '</td>';
            $tabla .= '<td>' . $item['preinversion'] . '</td>';
            $tabla .= '<td>' . $item['operacion'] . '</td>';
            $tabla .= '<td>' . number_format($item['total'], 2, ',', '.') . '</td>';
            $tabla .= '<td>' . number_format($item['ppto_inicial'], 2, ',', '.') . '</td>';
            $tabla .= '<td>' . number_format($item['mod_aprobadas'], 2, ',', '.') . '</td>';
            $tabla .= '<td>' . number_format($item['ppto_vigente'], 2, ',', '.') . '</td>';
            $porc = ($sum_ppto_vigente == 0) ? 0 : number_format((($item['ppto_vigente'] / $sum_ppto_vigente) * 100), 2, '.', '');
            $tabla .= '<td>' . $porc . ' %</td>';
            $tabla .= '<td>' . number_format($item['devengado'], 2, ',', '.') . '</td>';
            $tabla .= '<td>' . number_format($item['saldo'], 2, ',', '.') . '</td>';
            $porc_ejec = ($item['ppto_vigente'] == 0) ? 0 : number_format((($item['devengado'] / $item['ppto_vigente']) * 100), 2, '.', '');
            $tabla .= '<td>' . $porc_ejec . ' %</td>';
            $tabla .= '</tr>';
        }
        $tabla .= '<tr style="background: #3A3633;color: #fff">';
        $tabla .= '<td style="color: #fff"> TOTAL </td>';
        $tabla .= '<td style="color: #fff">' . $sum_inversion . '</td>';
        $tabla .= '<td style="color: #fff">' . $sum_preinversion . '</td>';
        $tabla .= '<td style="color: #fff">' . $sum_operacion . '</td>';
        $tabla .= '<td style="color: #fff">' . number_format($sum_total, 2, ',', '.') . '</td>';
        $tabla .= '<td style="color: #fff">' . number_format($sum_inicial, 2, ',', '.') . '</td>';
        $tabla .= '<td style="color: #fff">' . number_format($sum_mod, 2, ',', '.') . '</td>';
        $tabla .= '<td style="color: #fff">' . number_format($sum_ppto_vigente, 2, ',', '.') . '</td>';
        $tabla .= '<td style="color: #fff"> 100 %</td>';
        $tabla .= '<td style="color: #fff">' . number_format($sum_devengado, 2, ',', '.') . '</td>';
        $tabla .= '<td style="color: #fff">' . number_format($sum_saldo, 2, ',', '.') . '</td>';
        $porc_ejec = ($sum_ppto_vigente == 0) ? 0 : number_format((($sum_devengado / $sum_ppto_vigente) * 100), 2, '.', '');
        $tabla .= '<td style="color: #fff">' . $porc_ejec . ' %</td>';
        $tabla .= '</tr>';
        $dato['tabla'] = $tabla;
        return $dato;
    }

    function nivel_unidad()
    {
        $data['mes'] = $this->mes_nombre();
        $mes_id = $this->session->userData('mes');
        /*$data_tabla = $this->tabla_nivel_tipo_proyecto($mes_id, $this->gestion);
        $data['tabla'] = $data_tabla['tabla'];
        $data['pie_proy'] = $data_tabla['pie_proy'];
        $data['col_proy_titulo'] = $data_tabla['col_proy_titulo'];
        $data['col_proy_data'] = $data_tabla['col_proy_data'];*/

        $ruta = 'reportes/reportes_gerenciales/reporte_global/vunidad_ejecutora';
        $this->construir_vista($ruta, $data);
    }

    function nivel_unidad1()
    {
        $data['mes'] = $this->mes_nombre();
        $mes_id = $this->session->userData('mes');
        $data_tabla = $this->tabla_nivel_tipo_proyecto($mes_id, $this->gestion);
        $data['tabla'] = $data_tabla['tabla'];
        $data['pie_proy'] = $data_tabla['pie_proy'];
        $data['col_proy_titulo'] = $data_tabla['col_proy_titulo'];
        $data['col_proy_data'] = $data_tabla['col_proy_data'];
        $data_proy_inversion = $this->tabla_proyectos_inversion($mes_id, $this->gestion);//proyectos de inversion
        $data['tabla_proy_inv'] = $data_proy_inversion['tabla_proy_inv'];
        $data['pie_proy_inv'] = $data_proy_inversion['pie_proy_inv'];
        $data['col_titulo_proy_inv'] = $data_proy_inversion['col_titulo_proy_inv'];
        $data['col_data_proy_inv'] = $data_proy_inversion['col_data_proy_inv'];
        $data_prog_no_rec = $this->tabla_prog_no_rec($mes_id, $this->gestion);//programa no recurrente
        $data['tabla_prog_no_rec'] = $data_prog_no_rec['tabla_prog_no_rec'];
        $data['pie_prog_no_rec'] = $data_prog_no_rec['pie_prog_no_rec'];
        $data['col_titulo_prog_no_rec'] = $data_prog_no_rec['col_titulo_prog_no_rec'];
        $data['col_data_prog_no_rec'] = $data_prog_no_rec['col_data_prog_no_rec'];
        $data_pi_pnr = $this->tabla_pi_pnr($mes_id, $this->gestion);//programa de inversion + programa no recurrente
        $data['tabla_pi_pnr'] = $data_pi_pnr['tabla_pi_pnr'];
        $data['pie_pi_pnr'] = $data_pi_pnr['pie_pi_pnr'];
        $data['col_titulo_pi_pnr'] = $data_pi_pnr['col_titulo_pi_pnr'];
        $data['col_data_pi_pnr'] = $data_pi_pnr['col_data_pi_pnr'];
        $ruta = 'reportes/reportes_gerenciales/reporte_global/vunidad_ejecutora';
        $this->construir_vista($ruta, $data);
    }

    //tabla por el tipo de proyecto
    function tabla_nivel_tipo_proyecto($mes_id, $gestion)
    {
        $lproy_inv = $this->total_proy_inversion($mes_id, $gestion);
        $lprog = $this->total_programas($mes_id, $gestion);
        $mat['tabla'] = '';
        $mat['pie_proy'] = '';
        $mat['col_proy_titulo'] = '';
        $mat['col_proy_data'] = '';
        $proy_inv = '<td style="text-align: left"><a href="' . site_url("") . '/admin/rep/reporte_unidad/1" >' . $lproy_inv[0] . '</a></td>';
        $prog = '<td style="text-align: left">' . $lprog[0] . '</td>';
        $total = '<td style="text-align: left">TOTAL</td>';
        for ($i = 1; $i <= 3; $i++) {
            $proy_inv .= '<td>' . $lproy_inv[$i] . '</td>';
            $prog .= '<td>' . $lprog[$i] . '</td>';
            $total .= '<td>' . ($lproy_inv[$i] + $lprog[$i]) . '</td>';
        }
        for ($i = 4; $i <= 10; $i++) {
            if ($i == 7) {
                $sum = $lprog[$i] + $lproy_inv[$i];
                $sum_proy_inv = ($sum == 0) ? 0 : number_format(($lproy_inv[$i] / $sum) * 100, 2, '.', '');
                $sum_prog = ($sum == 0) ? 0 : number_format(($lprog[$i] / $sum) * 100, 2, '.', '');
                $proy_inv .= '<td>' . $sum_proy_inv . '%</td>';
                $prog .= '<td>' . $sum_prog . '%</td>';
                $total .= '<td>' . number_format(($sum_proy_inv + $sum_prog), 2, '.', '') . '%</td>';
                $mat['pie_proy'] .= "['" . $lproy_inv[0] . "'," . $sum_proy_inv . "],['" . $lprog[0] . "'," . $sum_prog . "],";
            } else if ($i == 10) {
                $proy_inv .= '<td>' . number_format($lproy_inv[$i], 2, '.', '') . '%</td>';
                $prog .= '<td>' . number_format($lprog[$i], 2, '.', ' ') . '%</td>';
                $pres_vig = $lproy_inv[6] + $lprog[6];
                $pres_ejec = $lproy_inv[8] + $lprog[8];
                $porc_ejec = ($pres_vig == 0) ? 0 : number_format(($pres_ejec / $pres_vig) * 100, 2, '.', '');
                $total .= '<td>' . $porc_ejec . '%</td>';
                $mat['col_proy_titulo'] .= "'" . $lproy_inv[0] . "','" . $lprog[0] . "'";
                $mat['col_proy_data'] .= number_format($lproy_inv[$i], 2, '.', '') . "," . number_format($lprog[$i], 2, '.', '');
            } else {
                $proy_inv .= '<td>' . number_format($lproy_inv[$i], 2, '.', ' ') . '</td>';
                $prog .= '<td>' . number_format($lprog[$i], 2, '.', ' ') . '</td>';
                $total .= '<td>' . number_format(($lproy_inv[$i] + $lprog[$i]), 2, '.', ' ') . '</td>';
            }

        }
        $mat['tabla'] = '<tr>' . $proy_inv . '</tr>';
        $mat['tabla'] .= '<tr>' . $prog . '</tr>';
        $mat['tabla'] .= '<tr bgcolor="#7CB5EC">' . $total . '</tr>';
        return $mat;
    }

    function total_proy_inversion($mes_id, $gestion)
    {
        $lista = $this->mreporte_global->proyecto_inversion($mes_id, $gestion);
        $mat[0] = 'PROYECTO DE INVERSIÓN';
        for ($i = 1; $i <= 10; $i++) {
            $mat[$i] = 0;
        }
        foreach ($lista as $item) {
            $mat[1] += $item['cant_inv'];
            $mat[2] += $item['cant_pinv'];
            $mat[3] += ($item['cant_inv'] + $item['cant_pinv']);//suma_total
            $mat[4] += $item['ppto_inicial'];//presupuesto inicial
            $mat[5] += $item['modif_aprobadas'];//modificaciones aprobadas
            $mat[6] += $item['ppto_vigente'];//presupuesto vigente
            $mat[7] += $item['ppto_vigente'];//porc
            $mat[8] += $item['devengado'];//devengado
            $mat[9] += ($item['ppto_vigente'] - $item['devengado']);//saldo
        }
        $mat[7] = ($mat[7] * 100);//porc
        $mat[10] = ($mat[6] == 0) ? 0 : ($mat[8] / $mat[6]) * 100;
        return $mat;
    }

    function total_programas($mes_id, $gestion)
    {
        $lista = $this->mreporte_global->programa_no_recurrente($mes_id, $gestion);
        $mat[0] = 'PROGRAMAS';
        for ($i = 1; $i <= 10; $i++) {
            $mat[$i] = 0;
        }
        foreach ($lista as $item) {
            $mat[1] += $item['prog_recurrente'];
            $mat[2] += $item['prog_no_recurrente'];
            $mat[3] += ($item['prog_recurrente'] + $item['prog_no_recurrente']);//suma_total
            $mat[4] += $item['ppto_inicial'];//presupuesto inicial
            $mat[5] += $item['modif_aprobadas'];//modificaciones aprobadas
            $mat[6] += $item['ppto_vigente'];//presupuesto vigente
            $mat[7] += $item['ppto_vigente'];//porc
            $mat[8] += $item['devengado'];//devengado
            $mat[9] += ($item['ppto_vigente'] - $item['devengado']);//saldo
        }
        $mat[7] = ($mat[7] * 100);//porc
        $mat[10] = ($mat[6] == 0) ? 0 : ($mat[8] / $mat[6]) * 100;
        return $mat;
    }

    function tabla_proyectos_inversion($mes_id, $gestion)
    {
        $lista = $this->mreporte_global->proyecto_inversion($mes_id, $gestion);
        $mat['tabla_proy_inv'] = '';
        $mat['pie_proy_inv'] = '';
        $mat['col_titulo_proy_inv'] = '';
        $mat['col_data_proy_inv'] = '';
        $total_ppto_vigente = 0;
        foreach ($lista as $item) {
            $total_ppto_vigente += $item['ppto_vigente'];
        }
        $sum_total = 0;
        $sum_ppto_inicial = 0;
        $sum_modif_apr = 0;
        $sum_pres_vig = 0;
        $sum_por = 0;
        $sum_ppto_ejec = 0;
        $sum_saldo = 0;
        $sum_inv = 0;
        $sum_pinv = 0;
        foreach ($lista as $item) {
            $mat['tabla_proy_inv'] .= '<tr>';
            $mat['tabla_proy_inv'] .= '<td class="descripcion">' . $item['uni_unidad'] . '</td>';
            $mat['tabla_proy_inv'] .= '<td>' . $item['cant_inv'] . '</td>';
            $mat['tabla_proy_inv'] .= '<td>' . $item['cant_pinv'] . '</td>';
            $mat['tabla_proy_inv'] .= '<td>' . ($item['cant_inv'] + $item['cant_pinv']) . '</td>';
            $mat['tabla_proy_inv'] .= '<td>' . number_format($item['ppto_inicial'], 2, ',', '.') . '</td>';
            $mat['tabla_proy_inv'] .= '<td>' . number_format($item['modif_aprobadas'], 2, ',', '.') . '</td>';
            $mat['tabla_proy_inv'] .= '<td>' . number_format($item['ppto_vigente'], 2, ',', '.') . '</td>';
            $porc = ($total_ppto_vigente == 0) ? 0 : ($item['ppto_vigente'] / $total_ppto_vigente) * 100;
            $mat['tabla_proy_inv'] .= '<td>' . number_format($porc, 2, ',', '.') . '%</td>';
            $mat['tabla_proy_inv'] .= '<td>' . number_format($item['devengado'], 2, ',', '.') . '</td>';
            $mat['tabla_proy_inv'] .= '<td>' . ($item['ppto_vigente'] - $item['devengado']) . '</td>';
            $porc_ejec = ($item['ppto_vigente'] == 0) ? 0 : ($item['devengado'] / $item['ppto_vigente']) * 100;
            $mat['tabla_proy_inv'] .= '<td>' . number_format($porc_ejec, 2, ',', '.') . '%</td>';
            $mat['tabla_proy_inv'] .= '</tr>';
            //para los reportes graficos
            $mat['pie_proy_inv'] .= "['" . $item['uni_unidad'] . "'," . number_format($porc, 2, '.', '') . "],";
            $mat['col_titulo_proy_inv'] .= "'" . $item['uni_unidad'] . "',";
            $mat['col_data_proy_inv'] .= number_format($porc_ejec, 2, '.', '') . ",";
            $sum_total += ($item['cant_inv'] + $item['cant_pinv']);
            $sum_ppto_inicial += $item['ppto_inicial'];
            $sum_modif_apr += $item['modif_aprobadas'];
            $sum_pres_vig += $item['ppto_vigente'];
            $sum_por += $porc;
            $sum_ppto_ejec += $item['devengado'];
            $sum_saldo += ($item['ppto_vigente'] - $item['devengado']);
            $sum_inv += $item['cant_inv'];
            $sum_pinv += $item['cant_pinv'];
        }
        $mat['tabla_proy_inv'] .= '<tr bgcolor="#90ED7D">';
        $mat['tabla_proy_inv'] .= '<td style="text-align: center">TOTAL</td>';
        $mat['tabla_proy_inv'] .= '<td>' . $sum_inv . '</td>';
        $mat['tabla_proy_inv'] .= '<td>' . $sum_pinv . '</td>';
        $mat['tabla_proy_inv'] .= '<td>' . $sum_total . '</td>';
        $mat['tabla_proy_inv'] .= '<td>' . number_format($sum_ppto_inicial, 2, ',', '.') . '</td>';
        $mat['tabla_proy_inv'] .= '<td>' . number_format($sum_modif_apr, 2, ',', '.') . '</td>';
        $mat['tabla_proy_inv'] .= '<td>' . number_format($sum_pres_vig, 2, ',', '.') . '</td>';
        $mat['tabla_proy_inv'] .= '<td>' . number_format($sum_por, 2, ',', '.') . '%</td>';
        $mat['tabla_proy_inv'] .= '<td>' . number_format($sum_ppto_ejec, 2, ',', '.') . '</td>';
        $mat['tabla_proy_inv'] .= '<td>' . number_format($sum_saldo, 2, ',', '.') . '</td>';
        $sum_porc_ejec = ($sum_pres_vig == 0) ? 0 : ($sum_ppto_ejec / $sum_pres_vig) * 100;
        $mat['tabla_proy_inv'] .= '<td>' . number_format($sum_porc_ejec, 2, ',', '.') . '%</td>';
        $mat['tabla_proy_inv'] .= '</tr>';
        return $mat;
    }

    function tabla_prog_no_rec($mes_id, $gestion)
    {
        $lista = $this->mreporte_global->programa_no_recurrente($mes_id, $gestion);
        $mat['tabla_prog_no_rec'] = '';
        $mat['pie_prog_no_rec'] = '';
        $mat['col_titulo_prog_no_rec'] = '';
        $mat['col_data_prog_no_rec'] = '';
        $total_ppto_vigente = 0;
        foreach ($lista as $item) {
            $total_ppto_vigente += $item['ppto_vigente'];
        }
        $sum_total = 0;
        $sum_ppto_inicial = 0;
        $sum_modif_apr = 0;
        $sum_pres_vig = 0;
        $sum_por = 0;
        $sum_ppto_ejec = 0;
        $sum_saldo = 0;
        $sum_inv = 0;
        $sum_pinv = 0;
        foreach ($lista as $item) {
            $mat['tabla_prog_no_rec'] .= '<tr>';
            $mat['tabla_prog_no_rec'] .= '<td class="descripcion">' . $item['uni_unidad'] . '</td>';
            $mat['tabla_prog_no_rec'] .= '<td>' . $item['prog_recurrente'] . '</td>';
            $mat['tabla_prog_no_rec'] .= '<td>' . $item['prog_no_recurrente'] . '</td>';
            $mat['tabla_prog_no_rec'] .= '<td>' . ($item['prog_recurrente'] + $item['prog_no_recurrente']) . '</td>';
            $mat['tabla_prog_no_rec'] .= '<td>' . number_format($item['ppto_inicial'], 2, ',', '.') . '</td>';
            $mat['tabla_prog_no_rec'] .= '<td>' . number_format($item['modif_aprobadas'], 2, ',', '.') . '</td>';
            $mat['tabla_prog_no_rec'] .= '<td>' . number_format($item['ppto_vigente'], 2, ',', '.') . '</td>';
            $porc = ($total_ppto_vigente == 0) ? 0 : ($item['ppto_vigente'] / $total_ppto_vigente) * 100;
            $mat['tabla_prog_no_rec'] .= '<td>' . number_format($porc, 2, ',', '.') . '%</td>';
            $mat['tabla_prog_no_rec'] .= '<td>' . number_format($item['devengado'], 2, ',', '.') . '</td>';
            $mat['tabla_prog_no_rec'] .= '<td>' . ($item['ppto_vigente'] - $item['devengado']) . '</td>';
            $porc_ejec = ($item['ppto_vigente'] == 0) ? 0 : ($item['devengado'] / $item['ppto_vigente']) * 100;
            $mat['tabla_prog_no_rec'] .= '<td>' . number_format($porc_ejec, 2, ',', '.') . '%</td>';
            $mat['tabla_prog_no_rec'] .= '</tr>';
            //para los reportes graficos
            $mat['pie_prog_no_rec'] .= "['" . $item['uni_unidad'] . "'," . number_format($porc, 2, '.', '') . "],";
            $mat['col_titulo_prog_no_rec'] .= "'" . $item['uni_unidad'] . "',";
            $mat['col_data_prog_no_rec'] .= number_format($porc_ejec, 2, '.', '') . ",";
            $sum_total += ($item['prog_recurrente'] + $item['prog_no_recurrente']);
            $sum_ppto_inicial += $item['ppto_inicial'];
            $sum_modif_apr += $item['modif_aprobadas'];
            $sum_pres_vig += $item['ppto_vigente'];
            $sum_por += $porc;
            $sum_ppto_ejec += $item['devengado'];
            $sum_saldo += ($item['ppto_vigente'] - $item['devengado']);
            $sum_inv += $item['prog_recurrente'];
            $sum_pinv += $item['prog_no_recurrente'];
        }
        $mat['tabla_prog_no_rec'] .= '<tr bgcolor="#A8829F">';
        $mat['tabla_prog_no_rec'] .= '<td style="text-align: center">TOTAL</td>';
        $mat['tabla_prog_no_rec'] .= '<td>' . $sum_inv . '</td>';
        $mat['tabla_prog_no_rec'] .= '<td>' . $sum_pinv . '</td>';
        $mat['tabla_prog_no_rec'] .= '<td>' . $sum_total . '</td>';
        $mat['tabla_prog_no_rec'] .= '<td>' . number_format($sum_ppto_inicial, 2, ',', '.') . '</td>';
        $mat['tabla_prog_no_rec'] .= '<td>' . number_format($sum_modif_apr, 2, ',', '.') . '</td>';
        $mat['tabla_prog_no_rec'] .= '<td>' . number_format($sum_pres_vig, 2, ',', '.') . '</td>';
        $mat['tabla_prog_no_rec'] .= '<td>' . number_format($sum_por, 2, ',', '.') . '%</td>';
        $mat['tabla_prog_no_rec'] .= '<td>' . number_format($sum_ppto_ejec, 2, ',', '.') . '</td>';
        $mat['tabla_prog_no_rec'] .= '<td>' . number_format($sum_saldo, 2, ',', '.') . '</td>';
        $sum_porc_ejec = ($sum_pres_vig == 0) ? 0 : ($sum_ppto_ejec / $sum_pres_vig) * 100;
        $mat['tabla_prog_no_rec'] .= '<td>' . number_format($sum_porc_ejec, 2, ',', '.') . '%</td>';
        $mat['tabla_prog_no_rec'] .= '</tr>';
        return $mat;
    }

    //tabla proyectos de inversion y programas no recurrentes
    function tabla_pi_pnr($mes_id, $gestion)
    {
        $lista = $this->mreporte_global->proy_inv_prog_no_rec($mes_id, $gestion);
        $mat['tabla_pi_pnr'] = '';
        $mat['pie_pi_pnr'] = '';
        $mat['col_titulo_pi_pnr'] = '';
        $mat['col_data_pi_pnr'] = '';
        $total_ppto_vigente = 0;
        foreach ($lista as $item) {
            $total_ppto_vigente += $item['ppto_vigente'];
        }
        $sum_total = 0;
        $sum_ppto_inicial = 0;
        $sum_modif_apr = 0;
        $sum_pres_vig = 0;
        $sum_por = 0;
        $sum_ppto_ejec = 0;
        $sum_saldo = 0;
        $sum_pi = 0; //proyecto de inversion
        $sum_pnr = 0; //programa no recurrente
        foreach ($lista as $item) {
            $mat['tabla_pi_pnr'] .= '<tr>';
            $mat['tabla_pi_pnr'] .= '<td class="descripcion"><a href="' . site_url("") . '/admin/rep/iframe_rep_unidad/' . $item['uni_id'] . '" class="enviar_poa" name="enviar_poa" id="enviar_poa">' . $item['uni_unidad'] . '</a></td>';
            $mat['tabla_pi_pnr'] .= '<td>' . $item['inversion'] . '</td>';
            $mat['tabla_pi_pnr'] .= '<td>' . $item['prog_no_rec'] . '</td>';
            $mat['tabla_pi_pnr'] .= '<td>' . ($item['inversion'] + $item['prog_no_rec']) . '</td>';
            $mat['tabla_pi_pnr'] .= '<td>' . number_format($item['ppto_inicial'], 2, ',', '.') . '</td>';
            $mat['tabla_pi_pnr'] .= '<td>' . number_format($item['modif_aprobadas'], 2, ',', '.') . '</td>';
            $mat['tabla_pi_pnr'] .= '<td>' . number_format($item['ppto_vigente'], 2, ',', '.') . '</td>';
            $porc = ($total_ppto_vigente == 0) ? 0 : ($item['ppto_vigente'] / $total_ppto_vigente) * 100;
            $mat['tabla_pi_pnr'] .= '<td>' . number_format($porc, 2, ',', '.') . '%</td>';
            $mat['tabla_pi_pnr'] .= '<td>' . number_format($item['devengado'], 2, ',', '.') . '</td>';
            $mat['tabla_pi_pnr'] .= '<td>' . ($item['ppto_vigente'] - $item['devengado']) . '</td>';
            $porc_ejec = ($item['ppto_vigente'] == 0) ? 0 : ($item['devengado'] / $item['ppto_vigente']) * 100;
            $mat['tabla_pi_pnr'] .= '<td>' . number_format($porc_ejec, 2, ',', '.') . '%</td>';
            $mat['tabla_pi_pnr'] .= '</tr>';
            //para los reportes graficos
            $mat['pie_pi_pnr'] .= "['" . $item['uni_unidad'] . "'," . number_format($porc, 2, '.', '') . "],";
            $mat['col_titulo_pi_pnr'] .= "'" . $item['uni_unidad'] . "',";
            $mat['col_data_pi_pnr'] .= number_format($porc_ejec, 2, '.', '') . ",";
            $sum_total += ($item['inversion'] + $item['prog_no_rec']);
            $sum_ppto_inicial += $item['ppto_inicial'];
            $sum_modif_apr += $item['modif_aprobadas'];
            $sum_pres_vig += $item['ppto_vigente'];
            $sum_por += $porc;
            $sum_ppto_ejec += $item['devengado'];
            $sum_saldo += ($item['ppto_vigente'] - $item['devengado']);
            $sum_pi += $item['inversion'];
            $sum_pnr += $item['prog_no_rec'];
        }
        $mat['tabla_pi_pnr'] .= '<tr bgcolor="#EBBF40">';
        $mat['tabla_pi_pnr'] .= '<td style="text-align: center">TOTAL</td>';
        $mat['tabla_pi_pnr'] .= '<td>' . $sum_pi . '</td>';
        $mat['tabla_pi_pnr'] .= '<td>' . $sum_pnr . '</td>';
        $mat['tabla_pi_pnr'] .= '<td>' . $sum_total . '</td>';
        $mat['tabla_pi_pnr'] .= '<td>' . number_format($sum_ppto_inicial, 2, ',', '.') . '</td>';
        $mat['tabla_pi_pnr'] .= '<td>' . number_format($sum_modif_apr, 2, ',', '.') . '</td>';
        $mat['tabla_pi_pnr'] .= '<td>' . number_format($sum_pres_vig, 2, ',', '.') . '</td>';
        $mat['tabla_pi_pnr'] .= '<td>' . number_format($sum_por, 2, ',', '.') . '%</td>';
        $mat['tabla_pi_pnr'] .= '<td>' . number_format($sum_ppto_ejec, 2, ',', '.') . '</td>';
        $mat['tabla_pi_pnr'] .= '<td>' . number_format($sum_saldo, 2, ',', '.') . '</td>';
        $sum_porc_ejec = ($sum_pres_vig == 0) ? 0 : ($sum_ppto_ejec / $sum_pres_vig) * 100;
        $mat['tabla_pi_pnr'] .= '<td>' . number_format($sum_porc_ejec, 2, ',', '.') . '%</td>';
        $mat['tabla_pi_pnr'] .= '</tr>';
        return $mat;
    }

    function mes_nombre()
    {
        $mes[1] = 'ENERO';
        $mes[2] = 'FEBRERO';
        $mes[3] = 'MARZO';
        $mes[4] = 'ABRIL';
        $mes[5] = 'MAYO';
        $mes[6] = 'JUNIO';
        $mes[7] = 'JULIO';
        $mes[8] = 'AGOSTO';
        $mes[9] = 'SEPTIEMBRE';
        $mes[10] = 'OCTUBRE';
        $mes[11] = 'NOVIEMBRE';
        $mes[12] = 'DICIEMBRE';
        return $mes;
    }

    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'REPORTES';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }
}