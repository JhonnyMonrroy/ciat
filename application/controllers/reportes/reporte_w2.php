<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Reporte_w2 extends CI_Controller {
  public $rol = array('1' => '6');  
  public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf');
        $this->load->library('pdf2');
        $this->load->model('Users_model','',true);
          if($this->rolfun($this->rol)){
            $this->load->model('reportes/model_objetivo');
            $this->load->model('programacion/model_proyecto');
            $this->load->model('programacion/model_producto');
            $this->load->model('programacion/model_faseetapa');
            $this->load->model('programacion/model_componente');
            $this->load->model('programacion/model_reporte_global');
            $this->load->model('programacion/model_actividad');
            $this->load->model('ejecucion/model_ejecucion');
            $this->load->model('mantenimiento/mpoa');
            $this->load->model('programacion/model_reporte');
            $this->load->model('programacion/mreporte_proy');
            $this->load->model('menu_modelo');
            $this->load->model('programacion/insumos/minsumos');
            $this->load->model('programacion/insumos/minsumos_delegado');
          }
          else{
            redirect('admin/dashboard');
          }
        }else{
            redirect('/','refresh');
        }
    }
    /*===================================== PROYECTOS APROBADOS ===========================================*/
    public function list_proyectos_aprobados()
    {
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['lista_aper_padres'] = $this->model_proyecto->list_prog();//lista de aperturas padres
        
        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $data['id_mes'] = $this->session->userdata("mes");
        $data['mes'] = $mes;
        //load the view

        $this->load->view('admin/reportes/reporte_excel/list_proy_ok', $data);
    }
 /*===================================== ALERTA TEMPRANA =============================================*/
 public function alerta_temprana_acciones()
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;

    $this->load->view('admin/reportes/menu_acciones', $data);
  }
 /*==============================================================================================================*/

 /*=========================================== ACCIONES =======================================================*/
 public function acciones()
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $data['lista_aper_padres'] = $this->mpoa->lista_poa();//lista de aperturas padres  

    $mes=$this->get_mes($this->session->userdata("mes"));
    $data['mes'] = $mes[1];
    $data['dias'] = $mes[2];

    $this->load->view('admin/reportes/acciones/avance_acciones', $data);
  }
 /*==============================================================================================================*/
/*=========================================== ACCIONES POR PROGRAMAS  =======================================================*/
 public function programas_acciones()
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $data['lista_aper_padres'] = $this->mpoa->lista_poa();//lista de aperturas padres  

    $mes=$this->get_mes($this->session->userdata("mes"));
    $data['mes'] = $mes[1];
    $data['dias'] = $mes[2];

    $this->load->view('admin/reportes/acciones/acciones_programas', $data);
  }
 /*==============================================================================================================*/
  /*=========================================== PRODUCTOS POR PROYECTOS  =======================================================*/
 public function proyectos_acciones($prog)
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $data['proyectos'] = $this->model_proyecto->programas_proyecto($prog,$this->session->userdata('gestion'));//lista de proyectos del programa
    $data['programa']=$prog.'00000000';
    
    $mes=$this->get_mes($this->session->userdata("mes"));
    $data['mes'] = $mes[1];
    $data['dias'] = $mes[2];

    $this->load->view('admin/reportes/acciones/acciones_proyectos', $data);
  }
 /*==============================================================================================================*/

  /*=========================================== PRODUCTOS A NIVEL INSTITUCIONAL =======================================================*/
 public function productos()
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $mes=$this->get_mes($this->session->userdata("mes"));
    $data['mes'] = $mes[1];
    $data['dias'] = $mes[2];
    $data['lista_aper_padres'] = $this->mpoa->lista_poa();//lista de aperturas padres 
    $this->load->view('admin/reportes/producto/avance_productos', $data);
  }
 /*==============================================================================================================*/
/*=========================================== PRODUCTOS POR PROGRAMAS  =======================================================*/
 public function programas_productos()
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $mes=$this->get_mes($this->session->userdata("mes"));
    $data['mes'] = $mes[1];
    $data['dias'] = $mes[2];
    $data['lista_aper_padres'] = $this->mpoa->lista_poa();//lista de aperturas padres  

    $this->load->view('admin/reportes/producto/productos_programas', $data);
  }
 /*==============================================================================================================*/
 /*=========================================== PRODUCTOS POR PROYECTOS  =======================================================*/
 public function proyectos_productos($prog)
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    
    $mes=$this->get_mes($this->session->userdata("mes"));
    $data['mes'] = $mes[1];
    $data['dias'] = $mes[2];

    $data['proyectos'] = $this->model_proyecto->programas_proyecto($prog,$this->session->userdata('gestion'));//lista de proyectos del programa
    $data['programa']=$prog.'00000000';
    $this->load->view('admin/reportes/producto/productos_proyectos', $data);
  }
 /*==============================================================================================================*/

/*------------------------------- SELECCION DE PROYECTOS FISICA-----------------------------------*/
    public function seleccion_reporte_resumen_fis()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $this->load->view('admin/reportes/gerencial/fisica/select_operaciones', $data);
    }
    /*------------------------------- SELECCION DE PROYECTOS FINANCIERA-----------------------------------*/
    public function seleccion_reporte_resumen_fin()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $this->load->view('admin/reportes/gerencial/financiera/select_operaciones', $data);
    }
    /*========================================= VALIDAR SELECCION DE ACCIONES================================================================================*/
    public function validar_seleccion_resumen_fis_fin()
    { 
        if($this->input->server('REQUEST_METHOD') === 'POST') 
        {
            if($this->input->post('p1')=='on'){$p1=1;}else{$p1=0;}
            if($this->input->post('p2')=='on'){$p2=1;}else{$p2=0;}
            if($this->input->post('p3')=='on'){$p3=1;}else{$p3=0;}
            if($this->input->post('p4')=='on'){$p4=1;}else{$p4=0;}

            redirect('admin/rep/gerencial_proyectos/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'/'.$this->input->post('tp').'');
            
        }
    }

 public function reporte_gerencial_proyectos($p1,$p2,$p3,$p4,$tp)
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $data['lista_aper_padres'] = $this->mpoa->lista_poa();//lista de aperturas padres  

    $meses=$this->get_mes($this->session->userdata("mes"));
    $mes = $meses[1];
    $dias = $meses[2];
    $id_mes=$this->session->userdata("mes");
    $data['id_mes'] = $id_mes;
    $data['mes'] = $mes;
    $data['dias'] = $dias;

    $data['p1'] = $p1; //// proyectos de inversion
    $data['p2'] = $p2; //// programas recurrentes
    $data['p3'] = $p3; //// programas no recurrentes
    $data['p4'] = $p4; //// accion de funcionamiento

    $pr1='';$pr2='';$pr3='';$pr4='';
    if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
    if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
    if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
    if($p4==1){$pr4=' - OPERACICI&Oacute;N DE FUNCIONAMIENTO';}
    
    $data['pr1']=$pr1;
    $data['pr2']=$pr2;
    $data['pr3']=$pr3;
    $data['pr4']=$pr4;

    $data['tp']=$tp;

    if($tp==1){
      $data['avance_fisico']=$this->avance_fisico_gerencial($id_mes,$p1,$p2,$p3,$p4); ///// AVANCE FISICO
      $this->load->view('admin/reportes/gerencial/fisica/gerencial_proyecto', $data);
    }
    elseif ($tp==2) {
      $data['avance_financiero']=$this->avance_financiero_gerencial($id_mes,$p1,$p2,$p3,$p4); ///// AVANCE FINANCIERO
      $this->load->view('admin/reportes/gerencial/financiera/gerencial_proyecto', $data);
    }
    else{
      redirect('admin/dashboard');
    }
    
  }

  /*=============================================== AVANCE A NIVEL INSTITUCIONAL ===============================================*/
  /*----------------------------------------------- AVANCE FISICO -----------------------------------------*/
  public function avance_fisico_gerencial($id_mes,$p1,$p2,$p3,$p4)
  {  
      $ms[0]='programado_total';$ms[1]='enero';$ms[2]='febrero';$ms[3]='marzo';$ms[4]='abril';$ms[5]='mayo';$ms[6]='junio';
      $ms[7]='julio';$ms[8]='agosto';$ms[9]='septiembre';$ms[10]='octubre';$ms[11]='noviembre';$ms[12]='diciembre';

      $unidades = $this->model_reporte->unidades_ejecu();//lista de unidades Ejecutoras
      $nro=1;$cont_alto_riesgo=0;$cont_riesgo=0; $cont_retraso=0; $cont_tiempo=0; $cont_sprogramar=0;

        $proyecto=$this->model_reporte_global->proyectos_ue_global_total($this->session->userdata('gestion'),$p1,$p2,$p3,$p4);
        foreach($proyecto  as $proy)
        {
          $fase = $this->model_faseetapa->get_id_fase($proy['proy_id']);
          $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
          $meses=$años*12;
          if($this->model_proyecto->verif_proy($proy['proy_id'])!=0)
          {
            //  echo "PROY_ID ".$proy['proy_id']."---".$fase[0]['pfec_fecha_inicio']."-".$fase[0]['pfec_fecha_fin']."<br>";
                $componentes = $this->model_componente->componentes_id($fase[0]['id']);
                for($p=1;$p<=$meses;$p++){$cp[1][$p]=0; $cp[2][$p]=0; $cp[3][$p]=0;}
                foreach ($componentes as $rowc)
                {
                  $productos = $this->model_producto->list_prod($rowc['com_id']);
                  for($p=1;$p<=$meses;$p++){$pp[1][$p]=0; $pp[2][$p]=0;}
                 // echo "COMPONENTE : ".$rowc['com_componente']."<br>";
                  foreach ($productos as $rowp)
                  {
                      $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                    //  echo "PRODUCTO : ".$rowp['prod_producto']."<br>";
                      for($p=1;$p<=$meses;$p++){$ap[1][$p]=0; $ap[2][$p]=0;}
                      foreach ($actividad as $rowa)
                      {   
                        $variablep=1;$variablee=1; $v=12; $nro=1; $sump=0;$sume=0;
                        for($p=1;$p<=$meses;$p++){$a[1][$p]=0; $a[2][$p]=0;$a[3][$p]=0;$a[4][$p]=0;$a[5][$p]=0;$a[6][$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                        for($i=$fase[0]['pfec_fecha_inicio'];$i<=$fase[0]['pfec_fecha_fin'];$i++)
                        {
                          $aprog = $this->model_actividad->actividad_programado($rowa['act_id'],$i);
                          if(count($aprog)!=0){
                            for ($j=1; $j <=12 ; $j++) { 
                              $a[1][$variablep]=$aprog[0][$ms[$j]];
                              $variablep++;
                            }
                          }
                          else{
                            $variablep=($v*$nro)+1;
                          }

                          $aejec = $this->model_actividad->actividad_ejecutado($rowa['act_id'],$i);
                          if(count($aejec)!=0){
                            for ($j=1; $j <=12 ; $j++) { 
                              $a[4][$variablee]=$aejec[0][$ms[$j]];
                              $variablee++;
                            }
                          }
                          else{
                            $variablee=($v*$nro)+1;
                          }

                          $nro++;
                        }

                        for ($i=1; $i <=$meses ; $i++) { 
                          $sump=$sump+$a[1][$i];
                          $a[2][$i]=$sump+$rowa['act_linea_base'];
                          
                          if($rowa['act_meta']!=0)
                          {
                            $a[3][$i]=round(((($sump+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                          }
                          $ap[1][$i]=$ap[1][$i]+(($a[3][$i]*$rowa['act_ponderacion'])/100);

                          $sume=$sume+$a[4][$i];
                          $a[5][$i]=$sume+$rowa['act_linea_base'];

                          if($rowa['act_meta']!=0)
                          {
                            $a[6][$i]=round(((($sume+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                          }
                          $ap[2][$i]=$ap[2][$i]+(($a[6][$i]*$rowa['act_ponderacion'])/100);
                        }

                      }
     
                        for ($i=1; $i <=$meses ; $i++) {
                          $pp[1][$i]=$pp[1][$i]+(($ap[1][$i]*$rowp['prod_ponderacion'])/100);
                          $pp[2][$i]=$pp[2][$i]+(($ap[2][$i]*$rowp['prod_ponderacion'])/100);
                        } 
                        
                  }
                    for ($i=1; $i <=$meses ; $i++) {
                          $cp[1][$i]=$cp[1][$i]+(($pp[1][$i]*$rowc['com_ponderacion'])/100);
                          $cp[2][$i]=$cp[2][$i]+(($pp[2][$i]*$rowc['com_ponderacion'])/100);
                        }

                }

                for ($i=1; $i <=$meses ; $i++) {
                  if($cp[1][$i]!=0){
                    $cp[3][$i]=round((($cp[2][$i]/$cp[1][$i])*100),1);
                  }
                }

                for ($i=1; $i <=12 ; $i++) { 
                  $tefic[1][$i]=0;$tefic[2][$i]=0;$tefic[3][$i]=0;
                }
                $i=1;$vmeses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;
                for($p=$fase[0]['pfec_fecha_inicio'];$p<=$fase[0]['pfec_fecha_fin'];$p++)
                {
                    $variable=0;
                    for($pos=$i;$pos<=$vmeses;$pos++)
                    {
                      $variable++;
                      $tefic[1][$variable]=$cp[1][$pos];
                      $tefic[2][$variable]=$cp[2][$pos];
                      $tefic[3][$variable]=$cp[3][$pos];
                    }
                    if($p==$this->session->userdata('gestion'))
                    {
                        if($tefic[1][$id_mes]>0 & $tefic[3][$id_mes]==0) /// en riesgo alto 
                            $cont_alto_riesgo++;
                        if($tefic[1][$id_mes]>0 & ($tefic[3][$id_mes]>1 & $tefic[3][$id_mes]<50)) /// en riesgo
                            $cont_riesgo++;  
                        if($tefic[3][$id_mes]>=51&$tefic[3][$id_mes]<=90) /// con retraso
                            $cont_retraso++;
                        if($tefic[3][$id_mes]>90) /// a tiempo
                            $cont_tiempo++;
                        if($tefic[1][$id_mes]==0 & $tefic[3][$id_mes]==0) /// sin programar
                            $cont_sprogramar++;
                    }

                      $i=$vmeses+1;
                      $vmeses=$vmeses+12;
                      $gestion++; $nro++;
                }
            }
        }

        $suma=$cont_alto_riesgo+$cont_riesgo+$cont_retraso+$cont_tiempo+$cont_sprogramar;
        $valor[1]=$cont_alto_riesgo;
        $valor[2]=$cont_riesgo;
        $valor[3]=$cont_retraso;
        $valor[4]=$cont_tiempo;
        $valor[5]=$cont_sprogramar;
        $valor[6]=$suma;
      
      return $valor;
  }
   /*----------------------------------------------- AVANCE FINANCIERO -----------------------------------------*/
  public function avance_financiero_gerencial($id_mes,$p1,$p2,$p3,$p4)
  {  
      $unidades = $this->model_reporte->unidades_ejecu();//lista de unidades Ejecutoras
      $nro=1;$cont_alto_riesgo=0;$cont_riesgo=0; $cont_retraso=0; $cont_tiempo=0; $cont_sprogramar=0;
                                               
        $proyecto=$this->model_reporte_global->proyectos_ue_global_total($this->session->userdata('gestion'),$p1,$p2,$p3,$p4);
        foreach($proyecto  as $row)
        {
          if($this->model_proyecto->verif_proy($row['proy_id'])!=0)
          {
           // echo "PROY ID : ".$row['proy_id']." -- ".$row['proy_nombre']."--- TP : ".$row['tp_id']."--".$row['pfec_ejecucion']."<br>";
            $ppto_fin=$this->tabla_total($row['proy_id'],$this->session->userdata("gestion"),$row['pfec_ejecucion']);
           // echo "P:".$ppto_fin[1][$id_mes]." -PA:".$ppto_fin[2][$id_mes]." E:".$ppto_fin[3][$id_mes]." -EA:".$ppto_fin[4][$id_mes]." -EFI :".$ppto_fin[5][$id_mes]."<br>";

            if($ppto_fin[1][$id_mes]>0 & $ppto_fin[5][$id_mes]==0) /// en riesgo alto
                $cont_alto_riesgo++;
            if(($ppto_fin[1][$id_mes]>0 & $ppto_fin[5][$id_mes]>1) & $ppto_fin[5][$id_mes]<50) /// en riesgo
                $cont_riesgo++;  
            if($ppto_fin[5][$id_mes]>=51 & $ppto_fin[5][$id_mes]<=90) /// con retraso
                $cont_retraso++;
            if($ppto_fin[5][$id_mes]>90) /// a tiempo
                $cont_tiempo++;
            if($ppto_fin[1][$id_mes]==0 & $ppto_fin[5][$id_mes]==0) /// sin programar
                $cont_sprogramar++;
          }
           
        }

          $suma=$cont_alto_riesgo+$cont_riesgo+$cont_retraso+$cont_tiempo+$cont_sprogramar;
          $valor[1]=$cont_alto_riesgo;
          $valor[2]=$cont_riesgo;
          $valor[3]=$cont_retraso;
          $valor[4]=$cont_tiempo;
          $valor[5]=$cont_sprogramar;
          $valor[6]=$suma;
      return $valor;
  }

    function tabla_total($proy_id,$gestion,$ejec)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $ms[0]='total';$ms[1]='enero';$ms[2]='febrero';$ms[3]='marzo';$ms[4]='abril';$ms[5]='mayo';$ms[6]='junio';
        $ms[7]='julio';$ms[8]='agosto';$ms[9]='septiembre';$ms[10]='octubre';$ms[11]='noviembre';$ms[12]='diciembre';
        
        for($i=1;$i<=12;$i++)
        {
          $totalp[1][$i]=0; //// p
          $totalp[2][$i]=0; //// %pa
          $totalp[3][$i]=0; //// e
          $totalp[4][$i]=0; //// %ea
          $totalp[5][$i]=0; //// eficacia
        }

        $programado = $this->minsumos->insumo_gestion_programado($proy_id,$gestion,$ejec);
        $ejecutado = $this->minsumos->insumo_gestion_ejecutado($proy_id,$gestion,$ejec);
        
        if(count($programado)!=0){

            $suma_pfin=0; $suma_efin=0;
            for($i=1;$i<=12;$i++)
            {
                $totalp[1][$i]=$programado[0][$ms[$i]]; /// programado
                $suma_pfin=$suma_pfin+$programado[0][$ms[$i]];

              if($programado[0][$ms[0]]!=0)
              {
                $totalp[2][$i]=round((($suma_pfin/$programado[0][$ms[0]])*100),2); ////// Programado Acumulado %
              }
              
              if(count($ejecutado)!=0){
                  $totalp[3][$i]=$ejecutado[0][$ms[$i]]; /// Ejecutado
                  $suma_efin=$suma_efin+$ejecutado[0][$ms[$i]];
                  
                  if($programado[0][$ms[0]]!=0)
                  {
                    $totalp[4][$i]=round((($suma_efin/$programado[0][$ms[0]])*100),2); ////// Ejecutado Acumulado %
                  }

                  if($totalp[2][$i]!=0)
                  {
                    $totalp[5][$i]=round((($totalp[4][$i]/$totalp[2][$i])*100),2); ////// Eficacia
                  }
              }
            }
        }
      
        return $totalp;
    }

 /*========================================================================================================================================*/
  /*=========================================== REPORTE RIESGOS A NIVEL UNIDADES EJECUTORAS =======================================================*/
  /*------------------------------------------------- FISICA ------------------------------------------------------*/
public function ver_reporte_gerencial_fis($tp,$id_mes,$p1,$p2,$p3,$p4,$tip_rep)
    { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;

    if($tp==1){$tip_rep='OPERACIONES SIN EJECUCI&Oacute;N';$color='#ED0F1A';}
    if($tp==2){$tip_rep='OPERACIONES EN RIESGO';$color='#F8912F';}
    if($tp==3){$tip_rep='OPERACIONES CON RETRASO';$color='#EDE90F';}
    if($tp==4){$tip_rep='OPERACIONES A TIEMPO';$color='#2ABE47';}
    if($tp==5){$tip_rep='OPERACIONES SIN PROGRAMAR';$color='#BFC2C0';}

    $data['p1'] = $p1; //// proyectos de inversion
    $data['p2'] = $p2; //// programas recurrentes
    $data['p3'] = $p3; //// programas no recurrentes
    $data['p4'] = $p4; //// accion de funcionamiento

    $pr1='';$pr2='';$pr3='';$pr4='';
    if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
    if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
    if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
    if($p4==1){$pr4=' - OPERACI&Oacute;N DE FUNCIONAMIENTO';}
    
    $data['pr1']=$pr1;
    $data['pr2']=$pr2;
    $data['pr3']=$pr3;
    $data['pr4']=$pr4;

    $data['tp']=$tp;

    $meses=$this->get_mes($this->session->userdata("mes"));
    $mes = $meses[1];
    $dias = $meses[2];

    $data['id_mes'] = $this->session->userdata("mes");
    $data['mes'] = $mes;
    $data['dias'] = $dias;
    $data['tip_rep'] = $tip_rep;
    $unidades = $this->model_reporte->unidades_ejecu();//lista de unidades Ejecutoras

    $ms[0]='programado_total';$ms[1]='enero';$ms[2]='febrero';$ms[3]='marzo';$ms[4]='abril';$ms[5]='mayo';$ms[6]='junio';
    $ms[7]='julio';$ms[8]='agosto';$ms[9]='septiembre';$ms[10]='octubre';$ms[11]='noviembre';$ms[12]='diciembre';

    $tabla = ''; $nro_ejec=1;
  //  $tabla .= '<table>';
        foreach($unidades  as $rowu)
        {   $sw=1;
            $proyecto=$this->model_reporte_global->proyectos_ue_global($rowu['uni_id'],$this->session->userdata('gestion'),$p1,$p2,$p3,$p4);
            //$proyecto=$this->model_reporte->proyectos_unidad($rowu['uni_id'],$this->session->userdata('gestion'));
            foreach($proyecto  as $proy)
            { 
                $fase = $this->model_faseetapa->get_id_fase($proy['proy_id']);
                $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
                $meses=$años*12;
                if($this->model_proyecto->verif_proy($proy['proy_id'])!=0)
                {
                  //  echo "PROY_ID ".$proy['proy_id']."---".$fase[0]['pfec_fecha_inicio']."-".$fase[0]['pfec_fecha_fin']."<br>";
                      $componentes = $this->model_componente->componentes_id($fase[0]['id']);
                      for($p=1;$p<=$meses;$p++){$cp[1][$p]=0; $cp[2][$p]=0; $cp[3][$p]=0;}
                      foreach ($componentes as $rowc)
                      {
                        $productos = $this->model_producto->list_prod($rowc['com_id']);
                        for($p=1;$p<=$meses;$p++){$pp[1][$p]=0; $pp[2][$p]=0;}
                       // echo "COMPONENTE : ".$rowc['com_componente']."<br>";
                        foreach ($productos as $rowp)
                        {
                            $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                          //  echo "PRODUCTO : ".$rowp['prod_producto']."<br>";
                            for($p=1;$p<=$meses;$p++){$ap[1][$p]=0; $ap[2][$p]=0;}
                            foreach ($actividad as $rowa)
                            {   
                              $variablep=1;$variablee=1; $v=12; $nro=1; $sump=0;$sume=0;
                              for($p=1;$p<=$meses;$p++){$a[1][$p]=0; $a[2][$p]=0;$a[3][$p]=0;$a[4][$p]=0;$a[5][$p]=0;$a[6][$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                              for($i=$fase[0]['pfec_fecha_inicio'];$i<=$fase[0]['pfec_fecha_fin'];$i++)
                              {
                                $aprog = $this->model_actividad->actividad_programado($rowa['act_id'],$i);
                                if(count($aprog)!=0){
                                  for ($j=1; $j <=12 ; $j++) { 
                                    $a[1][$variablep]=$aprog[0][$ms[$j]];
                                    $variablep++;
                                  }
                                }
                                else{
                                  $variablep=($v*$nro)+1;
                                }

                                $aejec = $this->model_actividad->actividad_ejecutado($rowa['act_id'],$i);
                                if(count($aejec)!=0){
                                  for ($j=1; $j <=12 ; $j++) { 
                                    $a[4][$variablee]=$aejec[0][$ms[$j]];
                                    $variablee++;
                                  }
                                }
                                else{
                                  $variablee=($v*$nro)+1;
                                }

                                $nro++;
                              }

                              for ($i=1; $i <=$meses ; $i++) { 
                                $sump=$sump+$a[1][$i];
                                $a[2][$i]=$sump+$rowa['act_linea_base'];
                                
                                if($rowa['act_meta']!=0)
                                {
                                  $a[3][$i]=round(((($sump+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                                }
                                $ap[1][$i]=$ap[1][$i]+(($a[3][$i]*$rowa['act_ponderacion'])/100);

                                $sume=$sume+$a[4][$i];
                                $a[5][$i]=$sume+$rowa['act_linea_base'];

                                if($rowa['act_meta']!=0)
                                {
                                  $a[6][$i]=round(((($sume+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                                }
                                $ap[2][$i]=$ap[2][$i]+(($a[6][$i]*$rowa['act_ponderacion'])/100);
                              }
                            }
                              for ($i=1; $i <=$meses ; $i++) {
                                $pp[1][$i]=$pp[1][$i]+(($ap[1][$i]*$rowp['prod_ponderacion'])/100);
                                $pp[2][$i]=$pp[2][$i]+(($ap[2][$i]*$rowp['prod_ponderacion'])/100);
                              } 
                              
                        }
                          for ($i=1; $i <=$meses ; $i++) {
                              $cp[1][$i]=$cp[1][$i]+(($pp[1][$i]*$rowc['com_ponderacion'])/100);
                              $cp[2][$i]=$cp[2][$i]+(($pp[2][$i]*$rowc['com_ponderacion'])/100);
                          }

                      }

                      for ($i=1; $i <=$meses ; $i++) {
                        if($cp[1][$i]!=0){
                          $cp[3][$i]=round((($cp[2][$i]/$cp[1][$i])*100),1);
                        }
                      }

                      for ($i=1; $i <=12 ; $i++) { 
                        $tefic[1][$i]=0;$tefic[2][$i]=0;$tefic[3][$i]=0;
                      }
                      $i=1;$vmeses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;
                      for($p=$fase[0]['pfec_fecha_inicio'];$p<=$fase[0]['pfec_fecha_fin'];$p++)
                      {
                          $variable=0;
                          for($pos=$i;$pos<=$vmeses;$pos++)
                          {
                            $variable++;
                            $tefic[1][$variable]=$cp[1][$pos];
                            $tefic[2][$variable]=$cp[2][$pos];
                            $tefic[3][$variable]=$cp[3][$pos];
                          }
                          if($p==$this->session->userdata('gestion'))
                          {
                              if($tefic[1][$id_mes]>0 & $tefic[3][$id_mes]==0) /// en riesgo alto 
                                $tipo=1;
                              if($tefic[1][$id_mes]>0 & ($tefic[3][$id_mes]>1 & $tefic[3][$id_mes]<50)) /// en riesgo
                                $tipo=2;
                              if($tefic[3][$id_mes]>=51&$tefic[3][$id_mes]<=90) /// con retraso
                                $tipo=3;
                              if($tefic[3][$id_mes]>90) /// a tiempo
                                $tipo=4;
                              if($tefic[1][$id_mes]==0 & $tefic[3][$id_mes]==0) /// sin programar
                                $tipo=5;

                              if($tipo==$tp)
                              {
                                if($sw==1)
                                {
                                  $tabla .= '<tr bgcolor="#3a3633">
                                              <td><font color="#ffffff"><b>'.$nro_ejec.'</b></font></td>
                                              <td><font color="#ffffff"><b>'.$rowu['uni_unidad'].'</b></font></td>
                                              <td></td>
                                              <td></td>
                                              <td></td>
                                              <td></td>
                                            </tr>
                                            <tr bgcolor="#c7c5c4">
                                              <td></td>
                                              <td>CATEGORIA PROGRAM&Aacute;TICA</td>
                                              <td>PROYECTO-PROGRAMA-OPERACI&Oacute;N</td>
                                              <td>TIPO DE OPERACI&Oacute;N</td>
                                              <td>UNIDAD EJECUTORA</td>
                                              <td></td>
                                            </tr>';
                                    $sw=0;
                                }
                                $tabla .= '<tr>';
                                $tabla .= '<td >
                                            <a href="" class="form_pond" data-toggle="modal" data-target="#' . $proy['proy_id'] . '">
                                                <img src="' . base_url() . 'assets/ifinal/doc.jpg" width="50" height="50" class="img-responsive" title="REPORTES"><br>Mis Reportes
                                            </a>
                                            <div class="modal fade bs-example-modal-lg" tabindex="-1" id="'.$proy['proy_id'].'"  role="dialog" aria-labelledby="myLargeModalLabel">
                                              <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                                                      &times;
                                                    </button>
                                                      <h4 class="modal-title">
                                                        <img src="' . base_url() . 'assets/img/logo.png" width="150" alt="SmartAdmin" class="img-responsive">
                                                              </h4>
                                                        </div>
                                                        <div class="modal-body no-padding">
                                                          <div class="well">
                                                            <table class="table table-hover">
                                                              <thead>
                                                              <tr>
                                                                <th colspan="3"><center>'.$proy['proy_nombre'].'</center></th>
                                                              </tr>
                                                              </thead>
                                                              <tr>
                                                                <td><b>IDENTIFICACI&Oacute;N DE LA ETAPA</b></td>
                                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/identificacion/'.$proy['proy_id'].'\');" title="IDENTIFICACION DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/identificacion/'.$proy['proy_id'].'\');" title="IDENTIFICACION DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                                              </tr>
                                                              <tr>
                                                                <td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA DE LA ETAPA</b></td>
                                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/rep_programacion/1/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FISICA ANUAL DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/rep_programacion/4/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FISICA PLURI-ANUAL DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                                              </tr>
                                                              <tr>
                                                                <td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N FINANCIERA DE LA ETAPA</b></td>
                                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/rep_financiero/1/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FINANCIERA ANUAL DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/rep_financiero/4/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FINANCIERA PLURI-ANUAL DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                                              </tr>
                                                              <tr>
                                                                <td><b>REGISTRO DE CONTRATOS</b></td>
                                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/contratos/'.$proy['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/contratos/'.$proy['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                                              </tr>
                                                              <tr>
                                                                <td><b>INFORMES DE SUPERVISI&Oacute;N Y EVALUACI&Oacute;N</b></td>
                                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/supervision_evaluacion/'.$proy['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/supervision_evaluacion/'.$proy['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                                              </tr>
                                                              <tr>
                                                                <td><b>CURVA "S" AVANCE F&Iacute;SICO </b></td>
                                                                <td><center><a href="javascript:abreVentanaFis(\''.site_url('admin').'/imprimir_curva_fis/1/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="GRAFICO AVANCE FISICO ANUAL"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                                <td><center><a href="javascript:abreVentanaFis(\''.site_url('admin').'/imprimir_curva_fis/4/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="GRAFICO AVANCE FISICO PLURI-ANUAL"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                                              </tr>
                                                              <tr>
                                                                <td><b>CURVA "S" AVANCE FINANCIERO</b></td>
                                                                <td><center><a href="javascript:abreVentanaFin(\''.site_url("admin").'/imprimir_curva_fin/1/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="GRAFICO AVANCE FINANCIERO ANUAL"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                                <td><center><a href="javascript:abreVentanaFin(\''.site_url("admin").'/imprimir_curva_fin/4/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="GRAFICO AVANCE FINANCIERO PLURI-ANUAL"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                                              </tr>
                                                            </table>
                                                          </div>
                                                        </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </td>';
                                $tabla .= '<td>' .$proy['aper_programa'].''.$proy['aper_proyecto'].''.$proy['aper_actividad'] . '</td>';
                                $tabla .= '<td>' .$proy['proy_nombre'] . '</td>';
                                $tabla .= '<td>' .$proy['tp_tipo'] . '</td>';
                                $tabla .= '<td>' .$rowu['uni_unidad'] . '</td>';
                                $tabla .= '<td >';
                                $tabla .= '
                                <table class="table table-bordered" class="col-md-12 col-sm-12 col-xs-12" >
                                    <thead>
                                    <tr>
                                        <th colspan="4" bgcolor="'.$color.'"><font color="#ffffff">'.$tip_rep.'</font></th>
                                    </tr>
                                    <tr>
                                        <th>AL MES</th>
                                        <th>PROGRAMACI&Oacute;N ACUMULADA AL MES DE '.$mes.'</th>
                                        <th>EJECUCI&Oacute;N ACUMULADA AL MES DE '.$mes.'</th>
                                        <th>EFICACIA</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        <td >'.$mes.'</td>
                                        <td>'.round($tefic[1][$id_mes],2).' %</td>
                                        <td>'.round($tefic[2][$id_mes],2).' %</td>
                                        <td><font color="blue"><b>'.$tefic[3][$id_mes].' %</b></font></td>
                                        </tr>
                                    </tbody>
                                </table>';

                                $tabla .= '</td>';
                                $tabla .= '</tr>';
                              }
                          }

                            $i=$vmeses+1;
                            $vmeses=$vmeses+12;
                            $gestion++; $nro++;
                      }

                  }
            }
            
          /*--------------------------- Fuera del Foreach Proyecto -------------------------------------*/
        $nro_ejec++;
        }
      //  $tabla .= '<table>';
      $data['tabla'] = $tabla;
      $this->load->view('admin/reportes/gerencial/fisica/riesgo_programas_fis', $data);
    }


    /*------------------------------------------------- FINANCIERO ------------------------------------------------------*/
    public function ver_reporte_gerencial_fin($tp,$id_mes,$p1,$p2,$p3,$p4,$tp_rep)
    { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;

    if($tp==1){$tip_rep='OPERACIONES SIN EJECUCI&Oacute;N';$color='#ED0F1A';}
    if($tp==2){$tip_rep='OPERACIONES EN RIESGO';$color='#F8912F';}
    if($tp==3){$tip_rep='OPERACIONES CON RETRASO';$color='#EDE90F';}
    if($tp==4){$tip_rep='OPERACIONES A TIEMPO';$color='#2ABE47';}
    if($tp==5){$tip_rep='EJECUCI&Oacute;N PROGRAMADA PARA LOS SIGUIENTES MESES';$color='#BFC2C0';}

    $data['p1'] = $p1; //// proyectos de inversion
    $data['p2'] = $p2; //// programas recurrentes
    $data['p3'] = $p3; //// programas no recurrentes
    $data['p4'] = $p4; //// accion de funcionamiento

    $pr1='';$pr2='';$pr3='';$pr4='';
    if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
    if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
    if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
    if($p4==1){$pr4=' - OPERACI&Oacute;N DE FUNCIONAMIENTO';}
    
    $data['pr1']=$pr1;
    $data['pr2']=$pr2;
    $data['pr3']=$pr3;
    $data['pr4']=$pr4;

    $data['tp_rep']=$tp_rep;

    $meses=$this->get_mes($this->session->userdata("mes"));
    $mes = $meses[1];
    $dias = $meses[2];

    $data['id_mes'] = $this->session->userdata("mes");
    $data['mes'] = $mes;
    $data['tip_rep'] = $tip_rep;
    $unidades = $this->model_reporte->unidades_ejecu();//lista de unidades Ejecutoras

    $tabla = ''; 
    foreach($unidades  as $rowu)
    { 
        $sw=1;
        $proyecto=$this->model_reporte_global->proyectos_ue_global($rowu['uni_id'],$this->session->userdata('gestion'),$p1,$p2,$p3,$p4);
        foreach($proyecto  as $proy)
        { 
          if($this->model_proyecto->verif_proy($proy['proy_id'])!=0)
          {
            $fase = $this->model_faseetapa->get_id_fase($proy['proy_id']);
            $ppto_fin=$this->tabla_total($proy['proy_id'],$this->session->userdata("gestion"),$proy['pfec_ejecucion']);
           // echo "P:".$ppto_fin[1][$id_mes]." -PA:".$ppto_fin[2][$id_mes]." E:".$ppto_fin[3][$id_mes]." -EA:".$ppto_fin[4][$id_mes]." -EFI :".$ppto_fin[5][$id_mes]."<br>";

            if($ppto_fin[1][$id_mes]>0 & $ppto_fin[5][$id_mes]==0) /// en riesgo alto
                $tipo=1;
            if(($ppto_fin[1][$id_mes]>0 & $ppto_fin[5][$id_mes]>1) & $ppto_fin[5][$id_mes]<50) /// en riesgo
                $tipo=2;
            if($ppto_fin[5][$id_mes]>=51 & $ppto_fin[5][$id_mes]<=90) /// con retraso
                $tipo=3;
            if($ppto_fin[5][$id_mes]>90) /// a tiempo
                $tipo=4;
            if($ppto_fin[1][$id_mes]==0 & $ppto_fin[5][$id_mes]==0) /// sin programar
                $tipo=5;
          
            if($tipo==$tp)
            {
              if($sw==1)
              {
                $tabla .= '<tr bgcolor="#3a3633">';
                $tabla .= '<td></td>';
                $tabla .= '<td></td>';
                $tabla .= '<td></td>';
                $tabla .= '<td></td>';
                $tabla .= '<td><font color="#ffffff"><b>'.$rowu['uni_unidad'].'</b></font></td>';
                $tabla .= '<td></td>';
                $tabla .= '</tr>
                    <tr bgcolor="#c7c5c4">
                      <td></td>
                      <td>CATEGORIA PROGRAM&Aacute;TICA</td>
                      <td>PROYECTO-PROGRAMA-ACCI&Oacute;N</td>
                      <td>TIPO DE ACCI&Oacute;N</td>
                      <td>UNIDAD EJECUTORA</td>
                      <td></td>
                    </tr>';
                $sw=0;
              }
              $tabla .= '<tr>';
                $tabla .= '<td >
                            <a href="" class="form_pond" data-toggle="modal" data-target="#' . $proy['proy_id'] . '">
                                <img src="' . base_url() . 'assets/ifinal/doc.jpg" width="50" height="50" class="img-responsive" title="REPORTES"><br>Mis Reportes
                            </a>
                            <div class="modal fade bs-example-modal-lg" tabindex="-1" id="'.$proy['proy_id'].'"  role="dialog" aria-labelledby="myLargeModalLabel">
                              <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                                      &times;
                                    </button>
                                      <h4 class="modal-title">
                                        <img src="' . base_url() . 'assets/img/logo.png" width="150" alt="SmartAdmin" class="img-responsive">
                                              </h4>
                                        </div>
                                        <div class="modal-body no-padding">
                                          <div class="well">
                                            <table class="table table-hover">
                                              <thead>
                                              <tr>
                                                <th colspan="3"><center>'.$proy['proy_nombre'].'</center></th>
                                              </tr>
                                              </thead>
                                              <tr>
                                                <td><b>IDENTIFICACI&Oacute;N DE LA ETAPA</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/identificacion/'.$proy['proy_id'].'\');" title="IDENTIFICACION DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/identificacion/'.$proy['proy_id'].'\');" title="IDENTIFICACION DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA DE LA ETAPA</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/rep_programacion/1/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FISICA ANUAL DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/rep_programacion/4/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FISICA PLURI-ANUAL DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N FINANCIERA DE LA ETAPA</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/rep_financiero/1/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FINANCIERA ANUAL DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/rep_financiero/4/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FINANCIERA PLURI-ANUAL DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>REGISTRO DE CONTRATOS</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/contratos/'.$proy['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/contratos/'.$proy['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>INFORMES DE SUPERVISI&Oacute;N Y EVALUACI&Oacute;N</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/supervision_evaluacion/'.$proy['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/supervision_evaluacion/'.$proy['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>CURVA "S" AVANCE F&Iacute;SICO </b></td>
                                                <td><center><a href="javascript:abreVentanaFis(\''.site_url('admin').'/imprimir_curva_fis/1/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="GRAFICO AVANCE FISICO ANUAL"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentanaFis(\''.site_url('admin').'/imprimir_curva_fis/4/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="GRAFICO AVANCE FISICO PLURI-ANUAL"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>CURVA "S" AVANCE FINANCIERO</b></td>
                                                <td><center><a href="javascript:abreVentanaFin(\''.site_url("admin").'/imprimir_curva_fin/1/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="GRAFICO AVANCE FINANCIERO ANUAL"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentanaFin(\''.site_url("admin").'/imprimir_curva_fin/4/'.$fase[0]['id'].'/'.$proy['proy_id'].'\');" title="GRAFICO AVANCE FINANCIERO PLURI-ANUAL"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                            </table>
                                          </div>
                                        </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </td>';
                $tabla .= '<td>' .$proy['aper_programa'].''.$proy['aper_proyecto'].''.$proy['aper_actividad'] . '</td>';
                $tabla .= '<td>' .$proy['proy_nombre'] . '</td>';
                $tabla .= '<td>' .$proy['tp_tipo'] . '</td>';
                $tabla .= '<td>' .$rowu['uni_unidad'] . '</td>';
                $tabla .= '<td >';
                $tabla .= '
                <table class="table table-bordered" class="col-md-12 col-sm-12 col-xs-12" >
                    <thead>
                    <tr>
                        <th colspan="4" bgcolor="'.$color.'"><font color="#ffffff">'.$tip_rep.'</font></th>
                    </tr>
                    <tr>
                        <th>AL MES</th>
                        <th>PROGRAMACI&Oacute;N ACUMULADA AL MES DE '.$mes.'</th>
                        <th>EJECUCI&Oacute;N ACUMULADA AL MES DE '.$mes.'</th>
                        <th>EFICACIA</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td >'.$mes.'</td>
                        <td>'.round($ppto_fin[2][$id_mes],2).' %</td>
                        <td>'.round($ppto_fin[4][$id_mes],2).' %</td>
                        <td><font color="blue"><b>'.$ppto_fin[5][$id_mes].' %</b></font></td>
                        </tr>
                    </tbody>
                </table>';

                $tabla .= '</td>';
                $tabla .= '</tr>';
            }
          }

        }
    }

    $data['tfin'] = $tabla;

    $this->load->view('admin/reportes/gerencial/financiera/riesgo_programas_fin', $data);
    }


  /*=========================================== REPORTE EXCEL ACCIONES =======================================================*/
  public function reporte_excel_acciones()
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;

    $meses=$this->get_mes($this->session->userdata("mes"));
    $mes = $meses[1];
    $dias = $meses[2];
    $lista_aper_padres = $this->mpoa->lista_poa();//lista de aperturas padres 

    $tabla = '';

    foreach($lista_aper_padres  as $aper)
    {   
        $tabla .= '<tr bgcolor="#E4E8E8">';
        $tabla .= '<td>' .$aper['poa_codigo']. '</td>';
        $tabla .= '<td><center><b>' .$aper['aper_programa'].''.$aper['aper_proyecto'].''.$aper['aper_actividad']. '</b></center></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '<td></td>';
        $tabla .= '</tr>';
        $programas=$this->model_reporte->list_programa_proyecto($aper['aper_programa'],$this->session->userdata('gestion'));
        foreach($programas  as $row)
        {
          $fase= $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
          $tabla .= '<tr>';
          $tabla .= '<td><center><a href="' . site_url("") . '/admin/rep/get_accion/'.$row['proy_id'].'" title="REPORTE DE EJECUCION DEL PROYECTO" target="black"><img src="'.base_url().'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"></a></td>';
          $tabla .= '<td><center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center></td>';
          $tabla .= '<td>'.$row['proy_nombre'].'</td>';
          $tabla .= '<td>'.$row['proy_sisin'].'</td>';
          $tabla .= '<td>'.strtoupper($row['tp_tipo']).'</td>';
          $tabla .= '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
          $tabla .= '<td>'.$row['uni_unidad'].'</td>';
          $tabla .= '<td>'.$fase[0]['fase'].'<br>'.$fase[0]['etapa'].'</td>';
          $tabla .= '<td>'.$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']).'</td>';
          $tabla .= '<td>'.$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']).'</td>';
          $tabla .= '<td>'.date('Y-m-d',strtotime($fase[0]['inicio'])).'</td>';
          $tabla .= '<td>'.date('Y-m-d',strtotime($fase[0]['final'])).'</td>';

          $ptto_gestion=$this->ptto_mod_aprobadas_presupuesto_vigente_proy($row['proy_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"));
          $tabla .= '<td bgcolor="#c9f3f3">'.number_format($ptto_gestion[1], 2, ',', '.').'</td>'; /// PPTO INICAL SIGEP
          $tabla .= '<td bgcolor="#c9f3f3">'.number_format($ptto_gestion[2], 2, ',', '.').'</td>'; /// MODIFICACIONES SIGEP
          $tabla .= '<td bgcolor="#c9f3f3">'.number_format($ptto_gestion[3], 2, ',', '.').'</td>'; /// VIGENTE SIGEP
          
          $ptto_gestion2=$this->avance_financiero($row['proy_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"));
          $tabla .= '<td bgcolor="#c9f3f3">'.number_format($ptto_gestion2[2], 2, ',', '.').'</td>'; //// PPTO PROGRAMADO MES ACTUAL
          $tabla .= '<td bgcolor="#c9f3f3">'.number_format($ptto_gestion2[5], 2, ',', '.').'</td>'; //// PPTO EJECUTADO MES ACTUAL
          $tabla .= '<td bgcolor="#c9f3f3">'.round($ptto_gestion2[7],2).' %</td>'; ///// % EJECUCION / MES ACTIVA
          $tabla .= '<td bgcolor="#c9f3f3">'.round($ptto_gestion2[8],2).' %</td>'; ///// % EJECUCION / GESTION ACTIVA

          $ctotal=$this->costo_total_accion($row['proy_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"));
          $tabla .= '<td bgcolor="#c0f1d5">'.number_format($ctotal[1], 2, ',', '.').'</td>'; //// COSTO INICIAL AUDI
          $tabla .= '<td bgcolor="#c0f1d5">'.number_format($ctotal[2], 2, ',', '.').'</td>'; //// MODIFICACIONES AUDI
          $tabla .= '<td bgcolor="#c0f1d5">'.number_format($ctotal[3], 2, ',', '.').'</td>'; //// COSTO VIGENTE AUDI
          $tabla .= '<td bgcolor="#c0f1d5">'.number_format($ctotal[4], 2, ',', '.').'</td>'; //// EJECUTADO ACUMULADO 
          $tabla .= '<td bgcolor="#c0f1d5">'.round($ctotal[5],2).' %</td>'; ///// % EJECUTADO ACUMULADO 
          
          $avance_fis_ac=$this->avance_fisico_gestiones($row['proy_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"));
          $tabla .= '<td>'.$avance_fis_ac[1].' %</td>'; //// AVANCE FISICO ACUMULADO %

          $inf_fis_anual=$this->informacion_fisica_anual($row['proy_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"));
          $tabla .= '<td bgcolor="#f5f5bb">'.round($inf_fis_anual[1],2).' %</td>'; /// PROGRAMADO GESTION 
          $tabla .= '<td bgcolor="#f5f5bb">'.round($inf_fis_anual[2],2).' %</td>'; /// PROGRAMADO AL MES ACTIVO
          $tabla .= '<td bgcolor="#f5f5bb">'.round($inf_fis_anual[3],2).' %</td>'; /// % EJECUCION AL MES ACTIVO
          $tabla .= '<td bgcolor="#f5f5bb">'.round($inf_fis_anual[4],2).' %</td>'; /// % EJECUCION/GESTION
          $tabla .= '<td bgcolor="#f5f5bb">'.round($inf_fis_anual[5],2).' %</td>'; /// % EJECUCION/MES 
          
          $ctta=$this->contrato_proyecto_fase($fase[0]['id']);
          $tabla .= '<td bgcolor="#F2EBA0">'.$ctta[1].'</td>';
          $tabla .= '<td bgcolor="#F2EBA0">'.$ctta[2].'</td>';
          $tabla .= '<td bgcolor="#F2EBA0">'.$ctta[3].'</td>';

          $estado=$this->estado_proyecto($fase[0]['id']);
          $tabla .= '<td>'.$estado[1].'</td>';
          $tabla .= '<td>'.$estado[2].'</td>';

          $pdes=$this->v_pedes($row['proy_id']);
          $tabla .= '<td>'.$pdes[1].'</td>';
          $tabla .= '<td>'.$pdes[2].'</td>';
          $tabla .= '<td>'.$pdes[3].'</td>';
          $tabla .= '<td>'.$pdes[4].'</td>';

          $ptdi=$this->v_ptdi($row['proy_id']);
          $tabla .= '<td>'.$ptdi[1].'</td>';
          $tabla .= '<td>'.$ptdi[2].'</td>';
          $tabla .= '<td>'.$ptdi[3].'</td>';
          $tabla .= '</tr>';
        }
      }
    $data['mes'] = $mes;
    $data['acciones'] = $tabla;
    $this->load->view('admin/reportes/reporte_excel/exportar_excel', $data);
  }

  /*====================================== CONTRATO DEL PROYECTO ===========================================*/
  public function contrato_proyecto_fase($pfec_id)
  {
    $ctta = $this->model_reporte->contratos_fase($pfec_id);
    for($i=1;$i<=3;$i++){$valor[$i]='null';} 

    if(count($ctta)!=0)
    {
      $valor[1]=$ctta[0]['ctta_nombre'];
      $valor[2]=$ctta[0]['ctto_plazo_dd'];
      $valor[3]=$ctta[0]['ctto_bolivianos'];
    }

    return $valor;
  }

  /*================================ COSTO TOTAL ACCION (AUDI MODIFICACIONES) =============================*/
  public function costo_total_accion($proy_id,$gestion,$mes)
  {
    $fase = $this->model_faseetapa->get_id_fase($proy_id);
    $ttotal = $this->model_reporte->costos_total_etapa($fase[0]['id']);  //// modificaciones auidtoria
    for($i=1;$i<=5;$i++){$valor[$i]=0;} 

    $pf=$this->ejecutado_pluri($proy_id,$mes);

    if(count($ttotal)!=0)
    {
      $valor[1]=$ttotal[0]['inicial'];
      $valor[2]=$ttotal[0]['diferencia'];
      $valor[3]=$ttotal[0]['vigente'];
    }
    else
    {
      $valor[1]=$fase[0]['pfec_ptto_fase'];
      $valor[3]=$fase[0]['pfec_ptto_fase'];
    }
    $valor[4]=$pf[2];
    if($valor[3]!=0){$valor[5]=(($valor[4]/$valor[3])*100);}

    return $valor;
  }
  /*=============================================== PEDES =======================================================*/
  public function v_pedes($proy_id)
  {
    $proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
    for($i=1;$i<=4;$i++){$valor[$i]='No Seleccionado';} 

    if($proyecto[0]['pdes_id']!=0)
    {
      $pdes=$this->model_proyecto->datos_pedes($proyecto[0]['pdes_id']);
      $valor[1]=$pdes[0]['id1'].' '.$pdes[0]['pilar'];
      $valor[2]=$pdes[0]['id2'].' '.$pdes[0]['meta'];
      $valor[3]=$pdes[0]['id3'].' '.$pdes[0]['resultado'];
      $valor[4]=$pdes[0]['id4'].' '.$pdes[0]['accion'];
    }
    return $valor;
  }
  /*==============================================================================================================*/
  /*=============================================== PTDI =======================================================*/
  public function v_ptdi($proy_id)
  {
    $proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
    for($i=1;$i<=3;$i++){$valor[$i]='No Seleccionado';} 

    if($proyecto[0]['ptdi_id']!=0)
    {
      $ptdi = $this->model_proyecto->datos_ptdi($proyecto[0]['ptdi_id']);
      if(count($ptdi)!=0)
      {
              $valor[1]=$ptdi[0]['id1'].' '.$ptdi[0]['pilar'];
      $valor[2]=$ptdi[0]['id2'].' '.$ptdi[0]['meta'];
      $valor[3]=$ptdi[0]['id3'].' '.$ptdi[0]['resultado'];
      }

    }
    return $valor;  
    
  }
  /*==============================================================================================================*/
  /*==========================CALCULO EJECUTADO FINANCIERO E,EA,%EA PLURI ANUAL ====================================*/
    public function ejecutado_pluri($proy_id,$id_mes)
    {
       $fase= $this->model_faseetapa->get_id_fase($proy_id);
       $valor[1]=0;$valor[2]=0;$valor[3]=0;

       $me[1]='ejec1';
       $me[2]='ejec2';
       $me[3]='ejec3';
       $me[4]='ejec4';
       $me[5]='ejec5';
       $me[6]='ejec6';
       $me[7]='ejec7';
       $me[8]='ejec8';
       $me[9]='ejec9';
       $me[10]='ejec10';
       $me[11]='ejec11';
       $me[12]='ejec12';

        $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $g_inicio=$fase[0]['pfec_fecha_inicio'];
        $meses=$años*12;
       
       for ($i=1; $i <=$meses; $i++){$ejec[$i]=0;$ejec_a[$i]=0;$ejec_a2[$i]=0;}
        for ($i=1; $i <=12; $i++){$e[$i]=0;$ea1[$i]=0;$ea2[$i]=0;}

       $variable_e=1; 
       for ($i=1; $i <=$años; $i++)
       {
          $ejecutado_fin= $this->model_reporte->ejecutado_financiero($proy_id,$g_inicio);
          if(count($ejecutado_fin)!=0) ///// tiene Ejecutado 
          {
            for ($j=1; $j <=12; $j++)
            {
              $ejec[$variable_e]=$ejecutado_fin[0][$me[$j]];
              $variable_e++;
            }
          }
          else
          {
            for ($j=1; $j <=12; $j++) ///// no tiene Ejecutado
            {
              $ejec[$variable_e]=0;
              $variable_e++;
            }
          }

        $g_inicio++;  
       }

      /*---------------------------------------- EA, %EA -------------------------------------*/
        $suma_ejec=0;
        for($i=1;$i<=$meses;$i++)
        {
          $suma_ejec=$suma_ejec+$ejec[$i];
          $ejec_a[$i]=$suma_ejec; ///// Programado acumulado
          if($fase[0]['pfec_ptto_fase']!=0)
          {
            $ejec_a2[$i]=round((($ejec_a[$i]/$fase[0]['pfec_ptto_fase'])*100),2); ////// Programado Acumulado %
          }
          
        }
      /*---------------------------------------------------------------------------------------*/

        $i=1;$meses=12;$gestion=$fase[0]['pfec_fecha_inicio'];
        for($kp=1;$kp<=$años;$kp++)
        {
          if($gestion==$this->session->userdata('gestion'))
          {
            $variable=1;
            for($pos=$i;$pos<=$meses;$pos++)
              {
                $e[$variable]=$ejec[$pos];
                $ea1[$variable]=$ejec_a[$pos];
                $ea2[$variable]=$ejec_a2[$pos];

                $variable++;
              }
          }

          $i=$meses+1;
          $meses=$meses+12;
          $gestion++; 
        }
       
       $valor[1]=$e[$id_mes];  ///// EJECUTADO AL MES ACTUAL
       $valor[2]=$ea1[$id_mes]; ///// EJECUTADO ACUMULADO AL MES ACTUAL
       $valor[3]=$ea2[$id_mes]; ///// % EJECUTADO AL MES ACTUAL
      return $valor;
    }
/*===================================================================================================================*/

/*=======================================  EJECUCION FISICA P,PA,%PA PLURI ANUAL ===================================*/
  public function avance_fisico_gestiones($proy_id,$id_gestion,$id_mes)
  {
  $valor[1]=0;$valor[2]=0;$valor[3]=0;
  $fase = $this->model_faseetapa->get_id_fase($proy_id);
  $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
  for($p=1;$p<=12;$p++){$prog[$p]=0; $ejec[$p]=0;$efic[$p]=0;} 
  $meses=$años*12;
  for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
  for($p=1;$p<=$meses;$p++){$cef[$p]=0;} //// vector eficacia
  for($p=1;$p<=$meses;$p++){$ef_menor[$p]=0;$ef_entre[$p]=0;$ef_mayor[$p]=0;} //// vector eficacia
  $componentes = $this->model_componente->componentes_id($fase[0]['id']);

  foreach ($componentes as $rowc)
  {
    for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
    $productos = $this->model_producto->list_prod($rowc['com_id']);
    
    foreach ($productos as $rowp)
    {
      $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
      for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
      
      foreach ($actividad as $rowa)
      {   
        $pa=0; $pe=0; $gestion=$fase[0]['pfec_fecha_inicio']; $a=1;$e=1;
        for($k=1;$k<=$años;$k++)
        { 
          $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$gestion); /// programado
          $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$gestion); /// ejecutado
          $nro=0;
          foreach($programado as $row)
          {
            $nro++;
            $matriz [1][$nro]=$row['m_id'];
            $matriz [2][$nro]=$row['pg_fis'];
          }
          /*---------------- llenando la matriz vacia --------------*/
          for($j = 1; $j<=12; $j++)
          {
            $matriz_r[1][$j]=$j;
            $matriz_r[2][$j]='0';
            $matriz_r[3][$j]='0';
            $matriz_r[4][$j]='0';
            $matriz_r[5][$j]='0';
            $matriz_r[6][$j]='0';
            $matriz_r[7][$j]='0';
            $matriz_r[8][$j]='0';
            $matriz_r[9][$j]='0';
            $matriz_r[10][$j]='0';
          }
          /*--------------------------------------------------------*/
          /*--------------------ejecutado gestion ------------------*/
            $nro_e=0;
            foreach($ejecutado as $row)
            {
              $nro_e++;
              $matriz_e [1][$nro_e]=$row['m_id'];
              $matriz_e [2][$nro_e]=$row['ejec_fis'];
              $matriz_e [3][$nro_e]=$row['ejec_fis_a'];
              $matriz_e [4][$nro_e]=$row['ejec_fis_b'];
            }
            /*--------------------------------------------------------*/
            /*------- asignando en la matriz P, PA, %PA ----------*/
            for($i = 1 ;$i<=$nro ;$i++)
            {
              for($j = 1 ;$j<=12 ;$j++)
              {
                if($matriz[1][$i]==$matriz_r[1][$j])
                {
                  $matriz_r[2][$j]=round($matriz[2][$i],2);
                }
              }
            }

            for($j = 1 ;$j<=12 ;$j++){
              $pa=$pa+$matriz_r[2][$j];
              $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
              $matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
            }

            if($rowa['indi_id']==1)
            {
              for($i = 1 ;$i<=$nro_e ;$i++){
                for($j = 1 ;$j<=12 ;$j++)
                {
                  if($matriz_e[1][$i]==$matriz_r[1][$j])
                  {
                    $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                  }
                }
              }
            }
            elseif ($rowa['indi_id']==2) 
            {
              if($rowa['act_denominador']==0)
              {
                for($i = 1 ;$i<=$nro_e ;$i++){
                  for($j = 1 ;$j<=12 ;$j++)
                  {
                    if($matriz_e[1][$i]==$matriz_r[1][$j])
                    {
                      $matriz_r[5][$j]=round($matriz_e[3][$i],1);
                      $matriz_r[6][$j]=round($matriz_e[4][$i],1);
                      $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                    }
                  }
                }
              }
              if ($rowa['act_denominador']==1)
              {
                for($i = 1 ;$i<=$nro_e ;$i++){
                  for($j = 1 ;$j<=12 ;$j++)
                  {
                    if($matriz_e[1][$i]==$matriz_r[1][$j])
                    {
                      $matriz_r[5][$j]=round($matriz_e[3][$i],1);
                      $matriz_r[6][$j]=round($matriz_e[4][$i],1);
                      $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                    }
                  }
                }
              }
             /*--------------------------------------------------------*/
            }
            /*--------------------matriz E,AE,%AE gestion ------------------*/
            for($j = 1 ;$j<=12 ;$j++){
              $pe=$pe+$matriz_r[7][$j];
              $matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
              $matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
              if($matriz_r[4][$j]==0)
              {$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),1);}
              else
              {
                $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),1);
              }
          }

            $ap[$a]=(($matriz_r[4][1]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][1]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][2]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][2]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][3]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][3]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][4]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][4]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][5]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][5]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][6]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][6]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][7]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][7]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][8]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][8]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][9]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][9]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][10]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][10]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][11]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][11]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][12]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][12]*$rowa['act_ponderacion'])/100); $e++;

          $gestion++;

        }

        for($k=1;$k<=$meses;$k++)
        {
          //echo '['.$ap[$k].']'; 
          $app[$k]=round(($app[$k]+$ap[$k]),2); //// sumando a nivel de las actividades
          $aee[$k]=round(($aee[$k]+$ae[$k]),2); //// sumando a nivel de las actividades
        }

      }

      for($kp=1;$kp<=$meses;$kp++)
      {
        //echo'[['.$app[$kp].']'; 
        $ppp[$kp]=round(($ppp[$kp]+($app[$kp]*$rowp['prod_ponderacion'])/100),2); //// sumando a nivel de los productos
        $ppe[$kp]=round(($ppe[$kp]+($aee[$kp]*$rowp['prod_ponderacion'])/100),2); //// sumando a nivel de los productos
      } 
    }
    
    for($k=1;$k<=$meses;$k++)
      {
        //echo '['.$ppp[$k].']'; 
        $cpp[$k]=round(($cpp[$k]+($ppp[$k]*$rowc['com_ponderacion'])/100),1); //// sumando a nivel de los productos
        $cpe[$k]=round(($cpe[$k]+($ppe[$k]*$rowc['com_ponderacion'])/100),1); //// sumando a nivel de los productos
        if($cpp[$k]==0)
        {$cef[$k]=$cpe[$k];}
        else{$cef[$k]=round((($cpe[$k]/$cpp[$k])*100),2);}
      }
  }

/*-------------------------------------------------------------------------------------*/

    $i=1;$meses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;$a=1;
      for($kp=1;$kp<=$años;$kp++)
      {
        $variable=0;
        for($pos=$i;$pos<=$meses;$pos++)
        {
          $variable++;
          $prog[$variable]=$cpp[$pos];
          $ejec[$variable]=$cpe[$pos];
          $efic[$variable]=$cef[$pos];

        }
        if($gestion==$id_gestion)
        {
            $valor[1]=$prog[$id_mes]; //// % PROGRAMADO ACUMULADO AL MES ACTUAL 
            $valor[2]=$ejec[$id_mes]; //// % EJECUTADO ACUMULADO AL MES ACTUAL 
            $valor[3]=$efic[$id_mes]; //// % EFICIENCIA ACUMULADO AL MES ACTUAL 
        }

          $i=$meses+1;
          $meses=$meses+12;
          $gestion++; $nro++; $a++;        
      }
    return $valor;
  }
  /*=========================================================================================================*/
/*=======================================  INFORMACION FISICA ANUAL - PLURI ANUAL ===================================*/
  public function informacion_fisica_anual($proy_id,$id_gestion,$id_mes)
  {
  for($p=1;$p<=6;$p++){$valor[$p]=0;}
  
  $fase = $this->model_faseetapa->get_id_fase($proy_id);
  $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
  for($p=1;$p<=12;$p++){$prog[$p]=0; $ejec[$p]=0;$efic[$p]=0;} 
  $meses=$años*12;
  for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
  for($p=1;$p<=$meses;$p++){$cef[$p]=0;} //// vector eficacia
  for($p=1;$p<=$meses;$p++){$ef_menor[$p]=0;$ef_entre[$p]=0;$ef_mayor[$p]=0;} //// vector eficacia
  $componentes = $this->model_componente->componentes_id($fase[0]['id']);

  foreach ($componentes as $rowc)
  {
    for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
    $productos = $this->model_producto->list_prod($rowc['com_id']);
    
    foreach ($productos as $rowp)
    {
      $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
      for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
      
      foreach ($actividad as $rowa)
      {   
        $pa=0; $pe=0; $gestion=$fase[0]['pfec_fecha_inicio']; $a=1;$e=1;
        for($k=1;$k<=$años;$k++)
        { 
          $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$gestion); /// programado
          $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$gestion); /// ejecutado
          $nro=0;
          foreach($programado as $row)
          {
            $nro++;
            $matriz [1][$nro]=$row['m_id'];
            $matriz [2][$nro]=$row['pg_fis'];
          }
          /*---------------- llenando la matriz vacia --------------*/
          for($j = 1; $j<=12; $j++)
          {
            $matriz_r[1][$j]=$j;
            $matriz_r[2][$j]='0';
            $matriz_r[3][$j]='0';
            $matriz_r[4][$j]='0';
            $matriz_r[5][$j]='0';
            $matriz_r[6][$j]='0';
            $matriz_r[7][$j]='0';
            $matriz_r[8][$j]='0';
            $matriz_r[9][$j]='0';
            $matriz_r[10][$j]='0';
          }
          /*--------------------------------------------------------*/
          /*--------------------ejecutado gestion ------------------*/
            $nro_e=0;
            foreach($ejecutado as $row)
            {
              $nro_e++;
              $matriz_e [1][$nro_e]=$row['m_id'];
              $matriz_e [2][$nro_e]=$row['ejec_fis'];
              $matriz_e [3][$nro_e]=$row['ejec_fis_a'];
              $matriz_e [4][$nro_e]=$row['ejec_fis_b'];
            }
            /*--------------------------------------------------------*/
            /*------- asignando en la matriz P, PA, %PA ----------*/
            for($i = 1 ;$i<=$nro ;$i++)
            {
              for($j = 1 ;$j<=12 ;$j++)
              {
                if($matriz[1][$i]==$matriz_r[1][$j])
                {
                  $matriz_r[2][$j]=round($matriz[2][$i],2);
                }
              }
            }

            for($j = 1 ;$j<=12 ;$j++){
              $pa=$pa+$matriz_r[2][$j];
              $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
              $matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
            }

            if($rowa['indi_id']==1)
            {
              for($i = 1 ;$i<=$nro_e ;$i++){
                for($j = 1 ;$j<=12 ;$j++)
                {
                  if($matriz_e[1][$i]==$matriz_r[1][$j])
                  {
                    $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                  }
                }
              }
            }
            elseif ($rowa['indi_id']==2) 
            {
              if($rowa['act_denominador']==0)
              {
                for($i = 1 ;$i<=$nro_e ;$i++){
                  for($j = 1 ;$j<=12 ;$j++)
                  {
                    if($matriz_e[1][$i]==$matriz_r[1][$j])
                    {
                      $matriz_r[5][$j]=round($matriz_e[3][$i],1);
                      $matriz_r[6][$j]=round($matriz_e[4][$i],1);
                      $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                    }
                  }
                }
              }
              if ($rowa['act_denominador']==1)
              {
                for($i = 1 ;$i<=$nro_e ;$i++){
                  for($j = 1 ;$j<=12 ;$j++)
                  {
                    if($matriz_e[1][$i]==$matriz_r[1][$j])
                    {
                      $matriz_r[5][$j]=round($matriz_e[3][$i],1);
                      $matriz_r[6][$j]=round($matriz_e[4][$i],1);
                      $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                    }
                  }
                }
              }
             /*--------------------------------------------------------*/
            }
            /*--------------------matriz E,AE,%AE gestion ------------------*/
            for($j = 1 ;$j<=12 ;$j++){
              $pe=$pe+$matriz_r[7][$j];
              $matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
              $matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
              if($matriz_r[4][$j]==0)
              {$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),1);}
              else
              {
                $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),1);
              }
          }

            $ap[$a]=(($matriz_r[4][1]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][1]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][2]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][2]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][3]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][3]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][4]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][4]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][5]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][5]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][6]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][6]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][7]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][7]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][8]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][8]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][9]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][9]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][10]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][10]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][11]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][11]*$rowa['act_ponderacion'])/100); $e++;

            $ap[$a]=(($matriz_r[4][12]*$rowa['act_ponderacion'])/100); $a++;
            $ae[$e]=(($matriz_r[9][12]*$rowa['act_ponderacion'])/100); $e++;

          $gestion++;

        }

        for($k=1;$k<=$meses;$k++)
        {
          //echo '['.$ap[$k].']'; 
          $app[$k]=round(($app[$k]+$ap[$k]),2); //// sumando a nivel de las actividades
          $aee[$k]=round(($aee[$k]+$ae[$k]),2); //// sumando a nivel de las actividades
        }

      }

      for($kp=1;$kp<=$meses;$kp++)
      {
        //echo'[['.$app[$kp].']'; 
        $ppp[$kp]=round(($ppp[$kp]+($app[$kp]*$rowp['prod_ponderacion'])/100),2); //// sumando a nivel de los productos
        $ppe[$kp]=round(($ppe[$kp]+($aee[$kp]*$rowp['prod_ponderacion'])/100),2); //// sumando a nivel de los productos
      } 
    }
    
    for($k=1;$k<=$meses;$k++)
      {
        //echo '['.$ppp[$k].']'; 
        $cpp[$k]=round(($cpp[$k]+($ppp[$k]*$rowc['com_ponderacion'])/100),1); //// sumando a nivel de los productos
        $cpe[$k]=round(($cpe[$k]+($ppe[$k]*$rowc['com_ponderacion'])/100),1); //// sumando a nivel de los productos
        if($cpp[$k]==0)
        {$cef[$k]=$cpe[$k];}
        else{$cef[$k]=round((($cpe[$k]/$cpp[$k])*100),2);}
      }
  }

/*-------------------------------------------------------------------------------------*/
      $i=1;$meses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$gest_prog=0;$gest_ejec=0;
      for($kp=1;$kp<=$años;$kp++)
      {
        $variable=0;$gest_prog=$prog[12];$gest_ejec=$ejec[12];
        for($pos=$i;$pos<=$meses;$pos++)
        {
          $variable++;
          $prog[$variable]=$cpp[$pos];
          $ejec[$variable]=$cpe[$pos];
          $efic[$variable]=$cef[$pos];

        }
        if($gestion==$id_gestion)
        {
            $valor[1]=$prog[12]-$gest_prog; //// PROGRAMADO GESTION
            $valor[2]=$prog[$id_mes]-$gest_prog; //// % PROGRAMADO AL MES ACTIVO
            $valor[3]=$ejec[$id_mes]-$gest_ejec;; //// % EJECUCION AL MES ACTIVO
        }
          $i=$meses+1;
          $meses=$meses+12;
          $gestion++;      
      }

      if($valor[1]!=0){$valor[4]=(($valor[3]/$valor[1])*100); /* // % EJECUCION / GESTION */}
      if($valor[2]!=0){$valor[5]=(($valor[3]/$valor[2])*100); /* // % EJECUCION / AL MES ACTIVO */}

    return $valor;
  }
  /*============================= EXPORTANDO A EXCEL AVANCE EJECUCION DE LAS ACCIONES ===========================*/
  public function get_ejecuccion_accion($proy_id)
    {  
        $gestion = $this->session->userdata('gestion');
        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $proyecto = $this->model_proyecto->get_id_proyecto($proy_id); 
        $unidad_responsable=$this->model_proyecto->responsable_proy($proy_id,1);
        $fase = $this->model_faseetapa->get_id_fase($proy_id);

        /*----------------------------------------------- PRESUPUESTO GESTION -----------------------------------------------------*/
        $ptto_gestion=$this->ptto_mod_aprobadas_presupuesto_vigente_proy($proy_id,$gestion,$this->session->userdata("mes"));
        $ptto_gestion2=$this->avance_financiero($proy_id,$gestion,$this->session->userdata("mes"));

        /*----------------------------------------------- COSTO TOTAL ACCION -----------------------------------------------------*/
        $ctotal=$this->costo_total_accion($proy_id,$gestion,$this->session->userdata("mes"));
        /*----------------------------------------------- AVANCE ACUMULADO %-----------------------------------------------------*/
        $avance_fis_ac=$this->avance_fisico_gestiones($proy_id,$gestion,$this->session->userdata("mes"));

        /*----------------------------------------------- INFORMACION FISICA ANUAL -----------------------------------------------------*/
        $inf_fis_anual=$this->informacion_fisica_anual($proy_id,$gestion,$this->session->userdata("mes"));

        /*----------------------------------------------- EMPRESA -----------------------------------------------------*/
        $ctta=$this->contrato_proyecto_fase($fase[0]['id']);
        /*----------------------------------------------- ESTADO -----------------------------------------------------*/
        $estado=$this->estado_proyecto($fase[0]['id']);
        /*----------------------------------------------- PDES -----------------------------------------------------*/
        $pdes=$this->v_pedes($proy_id);
        /*----------------------------------------------- PTDI -----------------------------------------------------*/
        $ptdi=$this->v_ptdi($proy_id);
        /*----------------------------------------------- IMAGEN DE EJECUCION -----------------------------------------------------*/
        $img=$this->model_faseetapa->get_img_ejec($fase[0]['id']);

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> RESUMEN DE EJECUCI&Oacute;N DE ACCI&Oacute;N <br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <hr>
                <div class="titulo_dictamen">DATOS GENERALES </div>
                 <table  class="table_contenedor" style="table-layout:fixed;" border="1">
                    <tr>
                        <td colspan="4">
                            '.$proyecto[0]['proy_nombre'].'<br>
                            <b>NOMBRE DEL PROYECTO-PROGRAMA-ACCI&Oacute;N</b>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:33.3%; text-align:center;">
                            '.$proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'].'<br>
                            <b>CATEGORIA PROGRAM&Aacute;TICA</b>
                        </td>
                        <td style="width:33.3%;" colspan="2" text-align:center;">';
                            if($proyecto[0]['tp_id']==1)
                            {$html .= ''.$proyecto[0]['proy_sisin'].'<br>';}
                            else
                            {$html .= 'N/A<br>';}
                            $html .= '<b>C&Oacute;DIGO SISIN</b>
                        </td>
                        
                        <td style="width:33.3%; text-align:center;">
                            '.strtoupper($proyecto[0]['tipo']).'<br>
                            <b>TIPO DE ACCI&Oacute;N</b>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" style="width:50%;"style="text-align:center;">
                            '.$unidad_responsable[0]['fun_nombre'].' '.$unidad_responsable[0]['fun_paterno'].' '.$unidad_responsable[0]['fun_materno'].'<br>
                            <b>RESPONSABLE</b>
                        </td>
                        <td colspan="2" style="width:50%;" style="text-align:center;">
                            '.$unidad_responsable[0]['uejec'].'<br>
                            <b>UNIDAD EJECUTORA</b>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:33.3%; text-align:center;">
                            FASE : '.$fase[0]['fase'].' ETAPA : '.$fase[0]['etapa'].'<br>
                            <b>FASE / ETAPA</b>
                        </td>
                        <td style="width:33.3%;" colspan="2" text-align:center;">
                          '.$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']).'<br>
                          <b>NUEVO / CONTINUIDAD</b>
                        </td>
                        
                        <td style="width:33.3%; text-align:center;">
                            '.$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']).'<br>
                            <b>ANUAL / PLURIANUAL</b>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" style="width:50%;"style="text-align:center;">
                            '.date('Y-m-d',strtotime($fase[0]['inicio'])).'<br>
                            <b>FECHA DE INICIO</b>
                        </td>
                        <td colspan="2" style="width:50%;" style="text-align:center;">
                            '.date('Y-m-d',strtotime($fase[0]['final'])).'<br>
                            <b>FECHA DE CONCLUSI&Oacute;N</b>
                        </td>
                    </tr>

                </table>
                <br>

                 <div class="titulo_dictamen">PRESUPUESTO GESTI&Oacute;N </div>
                 <table  class="table_contenedor" style="table-layout:fixed;" border="1">
                    <tr>
                      <th bgcolor="#454545">
                        PPTO INICIAL '.$gestion.'
                      </th>
                      <th bgcolor="#454545">
                        MODIFICACIONES '.$gestion.'
                      </th>
                      <th bgcolor="#454545">
                        PPTO VIGENTE '.$gestion.'
                      </th>
                      <th bgcolor="#454545">
                        PPTO PROGRAMADO A '.$mes.'
                      </th>
                      <th bgcolor="#454545">
                        PPTO EJECUTADO A '.$mes.' '.$gestion.'
                      </th>
                      <th bgcolor="#454545">
                        % EJECUCI&Oacute;N / MES '.$mes.'
                      </th>
                      <th bgcolor="#454545">
                        % EJECUCI&Oacute;N / GESTI&Oacute;N '.$gestion.'
                      </th>
                    </tr>

                     <tr>
                      <td>
                        '.number_format($ptto_gestion[1], 2, ',', '.').'
                      </td>
                      <td>
                        '.number_format($ptto_gestion[2], 2, ',', '.').'
                      </td>
                      <td>
                        '.number_format($ptto_gestion[3], 2, ',', '.').'
                      </td>
                      <td>
                        '.number_format($ptto_gestion2[2], 2, ',', '.').'
                      </td>
                      <td>
                        '.number_format($ptto_gestion2[5], 2, ',', '.').'
                      </td>
                      <td>
                        '.round($ptto_gestion2[7],2).' %
                      </td>
                      <td>
                        '.round($ptto_gestion2[8],2).' %
                      </td>
                </table>
                <br>

                 <div class="titulo_dictamen">COSTO TOTAL ACCI&Oacute;N</div>
                 <table  class="table_contenedor" style="table-layout:fixed;" border="1">
                    <tr>
                      <th bgcolor="#454545">
                        COSTO INICIAL (Bs.)
                      </th>
                      <th bgcolor="#454545">
                        MODIFICACIONES (Bs.)
                      </th>
                      <th bgcolor="#454545">
                        COSTO VIGENTE (Bs.)
                      </th>
                      <th bgcolor="#454545">
                        EJECUTADO ACUMULADO
                      </th>
                      <th bgcolor="#454545">
                        % EJECUTADO ACUMULADO
                      </th>
                      <th bgcolor="#454545">
                        AVANCE F&Iacute;SICO ACUMULADO %
                      </th>
                    </tr>

                    <tr>
                      <td>
                        '.number_format($ctotal[1], 2, ',', '.').'
                      </td>
                      <td>
                        '.number_format($ctotal[2], 2, ',', '.').'
                      </td>
                      <td>
                        '.number_format($ctotal[3], 2, ',', '.').'
                      </td>
                      <td>
                        '.number_format($ctotal[4], 2, ',', '.').'
                      </td>
                      <td>
                        '.round($ctotal[5],2).' %
                      </td>
                      <td>
                        '.$avance_fis_ac[1].' %
                      </td>
                    </tr>
                </table>
                <br>

                 <div class="titulo_dictamen">INFORMACI&Oacute;N FISICA ANUAL</div>
                 <table  class="table_contenedor" style="table-layout:fixed;" border="1">
                    <tr>
                      <th bgcolor="#454545">
                        PROGRAMADO GESTI&Oacute;N
                      </th>
                      <th bgcolor="#454545">
                        PROGRAMADO AL MES DE '.$mes.'
                      </th>
                      <th bgcolor="#454545">
                        % EJECUCI&Oacute;N AL MES DE '.$mes.'
                      </th>
                      <th bgcolor="#454545">
                        % EJECUCI&Oacute;N / GESTI&Oacute;N '.$gestion.'
                      </th>
                      <th bgcolor="#454545">
                        % EJECUCI&Oacute;N / MES
                      </th>
                    </tr>

                    <tr>
                      <td>
                        '.round($inf_fis_anual[1],2).' %
                      </td>
                      <td>
                        '.round($inf_fis_anual[2],2).' %
                      </td>
                      <td>
                        '.round($inf_fis_anual[3],2).' %
                      </td>
                      <td>
                        '.round($inf_fis_anual[4],2).' %
                      </td>
                      <td>
                        '.round($inf_fis_anual[5],2).' %
                      </td>
                    </tr>
                </table>
                <br>

                 <div class="titulo_dictamen">ESTADO DEL PROYECTO</div>
                 <table  class="table_contenedor" style="table-layout:fixed;" border="1">
                    <tr>
                      <th bgcolor="#454545">
                        NOMBRE DE LA EMPRESA
                      </th>
                      <th bgcolor="#454545">
                        PLAZO (Dias)
                      </th>
                      <th bgcolor="#454545">
                        MONTO DEL CONTRATO (Bs.)
                      </th>
                      <th bgcolor="#454545">
                        ESTADO DEL PROYECTO<br>General
                      </th>
                      <th bgcolor="#454545">
                        ESTADO DEL PROYECTO<br>Especifico
                      </th>
                    </tr>

                    <tr>
                      <td>
                        '.$ctta[1].'
                      </td>
                      <td>
                        '.$ctta[2].'
                      </td>
                      <td>
                        '.$ctta[3].'
                      </td>
                      <td>
                        '.$estado[1].'
                      </td>
                      <td>
                        '.$estado[2].'
                      </td>
                    </tr>
                </table>
                <br>

                <div class="titulo_dictamen">PDES</div>
                <table  class="table_contenedor" style="table-layout:fixed;" border="1">
                  <tr>
                    <td style="width:30%;">
                      <b>PILAR</b>
                    </td>
                    <td>
                      '.$pdes[1].'
                    </td>
                  </tr>
                  <tr>
                    <td style="width:30%;">
                      <b>META</b>
                    </td>
                    <td>
                      '.$pdes[2].'
                    </td>
                  </tr>
                  <tr>
                    <td style="width:30%;">
                      <b>RESULTADO</b>
                    </td>
                    <td>
                      '.$pdes[3].'
                    </td>
                  </tr>
                  <tr>
                    <td style="width:30%;">
                      <b>ACCI&Oacute;N</b>
                    </td>
                    <td>
                      '.$pdes[4].'
                    </td>
                  </tr>
                </table>
                <br>
          
                <div class="titulo_dictamen">PTDI</div>
                <table class="datos_principales" style="table-layout:fixed;" border=1>
                  <tr>
                    <td style="width:30%;">
                      <b>EJE DE DESARROLLO</b>
                    </td>
                    <td>
                      '.$ptdi[1].'
                    </td>
                  </tr>
                  <tr>
                    <td style="width:30%;">
                      <b>POLITICA</b>
                    </td>
                    <td>
                      '.$ptdi[2].'
                    </td>
                  </tr>
                  <tr>
                    <td style="width:30%;">
                      <b>PROGRAMA</b>
                    </td>
                    <td>
                      '.$ptdi[3].'
                    </td>
                  </tr>
                </table><br>';

                if(count($img)!=0)
                {
                  $html .= '<div class="titulo_dictamen">EJECUCI&Oacute;N DEL PROYECTO</div>
                    <table class="datos_principales" style="table-layout:fixed;" border=1>
                    <tr>
                      <th bgcolor="#454545" style="width:40%;">
                        <b>EJECUCI&Oacute;N</b>
                      </th>
                      <th bgcolor="#454545">
                        <b>DESCRIPCI&Oacute;N</b>
                      </th>
                    </tr>';
                  foreach($img as $row)
                  {
                    $html .= '
                    <tr>
                      <td style="width:30%;">
                         <img src="'.base_url().'archivos/emesproy/'.$row['archivo'].'"  style="width:160px; height:120px; ">
                      </td>
                      <td>
                        '.$row['documento'].'
                      </td>
                    </tr>
                    ';
                  }
                  $html .= '</table>';
                }
              
               
                $html .= '
                <div >
                    <table class="table_contenedor" style="margin-top: 1px; border=1>
                        <tr>
                            <td class="fila_unitaria">FIRMAS:</td>
                        </tr>
                    </table>
                    <table style="width: 80%;margin: 0 auto;">
                        <tr>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                        </tr>
                    </table>

                </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'portrait');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }
  /*=============================================================================================================*/
  public function excel()
  {
    header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");  
    header ("Cache-Control: no-cache, must-revalidate");  
    //header("Content-type: application/vnd.ms-excel; name='excel'");
    header ("Content-type: application/x-msexcel");
    //header("Content-Disposition: filename=ficheroExcel.xls");
    header ("Content-Disposition: attachment; filename=Ejecucion_acciones".$this->session->userdata('gestion').".xls" );
    header("Pragma: no-cache");
    header("Expires: 0");

    $dados_recebido = iconv('utf-8','iso-8859-1',$_POST['datos_a_enviar']);
    echo $dados_recebido;
   // echo $_POST['datos_a_enviar'];
  }

  /*========================================= EVALUACION DE ACCIONES ===========================================*/
  public function evaluacion_acciones()
  {
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $data['lista_aper_padres'] = $this->mpoa->lista_poa();//lista de aperturas padres  

    $this->load->view('admin/reportes/evaluacion/list_programas', $data);
  }
  /*=================================================================================================================*/
  /*========================================== LISTA DE PROYECTOS ==========================================*/
    public function list_proyectos($prog)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $acciones=$this->model_reporte->list_acciones($prog,$this->session->userdata("gestion"));
        $data['acciones'] = $acciones;

        $data['prog'] = $prog;

        //load the view
        $this->load->view('admin/reportes/evaluacion/list_proy', $data);
    }
    /*============================================================================================================*/
    /*========================================== IFRAME EVALUACION ==========================================*/
    public function evaluacion_eficacia($proy_id,$prog)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['prog'] = $prog;
        $data['proy_id'] = $proy_id;
        $data['fase']=$this->model_faseetapa->get_id_fase($proy_id); ///// fase etapa
        $modificaciones=$this->model_reporte->costos_total_etapa($data['fase'][0]['id']); ///// Modificaciones presupuesto
        if(count($modificaciones)!=0){
          $data['vigente'] = $modificaciones[0]['vigente'];
        }
        else{
          $data['vigente'] = $data['fase'][0]['pfec_ptto_fase'];
        }
        $data['ejecucion']=$this->model_reporte->suma_ejecucion($data['fase'][0]['id']); ///// suma ejecucion
        $data['metas']=$this->model_proyecto->metas_p($proy_id); ///// metas
        //load the view
        $this->load->view('admin/reportes/evaluacion/evaluacion_eficacia', $data);
    }
    /*============================================================================================================*/
    /*========================================== VALIDA EVALUACION ==========================================*/
    public function valida_evaluacion()
    { 
      if (!empty($_POST["meta_id"]) && is_array($_POST["ppto"]) ) 
      {
        $vigente=$this->input->post('vig');
        $ejecutado=$this->input->post('ejec');

        foreach ( array_keys($_POST["ppto"]) as $como  ) 
        {
          $prog=($_POST["ppto"][$como]/100)*$vigente;
          $ejec=($_POST["ppto"][$como]/100)*$ejecutado;

          /*------------------------ ACTUALIZANDO METAS --------------------*/
          $update_meta = array(
                  'pct_ppto' => $_POST["ppto"][$como],
                  'ppto_programado' => $prog,
                  'ppto_ejecutado' => $ejec,
                  'fun_id' => $this->session->userdata("fun_id"),
              );
          $this->db->where('meta_id', $_POST["meta_id"][$como]);
          $this->db->update('_metas', $update_meta);
          /*---------------------------------------------------------------------------------*/

        }

      }
      redirect('admin/rep/evaluacion/'.$this->input->post('proy_id').'/'.$this->input->post('prog').'');
    }
    /*============================================================================================================*/
    /*========================================== IFRAME EVALUACION ==========================================*/
    public function iframe_evaluacion($proy_id,$prog)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['prog'] = $prog;
        $data['proy_id'] = $proy_id;

        //load the view
        $this->load->view('admin/reportes/evaluacion/iframe_evaluacion', $data);
    }
    public function evaluacion_accion($proy_id,$prog)
    {  
        $gestion=$this->session->userdata('gestion');
        $meses=$this->get_mes($this->session->userdata("mes"));
        $metas=$this->model_proyecto->metas_p($proy_id); ///// metas        

        $fase=$this->model_faseetapa->get_id_fase($proy_id); ///// fase etapa
        $ejecucion=$this->model_reporte->suma_ejecucion($fase[0]['id']); ///// suma ejecucion
        /*-----------------------------  EJECUCION PRESUPESTARIA --------------------------*/
        $inicial = $fase[0]['pfec_ptto_fase'];
        $mod = 0;
        $vig = $fase[0]['pfec_ptto_fase'];
        $efic=0;

        $modificaciones=$this->model_reporte->costos_total_etapa($fase[0]['id']); ///// Modificaciones presupuesto
        if(count($modificaciones)!=0){
          $inicial = $modificaciones[0]['inicial'];
          $mod = $modificaciones[0]['diferencia'];
          $vig = $modificaciones[0]['vigente'];
        }

        if($inicial!=0){$efic=round((($ejecucion[0]['total']/$inicial)*100),2);}
        /*------------------------------------------------------------------------------*/

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> EVALUACI&Oacute;N DE ACCIONES<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>';

                $html .= '
                  <font size=1><b>EFICACIA</font>
                  <table class="table_contenedor">
                  <thead>
                    <tr class="collapse_t" style="background-color:#ac9fae;">
                      <th bgcolor="#000000" style="width:5%;"></th>
                      <th bgcolor="#000000" style="width:20%;" >METAS</th>
                      <th bgcolor="#000000" style="width:10%;" >PROGRAMADO</th>
                      <th bgcolor="#000000" style="width:10%;" >EJECUTADO</th> 
                      <th bgcolor="#000000" style="width:10%;" >EFICACIA</th> 
                    </tr>
                  </thead>
                    <tbody id="bdi">';
                        $nro=1;
                        foreach($metas as $row)
                          {
                            $html .= '
                            <tr>
                              <td>'.$nro.'</td>
                              <td>'.$row['meta_descripcion'].'</td>
                              <td>'.$row['meta_meta'].'</td>
                              <td>'.$row['meta_ejec'].'</td>
                              <td>'.$row['meta_efic'].' %</td>
                              
                            </tr>
                            ';
                            $nro++;
                          }
                      $html .= '
                    </tbody>
                    </table><br>

                    <font size=1><b>EJECUCI&Oacute;N PRESUPUESTARIA</font>
                    <table class="table_contenedor">
                      <thead>
                        <tr class="collapse_t" style="background-color:#ac9fae;">
                          <th bgcolor="#000000" style="width:5%;">INICIAL</th>
                          <th bgcolor="#000000" style="width:10%;" >MODIFICACIONES</th>
                          <th bgcolor="#000000" style="width:10%;" >VIGENTE</th> 
                          <th bgcolor="#000000" style="width:10%;" >EJECUTADO</th>
                          <th bgcolor="#000000" style="width:10%;" >%</th> 
                        </tr>
                      </thead>
                        <tbody id="bdi">
                          <tr>
                            <td>'.number_format($inicial, 2, ',', '.').'</td>
                            <td>'.number_format($mod, 2, ',', '.').'</td>
                            <td>'.number_format($vig, 2, ',', '.').'</td>
                            <td>'.number_format($ejecucion[0]['total'], 2, ',', '.').'</td>
                            <td>'.$efic.'%</td>
                          </tr>
                        </tbody>
                    </table><br>

                    <font size=1><b>EFICIENCIA FINANCIERA</font>';
                      foreach($metas as $row)
                        {
                          $html .= '
                          <table class="table_contenedor">
                            <thead>
                              <tr class="collapse_t" style="background-color:#ac9fae;">
                                <th bgcolor="#000000" style="width:20%;" >METAS</th>
                                <th bgcolor="#000000" style="width:10%;" >PROGRAMADO</th>
                                <th bgcolor="#000000" style="width:10%;" >EJECUTADO</th>
                                <th bgcolor="#000000" style="width:10%;" >% DE PRESUPUESTO</th> 
                                <th bgcolor="#000000" style="width:10%;" >PROGRAMADO</th> 
                                <th bgcolor="#000000" style="width:10%;" >EJECUTADO</th> 
                              </tr>
                            </thead>
                            <tr>
                              <td>'.$row['meta_descripcion'].'</td>
                              <td>'.$row['meta_meta'].'</td>
                              <td>'.$row['meta_ejec'].'</td>
                              <td>'.$row['pct_ppto'].' %</td>
                              <td>'.number_format($row['ppto_programado'], 2, ',', '.').'</td>
                              <td>'.number_format($row['ppto_ejecutado'], 2, ',', '.').'</td>
                            </tr>
                            </table>
                            <br>';
                            $cup=(($row['meta_meta']/100)*$row['ppto_programado']);
                            $html .= '
                            <table class="table_contenedor" style="width:50%;">
                              <thead>
                                <tr class="collapse_t">
                                  <td bgcolor="#000000" style="width:50%;"><font color="#ffffff">COSTO UNITARIO PROGRAMADO</font></td>
                                  <td style="width:50%;">'.number_format($cup, 2, ',', '.').'</td> 
                                </tr>
                                <tr class="collapse_t">
                                  <td bgcolor="#000000" style="width:50%;"><font color="#ffffff">COSTO UNITARIO EJECUTADO</font></td>
                                  <td style="width:50%;" ></td> 
                                </tr>
                                <tr class="collapse_t">
                                  <td bgcolor="#000000" style="width:50%;"><font color="#ffffff">EFICIENCIA FINANCIERA</font></td>
                                  <td style="width:50%;" ></td> 
                                </tr>
                              </thead>
                                
                            </table>';
                        }

                    
                $html .= '
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'portrait');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }
    /*============================================================================================================*/


    /*============================================= FICHA TECNICA DE PRODUCTOS =========================================*/
private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 7px;
                color: #ffffff;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1px;
                text-align: center;
                font-size: 6px;
            }
        .mv{font-size:10px;}
        .siipp{width:180px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .datos_principales {
            text-align: center;
            font-size: 8px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }

        .indi_desemp{
            font-size: 14px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 7px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 1px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .lista{
            text-align:left;
            padding-left: 8;
            list-style-type:square;
            margin:2px;
        }
    </style>';
  /*================================== REPORTE POR UNIDAD EJECTORA ===========================================*/
    public function reporte_por_unidad($uni_id,$gestion,$id_mes)
    {   $unidad=$this->model_proyecto->get_unidad($uni_id);
        $proyectos=$this->model_reporte->reporte_por_unidad($uni_id,$gestion);

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];
        $id_mes==$this->session->userdata("mes");

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> REPORTE POR UNIDAD EJECUTORA | '.$unidad[0]['uni_unidad'].' <br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer> ';
                    $nro=1;
                    foreach ($proyectos as $row) 
                    { $nro_f = $this->model_faseetapa->nro_fase($row['proy_id']); //// nro de fases 
                      $color='';
                      if($row['tp_id']==1){$color='#f2f2f2';$tp='INVERSI&Oacute;N';}elseif ($row['tp_id']==3) {$tp='PROGRAMA NO RECURRENTE';}
                      $html .= '
                      <table class="datos_principales" style="table-layout:fixed;" border="1">
                          <tr class="titulo_dictamen">
                            <td>'.$nro.'</td>
                            <td colspan=9><b>DATOS GENERALES</b></td>
                          </tr>
                          <tr class="titulo_dictamen">
                            <td>APERTURA PROGRAMATICA</td>
                            <td>NOMBRE PROYECTO</td>
                            <td>C&Oacute;DIGO_SISIN</td>
                            <td>TIPO DE PROYECTO</td>
                            <td>RESPONSABLE (UE)</b></td>
                            <td>FASE/ETAPA</td>
                            <td>NUEVO CONTINUO</td>
                            <td>ANUAL PLURIANUAL</td>
                            <td>FECHA INICIAL</td>
                            <td>FECHA INICIAL</td>
                          </tr>
                          <tr bgcolor='.$color.'>
                          <td>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</td>
                          <td>'.$row['proy_nombre'].'</td>
                          <td>'.$row['proy_sisin'].'</td>
                          <td>'.$tp.'</td>
                          <td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
                          
                          if($nro_f!=0) //// tiene fase
                            {
                              $fase = $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
                              $a_fis=$this->avance_fisico_gestion($row['proy_id'],$gestion,$id_mes);
                              $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
                              $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']); //// calcula Anual/Plurianual
                              $html .= '
                              <td> - '.$fase[0]['fase'].'<br> - '.$fase[0]['etapa'].'</td>
                              <td>'.$nc.'</td>
                              <td>'.$ap.'</td>
                              <td>'.date('d/m/Y',strtotime($fase[0]['inicio'])).'</td>
                              <td>'.date('d/m/Y',strtotime($fase[0]['final'])).'</td>
                              ';
                            }
                            else
                            {
                              $html .= '
                              <td>N/A</td>
                              <td>N/A</td>
                              <td>N/A</td>
                              <td>N/A</td>
                              <td>N/A</td>
                              ';
                            }
                          $html .= '
                          </tr>
                        </table>
                        <table class="datos_principales" style="table-layout:fixed;" border="1">
                          <tr class="titulo_dictamen">
                            <td colspan=5><b>COSTO TOTAL ETAPA</b></td>
                            <td colspan=5><b>EJECUCI&Oacute;N PRESUPUESTARIA '.$this->session->userdata("gestion").'</b></td>
                          </tr>
                          <tr class="titulo_dictamen">
                            <td>COSTO INICIAL (Bs.)</td>
                            <td>MODIFICACI&Oacute;N (Bs.)</td>
                            <td>COSTO VIGENTE (BS.)</td>
                            <td>EJECUTADO</td>
                            <td>EJECUTADO ACUMULADO %</td>
                            
                            <td>PTTO INICIAL '.$this->session->userdata("gestion").'</td>
                            <td>MODIFICACIONES '.$this->session->userdata("gestion").'</td>
                            <td>PTTO VIGENTE '.$this->session->userdata("gestion").'</td>
                            <td>PTTO EJECUTADO VIGENTE '.$mes.'-'.$this->session->userdata("gestion").'</td>
                            <td>EJECUTADO ACUMULADO '.$this->session->userdata("gestion").' %</td>
                          </tr>';
                          /*-------------------------------------- COSTO TOTAL ETAPA --------------------------*/
                          if($nro_f!=0 && $this->model_reporte->nro_costos_total_etapa($fase[0]['id'])!=0) //// tiene modificaciones Costo Total Etapa
                          {
                              $ct_etapa = $this->model_reporte->costos_total_etapa($fase[0]['id']);
                              $html .='
                              <td bgcolor="#DEFAFA">'.number_format($ct_etapa[0]['inicial'], 2, ',', ' ').' Bs.</td> 
                              <td bgcolor="#DEFAFA">'.number_format($ct_etapa[0]['diferencia'], 2, ',', ' ').' Bs.</td>
                              <td bgcolor="#DEFAFA">'.number_format($ct_etapa[0]['vigente'], 2, ',', ' ').' Bs.</td>';
                              $fase_prog=$ct_etapa[0]['vigente'];
                          }
                          else ////// No tiene modificaciones Costo Total Etapa
                          {
                              $fase = $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
                              $html .='
                              <td bgcolor="#DEFAFA"></td> 
                              <td bgcolor="#DEFAFA"> - </td>
                              <td bgcolor="#DEFAFA"></td>';
                              $fase_prog=0;
                          }
                          $html .='
                        </table>';
                           
                            
                          $html .='
                          <br>';
                          $nro++;
                    }
                $html .= '
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }
  /*=========================================================================================================================*/
    /*========= SELECCION DE OPCIONES - REPORTE EJECUCION INVERSION PUBLICA =================*/
    public function seleccion_reporte_inversion_publica()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $u_ejecutoras = $this->model_reporte->unidades_ejecutoras(1,$this->session->userdata('gestion'));//lista de unidades ejecutoras 

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $verif = $this->model_reporte->verif_ejecucion($this->session->userdata('mes'),$this->session->userdata('gestion'));
        $data['verif'] = $verif;
        
        $this->load->view('admin/reportes/ejecucion_ip/rep_ei_publica/select_unidad', $data);
      }
   /*========================================= VALIDAR NUEVA FASE ETAPA 1 ================================================================================*/
    public function validar_seleccion()
    { 
        if($this->input->server('REQUEST_METHOD') === 'POST') 
        {
            if($this->input->post('p1')=='on'){$p1=1;}else{$p1=0;}
            if($this->input->post('p2')=='on'){$p2=1;}else{$p2=0;}
            if($this->input->post('p3')=='on'){$p3=1;}else{$p3=0;}
            if($this->input->post('p4')=='on'){$p4=1;}else{$p4=0;}

            redirect('admin/rep/reporte_inversion/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'');
        }
    }
    /*========= REPORTE GERENCIAL-REPORTE EJECUCION INVERSION PUBLICA =================*/
    public function reporte_ejecucion_inversion_publica($p1,$p2,$p3,$p4)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $u_ejecutoras = $this->model_reporte->unidades_ejecutoras(1,$this->session->userdata('gestion'));//lista de unidades ejecutoras 

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['p1'] = $p1; //// proyectos de inversion
        $data['p2'] = $p2; //// programas recurrentes
        $data['p3'] = $p3; //// programas no recurrentes
        $data['p4'] = $p4; //// accion de funcionamiento

        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}

        $data['pr1']=$pr1;
        $data['pr2']=$pr2;
        $data['pr3']=$pr3;
        $data['pr4']=$pr4;

        $tabla1 = '';
        foreach($u_ejecutoras  as $ue)
        {   
            $tabla1 .= '<tr>';
            $tabla1 .= '<td style="text-align: left"><a href="' . site_url("") . '/admin/rep/rep_inversion_unidad/'.$ue['uni_id'].'/1/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'" >'.$ue['uni_unidad'].'</a></td>';
            $ct=$this->cantidad($ue['uni_id'],$this->session->userdata("gestion"),1);
            $tabla1 .= '<td>'.$ct[1].'</td>';
            $tabla1 .= '<td>'.$ct[2].'</td>';
            $tabla1 .= '<td>'.$ct[3].'</td>';
            $ptto_mod=$this->ptto_mod_aprobadas_presupuesto_vigente($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),1);
            $tabla1 .= '<td>'.number_format($ptto_mod[1], 2, ',', '.').'</td>'; //// Ptto Inicial
            $tabla1 .= '<td>'.number_format($ptto_mod[2], 2, ',', '.').'</td>'; //// Mod Aprobadas
            $tabla1 .= '<td>'.number_format($ptto_mod[4], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE CIATZ
            $tabla1 .= '<td>'.number_format($ptto_mod[5], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE OTROS
            $tabla1 .= '<td bgcolor=#ddead6>'.number_format($ptto_mod[3], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE TOTAL
            $ptto_ejec=$this->ptto_ejecutado($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),1);
            $tabla1 .= '<td>'.number_format($ptto_ejec[4], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO CIATZ (DEVENGADO DE TODOS LOS MESES)
            $tabla1 .= '<td>'.number_format($ptto_ejec[5], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO OTROS (DEVENGADO DE TODOS LOS MESES) 
            $tabla1 .= '<td bgcolor=#ddead6>'.number_format($ptto_ejec[4]+$ptto_ejec[5], 2, ',', '.').'</td>'; // PRESUPUESTO EJECUTADO TOTAL (DEVENGADO DE TODOS LOS MESES)
            if($ptto_mod[4]==0){$eg=$ptto_ejec[4];}else{$eg=round((($ptto_ejec[4]/$ptto_mod[4])*100),2);}
            $tabla1 .= '<td>'.$eg.'%</td>'; // (PRESUPUESTO EJECUTADO CIATZ / PRESUPUESTO VIGENTE CIATZ)*100
            if($ptto_mod[5]==0){$eo=$ptto_ejec[5];}else{$eo=round((($ptto_ejec[5]/$ptto_mod[5])*100),2);}
            $tabla1 .= '<td>'.$eo.'%</td>'; // (PRESUPUESTO EJECUTADO OTROS / PRESUPUESTO VIGENTE OTROS)*100
            if($ptto_mod[3]==0){$et=$ptto_ejec[4]+$ptto_ejec[5];}else{$et=round(((($ptto_ejec[4]+$ptto_ejec[5])/$ptto_mod[3])*100),2);}
            $tabla1 .= '<td bgcolor=#ddead6>'.$et.'%</td>'; // (PRESUPUESTO EJECUTADO TOTAL / PRESUPUESTO VIGENTE TOTAL)*100
            $tabla1 .= '<td>'.number_format(($ptto_mod[4]-$ptto_ejec[4]), 2, ',', '.').'</td>';
            $tabla1 .= '<td>'.number_format(($ptto_mod[5]-$ptto_ejec[5]), 2, ',', '.').'</td>';
            $tabla1 .= '<td bgcolor=#ddead6>'.number_format(($ptto_mod[3]-($ptto_ejec[4]+$ptto_ejec[5])), 2, ',', '.').'</td>';
            $tabla1 .= '</tr>';
        }



        $u_ejecutoras = $this->model_reporte->unidades_ejecutoras(2,$this->session->userdata('gestion'));//lista de unidades ejecutoras 
        $tabla2 = '';
        foreach($u_ejecutoras  as $ue)
        {   
            $tabla2 .= '<tr>';
            $tabla2 .= '<td style="text-align: left"><a href="' . site_url("") . '/admin/rep/rep_inversion_unidad/'.$ue['uni_id'].'/2/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'" >'.$ue['uni_unidad'].'</a></td>';
            $ct=$this->cantidad($ue['uni_id'],$this->session->userdata("gestion"),2);
            $tabla2 .= '<td>'.$ct[1].'</td>';
            $tabla2 .= '<td>'.$ct[2].'</td>';
            $tabla2 .= '<td>'.$ct[3].'</td>';
            $ptto_mod=$this->ptto_mod_aprobadas_presupuesto_vigente($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),2);
            $tabla2 .= '<td>'.number_format($ptto_mod[1], 2, ',', '.').'</td>'; //// Ptto Inicial
            $tabla2 .= '<td>'.number_format($ptto_mod[2], 2, ',', '.').'</td>'; //// Mod Aprobadas
            $tabla2 .= '<td>'.number_format($ptto_mod[4], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE CIATZ
            $tabla2 .= '<td>'.number_format($ptto_mod[5], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE OTROS
            $tabla2 .= '<td bgcolor=#ddead6>'.number_format($ptto_mod[3], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE TOTAL
            $ptto_ejec=$this->ptto_ejecutado($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),2);
            $tabla2 .= '<td>'.number_format($ptto_ejec[4], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO CIATZ (DEVENGADO DE TODOS LOS MESES)
            $tabla2 .= '<td>'.number_format($ptto_ejec[5], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO OTROS (DEVENGADO DE TODOS LOS MESES) 
            $tabla2 .= '<td bgcolor=#ddead6>'.number_format($ptto_ejec[4]+$ptto_ejec[5], 2, ',', '.').'</td>'; // PRESUPUESTO EJECUTADO TOTAL (DEVENGADO DE TODOS LOS MESES)
            if($ptto_mod[4]==0){$eg=$ptto_ejec[4];}else{$eg=round((($ptto_ejec[4]/$ptto_mod[4])*100),2);}
            $tabla2 .= '<td>'.$eg.'%</td>'; // (PRESUPUESTO EJECUTADO CIATZ / PRESUPUESTO VIGENTE CIATZ)*100
            if($ptto_mod[5]==0){$eo=$ptto_ejec[5];}else{$eo=round((($ptto_ejec[5]/$ptto_mod[5])*100),2);}
            $tabla2 .= '<td>'.$eo.'%</td>'; // (PRESUPUESTO EJECUTADO OTROS / PRESUPUESTO VIGENTE OTROS)*100
            if($ptto_mod[3]==0){$et=$ptto_ejec[4]+$ptto_ejec[5];}else{$et=round(((($ptto_ejec[4]+$ptto_ejec[5])/$ptto_mod[3])*100),2);}
            $tabla2 .= '<td bgcolor=#ddead6>'.$et.'%</td>'; // (PRESUPUESTO EJECUTADO TOTAL / PRESUPUESTO VIGENTE TOTAL)*100
            $tabla2 .= '<td>'.number_format(($ptto_mod[4]-$ptto_ejec[4]), 2, ',', '.').'</td>';
            $tabla2 .= '<td>'.number_format(($ptto_mod[5]-$ptto_ejec[5]), 2, ',', '.').'</td>';
            $tabla2 .= '<td bgcolor=#ddead6>'.number_format(($ptto_mod[3]-($ptto_ejec[4]+$ptto_ejec[5])), 2, ',', '.').'</td>';
            $tabla2 .= '</tr>';
        }


        $u_ejecutoras = $this->model_reporte->unidades_ejecutoras(3,$this->session->userdata('gestion'));//lista de unidades ejecutoras 
        $tabla3 = '';
        foreach($u_ejecutoras  as $ue)
        {   
            $tabla3 .= '<tr>';
            $tabla3 .= '<td style="text-align: left"><a href="' . site_url("") . '/admin/rep/rep_inversion_unidad/'.$ue['uni_id'].'/3/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'" >'.$ue['uni_unidad'].'</a></td>';
            $ct=$this->cantidad($ue['uni_id'],$this->session->userdata("gestion"),3);
            $tabla3 .= '<td>'.$ct[1].'</td>';
            $tabla3 .= '<td>'.$ct[2].'</td>';
            $tabla3 .= '<td>'.$ct[3].'</td>';
            $ptto_mod=$this->ptto_mod_aprobadas_presupuesto_vigente($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),3);
            $tabla3 .= '<td>'.number_format($ptto_mod[1], 2, ',', '.').'</td>'; //// Ptto Inicial
            $tabla3 .= '<td>'.number_format($ptto_mod[2], 2, ',', '.').'</td>'; //// Mod Aprobadas
            $tabla3 .= '<td>'.number_format($ptto_mod[4], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE CIATZ
            $tabla3 .= '<td>'.number_format($ptto_mod[5], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE OTROS
            $tabla3 .= '<td bgcolor=#ddead6>'.number_format($ptto_mod[3], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE TOTAL
            $ptto_ejec=$this->ptto_ejecutado($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),3);
            $tabla3 .= '<td>'.number_format($ptto_ejec[4], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO CIATZ (DEVENGADO DE TODOS LOS MESES)
            $tabla3 .= '<td>'.number_format($ptto_ejec[5], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO OTROS (DEVENGADO DE TODOS LOS MESES) 
            $tabla3 .= '<td bgcolor=#ddead6>'.number_format($ptto_ejec[4]+$ptto_ejec[5], 2, ',', '.').'</td>'; // PRESUPUESTO EJECUTADO TOTAL (DEVENGADO DE TODOS LOS MESES)
            if($ptto_mod[4]==0){$eg=$ptto_ejec[4];}else{$eg=round((($ptto_ejec[4]/$ptto_mod[4])*100),2);}
            $tabla3 .= '<td>'.$eg.'%</td>'; // (PRESUPUESTO EJECUTADO CIATZ / PRESUPUESTO VIGENTE CIATZ)*100
            if($ptto_mod[5]==0){$eo=$ptto_ejec[5];}else{$eo=round((($ptto_ejec[5]/$ptto_mod[5])*100),2);}
            $tabla3 .= '<td>'.$eo.'%</td>'; // (PRESUPUESTO EJECUTADO OTROS / PRESUPUESTO VIGENTE OTROS)*100
            if($ptto_mod[3]==0){$et=$ptto_ejec[4]+$ptto_ejec[5];}else{$et=round(((($ptto_ejec[4]+$ptto_ejec[5])/$ptto_mod[3])*100),2);}
            $tabla3 .= '<td bgcolor=#ddead6>'.$et.'%</td>'; // (PRESUPUESTO EJECUTADO TOTAL / PRESUPUESTO VIGENTE TOTAL)*100
            $tabla3 .= '<td>'.number_format(($ptto_mod[4]-$ptto_ejec[4]), 2, ',', '.').'</td>';
            $tabla3 .= '<td>'.number_format(($ptto_mod[5]-$ptto_ejec[5]), 2, ',', '.').'</td>';
            $tabla3 .= '<td bgcolor=#ddead6>'.number_format(($ptto_mod[3]-($ptto_ejec[4]+$ptto_ejec[5])), 2, ',', '.').'</td>';
            $tabla3 .= '</tr>';
        }


        $u_ejecutoras = $this->model_reporte->unidades_ejecutoras(4,$this->session->userdata('gestion'));//lista de unidades ejecutoras 
        $tabla4 = '';
        foreach($u_ejecutoras  as $ue)
        {   
            $tabla4 .= '<tr>';
            $tabla4 .= '<td style="text-align: left"><a href="' . site_url("") . '/admin/rep/rep_inversion_unidad/'.$ue['uni_id'].'/4/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'" >'.$ue['uni_unidad'].'</a></td>';
            $ct=$this->cantidad($ue['uni_id'],$this->session->userdata("gestion"),4);
            $tabla4 .= '<td>'.$ct[1].'</td>';
            $tabla4 .= '<td>'.$ct[2].'</td>';
            $tabla4 .= '<td>'.$ct[3].'</td>';
            $ptto_mod=$this->ptto_mod_aprobadas_presupuesto_vigente($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),4);
            $tabla4 .= '<td>'.number_format($ptto_mod[1], 2, ',', '.').'</td>'; //// Ptto Inicial
            $tabla4 .= '<td>'.number_format($ptto_mod[2], 2, ',', '.').'</td>'; //// Mod Aprobadas
            $tabla4 .= '<td>'.number_format($ptto_mod[4], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE CIATZ
            $tabla4 .= '<td>'.number_format($ptto_mod[5], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE OTROS
            $tabla4 .= '<td bgcolor=#ddead6>'.number_format($ptto_mod[3], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE TOTAL
            $ptto_ejec=$this->ptto_ejecutado($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),4);
            $tabla4 .= '<td>'.number_format($ptto_ejec[4], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO CIATZ (DEVENGADO DE TODOS LOS MESES)
            $tabla4 .= '<td>'.number_format($ptto_ejec[5], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO OTROS (DEVENGADO DE TODOS LOS MESES) 
            $tabla4 .= '<td bgcolor=#ddead6>'.number_format($ptto_ejec[4]+$ptto_ejec[5], 2, ',', '.').'</td>'; // PRESUPUESTO EJECUTADO TOTAL (DEVENGADO DE TODOS LOS MESES)
            if($ptto_mod[4]==0){$eg=$ptto_ejec[4];}else{$eg=round((($ptto_ejec[4]/$ptto_mod[4])*100),2);}
            $tabla4 .= '<td>'.$eg.'%</td>'; // (PRESUPUESTO EJECUTADO CIATZ / PRESUPUESTO VIGENTE CIATZ)*100
            if($ptto_mod[5]==0){$eo=$ptto_ejec[5];}else{$eo=round((($ptto_ejec[5]/$ptto_mod[5])*100),2);}
            $tabla4 .= '<td>'.$eo.'%</td>'; // (PRESUPUESTO EJECUTADO OTROS / PRESUPUESTO VIGENTE OTROS)*100
            if($ptto_mod[3]==0){$et=$ptto_ejec[4]+$ptto_ejec[5];}else{$et=round(((($ptto_ejec[4]+$ptto_ejec[5])/$ptto_mod[3])*100),2);}
            $tabla4 .= '<td bgcolor=#ddead6>'.$et.'%</td>'; // (PRESUPUESTO EJECUTADO TOTAL / PRESUPUESTO VIGENTE TOTAL)*100
            $tabla4 .= '<td>'.number_format(($ptto_mod[4]-$ptto_ejec[4]), 2, ',', '.').'</td>';
            $tabla4 .= '<td>'.number_format(($ptto_mod[5]-$ptto_ejec[5]), 2, ',', '.').'</td>';
            $tabla4 .= '<td bgcolor=#ddead6>'.number_format(($ptto_mod[3]-($ptto_ejec[4]+$ptto_ejec[5])), 2, ',', '.').'</td>';
            $tabla4 .= '</tr>';
        }

        $data['inversion'] = $tabla1;
        $data['programas'] = $tabla2;
        $data['nprogramas'] = $tabla3;
        $data['accion'] = $tabla4;
        //load the view
        $this->load->view('admin/reportes/ejecucion_ip/rep_ei_publica/unidad_ejecutora', $data);
    }

  /*============================== CALCULANDO LA CANTIDAD DE POR UE  =============================================*/
  public function cantidad($uni_id,$gestion,$tp)
  {
    $proyectos = $this->model_reporte->proyecto_ue($uni_id,$gestion,$tp);//lista de proyectos
    $nro_continuos=0;$nro_nuevo=0;$nro_null=0;
    $valor[1]=0;$valor[2]=0;$valor[3]=count($proyectos);
    foreach($proyectos  as $pr)
    {
      $fase = $this->model_faseetapa->get_id_fase($pr['proy_id']);// fase activa
      $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
      if($nc=='CONTINUIDAD'){$nro_continuos++;}
      elseif($nc=='NUEVO'){$nro_nuevo++;}
    }
    $valor[1]=$nro_continuos;
    $valor[2]=$nro_nuevo;
    return $valor;
  }

  /*============================== PTTO - MOD APROBADAS - PRESUPUESTO VIGENTE =============================================*/
  public function ptto_mod_aprobadas_presupuesto_vigente($uni_id,$gestion,$mes,$tp)
  {
    $proyectos = $this->model_reporte->proyecto_ue($uni_id,$gestion,$tp);//lista de proyectos
    
    $ptto_inicial=0;$modificaciones=0;$vigente=0;
    $gadlpz=0;$otros=0; $dev_gadlpz=0;$dev_otros=0;
    $valor[1]=0;$valor[2]=0;$valor[3]=0;$valor[4]=0;$valor[5]=0;$valor[6]=0;$valor[7]=0;
     
    foreach($proyectos  as $pr)
    {
      $proyectos_ejec=$this->model_reporte->lista_proy_ejec($gestion,$mes,$pr['proy_id']);//proyectos en ejecucion
      $suma_ptto=0;$suma_mod=0;$suma_vgente=0;
      $suma_gadlpz=0;$suma_otros=0; $devengado_gadlpz=0; $devengado_otros=0;
      foreach($proyectos_ejec  as $pe)
      {
        $suma_ptto=$suma_ptto+$pe['pem_ppto_inicial'];
        $suma_mod=$suma_mod+$pe['pem_modif_aprobadas'];
        $suma_vgente=$suma_vgente+$pe['pem_ppto_vigente'];

        if(($pe['ff_codigo']==20 || $pe['ff_codigo']==41) && ($pe['of_codigo']==220 || $pe['of_codigo']==230 || $pe['of_codigo']==116 || $pe['of_codigo']==117 || $pe['of_codigo']==119))
        {
          $suma_gadlpz=$suma_gadlpz+$pe['pem_ppto_vigente'];
          $devengado_gadlpz=$devengado_gadlpz+$pe['pem_devengado'];
        }
        else
        {
          $suma_otros=$suma_otros+$pe['pem_ppto_vigente'];
          $devengado_otros=$devengado_otros+$pe['pem_devengado'];
        }
      }

      $ptto_inicial=$ptto_inicial+$suma_ptto;
      $modificaciones=$modificaciones+$suma_mod;
      $vigente=$vigente+$suma_vgente;

      $gadlpz=$gadlpz+$suma_gadlpz;
      $otros=$otros+$suma_otros;
      $dev_gadlpz=$dev_gadlpz+$devengado_gadlpz;
      $dev_otros=$dev_otros+$devengado_otros;

    }
    $valor[1]=$ptto_inicial; ///// presupuesto inicial
    $valor[2]=$modificaciones; ///// Modificaciones aprobadas
    $valor[3]=$vigente; ////// Presupuesto Vigente Total
    $valor[4]=$gadlpz; ///// Presupuesto Vigente GADLPZ
    $valor[5]=$otros; ///// Presupuesto Vigente OTROS
    $valor[6]=$dev_gadlpz; ///// Devengado CIATZ
    $valor[7]=$dev_otros; ///// Devengado  OTROS

    return $valor;
  }

  /*======================= PTTO - MOD APROBADAS - PRESUPUESTO VIGENTE ==================================*/
  public function ptto_ejecutado($uni_id,$gestion,$mes,$tp)
  {
    $valor_mes=$mes;
    $gadlpz=0;$otros=0;$total=0;$dev_gadlpz=0;$dev_otros=0;
    $valor[1]=0;$valor[2]=0;$valor[3]=0;$valor[4]=0;$valor[5]=0;
    for($i=$valor_mes; $i>0; $i=$i-1)
    {
      $valor_mes=$this->ptto_mod_aprobadas_presupuesto_vigente($uni_id,$gestion,$i,$tp);
      $gadlpz=$gadlpz+$valor_mes[4];
      $otros=$otros+$valor_mes[5];
      $total=$total+$valor_mes[3];
      $dev_gadlpz=$dev_gadlpz+$valor_mes[6];
      $dev_otros=$dev_otros+$valor_mes[7];
    }
    $valor[1]=$gadlpz;
    $valor[2]=$otros;
    $valor[3]=$total;
    $valor[4]=$dev_gadlpz;
    $valor[5]=$dev_otros;
    
    return $valor;
  }

  /*================================== REPORTE POR UNIDAD EJECTORA ===========================================*/
    public function pdf_reporte_ejecucion_inversion_publica($p1,$p2,$p3,$p4)
    {  
      $gestion=$this->session->userdata('gestion');

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> RESUMEN DE EJECUCI&Oacute;N PRESUPUESTARIA POR RECURSOS DE LA GOBERNACI&Oacute;N Y OTROS REPORTE AL '.$dias.' DE '.$mes.' DE '.$gestion.' <br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>';
                if($p1==1)
                {
                  $html .= '
                  <b>PROYECTOS DE INVERSI&Oacute;N P&Uacute;BLICA</b>
                  <table class="datos_principales" style="table-layout:fixed;">
                      <tr>
                          <th style="width:2%;" rowspan="2" bgcolor="#000000">UNIDAD_EJECUTORA</th>
                          <th colspan="3" bgcolor="#000000">CANTIDAD</th>
                          <th rowspan="2" bgcolor="#000000">PRESUPUESTO INICIAL</th>
                          <th rowspan="2" bgcolor="#000000">MODIFICACIONES APROBADAS</th>
                          <th colspan="3" bgcolor="#000000">PRESUPUESTO VIGENTE</th>
                          <th colspan="3" bgcolor="#000000">PRESUPUESTO EJECUTADO</th>
                          <th colspan="3" bgcolor="#000000">% EJECUTADO</th>
                          <th colspan="3" bgcolor="#000000">SALDO POR EJECUTAR</th>
                         
                        </tr>
                        <tr>

                          <th style="width:1%;" bgcolor="#000000">CONT.</th>
                          <th style="width:1%;" bgcolor="#000000">NUEVO</th>
                          <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                          
                          
                          <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                          <th style="width:1%;" bgcolor="#000000">OTROS</th>
                          <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                          <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                          <th style="width:1%;" bgcolor="#000000">OTROS</th>
                          <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                          <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                          <th style="width:1%;" bgcolor="#000000">OTROS</th>
                          <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                          <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                          <th style="width:1%;" bgcolor="#000000">OTROS</th>
                          <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        </tr>
                    <tbody id="bdi">';
                    $u_ejecutoras = $this->model_reporte->unidades_ejecutoras(1,$this->session->userdata('gestion'));//lista de unidades ejecutoras 
                    $cont_cant_continuo=0;$cont_cant_nuevo=0;$cont_cant_total=0;$cont_ptto_inicial=0;$cont_mod=0;$cont_ptto_vigente_gadlp=0;$cont_ptto_vigente_otros=0;$cont_ptto_vigente_total=0;$cont_ptto_ejec_gadlp=0;
                    $cont_ptto_ejec_otros=0;$cont_ptto_ejec_total=0;$cont_ejec_gadlp=0;$cont_ejec_otros=0;$cont_ejec_total=0;$cont_saldoejec_gadlp=0;$cont_saldoejec_otros=0;$cont_saldoejec_total=0; 
                     foreach($u_ejecutoras  as $ue)
                      {   
                          $html .= '<tr>';
                          $html .= '<td>'.$ue['uni_unidad'].'</td>';
                          $ct=$this->cantidad($ue['uni_id'],$this->session->userdata("gestion"),1);
                          $html .= '<td>'.$ct[1].'</td>';
                          $html .= '<td>'.$ct[2].'</td>';
                          $html .= '<td>'.$ct[3].'</td>';
                          $ptto_mod=$this->ptto_mod_aprobadas_presupuesto_vigente($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),1);
                          $html .= '<td>'.number_format($ptto_mod[1], 2, ',', '.').'</td>'; //// Ptto Inicial
                          $html .= '<td>'.number_format($ptto_mod[2], 2, ',', '.').'</td>'; //// Mod Aprobadas
                          $html .= '<td>'.number_format($ptto_mod[4], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE CIATZ
                          $html .= '<td>'.number_format($ptto_mod[5], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE OTROS
                          $html .= '<td bgcolor=#ddead6>'.number_format($ptto_mod[3], 2, ',', ' ').'</td>'; //// PRESUPUESTO VIGENTE TOTAL
                          $ptto_ejec=$this->ptto_ejecutado($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),1);
                          $html .= '<td>'.number_format($ptto_ejec[4], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO CIATZ (DEVENGADO DE TODOS LOS MESES)
                          $html .= '<td>'.number_format($ptto_ejec[5], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO OTROS (DEVENGADO DE TODOS LOS MESES) 
                          $html .= '<td bgcolor=#ddead6>'.number_format($ptto_ejec[4]+$ptto_ejec[5], 2, ',', '.').'</td>'; // PRESUPUESTO EJECUTADO TOTAL (DEVENGADO DE TODOS LOS MESES)
                          if($ptto_mod[4]==0){$eg=$ptto_ejec[4];}else{$eg=round((($ptto_ejec[4]/$ptto_mod[4])*100),2);}
                          $html .= '<td>'.$eg.'%</td>'; // (PRESUPUESTO EJECUTADO CIATZ / PRESUPUESTO VIGENTE CIATZ)*100
                          if($ptto_mod[5]==0){$eo=$ptto_ejec[5];}else{$eo=round((($ptto_ejec[5]/$ptto_mod[5])*100),2);}
                          $html .= '<td>'.$eo.'%</td>'; // (PRESUPUESTO EJECUTADO OTROS / PRESUPUESTO VIGENTE OTROS)*100
                          if($ptto_mod[3]==0){$et=$ptto_ejec[4]+$ptto_ejec[5];}else{$et=round(((($ptto_ejec[4]+$ptto_ejec[5])/$ptto_mod[3])*100),2);}
                          $html .= '<td bgcolor=#ddead6>'.$et.'%</td>'; // (PRESUPUESTO EJECUTADO TOTAL / PRESUPUESTO VIGENTE TOTAL)*100
                          $html .= '<td>'.number_format(($ptto_mod[4]-$ptto_ejec[4]), 2, ',', '.').'</td>';
                          $html .= '<td>'.number_format(($ptto_mod[5]-$ptto_ejec[5]), 2, ',', '.').'</td>';
                          $html .= '<td bgcolor=#ddead6>'.number_format(($ptto_mod[3]-($ptto_ejec[4]+$ptto_ejec[5])), 2, ',', '.').'</td>';
                          $html .= '</tr>';
                          /*---------------------------------------------------------------*/
                          $cont_cant_continuo=$cont_cant_continuo+$ct[1];
                          $cont_cant_nuevo=$cont_cant_nuevo+$ct[2];
                          $cont_cant_total=$cont_cant_total+$ct[3];

                          $cont_ptto_inicial=$cont_ptto_inicial+$ptto_mod[1];
                          $cont_mod=$cont_mod+$ptto_mod[2];

                          $cont_ptto_vigente_gadlp=$cont_ptto_vigente_gadlp+$ptto_mod[4];
                          $cont_ptto_vigente_otros=$cont_ptto_vigente_otros+$ptto_mod[5];
                          $cont_ptto_vigente_total=$cont_ptto_vigente_total+$ptto_mod[3];

                          $cont_ptto_ejec_gadlp=$cont_ptto_ejec_gadlp+$ptto_ejec[4];
                          $cont_ptto_ejec_otros=$cont_ptto_ejec_otros+$ptto_ejec[5];
                          $cont_ptto_ejec_total=$cont_ptto_ejec_total+($ptto_ejec[4]+$ptto_ejec[5]);

                          $cont_ejec_gadlp=$cont_ejec_gadlp+$eg;
                          $cont_ejec_otros=$cont_ejec_otros+$eo;
                          $cont_ejec_total=$cont_ejec_total+$et;

                          $cont_saldoejec_gadlp=$cont_saldoejec_gadlp+(($ptto_mod[4]-$ptto_ejec[4]));
                          $cont_saldoejec_otros=$cont_saldoejec_otros+($ptto_mod[5]-$ptto_ejec[5]);
                          $cont_saldoejec_total=$cont_saldoejec_total+($ptto_mod[3]-($ptto_ejec[4]+$ptto_ejec[5]));
                          /*---------------------------------------------------------------*/
                      }
                      $html .= '
                      <tr>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_cant_continuo.'</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_cant_nuevo.'</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_cant_total.'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_inicial, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_mod, 2, ',', ' ').' Bs.</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_vigente_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_vigente_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_vigente_total, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_ejec_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_ejec_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_ejec_total, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_ejec_gadlp.'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_ejec_otros.'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_ejec_total.'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_saldoejec_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_saldoejec_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_saldoejec_total, 2, ',', '.').'</th>
                      </tr>
                    </tbody>
                    </table><br>';
                }
                if($p2==1)
                {
              $html .= '
              <b>PROGRAMAS RECURRENTES</b>
                <table class="datos_principales" style="table-layout:fixed;">
                  <thead>
                    <tr>
                        <th style="width:2%;" rowspan="2" bgcolor="#000000">UNIDAD_EJECUTORA</th>
                        <th colspan="3" bgcolor="#000000">CANTIDAD</th>
                        <th rowspan="2" bgcolor="#000000">PRESUPUESTO INICIAL</th>
                        <th rowspan="2" bgcolor="#000000">MODIFICACIONES APROBADAS</th>
                        <th colspan="3" bgcolor="#000000">PRESUPUESTO VIGENTE</th>
                        <th colspan="3" bgcolor="#000000">PRESUPUESTO EJECUTADO</th>
                        <th colspan="3" bgcolor="#000000">% EJECUTADO</th>
                        <th colspan="3" bgcolor="#000000">SALDO POR EJECUTAR</th>
                       
                      </tr>
                      <tr>

                        <th style="width:1%;" bgcolor="#000000">CONT.</th>
                        <th style="width:1%;" bgcolor="#000000">NUEVO</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        
                        
                        <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                        <th style="width:1%;" bgcolor="#000000">OTROS</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                        <th style="width:1%;" bgcolor="#000000">OTROS</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                        <th style="width:1%;" bgcolor="#000000">OTROS</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                        <th style="width:1%;" bgcolor="#000000">OTROS</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                      </tr>
                    </thead>
                  <tbody id="bdi">';
                  $u_ejecutoras = $this->model_reporte->unidades_ejecutoras(2,$this->session->userdata('gestion'));//lista de unidades ejecutoras 
                    $cont_cant_continuo=0;$cont_cant_nuevo=0;$cont_cant_total=0;$cont_ptto_inicial=0;$cont_mod=0;$cont_ptto_vigente_gadlp=0;$cont_ptto_vigente_otros=0;$cont_ptto_vigente_total=0;$cont_ptto_ejec_gadlp=0;
                    $cont_ptto_ejec_otros=0;$cont_ptto_ejec_total=0;$cont_ejec_gadlp=0;$cont_ejec_otros=0;$cont_ejec_total=0;$cont_saldoejec_gadlp=0;$cont_saldoejec_otros=0;$cont_saldoejec_total=0;  
                   foreach($u_ejecutoras  as $ue)
                    {   
                        $html .= '<tr>';
                        $html .= '<td>'.$ue['uni_unidad'].'</td>';
                        $ct=$this->cantidad($ue['uni_id'],$this->session->userdata("gestion"),2);
                        $html .= '<td>'.$ct[1].'</td>';
                        $html .= '<td>'.$ct[2].'</td>';
                        $html .= '<td>'.$ct[3].'</td>';
                        $ptto_mod=$this->ptto_mod_aprobadas_presupuesto_vigente($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),2);
                        $html .= '<td>'.number_format($ptto_mod[1], 2, ',', '.').'</td>'; //// Ptto Inicial
                        $html .= '<td>'.number_format($ptto_mod[2], 2, ',', '.').'</td>'; //// Mod Aprobadas
                        $html .= '<td>'.number_format($ptto_mod[4], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE CIATZ
                        $html .= '<td>'.number_format($ptto_mod[5], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE OTROS
                        $html .= '<td bgcolor=#ddead6>'.number_format($ptto_mod[3], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE TOTAL
                        $ptto_ejec=$this->ptto_ejecutado($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),2);
                        $html .= '<td>'.number_format($ptto_ejec[4], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO CIATZ (DEVENGADO DE TODOS LOS MESES)
                        $html .= '<td>'.number_format($ptto_ejec[5], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO OTROS (DEVENGADO DE TODOS LOS MESES) 
                        $html .= '<td bgcolor=#ddead6>'.number_format($ptto_ejec[4]+$ptto_ejec[5], 2, ',', '.').'</td>'; // PRESUPUESTO EJECUTADO TOTAL (DEVENGADO DE TODOS LOS MESES)
                        if($ptto_mod[4]==0){$eg=$ptto_ejec[4];}else{$eg=round((($ptto_ejec[4]/$ptto_mod[4])*100),2);}
                        $html .= '<td>'.$eg.'%</td>'; // (PRESUPUESTO EJECUTADO CIATZ / PRESUPUESTO VIGENTE CIATZ)*100
                        if($ptto_mod[5]==0){$eo=$ptto_ejec[5];}else{$eo=round((($ptto_ejec[5]/$ptto_mod[5])*100),2);}
                        $html .= '<td>'.$eo.'%</td>'; // (PRESUPUESTO EJECUTADO OTROS / PRESUPUESTO VIGENTE OTROS)*100
                        if($ptto_mod[3]==0){$et=$ptto_ejec[4]+$ptto_ejec[5];}else{$et=round(((($ptto_ejec[4]+$ptto_ejec[5])/$ptto_mod[3])*100),2);}
                        $html .= '<td bgcolor=#ddead6>'.$et.'%</td>'; // (PRESUPUESTO EJECUTADO TOTAL / PRESUPUESTO VIGENTE TOTAL)*100
                        $html .= '<td>'.number_format(($ptto_mod[4]-$ptto_ejec[4]), 2, ',', '.').'</td>';
                        $html .= '<td>'.number_format(($ptto_mod[5]-$ptto_ejec[5]), 2, ',', '.').'</td>';
                        $html .= '<td bgcolor=#ddead6>'.number_format(($ptto_mod[3]-($ptto_ejec[4]+$ptto_ejec[5])), 2, ',', '.').'</td>';
                        $html .= '</tr>';
                        /*---------------------------------------------------------------*/
                          $cont_cant_continuo=$cont_cant_continuo+$ct[1];
                          $cont_cant_nuevo=$cont_cant_nuevo+$ct[2];
                          $cont_cant_total=$cont_cant_total+$ct[3];

                          $cont_ptto_inicial=$cont_ptto_inicial+$ptto_mod[1];
                          $cont_mod=$cont_mod+$ptto_mod[2];

                          $cont_ptto_vigente_gadlp=$cont_ptto_vigente_gadlp+$ptto_mod[4];
                          $cont_ptto_vigente_otros=$cont_ptto_vigente_otros+$ptto_mod[5];
                          $cont_ptto_vigente_total=$cont_ptto_vigente_total+$ptto_mod[3];

                          $cont_ptto_ejec_gadlp=$cont_ptto_ejec_gadlp+$ptto_ejec[4];
                          $cont_ptto_ejec_otros=$cont_ptto_ejec_otros+$ptto_ejec[5];
                          $cont_ptto_ejec_total=$cont_ptto_ejec_total+($ptto_ejec[4]+$ptto_ejec[5]);

                          $cont_ejec_gadlp=$cont_ejec_gadlp+$eg;
                          $cont_ejec_otros=$cont_ejec_otros+$eo;
                          $cont_ejec_total=$cont_ejec_total+$et;

                          $cont_saldoejec_gadlp=$cont_saldoejec_gadlp+(($ptto_mod[4]-$ptto_ejec[4]));
                          $cont_saldoejec_otros=$cont_saldoejec_otros+($ptto_mod[5]-$ptto_ejec[5]);
                          $cont_saldoejec_total=$cont_saldoejec_total+($ptto_mod[3]-($ptto_ejec[4]+$ptto_ejec[5]));
                          /*---------------------------------------------------------------*/
                    }
                    $html .= '
                    <tr>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_cant_continuo.'</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_cant_nuevo.'</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_cant_total.'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_inicial, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_mod, 2, ',', ' ').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_vigente_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_vigente_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_vigente_total, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_ejec_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_ejec_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_ejec_total, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_ejec_gadlp.'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_ejec_otros.'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_ejec_total.'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_saldoejec_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_saldoejec_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_saldoejec_total, 2, ',', '.').'</th>
                      </tr>
                  </tbody>
                  </table><br>';
                }
                if($p3==1)
                {
                $html .= '
                <b>PROGRAMAS NO RECURRENTES</b>
                <table class="datos_principales" style="table-layout:fixed;">
                  <thead>
                    <tr>
                        <th style="width:2%;" rowspan="2" bgcolor="#000000">UNIDAD_EJECUTORA</th>
                        <th colspan="3" bgcolor="#000000">CANTIDAD</th>
                        <th rowspan="2" bgcolor="#000000">PRESUPUESTO INICIAL</th>
                        <th rowspan="2" bgcolor="#000000">MODIFICACIONES APROBADAS</th>
                        <th colspan="3" bgcolor="#000000">PRESUPUESTO VIGENTE</th>
                        <th colspan="3" bgcolor="#000000">PRESUPUESTO EJECUTADO</th>
                        <th colspan="3" bgcolor="#000000">% EJECUTADO</th>
                        <th colspan="3" bgcolor="#000000">SALDO POR EJECUTAR</th>
                       
                      </tr>
                      <tr>

                        <th style="width:1%;" bgcolor="#000000">CONT.</th>
                        <th style="width:1%;" bgcolor="#000000">NUEVO</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        
                        
                        <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                        <th style="width:1%;" bgcolor="#000000">OTROS</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                        <th style="width:1%;" bgcolor="#000000">OTROS</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                        <th style="width:1%;" bgcolor="#000000">OTROS</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                        <th style="width:1%;" bgcolor="#000000">OTROS</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                      </tr>
                    </thead>
                  <tbody id="bdi">';
                  $u_ejecutoras = $this->model_reporte->unidades_ejecutoras(3,$this->session->userdata('gestion'));//lista de unidades ejecutoras 
                    $cont_cant_continuo=0;$cont_cant_nuevo=0;$cont_cant_total=0;$cont_ptto_inicial=0;$cont_mod=0;$cont_ptto_vigente_gadlp=0;$cont_ptto_vigente_otros=0;$cont_ptto_vigente_total=0;$cont_ptto_ejec_gadlp=0;
                    $cont_ptto_ejec_otros=0;$cont_ptto_ejec_total=0;$cont_ejec_gadlp=0;$cont_ejec_otros=0;$cont_ejec_total=0;$cont_saldoejec_gadlp=0;$cont_saldoejec_otros=0;$cont_saldoejec_total=0;  
                   foreach($u_ejecutoras  as $ue)
                    {   
                        $html .= '<tr>';
                        $html .= '<td>'.$ue['uni_unidad'].'</td>';
                        $ct=$this->cantidad($ue['uni_id'],$this->session->userdata("gestion"),3);
                        $html .= '<td>'.$ct[1].'</td>';
                        $html .= '<td>'.$ct[2].'</td>';
                        $html .= '<td>'.$ct[3].'</td>';
                        $ptto_mod=$this->ptto_mod_aprobadas_presupuesto_vigente($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),3);
                        $html .= '<td>'.number_format($ptto_mod[1], 2, ',', '.').'</td>'; //// Ptto Inicial
                        $html .= '<td>'.number_format($ptto_mod[2], 2, ',', '.').'</td>'; //// Mod Aprobadas
                        $html .= '<td>'.number_format($ptto_mod[4], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE CIATZ
                        $html .= '<td>'.number_format($ptto_mod[5], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE OTROS
                        $html .= '<td bgcolor=#ddead6>'.number_format($ptto_mod[3], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE TOTAL
                        $ptto_ejec=$this->ptto_ejecutado($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),3);
                        $html .= '<td>'.number_format($ptto_ejec[4], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO CIATZ (DEVENGADO DE TODOS LOS MESES)
                        $html .= '<td>'.number_format($ptto_ejec[5], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO OTROS (DEVENGADO DE TODOS LOS MESES) 
                        $html .= '<td bgcolor=#ddead6>'.number_format($ptto_ejec[4]+$ptto_ejec[5], 2, ',', '.').'</td>'; // PRESUPUESTO EJECUTADO TOTAL (DEVENGADO DE TODOS LOS MESES)
                        if($ptto_mod[4]==0){$eg=$ptto_ejec[4];}else{$eg=round((($ptto_ejec[4]/$ptto_mod[4])*100),2);}
                        $html .= '<td>'.$eg.'%</td>'; // (PRESUPUESTO EJECUTADO CIATZ / PRESUPUESTO VIGENTE CIATZ)*100
                        if($ptto_mod[5]==0){$eo=$ptto_ejec[5];}else{$eo=round((($ptto_ejec[5]/$ptto_mod[5])*100),2);}
                        $html .= '<td>'.$eo.'%</td>'; // (PRESUPUESTO EJECUTADO OTROS / PRESUPUESTO VIGENTE OTROS)*100
                        if($ptto_mod[3]==0){$et=$ptto_ejec[4]+$ptto_ejec[5];}else{$et=round(((($ptto_ejec[4]+$ptto_ejec[5])/$ptto_mod[3])*100),2);}
                        $html .= '<td bgcolor=#ddead6>'.$et.'%</td>'; // (PRESUPUESTO EJECUTADO TOTAL / PRESUPUESTO VIGENTE TOTAL)*100
                        $html .= '<td>'.number_format(($ptto_mod[4]-$ptto_ejec[4]), 2, ',', '.').'</td>';
                        $html .= '<td>'.number_format(($ptto_mod[5]-$ptto_ejec[5]), 2, ',', '.').'</td>';
                        $html .= '<td bgcolor=#ddead6>'.number_format(($ptto_mod[3]-($ptto_ejec[4]+$ptto_ejec[5])), 2, ',', '.').'</td>';
                        $html .= '</tr>';
                        /*---------------------------------------------------------------*/
                          $cont_cant_continuo=$cont_cant_continuo+$ct[1];
                          $cont_cant_nuevo=$cont_cant_nuevo+$ct[2];
                          $cont_cant_total=$cont_cant_total+$ct[3];

                          $cont_ptto_inicial=$cont_ptto_inicial+$ptto_mod[1];
                          $cont_mod=$cont_mod+$ptto_mod[2];

                          $cont_ptto_vigente_gadlp=$cont_ptto_vigente_gadlp+$ptto_mod[4];
                          $cont_ptto_vigente_otros=$cont_ptto_vigente_otros+$ptto_mod[5];
                          $cont_ptto_vigente_total=$cont_ptto_vigente_total+$ptto_mod[3];

                          $cont_ptto_ejec_gadlp=$cont_ptto_ejec_gadlp+$ptto_ejec[4];
                          $cont_ptto_ejec_otros=$cont_ptto_ejec_otros+$ptto_ejec[5];
                          $cont_ptto_ejec_total=$cont_ptto_ejec_total+($ptto_ejec[4]+$ptto_ejec[5]);

                          $cont_ejec_gadlp=$cont_ejec_gadlp+$eg;
                          $cont_ejec_otros=$cont_ejec_otros+$eo;
                          $cont_ejec_total=$cont_ejec_total+$et;

                          $cont_saldoejec_gadlp=$cont_saldoejec_gadlp+(($ptto_mod[4]-$ptto_ejec[4]));
                          $cont_saldoejec_otros=$cont_saldoejec_otros+($ptto_mod[5]-$ptto_ejec[5]);
                          $cont_saldoejec_total=$cont_saldoejec_total+($ptto_mod[3]-($ptto_ejec[4]+$ptto_ejec[5]));
                          /*---------------------------------------------------------------*/
                    }
                    $html .= '
                    <tr>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_cant_continuo.'</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_cant_nuevo.'</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_cant_total.'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_inicial, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_mod, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_vigente_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_vigente_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_vigente_total, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_ejec_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_ejec_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_ejec_total, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_ejec_gadlp.'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_ejec_otros.'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_ejec_total.'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_saldoejec_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_saldoejec_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_saldoejec_total, 2, ',', '.').'</th>
                      </tr>
                  </tbody>
                  </table><br>';
                }
                if($p4==1)
                {
                $html .= '
                <b>ACCI&Oacute;N DE FUNCIONAMIENTO</b>
                <table class="datos_principales" style="table-layout:fixed;">
                  <thead>
                    <tr>
                        <th style="width:2%;" rowspan="2" bgcolor="#000000">UNIDAD_EJECUTORA</th>
                        <th colspan="3" bgcolor="#000000">CANTIDAD</th>
                        <th rowspan="2" bgcolor="#000000">PRESUPUESTO INICIAL</th>
                        <th rowspan="2" bgcolor="#000000">MODIFICACIONES APROBADAS</th>
                        <th colspan="3" bgcolor="#000000">PRESUPUESTO VIGENTE</th>
                        <th colspan="3" bgcolor="#000000">PRESUPUESTO EJECUTADO</th>
                        <th colspan="3" bgcolor="#000000">% EJECUTADO</th>
                        <th colspan="3" bgcolor="#000000">SALDO POR EJECUTAR</th>
                       
                      </tr>
                      <tr>

                        <th style="width:1%;" bgcolor="#000000">CONT.</th>
                        <th style="width:1%;" bgcolor="#000000">NUEVO</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        
                        
                        <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                        <th style="width:1%;" bgcolor="#000000">OTROS</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                        <th style="width:1%;" bgcolor="#000000">OTROS</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                        <th style="width:1%;" bgcolor="#000000">OTROS</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                        <th style="width:1%;" bgcolor="#000000">OTROS</th>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                      </tr>
                    </thead>
                  <tbody id="bdi">';
                  $u_ejecutoras = $this->model_reporte->unidades_ejecutoras(4,$this->session->userdata('gestion'));//lista de unidades ejecutoras 
                    $cont_cant_continuo=0;$cont_cant_nuevo=0;$cont_cant_total=0;$cont_ptto_inicial=0;$cont_mod=0;$cont_ptto_vigente_gadlp=0;$cont_ptto_vigente_otros=0;$cont_ptto_vigente_total=0;$cont_ptto_ejec_gadlp=0;
                    $cont_ptto_ejec_otros=0;$cont_ptto_ejec_total=0;$cont_ejec_gadlp=0;$cont_ejec_otros=0;$cont_ejec_total=0;$cont_saldoejec_gadlp=0;$cont_saldoejec_otros=0;$cont_saldoejec_total=0;  
                   foreach($u_ejecutoras  as $ue)
                    {   
                        $html .= '<tr>';
                        $html .= '<td>'.$ue['uni_unidad'].'</td>';
                        $ct=$this->cantidad($ue['uni_id'],$this->session->userdata("gestion"),4);
                        $html .= '<td>'.$ct[1].'</td>';
                        $html .= '<td>'.$ct[2].'</td>';
                        $html .= '<td>'.$ct[3].'</td>';
                        $ptto_mod=$this->ptto_mod_aprobadas_presupuesto_vigente($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),4);
                        $html .= '<td>'.number_format($ptto_mod[1], 2, ',', '.').'</td>'; //// Ptto Inicial
                        $html .= '<td>'.number_format($ptto_mod[2], 2, ',', '.').'</td>'; //// Mod Aprobadas
                        $html .= '<td>'.number_format($ptto_mod[4], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE CIATZ
                        $html .= '<td>'.number_format($ptto_mod[5], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE OTROS
                        $html .= '<td bgcolor=#ddead6>'.number_format($ptto_mod[3], 2, ',', '.').'</td>'; //// PRESUPUESTO VIGENTE TOTAL
                        $ptto_ejec=$this->ptto_ejecutado($ue['uni_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"),4);
                        $html .= '<td>'.number_format($ptto_ejec[4], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO CIATZ (DEVENGADO DE TODOS LOS MESES)
                        $html .= '<td>'.number_format($ptto_ejec[5], 2, ',', '.').'</td>'; //// PRESUPUESTO EJECUTADO OTROS (DEVENGADO DE TODOS LOS MESES) 
                        $html .= '<td bgcolor=#ddead6>'.number_format($ptto_ejec[4]+$ptto_ejec[5], 2, ',', '.').'</td>'; // PRESUPUESTO EJECUTADO TOTAL (DEVENGADO DE TODOS LOS MESES)
                        if($ptto_mod[4]==0){$eg=$ptto_ejec[4];}else{$eg=round((($ptto_ejec[4]/$ptto_mod[4])*100),2);}
                        $html .= '<td>'.$eg.'%</td>'; // (PRESUPUESTO EJECUTADO CIATZ / PRESUPUESTO VIGENTE CIATZ)*100
                        if($ptto_mod[5]==0){$eo=$ptto_ejec[5];}else{$eo=round((($ptto_ejec[5]/$ptto_mod[5])*100),2);}
                        $html .= '<td>'.$eo.'%</td>'; // (PRESUPUESTO EJECUTADO OTROS / PRESUPUESTO VIGENTE OTROS)*100
                        if($ptto_mod[3]==0){$et=$ptto_ejec[4]+$ptto_ejec[5];}else{$et=round(((($ptto_ejec[4]+$ptto_ejec[5])/$ptto_mod[3])*100),2);}
                        $html .= '<td bgcolor=#ddead6>'.$et.'%</td>'; // (PRESUPUESTO EJECUTADO TOTAL / PRESUPUESTO VIGENTE TOTAL)*100
                        $html .= '<td>'.number_format(($ptto_mod[4]-$ptto_ejec[4]), 2, ',', '.').'</td>';
                        $html .= '<td>'.number_format(($ptto_mod[5]-$ptto_ejec[5]), 2, ',', '.').'</td>';
                        $html .= '<td bgcolor=#ddead6>'.number_format(($ptto_mod[3]-($ptto_ejec[4]+$ptto_ejec[5])), 2, ',', '.').'</td>';
                        $html .= '</tr>';
                        /*---------------------------------------------------------------*/
                          $cont_cant_continuo=$cont_cant_continuo+$ct[1];
                          $cont_cant_nuevo=$cont_cant_nuevo+$ct[2];
                          $cont_cant_total=$cont_cant_total+$ct[3];

                          $cont_ptto_inicial=$cont_ptto_inicial+$ptto_mod[1];
                          $cont_mod=$cont_mod+$ptto_mod[2];

                          $cont_ptto_vigente_gadlp=$cont_ptto_vigente_gadlp+$ptto_mod[4];
                          $cont_ptto_vigente_otros=$cont_ptto_vigente_otros+$ptto_mod[5];
                          $cont_ptto_vigente_total=$cont_ptto_vigente_total+$ptto_mod[3];

                          $cont_ptto_ejec_gadlp=$cont_ptto_ejec_gadlp+$ptto_ejec[4];
                          $cont_ptto_ejec_otros=$cont_ptto_ejec_otros+$ptto_ejec[5];
                          $cont_ptto_ejec_total=$cont_ptto_ejec_total+($ptto_ejec[4]+$ptto_ejec[5]);

                          $cont_ejec_gadlp=$cont_ejec_gadlp+$eg;
                          $cont_ejec_otros=$cont_ejec_otros+$eo;
                          $cont_ejec_total=$cont_ejec_total+$et;

                          $cont_saldoejec_gadlp=$cont_saldoejec_gadlp+(($ptto_mod[4]-$ptto_ejec[4]));
                          $cont_saldoejec_otros=$cont_saldoejec_otros+($ptto_mod[5]-$ptto_ejec[5]);
                          $cont_saldoejec_total=$cont_saldoejec_total+($ptto_mod[3]-($ptto_ejec[4]+$ptto_ejec[5]));
                          /*---------------------------------------------------------------*/
                    }
                    $html .= '
                    <tr>
                        <th style="width:1%;" bgcolor="#000000">TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_cant_continuo.'</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_cant_nuevo.'</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_cant_total.'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_inicial, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_mod, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_vigente_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_vigente_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_vigente_total, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_ejec_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_ejec_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_ptto_ejec_total, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_ejec_gadlp.'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_ejec_otros.'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.$cont_ejec_total.'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_saldoejec_gadlp, 2, ',', '.').' Bs.</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_saldoejec_otros, 2, ',', '.').' Bs.</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($cont_saldoejec_total, 2, ',', '.').' Bs.</th>
                      </tr>
                  </tbody>
                  </table>';
                }

                $html .= '
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }
  /*=========================================================================================================================*/
    /*==================== REPORTE GERENCIAL-REPORTE INSTITUCIONAL-REPORTE DE PROGRAMA DE INVERSION PUBLICA POR UNIDAD =================*/
    public function reporte_ejecucion_inversion_publica_unidad($uni_id,$tp,$p1,$p2,$p3,$p4)
    { 
      $this->load->model('menu_modelo');
      $enlaces=$this->menu_modelo->get_Modulos(7);
      $data['enlaces'] = $enlaces;
        
      for($i=0;$i<count($enlaces);$i++)
      {
        $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
      }
      $data['subenlaces'] = $subenlaces;

      if($tp==1){$titulo='PROYECTOS DE INVERSI&Oacute;N P&Uacute;BLICA ';}
      elseif($tp==2){$titulo='PROGRAMAS RECURRENTES';}
      elseif($tp==3){$titulo='PROGRAMAS NO RECURRENTES';}
      elseif($tp==4){$titulo='ACCI&Oacute;N DE FUNCIONAMIENTO';}
      $data['titulo'] = $titulo;
      $data['unidad'] = $this->model_reporte->get_unidad($uni_id);
      $data['p1'] = $p1;
      $data['p2'] = $p2;
      $data['p3'] = $p3;
      $data['p4'] = $p4;
      $data['tp'] = $tp;


        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
        if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}

        $data['pr1']=$pr1;
        $data['pr2']=$pr2;
        $data['pr3']=$pr3;
        $data['pr4']=$pr4;

      $meses=$this->get_mes($this->session->userdata("mes"));
      $data['mes'] = $meses[1];
      $data['dias'] = $meses[2];

      $proyectos = $this->model_reporte->proyecto_ue($uni_id,$this->session->userdata('gestion'),$tp);//lista de proyectos
      $tabla1 = ''; $nro=1;
        foreach($proyectos  as $pr)
        {   
            $tabla1 .= '<tr>';
            $tabla1 .= '<td>'.$nro.'</td>';
            $tabla1 .= '<td>'.$pr['proy_nombre'].'</td>';
            $tabla1 .= '<td>'.$pr['fase'].'</td>';
            $nc=$this->model_faseetapa->calcula_nc2($pr['pfec_fecha_inicio']); //// calcula nuevo/continuo
            $tabla1 .= '<td>'.$nc.'</td>';
            $tabla1 .= '<td>'.number_format($pr['ptto'], 2, ',', '.').'</td>';
            $ptto_mod=$this->ptto_mod_aprobadas_presupuesto_vigente_proy($pr['proy_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"));
            $tabla1 .= '<td>'.number_format($ptto_mod[1], 2, ',', '.').'</td>'; /// PTTO INICIAL
            $tabla1 .= '<td>'.number_format($ptto_mod[2], 2, ',', '.').'</td>';  /// MODIFICACIONES
/*            $tabla1 .= '<td>'.number_format($ptto_mod[3], 2, ',', '.').'</td>';  /// VIGENTE*/
            $tabla1 .= '<td>'.number_format($ptto_mod[4], 2, ',', '.').'</td>'; /// PTTO VIGENTE CIATZ
            $tabla1 .= '<td>'.number_format($ptto_mod[5], 2, ',', '.').'</td>'; /// PTTO VIGENTE OTROS
            $tabla1 .= '<td bgcolor=#ddead6>'.number_format($ptto_mod[6], 2, ',', ' ').'</td>'; /// PTTO VIGENTE TOTAL
            $ptto_dev=$this->ptto_ejecutado_devengado($pr['proy_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"));
            $tabla1 .= '<td>'.number_format($ptto_dev[1], 2, ',', '.').'</td>'; /// PTTO EJECUTADO CIATZ (DEVENGADO)
            $tabla1 .= '<td>'.number_format($ptto_dev[2], 2, ',', '.').'</td>'; /// PTTO EJECUTADO OTROS (DEVENGADO)
            $tabla1 .= '<td bgcolor=#ddead6>'.number_format($ptto_dev[3], 2, ',', ' ').'</td>'; /// PTTO EJECUTADO TOTAL (DEVENGADO)
            if($ptto_mod[4]==0){$eptto=$ptto_dev[1];}else{$eptto=(($ptto_dev[1]/$ptto_mod[4])*100);}
            $tabla1 .= '<td>'.round($eptto,2).'%</td>';       //// EJEC PTTO CIATZ %
            if($ptto_mod[5]==0){$eotros=$ptto_dev[2];}else{$eotros=(($ptto_dev[2]/$ptto_mod[5])*100);}
            $tabla1 .= '<td>'.round($eotros,2).'%</td>';      //// EJEC PTTO OTROS %
            if($ptto_mod[6]==0){$etotal=$ptto_dev[3];}else{$etotal=(($ptto_dev[3]/$ptto_mod[6])*100);}
            $tabla1 .= '<td bgcolor=#ddead6>'.round($etotal,2).'%</td>';  //// EJEC PTTO TOTAL %
            $tabla1 .= '<td>'.number_format(($ptto_mod[4]-$ptto_dev[1]), 2, ',', '.').'</td>';
            $tabla1 .= '<td>'.number_format(($ptto_mod[5]-$ptto_dev[2]), 2, ',', '.').'</td>';
            $tabla1 .= '<td>'.number_format(($ptto_mod[6]-$ptto_dev[3]), 2, ',', '.').'</td>';
            $afin=$this->avance_financiero($pr['proy_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"));
            $tabla1 .= '<td>'.$afin[3].' %</td>';
            $afisico=$this->avance_fisico_gestiones($pr['proy_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"));
            $tabla1 .= '<td>'.round($afisico[1],2).' %</td>';
            $estado=$this->estado_proyecto($pr['pfec_id']);
            $tabla1 .= '<td><center>'.$estado[1].'</center></td>';
            $tabla1 .= '<td>'.$this->provincias_proyecto($pr['proy_id']).'</td>';
            $tabla1 .= '</tr>';

            $nro++;
        }

        $data['proyecto'] = $tabla1;
      //load the view
      $this->load->view('admin/reportes/ejecucion_ip/rep_ei_publica/reporte_detallado/unidad', $data);
    }
    /*=================================================================================================================*/
  /*======================== PTTO - MOD APROBADAS - PRESUPUESTO VIGENTE DEL PROYECTO X =============================*/
    public function ptto_mod_aprobadas_presupuesto_vigente_proy($proy_id,$gestion,$mes)
    {

      $valor[1]=0;$valor[2]=0;$valor[3]=0;$valor[4]=0;$valor[5]=0;$valor[6]=0;$valor[7]=0;$valor[8]=0;$valor[9]=0;
       
        $proyectos_ejec=$this->model_reporte->lista_proy_ejec($gestion,$mes,$proy_id);//proyectos en ejecucion
        
        $suma_ptto=0;$suma_mod=0;$suma_vgente=0;
        $suma_gadlpz=0;$suma_otros=0; $devengado_gadlpz=0; $devengado_otros=0;$devengado_total=0;
        
        foreach($proyectos_ejec  as $pe)
        {
          $suma_ptto=$suma_ptto+$pe['pem_ppto_inicial']; //// suma presupuesto inicial
          $suma_mod=$suma_mod+$pe['pem_modif_aprobadas']; //// suma modificaciones
          $suma_vgente=$suma_vgente+$pe['pem_ppto_vigente']; ///// suma vigentes

          if(($pe['ff_codigo']==20 || $pe['ff_codigo']==41) && ($pe['of_codigo']==220 || $pe['of_codigo']==230 || $pe['of_codigo']==116 || $pe['of_codigo']==117 || $pe['of_codigo']==119))
          {
            $suma_gadlpz=$suma_gadlpz+$pe['pem_ppto_vigente']; //// suma presupuesto vigente CIATz
            $devengado_gadlpz=$devengado_gadlpz+$pe['pem_devengado']; /// suma presupuesto ejecutado gadlpz (devengado)
            $devengado_total=$devengado_total+$pe['pem_devengado']; /// suma presupesto ejecutado total (devengado)
          }
          else
          {
            $suma_otros=$suma_otros+$pe['pem_ppto_vigente']; //// suma presupuesto vigente otros
            $devengado_otros=$devengado_otros+$pe['pem_devengado']; /// suma presupuesto ejecutado otros (devengado)
            $devengado_total=$devengado_total+$pe['pem_devengado']; /// suma presupesto ejecutado total (devengado)
          }

        }

        $valor[1]=$suma_ptto; /// Ptto Inicial
        $valor[2]=$suma_mod;  /// modificaciones
        $valor[3]=$suma_vgente; /// Vigente

        $valor[4]=$suma_gadlpz; /// presupuesto vigente CIATZ
        $valor[5]=$suma_otros; /// presupuesto vigente OTROS
        $valor[6]=$suma_vgente; /// presupuesto vigente TOTAL

        $valor[7]=$devengado_gadlpz; /// presupesto ejecutado CIATZ (devengado)
        $valor[8]=$devengado_otros;  /// presupesto ejecutado OTROS (devengado)
        $valor[9]=$devengado_total;  /// presupesto ejecutado TOTAL (devengado)

      return $valor;
    }

    /*======================= SUMATORIA DE TODO LOS DEVENGADOS ==================================*/
    public function ptto_ejecutado_devengado($proy_id,$gestion,$mes)
    {
      $valor_mes=$mes;
      $ejec_gadlpz=0;$ejec_otros=0;$ejec_total=0;
      $valor[1]=0;$valor[2]=0;$valor[3]=0;
      
      for($i=$valor_mes; $i>0; $i=$i-1)
      {
        $proyecto=$this->ptto_mod_aprobadas_presupuesto_vigente_proy($proy_id,$gestion,$i);
        
        $ejec_gadlpz=$ejec_gadlpz+$proyecto[7];
        $ejec_otros=$ejec_otros+$proyecto[8];
        $ejec_total=$ejec_total+$proyecto[9];
      }

      $valor[1]=$ejec_gadlpz;
      $valor[2]=$ejec_otros;
      $valor[3]=$ejec_total;

      return $valor;
    }

    /*======================= ESTADO DEL PROYECTO ==================================*/
    public function estado_proyecto($pfec_id)
    {
     $valor[1]='null';$valor[2]='null';
     $fase_proy=$this->model_reporte->fase_ejecucion($pfec_id);// estado actual del proyecto
     if(count($fase_proy)!=0)
     {
      $valor[1]=strtoupper($fase_proy[0]['ep_descripcion']);
      $valor[2]=strtoupper($fase_proy[0]['st_descripcion']);
     }
      return $valor;
    }

    /*======================= ESTADO DEL PROYECTO ==================================*/
    public function provincias_proyecto($proy_id)
    {
      $provincias=$this->model_proyecto->proy_prov($proy_id);// provincias
      $tabla = '';
      if(count($provincias)!=0)
      {
        $tabla .= '<table >';
        foreach($provincias  as $prov)
        {
          $tabla .= '<tr>';
          $tabla .= '<td>'.strtoupper($prov['prov_provincia']).'</td>';
          $tabla .= '</tr>';
        }
        $tabla .= '</table>';
      }
       
      return $tabla; 
    }
    /*======================= AVANCE FINANCIERO ANUAL ==================================*/
    public function avance_financiero($proy_id,$gestion,$mes)
    {
        $pmes[0]='monto';
        $pmes[1]='enero';
        $pmes[2]='febrero';
        $pmes[3]='marzo';
        $pmes[4]='abril';
        $pmes[5]='mayo';
        $pmes[6]='junio';
        $pmes[7]='julio';
        $pmes[8]='agosto';
        $pmes[9]='septiembre';
        $pmes[10]='octubre';
        $pmes[11]='noviembre';
        $pmes[12]='diciembre';

        $emes[1]='ejec1';
        $emes[2]='ejec2';
        $emes[3]='ejec3';
        $emes[4]='ejec4';
        $emes[5]='ejec5';
        $emes[6]='ejec6';
        $emes[7]='ejec7';
        $emes[8]='ejec8';
        $emes[9]='ejec9';
        $emes[10]='ejec10';
        $emes[11]='ejec11';
        $emes[12]='ejec12';


        for($i=0;$i<=12;$i++)
        {
          $pfin[$i]=0;
          $pafin[$i]=0;
          $pafin2[$i]=0;

          $efin[$i]=0;
          $eafin[$i]=0;
          $eafin2[$i]=0;

          $eficacia[$i]=0;
          $valor[$i]=0;
        }
        $fase = $this->model_faseetapa->get_id_fase($proy_id);//DATOS DE LA FASE
        
        if ($fase[0]['pfec_ejecucion'] == 1) ///// EJECUCION DIRECTA
        {
            $prfin= $this->model_reporte->programado_financiero_directo($gestion, $proy_id);
        } 
        elseif ($fase[0]['pfec_ejecucion'] == 2) ////// EJECUCION DELEGADA
        {
            $prfin= $this->model_reporte->programado_financiero_delegado($gestion, $proy_id);
        }

        if(count($prfin)!=0)
        {
          for($i=0;$i<=12;$i++)
          {
            $pfin[$i]=$prfin[0][$pmes[$i]];
          }
        }

        $ejfin= $this->model_reporte->ejecutado_financiero($proy_id, $gestion); //// ejecutado Financiero
        if(count($ejfin)!=0)
        {
          for($i=1;$i<=12;$i++)
          {
            $efin[$i]=$ejfin[0][$emes[$i]];
          }
        }

        $suma_pfin=0; $suma_efin=0;
        for($i=1;$i<=12;$i++)
        {
          $suma_pfin=$suma_pfin+$pfin[$i];
          $pafin[$i]=$suma_pfin; ///// Programado acumulado
          
          $suma_efin=$suma_efin+$efin[$i];
          $eafin[$i]=$suma_efin; ///// Ejecutado acumulado
          

          if($pfin[0]!=0)
          {
           $pafin2[$i]=round((($pafin[$i]/$pfin[0])*100),2); ////// Programado Acumulado %
           $eafin2[$i]=round((($eafin[$i]/$pfin[0])*100),2); ////// Ejecutado Acumulado % 
          }
        }


          $valor[0]=$pfin[0]; ///// programado total
          $valor[1]=$pfin[$mes]; ///// programado mes
          $valor[2]=$pafin[$mes]; ///// programado acumulado mes
          $valor[3]=$pafin2[$mes]; ///// programado acumulado % mes
          $valor[4]=$efin[$mes]; ///// Ejecutado mes
          $valor[5]=$eafin[$mes]; ///// Ejecutado Acumulado mes
          $valor[6]=$eafin2[$mes]; ///// Ejecutado Acumulado % mes
          if($valor[2]!=0 && $valor[5]!=0){$valor[7]=(($valor[5]/$valor[2])*100);} //// % Ejecutado Acumulado/Programado Acumulado
          if($fase[0]['pfec_ptto_fase']!=0 && $valor[5]!=0){$valor[8]=(($valor[5]/$fase[0]['pfec_ptto_fase'])*100);} //// % Ejecucion/Ptto Gestion fase Actual 

      return $valor; 
    }
/*================================== REPORTE POR UNIDAD SELECCIONADA===========================================*/
    public function pdf_reporte_ejecucion_inversion_publica_unidad($uni_id,$tp)
    {  
      $gestion=$this->session->userdata('gestion');
        
        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        if($tp==1){$titulo='PROYECTOS DE INVERSI&Oacute;N P&Uacute;BLICA ';}
        elseif($tp==2){$titulo='PROGRAMAS RECURRENTES';}
        elseif($tp==3){$titulo='PROGRAMAS NO RECURRENTES';}
        elseif($tp==4){$titulo='ACCI&Oacute;N DE FUNCIONAMIENTO';}

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>'.$titulo.'</b> - EJECUCI&Oacute;N F&Iacute;SICO FINANCIERA (AL '.$dias.' de '.$mes.' DE '.$gestion.' ) <br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="80px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                  <b>EJECUCI&Oacute;N - '.$titulo.'</b>
                  <table class="datos_principales" style="table-layout:fixed;">
                  <thead>
                    <tr>
                      <th bgcolor="#000000"></th>
                      <th bgcolor="#000000"></th>
                      <th bgcolor="#000000"></th>
                      <th bgcolor="#000000"></th>
                      <th bgcolor="#000000"></th>
                      <th bgcolor="#000000"></th>
                      <th bgcolor="#000000"></th>
                      <th colspan="3" bgcolor="#000000">PRESUPUESTO VIGENTE '.$this->session->userdata("gestion").'</th>
                      <th colspan="3" bgcolor="#000000">PRESUPUESTO EJECUTADO '.$this->session->userdata("gestion").'</th>
                      <th colspan="3" bgcolor="#000000">EJECUCI&Oacute;N DEL PTTO (%)</th>
                      <th colspan="3" bgcolor="#000000">SALDO '.$this->session->userdata("gestion").'</th>
                      <th bgcolor="#000000"></th>
                      <th bgcolor="#000000"></th>
                      <th bgcolor="#000000"></th>
                      <th bgcolor="#000000"></th>
                     
                    </tr>
                    <tr>
                      <th style="width:1%;" bgcolor="#000000">N°</th>
                      <th style="width:1%;" bgcolor="#000000">PROYECTOS</th>
                      <th style="width:1%;" bgcolor="#000000">FASE</th>
                      <th style="width:1%;" bgcolor="#000000">ETAPA</th>
                      <th style="width:1%;" bgcolor="#000000">COSTO_TOTAL<BR>PROY.</th>

                      <th style="width:1%;" bgcolor="#000000">PTTO_INICIAL<BR>'.$this->session->userdata("gestion").'</th>
                      <th style="width:1%;" bgcolor="#000000">MODIFICACIONES<BR>'.$this->session->userdata("gestion").'</th>


                      <th style="width:1%;" bgcolor="#000000">PTTO_'.$this->session->userData('entidad_sigla').'</th>
                      <th style="width:1%;" bgcolor="#000000">PTTO_OTROS</th>
                      <th style="width:1%;" bgcolor="#000000">PTTO_TOTAL</th>

                      <th style="width:1%;" bgcolor="#000000">PTTO_'.$this->session->userData('entidad_sigla').'</th>
                      <th style="width:1%;" bgcolor="#000000">PTTO_OTROS</th>
                      <th style="width:1%;" bgcolor="#000000">PTTO_TOTAL</th>

                      <th style="width:1%;" bgcolor="#000000">'.$this->session->userData('entidad_sigla').'</th>
                      <th style="width:1%;" bgcolor="#000000">OTROS</th>
                      <th style="width:1%;" bgcolor="#000000">TOTAL</th>

                      <th style="width:1%;" bgcolor="#000000">PTTO_'.$this->session->userData('entidad_sigla').'</th>
                      <th style="width:1%;" bgcolor="#000000">PTTO_OTROS</th>
                      <th style="width:1%;" bgcolor="#000000">PTTO_TOTAL</th>

                      <th style="width:1%;" bgcolor="#000000">AVANCE FIN.<BR>ACUMULADO(%)</th>
                      <th style="width:1%;" bgcolor="#000000">AVANCE FISICO<BR>ACUMULADO(%)</th>

                      <th style="width:1%;" bgcolor="#000000">ESTADO_ACTUAL</th>
                      <th style="width:1%;" bgcolor="#000000">PROVINCIA</th>
                    </tr>
                    </thead>
                    <tbody id="bdi">';
                     $proyectos = $this->model_reporte->proyecto_ue($uni_id,$this->session->userdata('gestion'),$tp);//lista de proyectos
                      $costo_total_proy=0;$ptto_inicial=0;$modificaciones=0;$ptto_vigente=0;$ptto_vig_gadlp=0;$ptto_vig_otros=0;$ptto_vig_total=0;
                      $ptto_ejec_gadlp=0;$ptto_ejec_otros=0;$ptto_ejec_total=0;
                      $ptto_ejec_gadlp_porcentual=0;$ptto_ejec_otros_porcentual=0;$ptto_ejec_total_porcentual=0;
                      $saldo_gadlp=0;$saldo_otros=0;$saldo_total=0;
                      $nro=1;
                        foreach($proyectos  as $pr)
                        {   
                            $html .= '<tr>';
                            $html .= '<td>'.$nro.'</td>';
                            $html .= '<td>'.$pr['proy_nombre'].'</td>';
                            $html .= '<td>'.$pr['fase'].'</td>';
                            $nc=$this->model_faseetapa->calcula_nc2($pr['pfec_fecha_inicio']); //// calcula nuevo/continuo
                            $html .= '<td>'.$nc.'</td>';
                            $html .= '<td>'.number_format($pr['ptto'], 2, ',', '.').'</td>';
                            $ptto_mod=$this->ptto_mod_aprobadas_presupuesto_vigente_proy($pr['proy_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"));
                            $html .= '<td>'.number_format($ptto_mod[1], 2, ',', '.').'</td>'; /// PTTO INICIAL
                            $html .= '<td>'.number_format($ptto_mod[2], 2, ',', '.').'</td>';  /// MODIFICACIONES
                /*            $tabla1 .= '<td>'.number_format($ptto_mod[3], 2, ',', '.').'</td>';  /// VIGENTE*/
                            $html .= '<td>'.number_format($ptto_mod[4], 2, ',', '.').'</td>'; /// PTTO VIGENTE CIATZ
                            $html .= '<td>'.number_format($ptto_mod[5], 2, ',', '.').'</td>'; /// PTTO VIGENTE OTROS
                            $html .= '<td bgcolor=#ddead6>'.number_format($ptto_mod[6], 2, ',', ' ').'</td>'; /// PTTO VIGENTE TOTAL
                            $ptto_dev=$this->ptto_ejecutado_devengado($pr['proy_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"));
                            $html .= '<td>'.number_format($ptto_dev[1], 2, ',', '.').'</td>'; /// PTTO EJECUTADO CIATZ (DEVENGADO)
                            $html .= '<td>'.number_format($ptto_dev[2], 2, ',', '.').'</td>'; /// PTTO EJECUTADO OTROS (DEVENGADO)
                            $html .= '<td bgcolor=#ddead6>'.number_format($ptto_dev[3], 2, ',', ' ').'</td>'; /// PTTO EJECUTADO TOTAL (DEVENGADO)
                            if($ptto_mod[4]==0){$eptto=$ptto_dev[1];}else{$eptto=(($ptto_dev[1]/$ptto_mod[4])*100);}
                            $html .= '<td>'.round($eptto,2).'%</td>';       //// EJEC PTTO CIATZ %
                            if($ptto_mod[5]==0){$eotros=$ptto_dev[2];}else{$eotros=(($ptto_dev[2]/$ptto_mod[5])*100);}
                            $html .= '<td>'.round($eotros,2).'%</td>';      //// EJEC PTTO OTROS %
                            if($ptto_mod[6]==0){$etotal=$ptto_dev[3];}else{$etotal=(($ptto_dev[3]/$ptto_mod[6])*100);}
                            $html .= '<td bgcolor=#ddead6>'.round($etotal,2).'%</td>';  //// EJEC PTTO TOTAL %
                            $html .= '<td>'.number_format(($ptto_mod[4]-$ptto_dev[1]), 2, ',', '.').'</td>';
                            $html .= '<td>'.number_format(($ptto_mod[5]-$ptto_dev[2]), 2, ',', '.').'</td>';
                            $html .= '<td>'.number_format(($ptto_mod[6]-$ptto_dev[3]), 2, ',', '.').'</td>';
                            $afin=$this->avance_financiero($pr['proy_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"));
                            $html .= '<td>'.$afin[3].' %</td>';
                            $afisico=$this->avance_fisico_gestiones($pr['proy_id'],$this->session->userdata("gestion"),$this->session->userdata("mes"));
                            $html .= '<td>'.round($afisico[1],2).' %</td>';
                            $estado=$this->estado_proyecto($pr['pfec_id']);
                            $html .= '<td><center>'.$estado[1].'</center></td>';
                            $html .= '<td>'.$this->provincias_proyecto($pr['proy_id']).'</td>';
                            $html .= '</tr>';

                            $costo_total_proy=$costo_total_proy+$pr['ptto'];
                            $ptto_inicial=$ptto_inicial+$ptto_mod[1];
                            $modificaciones=$modificaciones+$ptto_mod[2];
                            $ptto_vigente=$ptto_vigente+$ptto_mod[3];
                            $ptto_vig_gadlp=$ptto_vig_gadlp+$ptto_mod[4];
                            $ptto_vig_otros=$ptto_vig_otros+$ptto_mod[5];
                            $ptto_vig_total=$ptto_vig_total+$ptto_mod[6];
                            $ptto_ejec_gadlp=$ptto_ejec_gadlp+$ptto_dev[1];
                            $ptto_ejec_otros=$ptto_ejec_otros+$ptto_dev[2];
                            $ptto_ejec_total=$ptto_ejec_total+$ptto_dev[3];

                            $ptto_ejec_gadlp_porcentual=$ptto_ejec_gadlp_porcentual+$eptto;
                            $ptto_ejec_otros_porcentual=$ptto_ejec_otros_porcentual+$eotros;
                            $ptto_ejec_total_porcentual=$ptto_ejec_total_porcentual+$etotal;

                            $saldo_gadlp=$saldo_gadlp+($ptto_mod[4]-$ptto_dev[1]);
                            $saldo_otros=$saldo_otros+($ptto_mod[5]-$ptto_dev[2]);
                            $saldo_total=$saldo_total+($ptto_mod[6]-$ptto_dev[3]);
                            $nro++;
                        }
                      $html .= '
                      <tr>
                        <th style="width:1%;" bgcolor="#000000" colspan=4>TOTAL</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($costo_total_proy, 2, ',', '.').'</th>

                        <th style="width:1%;" bgcolor="#000000">'.number_format($ptto_inicial, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($modificaciones, 2, ',', '.').'</th>


                        <th style="width:1%;" bgcolor="#000000">'.number_format($ptto_vig_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($ptto_vig_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($ptto_vig_total, 2, ',', '.').'</th>

                        <th style="width:1%;" bgcolor="#000000">'.number_format($ptto_ejec_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($ptto_ejec_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($ptto_ejec_total, 2, ',', '.').'</th>

                        <th style="width:1%;" bgcolor="#000000">'.round($ptto_ejec_gadlp_porcentual,2).'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.round($ptto_ejec_otros_porcentual,2).'%</th>
                        <th style="width:1%;" bgcolor="#000000">'.round($ptto_ejec_total_porcentual,2).'%</th>

                        <th style="width:1%;" bgcolor="#000000">'.number_format($saldo_gadlp, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($saldo_otros, 2, ',', '.').'</th>
                        <th style="width:1%;" bgcolor="#000000">'.number_format($saldo_total, 2, ',', '.').'</th>

                        <th style="width:1%;" bgcolor="#000000"></th>
                        <th style="width:1%;" bgcolor="#000000"></th>

                        <th style="width:1%;" bgcolor="#000000"></th>
                        <th style="width:1%;" bgcolor="#000000"></th>
                      </tr>
                    </tbody>
                    </table>';

                $html .= '
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }
  /*=========================================================================================================================*/
    public function get_mes($mes_id)
    {
      $mes[1]='ENERO';
      $mes[2]='FEBRERO';
      $mes[3]='MARZO';
      $mes[4]='ABRIL';
      $mes[5]='MAYO';
      $mes[6]='JUNIO';
      $mes[7]='JULIO';
      $mes[8]='AGOSTO';
      $mes[9]='SEPTIEMBRE';
      $mes[10]='OCTUBRE';
      $mes[11]='NOVIEMBRE';
      $mes[12]='DICIEMBRE';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }

    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }
}