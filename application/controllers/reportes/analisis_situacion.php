<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Analisis_situacion extends CI_Controller 
{
    private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 8px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1.5px;
                text-align: center;
                font-size: 8px;
            }
        .mv{font-size:10px;}
        .siipp{width:180px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 9px;
        }
        .datos_principales {
            text-align: center;
            font-size: 11px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }
        .indi_desemp{
            font-size: 14px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:5px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 9px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .lista{
            text-align:left;
            padding-left: 8;
            list-style-type:square;
            margin:2px;
        }
    </style>';
    public function __construct ()
    {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->library('pdf2');
        $this->load->model('analisis_situacion/model_analisis_situacion');
    }
    public function pdf_analisis_situacion($id)
    {
        $poa_id = $id;
        $lista_foda = $this->model_analisis_situacion->get_foda_por_poa_id($poa_id);
        $gestion = $this->session->userdata('gestion');
        $datos = $this->model_analisis_situacion->get_datos_poa_para_pdf($poa_id,$gestion);
        $datos = $datos->row();
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> ANALISIS DE SITUACIÓN <br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <table class="datos_principales" style="table-layout:fixed;" border="1">
                    <tr>
                        <td style="text-align:left;">
                            <b>PROGRAMA : </b> '.$datos->aper_programa.''.$datos->aper_proyecto.''.$datos->aper_actividad.' - '.$datos->aper_descripcion.'
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">
                            <b>UNIDAD EJECUTORA: </b> '.$datos->uni_unidad.'
                        </td>
                    </tr>
                </table>
                <br>
                <div class="contenedor_principal">
                    <div class="titulo_dictamen"> INTERNOS </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">'.$this->analisis_interno($lista_foda).'</table>
                    </div>
                    <div class="titulo_dictamen"> EXTERNOS </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">'.$this->analisis_externo($lista_foda).'</table>
                    </div>
                    <table class="table_contenedor" style="margin-top: 10px;>
                        <tr>
                            <td class="fila_unitaria"> &nbsp;FIRMAS:</td>
                        </tr>
                    </table>
                    <table style="width: 80%;margin: 0 auto;margin-top: 100px;margin-bottom: 50px;">
                        <tr>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                        </tr>
                    </table>
                </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'portrait');
        $dompdf->render();
        $dompdf->stream("analisis_situacion.pdf", array("Attachment" => false));
    }
    public function analisis_interno($lista_foda)
    {
        if(count($lista_foda)>=1){
            $html_interno= '
                <tr class="collapse_t" style="background-color:#ac9fae;">
                    <td>#</td>
                    <td>TIPO FODA</td>
                    <td>ANALISIS</td>
                    <td>DESCRIPCIÓN</td>
                    <td>INCIDENCIA</td>
                </tr>
            ';
        }else{
            $html_interno= '
                <tr class="collapse_t">
                    <td>Sin Datos Registrados</td>
                </tr>
            ';
        }
        $n = 0;
        foreach ($lista_foda as $fila) {
            if($fila['tfoda_id']=='D' || $fila['tfoda_id']=='F'){
                $n++;
                $html_interno .='
                    <tr class="collapse_t">
                        <td>'.$n.'</td>
                        <td>'.$fila['tfoda_descripcion'].'</td>
                        <td>'.$fila['tfoda_analisis'].'</td>
                        <td>'.$fila['foda_variables'].'</td>
                        <td>'.$fila['foda_incidencia'].'</td>
                    </tr>
                ';
            }
        }
        return $html_interno;
    }
    public function analisis_externo($lista_foda)
    {
        if(count($lista_foda)>=1){
            $html_externo= '
                <tr class="collapse_t" style="background-color:#ac9fae;">
                    <td>#</td>
                    <td>TIPO FODA</td>
                    <td>ANALISIS</td>
                    <td>DESCRIPCIÓN</td>
                    <td>INCIDENCIA</td>
                </tr>
            ';
        }else{
            $html_externo= '
                <tr class="collapse_t">
                    <td>Sin Datos Registrados</td>
                </tr>
            ';
        }
        $n = 0;
        foreach ($lista_foda as $fila) {
            if($fila['tfoda_id']=='A' || $fila['tfoda_id']=='O'){
                $n++;
                $html_externo .='
                    <tr class="collapse_t">
                        <td>'.$n.'</td>
                        <td>'.$fila['tfoda_descripcion'].'</td>
                        <td>'.$fila['tfoda_analisis'].'</td>
                        <td>'.$fila['foda_variables'].'</td>
                        <td>'.$fila['foda_incidencia'].'</td>
                    </tr>
                ';
            }
        }
        return $html_externo;
    }

}