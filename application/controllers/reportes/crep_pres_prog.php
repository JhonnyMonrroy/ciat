<?php

//Reporte - Presupuesto programado
class Crep_pres_prog extends CI_Controller
{
    var $gestion;
    var $mes;

    function __construct()
    {
        parent:: __construct();
        $this->load->model('reportes/presupuesto/mreporte_pres_prog');
        $this->load->model('mantenimiento/mpoa');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(7);
        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
    }

    function index()
    {
        $data['lista_poa'] = $this->mpoa->lista_poa();
        $data['ruta_proy'] = 'rep/prog_lproy';
        $ruta = 'reportes/presupuesto/vlista_programas';
        $this->construir_vista($ruta, $data);
    }

    function lista_proyectos($programa)
    {
        $data['atras'] = site_url() . '/rep/pres_prog';
        $data['ruta'] = site_url() . '/rep/pres_prog_proy/'.$programa.'/';
        $data['lista_proy'] = $this->mreporte_pres_prog->lista_proyectos($programa, $this->gestion);
        $ruta = 'reportes/presupuesto/vlista_proyectos';
        $this->construir_vista($ruta, $data);
    }

    function presupuesto_programado($programa, $proy_id, $tipo_ejec)
    {
        if ($tipo_ejec == 1) {//directo
            $prog_pres = $this->mreporte_pres_prog->proy_prog_directo($proy_id, $this->gestion);
            $data_tabla = $this->pres_prog($prog_pres);
            $data['tabla_pres_prog'] = $data_tabla['tabla'];
            $data['footer'] = $data_tabla['footer'];
        } elseif ($tipo_ejec == 2) {//delegado
            $prog_pres = $this->mreporte_pres_prog->proy_prog_delegado($proy_id, $this->gestion);
            $data_tabla = $this->pres_prog($prog_pres);
            $data['tabla_pres_prog'] = $data_tabla['tabla'];
            $data['footer'] = $data_tabla['footer'];
        } else {//default
            $data['tabla_pres_prog'] = 'DATOS ERRONEOS';
            $data['footer'] = '';
        }
        $data['atras_main'] = site_url() . '/rep/pres_prog';
        $data['atras'] = site_url() . '/rep/prog_lproy/'.$programa;
        $ruta = 'reportes/presupuesto/vpres_prog_proy';
        $this->construir_vista($ruta, $data);
    }

    //presupuesto programado
    function pres_prog($prog_pres)
    {
        if (count($prog_pres) != 0) {
            $tabla = '';
            $cont = 1;
            //vector de sumas
            for ($i = 1; $i <= 13; $i++) {
                $suma[$i] = 0;
            }
            foreach ($prog_pres as $item) {
                $tabla .= '<tr>';
                $tabla .= '<td>' . $cont . '</td>';
                $tabla .= '<td>' . $item['par_codigo'] . '</td>';
                $tabla .= '<td>' . $item['par_nombre'] . '</td>';
                $tabla .= '<td>' . $item['ff_codigo'] . '</td>';
                $tabla .= '<td>' . $item['of_codigo'] . '</td>';
                $tabla .= '<td>' . $item['et_codigo'] . '</td>';
                $ppto_inicial = ($item['enero'] + $item['febrero'] + $item['marzo'] + $item['abril'] + $item['mayo'] + $item['junio'] + $item['julio'] + $item['agosto']
                    + $item['septiembre'] + $item['octubre'] + $item['noviembre'] + $item['diciembre']);
                $tabla .= '<td>' . number_format($ppto_inicial, 2, ',', '.') . '</td>';
                $tabla .= '<td>';//---------------------------------------------------
                $tabla .= '<table  class="table table-bordered">';
                $tabla .= '<thead>';
                $tabla .= '<tr>'; //cabecera
                for ($i = 1; $i <= 12; $i++) {
                    $tabla .= '<td style="width: 5px;background-color: #568A89;color: white">' . $this->get_mes($i) . '</td>';
                }
                $tabla .= '</tr>';
                $tabla .= '<tr>';
                $tabla .= '<td>' . number_format($item['enero'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['febrero'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['marzo'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['abril'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['mayo'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['junio'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['julio'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['agosto'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['septiembre'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['octubre'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['noviembre'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['diciembre'], 2, ',', '.') . '</td>';
                $tabla .= '</tr>';
                $tabla .= '<thead>';
                $tabla .= '</table>';
                $tabla .= '</td>';//---------------------------------------------------
                $tabla .= '</tr>';
                $suma[1] += $item['enero'];
                $suma[2] += $item['febrero'];
                $suma[3] += $item['marzo'];
                $suma[4] += $item['abril'];
                $suma[5] += $item['mayo'];
                $suma[6] += $item['junio'];
                $suma[7] += $item['julio'];
                $suma[8] += $item['agosto'];
                $suma[9] += $item['septiembre'];
                $suma[10] += $item['octubre'];
                $suma[11] += $item['noviembre'];
                $suma[12] += $item['diciembre'];
                $suma[13] += $ppto_inicial;
                $cont++;
            }
            $footer = '<th colspan="6"><center>SUMA TOTAL</center></th>';
            $footer .= '<th>' . number_format($suma[13], 2, ',', '.') . '</th>';
            $footer .= '<th>';
            $footer .= '<table class="table table-bordered">';
            $footer .= '<tr>';
            for ($i = 1; $i <= 12; $i++) {
                $footer .= '<td>' . number_format($suma[$i], 2, ',', '.') . '</td>';
            }
            $footer .= '</tr>';
            $footer .= '</table>';
            $footer .= '</th>';

            $data['tabla'] = $tabla;
            $data['footer'] = $footer;
        } else {
            //NO EXISTE DATOS
            $data['tabla'] = '';
            $data['footer'] = '';
        }
        return $data;
    }

    function get_mes($num)
    {
        $mes[1] = 'ENERO';
        $mes[2] = 'FEBRERO';
        $mes[3] = 'MARZO';
        $mes[4] = 'ABRIL';
        $mes[5] = 'MAYO';
        $mes[6] = 'JUNIO';
        $mes[7] = 'JULIO';
        $mes[8] = 'AGOSTO';
        $mes[9] = 'SEPTIEMBRE';
        $mes[10] = 'OCTUBRE';
        $mes[11] = 'NOVIEMBRE';
        $mes[12] = 'DICIEMBRE';
        return $mes[$num];
    }

    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'REPORTES';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }
}