<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Reporte_tipogasto extends CI_Controller { 
  public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf');
        $this->load->library('pdf2');

        $this->load->model('reportes/model_objetivo');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('programacion/model_reporte');
        $this->load->model('programacion/model_reporte_global');
        $this->load->model('programacion/model_reporte_tipogasto');
        $this->load->model('programacion/mreporte_proy');
        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);
        }else{
            redirect('/','refresh');
        }
    }
        /*============================== FICHA TECNICA CABECERA =================================*/
       private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 8px;
                color: #ffffff;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1px;
                text-align: center;
                font-size: 7px;
            }
        .mv{font-size:10px;}
        .siipp{width:200px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .datos_principales {
            text-align: center;
            font-size: 9px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }

        .indi_desemp{
            font-size: 14px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 10px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 1px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .lista{
            text-align:left;
            padding-left: 8;
            list-style-type:square;
            margin:2px;
        }
    </style>';
 
  /*=========================================================================================================================*/
  
    /*========= SELECCION DE OPCIONES - REPORTE INSTITUCIONAL =================*/
    public function seleccion_tipo_gasto()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        
        $this->load->view('admin/reportes/tipo_de_gasto/select_accion', $data);
    }
    /*========================================= VALIDAR SELECCION DE ACCIONES================================================================================*/
    public function validar_seleccion()
    { 
        if($this->input->server('REQUEST_METHOD') === 'POST') 
        {
            if($this->input->post('p1')=='on'){$p1=1;}else{$p1=0;}
            if($this->input->post('p2')=='on'){$p2=1;}else{$p2=0;}
            if($this->input->post('p3')=='on'){$p3=1;}else{$p3=0;}
            if($this->input->post('p4')=='on'){$p4=1;}else{$p4=0;}

            redirect('admin/rep/mis_acciones_tg/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'');
        }
    }

    /*--------------------------------------- EJECUCION PRESUPUESTARIA --------------------------------------*/
    public function ejec_ptto($uni_id,$tg_id,$sumatotal,$total,$p1,$p2,$p3,$p4)
    {
      $mes=$this->session->userdata("mes");
      $gestion=$this->session->userdata("gestion");

      for($i=0;$i<=10;$i++){$valor[$i]=0;}
      $accion = $this->model_reporte_tipogasto->proyectos_ue_tpgasto($uni_id,$tg_id,$p1,$p2,$p3,$p4);//lista de acciones en ejeucion
      $ptto_i=0;$ptto_mod=0;$ptto_v=0;$ptto_e=0;
      
      if(count($accion)!=0)
      { 
        foreach($accion  as $row)
        {
          $pr = $this->model_reporte_global->cant_presupuesto_ejecucion($row['proy_id'],$mes,$gestion); ///// valores financieros del mes actual
          if(count($pr)!=0)
          {
            $ptto_i=$ptto_i+$pr[0]['inicial']; //// suma presupuesto inicial
            $ptto_mod=$ptto_mod+$pr[0]['modificado']; //// suma presupuesto modificado
            $ptto_v=$ptto_v+$pr[0]['vigente']; //// suma presupuesto vigente
          }


          $pd_m=0;
          for($j=1;$j<=$mes;$j++)
          {
            $ejec = $this->model_reporte_global->cant_presupuesto_ejecucion($row['proy_id'],$j,$gestion); //// valor ejecuicon a los meses
            if(count($ejec)!=0)
            {
              $pd_m=$pd_m+$ejec[0]['ejecutado']; /// suma devengado por meses
            }
          }
          $ptto_e=$ptto_e+$pd_m; //// suma devengado

        }
      }
      
      $valor[1]=$ptto_i; //// PPTO INICIAL
      $valor[2]=$ptto_mod; //// PPTO MODIFICADO
      $valor[3]=$ptto_v; //// PPTO VIGENTE
      if($total!=0){$valor[4]=(($valor[3]/$total)*100);} /// % TIPO GASTO
      if($sumatotal!=0){$valor[0]=(($valor[3]/$sumatotal)*100);} /// % TIPO TOTAL
      $valor[5]=$ptto_e; //// PPTO EJECUTADO
      $valor[6]=$valor[3]-$valor[5]; //// PPTO SALDO
      if($valor[3]!=0){$valor[7]=round((($valor[5]/$valor[3])*100),2);} //// %E

      return $valor;
    }

    public function total_ejec_ptto($tg_id,$total,$p1,$p2,$p3,$p4)
    {
        for($i=0;$i<=10;$i++){$valor[$i]=0;}
        $uni_ejec = $this->model_reporte_tipogasto->unidades_ejecutoras($tg_id,$p1,$p2,$p3,$p4);
        $ptto_i=0;$ptto_mod=0;$ptto_v=0;$ptto_e=0;$p=0;

        foreach($uni_ejec  as $row)
        {
            $ptto=$this->ejec_ptto($row['uni_id'],$tg_id,0,0,$p1,$p2,$p3,$p4);
            $ptto_i=$ptto_i+$ptto[1];
            $ptto_mod=$ptto_mod+$ptto[2];
            $ptto_v=$ptto_v+$ptto[3];
            $p=$p+$ptto[4];
            $ptto_e=$ptto_e+$ptto[5];
        }

        $valor[1]=$ptto_i; //// PPTO INICIAL
        $valor[2]=$ptto_mod; //// PPTO MODIFICADO
        $valor[3]=$ptto_v; //// PPTO VIGENTE
        if($total!=0){$valor[4]=(($valor[3]/$total)*100);}
        $valor[5]=$ptto_e; //// PPTO EJECUTADO
        $valor[6]=$valor[3]-$valor[5]; //// PPTO SALDO
        if($valor[3]!=0){$valor[7]=round((($valor[5]/$valor[3])*100),2);} //// %E

      return $valor; 
    }

    public function suma_total_ejec_ptto($p1,$p2,$p3,$p4)
    {
        for($i=0;$i<=10;$i++){$valor[$i]=0;}
        
        $tp_gasto = $this->model_reporte_tipogasto->tip_gasto();
        $ptto_i=0;$ptto_mod=0;$ptto_v=0;$ptto_e=0;$p=0;

        foreach($tp_gasto  as $rowtp)
        {
            $ptto_total=$this->total_ejec_ptto($rowtp['tg_id'],0,$p1,$p2,$p3,$p4);
            $ptto_i=$ptto_i+$ptto_total[1];
            $ptto_mod=$ptto_mod+$ptto_total[2];
            $ptto_v=$ptto_v+$ptto_total[3];
            $p=$p+$ptto_total[4];
            $ptto_e=$ptto_e+$ptto_total[5];
        }

        $valor[1]=$ptto_i; //// PPTO INICIAL
        $valor[2]=$ptto_mod; //// PPTO MODIFICADO
        $valor[3]=$ptto_v; //// PPTO VIGENTE
        $valor[4]=$p; //// % P
        $valor[5]=$ptto_e; //// PPTO EJECUTADO
        $valor[6]=$valor[3]-$valor[5]; //// PPTO SALDO
        if($valor[3]!=0){$valor[7]=round((($valor[5]/$valor[3])*100),2);} //// %E

      return $valor; 
    }
    /*---------------------------------------------------------------------------------------------------------------*/

    /*====================================== LISTA DE ACCIONES SELECCIONADAS ========================================*/
    public function mis_acciones_seleccionadas($p1,$p2,$p3,$p4)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['p1'] = $p1; //// proyectos de inversion
        $data['p2'] = $p2; //// programas recurrentes
        $data['p3'] = $p3; //// programas no recurrentes
        $data['p4'] = $p4; //// accion de funcionamiento

        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $data['pr1']=$pr1;
        $data['pr2']=$pr2;
        $data['pr3']=$pr3;
        $data['pr4']=$pr4;

        $ptto_suma_total=$this->suma_total_ejec_ptto($p1,$p2,$p3,$p4);

        $tp_gasto = $this->model_reporte_tipogasto->tip_gasto();
        $tabla = '';
        foreach($tp_gasto  as $rowtp)
        {
            $tabla .= '<tr bgcolor=#bdd8ef>';
                $tabla .= '<td><b>'.$rowtp['tg_descripcion'].'</b></td>';
                $ptto_total=$this->total_ejec_ptto($rowtp['tg_id'],$ptto_suma_total[3],$p1,$p2,$p3,$p4);
                $tabla .= '<td><b>'.number_format($ptto_total[1], 2, ',', '.').'</b></td>'; /// INICIAL
                $tabla .= '<td><b>'.number_format($ptto_total[2], 2, ',', '.').'</b></td>'; /// MOD.
                $tabla .= '<td><b>'.number_format($ptto_total[3], 2, ',', '.').'</b></td>'; /// VIGENTE
                $tabla .= '<td><b>';
                if($ptto_total[3]!=0){$tabla .= '100 %';}else{$tabla .= '0 %';}
                $tabla .= '</b></td>';
                $tabla .= '<td><b>'.round($ptto_total[4],2).' %</b></td>';
                $tabla .= '<td><b>'.number_format($ptto_total[5], 2, ',', '.').'</b></td>';
                $tabla .= '<td><b>'.round($ptto_total[7],2).' %</b></td>';
                $tabla .= '<td><b>'.number_format($ptto_total[6], 2, ',', '.').'</b></td>';
            $tabla .= '</tr>';

            $uni_ejec = $this->model_reporte_tipogasto->unidades_ejecutoras($rowtp['tg_id'],$p1,$p2,$p3,$p4);
            foreach($uni_ejec  as $row)
            {
            $tabla .= '<tr>';
                $tabla .= '<td><b>'.$row['uni_unidad'].'</b></td>';
                $ptto=$this->ejec_ptto($row['uni_id'],$rowtp['tg_id'],$ptto_suma_total[3],$ptto_total[3],$p1,$p2,$p3,$p4);
                $tabla .= '<td>'.number_format($ptto[1], 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($ptto[2], 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($ptto[3], 2, ',', '.').'</td>';
                $tabla .= '<td>'.round($ptto[4],2).' %</td>';
                $tabla .= '<td>'.round($ptto[0],2).' %</td>';
                $tabla .= '<td>'.number_format($ptto[5], 2, ',', '.').'</td>';
                $tabla .= '<td>'.round($ptto[7],2).' %</td>';
                $tabla .= '<td>'.number_format($ptto[6], 2, ',', '.').'</td>';
            $tabla .= '</tr>';
            }    
        }

        $data['ptto_i'] = $ptto_suma_total[1];
        $data['ptto_m'] = $ptto_suma_total[2];
        $data['ptto_v'] = $ptto_suma_total[3];
        $data['ptto_e'] = $ptto_suma_total[5];
        $data['e'] = $ptto_suma_total[7];
        $data['ptto_s'] = $ptto_suma_total[6];

        $data['ttgasto'] = $tabla;
        $this->load->view('admin/reportes/tipo_de_gasto/acciones', $data);
    }
    /*---------------------- REPORTE EJECUCION PRESUPUESTARIA POR UNIDAD--------------------------*/
    public function rep_ejecucion_ptto_unidad_ejecutora($p1,$p2,$p3,$p4)
    {  
        $gestion=$this->session->userdata('gestion');
        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> RESUMEN DE EJECUCI&Oacute;N PRESUPUESTARIA POR TIPO DE GASTO<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>';

                  $html .= '
                  <font size=1><b>REPORTES DE : '.$pr1.''.$pr2.''.$pr3.''.$pr4.'</b></font>
                  <table class="datos_principales" style="table-layout:fixed;">
                    <tr>
                        <th bgcolor="#000000" style="width:20%;">TIPO DE GASTO/UNIDAD_EJECUTORA</th>
                        <th bgcolor="#000000" style="width:10%;">PRESUPUESTO INICIAL</th>
                        <th bgcolor="#000000" style="width:10%;">MODIFICACIONES APROBADAS</th>
                        <th bgcolor="#000000" style="width:10%;">PRESUPUESTO VIGENTE</th>
                        <th bgcolor="#000000" style="width:5%;">% TIPO DE GASTO</th>
                        <th bgcolor="#000000" style="width:5%;">% TIPO TOTAL</th>
                        <th bgcolor="#000000" style="width:10%;">PRESUPUESTO EJECUTADO</th>
                        <th bgcolor="#000000" style="width:5%;">% EJECUTADO</th>
                        <th bgcolor="#000000" style="width:10%;">SALDO POR EJECUTAR</th>
                    </tr>

                    <tbody id="bdi">';
                        $ptto_suma_total=$this->suma_total_ejec_ptto($p1,$p2,$p3,$p4);
                        $tp_gasto = $this->model_reporte_tipogasto->tip_gasto();
                        foreach($tp_gasto  as $rowtp)
                        {   
                        $html .= '<tr bgcolor=#bdd8ef>';
                            $html .= '<td><b>'.$rowtp['tg_descripcion'].'</b></td>';
                            $ptto_total=$this->total_ejec_ptto($rowtp['tg_id'],$ptto_suma_total[3],$p1,$p2,$p3,$p4);
                            $html .= '<td><b>'.number_format($ptto_total[1], 2, ',', '.').'</b></td>'; /// INICIAL
                            $html .= '<td><b>'.number_format($ptto_total[2], 2, ',', '.').'</b></td>'; /// MOD.
                            $html .= '<td><b>'.number_format($ptto_total[3], 2, ',', '.').'</b></td>'; /// VIGENTE
                            $html .= '<td><b>';
                            if($ptto_total[3]!=0){$html .= '100 %';}else{$html .= '0 %';}
                            $html .= '</b></td>';
                            $html .= '<td><b>'.round($ptto_total[4],2).' %</b></td>';
                            $html .= '<td><b>'.number_format($ptto_total[5], 2, ',', '.').'</b></td>';
                            $html .= '<td><b>'.round($ptto_total[7],2).' %</b></td>';
                            $html .= '<td><b>'.number_format($ptto_total[6], 2, ',', '.').'</b></td>';
                        $html .= '</tr>';

                            $uni_ejec = $this->model_reporte_tipogasto->unidades_ejecutoras($rowtp['tg_id'],$p1,$p2,$p3,$p4);
                            foreach($uni_ejec  as $row)
                            {
                            $html .= '<tr bgcolor=#fafafa>';
                                $html .= '<td><b>'.$row['uni_unidad'].'</b></td>';
                                $ptto=$this->ejec_ptto($row['uni_id'],$rowtp['tg_id'],$ptto_suma_total[3],$ptto_total[3],$p1,$p2,$p3,$p4);
                                $html .= '<td>'.number_format($ptto[1], 2, ',', '.').'</td>';
                                $html .= '<td>'.number_format($ptto[2], 2, ',', '.').'</td>';
                                $html .= '<td>'.number_format($ptto[3], 2, ',', '.').'</td>';
                                $html .= '<td>'.round($ptto[4],2).' %</td>';
                                $html .= '<td>'.round($ptto[0],2).' %</td>';
                                $html .= '<td>'.number_format($ptto[5], 2, ',', '.').'</td>';
                                $html .= '<td>'.round($ptto[7],2).' %</td>';
                                $html .= '<td>'.number_format($ptto[6], 2, ',', '.').'</td>';
                            $html .= '</tr>';
                            }
                        }
                           $html .= '<tr>';
                                $html .= '<th bgcolor="#000000">TOTAL GENERAL</th>';
                                $html .= '<th bgcolor="#000000">'.number_format($ptto_suma_total[1], 2, ',', '.').'</th>';
                                $html .= '<th bgcolor="#000000">'.number_format($ptto_suma_total[2], 2, ',', '.').'</th>';
                                $html .= '<th bgcolor="#000000">'.number_format($ptto_suma_total[3], 2, ',', '.').'</th>';
                                $html .= '<th bgcolor="#000000" colspan=2></th>';
                                $html .= '<th bgcolor="#000000">'.number_format($ptto_suma_total[5], 2, ',', '.').'</th>';
                                $html .= '<th bgcolor="#000000">'.round($ptto_suma_total[7],2).'</th>';
                                $html .= '<th bgcolor="#000000">'.number_format($ptto_suma_total[6], 2, ',', '.').'</th>';
                            $html .= '</tr>';
                      $html .= '

                    </tbody>
                    </table>';
                $html .= '
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }
    /*==================================================================================================================*/ 
    
  /*=========================================================================================================================*/
    public function get_mes($mes_id)
    {
      $mes[1]='Enero';
      $mes[2]='Febrero';
      $mes[3]='Marzo';
      $mes[4]='Abril';
      $mes[5]='Mayo';
      $mes[6]='Junio';
      $mes[7]='Julio';
      $mes[8]='Agosto';
      $mes[9]='Septiembre';
      $mes[10]='Octubre';
      $mes[11]='Noviembre';
      $mes[12]='Diciembre';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }
}