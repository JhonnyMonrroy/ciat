<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Reporte_w extends CI_Controller {
public $rol = array('1' => '4');
  public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf');
        $this->load->library('pdf2');
        $this->load->model('Users_model','',true);
        if($this->rolfun($this->rol)){
            $this->load->model('reportes/model_objetivo');
            $this->load->model('programacion/model_proyecto');
            $this->load->model('programacion/model_producto');
            $this->load->model('programacion/model_faseetapa');
            $this->load->model('programacion/model_componente');
            $this->load->model('programacion/model_reporte_global');
            $this->load->model('programacion/model_actividad');
            $this->load->model('ejecucion/model_ejecucion');
            $this->load->model('mantenimiento/mpoa');
            $this->load->model('programacion/model_reporte');
            $this->load->model('programacion/mreporte_proy');
            $this->load->model('menu_modelo');
            $this->load->model('programacion/insumos/minsumos');
            $this->load->model('programacion/insumos/minsumos_delegado');
            
            $this->load->model('reportes_tarija_das/mdas_complementario');
            $this->gestion = $this->session->userData('gestion');
            $this->mes = $this->session->userData('mes');
        }
        else{
            redirect('admin/dashboard');
        }
       
        }else{
            redirect('/','refresh');
        }
    }
    /*===================================== PROYECTOS APROBADOS ===========================================*/
    public function list_proyectos_aprobados()
    {
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['lista_aper_padres'] = $this->model_proyecto->list_prog();//lista de aperturas padres
        
        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $data['id_mes'] = $this->session->userdata("mes");
        $data['mes'] = $mes;
        //load the view
        redirect('admin/dashboard');
      //  $this->load->view('admin/reportes/reporte_excel/list_proy_ok', $data);
    }
 /*===================================== ALERTA TEMPRANA =============================================*/
 public function alerta_temprana_acciones()
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;

    $this->load->view('admin/reportes/menu_acciones', $data);
  }
 /*==============================================================================================================*/

 /*=========================================== ACCIONES =======================================================*/
 public function acciones()
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $data['lista_aper_padres'] = $this->mpoa->lista_poa();//lista de aperturas padres  

    $mes=$this->get_mes($this->session->userdata("mes"));
    $data['mes'] = $mes[1];
    $data['dias'] = $mes[2];

    $this->load->view('admin/reportes/acciones/avance_acciones', $data);
  }
 /*==============================================================================================================*/
/*=========================================== ACCIONES POR PROGRAMAS  =======================================================*/
 public function programas_acciones()
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $data['lista_aper_padres'] = $this->mpoa->lista_poa();//lista de aperturas padres  

    $mes=$this->get_mes($this->session->userdata("mes"));
    $data['mes'] = $mes[1];
    $data['dias'] = $mes[2];

    $this->load->view('admin/reportes/acciones/acciones_programas', $data);
  }
 /*==============================================================================================================*/
  /*=========================================== PRODUCTOS POR PROYECTOS  =======================================================*/
 public function proyectos_acciones($prog)
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $data['proyectos'] = $this->model_proyecto->programas_proyecto($prog,$this->session->userdata('gestion'));//lista de proyectos del programa
    $data['programa']=$prog.'00000000';
    
    $mes=$this->get_mes($this->session->userdata("mes"));
    $data['mes'] = $mes[1];
    $data['dias'] = $mes[2];

    $this->load->view('admin/reportes/acciones/acciones_proyectos', $data);
  }
 /*==============================================================================================================*/

  /*=========================================== PRODUCTOS A NIVEL INSTITUCIONAL =======================================================*/
 public function productos()
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $mes=$this->get_mes($this->session->userdata("mes"));
    $data['mes'] = $mes[1];
    $data['dias'] = $mes[2];
    $data['lista_aper_padres'] = $this->mpoa->lista_poa();//lista de aperturas padres 
    $this->load->view('admin/reportes/producto/avance_productos', $data);
  }
 /*==============================================================================================================*/
/*=========================================== PRODUCTOS POR PROGRAMAS  =======================================================*/
 public function programas_productos()
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $mes=$this->get_mes($this->session->userdata("mes"));
    $data['mes'] = $mes[1];
    $data['dias'] = $mes[2];
    $data['lista_aper_padres'] = $this->mpoa->lista_poa();//lista de aperturas padres  

    $this->load->view('admin/reportes/producto/productos_programas', $data);
  }
 /*==============================================================================================================*/
 /*=========================================== PRODUCTOS POR PROYECTOS  =======================================================*/
 public function proyectos_productos($prog)
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    
    $mes=$this->get_mes($this->session->userdata("mes"));
    $data['mes'] = $mes[1];
    $data['dias'] = $mes[2];

    $data['proyectos'] = $this->model_proyecto->programas_proyecto($prog,$this->session->userdata('gestion'));//lista de proyectos del programa
    $data['programa']=$prog.'00000000';
    $this->load->view('admin/reportes/producto/productos_proyectos', $data);
  }
 /*==============================================================================================================*/


  /*=========================================== REPORTE GERENCIAL =======================================================*/
    /*------------------------------- SELECCION DE PROYECTOS FISICA-----------------------------------*/
    public function seleccion_reporte_resumen_fis()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++){
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $this->load->view('admin/reportes/gerencial/fisica/select_operaciones', $data);
    }
    /*------------------------------- SELECCION DE PROYECTOS FINANCIERA-----------------------------------*/
    public function seleccion_reporte_resumen_fin()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++){
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $this->load->view('admin/reportes/gerencial/financiera/select_operaciones', $data);
    }
    public function seleccione_reporte(){
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;

        for($i=0;$i<count($enlaces);$i++){
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $this->load->view('admin/reportes/gerencial/select_reporte', $data);

    }
    /*=================================== VALIDAR SELECCION DE ACCIONES ========================================================*/
    public function validar_seleccion_resumen_fis_fin()
    { 
        if($this->input->server('REQUEST_METHOD') === 'POST') 
        {
            if($this->input->post('p1')=='on'){$p1=1;}else{$p1=0;}
            if($this->input->post('p2')=='on'){$p2=1;}else{$p2=0;}
            if($this->input->post('p3')=='on'){$p3=1;}else{$p3=0;}
            if($this->input->post('p4')=='on'){$p4=1;}else{$p4=0;}

            redirect('admin/rep/gerencial_proyectos/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'/'.$this->input->post('tp').'');   
        }
    }

    /*============================ VALIDAR SELECCION DE ACCIONES-CONTROL SOCIAL ===========================================*/
    public function validar_seleccion_resumen_fis_fin_control()
    { 
        if($this->input->server('REQUEST_METHOD') === 'POST') 
        {
            if($this->input->post('p1')=='on'){$p1=1;}else{$p1=0;}
            if($this->input->post('p2')=='on'){$p2=1;}else{$p2=0;}
            if($this->input->post('p3')=='on'){$p3=1;}else{$p3=0;}
            if($this->input->post('p4')=='on'){$p4=1;}else{$p4=0;}

            redirect('rep/gerencial_proyectos/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'/'.$this->input->post('tp').'');   
        }
    }

  public function reporte_gerencial_proyectos($p1,$p2,$p3,$p4,$tp)
  { 
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $data['lista_aper_padres'] = $this->mpoa->lista_poa();//lista de aperturas padres  

    $meses=$this->get_mes($this->session->userdata("mes"));
    $mes = $meses[1];
    $dias = $meses[2];
    $id_mes=$this->session->userdata("mes");
    $data['id_mes'] = $id_mes;
    $data['mes'] = $mes;
    $data['dias'] = $dias;

    $data['p1'] = $p1; //// proyectos de inversion
    $data['p2'] = $p2; //// programas recurrentes
    $data['p3'] = $p3; //// programas no recurrentes
    $data['p4'] = $p4; //// accion de funcionamiento

    $pr1='';$pr2='';$pr3='';$pr4='';
    if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
    if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
    if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
    if($p4==1){$pr4=' - OPERACI&Oacute;N DE FUNCIONAMIENTO';}
    
    $data['pr1']=$pr1;
    $data['pr2']=$pr2;
    $data['pr3']=$pr3;
    $data['pr4']=$pr4;

    $data['tp']=$tp;

    if($tp==1){
      $data['avance_fisico']=$this->avance_fisico_gerencial($id_mes,$p1,$p2,$p3,$p4); ///// AVANCE FISICO
      $this->load->view('admin/reportes/gerencial/fisica/gerencial_proyecto', $data);
    }
    elseif ($tp==2) {
      $data['avance_financiero']=$this->avance_financiero_gerencial($id_mes,$p1,$p2,$p3,$p4); ///// AVANCE FINANCIERO
      $this->load->view('admin/reportes/gerencial/financiera/gerencial_proyecto', $data);
    }
    else{
      redirect('admin/dashboard');
    }
    
  }


  public function reporte_aciones_mediano()
  { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++){
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $this->load->view('admin/reportes/gerencial/objetivos/select_periodo', $data);
    
  }


  public function reporte_aciones_mediano1()
  { 
    
    $enlaces = $this->menu_modelo->get_Modulos(7);
    $data['enlaces'] = $enlaces;
    for ($i = 0; $i < count($enlaces); $i++) {
        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $data['lista_aper_padres'] = $this->mpoa->lista_poa();//lista de aperturas padres  

    $meses=$this->get_mes($this->input->post('s1'));
    $mes = $meses[1];
    $dias = $meses[2];
    $id_mes=$this->session->userdata("mes");
    $data['id_mes'] = $id_mes;
    $data['mes'] = $mes;
    $data['dias'] = $dias;
    $gestion = $this->session->userdata('gestion');
    $n = $this->input->post('s1');

    $p1=1;$p2=3;$p3=5;$p4=1;
    $data['p1'] = $p1; //// proyectos de inversion
    $data['p2'] = $p2; //// programas recurrentes
    $data['p3'] = $p3; //// programas no recurrentes
    $data['p4'] = $p4; //// accion de funcionamiento

    $data['tp']=$n;

    $objetivos = $this->model_proyecto->reporte_objetivos_cp($gestion);
    $ebaja=0; $eriesgo=0; $emedia=0; $eatiempo=0;
    // var_dump($objetivos);
    foreach ($objetivos as $obje) {
        $acumula = 0;
        $acumule = 0;
        for ($j = 1; $j <= $n; $j++) {
            $resul = $this->model_proyecto->reporte_obtener_pg($j, $obje['o_id']);
            if(!empty($resul))
            {
                $acumula += $resul[0]["opm_fis"];
            }
            $resul = $this->model_proyecto->reporte_obtener_ej($j, $obje['o_id']);
            if(!empty($resul))
            {
                $acumule += $resul[0]["oem_fis"];
            }
        }
        if($acumula != 0)
        {
                $percent = (($acumule/$acumula)*100);
                if($percent < 25){
                    $ebaja++;
                }elseif($percent>=25 && $percent < 50){
                    $eriesgo++;
                }elseif($percent>=50 && $percent < 75){
                    $emedia++;
                }else{
                    $eatiempo++;
                }
        } else {
                $eatiempo++;
        }
    }

    $pr1='LISTA ';
    $pr2='DE';
    $pr3=' OBJETIVOS';
    
    $data['pr1']=$pr1;
    $data['pr2']=$pr2;
    $data['pr3']=$pr3;

    $valor[1]=$ebaja;
    $valor[2]=$eriesgo;
    $valor[3]=$emedia;
    $valor[4]=$eatiempo;
    $valor[6]=$ebaja+$eriesgo+$emedia+$eatiempo;
    $data['avance_financiero']=$valor;

    $this->load->view('admin/reportes/gerencial/objetivos/gerencial_objetivos_gestion', $data);
  }

  /*------------------- IMPRIMIR REPORTE ESTADOS ------------------*/
    public function reporte_avance_objetivos($p1,$p2,$p3,$p4)
    {
    
    $meses=$this->get_mes($p4);
    $mes = $meses[1];
    $dias = $meses[2];
    $id_mes=$this->session->userdata("mes");
    $data['id_mes'] = $id_mes;
    $data['mes'] = $mes;
    $data['dias'] = $dias;
    $gestion = $this->session->userdata('gestion');
    $n = $p4;

    $p1=1;$p2=3;$p3=5;$p4=1;
    $data['p1'] = $p1; //// proyectos de inversion
    $data['p2'] = $p2; //// programas recurrentes
    $data['p3'] = $p3; //// programas no recurrentes
    $data['p4'] = $p4; //// accion de funcionamiento

    $data['tp']=$n;
      
    $objetivos = $this->model_proyecto->reporte_objetivos_cp($gestion);
    $ebaja=0; $eriesgo=0; $emedia=0; $eatiempo=0;
    // var_dump($objetivos);
    foreach ($objetivos as $obje) {
        $acumula = 0;
        $acumule = 0;
        for ($j = 1; $j <= $n; $j++) {
            $resul = $this->model_proyecto->reporte_obtener_pg($j, $obje['o_id']);
            if(!empty($resul))
            {
                $acumula += $resul[0]["opm_fis"];
            }
            $resul = $this->model_proyecto->reporte_obtener_ej($j, $obje['o_id']);
            if(!empty($resul))
            {
                $acumule += $resul[0]["oem_fis"];
            }
        }
        if($acumula != 0)
        {
                $percent = (($acumule/$acumula)*100);
                if($percent < 25){
                    $ebaja++;
                }elseif($percent>=25 && $percent < 50){
                    $eriesgo++;
                }elseif($percent>=50 && $percent < 75){
                    $emedia++;
                }else{
                    $eatiempo++;
                }
        } else {
                $eatiempo++;
        }
    }

    $pr1='LISTA ';
    $pr2='DE';
    $pr3=' OBJETIVOS';
    
    $data['pr1']=$pr1;
    $data['pr2']=$pr2;
    $data['pr3']=$pr3;

    $valor[1]=$ebaja;
    $valor[2]=$eriesgo;
    $valor[3]=$emedia;
    $valor[4]=$eatiempo;
    $valor[6]=$ebaja+$eriesgo+$emedia+$eatiempo;
    $data['avance_financiero']=$valor;

    // var_dump($data);      die;
    
      // $data['avance_financiero']=$this->avance_financiero_gerencial($this->mes,$p1,$p2,$p3,$p4); ///// AVANCE FINANCIERO
      $this->load->view('admin/reportes/gerencial/objetivos/imprimir_reporte', $data);

    }


  /*------------------------------------------- CONTROL SOCIAL ---------------------------------------------*/
  public function reporte_gerencial_proyectos_control($p1,$p2,$p3,$p4,$tp)
  { 
    $data['lista_aper_padres'] = $this->mpoa->lista_poa();//lista de aperturas padres  

    $meses=$this->get_mes($this->session->userdata("mes"));
    $mes = $meses[1];
    $dias = $meses[2];
    $id_mes=$this->session->userdata("mes");
    $data['id_mes'] = $id_mes;
    $data['mes'] = $mes;
    $data['dias'] = $dias;

    $data['p1'] = $p1; //// proyectos de inversion
    $data['p2'] = $p2; //// programas recurrentes
    $data['p3'] = $p3; //// programas no recurrentes
    $data['p4'] = $p4; //// accion de funcionamiento

    $pr1='';$pr2='';$pr3='';$pr4='';
    if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
    if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
    if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
    if($p4==1){$pr4=' - OPERACI&Oacute;N DE FUNCIONAMIENTO';}
    
    $data['pr1']=$pr1;
    $data['pr2']=$pr2;
    $data['pr3']=$pr3;
    $data['pr4']=$pr4;

    $data['tp']=$tp;

    if($tp==1){
      $data['avance_fisico']=$this->avance_fisico_gerencial($id_mes,$p1,$p2,$p3,$p4); ///// AVANCE FISICO
      $this->load->view('admin/control_social/fisica/gerencial_proyecto', $data);
    }
    elseif ($tp==2) {
      $data['avance_financiero']=$this->avance_financiero_gerencial($id_mes,$p1,$p2,$p3,$p4); ///// AVANCE FINANCIERO
      $this->load->view('admin/control_social/financiera/gerencial_proyecto', $data);
    }
    else{
      redirect('/','refresh');
    }
    
  }
  
  /*----------------------------------------------- AVANCE FISICO -----------------------------------------*/
  public function avance_fisico_gerencial($id_mes,$p1,$p2,$p3,$p4)
  {  
      $ms[0]='programado_total';$ms[1]='enero';$ms[2]='febrero';$ms[3]='marzo';$ms[4]='abril';$ms[5]='mayo';$ms[6]='junio';
      $ms[7]='julio';$ms[8]='agosto';$ms[9]='septiembre';$ms[10]='octubre';$ms[11]='noviembre';$ms[12]='diciembre';

      $nro=1;$cont_alto_riesgo=0;$cont_riesgo=0; $cont_retraso=0; $cont_tiempo=0; $cont_sprogramar=0;

      $proyecto=$this->model_reporte_global->list_operaciones($p1,$p2,$p3,$p4);
      foreach($proyecto  as $proy)
      {
        $fase = $this->model_faseetapa->get_id_fase($proy['proy_id']);
        $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $meses=$años*12;
        if($this->model_proyecto->verif_proy($proy['proy_id'])!=0)
        {
          //  echo "PROY_ID ".$proy['proy_id']."---".$fase[0]['pfec_fecha_inicio']."-".$fase[0]['pfec_fecha_fin']."<br>";
            $componentes = $this->model_componente->componentes_id($fase[0]['id']);
            for($p=1;$p<=$meses;$p++){$cp[1][$p]=0; $cp[2][$p]=0; $cp[3][$p]=0;}
            foreach ($componentes as $rowc)
            {
              $productos = $this->model_producto->list_prod($rowc['com_id']);
              for($p=1;$p<=$meses;$p++){$pp[1][$p]=0; $pp[2][$p]=0;}
             // echo "COMPONENTE : ".$rowc['com_componente']."<br>";
              foreach ($productos as $rowp)
              {
                  $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                //  echo "PRODUCTO : ".$rowp['prod_producto']."<br>";
                  for($p=1;$p<=$meses;$p++){$ap[1][$p]=0; $ap[2][$p]=0;}
                  foreach ($actividad as $rowa)
                  {   
                    $variablep=1;$variablee=1; $v=12; $nro=1; $sump=0;$sume=0;
                    for($p=1;$p<=$meses;$p++){$a[1][$p]=0; $a[2][$p]=0;$a[3][$p]=0;$a[4][$p]=0;$a[5][$p]=0;$a[6][$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                    for($i=$fase[0]['pfec_fecha_inicio'];$i<=$fase[0]['pfec_fecha_fin'];$i++)
                    {
                      $aprog = $this->model_actividad->actividad_programado($rowa['act_id'],$i);
                      if(count($aprog)!=0){
                        for ($j=1; $j <=12 ; $j++) { 
                          $a[1][$variablep]=$aprog[0][$ms[$j]];
                          $variablep++;
                        }
                      }
                      else{
                        $variablep=($v*$nro)+1;
                      }

                      $aejec = $this->model_actividad->actividad_ejecutado($rowa['act_id'],$i);
                      if(count($aejec)!=0){
                        for ($j=1; $j <=12 ; $j++) { 
                          $a[4][$variablee]=$aejec[0][$ms[$j]];
                          $variablee++;
                        }
                      }
                      else{
                        $variablee=($v*$nro)+1;
                      }

                      $nro++;
                    }

                    for ($i=1; $i <=$meses ; $i++) { 
                      $sump=$sump+$a[1][$i];
                      $a[2][$i]=$sump+$rowa['act_linea_base'];
                      
                      if($rowa['act_meta']!=0)
                      {
                        $a[3][$i]=round(((($sump+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                      }
                      $ap[1][$i]=$ap[1][$i]+(($a[3][$i]*$rowa['act_ponderacion'])/100);

                      $sume=$sume+$a[4][$i];
                      $a[5][$i]=$sume+$rowa['act_linea_base'];

                      if($rowa['act_meta']!=0)
                      {
                        $a[6][$i]=round(((($sume+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                      }
                      $ap[2][$i]=$ap[2][$i]+(($a[6][$i]*$rowa['act_ponderacion'])/100);
                    }

                  }
 
                    for ($i=1; $i <=$meses ; $i++) {
                      $pp[1][$i]=$pp[1][$i]+(($ap[1][$i]*$rowp['prod_ponderacion'])/100);
                      $pp[2][$i]=$pp[2][$i]+(($ap[2][$i]*$rowp['prod_ponderacion'])/100);
                    } 
                      
                }
                for ($i=1; $i <=$meses ; $i++) {
                      $cp[1][$i]=$cp[1][$i]+(($pp[1][$i]*$rowc['com_ponderacion'])/100);
                      $cp[2][$i]=$cp[2][$i]+(($pp[2][$i]*$rowc['com_ponderacion'])/100);
                    }

              }

              for ($i=1; $i <=$meses ; $i++) {
                if($cp[1][$i]!=0){
                  $cp[3][$i]=round((($cp[2][$i]/$cp[1][$i])*100),1);
                }
              }

              for ($i=1; $i <=12 ; $i++) { 
                $tefic[1][$i]=0;$tefic[2][$i]=0;$tefic[3][$i]=0;
              }
              $i=1;$vmeses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;
              for($p=$fase[0]['pfec_fecha_inicio'];$p<=$fase[0]['pfec_fecha_fin'];$p++)
              {
                  $variable=0;
                  for($pos=$i;$pos<=$vmeses;$pos++)
                  {
                    $variable++;
                    $tefic[1][$variable]=$cp[1][$pos];
                    $tefic[2][$variable]=$cp[2][$pos];
                    $tefic[3][$variable]=$cp[3][$pos];
                  }
                  if($p==$this->session->userdata('gestion'))
                  {
                      if($tefic[3][$id_mes]>=0 & $tefic[3][$id_mes]<=25) /// Baja Ejecucion
                          $cont_alto_riesgo++;
                      if($tefic[3][$id_mes]>25 & $tefic[3][$id_mes]<=50) /// En Riesgo
                          $cont_riesgo++;  
                      if($tefic[3][$id_mes]>50 & $tefic[3][$id_mes]<=75) /// Mediana Ejecucion
                          $cont_retraso++;
                      if($tefic[3][$id_mes]>75 & $tefic[3][$id_mes]<=100) /// Ejecucion a tiempo
                          $cont_tiempo++;
                  }

                    $i=$vmeses+1;
                    $vmeses=$vmeses+12;
                    $gestion++; $nro++;
              }
          }
      }

      $suma=$cont_alto_riesgo+$cont_riesgo+$cont_retraso+$cont_tiempo;
      $valor[1]=$cont_alto_riesgo;
      $valor[2]=$cont_riesgo;
      $valor[3]=$cont_retraso;
      $valor[4]=$cont_tiempo;
      $valor[6]=$suma;
      
      return $valor;
  }

   /*----------------------------------------------- AVANCE FINANCIERO -----------------------------------------*/
  public function avance_financiero_gerencial($mes_id,$p1,$p2,$p3,$p4)
  {  
      $nro=1;$cont_alto_riesgo=0;$cont_riesgo=0; $cont_retraso=0; $cont_tiempo=0; $cont_sprogramar=0;                                    
      $proyecto=$this->model_reporte_global->proyectos_ue_global_total($p1,$p2,$p3,$p4);
      foreach($proyecto  as $row)
      {
          $ejec=0;
          if($row['pe_pv']!=0){
            $ejec=round((($row['pe_pe']/$row['pe_pv'])*100),2);
          }

          if($ejec>=0 & $ejec<=25) /// en riesgo alto
              $cont_alto_riesgo++;
          elseif($ejec>25 & $ejec<=50) /// en riesgo
              $cont_riesgo++;  
          elseif($ejec>50 & $ejec<=75) /// con retraso
              $cont_retraso++;
          elseif($ejec>75 & $ejec<=100) /// a tiempo
              $cont_tiempo++;
        }

        $suma=$cont_alto_riesgo+$cont_riesgo+$cont_retraso+$cont_tiempo;
        $valor[1]=$cont_alto_riesgo;
        $valor[2]=$cont_riesgo;
        $valor[3]=$cont_retraso;
        $valor[4]=$cont_tiempo;
        $valor[6]=$suma;
      return $valor;
  }

  /*------------------------------------------------- FISICA ------------------------------------------------------*/
    public function ver_reporte_gerencial_fis($tp,$p1,$p2,$p3,$p4,$tp_rep)
    { 
      $enlaces = $this->menu_modelo->get_Modulos(7);
      $data['enlaces'] = $enlaces;
      for ($i = 0; $i < count($enlaces); $i++) {
          $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
      }
      $data['subenlaces'] = $subenlaces;

      if($tp==1){$tip_rep='BAJA EJECUCI&Oacute;N : 0-25 %';$color='#ED0F1A';}
      if($tp==2){$tip_rep='EN RIESGO: 26-50 %';$color='#F8912F';}
      if($tp==3){$tip_rep='MEDIANA EJECUCI&Oacute;N: 51-75 %';$color='#EDE90F';}
      if($tp==4){$tip_rep='EJECUCI&Oacute;N A TIEMPO: 76-100 %';$color='#2ABE47';}

      $data['p1'] = $p1; //// proyectos de inversion
      $data['p2'] = $p2; //// programas recurrentes
      $data['p3'] = $p3; //// programas no recurrentes
      $data['p4'] = $p4; //// Operacion de funcionamiento

      $pr1='';$pr2='';$pr3='';$pr4='';
      if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
      if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
      if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
      if($p4==1){$pr4=' - OPERACI&Oacute;N DE FUNCIONAMIENTO';}
      
      $data['pr1']=$pr1;
      $data['pr2']=$pr2;
      $data['pr3']=$pr3;
      $data['pr4']=$pr4;

      $data['tp_rep']=$tp_rep;

      $meses=$this->get_mes($this->session->userdata("mes"));
      $mes = $meses[1];
      $dias = $meses[2];

      $data['id_mes'] = $this->session->userdata("mes");
      $data['mes'] = $mes;
      $data['tip_rep'] = $tip_rep;
      $data['tp'] = $tp;

    $data['tfis'] = $this->tabla_avance_fisico($tp,$p1,$p2,$p3,$p4,1);
    $this->load->view('admin/reportes/gerencial/fisica/riesgo_programas_fis', $data);
    }

    /*------------------------------------ FISICA - CONTROL SOCIAL ------------------------------------------*/
    public function ver_reporte_gerencial_fis_control($tp,$p1,$p2,$p3,$p4,$tp_rep)
    { 
      if($tp==1){$tip_rep='BAJA EJECUCI&Oacute;N : 0-25 %';$color='#ED0F1A';}
      if($tp==2){$tip_rep='EN RIESGO: 26-50 %';$color='#F8912F';}
      if($tp==3){$tip_rep='MEDIANA EJECUCI&Oacute;N: 51-75 %';$color='#EDE90F';}
      if($tp==4){$tip_rep='EJECUCI&Oacute;N A TIEMPO: 76-100 %';$color='#2ABE47';}

      $data['p1'] = $p1; //// proyectos de inversion
      $data['p2'] = $p2; //// programas recurrentes
      $data['p3'] = $p3; //// programas no recurrentes
      $data['p4'] = $p4; //// Operacion de funcionamiento

      $pr1='';$pr2='';$pr3='';$pr4='';
      if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
      if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
      if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
      if($p4==1){$pr4=' - OPERACI&Oacute;N DE FUNCIONAMIENTO';}
      
      $data['pr1']=$pr1;
      $data['pr2']=$pr2;
      $data['pr3']=$pr3;
      $data['pr4']=$pr4;

      $data['tp_rep']=$tp_rep;

      $meses=$this->get_mes($this->session->userdata("mes"));
      $mes = $meses[1];
      $dias = $meses[2];

      $data['id_mes'] = $this->session->userdata("mes");
      $data['mes'] = $mes;
      $data['tip_rep'] = $tip_rep;
      $data['tp'] = $tp;

      $data['tfis'] = $this->tabla_avance_fisico($tp,$p1,$p2,$p3,$p4,1);
      $this->load->view('admin/control_social/fisica/riesgo_programas_fis', $data);
    }

    public function tabla_avance_fisico($tp,$p1,$p2,$p3,$p4,$tp_rep)
    {
        $ms[0]='programado_total';$ms[1]='enero';$ms[2]='febrero';$ms[3]='marzo';$ms[4]='abril';$ms[5]='mayo';$ms[6]='junio';
        $ms[7]='julio';$ms[8]='agosto';$ms[9]='septiembre';$ms[10]='octubre';$ms[11]='noviembre';$ms[12]='diciembre';

        if($tp==1){$titulo='BAJA EJECUCI&Oacute;N: 0 a 25 %';}
        if($tp==2){$titulo='EN RIESGO: 26 a 50 %';}
        if($tp==3){$titulo='MEDIANA EJECUCI&Oacute;N: 51 a 75 %';}
        if($tp==4){$titulo='EJECUCI&Oacute;N A TIEMPO: 76 a 100 %';}

        if($tp_rep==1){
          $tb='id="dt_basic" class="table table table-bordered" width="100%"';
        }
        elseif ($tp_rep==2) {
          $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        }

        $tabla ='';
        $tabla .='<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h2 class="alert alert-success"><center>'.$titulo.'</center></h2>
                  </article>';

        $proyecto=$this->model_reporte_global->list_operaciones($p1,$p2,$p3,$p4);
        $nro=0;
        if(count($proyecto)!=0){
            $nro++;
            $tabla .='
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="jarviswidget jarviswidget-color-darken" >
                      <header>
                        <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                        <h2 class="font-md"><strong>POR DIRECCIONES ADMINISTRATIVAS</strong></h2>  
                      </header>
                <div>
                  <div class="widget-body no-padding">
                  <div class="table-responsive">';
            $tabla .='<table '.$tb.'>';
            $tabla .='<thead>';
            $tabla .='<tr class="modo1">';
              $tabla .='<th style="width:1%;">#</th>';
              $tabla .='<th style="width:15%;">DIRECCI&Oacute;N ADMINISTRATIVA</th>';
              $tabla .='<th style="width:15%;">UNIDAD EJECUTORA</th>';
              $tabla .='<th style="width:20%;">PROYECTO_PROGRAMA_OPERACI&Oacute;N</th>';
              $tabla .='<th style="width:10%;">TIPO DE OPERACI&Oacute;N</th>';
              $tabla .='<th>PROGRAMADO '.$this->gestion.'</th>';
              $tabla .='<th>EJECUCI&Oacute;N F&Iacute;SICA ACUMULADA</th>';
              $tabla .='<th>EFICACIA</th>';
              $tabla .='<th>ESTADO</th>';
            $tabla .='</tr>';
            $tabla .='</thead>';
            $nrop=0;
            $tabla .='<tbody>';
            $nro_ejec=1;
            foreach($proyecto  as $proy)
            {
              $fase = $this->model_faseetapa->get_id_fase($proy['proy_id']);
              $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
              $meses=$años*12;
              if($this->model_proyecto->verif_proy($proy['proy_id'])!=0)
              {
                //  echo "PROY_ID ".$proy['proy_id']."---".$fase[0]['pfec_fecha_inicio']."-".$fase[0]['pfec_fecha_fin']."<br>";
                  $componentes = $this->model_componente->componentes_id($fase[0]['id']);
                  for($p=1;$p<=$meses;$p++){$cp[1][$p]=0; $cp[2][$p]=0; $cp[3][$p]=0;}
                  foreach ($componentes as $rowc)
                  {
                    $productos = $this->model_producto->list_prod($rowc['com_id']);
                    for($p=1;$p<=$meses;$p++){$pp[1][$p]=0; $pp[2][$p]=0;}
                   // echo "COMPONENTE : ".$rowc['com_componente']."<br>";
                    foreach ($productos as $rowp)
                    {
                        $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                      //  echo "PRODUCTO : ".$rowp['prod_producto']."<br>";
                        for($p=1;$p<=$meses;$p++){$ap[1][$p]=0; $ap[2][$p]=0;}
                        foreach ($actividad as $rowa)
                        {   
                          $variablep=1;$variablee=1; $v=12; $nro=1; $sump=0;$sume=0;
                          for($p=1;$p<=$meses;$p++){$a[1][$p]=0; $a[2][$p]=0;$a[3][$p]=0;$a[4][$p]=0;$a[5][$p]=0;$a[6][$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                          for($i=$fase[0]['pfec_fecha_inicio'];$i<=$fase[0]['pfec_fecha_fin'];$i++)
                          {
                            $aprog = $this->model_actividad->actividad_programado($rowa['act_id'],$i);
                            if(count($aprog)!=0){
                              for ($j=1; $j <=12 ; $j++) { 
                                $a[1][$variablep]=$aprog[0][$ms[$j]];
                                $variablep++;
                              }
                            }
                            else{
                              $variablep=($v*$nro)+1;
                            }

                            $aejec = $this->model_actividad->actividad_ejecutado($rowa['act_id'],$i);
                            if(count($aejec)!=0){
                              for ($j=1; $j <=12 ; $j++) { 
                                $a[4][$variablee]=$aejec[0][$ms[$j]];
                                $variablee++;
                              }
                            }
                            else{
                              $variablee=($v*$nro)+1;
                            }

                            $nro++;
                          }

                          for ($i=1; $i <=$meses ; $i++) { 
                            $sump=$sump+$a[1][$i];
                            $a[2][$i]=$sump+$rowa['act_linea_base'];
                            
                            if($rowa['act_meta']!=0)
                            {
                              $a[3][$i]=round(((($sump+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                            }
                            $ap[1][$i]=$ap[1][$i]+(($a[3][$i]*$rowa['act_ponderacion'])/100);

                            $sume=$sume+$a[4][$i];
                            $a[5][$i]=$sume+$rowa['act_linea_base'];

                            if($rowa['act_meta']!=0)
                            {
                              $a[6][$i]=round(((($sume+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                            }
                            $ap[2][$i]=$ap[2][$i]+(($a[6][$i]*$rowa['act_ponderacion'])/100);
                          }

                        }
       
                          for ($i=1; $i <=$meses ; $i++) {
                            $pp[1][$i]=$pp[1][$i]+(($ap[1][$i]*$rowp['prod_ponderacion'])/100);
                            $pp[2][$i]=$pp[2][$i]+(($ap[2][$i]*$rowp['prod_ponderacion'])/100);
                          } 
                            
                      }
                      for ($i=1; $i <=$meses ; $i++) {
                            $cp[1][$i]=$cp[1][$i]+(($pp[1][$i]*$rowc['com_ponderacion'])/100);
                            $cp[2][$i]=$cp[2][$i]+(($pp[2][$i]*$rowc['com_ponderacion'])/100);
                          }

                    }

                    for ($i=1; $i <=$meses ; $i++) {
                      if($cp[1][$i]!=0){
                        $cp[3][$i]=round((($cp[2][$i]/$cp[1][$i])*100),1);
                      }
                    }

                    for ($i=1; $i <=12 ; $i++) { 
                      $tefic[1][$i]=0;$tefic[2][$i]=0;$tefic[3][$i]=0;
                    }
                    $i=1;$vmeses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;
                    for($p=$fase[0]['pfec_fecha_inicio'];$p<=$fase[0]['pfec_fecha_fin'];$p++)
                    {
                        $variable=0;
                        for($pos=$i;$pos<=$vmeses;$pos++)
                        {
                          $variable++;
                          $tefic[1][$variable]=$cp[1][$pos];
                          $tefic[2][$variable]=$cp[2][$pos];
                          $tefic[3][$variable]=$cp[3][$pos];
                        }
                        if($p==$this->session->userdata('gestion'))
                        {
                            if($tefic[3][$this->mes]>=0 & $tefic[3][$this->mes]<=25) /// Baja Ejecucion
                                $tp_ejec=1;
                            elseif($tefic[3][$this->mes]>25 & $tefic[3][$this->mes]<=50) /// En Riesgo
                                $tp_ejec=2;
                            elseif($tefic[3][$this->mes]>50 & $tefic[3][$this->mes]<=75) /// Mediana Ejecucion
                                $tp_ejec=3;
                            elseif($tefic[3][$this->mes]>75 & $tefic[3][$this->mes]<=100) /// Ejecucion a tiempo
                                $tp_ejec=4;
                            else
                              $tp_ejec=0;

                            if($tp==$tp_ejec){
                                $nrop++;
                                $estado=$this->estado_proyecto($proy['proy_id']);
                                $tabla .='<tr class="modo1 derecha">';
                                  $tabla .='<td>'.$nrop.'</td>';
                                  $tabla .='<td class="izquierda">'.$proy['das'].'</td>';
                                  $tabla .='<td class="izquierda">'.$proy['u_ejec'].'</td>';
                                  $tabla .='<td class="izquierda">'.$proy['proy_nombre'].'</td>';
                                  $tabla .='<td class="izquierda">'.$proy['tp_tipo'].'</td>';
                                  $tabla .='<td>'.round($tefic[1][$this->mes],2).' %</td>';
                                  $tabla .='<td>'.round($tefic[2][$this->mes],2).' %</td>';
                                  $tabla .='<td bgcolor="#c9eac9">'.round($tefic[3][$this->mes],2).' %</td>';
                                  $tabla .='<td class="izquierda">'.$estado.'</td>';
                                $tabla .='</tr>';
                            }
                        }

                          $i=$vmeses+1;
                          $vmeses=$vmeses+12;
                          $gestion++; $nro++;
                    }
                }
            }
            $tabla .='</tbody>';
            $tabla .='</table>';
            $tabla .='</div>

                  </div>
                </div>
              </div>
            </article>';
          }

      return $tabla;
    }

    /*--------------------------------------------REP OBJETIVOS DE CORTO PLAZO ------------------------------------------------------*/
    public function ver_reporte_objetivos_grupo($tp,$p1,$p2,$p3,$p4,$tp_rep)
    { 
      $enlaces = $this->menu_modelo->get_Modulos(7);
      $data['enlaces'] = $enlaces;
      for ($i = 0; $i < count($enlaces); $i++) {
          $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
      }
      $data['subenlaces'] = $subenlaces;

      if($tp==1){$tip_rep='BAJA EJECUCI&Oacute;N : 0-25 %';$color='#ED0F1A';}
      if($tp==2){$tip_rep='EN RIESGO: 26-50 %';$color='#F8912F';}
      if($tp==3){$tip_rep='MEDIANA EJECUCI&Oacute;N: 51-75 %';$color='#EDE90F';}
      if($tp==4){$tip_rep='EJECUCI&Oacute;N A TIEMPO: 76-100 %';$color='#2ABE47';}

      $data['p1'] = $p1; //// proyectos de inversion
      $data['p2'] = $p2; //// programas recurrentes
      $data['p3'] = $p3; //// programas no recurrentes
      $data['p4'] = $p4; //// accion de funcionamiento

      $pr1='';$pr2='';$pr3='';$pr4='';
      if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
      if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
      if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
      if($p4==1){$pr4=' - OPERACI&Oacute;N DE FUNCIONAMIENTO';}
      
      $data['pr1']=$pr1;
      $data['pr2']=$pr2;
      $data['pr3']=$pr3;
      $data['pr4']=$pr4;

      $data['tp_rep']=$tp_rep;

      $meses=$this->get_mes($tp_rep);
      $mes = $meses[1];
      $dias = $meses[2];

      $data['id_mes'] = $this->session->userdata("mes");
      $data['mes'] = $mes;
      $data['tip_rep'] = $tip_rep;
      $data['tp'] = $tp;
    
      //generando el listado
    $n =  $tp_rep; 
    $gestion = $this->session->userdata('gestion');
    $objetivos = $this->model_proyecto->reporte_objetivos_cp($gestion);
    $ebaja=0; $eriesgo=0; $emedia=0; $eatiempo=0;

    $tb='id="dt_basic" class="table table table-bordered" width="100%"';
       
        // elseif ($tp_rep==2) {
        //   $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        // }

        $tabla ='';
        $tabla .='<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h2 class="alert alert-success"><center>'.$tip_rep.'</center></h2>
                  </article>';

        $nro=0;
            $nro++;
            $tabla .='
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                        <header>
                          <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                          <h2 class="font-md"><strong>POR DIRECCIONES ADMINISTRATIVAS</strong></h2>  
                        </header>
                <div>
                  <div class="widget-body no-padding">
                  <div class="table-responsive">';
            $tabla .='<table '.$tb.'>';
            $tabla .='<thead>';
            $tabla .='<tr class="modo1">';
              $tabla .='<th style="width:1%;">#</th>';
              $tabla .='<th style="width:15%;">ACCIONES DE CORTO PLAZO</th>';
              $tabla .='<th style="width:15%;">LINEA BASE</th>';
              $tabla .='<th style="width:20%;">META</th>';

            $tabla .='</tr>';
            $tabla .='</thead>';
            $tabla .='<tbody>';

                    $tabla .='<tr class="modo1">';

    foreach ($objetivos as $obje) {
        $acumula = 0;
        $acumule = 0;
        for ($j = 1; $j <= $n; $j++) {
            $resul = $this->model_proyecto->reporte_obtener_pg($j, $obje['o_id']);
            if(!empty($resul))
            {
                $acumula += $resul[0]["opm_fis"];
            }
            $resul = $this->model_proyecto->reporte_obtener_ej($j, $obje['o_id']);
            if(!empty($resul))
            {
                $acumule += $resul[0]["oem_fis"];
            }
        }

        if($tp==1){$titulo='BAJA EJECUCI&Oacute;N: 0-25 %';}
        if($tp==2){$titulo='EN RIESGO: 26-50 %';}
        if($tp==3){$titulo='MEDIANA EJECUCI&Oacute;N: 51-75 %';}
        if($tp==4){$titulo='EJECUCI&Oacute;N A TIEMPO: 76-100 %';}

        // echo '<br>acum: '.$acumula;
        $tabla .='<tr>';

        if($acumula != 0)   {
                // echo '<br>obje: '.$obje['o_objetivo'];
                $percent = (($acumule/$acumula)*100);
                if($percent < 25 && $tp == 1){
                    $tabla .='<td>'.$nro.'</td>';
                      $tabla .='<td>'.$obje['o_objetivo'].'</td>';
                      $tabla .='<td>'.$obje['o_linea_base'].'</td>';
                      $tabla .='<td>'.$obje['o_meta'].'</td>';
                }elseif($percent>=25 && $percent < 50 && $tp == 2){
                    $tabla .='<td>'.$nro.'</td>';
                      $tabla .='<td>'.$obje['o_objetivo'].'</td>';
                      $tabla .='<td>'.$obje['o_linea_base'].'</td>';
                      $tabla .='<td>'.$obje['o_meta'].'</td>';
                }elseif($percent>=50 && $percent < 75 && $tp == 3){
                    $tabla .='<td>'.$nro.'</td>';
                      $tabla .='<td>'.$obje['o_objetivo'].'</td>';
                      $tabla .='<td>'.$obje['o_linea_base'].'</td>';
                      $tabla .='<td>'.$obje['o_meta'].'</td>';
                }elseif( $percent >= 75&& $tp == 4){
                    $tabla .='<td>'.$nro.'</td>';
                      $tabla .='<td>'.$obje['o_objetivo'].'</td>';
                      $tabla .='<td>'.$obje['o_linea_base'].'</td>';
                      $tabla .='<td>'.$obje['o_meta'].'</td>';
                }
                $nro++;
        } else {

                    $tabla .='<td>'.$nro.'</td>';
                      $tabla .='<td>'.$obje['o_objetivo'].'</td>';
                      $tabla .='<td>'.$obje['o_linea_base'].'</td>';
                      $tabla .='<td>'.$obje['o_meta'].'</td>';
               
                $nro++;
        }
        $tabla .='</tr>';

    }
    // die;  

    $tabla .='</tr>';
     $tabla .='</tbody>';
              $tabla .='</table>';
              $tabla .='</div>

                  </div>
                </div>
              </div>
            </article>';
          



    $data['tfin'] = $tabla;
    $this->load->view('admin/reportes/gerencial/objetivos/riesgo_objetivos', $data);
    }

    /*------------------------------------------------- FINANCIERO ------------------------------------------------------*/
    public function ver_reporte_gerencial_fin($tp,$p1,$p2,$p3,$p4,$tp_rep)
    { 
      $enlaces = $this->menu_modelo->get_Modulos(7);
      $data['enlaces'] = $enlaces;
      for ($i = 0; $i < count($enlaces); $i++) {
          $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
      }
      $data['subenlaces'] = $subenlaces;

      if($tp==1){$tip_rep='BAJA EJECUCI&Oacute;N : 0-25 %';$color='#ED0F1A';}
      if($tp==2){$tip_rep='EN RIESGO: 26-50 %';$color='#F8912F';}
      if($tp==3){$tip_rep='MEDIANA EJECUCI&Oacute;N: 51-75 %';$color='#EDE90F';}
      if($tp==4){$tip_rep='EJECUCI&Oacute;N A TIEMPO: 76-100 %';$color='#2ABE47';}

      $data['p1'] = $p1; //// proyectos de inversion
      $data['p2'] = $p2; //// programas recurrentes
      $data['p3'] = $p3; //// programas no recurrentes
      $data['p4'] = $p4; //// accion de funcionamiento

      $pr1='';$pr2='';$pr3='';$pr4='';
      if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
      if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
      if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
      if($p4==1){$pr4=' - OPERACI&Oacute;N DE FUNCIONAMIENTO';}
      
      $data['pr1']=$pr1;
      $data['pr2']=$pr2;
      $data['pr3']=$pr3;
      $data['pr4']=$pr4;

      $data['tp_rep']=$tp_rep;

      $meses=$this->get_mes($this->session->userdata("mes"));
      $mes = $meses[1];
      $dias = $meses[2];

      $data['id_mes'] = $this->session->userdata("mes");
      $data['mes'] = $mes;
      $data['tip_rep'] = $tip_rep;
      $data['tp'] = $tp;

    $data['tfin'] = $this->tabla_avance_financiero($tp,$p1,$p2,$p3,$p4,1);
    $this->load->view('admin/reportes/gerencial/financiera/riesgo_programas_fin', $data);
    }

    /*------------------------------------------------- FINANCIERO - CONTROL SOCIAL ------------------------------------------------------*/
    public function ver_reporte_gerencial_fin_control($tp,$p1,$p2,$p3,$p4,$tp_rep)
    { 
      if($tp==1){$tip_rep='BAJA EJECUCI&Oacute;N : 0-25 %';$color='#ED0F1A';}
      if($tp==2){$tip_rep='EN RIESGO: 26-50 %';$color='#F8912F';}
      if($tp==3){$tip_rep='MEDIANA EJECUCI&Oacute;N: 51-75 %';$color='#EDE90F';}
      if($tp==4){$tip_rep='EJECUCI&Oacute;N A TIEMPO: 76-100 %';$color='#2ABE47';}

      $data['p1'] = $p1; //// proyectos de inversion
      $data['p2'] = $p2; //// programas recurrentes
      $data['p3'] = $p3; //// programas no recurrentes
      $data['p4'] = $p4; //// accion de funcionamiento

      $pr1='';$pr2='';$pr3='';$pr4='';
      if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
      if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
      if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
      if($p4==1){$pr4=' - OPERACI&Oacute;N DE FUNCIONAMIENTO';}
      
      $data['pr1']=$pr1;
      $data['pr2']=$pr2;
      $data['pr3']=$pr3;
      $data['pr4']=$pr4;

      $data['tp_rep']=$tp_rep;

      $meses=$this->get_mes($this->session->userdata("mes"));
      $mes = $meses[1];
      $dias = $meses[2];

      $data['id_mes'] = $this->session->userdata("mes");
      $data['mes'] = $mes;
      $data['tip_rep'] = $tip_rep;
      $data['tp'] = $tp;

      $data['tfin'] = $this->tabla_avance_financiero($tp,$p1,$p2,$p3,$p4,1);
      $this->load->view('admin/control_social/financiera/riesgo_programas_fin', $data);
    }

    public function tabla_avance_financiero($tp,$p1,$p2,$p3,$p4,$tp_rep)
    {
        if($tp==1){$titulo='BAJA EJECUCI&Oacute;N: 0-25 %';}
        if($tp==2){$titulo='EN RIESGO: 26-50 %';}
        if($tp==3){$titulo='MEDIANA EJECUCI&Oacute;N: 51-75 %';}
        if($tp==4){$titulo='EJECUCI&Oacute;N A TIEMPO: 76-100 %';}

        if($tp_rep==1){
          $tb='id="dt_basic" class="table table table-bordered" width="100%"';
        }
        elseif ($tp_rep==2) {
          $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        }

        $tabla ='';
        $tabla .='<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h2 class="alert alert-success"><center>'.$titulo.'</center></h2>
                  </article>';

        $proyecto=$this->model_reporte_global->proyectos_ue_global_total($p1,$p2,$p3,$p4);
        $nro=0;
        if(count($proyecto)!=0){
            $nro++;
            $tabla .='
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                        <header>
                          <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                          <h2 class="font-md"><strong>POR DIRECCIONES ADMINISTRATIVAS</strong></h2>  
                        </header>
                <div>
                  <div class="widget-body no-padding">
                  <div class="table-responsive">';
            $tabla .='<table '.$tb.'>';
            $tabla .='<thead>';
            $tabla .='<tr class="modo1">';
              $tabla .='<th style="width:1%;">#</th>';
              $tabla .='<th style="width:15%;">DIRECCI&Oacute;N ADMINISTRATIVA</th>';
              $tabla .='<th style="width:15%;">UNIDAD EJECUTORA</th>';
              $tabla .='<th style="width:20%;">PROYECTO_PROGRAMA_OPERACI&Oacute;N</th>';
              $tabla .='<th style="width:10%;">TIPO DE OPERACI&Oacute;N</th>';
              $tabla .='<th>AVANCE FINANCIERO</th>';
              $tabla .='<th>AVANCE F&Iacute;SICO</th>';
              $tabla .='<th>ESTADO</th>';
            $tabla .='</tr>';
            $tabla .='</thead>';
            $nrop=0;
            $tabla .='<tbody>';
              foreach($proyecto as $rowp)
              {
                $fis=$this->avance_fisico($rowp['proy_id'],$this->mes);
                $estado=$this->estado_proyecto($rowp['proy_id']);
                $ejec=0;
                if($rowp['pe_pv']!=0){
                  $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
                }

                if($ejec>=0 & $ejec<=25) /// en riesgo alto
                  $tp_ejec=1;
                elseif($ejec>25 & $ejec<=50) /// en riesgo
                  $tp_ejec=2;
                elseif($ejec>50 & $ejec<=75) /// con retraso
                  $tp_ejec=3;
                elseif($ejec>75 & $ejec<=100) /// a tiempo
                  $tp_ejec=4;

                if($tp==$tp_ejec){
                    $nrop++;
                    $tabla .='<tr class="modo1">';
                      $tabla .='<td>'.$nrop.'</td>';
                      $tabla .='<td>'.$rowp['das'].'</td>';
                      $tabla .='<td>'.$rowp['u_ejec'].'</td>';
                      $tabla .='<td>'.$rowp['proy_nombre'].'</td>';
                      $tabla .='<td>'.$rowp['tp_tipo'].'</td>';
                      $tabla .='<td bgcolor="#c9eac9">'.$ejec.'%</td>';
                      $tabla .='<td>'.$fis[2].'%</td>';
                      $tabla .='<td>'.$estado.'</td>';
                    $tabla .='</tr>';
                }
              }
              $tabla .='</tbody>';
                $tabla .='<tr bgcolor="#f5f5f5">';
                  $tabla .='<td></td>';
                  $tabla .='<td><b>TOTAL : '.$nrop.'</b></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                $tabla .='</tr>';
              $tabla .='</table>';
              $tabla .='</div>

                  </div>
                </div>
              </div>
            </article>';
          }

      return $tabla;
    }

    /*------------------- IMPRIMIR REPORTE FINANCIERO ------------------*/
    public function reporte_avance_financiero($p1,$p2,$p3,$p4)
    {
      $mes=$this->get_mes($this->session->userdata("mes"));
      $data['mes'] = $mes[1];
      $data['dias'] = $mes[2];
      $data['avance_financiero']=$this->avance_financiero_gerencial($this->mes,$p1,$p2,$p3,$p4); ///// AVANCE FINANCIERO
      $this->load->view('admin/reportes/gerencial/financiera/imprimir_reporte', $data);
    }

    /*------------------- IMPRIMIR REPORTE FISICO ------------------*/
    public function reporte_avance_fisico($p1,$p2,$p3,$p4)
    {
      $mes=$this->get_mes($this->session->userdata("mes"));
      $data['mes'] = $mes[1];
      $data['dias'] = $mes[2];

      $data['avance_financiero']=$this->avance_fisico_gerencial($this->mes,$p1,$p2,$p3,$p4); ///// AVANCE FINANCIERO
      $this->load->view('admin/reportes/gerencial/fisica/imprimir_reporte', $data);
    }

    /*-------------------- Avance Fisico -------------------*/
    public function avance_fisico($proy_id,$id_mes)
    {  
      $fase = $this->model_faseetapa->get_id_fase($proy_id);
      $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
      $meses=$años*12;

      $ms[0]='programado_total';$ms[1]='enero';$ms[2]='febrero';$ms[3]='marzo';$ms[4]='abril';$ms[5]='mayo';$ms[6]='junio';
      $ms[7]='julio';$ms[8]='agosto';$ms[9]='septiembre';$ms[10]='octubre';$ms[11]='noviembre';$ms[12]='diciembre';

      $componentes = $this->model_componente->componentes_id($fase[0]['id']);
      for($p=1;$p<=$meses;$p++){$cp[1][$p]=0; $cp[2][$p]=0; $cp[3][$p]=0;}
      foreach ($componentes as $rowc)
      {
        $productos = $this->model_producto->list_prod($rowc['com_id']);
        for($p=1;$p<=$meses;$p++){$pp[1][$p]=0; $pp[2][$p]=0;}
       // echo "COMPONENTE : ".$rowc['com_componente']."<br>";
        foreach ($productos as $rowp)
        {
            $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
          //  echo "PRODUCTO : ".$rowp['prod_producto']."<br>";
            for($p=1;$p<=$meses;$p++){$ap[1][$p]=0; $ap[2][$p]=0;}
            foreach ($actividad as $rowa)
            {   
              $variablep=1;$variablee=1; $v=12; $nro=1; $sump=0;$sume=0;
              for($p=1;$p<=$meses;$p++){$a[1][$p]=0; $a[2][$p]=0;$a[3][$p]=0;$a[4][$p]=0;$a[5][$p]=0;$a[6][$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
              for($i=$fase[0]['pfec_fecha_inicio'];$i<=$fase[0]['pfec_fecha_fin'];$i++)
              {
                $aprog = $this->model_actividad->actividad_programado($rowa['act_id'],$i);
                if(count($aprog)!=0){
                  for ($j=1; $j <=12 ; $j++) { 
                    $a[1][$variablep]=$aprog[0][$ms[$j]];
                    $variablep++;
                  }
                }
                else{
                  $variablep=($v*$nro)+1;
                }

                $aejec = $this->model_actividad->actividad_ejecutado($rowa['act_id'],$i);
                if(count($aejec)!=0){
                  for ($j=1; $j <=12 ; $j++) { 
                    $a[4][$variablee]=$aejec[0][$ms[$j]];
                    $variablee++;
                  }
                }
                else{
                  $variablee=($v*$nro)+1;
                }

                $nro++;
              }

              for ($i=1; $i <=$meses ; $i++) { 
                $sump=$sump+$a[1][$i];
                $a[2][$i]=$sump+$rowa['act_linea_base'];
                
                if($rowa['act_meta']!=0)
                {
                  $a[3][$i]=round(((($sump+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                }
                $ap[1][$i]=$ap[1][$i]+(($a[3][$i]*$rowa['act_ponderacion'])/100);

                $sume=$sume+$a[4][$i];
                $a[5][$i]=$sume+$rowa['act_linea_base'];

                if($rowa['act_meta']!=0)
                {
                  $a[6][$i]=round(((($sume+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                }
                $ap[2][$i]=$ap[2][$i]+(($a[6][$i]*$rowa['act_ponderacion'])/100);
              }

            }

              for ($i=1; $i <=$meses ; $i++) {
                $pp[1][$i]=$pp[1][$i]+(($ap[1][$i]*$rowp['prod_ponderacion'])/100);
                $pp[2][$i]=$pp[2][$i]+(($ap[2][$i]*$rowp['prod_ponderacion'])/100);
              } 
              
        }
          for ($i=1; $i <=$meses ; $i++) {
                $cp[1][$i]=$cp[1][$i]+(($pp[1][$i]*$rowc['com_ponderacion'])/100);
                $cp[2][$i]=$cp[2][$i]+(($pp[2][$i]*$rowc['com_ponderacion'])/100);
              }

      }

      for ($i=1; $i <=$meses ; $i++) {
        if($cp[1][$i]!=0){
          $cp[3][$i]=round((($cp[2][$i]/$cp[1][$i])*100),1);
        }
      }

      for ($i=1; $i <=12 ; $i++) { 
        $tefic[1][$i]=0;$tefic[2][$i]=0;$tefic[3][$i]=0;
      }
      $i=1;$vmeses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;
      for($p=$fase[0]['pfec_fecha_inicio'];$p<=$fase[0]['pfec_fecha_fin'];$p++)
      {
          $variable=0;
          for($pos=$i;$pos<=$vmeses;$pos++)
          {
            $variable++;
            $tefic[1][$variable]=$cp[1][$pos];
            $tefic[2][$variable]=$cp[2][$pos];
            $tefic[3][$variable]=$cp[3][$pos];
          }
          if($p==$this->session->userdata('gestion'))
          {
              $ejecucion[1]=round($tefic[1][$id_mes],2); //// Programado 
              $ejecucion[2]=round($tefic[2][$id_mes],2); //// Ejecutado
              $ejecucion[3]=round($tefic[3][$id_mes],2); //// Eficacia
          }

            $i=$vmeses+1;
            $vmeses=$vmeses+12;
            $gestion++; $nro++;
      }

      return $ejecucion;
  }

    /*-------------------- Estado Actual del proyecto -------------------*/
    public function estado_proyecto($proy_id)
    { 
      $fase = $this->model_faseetapa->get_id_fase($proy_id);
      $estado=$this->mdas_complementario->estado_operacion($fase[0]['id'],$this->mes,$this->gestion);
      $estado_proy='-----';
        if(count($estado)!=0){
            $estado_proy=$estado[0]['ep_descripcion'];
        }

      return $estado_proy;
    }
  
  public function reporte_gerencial_fisica($tp,$p1,$p2,$p3,$p4,$tp_rep)
   {
    $mess=$this->get_mes($this->session->userdata("mes"));
    $mes = $mess[1];
    $dias = $mess[2];

    if($tp==1){$tip_rep='BAJA EJECUCI&Oacute;N: 0 a 25 %';}
    if($tp==2){$tip_rep='EN RIESGO: 26 a 50 %';}
    if($tp==3){$tip_rep='MEDIANA EJECUCI&Oacute;N: 51 a 75 %';}
    if($tp==4){$tip_rep='EJECUCI&Oacute;N A TIEMPO: 76 a 100 %';}


    $pr1='';$pr2='';$pr3='';$pr4='';
    if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
    if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
    if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
    if($p4==1){$pr4=' - OPERACI&Oacute;N DE FUNCIONAMIENTO';}

    $tabla = $this->tabla_avance_fisico($tp,$p1,$p2,$p3,$p4,2);

    $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr >
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> Resumen de Indicadores de Eficacia F&iacute;sica por '.$pr1.''.$pr2.''.$pr3.''.$pr4.' <br>
                            (De 1 de ENERO Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <div>
                  '.$tabla.'
                </div>
            </body>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_gerencial_fisica.pdf", array("Attachment" => false));
   }

  public function reporte_gerencial_financiero($tp,$p1,$p2,$p3,$p4,$tp_rep)
   {
    $mess=$this->get_mes($this->session->userdata("mes"));
    $mes = $mess[1];
    $dias = $mess[2];

    if($tp==1){$tip_rep='BAJA EJECUCI&Oacute;N: 0 a 25 %';}
    if($tp==2){$tip_rep='EN RIESGO: 26 a 50 %';}
    if($tp==3){$tip_rep='MEDIANA EJECUCI&Oacute;N: 51 a 75 %';}
    if($tp==4){$tip_rep='EJECUCI&Oacute;N A TIEMPO: 76 a 100 %';}


    $pr1='';$pr2='';$pr3='';$pr4='';
    if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
    if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
    if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
    if($p4==1){$pr4=' - OPERACI&Oacute;N DE FUNCIONAMIENTO';}

    $tabla = $this->tabla_avance_financiero($tp,$p1,$p2,$p3,$p4,2);

    $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr >
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> Ejecuci&oacute;n Financiera a nivel de '.$pr1.''.$pr2.''.$pr3.''.$pr4.' <br>
                            (De 1 de ENERO Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <div>
                  '.$tabla.'
                </div>
            </body>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_ejecucion_financiera.pdf", array("Attachment" => false));
   }

    /*============================================= FICHA TECNICA DE PRODUCTOS =========================================*/
  private $estilo_vertical = 
    '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }

        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
    table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        .mv{font-size:10px;}
        .siipp{width:120px;}

    .titulo_pdf {
                text-align: left;
                font-size: 10px;
            }
            .tabla {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 7px;
                width: 100%;

    }
    .tabla th {
    padding: 5px;
    font-size: 7px;
    background-color: #83aec0;
    background-image: url(fondo_th.png);
    background-repeat: repeat-x;
    color: #FFFFFF;
    border-right-width: 1px;
    border-bottom-width: 1px;
    border-right-style: solid;
    border-bottom-style: solid;
    border-right-color: #558FA6;
    border-bottom-color: #558FA6;
    font-family: "Trebuchet MS", Arial;
    text-transform: uppercase;
    }
    .tabla .modo1 {
    font-size: 7px;
    font-weight:bold;

    background-image: url(fondo_tr01.png);
    background-repeat: repeat-x;
    color: #34484E;
    font-family: "Trebuchet MS", Arial;
    }
    .tabla .modo1 td {
    padding: 5px;
    border-right-width: 1px;
    border-bottom-width: 1px;
    border-right-style: solid;
    border-bottom-style: solid;
    border-right-color: #A4C4D0;
    border-bottom-color: #A4C4D0;

    }
    </style>';
  
  /*=========================================================================================================================*/
    public function get_mes($mes_id)
    {
      $mes[1]='ENERO';
      $mes[2]='FEBRERO';
      $mes[3]='MARZO';
      $mes[4]='ABRIL';
      $mes[5]='MAYO';
      $mes[6]='JUNIO';
      $mes[7]='JULIO';
      $mes[8]='AGOSTO';
      $mes[9]='SEPTIEMBRE';
      $mes[10]='OCTUBRE';
      $mes[11]='NOVIEMBRE';
      $mes[12]='DICIEMBRE';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }

    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }
}