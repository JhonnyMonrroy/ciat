<?php
class Model_nfisfin extends CI_Model {
    public function __construct()
    {
        $this->load->database();
        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
    }

    /*-------------- Lista de direcciones administrativa ------------*/
    public function direcciones_administrativas()
    {
        $sql = 'select *
                from unidadorganizacional
                where uni_adm=\'1\' and uni_estado!=\'3\'
                order by uni_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- Get Unidad Administrativa ------------*/
    public function get_unidad_organizacional($uni_id)
    {
        $sql = 'select *
                from unidadorganizacional
                where uni_id='.$uni_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }


    /*======================= LISTA DE PROYECTOS FISICO =====================*/
    public function lista_programas($f1,$f2,$p1,$p2,$p3,$p4)
    {
        $fe1=0;$fe2=0;$tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($f1==1){$fe1=1;}
        if($f2==1){$fe2=2;}

        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = 'select uad.uni_id as das_id,uad.uni_unidad as das,p.proy_id,p.proy_nombre,tp.tp_tipo,pf.*,tp.tp_tipo,u.uni_unidad as u_ejec
            from aperturaprogramatica a
            Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
            Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
            Inner Join _proyectos as p On ap.proy_id = p.proy_id
            Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
            Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
            Inner Join unidadorganizacional as uad On uad.uni_id = pfun.uni_adm
            Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
            where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$this->gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and pf.pfec_estado=\'1\' and (pf.pfec_ejecucion='.$fe1.' or pf.pfec_ejecucion='.$fe2.')and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.') and pfun.pfun_tp=\'1\' and uad.uni_adm=\'1\'
            order by uad.uni_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*======================= LISTA DE PROYECTOS SEGUN SU DAS =====================*/
    public function get_proyectos_prog_fis($f1,$f2,$p1,$p2,$p3,$p4,$dea_id)
    {
        $fe1=0;$fe1=0;$tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($f1==1){$fe1=1;}
        if($f2==1){$fe2=2;}

        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = 'select uad.uni_id as das_id,uad.uni_unidad as das,p.proy_id,p.proy_nombre,tp.tp_tipo,pf.*,u.uni_unidad as u_ejec
            from aperturaprogramatica a
            Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
            Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
            Inner Join _proyectos as p On ap.proy_id = p.proy_id
            Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
            Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
            Inner Join unidadorganizacional as uad On uad.uni_id = pfun.uni_adm
            Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
            where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$this->gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') 
            and pf.pfec_estado=\'1\' and (pf.pfec_ejecucion='.$fe1.' or pf.pfec_ejecucion='.$fe2.') and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.') and pfun.pfun_tp=\'1\' and uad.uni_adm=\'1\' and uad.uni_id='.$dea_id.'
            order by uad.uni_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*======================= LISTA DE PROYECTOS FINANCIERO =====================*/
    public function lista_programas_fin($f1,$f2,$p1,$p2,$p3,$p4)
    {
        $fe1=0;$fe2=0;$tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($f1==1){$fe1=1;}
        if($f2==1){$fe2=2;}

        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = 'select uad.uni_id as das_id,uad.uni_unidad as das,p.proy_id,p.proy_nombre,pf.*,tp.tp_tipo,pe.*,u.uni_unidad as u_ejec
            from aperturaprogramatica a
            Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
            Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
            Inner Join _proyectos as p On ap.proy_id = p.proy_id
            Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
            Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
            Inner Join unidadorganizacional as uad On uad.uni_id = pfun.uni_adm
            Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
            Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
            Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
            where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$this->gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$this->mes.' and pf.pfec_estado=\'1\' and (pf.pfec_ejecucion='.$fe1.' or pf.pfec_ejecucion='.$fe2.') and pfg.g_id='.$this->gestion.' and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.') and pfun.pfun_tp=\'2\' and uad.uni_adm=\'1\'
            order by uad.uni_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*======================= LISTA DE PROYECTOS FINANCIEROS SEGUN SU DAS =====================*/
    public function get_proyectos_prog_fin($f1,$f2,$p1,$p2,$p3,$p4,$dea_id)
    {
        $fe1=0;$fe2=0;$tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($f1==1){$fe1=1;}
        if($f2==1){$fe2=2;}

        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = 'select uad.uni_id as das_id,uad.uni_unidad as das,p.proy_id,p.proy_nombre,tp.tp_tipo,pe.*,pf.*,u.uni_unidad as u_ejec
            from aperturaprogramatica a
            Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
            Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
            Inner Join _proyectos as p On ap.proy_id = p.proy_id
            Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
            Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
            Inner Join unidadorganizacional as uad On uad.uni_id = pfun.uni_adm
            Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
            Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
            Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
            where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$this->gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$this->mes.' and pf.pfec_estado=\'1\' and (pf.pfec_ejecucion='.$fe1.' or pf.pfec_ejecucion='.$fe2.') and pfg.g_id='.$this->gestion.' and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.') and pfun.pfun_tp=\'2\' and uad.uni_adm=\'1\'  and uad.uni_id='.$dea_id.'
            order by uad.uni_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}