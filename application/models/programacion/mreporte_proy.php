<?php

class Mreporte_proy extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    function lista_componete($proy_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vista_componentes_dictamen');
        $this->db->WHERE('proy_id', $proy_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    //LISTA DE FINANCIAMIENTO DEL PROYECTO DIRECTO
    function lista_financiamiento_dir($proy_id, $com_id)
    {
        $this->db->SELECT('i.*,f.*');
        $this->db->FROM('vrelacion_proy_prod_act_ins i');
        $this->db->JOIN('v_ins_financiamiento_programado f', 'i.ins_id = f.ins_id', 'INNER');
        $this->db->WHERE('proy_id', $proy_id);
        $this->db->WHERE('com_id', $com_id);
        $query = $this->db->get();
        return $query;
    }

    //SUMA FINANCIAMIENTO POR MES DEL PROYECTO DE FORMA DIRECTA
    function suma_fin_proyecto_directo($proy_id, $gestion)
    {
        $this->db->SELECT('i.proy_id, SUM(f.ifin_monto) AS monto, SUM(f.enero) AS enero, SUM(f.febrero) AS febrero, SUM(f.marzo) AS marzo, SUM(f.abril) AS abril,
        SUM(f.mayo) AS mayo, SUM(f.junio) AS junio, SUM(f.julio) AS julio, SUM(f.agosto) AS agosto, SUM(f.septiembre) AS septiembre,
        SUM(f.octubre) AS octubre, SUM(f.noviembre) AS noviembre, SUM(f.diciembre) AS diciembre');
        $this->db->FROM('vrelacion_proy_prod_act_ins i');
        $this->db->JOIN('v_ins_financiamiento_programado f', 'i.ins_id = f.ins_id', 'INNER');
        $this->db->WHERE('i.proy_id', $proy_id);
        $this->db->WHERE('f.ifin_gestion', $gestion);
        $this->db->GROUP_BY('i.proy_id');
        $query = $this->db->get();
        return $query->row();
    }

    //LISTA DE FINANCIAMIENTO DEL PROYECTO DIRECTO
    function lista_financiamiento_del($proy_id, $com_id)
    {
        $this->db->SELECT('i.*,f.*');
        $this->db->FROM('vrelacion_proy_com_ins i');
        $this->db->JOIN('v_ins_financiamiento_programado f', 'i.ins_id = f.ins_id', 'INNER');
        $this->db->WHERE('proy_id', $proy_id);
        $this->db->WHERE('com_id', $com_id);
        $query = $this->db->get();
        return $query;
    }

    //SUMA FINANCIAMIENTO POR MES DEL PROYECTO DELEGADO
    function suma_fin_proyecto_del($proy_id, $gestion)
    {
        $this->db->SELECT('i.proy_id, SUM(f.ifin_monto) AS monto, SUM(f.enero) AS enero, SUM(f.febrero) AS febrero, SUM(f.marzo) AS marzo, SUM(f.abril) AS abril,
        SUM(f.mayo) AS mayo, SUM(f.junio) AS junio, SUM(f.julio) AS julio, SUM(f.agosto) AS agosto, SUM(f.septiembre) AS septiembre,
        SUM(f.octubre) AS octubre, SUM(f.noviembre) AS noviembre, SUM(f.diciembre) AS diciembre');
        $this->db->FROM('vrelacion_proy_com_ins i');
        $this->db->JOIN('v_ins_financiamiento_programado f', 'i.ins_id = f.ins_id', 'INNER');
        $this->db->WHERE('i.proy_id', $proy_id);
        $this->db->WHERE('f.ifin_gestion', $gestion);
        $this->db->GROUP_BY('i.proy_id');
        $query = $this->db->get();
        return $query->row();
    }

    //ejecucion financiera
    function get_proy_ejec($proy_id, $gestion)
    {
        $this->db->SELECT('*');
        $this->db->FROM('v_proy_ejec_mes');
        $this->db->WHERE('proy_id', $proy_id);
        $this->db->WHERE('gestion', $gestion);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_presupuesto_fases($proy_id)
    {
        $sql = "
        SELECT p.*
        FROM _proyectofaseetapacomponente f
        INNER JOIN ptto_fase_gestion p ON f.pfec_id = p.pfec_id
        WHERE f.proy_id = " . $proy_id . " AND f.pfec_estado = 1 AND p.estado IN (1,2)
        ORDER BY p.g_id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}