<?php

class Minsumos_partida extends CI_Model
{
    var $gestion;
    var $fun_id;

    public function __construct()
    {
        $this->load->database();
        $this->gestion = $this->session->userData('gestion');
        $this->fun_id = $this->session->userData('fun_id');
    }

    public function delete_insumo_partida_gestion($insp_id){
        $this->db->where('insp_id', $insp_id);
        $this->db->delete('insumo_partida_gestion');
    }
    public function add_insumo_gestion($insp_id,$gestion,$m_id,$monto){
        $data_to_store = array(
            'insp_id' => $insp_id, /// Id Insumo
            'g_id' => $gestion, /// Gestion
            'inspg_monto_prog' => $monto, /// Monto programado
            'm_id' => $m_id,
        );
        $this->db->insert('insumo_partida_gestion', $data_to_store); ///// Guardar en Tabla Insumo Gestion
    }
    public function insumo_prog_mensual($insp_id,$gest)
    {
        $this->db->from('insumo_partida_gestion');
        $this->db->where('insp_id', $insp_id);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*------------ SUMA INSUMOS GESTION PROGRAMADO --------*/
    public function insumo_gestion_programado($proy_id, $gestion,$pfec_ejecucion)
    {
       $sql = 'select * from insumo_programado_gestion('.$proy_id.','.$gestion.','.$pfec_ejecucion.')';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*------------ SUMA INSUMOS GESTION EJECUTADO --------*/
    public function insumo_gestion_ejecutado($proy_id, $gestion,$pfec_ejecucion)
    {
       $sql = 'select * from insumo_ejecutado_gestion('.$proy_id.','.$gestion.','.$pfec_ejecucion.')';

        $query = $this->db->query($sql);
        return $query->result_array();
    }



    /*------------ GET INSUMOS PROGRAMADO (reportes-programacion) ANUAL --------*/
    public function list_insumo_prog($act_id, $gestion)
    {
       $sql = 'select *
                from vrelacion_proy_prod_act_ins i
                INNER JOIN insumo_gestion ig ON ig.ins_id = i.ins_id
                INNER JOIN vifin_prog_mes ip ON ip.insg_id = ig.insg_id
                where i.act_id='.$act_id.' and ig.g_id='.$gestion.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*------------ SUMA TOTAL PROGRAMADADO INSUMO POR MESES (reportes-programacion) ANUAL--------*/
    public function sum_total_insumo_prog($proy_id,$gestion)
    {
       $sql = 'select prod.proy_id,SUM(programado_total) as total, SUM(mes1) as mes1,SUM(mes2) as mes2, SUM(mes3) as mes3, SUM(mes4) as mes4, SUM(mes5) as mes5, SUM(mes6) as mes6,
                SUM(mes7) as mes7, SUM(mes8) as mes8, SUM(mes9) as mes9, SUM(mes10) as mes10, SUM(mes11) as mes11, SUM(mes12) as mes12
                from vista_producto prod
                INNER JOIN vrelacion_proy_prod_act_ins i ON i.prod_id = prod.prod_id
                INNER JOIN insumo_gestion ig ON ig.ins_id = i.ins_id
                INNER JOIN vifin_prog_mes ip ON ip.insg_id = ig.insg_id
                where prod.proy_id='.$proy_id.' and prod.estado!=\'3\' and ig.g_id='.$gestion.'
                group by prod.proy_id
                order by prod.proy_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*------------ SUMA TOTAL EJECUTADO INSUMO POR MESES (reportes-programacion) ANUAL --------*/
    public function sum_total_insumo_ejec($proy_id,$gestion)
    {
       $sql = 'select prod.proy_id,SUM(ejecutado_total) as total, SUM(mes1) as mes1,SUM(mes2) as mes2, SUM(mes3) as mes3, SUM(mes4) as mes4, SUM(mes5) as mes5, SUM(mes6) as mes6,
                SUM(mes7) as mes7, SUM(mes8) as mes8, SUM(mes9) as mes9, SUM(mes10) as mes10, SUM(mes11) as mes11, SUM(mes12) as mes12
                from vista_producto prod
                INNER JOIN vrelacion_proy_prod_act_ins i ON i.prod_id = prod.prod_id
                INNER JOIN insumo_gestion ig ON ig.ins_id = i.ins_id
                INNER JOIN vifin_ejec_mes ip ON ip.insg_id = ig.insg_id
                where prod.proy_id='.$proy_id.' and prod.estado!=\'3\' and ig.g_id='.$gestion.'
                group by prod.proy_id
                order by prod.proy_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

        /*------------ GET INSUMOS PROGRAMADO (reportes-programacion) PLURI ANUAL --------*/
    public function list_insumo_prog_pluri($act_id)
    {
       $sql = 'select *
                from vrelacion_proy_prod_act_ins i
                INNER JOIN insumo_gestion ig ON ig.ins_id = i.ins_id
                INNER JOIN vifin_prog_mes ip ON ip.insg_id = ig.insg_id
                where i.act_id='.$act_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*------------ SUMA TOTAL PROGRAMADADO INSUMO POR MESES (reportes-programacion) PLURI ANUAL--------*/
    public function sum_total_insumo_prog_pluri($proy_id)
    {
       $sql = 'select prod.proy_id,SUM(programado_total) as total, SUM(mes1) as mes1,SUM(mes2) as mes2, SUM(mes3) as mes3, SUM(mes4) as mes4, SUM(mes5) as mes5, SUM(mes6) as mes6,
                SUM(mes7) as mes7, SUM(mes8) as mes8, SUM(mes9) as mes9, SUM(mes10) as mes10, SUM(mes11) as mes11, SUM(mes12) as mes12
                from vista_producto prod
                INNER JOIN vrelacion_proy_prod_act_ins i ON i.prod_id = prod.prod_id
                INNER JOIN insumo_gestion ig ON ig.ins_id = i.ins_id
                INNER JOIN vifin_prog_mes ip ON ip.insg_id = ig.insg_id
                where prod.proy_id='.$proy_id.' and prod.estado!=\'3\'
                group by prod.proy_id
                order by prod.proy_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*------------ SUMA TOTAL EJECUTADO INSUMO POR MESES (reportes-programacion) PLURI ANUAL --------*/
    public function sum_total_insumo_ejec_pluri($proy_id)
    {
       $sql = 'select prod.proy_id,SUM(ejecutado_total) as total, SUM(mes1) as mes1,SUM(mes2) as mes2, SUM(mes3) as mes3, SUM(mes4) as mes4, SUM(mes5) as mes5, SUM(mes6) as mes6,
                SUM(mes7) as mes7, SUM(mes8) as mes8, SUM(mes9) as mes9, SUM(mes10) as mes10, SUM(mes11) as mes11, SUM(mes12) as mes12
                from vista_producto prod
                INNER JOIN vrelacion_proy_prod_act_ins i ON i.prod_id = prod.prod_id
                INNER JOIN insumo_gestion ig ON ig.ins_id = i.ins_id
                INNER JOIN vifin_ejec_mes ip ON ip.insg_id = ig.insg_id
                where prod.proy_id='.$proy_id.' and prod.estado!=\'3\'
                group by prod.proy_id
                order by prod.proy_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }


    /*------------ SUMA TOTAL A NIVEL INSTITUCIONAL PROGRAMADO INSUMO ACTIVIDAD (DIRECTO)--------*/
    public function sum_total_programado_institucional_directo($gestion)
    {
       $sql = 'select SUM(programado_total) as total, SUM(mes1) as mes1,SUM(mes2) as mes2, SUM(mes3) as mes3, SUM(mes4) as mes4, SUM(mes5) as mes5, SUM(mes6) as mes6,
                SUM(mes7) as mes7, SUM(mes8) as mes8, SUM(mes9) as mes9, SUM(mes10) as mes10, SUM(mes11) as mes11, SUM(mes12) as mes12
                from vista_producto prod
                INNER JOIN vrelacion_proy_prod_act_ins i ON i.prod_id = prod.prod_id
                INNER JOIN insumo_gestion ig ON ig.ins_id = i.ins_id
                INNER JOIN vifin_prog_mes ip ON ip.insg_id = ig.insg_id
                where prod.estado!=\'3\' and ig.g_id='.$gestion.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*------------ SUMA TOTAL A NIVEL INSTITUCIONAL EJECUTADO INSUMO ACTIVIDAD (DIRECTO)--------*/
    public function sum_total_ejecutado_institucional_directo($gestion)
    {
       $sql = 'select SUM(ejecutado_total) as total, SUM(mes1) as mes1,SUM(mes2) as mes2, SUM(mes3) as mes3, SUM(mes4) as mes4, SUM(mes5) as mes5, SUM(mes6) as mes6,
                SUM(mes7) as mes7, SUM(mes8) as mes8, SUM(mes9) as mes9, SUM(mes10) as mes10, SUM(mes11) as mes11, SUM(mes12) as mes12
                from vista_producto prod
                INNER JOIN vrelacion_proy_prod_act_ins i ON i.prod_id = prod.prod_id
                INNER JOIN insumo_gestion ig ON ig.ins_id = i.ins_id
                INNER JOIN vifin_ejec_mes ip ON ip.insg_id = ig.insg_id
                where prod.estado!=\'3\' and ig.g_id='.$gestion.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------- LISTA PRODUCTOS ------------*/
    public function lista_productos($proy_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vista_producto');
        $this->db->WHERE('proy_id', $proy_id);
        $this->db->WHERE('estado IN (1,2)');
        $this->db->ORDER_BY('prod_id', 'ASC');
        //$this->db->WHERE("(cast(to_char(fecha,'yyyy')as integer))=" . $gestion);
        $query = $this->db->get();
        return $query->result_array();
    }

    //LISTA DE ACTIVIDADES FILTRADO POR PRODUCTO ANUAL
    public function lista_actividades($prod_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vista_actividad');
        $this->db->WHERE('prod_id', $prod_id);
        $this->db->ORDER_BY('act_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function dato_proyecto($proy_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('_proyectos');
        $this->db->WHERE('proy_id', $proy_id);
        $query = $this->db->get();
        return $query->row();
    }

    //OBTENER PRESUPUESTO DEL PROYECTO
    public function tabla_presupuesto($proy_id, $gestion)
    {
        $sql = 'SELECT * FROM fnfinanciamiento_proy(' . $proy_id . ',' . $gestion . ')';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //DATO DE PRODUCTOS
    public function dato_producto($prod_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vista_producto');
        $this->db->WHERE('prod_id', $prod_id);
        $query = $this->db->get();
        return $query->row();
    }

    //DATO DE ACTIVIDADES
    public function dato_actividad($act_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vista_actividad');
        $this->db->WHERE('act_id', $act_id);
        $query = $this->db->get();
        return $query->row();
    }
    //DATO DE COMPONENTE
    public function dato_componente($com_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('_componentes');
        $this->db->WHERE('com_id', $com_id);
        $query = $this->db->get();
        return $query->row();
    }
    //GET PARTIDAS
    function get_partida($par_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('partidas');
        $this->db->WHERE('par_id', $par_id);
        // $this->db->WHERE('par_gestion',$this->gestion);
        $this->db->ORDER_BY('par_codigo', 'ASC');
        $query = $this->db->get();
        return $query->row();
    }

    //LISTA PARTIDAS DEPENDIENTES
    function lista_par_dependientes($par_codigo)
    {
        $this->db->SELECT('*');
        $this->db->FROM('partidas');
        $this->db->WHERE('par_depende', $par_codigo);
        //$this->db->WHERE('par_gestion',$this->gestion);
        $this->db->ORDER_BY('par_codigo', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    //GUARDAR INSUMO
    function guardar_insumo($data_insumo, $post, $act_id, $cant_fin)
    {
        $this->db->trans_begin();
        $ins_id = $this->guardar_tabla_insumo($data_insumo);// GUARDAR EN MI TABLA INSUMO
        $this->guardar_prog_ins($post, $ins_id, $cant_fin);//guardar la programacion mensual de insumo
        $this->add_insumo_actividad($act_id, $ins_id);//guardar relacion en la tabla insumos actividad
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            return $this->db->trans_commit();
        }
    }

    function  guardar_tabla_insumo($data_insumo)
    {
        $data = $this->genera_codigo(($data_insumo['ins_tipo']));
        $data_insumo['ins_codigo'] = $data['codigo'];//guardar codigo
        $data_insumo['ins_gestion'] = $this->gestion;
        $data_insumo['fun_id'] = $this->fun_id;
        $this->db->insert('insumos', $data_insumo);
        $ins_id = $this->db->insert_id();
        $this->actualizar_conf_ins($data['cont'],($data_insumo['ins_tipo'])); //actualizar mi contador de codigo de insumos
        return $ins_id;
    }

    //guardar relacion insumo actividad
    function add_insumo_actividad($act_id, $ins_id)
    {
        $data = array('act_id' => $act_id, 'ins_id' => $ins_id);
        $this->db->INSERT('_insumoactividad', $data);
    }

    //GUARDAR PROGRAMACION MENSUAL DE INSUMO
    function guardar_prog_ins($post, $ins_id, $cant_fin)
    {
        for ($i = 1; $i <= $cant_fin; $i++) {
            $monto_asignado = $post[('ins_monto' . $i)];
            if ($monto_asignado != 0) {
                $data = array(//GUARDAR EN INSUMOFINANCIAMIENTO
                    'ins_id' => $ins_id,
                    'ff_id' => $post[('ff' . $i)],
                    'of_id' => $post[('of' . $i)],
                    'et_id' => $post[('ins_et' . $i)],
                    'ifin_monto' => $monto_asignado,
                    'ifin_gestion' => $this->gestion
                );
                $this->db->INSERT('insumofinanciamiento', $data);
                $ifin_id = $this->db->insert_id();
                for ($j = 1; $j <= 12; $j++) {
                    $mes = $post[('mes' . $i . $j)];
                    if ($mes != 0) {
                        $data = array(
                            'ifin_id' => $ifin_id,
                            'mes_id' => $j,
                            'ipm_fis' => $mes
                        );
                        $this->db->INSERT('ifin_prog_mes', $data);
                    }
                }
            }

        }
    }

    //GENERAR CODIGO DEL INSUMO
    function genera_codigo($tipo)
    {
        $cont = $this->get_cont($tipo);
        $cont++;
        $codigo = 'SIIP/INS/' . $this->get_tipo($tipo) . '/' . $this->gestion . '/0' . $cont;
        $data['cont'] = $cont;
        $data['codigo'] = $codigo;
        return $data;
    }

    //OBTENER CONTADOR DE ID POR EL TIPO DE INSUMO
    function get_cont($tipo)
    {
        switch ($tipo) {
            case 1:
                $this->db->SELECT('conf_rrhhp');
                $this->db->FROM('configuracion');
                $this->db->WHERE('ide', $this->gestion);
                $query = $this->db->get();
                return $query->row()->conf_rrhhp;
                break;
            case 2:
                $this->db->SELECT('conf_servicios');
                $this->db->FROM('configuracion');
                $this->db->WHERE('ide', $this->gestion);
                $query = $this->db->get();
                return $query->row()->conf_servicios;
                break;
            case 3:
                $this->db->SELECT('conf_pasajes');
                $this->db->FROM('configuracion');
                $this->db->WHERE('ide', $this->gestion);
                $query = $this->db->get();
                return $query->row()->conf_pasajes;
                break;
            case 4:
                $this->db->SELECT('conf_viaticos');
                $this->db->FROM('configuracion');
                $this->db->WHERE('ide', $this->gestion);
                $query = $this->db->get();
                return $query->row()->conf_viaticos;
                break;
            case 5:
                $this->db->SELECT('conf_cons_producto');
                $this->db->FROM('configuracion');
                $this->db->WHERE('ide', $this->gestion);
                $query = $this->db->get();
                return $query->row()->conf_cons_producto;
                break;
            case 6:
                $this->db->SELECT('conf_cons_linea');
                $this->db->FROM('configuracion');
                $this->db->WHERE('ide', $this->gestion);
                $query = $this->db->get();
                return $query->row()->conf_cons_linea;
                break;
            case 7:
                $this->db->SELECT('conf_materiales');
                $this->db->FROM('configuracion');
                $this->db->WHERE('ide', $this->gestion);
                $query = $this->db->get();
                return $query->row()->conf_materiales;
                break;
            case 8:
                $this->db->SELECT('conf_activos');
                $this->db->FROM('configuracion');
                $this->db->WHERE('ide', $this->gestion);
                $query = $this->db->get();
                return $query->row()->conf_activos;
                break;
            case 9:
                $this->db->SELECT('conf_otros_insumos');
                $this->db->FROM('configuracion');
                $this->db->WHERE('ide', $this->gestion);
                $query = $this->db->get();
                return $query->row()->conf_otros_insumos;
                break;
        }
    }

    //OBTENER EL TIPO DE INSUMO
    function get_tipo($tipo)
    {
        switch ($tipo) {
            case 1:
                return 'RHP';
                break;
            case 2:
                return 'SER';
                break;
            case 3:
                return 'PAS';
                break;
            case 4:
                return 'VIA';
                break;
            case 5:
                return 'CPP';
                break;
            case 6:
                return 'CL';
                break;
            case 7:
                return 'MAT';
                break;
            case 8:
                return 'AF';
                break;
            case 9:
                return 'OI';
                break;
        }
    }

    //ACTUALIZA EL CONTADOR DE MI CODIGO DE INSUMOS
    function actualizar_conf_ins($cont,$tipo)
    {
        switch ($tipo) {
            case 1:
                $data['conf_rrhhp'] = $cont;
                break;
            case 2:
                $data['conf_servicios'] = $cont;
                break;
            case 3:
                $data['conf_pasajes'] = $cont;
                break;
            case 4:
                $data['conf_viaticos'] = $cont;
                break;
            case 5:
                $data['conf_cons_producto'] = $cont;
                break;
            case 6:
                $data['conf_cons_linea'] = $cont;
                break;
            case 7:
                $data['conf_materiales'] = $cont;
                break;
            case 8:
                $data['conf_activos'] = $cont;
                break;
            case 9:
                $data['conf_otros_insumos'] = $cont;
                break;
        }
        $this->db->WHERE('ide', $this->gestion);
        $this->db->UPDATE('configuracion ', $data);
    }

    //LISTA DE CARGOS
    function lista_cargo()
    {
        $this->db->FROM('cargo');
        $this->db->WHERE('(car_estado = 1 OR car_estado = 1)');
        $query = $this->db->get();
        return $query->result_array();
    }

    //LISTA DE INSUMOS  NIVEL DE ACTIVIDADES 
    function lista_insumos($act_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vrelacion_insumo_partida_actividad');
        $this->db->WHERE('act_id', $act_id);
        //$this->db->WHERE('insp_gestion', $this->gestion);
        $this->db->ORDER_BY('insp_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    //Lista de insumos por componente
    function lista_insumos_com($act_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vrelacion_insumo_partida_componente');
        $this->db->WHERE('com_id', $act_id);
        //$this->db->WHERE('insp_gestion', $this->gestion);
        $this->db->ORDER_BY('insp_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function insumos_partida_mensual($insp_id,$gest)
    {
        $this->db->from('insumo_partida_gestion');
        $this->db->where('insp_id', $insp_id);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function insumos_partida_pres_anual($insp_id,$gest){
        $sql='select sum(inspg_monto_prog) as p_total from insumo_partida_gestion
              where insp_id='.$insp_id.' and g_id='.$gest;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function insumos_partida_pres_total($insp_id){
        $sql='select sum(inspg_monto_prog) as p_total from insumo_partida_gestion
              where insp_id='.$insp_id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    //SUMA  DEL COSTO TOTAL DE INSUMOS
    function get_suma_costo_total($act_id)
    {
        $this->db->FROM("fnsuma_costo_total_ins(".$act_id.")");
        $query = $this->db->get();
        return $query->row();
    }

    //SALDO TOTAL DE FINANCIAMIENTO
    function saldo_total_fin($proy_id,$gestion){
        $sql = 'SELECT * FROM fnsaldo_total_fin(' . $proy_id . ',' . $gestion . ')';
        $query = $this->db->query($sql);
        return $query->row();
    }

    //LISTA DE INSUMOS FILTRADO POR TIPO DE INSUMO
    function lista_insumos_tipo($act_id,$tipo)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vrelacion_proy_prod_act_ins');
        $this->db->WHERE('act_id', $act_id);
        $this->db->WHERE('ins_tipo', $tipo);
     //   $this->db->WHERE('ins_gestion', $this->gestion);
        $this->db->ORDER_BY('ins_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    //LISTA PROGRAMADO MENSUAL DEL INSUMO FINANCIAMIENTO
    function lista_progmensual_ins($ins_id){
        $this->db->SELECT('*');
        $this->db->FROM('v_ins_financiamiento_programado');
        $this->db->WHERE('ins_id', $ins_id);
       // $this->db->WHERE('ifin_gestion', $this->gestion);
        $this->db->ORDER_BY('ins_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_partida_codigo($par_codigo)
    {
        $this->db->SELECT('*');
        $this->db->FROM('partidas');
        $this->db->WHERE('par_codigo', $par_codigo);
        $query = $this->db->get();
        return $query->result_array();
    }

    //DATOS DEL INSUMO
    function get_insumo($ins_id){
        $this->db->SELECT('*');
        $this->db->FROM('vlista_insumos');
        $this->db->WHERE('ins_id',$ins_id);
        $query = $this->db->get();
        return $query->row();
    }

    //DATOS DEL INSUMO + PROGRAMADO (wilmer)
    function suma_dato_insumo_programado($ins_id,$gestion){
        $sql = '
            select SUM(infp.suma) as suma
            from insumo_gestion ig
            Inner Join (select * from insumo_financiamiento) as inf On inf.insg_id=ig.insg_id
            Inner Join (select SUM(ipm_fis) as suma, ifin_id
            from ifin_prog_mes
            GROUP BY ifin_id) as infp On infp.ifin_id=inf.ifin_id
            where ig.ins_id='.$ins_id.' and ig.g_id='.$gestion.' and ig.insg_estado!=\'3\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //DATOS DEL INSUMO (wilmer)
    function get_dato_insumo($insp_id){
        $sql = '
            select *
            from insumos_partida i
            Inner Join (select g.par_id as gid, g.par_codigo as cod1, g.par_nombre as grupo, p.par_id as pid, p.par_codigo as cod2, p.par_nombre as partida
            from partidas p
            Inner Join partidas as g On g.par_codigo=p.par_depende) as pr On pr.pid=i.par_id
            where insp_id='.$insp_id.' and insp_estado!=\'3\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //DATOS DEL INSUMO (wilmer-nuevo)
    function get_dato_insumo_partida($insp_id,$pfec_ejecucion){
        // 1: actividad
        // 2: Componente
        if($pfec_ejecucion==1){
            $sql = '
                select *
                from insumos_partida i
                Inner Join insumos_partida_actividad ia ON ia.insp_id = i.insp_id
                Inner Join (select g.par_id as gid, g.par_codigo as cod1, g.par_nombre as grupo, p.par_id as pid, p.par_codigo as cod2, p.par_nombre as partida
                from partidas p
                Inner Join partidas as g On g.par_codigo=p.par_depende) as pr On pr.pid=i.par_id
                where i.insp_id='.$insp_id.' and i.insp_estado!=\'3\'';
        }
        else{
            $sql = '
                select *
                from insumos_partida i
                Inner Join insumos_partida_componente ic ON ic.insp_id = i.insp_id
                Inner Join (select g.par_id as gid, g.par_codigo as cod1, g.par_nombre as grupo, p.par_id as pid, p.par_codigo as cod2, p.par_nombre as partida
                from partidas p
                Inner Join partidas as g On g.par_codigo=p.par_depende) as pr On pr.pid=i.par_id
                where i.insp_id='.$insp_id.' and i.insp_estado!=\'3\'';
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //DATOS DEL INSUMO GESTION (wilmer)
    function get_dato_insumo_gestion($inspg_id,$insp_id){
        $sql = 'select *
                from insumo_partida_gestion
                where inspg_id='.$inspg_id.' and insp_id='.$insp_id.' and inspg_estado!=\'3\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //DATOS DEL INSUMO GESTION ACTUAL (wilmer)
    function get_dato_insumo_gestion_actual($ins_id){
        $sql = 'select *
                from insumo_gestion
                where ins_id='.$ins_id.' and g_id='.$this->gestion.' and insg_estado!=\'3\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //LISTA DE INSUMOS GESTION (wilmer)
    function list_insumos_gestion($insp_id){
        $sql = 'select *
                from insumo_partida_gestion
                where insp_id='.$insp_id.' and inspg_estado!=\'3\' order by g_id';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //GET DE INSUMOS GESTION (wilmer)
    function get_insumo_gestion($insp_id,$gestion){
        $sql = 'select *
                from insumo_partida_gestion
                where insp_id='.$insp_id.' and inspg_estado!=\'3\' and g_id='.$gestion.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*========== LISTA DE INSUMOS FINANCIAMIENTOS (Wilmer) ==========*/
    public function list_insumo_financiamiento($insg_id)
    {
        $sql = 'select *
                from insumo_financiamiento 
                where insg_id='.$insg_id.' and ifin_estado!=\'3\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*========== GET INSUMO FINANCIAMIENTO PROGRAMADO (Wilmer) Para el registro de programacion ==========*/
    public function get_insumo_financiamiento($insg_id,$ffofet_id,$gestion,$nro)
    {
        $sql = 'select *
                from vifin_prog_mes 
                where insg_id='.$insg_id.' and ffofet_id='.$ffofet_id.' and ifin_gestion='.$gestion.' and nro_if='.$nro.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*========== GET INSUMO FINANCIAMIENTO EJECUTADO (Wilmer) Para la ejecucion de programacion ==========*/
    public function get_insumo_financiamiento_ejec($ifin_id,$gestion)
    {
        $sql = 'select *
                from vifin_ejec_mes 
                where ifin_id='.$ifin_id.'and ifin_gestion='.$gestion.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*========== GET INSUMO FINANCIAMIENTO PROGRAMADO (Wilmer) Para el registro de programacion ==========*/
/*    public function get_insumo_financiamiento($insg_id,$ffofet_id,$gestion,$nro)
    {
        $sql = 'select *
                from vifin_prog_mes 
                where insg_id='.$insg_id.' and ffofet_id='.$ffofet_id.' and ifin_gestion='.$gestion.' and nro_if='.$nro.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }*/

    /*========== LITA DE PROGRAMACION MENSUAL (Wilmer) ==========*/
    public function get_list_insumo_financiamiento($insg_id)
    {
        $sql = 'select *
                from vifin_prog_mes 
                where insg_id='.$insg_id.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*========== GET EJECUCION FINANCIERA (Wilmer) ==========*/
    public function get_insumo_ejec($ifin_id)
    {
        $sql = 'select *
                from vifin_ejec_mes 
                where ifin_id='.$ifin_id.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

        /*------------------ monto programado por fuentes -------------------*/
    public function suma_monto_prog_insumo($of_id,$ff_id,$gestion,$tp_ejec)
    {
        if($tp_ejec==1){
            $sql = 'select sum(inspg_monto_prog) as programado from insumo_partida_gestion ipg
                    inner join insumos_partida as ip on ipg.insp_id=ip.insp_id
                    where insp_estado!=3 and inspg_estado!=3 and ipg.g_id='.$gestion.' and ip.of_id='.$of_id.' and ip.ff_id='.$ff_id;
        }
        else{
            $sql = 'select SUM(programado_total) as programado
                from vproy_insumo_delegado_programado
                where ffofet_id='.$of_id.' and g_id='.$gestion;
        }
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function presupuesto_total_gestion($proy_id,$gestion,$tp_ejec){
        if ($tp_ejec==1){
            $sql = 'select ip.ff_id,ff.ff_codigo,ff.ff_descripcion,ip.of_id,of.of_codigo, of.of_descripcion, sum(inspg_monto_prog) as programado from insumo_partida_gestion ipg
                    inner join insumos_partida as ip on ipg.insp_id=ip.insp_id
                    inner join organismofinanciador as of on ip.of_id=of.of_id
                    inner join fuentefinanciamiento as ff on ip.ff_id=ff.ff_id
                    inner join insumos_partida_actividad as ipa on ip.insp_id=ipa.insp_id
                    where insp_estado!=3 and inspg_estado!=3 and ipg.g_id='.$gestion.' and ipa.act_id in(SELECT
                    _actividades.act_id
                    FROM
                    _productos
                    INNER JOIN _actividades ON _actividades.prod_id = _productos.prod_id
                    INNER JOIN _componentes ON _componentes.com_id = _productos.com_id
                    INNER JOIN _proyectofaseetapacomponente ON _componentes.pfec_id = _proyectofaseetapacomponente.pfec_id
                    WHERE
                    _actividades.estado != 3 AND
                    _componentes.estado != 3 AND
                    _productos.estado != 3 AND 
                    _proyectofaseetapacomponente.proy_id='.$proy_id.')
                    GROUP BY ip.of_id,of_codigo,of_descripcion,ip.ff_id,ff_codigo,ff_descripcion';
        }else{
            $sql = 'select ip.ff_id,ff.ff_codigo,ff.ff_descripcion,ip.of_id,of.of_codigo, of.of_descripcion, sum(inspg_monto_prog) as programado from insumo_partida_gestion ipg
                    inner join insumos_partida as ip on ipg.insp_id=ip.insp_id
                    inner join organismofinanciador as of on ip.of_id=of.of_id
                    inner join fuentefinanciamiento as ff on ip.ff_id=ff.ff_id
                    inner join insumos_partida_componente as ipa on ip.insp_id=ipa.insp_id
                    where insp_estado!=3 and inspg_estado!=3 and ipg.g_id='.$gestion.' and 
                    ipa.com_id in (select com_id from _componentes
                    where pfec_id in(
                    select pfec_id from _proyectofaseetapacomponente
                    where proy_id='.$proy_id.') and estado!=3)
                    GROUP BY ip.of_id,of_codigo,of_descripcion,ip.ff_id,ff_codigo,ff_descripcion';
        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function presupuesto_programado_act($act_id,$gestion)
    {
        $sql = 'select ip.ff_id,ff.ff_codigo,ff.ff_descripcion,ip.of_id,of.of_codigo, of.of_descripcion, sum(inspg_monto_prog) as programado from insumo_partida_gestion ipg
                    inner join insumos_partida as ip on ipg.insp_id=ip.insp_id
                    inner join organismofinanciador as of on ip.of_id=of.of_id
                    inner join fuentefinanciamiento as ff on ip.ff_id=ff.ff_id
                    inner join insumos_partida_actividad as ipa on ip.insp_id=ipa.insp_id
                    where insp_estado!=3 and inspg_estado!=3 and ipg.g_id='.$gestion.' and ipa.act_id='.$act_id.'
                    GROUP BY ip.of_id,of_codigo,of_descripcion,ip.ff_id,ff_codigo,ff_descripcion';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function presupuesto_programado_componente($comp_id,$gestion)
    {
        $sql = 'select ip.ff_id,ff.ff_codigo,ff.ff_descripcion,ip.of_id,of.of_codigo, of.of_descripcion, sum(inspg_monto_prog) as programado from insumo_partida_gestion ipg
                    inner join insumos_partida as ip on ipg.insp_id=ip.insp_id
                    inner join organismofinanciador as of on ip.of_id=of.of_id
                    inner join fuentefinanciamiento as ff on ip.ff_id=ff.ff_id
                    inner join insumos_partida_componente as ipa on ip.insp_id=ipa.insp_id
                    where insp_estado!=3 and inspg_estado!=3 and ipg.g_id='.$gestion.' and ipa.com_id='.$comp_id.'
                    GROUP BY ip.of_id,of_codigo,of_descripcion,ip.ff_id,ff_codigo,ff_descripcion';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function suma_monto_asignado_insumo($proy_id,$of_id,$ff_id,$gestion,$tp_ejec)
    {
        if($tp_ejec==1){
            $sql = 'SELECT sum(r.ffofet_monto) as asignado
	FROM _proyectofaseetapacomponente p
	INNER JOIN ptto_fase_gestion q ON q.pfec_id = p.pfec_id
	INNER JOIN _ffofet r ON r.ptofecg_id = q.ptofecg_id 
WHERE p.proy_id ='.$proy_id.' and q.g_id='.$gestion.' and ff_id='.$ff_id.' and of_id='.$of_id;
        }
        else{
            $sql = 'select SUM(r.ffofet_monto) as asignado
                	FROM _proyectofaseetapacomponente p
	INNER JOIN ptto_fase_gestion q ON q.pfec_id = p.pfec_id
	INNER JOIN _ffofet r ON r.ptofecg_id = q.ptofecg_id 
                where p.proy_id='.$proy_id.'and of_id='.$of_id.' and q.g_id='.$gestion.' and ff_id='.$ff_id;
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }


    //OBTENER LA PROGRAMACION MENSUAL DEL INSUMO (a borrar)
    public function ins_prog_mensual($ins_id,$ff_id,$of_id){
        $this->db->SELECT('*');
        $this->db->FROM('v_ins_financiamiento_programado');
        $this->db->WHERE('ins_id',$ins_id);
        $this->db->WHERE('ff_id',$ff_id);
        $this->db->WHERE('of_id',$of_id);
        $query = $this->db->get();
        return $query;
    }
    //CERRAR PROGRAMACION DE INSUMO
    function cerrar_insumo($proy_id){
        $data['proy_estado'] = 3;
        $this->db->WHERE('proy_id',$proy_id);
        return $this->db->UPDATE('_proyectos',$data);
    }

    //MODIFICAR INSUMO
    function modificar_insumo($data_insumo, $post,$cant_fin,$ins_id){
        $this->db->trans_begin();
        $this->mod_tabla_insumo($data_insumo,$ins_id);// MODIFICAR EN MI TABLA INSUMO
        $this->mod_prog_ins($post, $ins_id, $cant_fin);//MODIFICAR la programacion mensual de insumo
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            return $this->db->trans_commit();
        }
    }

    //MODIFICAR TABLA INSUMO
    function  mod_tabla_insumo($data_insumo,$ins_id)
    {
        $data_insumo['ins_gestion'] = $this->gestion;
        $data_insumo['fun_id'] = $this->fun_id;
        $this->db->WHERE('ins_id', $ins_id);
        $this->db->UPDATE('insumos', $data_insumo);
    }

    //MODIFICAR PROGRAMACION MENSUAL DE INSUMO
    function mod_prog_ins($post, $ins_id, $cant_fin)
    {
        //LIMPIAR INSUMOS
        $this->limpiar_ins_financiamiento($ins_id);
        //GUARDAR NUEVO INSUMO
        $this->guardar_prog_ins($post, $ins_id, $cant_fin);
    }
    //LIMPIAR INSUMO FINANCIAMIENTO
    function limpiar_ins_financiamiento($ins_id){
        $lista_ins_fin = $this->get_ins_fin($ins_id);//LISTA DE INSUMOS DE FINANCIAMIENTOS
        foreach($lista_ins_fin AS $row){
            $this->del_ifin_prog_mes($row['ifin_id']);
        }
        $this->del_ins_fin($ins_id);
    }

    //ELIMINAR LA TABLA DE PROGRAMACION MENSUAL DE INSUMOS
    function del_ifin_prog_mes($ifin_id){
        $this->db->WHERE('ifin_id', $ifin_id);
        $this->db->DELETE('ifin_prog_mes');
    }

    //OBTENER INSUMO FINANCIAMIENTO
    function get_ins_fin($ins_id){
        $this->db->SELECT('*');
        $this->db->FROM('insumofinanciamiento');
        $this->db->WHERE('ins_id',$ins_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    //ELIMINAR INSUMO FINANCIAMIENTO
    function del_ins_fin($ins_id){
        $this->db->WHERE('ins_id', $ins_id);
        $this->db->DELETE('insumofinanciamiento');
    }

    //
    function eliminar_insumo($ins_id)
    {
        $this->db->trans_begin();
        $this->del_prog_ins($ins_id);
        $this->del_insumos($ins_id);
        $this->del_ins_act($ins_id);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            return $this->db->trans_commit();
        }
    }

    //eliminar tabla insumos
    function del_insumos($ins_id)
    {
        $dato['ins_estado'] = 3;
        $this->db->WHERE('ins_id', $ins_id);
        $this->db->UPDATE('insumos', $dato);
    }

    //eliminar promacion del insumo
    function del_prog_ins($ins_id)
    {
        $lista_ifin = $this->lista_ins_fin($ins_id);
        foreach ($lista_ifin as $row) {
            $this->del_ifin($row['ifin_id']);//eliminar la programacion del insumo
        }
        $this->ifin_eliminar($ins_id);//eliminar insumo financiamiento

    }

    function lista_ins_fin($ins_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('insumofinanciamiento');
        $this->db->WHERE('ins_id', $ins_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    //eliminar programacion mes
    function del_ifin($ifin_id)
    {
        $this->db->WHERE('ifin_id', $ifin_id);
        $this->db->DELETE('ifin_prog_mes');
    }

    //cambiar a estado de eliminacion
    function ifin_eliminar($ins_id)
    {
        $dato['ifin_estado'] = 3;
        $this->db->WHERE('ins_id', $ins_id);
        $this->db->UPDATE('insumofinanciamiento', $dato);
    }

    //eliminar relacion insumo actividad
    function del_ins_act($ins_id)
    {
        $this->db->WHERE('ins_id',$ins_id);
        $this->db->DELETE('_insumoactividad');
    }
}
?>