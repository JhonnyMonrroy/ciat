<?php
class Model_reporte_global extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
    }

    /*=================== TIPO DE PROYECTOS PARA REPORTE GLOBAL=============================================================*/
    public function tip_proy($p1,$p2,$p3,$p4)
    {
        $tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = '
        select tp.*
        from _tipoproyecto as tp
        where tp.tp_estado!=0 and (tp_id='.$tp1.' or tp_id='.$tp2.' or tp_id='.$tp3.' or tp_id='.$tp4.')'; 
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*======================= CANTIDAD REPORTE GLOBAL ======================================*/
    public function cant_accion($tp_id,$gestion)
    {
        $sql = 'select p.*,fe.*
                from _proyectos as p
                Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                Inner Join _proyectofaseetapacomponente as fe On p.proy_id=fe.proy_id
                where p.tp_id='.$tp_id.' and p.estado!=\'3\' and tap.aper_gestion='.$gestion.' and fe.pfec_estado=\'1\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*======================= PRESUPUESTO EJECUCION REPORTE GLOBAL ======================================*/
    public function cant_presupuesto_ejecucion($proy_id,$mes_id,$gestion)
    {
        $sql = 'select p.proy_id, p.proy_nombre,
                SUM(e.pem_ppto_inicial) as inicial, SUM(e.pem_ppto_vigente)as vigente, SUM(e.pem_modif_aprobadas) as modificado,SUM(e.pem_devengado) as ejecutado
                from proy_ejec_mes as e 
                Inner Join _proyectos as p On e.proy_id = p.proy_id
                where e.mes_id='.$mes_id.' and e.gestion='.$gestion.' and p.proy_id='.$proy_id.'
                group by p.proy_id';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*================================= REPORTE GLOBAL UNIDADES EJECUTORAS ======================================*/
    public function unidades_ejecutoras_global($gestion,$p1,$p2,$p3,$p4)
    {
        $tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = 'select ue.uni_id,ue.uni_unidad, count(ue.uni_id)as nro
                from unidadorganizacional as ue
                Inner Join _proyectofuncionario as pf On pf.uni_ejec=ue.uni_id
                Inner Join _proyectos as p On p.proy_id=pf.proy_id
                Inner Join _proyectofaseetapacomponente as fe On p.proy_id=fe.proy_id
                Inner Join (select apy.*, apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id and apg.aper_gestion='.$gestion.') as tap On p.proy_id=tap.proy_id
                where ue.uni_ejecutora=\'1\' and pf.pfun_tp=\'1\' and p.estado!=\'3\' and fe.pfec_estado=\'1\' and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.')
                group by ue.uni_id
                order by ue.uni_id';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*================================= REPORTE GLOBAL PROYECTOS POR UNIDAD EJECUTORA ======================================*/
    public function proyectos_ue_global($uni_id,$gestion,$p1,$p2,$p3,$p4)
    {
        $tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = 'select p.*,fe.*,tap.*,pf.*,f.*,tp.*,ue.*
                from unidadorganizacional as ue
                Inner Join _proyectofuncionario as pf On pf.uni_ejec=ue.uni_id
                Inner Join funcionario as f On pf.fun_id=f.fun_id
                Inner Join _proyectos as p On p.proy_id=pf.proy_id
                Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                Inner Join _proyectofaseetapacomponente as fe On p.proy_id=fe.proy_id
                Inner Join (select apy.*, apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id and apg.aper_gestion='.$gestion.') as tap On p.proy_id=tap.proy_id
                where ue.uni_id='.$uni_id.' and pf.pfun_tp=\'1\' and p.estado!=\'3\' and fe.pfec_estado=\'1\' and pf.pfun_tp=\'1\' and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.')';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function proyectos_ue_global_total($p1,$p2,$p3,$p4)
    {
        $tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = 'select uad.uni_id as das_id,uad.uni_unidad as das,p.proy_id,p.proy_nombre,pf.*,tp.tp_tipo,pe.*,u.uni_unidad as u_ejec
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On ap.proy_id = p.proy_id
                Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join unidadorganizacional as uad On uad.uni_id = pfun.uni_adm
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$this->gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$this->mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$this->gestion.' and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.') and pfun.pfun_tp=\'2\' and uad.uni_adm=\'1\'
                order by uad.uni_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------------------------------- LISTA DE PROYECTOS PARA EL REPORTE FISICA EFICACIA ------------------------*/
    public function list_operaciones($p1,$p2,$p3,$p4)
    {
        $tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = 'select uad.uni_id as das_id,uad.uni_unidad as das,p.proy_id,p.proy_nombre,pf.*,tp.tp_tipo,u.uni_unidad as u_ejec
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On ap.proy_id = p.proy_id
                Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join unidadorganizacional as uad On uad.uni_id = pfun.uni_adm
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$this->gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and pf.pfec_estado=\'1\' and pfg.g_id='.$this->gestion.' and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.') and pfun.pfun_tp=\'1\' and uad.uni_adm=\'1\'
                order by uad.uni_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*------------------------------------------------------------------------------------------------------------------*/
    /*============================================ ESTADO DE PROYECTOS ========================================================*/
    public function estado_proyecto()
    {
        $sql = 'select *
                from _estadoproyecto
                where ep_estado!=\'0\' order by ep_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function proyectos_est_global($ep_id,$gestion,$p1,$p2,$p3,$p4)
    {
        $tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = 'select p.*,fe.*,e.*,tp.*,ue.*
                from _proyectos as p
                Inner Join _proyectofuncionario as pf On pf.proy_id=p.proy_id
                Inner Join unidadorganizacional as ue On ue.uni_id=pf.uni_ejec
                Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                Inner Join (select apy.*, apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id and apg.aper_gestion='.$gestion.') as tap On p.proy_id=tap.proy_id
                Inner Join _proyectofaseetapacomponente as fe On p.proy_id=fe.proy_id
                Inner Join (select pfec_id,estado,m_id from fase_ejecucion where m_id in (select MAx(m_id)as mes
                from fase_ejecucion
                where g_id='.$gestion.')) as e On e.pfec_id=fe.pfec_id
                where e.estado='.$ep_id.' and pf.pfun_tp=\'1\' and p.estado!=\'3\' and fe.pfec_estado=\'1\' and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.') and p.proy_estado=\'4\' 

                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*========================================= EJE PROGRAMATICA ================================================*/
    public function eje_programatica()
    {
        $sql = 'select *
                from ptdi
                where ptdi_jerarquia=\'1\' and ptdi_gestion='.$this->session->userdata('gestion').'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function proyectos_eje_global($ptdi_id,$gestion,$p1,$p2,$p3,$p4)
    {
        $tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = 'select p.*,fe.*,ptdi.*,tp.*,ue.*
                from _proyectos as p
                Inner Join _proyectofuncionario as pf On pf.proy_id=p.proy_id
                Inner Join unidadorganizacional as ue On ue.uni_id=pf.uni_ejec
                Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                Inner Join (select apy.*, apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id and apg.aper_gestion=2017) as tap On p.proy_id=tap.proy_id
                Inner Join _proyectofaseetapacomponente as fe On p.proy_id=fe.proy_id
                Inner Join(        
                    select p1.ptdi_id as ptdi_id1,p3.ptdi_id as ptdi_id3
                    from ptdi as p3
                    Inner Join ptdi as p2 On p3.ptdi_depende=p2.ptdi_codigo
                    Inner Join ptdi as p1 On p2.ptdi_depende=p1.ptdi_codigo
                    where p3.ptdi_gestion='.$gestion.' and p2.ptdi_gestion='.$gestion.' and p1.ptdi_gestion='.$gestion.') as ptdi On ptdi.ptdi_id3=p.ptdi_id
                where ptdi.ptdi_id1='.$ptdi_id.' and pf.pfun_tp=\'1\' and  p.estado!=\'3\' and fe.pfec_estado=\'1\' and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.') and p.ptdi_id!=\'0\'

                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*========================================= POR REGIONES ================================================*/
    public function regiones()
    {
        $sql = ' select *
                 from _regiones
                 where reg_estado!=\'0\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function proyectos_reg_global($reg_id,$gestion,$p1,$p2,$p3,$p4)
    {
        $tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = ' select p.*,fe.*,r.reg_id,tp.*,ue.*
                 from _proyectos as p
                 Inner Join _proyectofuncionario as pf On pf.proy_id=p.proy_id
                 Inner Join unidadorganizacional as ue On ue.uni_id=pf.uni_ejec
                 Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                 Inner Join (select apy.*, apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id and apg.aper_gestion='.$gestion.') as tap On p.proy_id=tap.proy_id
                 Inner Join _proyectofaseetapacomponente as fe On p.proy_id=fe.proy_id
                 Inner Join (select pm.proy_id,m.reg_id
                        from _proyectosmunicipios pm
                        Inner Join _municipios as m On pm.muni_id=m.muni_id
                        where m.muni_estado=\'1\' and m.reg_id!=\'0\'
                        group by m.reg_id,pm.proy_id
                        order by m.reg_id,pm.proy_id) as r On r.proy_id=p.proy_id
                 where r.reg_id='.$reg_id.' and pf.pfun_tp=\'1\' and p.estado!=\'3\' and fe.pfec_estado=\'1\' and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.')
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*========================================= POR MUNICIPIO ================================================*/
    public function municipios()
    {
        $sql = 'select mun.*
                from _provincias as prov
                Inner Join _municipios as mun On prov.prov_id=mun.prov_id
                where prov.dep_id=\'2\' and prov.prov_estado=\'1\' and mun.muni_estado=\'1\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function proyectos_muni_global($muni_id,$gestion,$p1,$p2,$p3,$p4)
    {
        $tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = ' select p.*,fe.*,muni.muni_id,tp.*,ue.*
                 from _proyectos as p
                 Inner Join _proyectofuncionario as pf On pf.proy_id=p.proy_id
                 Inner Join unidadorganizacional as ue On ue.uni_id=pf.uni_ejec
                 Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                 Inner Join (select apy.*, apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id and apg.aper_gestion='.$gestion.') as tap On p.proy_id=tap.proy_id
                 Inner Join _proyectofaseetapacomponente as fe On p.proy_id=fe.proy_id
                 Inner Join (select proy_id,muni_id
                        from _proyectosmunicipios
                        group by proy_id,muni_id) as muni On muni.proy_id=p.proy_id
                 where muni_id='.$muni_id.' and pf.pfun_tp=\'1\' and p.estado!=\'3\' and fe.pfec_estado=\'1\' and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.')
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*========================================= POR PROVINCIA ================================================*/
    public function provincias()
    {
        $sql = ' select *
                 from _provincias as prov
                 where prov.dep_id=\'2\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function proyectos_prov_global($prov_id,$gestion,$p1,$p2,$p3,$p4)
    {
        $tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = ' select p.*,fe.*,prov.prov_id,tp.*,ue.*
                 from _proyectos as p
                 Inner Join _proyectofuncionario as pf On pf.proy_id=p.proy_id
                 Inner Join unidadorganizacional as ue On ue.uni_id=pf.uni_ejec
                 Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                 Inner Join (select apy.*, apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id and apg.aper_gestion='.$gestion.') as tap On p.proy_id=tap.proy_id
                 Inner Join _proyectofaseetapacomponente as fe On p.proy_id=fe.proy_id
                 Inner Join (select proy_id,prov_id
                        from _proyectosprovincias
                        where estado=\'1\'
                        group by proy_id,prov_id) as prov On prov.proy_id=p.proy_id
                 where prov.prov_id='.$prov_id.' and pf.pfun_tp=\'1\' and p.estado!=\'3\' and fe.pfec_estado=\'1\' and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.')
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}