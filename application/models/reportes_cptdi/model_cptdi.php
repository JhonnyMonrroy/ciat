<?php
class Model_cptdi extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }

    /*-------------- Lista de Componentes ------------*/
    public function ptdi_componentes($gestion)
    {
        $sql = 'select *
                from ptdi 
                where ptdi_jerarquia=\'1\' and ptdi_estado!=\'3\' and ptdi_gestion='.$gestion.'
                order by ptdi_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*======================= EJECUCION PRESUPUESTARIA PTDI ID UNI EJEC =====================*/
    public function lista_programas_ptdi_detalle($gestion,$mes,$ptdi_id, $uni_ejec)
    {
        $sql = 'select a.*,pe.*,u.*,p.*,pfg.*,pfun.*,uad.*,ptdi.pt1
                from aperturaprogramatica a
                    Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                    Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                    Inner Join _proyectos as p On ap.proy_id = p.proy_id
                    Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                    Inner Join unidadorganizacional as uad On uad.uni_id = pfun.uni_adm
                    Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                    Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                    Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                    Inner Join (
                        SELECT ptdi.pt1, ptdi.pt2, ptdi.pt3              
                        FROM ptdi as p
                        Inner Join (    SELECT p1.pt1, p2.pt2,p3.ptdi_id as pt3
                        FROM ptdi as p3
                        Inner Join (select ptdi_id as pt2,ptdi_depende,ptdi_descripcion, ptdi_codigo from ptdi where ptdi_jerarquia=\'2\') as p2 On p2.ptdi_codigo=p3.ptdi_depende
                        Inner Join (select ptdi_id as pt1,ptdi_depende,ptdi_descripcion, ptdi_codigo from ptdi where ptdi_jerarquia=\'1\') as p1 On p1.ptdi_codigo=p2.ptdi_depende
                        ) as ptdi On p.ptdi_id=ptdi.pt3
                    ) as ptdi On p.ptdi_id=ptdi.pt3
                    where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and ptdi.pt1='.$ptdi_id.' and pfun.uni_ejec='.$uni_ejec.'
                    order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*======================= SUMA EJECUCION PRESUPUESTARIA POR PTDI =====================*/
    public function suma_lista_programas_ptdi($gestion,$mes,$ptdi_id)
    {
        $sql = 'select ptdi.pt1,SUM(pfecg_ppto_total) as costo,SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                    Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                    Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                    Inner Join _proyectos as p On ap.proy_id = p.proy_id
                    Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                    Inner Join unidadorganizacional as uad On uad.uni_id = pfun.uni_adm
                    Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                    Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                    Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                    Inner Join (
                        SELECT ptdi.pt1, ptdi.pt2, ptdi.pt3              
                        FROM ptdi as p
                        Inner Join (SELECT p1.pt1, p2.pt2,p3.ptdi_id as pt3
                        FROM ptdi as p3
                        Inner Join (select ptdi_id as pt2,ptdi_depende,ptdi_descripcion, ptdi_codigo from ptdi where ptdi_jerarquia=\'2\') as p2 On p2.ptdi_codigo=p3.ptdi_depende
                        Inner Join (select ptdi_id as pt1,ptdi_depende,ptdi_descripcion, ptdi_codigo from ptdi where ptdi_jerarquia=\'1\') as p1 On p1.ptdi_codigo=p2.ptdi_depende
                        ) as ptdi On p.ptdi_id=ptdi.pt3
                    ) as ptdi On p.ptdi_id=ptdi.pt3
                    where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and ptdi.pt1='.$ptdi_id.'
                    group by ptdi.pt1
                    order by ptdi.pt1 asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*======================= SUMA TOTAL EJECUCION PRESUPUESTARIA PTDI =====================*/
    public function suma_total_lista_programas_ptdi($gestion,$mes)
    {
        $sql = 'select SUM(pfecg_ppto_total) as costo,SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                    Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                    Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                    Inner Join _proyectos as p On ap.proy_id = p.proy_id
                    Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                    Inner Join unidadorganizacional as uad On uad.uni_id = pfun.uni_adm
                    Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                    Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                    Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                    Inner Join (
                        SELECT ptdi.pt1, ptdi.pt2, ptdi.pt3              
                        FROM ptdi as p
                        Inner Join (SELECT p1.pt1, p2.pt2,p3.ptdi_id as pt3
                        FROM ptdi as p3
                        Inner Join (select ptdi_id as pt2,ptdi_depende,ptdi_descripcion, ptdi_codigo from ptdi where ptdi_jerarquia=\'2\') as p2 On p2.ptdi_codigo=p3.ptdi_depende
                        Inner Join (select ptdi_id as pt1,ptdi_depende,ptdi_descripcion, ptdi_codigo from ptdi where ptdi_jerarquia=\'1\') as p1 On p1.ptdi_codigo=p2.ptdi_depende
                        ) as ptdi On p.ptdi_id=ptdi.pt3
                    ) as ptdi On p.ptdi_id=ptdi.pt3
                    where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

}