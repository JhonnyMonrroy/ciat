<?php
class Mindicador extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }
    //OBTENER LOS DATOS DE MI INDICADOR
    public function get_indicador()
    {
        $this->db->select('*');
        $this->db->from('indicador ');
        $this->db->where('(indi_estado = 1 OR indi_estado = 2)');
        $this->db->ORDER_BY('indi_id');
        $query = $this->db->get();
        return $query->result_array();
    }


 
}

