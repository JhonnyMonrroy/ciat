<?php
class model_organismo_fin extends CI_Model {
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }
    public function lista_orga_fin()
    {
        $sql = 'SELECT *from organismofinanciador where of_estado=1 order by of_id';
        $query = $this->db->query($sql);
        return $query->result_array();
        redirect('organismo_financiador');
    } 
     function verificar_ofcod($cod,$gestion){
        //empezamos una transacciÃ³n
        $this->db->trans_begin();
        $this->db->WHERE('of_codigo',$cod);
        $this->db->WHERE('of_estado',1);
        $this->db->WHERE('of_gestion',$gestion);
        $this->db->FROM('organismofinanciador');
        $query = $this->db->get();
        //comprobamos si se han llevado a cabo correctamente todas
        //las consultas
        if ($this->db->trans_status() === FALSE)
        {
            //si ha habido algÃºn error lo debemos mostrar aquÃ­
            $this->db->trans_rollback();
        }else{
            //correcto
            $this->db->trans_commit();
            return $query->result_array();
        }

    }
    function mod_of($ofid,$ofdescripcion,$ofsigla,$ofgestion,$ofcodigo){
        $ofdescripcion = strtoupper($ofdescripcion);
        $ofsigla = strtoupper($ofsigla);
       $sql = "UPDATE organismofinanciador SET of_descripcion='".$ofdescripcion."',of_sigla='".$ofsigla."',of_gestion=".$ofgestion.",of_codigo=".$ofcodigo." WHERE of_id=".$ofid;
        $this->db->query($sql);
    }
     function add_of($ofdescripcion,$ofsigla,$ofcodigo,$ofgestion)
    {
        $this->db->trans_begin();
        $id_antes = $this->generar_id('organismofinanciador','of_id');
        $nuevo_id = $id_antes[0]['id_antes'];
        $nuevo_id++;
        $data = array(
            'of_id' => $nuevo_id,
            'of_descripcion' => strtoupper( $ofdescripcion),
            'of_sigla' => strtoupper($ofsigla),
            'of_estado' => 1,
            'of_gestion' => $ofgestion,
            'of_codigo' => $ofcodigo,
        );
        $this->db->insert('organismofinanciador',$data);
        if ($this->db->trans_status() === FALSE)
        {
            return "false";
        }else{
            //correcto
            $this->db->trans_commit();
            return "true";
        }
    }
    function dato_of($id)
    {
        $this->db->WHERE('of_id',$id);
        $this->db->FROM('organismofinanciador');
        $query = $this->db->get();
        return $query->result_array();
    }
    function generar_id($tabla,$id){
        $query =$this->db->query('SELECT MAX('.$id.') AS id_antes FROM '.$tabla);
        return $query->result_array();
       // return $query->row_array();
    }

    
}
?>  
