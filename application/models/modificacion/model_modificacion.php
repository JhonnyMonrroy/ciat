<?php
class Model_modificacion extends CI_Model
{
    var $gestion;
    public function __construct()
    {
        $this->load->database();
        $this->gestion = $this->session->userData('gestion');
    }
    //lista de organismo financiador
    /*================================= LISTA DE POAS ======================================*/
    public function lista_poa()
    {
        //FILTRADO POR GESTION DESDE POSTGRES
        $this->db->SELECT('*');
        $this->db->FROM('vista_poa');
        $this->db->WHERE('poa_gestion', $this->gestion);
        $this->db->ORDER_BY("aper_programa,aper_proyecto,aper_actividad", "ASC");
        $query = $this->db->get();
        return $query->result_array();
    }
    public function lista_poa_para_modificar()
    {
        $sql = 'select v.*, o.o_objetivo, o.o_id 
        from vista_poa v
        LEFT JOIN objetivosgestion o ON v.aper_id = o.aper_id 
        WHERE v.poa_gestion = ' . $this->gestion . '
        ORDER BY v.aper_programa, v.aper_proyecto, v.aper_actividad ASC';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*============================ POA ID ===================================*/
    public function poa_id($id_poa)
    {
        $this->db->from('vista_poa');
        $this->db->where('poa_id', $id_poa);
        $this->db->where('poa_gestion', $this->gestion);
        $query = $this->db->get();
        return $query->result_array();
    }

    //GUARDANDO LOS DATOS ANTES DE MODIFICAR
    function modifica_ogestion($o_id, $obje_id, $sw)
    {
        
        $sql = 'SELECT * FROM modifica_ogestion(' . $o_id . ','.$obje_id.','.$sw.')';
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_copy_table($o_id)
    {

        $query = "INSERT INTO objetivosgestion_mod(o_id, obje_id, o_codigo, o_objetivo, indi_id, o_indicador, o_formula, o_linea_base, o_meta, o_fuente_verificacion, o_supuestos, o_ponderacion, aper_id, 
            o_cierre, o_gestion, fun_id, o_archivo_adjunto, o_eficacia, o_efinanciera, o_epejecucion, o_efisica, fecha_creacion, o_estado, o_total_casos, o_casos_favorables, 
            o_casos_desfavorables, o_denominador, fecha_mod)
    SELECT o_id, obje_id, o_codigo, o_objetivo, indi_id, o_indicador, o_formula,  o_linea_base, o_meta, o_fuente_verificacion, o_supuestos, o_ponderacion, 
       aper_id, o_cierre, o_gestion, fun_id, o_archivo_adjunto, o_eficacia,    o_efinanciera, o_epejecucion, o_efisica, fecha_creacion, o_estado, 
       o_total_casos, o_casos_favorables, o_casos_desfavorables, o_denominador, fecha_mod FROM objetivosgestion WHERE o_id = ".$o_id;

        $query = $this->db->query($query);
        if($query){
            $quer = "Select objetivosgestion_mod_id from objetivosgestion_mod order by objetivosgestion_mod_id desc limit 1";
            $query = $this->db->query($quer);
            $valor = $query->result_array();

            $query = "INSERT INTO ogestion_prog_mes_mod(objetivosgestion_mod_id, opm_id, o_id, mes_id, opm_fis)
            select ".$valor[0]['objetivosgestion_mod_id']."::integer, opm_id, o_id, mes_id, opm_fis from ogestion_prog_mes where o_id = ".$o_id;
            $query = $this->db->query($query);
        }

        return $query;
    }

    //LISTAR MODIFICADOS
    function get_modificacion_xobjetivo($o_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('objetivosgestion_mod');
        $this->db->WHERE('o_id', $o_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    //LISTAR MODIFICADOS
    function get_modificado($o_dat)
    {
        $this->db->SELECT('*');
        $this->db->FROM('objetivosgestion_mod');
        $this->db->WHERE('objetivosgestion_mod_id', $o_dat);
        $query = $this->db->get();
        return $query->result_array();
    }

        //FUNCION QUE RETORNA DATOS DEL POA FILTRADO POR ID
    function dato_poa_id($poa_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vista_poa');
        $this->db->WHERE('poa_id', $poa_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*========================= LISTA DE OBJETIVOS DE GESTION =============================*/
    public function lista_ogestion($obje_id, $aper_id)
    {
        $this->db->SELECT('o.*,f.fun_nombre,f.fun_paterno,f.fun_materno,(SELECT get_unidad(f.uni_id)) AS unidad,i.indi_abreviacion');
        $this->db->FROM('objetivosgestion o');
        $this->db->JOIN('funcionario f', 'o.fun_id = f.fun_id');
        $this->db->JOIN('indicador i', 'i.indi_id = o.indi_id');
        //$this->db->WHERE('f.fun_id',$this->session->userdata('fun_id'));
        $this->db->WHERE('(o.o_estado = 1 OR o.o_estado = 2)');
        $this->db->WHERE('obje_id', $obje_id);
        $this->db->WHERE('aper_id', $aper_id);
        $this->db->ORDER_BY('o.o_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    /*========================= LISTA DE OBJETIVOS DE GESTION POR OBJ ESTRATEGICO=============================*/
    public function lista_ogestion_estarategico($obje_id)
    {
        $this->db->SELECT('o.*,f.fun_nombre,f.fun_paterno,f.fun_materno,(SELECT get_unidad(f.uni_id)) AS unidad,i.indi_abreviacion,a.aper_entidad || a.aper_programa || a.aper_proyecto || a.aper_actividad || \' - \'|| a.aper_descripcion as aper');
        $this->db->FROM('objetivosgestion o');
        $this->db->JOIN('funcionario f', 'o.fun_id = f.fun_id');
        $this->db->JOIN('indicador i', 'i.indi_id = o.indi_id');
        $this->db->JOIN('aperturaprogramatica a', 'a.aper_id = o.aper_id');
        //$this->db->WHERE('f.fun_id',$this->session->userdata('fun_id'));
        $this->db->WHERE('(o.o_estado = 1 OR o.o_estado = 2)');
        $this->db->WHERE('obje_id', $obje_id);
        $this->db->ORDER_BY('o.o_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    //OBETENER DATO DE OBJETIVO DE GESTION
    function get_ogestion($o_id)
    {
        $this->db->SELECT('o.*,i.*,f.fun_nombre,f.fun_paterno,f.fun_materno,(SELECT get_unidad(f.uni_id)) AS unidad,i.indi_abreviacion');
        $this->db->FROM('objetivosgestion o');
        $this->db->JOIN('funcionario f', 'o.fun_id = f.fun_id','LEFT');
        $this->db->JOIN('indicador i', 'i.indi_id = o.indi_id','LET');
        $this->db->WHERE('o.o_id', $o_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*=================================== LISTA DE PROGRAMADOS OBJETIVOS DE GESTION  ====================================*/
    public function obj_prog_mensual($o_id)
    {
        $this->db->from('ogestion_prog_mes');
        $this->db->where('o_id', $o_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*==============================================================================================================*/
    /*============================ BORRA DATOS DEL OBJETIVO PROGRAMADO GESTION =================================*/
    public function delete_og_prog($o_id){ 
        $this->db->where('o_id', $o_id);
        $this->db->delete('ogestion_prog_mes'); 
    }
    /*=================================================================================================*/
    /*=================================== AGREGAR OBJETIVO GESTION  PROGRAMADO ====================================*/
    public function add_og_prog($o_id,$m_id,$opm_fis)
    {
        $data = array(
            'o_id' => $o_id,
            'mes_id' => $m_id,
            'opm_fis' => $opm_fis,
        );
        $this->db->insert('ogestion_prog_mes',$data);
    }
    /*==============================================================================================================*/
    /*===================================== GET PRODUCTO TERMINAL ==================================================*/
    public function get_pterminal($pt_id)
    {
        $this->db->SELECT('p.*,i.*,f.fun_nombre,f.fun_paterno,f.fun_materno,(SELECT get_unidad(f.uni_id)) AS unidad,i.indi_abreviacion');
        $this->db->FROM('_productoterminal p');
        $this->db->JOIN('funcionario f', 'p.fun_id = f.fun_id','LEFT');
        $this->db->JOIN('indicador i', 'i.indi_id = p.indi_id','LEFT');
        $this->db->WHERE('p.pt_id', $pt_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*=================================== LISTA DE PROGRAMADOS OBJETIVOS DE GESTION  ====================================*/
    public function pt_prog_mensual($pt_id)
    {
        $this->db->from('pt_prog_mes');
        $this->db->where('pt_id', $pt_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*==============================================================================================================*/
    /*============================ BORRA DATOS DEL PRODUCTO TERMINAL =================================*/
    public function delete_pt_prog($pt_id){ 
        $this->db->where('pt_id', $pt_id);
        $this->db->delete('pt_prog_mes'); 
    }
    /*=================================================================================================*/
    /*=================================== AGREGAR OBJETIVO GESTION  PROGRAMADO ====================================*/
    public function add_pt_prog($pt_id,$m_id,$ppm_fis)
    {
        $data = array(
            'pt_id' => $pt_id,
            'mes_id' => $m_id,
            'ppm_fis' => $ppm_fis,
        );
        $this->db->insert('pt_prog_mes',$data);
    }
    /*==============================================================================================================*/
}
