<?php

class Mreporte_global extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    function reporte_institucional($mes, $gestion)
    {
        $sql = "
                SELECT y.tp_id, y.tp_tipo, SUM(y.preinversion) AS preinversion, SUM(y.inversion) AS inversion, SUM(y.operacion) AS operacion,
                (SUM(y.preinversion) + SUM(y.inversion) + SUM(y.operacion) ) AS total, SUM(y.ppto_inicial) AS ppto_inicial,
                SUM(y.mod_aprobadas) AS mod_aprobadas, SUM(y.ppto_vigente) AS ppto_vigente, SUM(y.devengado) AS devengado,
                (SUM(y.ppto_vigente) - SUM(y.devengado)) AS saldo
                FROM (
                SELECT x.tp_id, x.tp_tipo,
                to_number(COALESCE(string_agg(case when fas_id = 1 then to_char(cantidad, '9999999999.99')end,''),'0'), '9999999999.99') as preinversion,
                to_number(COALESCE(string_agg(case when fas_id = 2 then to_char(cantidad, '9999999999.99')end,''),'0'), '9999999999.99') as inversion,
                to_number(COALESCE(string_agg(case when fas_id = 3 then to_char(cantidad, '9999999999.99')end,''),'0'), '9999999999.99') as operacion,
                SUM(x.ppto_inicial) AS ppto_inicial, SUM(x.mod_aprobadas) AS mod_aprobadas, SUM(x.ppto_vigente) AS ppto_vigente,
                SUM(x.devengado) AS devengado
                FROM (
                            SELECT tp.tp_id, tp.tp_tipo,a.fas_id,count(p.proy_id) AS cantidad, SUM(e.pem_ppto_inicial) AS ppto_inicial,
                            SUM(e.pem_modif_aprobadas) AS mod_aprobadas, SUM(e.pem_ppto_vigente) AS ppto_vigente, SUM(e.pem_devengado) AS devengado
                            FROM _tipoproyecto tp
                            INNER JOIN _proyectos p ON p.tp_id = tp.tp_id
                            INNER JOIN _proyectofaseetapacomponente f ON p.proy_id = f.proy_id
                            INNER JOIN _fases a ON f.fas_id = a.fas_id
                            INNER JOIN proy_ejec_mes e ON f.proy_id = e.proy_id
                            WHERE f.pfec_estado = 1 AND f.estado = 1 AND p.tp_id <> 0
                            --AND f.fas_id <> 0 AND f.fas_id <> 4
                            AND e.mes_id = " . $mes . " AND e.gestion = " . $gestion . "
                            GROUP BY tp.tp_id, tp.tp_tipo, a.fas_id
                )x
                 GROUP BY x.tp_id, x.tp_tipo, x.fas_id
                )y
                GROUP BY y.tp_id, y.tp_tipo";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function reporte_institucional_tipo_proy($mes, $gestion, $vec_tipo_proy)
    {

        $sql = "
                SELECT y.tp_id, y.tp_tipo, SUM(y.preinversion) AS preinversion, SUM(y.inversion) AS inversion, SUM(y.operacion) AS operacion,
                (SUM(y.preinversion) + SUM(y.inversion) + SUM(y.operacion) ) AS total, SUM(y.ppto_inicial) AS ppto_inicial,
                SUM(y.mod_aprobadas) AS mod_aprobadas, SUM(y.ppto_vigente) AS ppto_vigente, SUM(y.devengado) AS devengado,
                (SUM(y.ppto_vigente) - SUM(y.devengado)) AS saldo
                FROM (
                SELECT x.tp_id, x.tp_tipo,
                to_number(COALESCE(string_agg(case when fas_id = 1 then to_char(cantidad, '9999999999.99')end,''),'0'), '9999999999.99') as preinversion,
                to_number(COALESCE(string_agg(case when fas_id = 2 then to_char(cantidad, '9999999999.99')end,''),'0'), '9999999999.99') as inversion,
                to_number(COALESCE(string_agg(case when fas_id = 3 then to_char(cantidad, '9999999999.99')end,''),'0'), '9999999999.99') as operacion,
                SUM(x.ppto_inicial) AS ppto_inicial, SUM(x.mod_aprobadas) AS mod_aprobadas, SUM(x.ppto_vigente) AS ppto_vigente,
                SUM(x.devengado) AS devengado
                FROM (
                            SELECT tp.tp_id, tp.tp_tipo,a.fas_id,count(p.proy_id) AS cantidad, SUM(e.pem_ppto_inicial) AS ppto_inicial,
                            SUM(e.pem_modif_aprobadas) AS mod_aprobadas, SUM(e.pem_ppto_vigente) AS ppto_vigente, SUM(e.pem_devengado) AS devengado
                            FROM _tipoproyecto tp
                            INNER JOIN _proyectos p ON p.tp_id = tp.tp_id
                            INNER JOIN _proyectofaseetapacomponente f ON p.proy_id = f.proy_id
                            INNER JOIN _fases a ON f.fas_id = a.fas_id
                            INNER JOIN proy_ejec_mes e ON f.proy_id = e.proy_id
                            WHERE f.pfec_estado = 1 AND f.estado = 1 AND p.tp_id <> 0
                            --AND f.fas_id <> 0 AND f.fas_id <> 4
                            AND e.mes_id = " . $mes . " AND e.gestion = " . $gestion . "
                            GROUP BY tp.tp_id, tp.tp_tipo, a.fas_id
                )x
                 GROUP BY x.tp_id, x.tp_tipo, x.fas_id
                )y
                WHERE y.tp_id IN (" . $vec_tipo_proy . ")
                GROUP BY y.tp_id, y.tp_tipo";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function reporte_unidad($mes, $gestion)
    {
        $sql = "
        SELECT x.tp_id, CASE WHEN x.tp_id = 1 THEN 'PROYECTO DE INVERSIÓN'  WHEN x.tp_id = 3 THEN 'PROGRAMA NO RECURRENTE' ELSE 'OTRO' END AS descripcion,
        to_number(COALESCE(string_agg(case when x.fas_id = 2 then to_char(cantidad, '99999999')end,''),'0'), '99999999') as cant_inv,
        to_number(COALESCE(string_agg(case when x.fas_id = 1 then to_char(cantidad, '99999999')end,''),'0'), '99999999') as cant_pinv,
        SUM(x.p_ini) AS ppto_inicial, SUM(x.p_vig) AS ppto_vigente, SUM(x.modi) AS modif_aprobadas, SUM(x.dev) AS devengado
        FROM (
        SELECT t.tp_id, t.fas_id, t.fas_fase, COUNT(t.tp_id)as cantidad, SUM(t.p_ini) AS p_ini, SUM(t.p_vig) AS p_vig,SUM(t.modi) AS modi, SUM(t.dev) AS dev
        FROM (
        SELECT p.tp_id, p.proy_id, p.proy_codigo, p.proy_nombre,q.fas_id, f.fas_fase,SUM(e.pem_ppto_inicial) AS p_ini,
        SUM(e.pem_ppto_vigente) AS p_vig, SUM(e.pem_modif_aprobadas) AS modi, SUM(e.pem_devengado) AS dev
        FROM vrel_proyecto_uni_ejec p
        INNER JOIN _proyectofaseetapacomponente q ON  p.proy_id = q.proy_id
        INNER JOIN _fases f ON  q.fas_id = f.fas_id
        INNER JOIN proy_ejec_mes e ON  p.proy_id = e.proy_id
        WHERE p.tp_id IN(1,3) AND (" . $gestion . " BETWEEN p.proy_gestion_inicio AND p.proy_gestion_fin)
        AND q.pfec_estado = 1 AND q.estado IN(1,2)
        AND f.fas_estado IN (0,1,2) AND  f.fas_id IN (1,2)
        AND e.mes_id = " . $mes . " AND e.gestion = " . $gestion . "
        GROUP BY p.proy_id, p.tp_id , p.proy_codigo, p.proy_nombre,q.fas_id, f.fas_fase
        )t
        GROUP BY t.tp_id, t.fas_id, t.fas_fase
        )x
        GROUP BY x.tp_id
        ORDER BY x.tp_id;
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //Proyectos de inversion publica
    function proyecto_inversion($mes, $gestion)
    {
        $sql = "
            SELECT x.uni_unidad, to_number(COALESCE(string_agg(case when x.fas_id = 2 then to_char(cantidad, '99999999')end,''),'0'), '99999999') as cant_inv,
            to_number(COALESCE(string_agg(case when x.fas_id = 1 then to_char(cantidad, '99999999')end,''),'0'), '99999999') as cant_pinv,
            SUM(x.p_ini) AS ppto_inicial, SUM(x.p_vig) AS ppto_vigente, SUM(x.modi) AS modif_aprobadas, SUM(x.dev) AS devengado
            FROM (
            SELECT t.uni_unidad, t.fas_id, t.fas_fase, COUNT(t.proy_id)as cantidad, SUM(t.p_ini) AS p_ini, SUM(t.p_vig) AS p_vig,SUM(t.modi) AS modi, SUM(t.dev) AS dev
            FROM (
            SELECT p.uni_unidad, q.fas_id, f.fas_fase, p.proy_id, SUM(e.pem_ppto_inicial) AS p_ini,
            SUM(e.pem_ppto_vigente) AS p_vig, SUM(e.pem_modif_aprobadas) AS modi, SUM(e.pem_devengado) AS dev
            FROM vrel_proyecto_uni_ejec p
            INNER JOIN _proyectofaseetapacomponente q ON  p.proy_id = q.proy_id
            INNER JOIN _fases f ON  q.fas_id = f.fas_id
            INNER JOIN proy_ejec_mes e ON  p.proy_id = e.proy_id
            WHERE p.tp_id = 1 AND (" . $gestion . " BETWEEN p.proy_gestion_inicio AND p.proy_gestion_fin)
            AND q.pfec_estado = 1 AND q.estado IN(1,2)
            AND f.fas_estado IN (1,2) AND  f.fas_id IN (1,2)
            AND e.mes_id = " . $mes . " AND e.gestion = " . $gestion . "
            GROUP BY p.uni_unidad,q.fas_id, f.fas_fase, p.proy_id
            )t
            GROUP BY t.uni_unidad, t.fas_id, t.fas_fase
            )x
            GROUP BY x.uni_unidad
            ORDER BY x.uni_unidad;
          ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function programa_no_recurrente($mes, $gestion)
    {
        $sql = "
        SELECT x.uni_unidad,
        to_number(COALESCE(string_agg(case when x.tp_id = 2 then to_char(cantidad, '99999999')end,''),'0'), '99999999') as prog_recurrente,
        to_number(COALESCE(string_agg(case when x.tp_id = 3 then to_char(cantidad, '99999999')end,''),'0'), '99999999') as prog_no_recurrente,
        SUM(x.p_ini) AS ppto_inicial, SUM(x.p_vig) AS ppto_vigente, SUM(x.modi) AS modif_aprobadas, SUM(x.dev) AS devengado
        FROM (
        SELECT t.uni_unidad, t.tp_id,COUNT(t.proy_id)as cantidad, SUM(t.p_ini) AS p_ini, SUM(t.p_vig) AS p_vig,SUM(t.modi) AS modi, SUM(t.dev) AS dev
        FROM (
        SELECT p.uni_unidad, p.proy_id, p.tp_id, SUM(e.pem_ppto_inicial) AS p_ini,
        SUM(e.pem_ppto_vigente) AS p_vig, SUM(e.pem_modif_aprobadas) AS modi, SUM(e.pem_devengado) AS dev
        FROM vrel_proyecto_uni_ejec p
        INNER JOIN proy_ejec_mes e ON  p.proy_id = e.proy_id
        WHERE p.tp_id IN (2,3) AND (" . $gestion . " BETWEEN p.proy_gestion_inicio AND p.proy_gestion_fin)
        AND e.mes_id = " . $mes . " AND e.gestion = " . $gestion . "
        GROUP BY p.uni_unidad, p.proy_id, p.tp_id
        )t
        GROUP BY t.uni_unidad, t.tp_id
        )x
        GROUP BY x.uni_unidad
        ORDER BY x.uni_unidad
        ";
        $query = $this->db->query($sql);
        return $query->result_array();

    }

    function proy_inv_prog_no_rec($mes, $gestion)
    {
        $sql = "
            SELECT x.uni_id, x.uni_unidad,
            to_number(COALESCE(string_agg(case when x.tp_id = 1 then to_char(cantidad, '99999999')end,''),'0'), '99999999') as inversion,
            to_number(COALESCE(string_agg(case when x.tp_id = 3 then to_char(cantidad, '99999999')end,''),'0'), '99999999') as prog_no_rec,
            SUM(x.p_ini) AS ppto_inicial, SUM(x.p_vig) AS ppto_vigente, SUM(x.modi) AS modif_aprobadas, SUM(x.dev) AS devengado
            FROM (
            SELECT t.tp_id,t.uni_id, t.uni_unidad, COUNT(t.proy_id)as cantidad, SUM(t.p_ini) AS p_ini, SUM(t.p_vig) AS p_vig,SUM(t.modi) AS modi, SUM(t.dev) AS dev
            FROM (
            SELECT p.uni_id, p.uni_unidad, p.tp_id, p.proy_id, SUM(e.pem_ppto_inicial) AS p_ini,
            SUM(e.pem_ppto_vigente) AS p_vig, SUM(e.pem_modif_aprobadas) AS modi, SUM(e.pem_devengado) AS dev
            FROM vrel_proyecto_uni_ejec p
            INNER JOIN proy_ejec_mes e ON  p.proy_id = e.proy_id
            WHERE p.tp_id IN(1,3) AND (" . $gestion . " BETWEEN p.proy_gestion_inicio AND p.proy_gestion_fin)
            AND e.mes_id = " . $mes . " AND e.gestion = " . $gestion . "
            GROUP BY p.uni_id, p.uni_unidad, p.tp_id, p.proy_id
            )t
            GROUP BY t.tp_id,t.uni_id, t.uni_unidad
            )x
            GROUP BY x.uni_id, x.uni_unidad
            ORDER BY x.uni_unidad
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}