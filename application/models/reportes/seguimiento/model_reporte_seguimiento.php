<?php

class Model_reporte_seguimiento extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    /*===============================================  OBJETIVOS DE GESTION ========================================================*/
    public function list_programas()
    {
         $sql = 'select v.*,COALESCE(p.reg_id,0) as ejecutado
                from vista_poa v
                LEFT JOIN programa_ejecucion p
                ON v.aper_id = p.aper_id AND p.reg_gestion = '.$this->session->userdata("gestion").' AND p.reg_estado IN (1,2)
                WHERE v.poa_gestion = '.$this->session->userdata("gestion").'
                ORDER BY v.aper_programa, v.aper_proyecto, v.aper_actividad ASC';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function get_programa($poa_id)
    {
         $sql = 'select v.*,COALESCE(p.reg_id,0) as ejecutado
                from vista_poa v
                LEFT JOIN programa_ejecucion p
                ON v.aper_id = p.aper_id AND p.reg_gestion = '.$this->session->userdata("gestion").' AND p.reg_estado IN (1,2)
                WHERE v.poa_id='.$poa_id.' and v.poa_gestion = '.$this->session->userdata("gestion").'
                ORDER BY v.aper_programa, v.aper_proyecto, v.aper_actividad ASC';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    public function list_ogestion($poa_id)
    {
         $sql = 'SELECT o.*, f.*,i.*
                FROM vista_poa v
                INNER JOIN poaobjetivosestrategicos po ON v.poa_id = po.poa_id
                INNER JOIN objetivosgestion o ON o.obje_id  = po.obje_id
                INNER JOIN indicador i ON o.indi_id  = i.indi_id
                INNER JOIN funcionario f  ON f.fun_id  = o.fun_id
                WHERE v.poa_id = '.$poa_id.' AND o.o_gestion = '.$this->session->userdata("gestion").' AND o.aper_id = v.aper_id
                ORDER BY o_id ASC';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    /*--------------------- Objetivo x ---------------------------*/
    public function get_objetivo($o_id)
    {
         $sql = 'select *
                 from objetivosgestion o
                 inner join indicador i on o.indi_id  = i.indi_id
                 where o_id='.$o_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*---------------------Og programado ---------------------------*/
    public function ogestion_programado($o_id)
    {
         $sql = 'select *
                 from ogestion_prog_mes
                 where o_id='.$o_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*---------------------Og ejecutado relativo ---------------------------*/
    public function ogestion_ejecutado_relativo($o_id)
    {
         $sql = 'select *
                 from ogestion_ejec_relativo
                 where o_id='.$o_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*---------------------Og ejecutado absoluto ---------------------------*/
    public function ogestion_ejecutado_absoluto($o_id)
    {
         $sql = 'select *
                 from ogestion_ejec_mes
                 where o_id='.$o_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*==========================================================================================================================*/
    /*==================================================== PRODUCTO TERMINAL =============================================*/
    /*--------------------- Lista de Productos Terminales ---------------------------*/
    public function list_pterminal($o_id)
    {
         $sql = 'select *
                 from _productoterminal pt
                 inner join indicador i on pt.indi_id  = i.indi_id
                 where pt.o_id='.$o_id.' order by pt_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*--------------------- Producto Terminal x ---------------------------*/
    public function get_pterminal($pt_id)
    {
         $sql = 'select *
                 from _productoterminal pt
                 inner join indicador i on pt.indi_id  = i.indi_id
                 where pt.pt_id='.$pt_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*---------------------Pt programado ---------------------------*/
    public function pterminal_programado($pt_id)
    {
         $sql = 'select *
                 from pt_prog_mes
                 where pt_id='.$pt_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*---------------------Pt ejecutado relativo ---------------------------*/
    public function pterminal_ejecutado_relativo($pt_id)
    {
         $sql = 'select *
                 from pt_ejec_relativo
                 where pt_id='.$pt_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*---------------------Pt ejecutado absoluto ---------------------------*/
    public function pterminal_ejecutado_absoluto($pt_id)
    {
         $sql = 'select *
                 from pt_ejec_mes
                 where pt_id='.$pt_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*====================================================================================================================*/
}