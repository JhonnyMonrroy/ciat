<?php
class Model_objetivo extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }

    public function get_dato_configuracion($gestion)
    {
        $query = "Select *
        from configuracion
        where ide = $gestion";
        $query = $this->db->query($query);
        return $query;
    }

    public function get_dictamen($id)
    {
        $query = "Select *
from vista_proyecto_dictamen
where proy_id = $id";
        $query = $this->db->query($query);
        return $query;
    }
    public function get_dictamen2($id, $gestion)
    {
        $query = "Select *
from vista_proyecto_dictamen
where proy_id = $id and aper_gestion = $gestion";
        $query = $this->db->query($query);
        return $query;
    }
    public function get_indicadores_metas($id)
    {
        $query = "SELECT proy_id, meta_descripcion, meta_meta, meta_ejec, meta_efic, CASE WHEN meta_rp = 0 then 'Resultado' else 'Producto' END tipo
        FROM _metas
        WHERE (estado = 1 and proy_id = $id) or (estado = 2 and proy_id = $id)";
        $query = $this->db->query($query);
        return $query;
    }
    public function get_aper_programatica_dictamen($proy_id,$gestion)
    {
        $query = "Select *
from vista_apertura_programatica_dictamen
where proy_id = $proy_id and aper_gestion = $gestion";
        $default = "Select $proy_id as proy_id, -1 as aper_id, $gestion as aper_gestion, 
        'Sin Apertura Programatica' as programatica, -1 as uni_id,
        'Sin Unidad Ejecutora' as uni_unidad";
        $default = $this->db->query($default);
        $query = $this->db->query($query);
        $resp = $query->num_rows()<1 ? $default : $query;
        return $resp;
    }
    public function get_presupuesto_dictamen($id,$gestion)
    {
        $query = "Select *
from vista_presupuesto_dictamen
where proy_id = $id and g_id = $gestion";
        $query = $this->db->query($query);
        return $query;
    }

    public function get_localizacion_dictamen($id)
    {
        $query = "Select *
from vista_localizacion_dictamen
where proy_id = $id
order by proy_id, dep_id, prov_id, muni_id;";
        $query = $this->db->query($query);
        return $query;
    }

    public function get_componentes_dictamen($proy_id)
    {
        $query = "SELECT *
        from vista_componentes_dictamen
        where proy_id = $proy_id";
        $query = $this->db->query($query);
        return $query;
    }

    public function get_productos_dictamen($com_id)
    {
        $query = "Select *
from vista_productos_dictamen
where com_id = $com_id";
        $query = $this->db->query($query);
        return $query;
    }

    public function get_temp_metas_productos($prod_id, $gestion)
    {
        $programado = "select *
from vista_productos_temporalizacion_programado_dictamen
where prod_id = $prod_id and g_id = $gestion";
        $programado = $this->db->query($programado);
        $ejecutado = "select *
from vista_productos_temporalizacion_ejecutado_dictamen
where prod_id = $prod_id and g_id = $gestion";
        $ejecutado = $this->db->query($ejecutado);
        $default = "Select -1 as cod_id, 0 as enero, 0 as febrero, 0 as marzo, 0 as abril, 0 as mayo,
0 as junio, 0 as julio, 0 as agosto, 0 as septiembre, 0 as octubre, 0 as noviembre, 0 as diciembre, 0 as n";
        $default = $this->db->query($default);
        $ejecutado = $ejecutado->num_rows()<1 ? $default : $ejecutado;
        $programado = $programado->num_rows()<1 ? $default : $programado;
        $data['programado'] = $programado;
        $data['ejecutado'] = $ejecutado; 
        return $data;
    }

    public function get_actividades_dictamen($prod_id)
    {
        $query = "Select * 
from vista_actividades_dictamen
where prod_id = $prod_id";
        $query = $this->db->query($query);
        return $query;
    }

    public function get_temp_metas_actividades($act_id, $gestion)
    {
        $programado = "select *
from vista_actividades_temporalizacion_programado_dictamen
where act_id = $act_id and g_id = $gestion";
        $programado = $this->db->query($programado);
        $ejecutado = "Select *
from vista_actividades_temporalizacion_ejecutado_dictamen
where act_id = $act_id and g_id = $gestion";
        $ejecutado = $this->db->query($ejecutado);
        $default = "Select -1 as cod_id, 0 as enero, 0 as febrero, 0 as marzo, 0 as abril, 0 as mayo,
0 as junio, 0 as julio, 0 as agosto, 0 as septiembre, 0 as octubre, 0 as noviembre, 0 as diciembre, 0 as n";
        $default = $this->db->query($default);
        $ejecutado = $ejecutado->num_rows()<1 ? $default : $ejecutado;
        $programado = $programado->num_rows()<1 ? $default : $programado;
        $data['programado'] = $programado;
        $data['ejecutado'] = $ejecutado; 
        return $data;
    }
    public function get_datos_apertura($aper_id,$gestion)
    {
        $query = "SELECT aper_id, aper_gestion, (aper_programa||aper_proyecto||aper_actividad) aper_programatica,
	aper_programa, aper_proyecto, aper_actividad, aper_descripcion, aper_sisin, uni_id
from aperturaprogramatica
where aper_id = $aper_id and aper_gestion = $gestion;";
        $query = $this->db->query($query);
        return $query;
    }
}