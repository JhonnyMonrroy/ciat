<!-- MAIN PANEL -->
<div id="main" role="main">
    <!-- RIBBON -->
    <div id="">
        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li><a href="<?php echo site_url("") ?>/mnt/red_o">Lista de Carpeta POA</a></li>
            <li> Acciones de Mediano Plazo Asignaci&oacute;n</li>
        </ol>
    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <section id="widget-grid" class="well">
                    <div class="" style="font-size: 15px">
                        <b> ACCI&Oacute;N DE MEDIANO PLAZO (ASIGNACI&Oacute;N)</b> <br>
                        <b>C&oacute;digo POA: </b><small class="txt-color-blueLight"><?php echo $dato_poa[0]['poa_codigo'] ?></small><br>
                        <b>Categoría Program&aacute;tica: </b><small class="txt-color-blueLight"><?php echo $dato_poa[0]['aper_programa'] . $dato_poa[0]['aper_proyecto'] . $dato_poa[0]['aper_actividad'] . " - " . $dato_poa[0]['aper_descripcion'] ?></small><br>
                        <b>Gesti&oacute;n: </b><small class="txt-color-blueLight"><?php echo $dato_poa[0]['poa_gestion'] ?></small><br>
                    </div>
                </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <section id="widget-grid" class="well">
                    <center>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                                OPCIONES
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url("") ?>/mnt/red_o">VOLVER ATRAS</a></li>
                            </ul>
                        </div>
                    </center>
                </section>
            </article>

        </div>

        <!--<div class="row">
            <article class="col-sm-8 col-md-8 col-lg-8">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <span class="glyphicon glyphicon-folder-close"></span> </span>

                        <h2>CARPETA POA</h2>
                    </header>
                    <div class="widget-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <LABEL><b>C&oacute;digo POA </b></label>
                                    <input class="form-control" type="text" disabled="disabled"
                                           value="<?php /*echo $dato_poa[0]['poa_codigo'] */?>">
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <LABEL><b>Apertura Program&aacute;tica </b></label>
                                    <input class="form-control" type="text" disabled="disabled"
                                           value="<?php /*echo $dato_poa[0]['aper_programa'] . $dato_poa[0]['aper_proyecto'] . $dato_poa[0]['aper_actividad'] . " - " . $dato_poa[0]['aper_descripcion'] */?>">
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-group">
                                    <LABEL><b>Gesti&oacute;n </b></label>
                                    <input class="form-control" type="text" disabled="disabled"
                                           value="<?php /*echo $dato_poa[0]['poa_gestion'] */?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>-->


        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <span class="fa fa-table"></span> </span>

                            <h2>LISTA DE ACCIONES DE MEDIANO PLAZO</h2>
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th> #</th>
                                            <th> A</th>
                                            <th>C&Oacute;DIGO</th>
                                            <th>PDES</th>
                                            <th>ACCI&Oacute;N DE MEDIANO PLAZO</th>
                                            <th>PBS.</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tabla_obj">
                                        <?php
                                        $cont = 1;
                                        foreach($lista_obje as $row) {
                                            echo '<tr> ';
                                            echo '<td>'.$cont.'</td>';
                                            echo '<td>';
                                                if(strlen($row['asignar'])==0){
                                                    echo'<center><button class="btn btn-labeled btn-success asignar" name="'.$row['obje_id'].'" id="'.$dato_poa[0]['poa_id'] .'"
                                                            title="ASIGNAR OBJETIVO ESTRATEGICO">Asignar</button><br></center>';
                                                }else{
                                                    echo'<center><button class="btn btn-labeled btn-danger quitar" name="'.$row['obje_id'].'" id="'.$dato_poa[0]['poa_id'].'"
                                                         title="QUITAR OBJETIVO ESTRATEGICO">Quitar</button></center>';
                                                }
                                            ?>
                                            </td>
                                            <?php
                                            echo '<td><font size="1">' . $row['obje_codigo'] . '</font></td>';
                                            echo '<td><font size="1"><u><b>PILAR</b></u> : ' . $row['pdes_pilar'] . '<br><u><b>META</b></u> : ' .
                                            $row['pdes_meta'] . '<br><u><b>RESULTADO</b></u> : ' . $row['pdes_resultado'] . '<br><u><b>ACCION</b></u> : '
                                            . $row['pdes_accion'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['obje_objetivo'] . '</font></td>';

                                            if(strlen($row['asignar'])==0){
                                                    echo '<td class="text-center"><b><i class="glyphicon glyphicon-remove" style="color:red"></i></b></td>';
                                                }else{
                                                    echo '<td class="text-center"><b><i class="glyphicon glyphicon-ok" style="color:green"></i></b></td>';
                                                }
                                            $cont++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>

                    <!-- end widget -->
                </article>
                <!-- WIDGET END -->
            </div>
        </section>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN PANEL -->
