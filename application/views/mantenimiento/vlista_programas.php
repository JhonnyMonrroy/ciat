<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li>Programas</li>
        </ol>
    </div>
    <div id="content">
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>

                            <h2 class="font-md"><strong> &nbsp;LISTA DE PROGRAMAS</strong></h2>
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th>
                                                <center>
                                                    <a data-toggle="modal" href="#modal_aper_nuevo" style="text-decoration: none" title="NUEVO PROGRAMA">
                                                        <img src="<?php echo base_url(); ?>assets/ifinal/2.png" width="30" height="30">
                                                    </a>
                                                    <br style="font-size: 20px">Nuevo Prog.
                                                </center>
                                            </th>
                                            <th>GESTIÓN</th>
                                            <th>PROGRAMA</th>
                                            <th>PROYECTO</th>
                                            <th>ACTIVIDAD</th>
                                            <th>DESCRIPCION</th>
                                            <th>UNIDAD ORGANIZACIONAL</th>
                                            <th title="MODIFICAR -ELIMINAR">M / E</th>
                                        </tr>
                                        </thead>
                                        <tbody id="bdi">
                                        <?php
                                        $cont = 1;
                                        $asignado[1] = 'Asignado';
                                        $asignado[0] = 'No Asignado';
                                        foreach ($lista_apertura as $row) {
                                            echo '<tr id="tr' . $row['aper_id'] . '">';
                                            echo '<td>' . $cont . '</td>';
                                            echo '<td><font size="1">' . $row['aper_gestion'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['aper_programa'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['aper_proyecto'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['aper_actividad'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['aper_descripcion'] . '</font></td>';

                                            echo '<td><font size="1">' . $row['uni_unidad'] . '</font></td>';
                                            ?>
                                            <td>
                                                <BUTTON data-toggle="modal" data-target="#modal_mod_aper" class="btn btn-xs botones dos mod_aper"
                                                        name="<?php echo $row['aper_id'] ?>" id="enviar_mod">
                                                    <img src="<?php echo base_url() ?>assets/ifinal/modificar.png" title="modificar">
                                                    <p style="font-size: 8px"><b>Modificar</b></p>
                                                </BUTTON>
                                                <BUTTON class="btn btn-xs botones uno del_aper" name="<?php echo $row['aper_id'] ?>">
                                                    <img src="<?php echo base_url() ?>assets/ifinal/eliminar.png" title="eliminar">
                                                    <p style="font-size: 8px"><b>Eliminar</b></p>
                                                </BUTTON>
                                            </td>
                                            <?php
                                            echo '</tr>';
                                            $cont++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <!-- ================== MODAL NUEVA APERTURA PROGRAMATICA    ========================== -->
    <div class="modal animated fadeInDown" id="modal_aper_nuevo" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> NUEVA PROGRAMAC&Oacute;N</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="form_add_programa" novalidate="novalidate" method="post">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>Gestión</b></label>
                                                <input class="form-control" type="text" name="aper_gestion" id="aper_gestion"
                                                       value="<?php echo $this->session->userData('gestion') ?>" data-mask="2099" class="invalid">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>Programa</b></label>
                                                <input class="form-control" type="text" data-mask="99"
                                                       data-mask-placeholder="X"
                                                       name="aper_programa" id="aper_programa">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <LABEL><b>Descripción</b></label> <!--onkeyup="javascript:this.value=this.value.toUpperCase();"-->
                                                <input class="form-control" type="text" name="aper_descripcion" id="aper_descripcion" placeholder="Ingrese la descripción"
                                                       onkeypress="if (this.value.length > 200){return false; }">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <LABEL><b>Unidad Organizacional</b></label>
                                                <select style="width:100%" name="unidad_o" id="unidad_o" tabindex="-1" class="select2 select2-offscreen">
                                                    <option value="">Seleccione</option>
                                                    <?php
                                                    foreach ($lista_unidad as $row) {
                                                        echo '<option value="' . $row['uni_id'] . '">' . $row['uni_unidad'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-md btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="enviar_aper" id="enviar_aper" class="btn  btn-md btn-primary"
                            ><i
                                    class="fa fa-save"></i>
                                GUARDAR
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- ================== Modal  MODIFICAR apertura========================== -->
    <div class="modal animated fadeInDown" id="modal_mod_aper" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR PROGRAMACI&Oacute;N</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="mod_formaper" name="mod_formaper" novalidate="novalidate" method="post">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>Gestión</b></label>
                                                <input disabled="disabled" class="form-control" type="text" name="modaper_gestion" id="modaper_gestion"
                                                       data-mask="2099" class="invalid">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>Programa</b></label>
                                                <input disabled="disabled" class="form-control" type="text" value="99" data-mask="99" data-mask-placeholder="X"
                                                       name="modaper_programa" id="modaper_programa" placeholder="Ingrese el Programa">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <LABEL><b>Descripción</b></label>
                                                <input class="form-control" type="text" name="modaper_descripcion" id="modaper_descripcion"
                                                       placeholder="Ingrese la descripción">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <LABEL><b>Unidad Organizacional</b></label>
                                                <select name="modunidad_o" id="modunidad_o" class="form-control">

                                                    <?php
                                                    foreach ($lista_unidad as $row) {
                                                        echo '<option value="' . $row['uni_id'] . '">' . $row['uni_unidad'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-md btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="mod_aperenviar" id="mod_aperenviar" class="btn  btn-md btn-primary"><i class="fa fa-save"></i>
                                ACEPTAR
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




