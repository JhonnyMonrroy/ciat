<!-- MAIN PANEL -->
<div id="main" role="main">
    <!-- RIBBON -->
    <div id="">


        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li>Partidas</li>
        </ol>
    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">
        <!--
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw "></i> LISTA DE PARTIDAS
                </h1>
            </div>
        </div>


        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12">
                <div class="form-group">
                    <a data-toggle="modal" href="#modal_nuevo_par" class="btn btn-labeled btn-success pull-left ">
                        <span class="btn-label"><i class="glyphicon glyphicon-file"></i></span><b>NUEVO</b></a><br><br>
                </div>
            </div>
        </div>-->
        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">

                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i></span>
                            <h2 class="font-md"><strong>PARTIDAS</strong></h2>
                        </header>

                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>
                                                <center>
                                                    <a data-toggle="modal" href="#modal_nuevo_par" style="text-decoration: none" title="NUEVA PARTIDA">
                                                        <img src="<?php echo base_url(); ?>assets/ifinal/2.png" width="30" height="30">
                                                    </a>
                                                    <br style="font-size: 20px">Nueva Partida
                                                </center>
                                            </th>
                                            <th> C&Oacute;DIGO</th>
                                            <th>NOMBRE DE LAS PARTIDAS</th>
                                            <th>DEPENDE</th>
                                            <th>GESTION</th>
                                            <th>MODIFICAR</th>
                                            <th>ELIMINAR</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tabla_par">
                                        <?php
                                        $cont = 1;
                                        $depende[0] = 'si';
                                        $depende[1] = 'no';
                                        foreach ($lista_p as $row) {
                                            echo '<tr id="tr' . $row['par_id'] . '">';
                                            echo '<td>' . $cont . '</td>';
                                            echo '<td>' . $row['par_codigo'] . '</td>';
                                            echo '<td><font size="1">' . $row['par_nombre'] . '</font></td>';
                                            if ($row['par_depende'] == 0) {
                                                echo '<td><font size="1" style="color:red;"> No depende</font></td>';
                                            } else {
                                                echo '<td>' . $row['par_depende'] . '</td>';
                                            }
                                            echo '<td><font size="1">' . $row['par_gestion'] . '</font></td>';

                                            ?>
                                            <input type="hidden" name="id_mod" id="id_mod"
                                                   value="<?php echo $row['par_id'] ?>">
                                            <td>
                                                <BUTTON data-toggle="modal" data-target="#modal_mod_par"
                                                        class="btn btn-xs botones dos mod_par"
                                                        name="<?php echo $row['par_id'] ?>" id="enviar_mod">
                                                    <div class="btn-hover-postion2">
                                                        <img src="<?php echo base_url() ?>assets/ifinal/modificar.png" title="modificar" id="modificar">
                                                        <P style="font-size: 8px;"><b>Modificar</b></P>
                                                    </div>
                                                </BUTTON>
                                            </td>
                                            <?php
                                            if ($row['par_depende'] == 0) {
                                                ?>
                                                <td>
                                                    <BUTTON disabled="disabled" class="btn btn-xs botones uno del_par"
                                                            name="<?php echo $row['par_id'] ?>" id="eliminar">
                                                        <div class="btn-hover-postion1">
                                                            <img src="<?php echo base_url() ?>assets/ifinal/eliminar.png" title="eliminar" id="eliminar">
                                                            <P style="font-size: 8px;"><b>Eliminar</b></P>
                                                        </div>
                                                    </BUTTON>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td>
                                                    <BUTTON class="btn btn-xs botones uno del_par"
                                                            name="<?php echo $row['par_id'] ?>" id="eliminar">
                                                        <div class="btn-hover-postion1">
                                                            <img src="<?php echo base_url() ?>assets/ifinal/eliminar.png" title="eliminar" id="eliminar">
                                                            <P style="font-size: 8px;"><b>Eliminar</b></P>
                                                        </div>
                                                    </BUTTON>
                                                </td>
                                                <?php
                                            }
                                            ?>

                                            <?php
                                            echo '</tr>';
                                            $cont++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                </article>
                <!-- WIDGET END -->
            </div>
        </section>
        <!-- end widget grid -->
    </div>
    <!-- END MAIN CONTENT -->
    <!-- ================== Modal NUEVA PARTIDA ========================== -->
    <div class="modal animated fadeInDown" id="modal_nuevo_par" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> NUEVO REGISTRO</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="form_par" novalidate="novalidate" method="post">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <LABEL><b>Ingrese el nombre de la Partida</b></label>
                                                <input class="form-control" type="text" name="par_nombre"
                                                       id="par_nombre" placeholder="Ingrese el nombre de la partida"
                                                       style="text-transform:uppercase;"
                                                       onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                       onkeypress="if (this.value.length < 100) { return soloLetras_carracter_especial(event);}else{return false; }"   >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <LABEL><b>Es dependiente? </b></LABEL><br>
                                                <input type="radio" id="depndiente" value="si" name="dependiente"
                                                       class="par_si"/> SI
                                                <input type="radio" id="dependiente" value="no" name="dependiente"
                                                       class="par_no"/> NO

                                            </div>
                                        </div>
                                    </div>
                                    <div id="content_parent" class="row" style="display: none;">
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <select name="padre" id="padre" class="form-control">
                                                    <option value="">Seleccione una Partida</option>
                                                    <?php
                                                    foreach ($list_par_padres as $row) {
                                                        echo '<option value="' . $row['par_id'] . '">' . $row['par_nombre'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>Código</b></label>
                                                <input class="form-control" type="text" name="par_codigo"
                                                       id="par_codigo" placeholder="Ingrese código"
                                                       onkeypress="if (this.value.length < 8) { return soloNumeros(event);}else{return false; }">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <LABEL><b>Gestión</b></label>
                                                <input class="form-control" type="text" name="par_gestion"
                                                       id="par_gestion" data-mask="9999" data-mask-placeholder="X"
                                                       value="<?php echo $this->session->userData('gestion') ?>"
                                                       placeholder="Ingrese gestión">
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-ms btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="enviar_par" id="enviar_par" class="btn  btn-ms btn-primary"><i
                                    class="fa fa-save"></i>
                                GUARDAR
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- ================== Modal  MODIFICAR   PARTIDAS ========================== -->
    <div class="modal animated fadeInDown" id="modal_mod_par" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR REGISTRO</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="mod_formpar" novalidate="novalidate" method="post">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-11">
                                            <div class="form-group">
                                                <LABEL><b>Ingrese el nombre de la Partida</b></label>
                                                <input class="form-control" type="text" name="modpar_nombre"
                                                       id="modpar_nombre" placeholder="Ingrese el nombre de la partida"
                                                       style="text-transform:uppercase;"
                                                       onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                       onkeypress="if (this.value.length < 100) { return soloLetras_carracter_especial(event);}else{return false; }"  >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <LABEL><b>Padre: </b></label>
                                                <input disabled="disabled" class="form-control" type="text"
                                                       name="modpar_padre" id="modpar_padre">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>Código</b></label>
                                                <input disabled="disabled" class="form-control" type="text"
                                                       name="modpar_codigo" id="modpar_codigo"
                                                       onkeypress="if (this.value.length < 8) { return soloNumeros(event);}else{return false; }">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <LABEL><b>Gestión</b></label>
                                                <input class="form-control" type="text" name="modpar_gestion" data-mask="9999" data-mask-placeholder="X"
                                                       id="modpar_gestion" placeholder="Ingrese gestión">
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-ms btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="mod_parenviar" id="mod_parenviar"
                                    class="btn  btn-ms btn-primary"><i class="fa fa-save"></i>
                                ACEPTAR
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

</div>
<!-- END MAIN PANEL -->