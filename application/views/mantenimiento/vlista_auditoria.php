<?php
    $gestion = $this->session->userdata('gestion');
?>
<div id="main" role="main">
    <div id="ribbon">
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li><?php echo $titulo;?></li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw "></i> <?php echo $titulo;?></h1>
            </div>
        </div>
        <section id="widget-grid">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
                        <header>
                            <center>
                                <h4>
                                    <b>
                                        <?php echo $titulo;?>
                                    </b>
                                </h4>
                            </center>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body no-padding">
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                <center>
                                                    <a data-toggle="modal" href="#modal_nuevo_sector" class="">
                                                        <img src="<?php echo base_url() ?>assets/ifinal/2.png" title="Agregar Sector">
                                                    </a>
                                                </center>
                                            </th>
                                            <th data-hide="phone">Nombre Tabla</th>
                                            <th data-hide="phone">Operación</th>
                                            <th data-hide="phone">Datos Antiguos</th>
                                            <th data-hide="phone">Datos Nuevos</th>
                                            <th data-hide="phone">Fecha Operación</th>
                                            <th data-hide="phone">FunID</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($auditoria as $fila) {
                                            ?>
                                                <tr>
                                                    <td>
                                                        <center>
                                                            <button data-toggle="modal" data-target="#modal_ver_auditoria" class="btn btn-xs botones dos ver_auditoria"  name="<?php echo $fila['nombretabla'];?>" id="<?php echo $fila['pk_audi_proy'];?>">
                                                                <img src="<?php echo base_url() ?>assets/ifinal/carp.png" id="ver_sector">
                                                            </button>
                                                        </center>
                                                    </td>
                                                    <td>
                                                        <?php echo $fila['nombretabla'];?>
                                                    </td>
                                                    <td>
                                                        <?php echo $fila['operacion'];?>
                                                    </td>
                                                    <td>
                                                        <?php echo $fila['old'];?>
                                                    </td>
                                                    <td>
                                                        <?php echo $fila['new'];?>
                                                    </td>
                                                    <td>
                                                        <?php echo $fila['fecha_creacion'];?>
                                                    </td>
                                                    <td>
                                                        <?php echo $fila['fun_id'];?>
                                                    </td>
                                                </tr>
                                            <?php }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <!-- ================== Modal Modificar Clasificación Sectorial ========================== -->
    <div class="modal fade bs-example-modal-lg animated fadeInDown" id="modal_ver_auditoria" tabindex="-1" role="dialog" style="width:auto;height:auto;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-qrcode"></i> VER AUDITORIA</b>
                    </h4>
                </div>
                    <div class="modal-body no-padding" id="audi_imprimir">
                        <div class="row">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label><b>NOMBRE TABLA: </b></label>
                                                <span id="nom_tabla" name="nom_tabla" style="font-size:18px;"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <style type="text/css">
                                        .tabla_audi{
                                            width: 80px;
                                            font-size:15px;
                                            /*border:solid 1px;*/
                                            border-collapse: collapse;
                                            width:100%
                                        }
                                        .tabla_audi, th, td {
                                            border: 1px solid black;
                                            border-collapse: collapse;
                                            margin:10px;
                                            padding:10px;
                                        }
                                    </style>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table id="tbl_audi" name="tbl_audi" class="tabla_audi">
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-3 pull-left">
                                <button class="btn btn-default btn-danger" data-dismiss="modal">CERRAR</button>
                            </div>
                            <div class="col-md-3 pull-right ">
                                <button onclick="printDiv('audi_imprimir')" name="print_audi" id="print_audi" class="btn btn-default btn-primary">
                                    <i class="fa fa-save"></i>
                                    IMPRIMIR
                                </button>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
