<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li> Creación de carpeta POA</li>
        </ol>
    </div>
    <div id="content">
        <!-- <div class="row">
             <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12">
                 <div class="form-group">
                     <a data-toggle="modal" href="#modal_poa_nuevo" class="btn btn-labeled btn-success pull-left nuevo_poa">
                         <span class="btn-label"><i class="glyphicon glyphicon-file"></i></span><b>NUEVA CARPETA POA</b></a><br><br>
                 </div>
                 <div id="respuesta">
                 </div>
             </div>
         </div>-->
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>

                            <h2 class="font-md"><strong> &nbsp;LISTA DEL POA</strong></h2>
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>
                                                <center>
                                                    <a data-toggle="modal" href="#modal_poa_nuevo" class="nuevo_poa" style="text-decoration: none" title="NUEVO POA">
                                                        <img src="<?php echo base_url(); ?>assets/ifinal/2.png" width="30" height="30">
                                                    </a>
                                                    <br style="font-size: 20px">Nuevo Poa
                                                </center>
                                            </th>
                                            <th>CÓDIGO</th>
                                            <th>APERTURA PROGRAMÁTICA</th>
                                            <th>UNIDAD ORGANIZACIONAL</th>
                                            <th>FECHA DE CREACIÓN</th>
                                            <th>E / M</th>
                                        </tr>
                                        </thead>
                                        <tbody id="bdi">
                                        <?php
                                        $cont = 1;
                                        foreach ($lista_poa as $row) {
                                            echo '<tr id="tr' . $row['poa_id'] . '">';
                                            echo '<td>' . $cont . '</td>';
                                            echo '<td><font size="1">' . $row['poa_codigo'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['aper_programa'] . $row['aper_proyecto'] . $row['aper_actividad'] . " - " . $row['aper_descripcion'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['uni_unidad'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['poa_fecha_creacion'] . '</font></td>';
                                            ?>
                                            <input type="hidden" name="id_mod" id="id_mod"
                                                   value="<?php echo $row['poa_id'] ?>">
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> ACCIONES <span class="caret"></span></button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_mod_poa" class="mod_poa" name="<?php echo $row['poa_id'] ?>" id="enviar_mod" title="MODIFICAR REGISTRO"><i class="glyphicon glyphicon-pencil"></i> Modificar</a></li>
                                                        <li><a href="#" class="eliminar_poa" name="<?php echo $row['poa_id'] ?>"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
                                                    </ul>
                                                </div>

                                            </td>

                                            <?php
                                            echo '</tr>';
                                            $cont++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <!-- END MAIN CONTENT -->
    <!-- ================== Modal NUEVO  POA========================== -->
    <div class="modal animated fadeInDown" id="modal_poa_nuevo" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> CARPETA POA (ADICIONAR)</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="form_poa" novalidate="novalidate" method="post">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <LABEL><b style="font-size:12px">C&Oacute;DIGO</b></label>
                                                <input disabled="disabled" class="form-control" type="text"
                                                       value="AUTOMÁTICO">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <LABEL><b style="font-size: 12px">APERTURA PROGRAM&Aacute;TICA</b></label>
                                                <select name="aper_programatica" id="aper_programatica"
                                                        class="form-control">
                                                    <option value=""> Seleccione</option>
                                                    <?php
                                                    foreach ($list_aper as $row) {
                                                        echo '<option value="' . $row['aper_id'] . '">' . $row['aper_programa'] . $row['aper_proyecto'] . $row['aper_actividad'] . " - " . $row['aper_descripcion'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <LABEL><b style="font-size: 12px">UNIDAD ORGANIZACIONAL</b></label>
                                                <textarea class="form-control" disabled="disabled" name="unidad"
                                                          id="unidad" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label><b style="font-size: 12px">FECHA CREACI&Oacute;N</b></label>
                                                <input type="text" value="<?php echo date('d/m/Y') ?>" name="poa_fecha"
                                                       id="poa_fecha" placeholder="Ingrese Fecha"
                                                       onKeyUp="this.value=formateafecha(this.value);"
                                                       class="form-control datepicker" data-dateformat="dd/mm/yy">
                                                <span class="input-group-btn"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="enviar_poa" id="enviar_poa" class="btn  btn-lg btn-primary"><i
                                    class="fa fa-save"></i>
                                GUARDAR
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- ================== Modal MODIFICAR  POA========================== -->
    <div class="modal animated fadeInDown" id="modal_mod_poa" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR CARPETA POA</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="mod_formpoa" novalidate="novalidate" method="post">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <LABEL><b>Código</b></label>
                                                <input disabled="disabled" class="form-control" type="text"
                                                       name="modpoa_codigo" id="modpoa_codigo"
                                                       placeholder="Ingrese el código">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <LABEL><b>Apertura Programática</b></label>
                                                <input class="form-control" disabled="disabled" type="text"
                                                       name="modpoa_aper" id="modpoa_aper">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label><b>Fecha Creación</b></label>
                                                <input type="text" name="modpoa_fecha" id="modpoa_fecha"
                                                       placeholder="Ingrese Fecha" class="form-control datepicker"
                                                       onKeyUp="this.value=formateafecha(this.value);"
                                                       data-dateformat="dd/mm/yy">
                                                <span class="input-group-btn"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="mod_poaenviar" id="mod_poaenviar"
                                    class="btn  btn-lg btn-primary"><i class="fa fa-save"></i>
                                GUARDAR
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>




