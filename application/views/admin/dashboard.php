<?php 
	if($this->session->userdata('usuario')){
?>
<!DOCTYPE html>
<html lang="en-us" xmlns="http://www.w3.org/1999/html">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--========================imagen de fondo==========================-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
		<script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        	<!--====================fin de imagen de fondo=======================-->
		<style type="text/css">
			.img-circle{ width: 61px;}
		</style>
		<style>

			*{
			margin: 0;
			padding: 0;
			outline: none;
			border: none;
				box-sizing: border-box;
			}
			*:before,
			*:after{
				box-sizing: border-box;
			}
			html,
			body{
				min-height: 100%;
				background-color:#333 !important;
			}
			.container{
				margin: 4% auto;
				width: 210px;
				height: 140px;
				position: relative;
				perspective: 1000px;
			}

			
			/* Estilos version 1.1. */
		.bg-gray3{
			background-color:#333;			
		}
		.hr_black{
			background-color:#666; border-color:#666;
			display:block;
			clear:both;
		}	
		.hr_white{
			background-color:#ccc; border-color:#ddd;
			display:block;
			clear:both;
		}	
		#shortcut ul{
			box-shadow:none;
		}
		
		.panel_siipp{
			background-color:#333;			
		}
		
		.right_panel{
            -moz-border-radius:15px 0 0 0 ; 
			-webkit-border-radius: 15px 0 0 0 ; 
			border-radius: 15px 0 0 0 ;
			background-color:#fff;
		}
		.smart-style-1 #header{
			background-color:#333;
		}
		h2{
			margin:0 0 10px 0;
		}

		.frame {
			height: 270px;
			overflow: hidden;
		}
		.frame-guias{
			height:400px;
		}
		.frame-videos{
			height:240px;
		}
		.frame ul {
			list-style: none;
			margin: 0;
			padding: 0;
			height: 100%;
		}
		.frame ul li {
			float: left;
			width: 400px;
			height: 100%;
			margin: 0 1px 0 0;
			padding: 5px;
			background: #eee;
			color: #333;
			text-align: center;
			cursor: pointer;
            -moz-border-radius:7px; 
			-webkit-border-radius:7px; 
			border-radius: 7px;
		}
		.frame ul li.active {
			color: #00721B;
			background: #ccc;
		}
		
		video { 
		   width:100%;
		   height:auto;
		}	
		.item_descrip{
			margin:2px auto 10px auto;
		}
	
		.copyleft{
		  position:relative;
		  top:-5px;
		  display:inline-block;
		  transform: rotate(180deg);
		}
		
		
		
		</style>
	</head>
	<body class="desktop-detected smart-style-1 mobile-view-activated">
		<header id="header" >
			<!-- pulled right: nav area -->
				<!-- PLACE YOUR LOGO HERE -->
                <img src="<?php echo base_url(); ?>assets/img_v1.1/SIIPP_logo_black.png" alt="<?php echo $this->session->userdata('name')?>" style="width:270px;height:50px">
				<!-- END LOGO PLACEHOLDER -->

			<div class="pull-right">
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
			</div>
		</header>
		<!-- RIBBON style="background-color:#2A2725;"-->
<!--		<div id="ribbon" style="background-color:#777;">
		</div>-->
		<!-- END RIBBON -->
		<style type="text/css">
			.error-text{ font-size: 50px; }
			.menuss{ background-image: url("<?php echo base_url(); ?>assets/img/img/textura.jpg");  opacity: 0.8; padding:5px;
				-webkit-box-shadow: -8px 14px 50px -1px rgba(0,0,0,0.63);
				-moz-box-shadow: -8px 14px 50px -1px rgba(0,0,0,0.63);
				box-shadow: -8px 14px 50px -1px rgba(0,0,0,0.63);}
			.asd{ height: 150px; }
		</style>
		<!-- container -->
		
		<div class="row panel_siipp">
			<div class="col-md-3 text-center">
			<br>
                <img src="<?php echo base_url(); ?>assets/img_v1.1/Escudo.min.png" alt="G.A.D.S.C." style="width:50%">
					<h5 style="font-size:1.15em; text-transform:uppercase;">
					Gobierno Aut&oacute;nomo Departamental de Santa Cruz
					<br>
					<br>
					<img src="<?php echo base_url(); ?>assets/img_v1.1/logo_sdpd.png" alt="<?php echo $this->session->userData('name') ?>" style="width:30%">
					<br>
					<span style="font-size:0.80em; color:#999;">Centro de Investigación Agrícola Tropical</span>
					</h5>
				<hr class="hr_black">
				<h5>
				Asistencia técnica
						<br>
						<span class="text-justify" style="font-size:0.8em; color:#999;">
						Cont&aacute;ctate con el personal a cargo de la implementación del <?php echo $this->session->userdata('name')?>
						a través del <strong>N-NNNNNN</strong>, o envía un e-mail a:
						<a href="#" class="btn btn-info btn-xs lead">
						ciatplan.soporte@santacruz.gob.bo
						</a>
						</span>
				</h5>				
			</div>
			<div class="col-md-9 right_panel text-center">
				<div class="text-center">
					<img src="<?php echo base_url(); ?>assets/img_v1.1/SIIPP_logo_white.png" alt="<?php echo $this->session->userdata('name')?>" style="width:70%;">
				</div>
				<div>
					<hr class="hr_white">
						<div class="" style="color:#333;">
							<h1 class="text-success text-center" style="font-size:2.5em;">
								Bienvenido: <?php echo $this->session->userdata("funcionario");?>
							</h1>
							<p>
								<h4>Esta es la página de inicio en el <?php echo $this->session->userdata('name')?>.</h4>
								<br>
								Ahora estas trabajando con datos al Mes de 
								<span style="font-size:1.2em !important;text-transform:uppercase; color: #00721B;">
								<?php echo $this->session->userdata('desc_mes')?>
								</span>
								en la Gestión 
								<span style="font-size:1.2em !important;text-transform:uppercase; color: #00721B;">
								<?php echo $this->session->userdata("gestion");?>
								</span>. 
								<br>
								Si necesitas cambiar el mes o la gestión de trabajo, puedes hacerlo mediante 
								<a href="<?php echo site_url('cambiar_gestion')?>">
								este enlace.
								</a>
							</p>
						</div>
				</div>
					<div class="row" style="color:#333;">
					<hr class="hr_white">
						<h2 class="text-success">
						Operaciones disponibles
						</h2>
						<div class="item_descrip">
							Este es el men&uacute; principal que te permite acceder a las operaciones disponibles del sistema.
						</div>
						<?php 
						$rol = $this->session->userdata('rol_id');
							if($rol==7){ ?>
								<div class="col-md-12">
									<div id="shortcut" style="display: block;">
										<center>
											<ul style="margin-left:20px;">
												<li>
													<a href="<?php echo base_url(); ?>index.php/admin/dm/7/" class="jarvismetro-tile big-cubes bg-color-pinkDark">
														<span class="iconbox">
															<img class="img-circle" src="<?php echo base_url(); ?>assets/img/impresora.png" />
															<span>
																Reportes
																<span class="label pull-right bg-color-darken"></span>
															</span>
														</span>
													</a>
												</li>
											</ul>
										</center>
									</div>
								</div>
							<?php }
							else{ ?>
								<div class="col-md-12">
									<div id="shortcut" style="display: block;">
										<center>
											<ul>
												<?php
												for ($i=0; $i < count($vector_menus); $i++) { 
													echo $vector_menus[$i];	
												}
												?>
											</ul>
										</center>
									</div>
								</div>
							<?php }
						?>
						<br>
						<hr class="hr_white">
						
						
<!-- Nav tabs -->
<ul class="nav nav-tabs nav-justified" role="tablist">
  <li class="active"><a href="#uno" role="tab" data-toggle="tab">¿CÓMO FUNCIONA EL <?php echo $this->session->userdata('name')?>?</a></li>
  <li><a href="#dos" role="tab" data-toggle="tab">CARGADO DE DATOS</a></li>
  <li><a href="#tres" role="tab" data-toggle="tab">GUIAS RAPIDAS</a></li>
  <li><a href="#cuatro" role="tab" data-toggle="tab">VIDEOTUTORIALES</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="uno">
			<hr class="hr_white">
			<div class="row box-white">
				<h1 class="text-success">
                    ¿Como funciona el <?php echo $this->session->userdata('name')?>?
				</h1>
				<hr class="hr_white">
				<div class="col-md-8">
					<div class="text-justify lead">
					El <?php echo $this->session->userdata('name')?> asocia los proyectos, programas, y operaciones de funcionamiento del
					CIAT con el correspondiente funcionario responsable (fiscal, supervisor, o
					coordinador). Básicamente el flujo es el siguiente:
					</div>
					<table class="table table-bordered table-condensed table-hover text-justify">
						<tr>
							<th class="text-center">Nº</th>
							<th class="text-center">Operación</th>
							<th class="text-center">Escenario</th>
						</tr>
						<tr>
							<td>1</td>
							<td>El funcionario responsable de la unidad solicita su cuenta de usuario (en
							caso de no tener uno).</td>
							<td class="text-center">CIAT</td>
						</tr>
						<tr>
							<td>2</td>
							<td>El funcionario responsable registra el proyecto hasta la programación física.</td>
							<td class="text-center">Unidad Ejecutora</td>
						</tr>
						<tr>
							<td>3</td>
							<td>El analista POA valida la programación física.</td>
							<td class="text-center">CIAT</td>
						</tr>
						<tr>
							<td>4</td>
							<td>El analista financiero asigna los recursos respectivos.</td>
							<td class="text-center">CIAT</td>
						</tr>
						<tr>
							<td>5</td>
							<td>El funcionario responsable registra la programación financiera (registro de insumos).</td>
							<td class="text-center">Unidad Ejecutora</td>
						</tr>
						<tr>
							<td>6</td>
							<td>El analista POA valida la programación fínanciera.</td>
							<td class="text-center">CIAT</td>
						</tr>
						<tr>
							<td>7</td>
							<td>El funcionario responsable registra la ejecución física - financiera.</td>
							<td class="text-center">Unidad Ejecutora</td>
						</tr>
					</table>
					
				</div>
				<div class="col-md-4">
                    <img src="<?php echo base_url(); ?>assets/img_v1.1/siipp-cogs.png" alt="Centro de Investigación Agrícola Tropical" style="width:100%; margin:0 auto;">
				</div>
			</div>
  </div>
  <div class="tab-pane" id="dos">
			<hr class="hr_white">
			<div class="row box-white">
				<h1 class="text-success">
				Etapas del cargado de datos en el <?php echo $this->session->userdata('name')?>
				</h1>
				<hr class="hr_white">
				<div class="col-md-12 text-left lead">
					El cargado de datos en el <?php echo $this->session->userdata('name')?> comprende 4 etapas:
					<br>
					i) Las 3 primeras corresponden al registro del proyecto, programa, u operación de
					funcionamiento,
					<br>
					ii) Y la última corresponde al seguimiento de la ejecución física -	financiera.
                    <img src="<?php echo base_url(); ?>assets/img_v1.1/proc-registro.png" alt="Centro de Investigación Agrícola Tropical" style="width:100%; margin:0 auto;">
				</div>
			</div>
  </div>
  <div class="tab-pane" id="tres">
						<hr class="hr_white" id="descripcion_ogob">
						<h2 class="text-success">
						Guias rápidas de capacitaci&oacute;n
						</h2>
						<hr class="hr_white">
						<div class="item_descrip lead">
						Consulta las guías rapidas para realizar tareas específicas en el <?php echo $this->session->userdata('name')?>.
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<table class="table">
									<tr>
										<td>
											<div class="well well-sm" style="text-transform:uppercase;">
												<a class="btn-guia" href="datos.generales">
												<img src="<?php echo base_url(); ?>assets/img_v1.1/guias/thumb-caratula-datos_generales.png" alt="Guia rapida Registro de Datos Generales" style="width:100%;">
												<p style="margin:0;margin-top:7px;">
												Datos generales
												<br>
												<small class="text-success">(4 páginas)</small>
												</p>
												</a>
											</div>
										</td>
										<td>
											<div class="well well-sm" style="text-transform:uppercase;">
												<a class="btn-guia" href="programacion.fisica">
												<img src="<?php echo base_url(); ?>assets/img_v1.1/guias/thumb-caratula-programacion_fisica.png" alt="Guia rapida Programación Física" style="width:100%;">
												<p style="margin:0;margin-top:7px;">
												Programaci&oacute;n f&iacute;sica
												<br>
												<small class="text-success">(8 páginas)</small>
												</p>
												</a>
											</div>
										</td>
										<td>
											<div class="well well-sm" style="text-transform:uppercase;">
												<a class="btn-guia" href="programacion.financiera">
												<img src="<?php echo base_url(); ?>assets/img_v1.1/guias/thumb-caratula-programacion_financiera.png" alt="Guia rapida Programación Financiera" style="width:100%;">
												<p style="margin:0;margin-top:7px;">
												Programaci&oacute;n financiera
												<br>
												<small class="text-success">(10 páginas)</small>
												</p>
												</a>
											</div>
										</td>
										<td>
											<div class="well well-sm" style="text-transform:uppercase;">
												<a class="btn-guia" href="ejecucion.fisica">
												<img src="<?php echo base_url(); ?>assets/img_v1.1/guias/thumb-caratula-ejecucion-fisica.png" alt="Guia rapida Ejecución Físico - Financiero" style="width:100%;">
												<p style="margin:0;margin-top:7px;">
												Ejecuci&oacute;n f&iacute;sica
												<br>
												<small class="text-success">(5 páginas)</small>
												</p>
												</a>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="well well-sm" style="text-transform:uppercase;">
												<a class="btn-guia" href="ejecucion.financiera">
												<img src="<?php echo base_url(); ?>assets/img_v1.1/guias/thumb-caratula-ejecucion-financiera.png" alt="Guia rapida Ejecución Físico - Financiero" style="width:100%;">
												<p style="margin:0;margin-top:7px;">
												Ejecuci&oacute;n financiera
												<br>
												<small class="text-success">(6 páginas)</small>
												</p>
												</a>
											</div>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
									</tr>
								</table>
							</div>
						</div>
  </div>
  <div class="tab-pane" id="cuatro">
						<hr class="hr_white">
						<h2 class="text-success">
						Videotutoriales de capacitaci&oacute;n
						</h2>
						<hr class="hr_white">
						<div class="item_descrip lead">
						Tienes a tu disposici&oacute;n videotutoriales para seguir aprendiendo las funcionalidades del <?php echo $this->session->userdata('name')?>.
						</div>	
						<div class="row">
							<div class="col-md-12 text-center">
								<div class="">
									<table class="table">
										<tr>
											<td>
												<div class="well well-sm" style="text-transform:uppercase;">
													<a class="btn-video" href="datos.generales">
													<img src="<?php echo base_url(); ?>assets/img_v1.1/thumb_videos/datos.generales.png" alt="Guia rapida Registro de Datos Generales" style="width:100%;">
													</a>
												</div>
											</td>
											<td>
												<div class="well well-sm" style="text-transform:uppercase;">
													<a class="btn-video" href="programacion.fisica">
													<img src="<?php echo base_url(); ?>assets/img_v1.1/thumb_videos/programacion.fisica.png" alt="Guia rapida Registro de Datos Generales" style="width:100%;">
													</a>
												</div>
											</td>
											<td>
												<div class="well well-sm" style="text-transform:uppercase;">
													<a class="btn-video" href="programacion.financiera">
													<img src="<?php echo base_url(); ?>assets/img_v1.1/thumb_videos/programacion.financiera.png" alt="Guia rapida Registro de Datos Generales" style="width:100%;">
													</a>
												</div>
											</td>
										</tr>
										<tr>
											<td>
											<div class="well well-sm" style="text-transform:uppercase;">
												<a class="btn-video" href="ejecucion.fisica">
													<a class="btn-video" href="ultimo">
												<img src="<?php echo base_url(); ?>assets/img_v1.1/thumb_videos/ejecucion.fisica.png" alt="Guia rapida Registro de Datos Generales" style="width:100%;">
												</a>
											</div>
											</td>
											<td>
												<div class="well well-sm" style="text-transform:uppercase;">
													<a class="btn-video" href="ultimo">
													<img src="<?php echo base_url(); ?>assets/img_v1.1/thumb_videos/ejecucion.financiera.png" alt="Guia rapida Registro de Datos Generales" style="width:100%;">
													</a>
												</div>
											</td>
											<td>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
  </div>
</div>						
						
						
						
						<hr class="hr_white">
						<hr class="hr_white">
					</div>

			</div>
		</div>
		<div id="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="row animated fadeInDown" style="margin-top:15px;">
					</div>
				</div>
			</div>
<!--			<div class="row">
				<h3>¿Necesitas capacitación?</h3>
				Revisa los manuales y video tutoriales disponibles
				<div class="col-md-12">
					<video width="320" height="240" controls>
					  <source src="<?php echo base_url(); ?>assets/video/registro_proyectos_part1_datos_generales.mp4" type="video/mp4">
					  Tu navegador Web no soporta este video. Es necesario que lo actualices.
					</video>
				</div>
				<h3>¿Necesitas asistencia técnica?</h3>
				Contactate con el personal 
				<div class="col-md-12">
					<video width="320" height="240" controls>
					  <source src="<?php echo base_url(); ?>assets/video/registro_proyectos_part1_datos_generales.mp4" type="video/mp4">
					  Tu navegador Web no soporta este video. Es necesario que lo actualices.
					</video>
				</div>
					
			</div>-->
		</div>
		<!-- END container-->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<span class="txt-color-white">
						<span class="copyleft">&copy;</span> 2017 - <span id="copy_year"></span> Centro de Investigación Agrícola Tropical - Bolivia
					</span>					
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!--[if IE 8]>
		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
		<![endif]-->
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<script type="<?php echo base_url(); ?>assets/text/javascript"></script>
		
		<!-- VERSION 1.1 -->
		<script src="<?php echo base_url(); ?>assets/js_v1.1/sly.min.js"></script>
		<!--MOSTRAR OPCIONES-->
		<script type="text/javascript">
			$(function(){
				//Año actual del Copyright
				$('#copy_year').text((new Date()).getFullYear());
				$('#copy_year').text((new Date()).getFullYear());
				//IFRAMES de guias
				$('.btn-guia').click(function(e){
						e.stopPropagation();
						e.preventDefault();
						var ruta = "<?php echo base_url('assets/guias/');?>";
						var narch = $(this).attr('href');
						var file = ruta+'/'+narch+'.pdf';
//						console.log("Redireccionando a: "+file);
						window.open(file);
//						$('#iframe_guia').attr('src',file);
	//					$('#guia_descarga').attr('href',file);
				});
				//IFRAMES de videos
//				$('.btn-video').click(function(e){
	//			});
				
				$(".btn-video").click(function(e){
						e.stopPropagation();
						e.preventDefault();
						var ruta = "<?php echo base_url('assets/video/');?>";
//						console.log("Esta es la ruta: "+ruta);
						var narch = $(this).attr('href');
						var file = ruta+'/'+narch+'.mp4';
						window.open(file);
//						console.log(file);
//						$('#iframe_video').attr('src',file);
//						$("video")[0].load();
//						$('#video_descarga').attr('href',file);
				});
				
			});

		</script>
		
		
<!--
######################################
DIALOGO PARA GUIAS
######################################
-->
<div class="modal fade" id="modal-guia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body" style="padding:5px;">
        <iframe id="iframe_guia" src="" style="border:none; width:100% !important; height:520px;"></iframe>
      </div>
      <div class="modal-footer" style="padding:5px;">
        <button type="button" class="btn btn-default" data-dismiss="modal">
			<i class="glyphicon glyphicon-remove"></i>
			Cerrar
		</button>
        <a id="guia_descarga" href="" class="btn btn-primary" download target="_blank">
			<i class="glyphicon glyphicon-download-alt"></i>
			Descargar gu&iacute;a
		</a>
      </div>
	  </div>
  </div>
</div>
<!--
######################################
DIALOGO PARA VIDEOS
######################################
-->
<div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body" style="padding:5px;">
		<video width="500" height="340" controls>
			<source id="iframe_video" src="" type="video/mp4">
			Tu navegador Web no soporta este video. Es necesario que lo actualices.
		</video>
      </div>
      <div class="modal-footer" style="padding:5px;">
        <button type="button" class="btn btn-default" data-dismiss="modal">
			<i class="glyphicon glyphicon-remove"></i>
			Cerrar
		</button>
        <a id="video_descarga" href="" class="btn btn-primary" download target="_blank">
			<i class="glyphicon glyphicon-download-alt"></i>
			Descargar video
		</a>
      </div>
	  </div>
  </div>
</div>
		
	</body>
</html>
<?php
 }else{
 ?>
<html>
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title><?php echo $this->session->userdata('name')?></title>

	<link href="https://fonts.googleapis.com/css?family=Overpass" rel="stylesheet">    <!-- Basic Styles -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
 	<script>
		function redireccionar(){window.location="<?php echo base_url(); ?>index.php/admin/logins";} 
		setTimeout ("redireccionar()", 3500);
	</script>
	<style>
		.espacio{height: 40%;}
		body{font-family: 'Overpass', sans-serif;}
	</style>
	</head>
	<body>
	<div class="container-fluid">
		<div class="espacio">
		</div>
		<div class="col-md-12 text-center">
			<img src="<?php echo base_url(); ?>assets/img_v1.1/SIIPP_logo_white.png" alt="<?php echo $this->session->userdata('name')?>" style="width:40%;">
			<h1 class="text-success">Cerrando sesi&oacute;n </h1>
			<img src="<?php echo base_url(); ?>assets/img_v1.1/preloader.gif" class="salir"/>
		</div>
	</div>
	</body>
</html>
	
<?php } ?>