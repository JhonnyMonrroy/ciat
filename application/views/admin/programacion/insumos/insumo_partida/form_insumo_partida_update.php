<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
        <style type="text/css">
            aside{background: #05678B;}
        </style>
    	<style type="text/css">
	    table{
	        font-size: 9px;
	        width: 100%;
	        max-width:1550px;;
	        overflow-x: scroll;
	        }
	    th{
	        padding: 1.4px;
	        text-align: center;
	        font-size: 9px;
	        }
	    </style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>

			<nav>
                <ul>
                    <li class="">
                        <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
                                    class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
                    </li>
                    <li class="text-center">
                        <?php
                        if($proyecto[0]['proy_estado']==1){ ?>
                            <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                            <?php
                        }
                        elseif ($proyecto[0]['proy_estado']==2){ ?>
                            <a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                            <?php
                        }
                        ?>
                    </li>
                    <?php
                    if($nro_fase==1){
                        for($i=0;$i<count($enlaces);$i++){ ?>
                            <li>
                                <a href="#" >
                                    <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                                <ul>
                                    <?php
                                    $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                                    foreach($submenu as $row) {
                                        ?>
                                        <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS OPERACIONES">...</a></li><li>Programaci&oacute;n de Requerimientos - Nivel de Actividades</li><li>Insumo Actividad</li><li>Modificar Requerimiento</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<!-- row -->
					<div class="row">
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				            <section id="widget-grid" class="well">
				                <div class="">
				                  <h1> PROGRAMACI&Oacute;N DE REQUERIMIENTO A NIVEL DE ACTIVIDADES </h1>
				                  <h1> CATEGORIA PROGRAM&Aacute;TICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad']?></small>
				                  <h1> <?php echo $titulo_proy;?> : <small><?php echo $proyecto[0]['proy_nombre']?></small>
				                  <!--<h1> PRODUCTO : <small><?php echo $dato_prod->prod_producto?></small>
				                  <h1> ACTIVIDAD : <small><?php echo $dato_act->act_actividad?></small>-->
				                </div>
				            </section>
				        </article>
				    </div>
				       <div class="row">
						<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
								<header>
									<span class="widget-icon"> <i class="fa fa-check"></i> </span>
								</header>
								<!-- widget div-->
								<div>
									<!-- widget edit box -->
									<div class="jarviswidget-editbox"></div>
									<div class="widget-body">
										<div class="row">
											<h2 class="alert alert-success"><center><?php //echo $ins_titulo;?></center></h2>
											<form action="<?php echo site_url("") . '/insumos/cprog_insumos_partida/guardar_insumo_partida_update' ?>" id="formulario" name="formulario" novalidate="novalidate" method="post">
			                            	<input type="hidden" name="proy_id" id="proy_id" value="<?php echo $proy_id;?>"> <!-- proy id -->
			                            	<input type="hidden" name="prod_id" id="prod_id" value="<?php echo $dato_prod->prod_id; ?>"> <!-- prod id -->
			                            	<input type="hidden" name="act_id" id="act_id" value="<?php echo $dato_act->act_id; ?>"> <!-- act id -->
			                            	<input type="hidden" name="insp_id" id="insp_id" value="<?php echo $insp_id ?>">
			                            	<!-- <input type="hidden" name="saldo_fin" id="saldo_fin" value="<?php echo $sumatorias[3]; ?>"> --> <!-- saldo prog -->
			                            	<input type="hidden" name="gestiones" id="gestiones" value="<?php echo $gestiones ?>"> <!-- gestiones -->
                                            <input class="form-control" type="hidden" name="gest" id="gest" value="<?php echo $fase[0]['pfec_fecha_inicio'] ?>">
			                            	<input type="hidden" name="gv" id="gv" value="<?php echo $this->session->userdata('gestion') ?>"> <!-- gestion vigente -->
			                            	<input type="hidden" name="gp" id="gp" value="0"> <!-- ptto gestion -->
                                                <article class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
			                            		<div class="row">
                                                    <div class="well">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label><font size="1"><b>SELECCIONE GRUPO </b></font><font color="blue">(Obligatorio)</font></label>
                                                                    <div >
                                                                        <select name="ins_partidas" id="ins_partidas" class="select2">
                                                                            <option value="">Seleccione una Partida</option>
                                                                            <?php
                                                                            foreach ($lista_partidas as $row) {
                                                                                ?>
                                                                                <option value="<?php echo $row['par_codigo'] ?>"<?php if($insumo[0]['gid']==$row['par_id']){ echo "selected";} ?>><?php echo $row['par_codigo'].'-'.$row['par_nombre']?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label><font size="1"><b>PARTIDA </b></font><font color="blue">(Obligatorio)</font></label>
                                                                    <select name="ins_partidas_dependientes" id="ins_partidas_dependientes" class="select2">
                                                                        <option value="">Seleccione</option>
                                                                        <?php
                                                                        foreach ($lista_partidas_hijo as $row) {
                                                                            ?>
                                                                            <option value="<?php echo $row['par_id'] ?>"<?php if($insumo[0]['par_id']==$row['par_id']){ echo "selected";} ?>><?php echo $row['par_codigo'].'-'.$row['par_nombre']?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label><font size="1"><b>FUENTE DE FINANCIAMIENTO </b></font><font color="blue">(Obligatorio)</font></label>
                                                                    <div >
                                                                        <select name="ins_ff" id="ins_ff" class="select2">
                                                                            <option value="">Seleccione Fuente financiamiento</option>
                                                                            <?php
                                                                            foreach($ffi as $row){
                                                                            ?>
                                                                                <option value="<?php echo $row['ff_id']; ?>" <?php if($insumo[0]['ff_id']==$row['ff_id']) echo 'selected';?>><?php echo $row['ff_codigo'].' - '.$row['ff_descripcion']; ?></option>
                                                                            <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label><font size="1"><b>ORGANISMO FINANCIADOR </b></font><font color="blue">(Obligatorio)</font></label>
                                                                    <select name="ins_of" id="ins_of" class="select2">
                                                                        <option value="">Seleccione Fuente Organismo </option>
                                                                        <?php
                                                                        foreach($fof as $row){
                                                                            ?>
                                                                            <option value="<?php echo $row['of_id']; ?>" <?php if($insumo[0]['of_id']==$row['of_id']) echo 'selected';?>><?php echo $row['of_codigo'].' - '.$row['of_descripcion']; ?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label><font size="1"><b>ENTIDAD DE TRANSFERENCIA</b></font><font color="blue">(Obligatorio)</font></label>
                                                                    <select name="ent_transf" id="ent_transf" class="select2">
                                                                        <option value="">Seleccione entidad de transferencia </option>
                                                                        <?php
                                                                        foreach($lista_entidad as $row){ ?>
                                                                            <option value="<?php echo $row['et_id']; ?>" <?php if($insumo[0]['et_id']==$row['et_id']) echo 'selected';?>><?php echo $row['et_codigo'].' - '.$row['et_descripcion']; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div> <!-- end well -->
                                                    <div class="well">
														<div class="row">

															<div class="col-sm-10">
																<div class="form-group">
																	<label><font size="1"><b>JUSTIFICACI&Oacute;N </b></font></label>
																		<textarea name="ins_detalle" id="ins_detalle" rows="3" class="form-control" maxlength="300"><?php echo $insumo[0]['insp_justificacion']?></textarea>
																</div>
															</div>
                                                            <div class="col-sm-2">
                                                                <div class="form-group">
                                                                    <label><font size="1"><b>PRESUPUESTO REQUERIDO</b></font></label>
                                                                    <input class="form-control" type="text" name="ins_cantidad" id="ins_cantidad" value="<?php echo $insumo[0]['insp_inicial']?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
                                                                </div>
                                                            </div>
														</div>
													</div><br>
												</div>
                                                </article>
                                                <article class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                                    <div class="row">
                                                        <div class="well">
                                                            <center><h3><strong>PROGRAMACI&Oacute;N FINANCIERA <?php echo $fase[0]['pfec_fecha_inicio'] ?> - <?php echo $fase[0]['pfec_fecha_fin'] ?></strong></h3></center>
                                                            <?php
                                                            $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
                                                            $suma_prog=0;
                                                            for($k=1;$k<=$años;$k++){
                                                                $prod_gest=$this->minsumos_partida->insumo_prog_mensual($insp_id,$fase[0]['pfec_fecha_inicio']);
                                                                $nro=0;
                                                                $total_gestion=0;
                                                                foreach($prod_gest as $row){
                                                                    $nro++;
                                                                    $matriz [1][$nro]=$row['m_id'];
                                                                    $matriz [2][$nro]=$row['inspg_monto_prog'];
                                                                    $suma_prog=$suma_prog+$row['inspg_monto_prog'];
                                                                }

                                                                for($j = 1 ;$j<=12 ;$j++){
                                                                    $matriz_r[1][$j]=$j;
                                                                    $matriz_r[2][$j]='0';
                                                                }

                                                                for($i = 1 ;$i<=$nro ;$i++){
                                                                    for($j = 1 ;$j<=12 ;$j++){
                                                                        if($matriz[1][$i]==$matriz_r[1][$j]){
                                                                            $matriz_r[2][$j]=round($matriz[2][$i],3);
                                                                            $total_gestion+=$matriz_r[2][$j];
                                                                        }
                                                                    }
                                                                }

                                                                ?>
                                                                <div class="row">
                                                                    <?php
                                                                    if($fase[0]['pfec_fecha_inicio']==$this->session->userdata("gestion")){
                                                                        ?>
                                                                        <div class="alert alert-block alert-success">
                                                                            <center><label><b>GESTI&Oacute;N ACTUAL - <?php echo $fase[0]['pfec_fecha_inicio'] ?></b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label style="text-align: right;">TOTAL:<input type="number" name="<?php echo $fase[0]['pfec_fecha_inicio']?>" id="g<?php echo $fase[0]['pfec_fecha_inicio']?>" value="<?php echo $total_gestion?>" disabled></label></center>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    elseif ($fase[0]['pfec_fecha_inicio']!=$this->session->userdata("gestion")) {
                                                                        ?>
                                                                        <div class="alert alert-block alert-success">
                                                                            <center><label>GESTI&Oacute;N  - <?php echo $fase[0]['pfec_fecha_inicio'] ?></label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label style="text-align: right;">TOTAL:<input type="number" name="<?php echo $fase[0]['pfec_fecha_inicio']?>" id="g<?php echo $fase[0]['pfec_fecha_inicio']?>" value="<?php echo $total_gestion?>" disabled></label></center>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <table class="table table-bordered table-hover" style="width:100%;" >
                                                                        <thead>
                                                                        <tr>
                                                                            <th style="width:20%;"><center>ENERO</center></th>
                                                                            <th style="width:20%;"><center>FEBRERO</center></th>
                                                                            <th style="width:20%;"><center>MARZO</center></th>
                                                                            <th style="width:20%;"><center>ABRIL</center></th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td><input  name="m1[]" id="<?php echo $fase[0]['pfec_fecha_inicio']?>" class="form-control num_decimal" type="text" onkeyup="suma(this.form,this.id)" style="width:100%;" value="<?php echo $matriz_r[2][1]; ?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
                                                                            <td><input  name="m2[]" id="<?php echo $fase[0]['pfec_fecha_inicio']?>" class="form-control num_decimal" type="text" onkeyup="suma(this.form,this.id)" style="width:100%;" value="<?php echo $matriz_r[2][2]; ?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
                                                                            <td><input  name="m3[]" id="<?php echo $fase[0]['pfec_fecha_inicio']?>" class="form-control num_decimal" type="text" onkeyup="suma(this.form,this.id)" style="width:100%;" value="<?php echo $matriz_r[2][3]; ?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
                                                                            <td><input  name="m4[]" id="<?php echo $fase[0]['pfec_fecha_inicio']?>" class="form-control num_decimal" type="text" onkeyup="suma(this.form,this.id)" style="width:100%;" value="<?php echo $matriz_r[2][4]; ?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <table class="table table-bordered table-hover" style="width:100%;" >
                                                                        <thead>
                                                                        <tr>
                                                                            <th style="width:20%;"><center>MAYO</center></th>
                                                                            <th style="width:20%;"><center>JUNIO</center></th>
                                                                            <th style="width:20%;"><center>JULIO</center></th>
                                                                            <th style="width:20%;"><center>AGOSTO</center></th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td><input  name="m5[]" id="<?php echo $fase[0]['pfec_fecha_inicio']?>" class="form-control num_decimal" type="text" onkeyup="suma(this.form,this.id)" style="width:100%;" value="<?php echo $matriz_r[2][5]; ?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
                                                                            <td><input  name="m6[]" id="<?php echo $fase[0]['pfec_fecha_inicio']?>" class="form-control num_decimal" type="text" onkeyup="suma(this.form,this.id)" style="width:100%;" value="<?php echo $matriz_r[2][6]; ?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
                                                                            <td><input  name="m7[]" id="<?php echo $fase[0]['pfec_fecha_inicio']?>" class="form-control num_decimal" type="text" onkeyup="suma(this.form,this.id)" style="width:100%;" value="<?php echo $matriz_r[2][7]; ?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
                                                                            <td><input  name="m8[]" id="<?php echo $fase[0]['pfec_fecha_inicio']?>" class="form-control num_decimal" type="text" onkeyup="suma(this.form,this.id)" style="width:100%;" value="<?php echo $matriz_r[2][8]; ?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <table class="table table-bordered table-hover" style="width:100%;" >
                                                                        <thead>
                                                                        <tr>
                                                                            <th style="width:20%;"><center>SEPTIEMBRE</center></th>
                                                                            <th style="width:20%;"><center>OCTUBRE</center></th>
                                                                            <th style="width:20%;"><center>NOVIEMBRE</center></th>
                                                                            <th style="width:20%;"><center>DICIEMBRE</center></th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td><input  name="m9[]"  id="<?php echo $fase[0]['pfec_fecha_inicio']?>" class="form-control num_decimal" type="text" onkeyup="suma(this.form,this.id)" style="width:100%;" value="<?php echo $matriz_r[2][9]; ?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
                                                                            <td><input  name="m10[]" id="<?php echo $fase[0]['pfec_fecha_inicio']?>" class="form-control num_decimal" type="text" onkeyup="suma(this.form,this.id)" style="width:100%;" value="<?php echo $matriz_r[2][10]; ?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
                                                                            <td><input  name="m11[]" id="<?php echo $fase[0]['pfec_fecha_inicio']?>" class="form-control num_decimal" type="text" onkeyup="suma(this.form,this.id)" style="width:100%;" value="<?php echo $matriz_r[2][11]; ?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
                                                                            <td><input  name="m12[]" id="<?php echo $fase[0]['pfec_fecha_inicio']?>" class="form-control num_decimal" type="text" onkeyup="suma(this.form,this.id)" style="width:100%;" value="<?php echo $matriz_r[2][12]; ?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <?php
                                                                $fase[0]['pfec_fecha_inicio']++;
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="well">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label><font size="2" color="blue"><b>SUMA TOTAL PROGRAMADO</b></font></label>
                                                                        <input class="form-control" name="total" type="text" id="total" value="<?php echo round($suma_prog,2) ?>" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="">
                                                                        <div class="form-group">
                                                                            <LABEL><b>SALDO POR PROGRAMAR</b></label>
                                                                            <input type="text" name="saldo" id="saldo" class="form-control" value="0" disabled="true">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
												<div  class="col-sm-12">
													<div class="form-actions">
														<a href="<?php echo site_url("").'/prog/ins_part/'.$proy_id.'/'.$prod_id.'/'.$act_id.''; ?>" class="btn btn-lg btn-default" title="REQUERIMIENTOS DE LA OPERACION"> CANCELAR </a>
														<input type="button" value="GUARDAR REQUERIMIENTO" id="btsubmit" class="btn btn-primary btn-lg" onclick="valida_envia()" title="GUARDAR REQUERIMIENTO">
													</div>
												</div>
											</form>
										</div>
				
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</div>
					</div>
				</section>
				<!-- end widget grid -->
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>

		<script>
			if (!window.jQuery) {document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');}
		</script>
		<script>
			if (!window.jQuery.ui) {document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<script type="text/javascript">

		$(document).ready(function() {
			
			pageSetUp();
			$("#ins_partidas").change(function () {
                $("#ins_partidas option:selected").each(function () {
                elegido=$(this).val();
                $.post("<?php echo base_url(); ?>index.php/prog/combo_partidas", { elegido: elegido }, function(data){ 
                $("#ins_partidas_dependientes").html(data);
                });     
            });
            });  
		})

        function valida_envia() {

            if (document.formulario.ins_partidas.value==""){
                alertify.alert("SELECCIONE GRUPO");
                document.formulario.ins_partidas.focus();
                return 0;
            }
            if (document.formulario.ins_partidas_dependientes.value==""){
                alertify.alert("SELECCIONE PARTIDA")
                document.formulario.ins_partidas_dependientes.focus()
                return 0;
            }
            if (document.formulario.ins_ff.value==""){
                alertify.alert("SELECCIONE FUENTE DE FINANCIAMIENTO")
                document.formulario.ins_ff.focus()
                return 0;
            }
            if (document.formulario.ins_of.value==""){
                alertify.alert("SELECCIONE ORGANISMO FINANCIADOR")
                document.formulario.ins_of.focus()
                return 0;
            }
            if (document.formulario.ent_transf.value==""){
                alertify.alert("SELECCIONE ENTIDAD DE TRANSFERENCIA")
                document.formulario.ent_transf.focus()
                return 0;
            }

            if (document.formulario.ins_detalle.value==""){
                alertify.alert("REGISTRE JUSTIFICACIÓN")
                document.formulario.ins_detalle.focus()
                return 0;
            }
            if (document.formulario.ins_cantidad.value==""){
                alertify.alert("REGISTRE PRESUPUESTO REQUERIDO")
                document.formulario.ins_cantidad.focus()
                return 0;
            }
            if (document.formulario.ins_cantidad.value==0){
                alertify.alert("REGISTRE PRESUPUESTO REQUERIDO")
                document.formulario.ins_cantidad.focus()
                return 0;
            }

            if(parseFloat(document.getElementById("ins_cantidad").value)==parseFloat(document.getElementById("total").value)){
                alertify.confirm("GUARDAR PROGRAMACIÓN ?", function (a) {
                    if (a) {
                        document.formulario.submit();
                    } else {
                        alertify.error("OPCI\u00D3N CANCELADA");
                    }
                });
            }
            else{
                if(parseFloat(document.getElementById("ins_cantidad").value)>parseFloat(document.getElementById("total").value)){
                    alertify.alert("<font size=2>ERROR !! LA SUMA PROGRAMADO POR GESTIONES NO PUEDE SER MENOR AL TOTAL DEL PRESUPUESTO REQUERIDO</font>");
                    return false;
                }
                else{
                    alertify.alert("<font size=2>ERROR !! LA SUMA PROGRAMADO POR GESTIONES NO PUEDE SER MAYOR AL TOTAL DEL PRESUPUESTO REQUERIDO</font>");
                    return false;
                }
            }
        }
		</script>
        <script>
            suma=function(f,id){
                var total=0;
                var total_gest=0;
                for(var x=0;x<f.length;x++){//recorremos los campos dentro del form
                    if(f[x].name.indexOf('m1')!=-1){
                        total+=Number(f[x].value);
                    }
                    if(f[x].name.indexOf('m2')!=-1){
                        total+=Number(f[x].value);
                    }
                    if(f[x].name.indexOf('m3')!=-1){
                        total+=Number(f[x].value);
                    }
                    if(f[x].name.indexOf('m4')!=-1){
                        total+=Number(f[x].value);
                    }
                    if(f[x].name.indexOf('m5')!=-1){
                        total+=Number(f[x].value);
                    }
                    if(f[x].name.indexOf('m6')!=-1){
                        total+=Number(f[x].value);
                    }
                    if(f[x].name.indexOf('m7')!=-1){
                        total+=Number(f[x].value);
                    }
                    if(f[x].name.indexOf('m8')!=-1){
                        total+=Number(f[x].value);
                    }
                    if(f[x].name.indexOf('m9')!=-1){
                        total+=Number(f[x].value);
                    }
                    if(f[x].id==id){
                        total_gest+=Number(f[x].value)
                    }
                }

                if (total - Math.floor(total)!= 0){
                    total = total.toFixed(2)
                }
                if (total_gest - Math.floor(total_gest)!= 0){
                    total_gest = total_gest.toFixed(2)
                }
                var id_g='g'+id;
                document.getElementById('total').value=total;//al final colocamos la suma en algÃºn input.
                document.getElementById(id_g).value=total_gest;//Total por gestion

                ctotal = parseFloat($('[name="ins_cantidad"]').val()); ///// Costo Total
                $('[name="saldo"]').val((ctotal-(total)).toFixed(2) );
            }
            //funcion permite el ingreso de decimales
            function numerosDecimales(e) {
                key = e.keyCode || e.which;
                tecla = String.fromCharCode(key).toLowerCase();
                letras = " 0123456789";
                especiales = [46];

                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                        tecla_especial = true;
                        break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial)
                    return false;
            }
        </script>
	</body>
</html>
