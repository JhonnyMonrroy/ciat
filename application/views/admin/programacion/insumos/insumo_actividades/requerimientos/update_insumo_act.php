<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <title><?php echo $this->session->userdata('name')?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
    <style type="text/css">
        aside{background: #05678B;}
    </style>
    <style type="text/css">
        table{
            font-size: 9px;
            width: 100%;
            max-width:1550px;;
            overflow-x: scroll;
        }
        th{
            padding: 1.4px;
            text-align: center;
            font-size: 9px;
        }
    </style>
</head>
<body class="">
<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
<!-- HEADER -->
<header id="header">
    <!-- pulled right: nav area -->
    <div class="pull-right">
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->
        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->
        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
    </div>
    <!-- end pulled right: nav area -->
</header>
<!-- END HEADER -->

<!-- Left panel : Navigation area -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is -->
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a>
				</span>
    </div>

    <nav>
        <ul>
            <li class="">
                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
                            class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
            </li>
            <li class="text-center">
                <?php
                if($proyecto[0]['proy_estado']==1){ ?>
                    <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                    <?php
                }
                elseif ($proyecto[0]['proy_estado']==2){ ?>
                    <a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                    <?php
                }
                ?>
            </li>
            <?php
            if($nro_fase==1){
                for($i=0;$i<count($enlaces);$i++){ ?>
                    <li>
                        <a href="#" >
                            <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                        <ul>
                            <?php
                            $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                            foreach($submenu as $row) {
                                ?>
                                <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
    </nav>
    <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

</aside>

<!-- MAIN PANEL -->
<div id="main" role="main">
    <!-- RIBBON -->
    <div id="ribbon">
				<span class="ribbon-button-alignment">
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span>
				</span>
        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS OPERACIONES">...</a></li><li>Programaci&oacute;n de Requerimientos - Nivel de Actividades</li><li>Insumo Actividad</li><li>Modificar Requerimiento</li>
        </ol>
    </div>
    <!-- END RIBBON -->
    <!-- MAIN CONTENT -->
    <div id="content">
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <section id="widget-grid" class="well">
                        <div class="">
                            <h1> PROGRAMACI&Oacute;N DE REQUERIMIENTO A NIVEL DE ACTIVIDADES - NUEVO INSUMO</h1>
                            <h1> CATEGORIA PROGRAM&Aacute;TICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad']?></small>
                                <h1> <?php echo $titulo_proy;?> : <small><?php echo $proyecto[0]['proy_nombre']?></small>
                                    <h1> PRODUCTO : <small><?php echo $dato_prod->prod_producto?></small>
                                        <h1> ACTIVIDAD : <small><?php echo $dato_act->act_actividad?></small>
                        </div>
                    </section>
                </article>
            </div>
            <div class="row">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-check"></i> </span>
                    </header>
                    <!-- widget div-->
                    <div>
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body">
                            <div class="row">
                                <h2 class="alert alert-success"><center><?php echo $ins_titulo;?></center></h2>
                                <form action="<?php echo site_url("") . '/insumos/cprog_insumos_directo/guardar_update_insumo' ?>" id="ins_form_nuevo" name="ins_form_nuevo" novalidate="novalidate" method="post">
                                    <input type="hidden" name="proy_id" id="proy_id" value="<?php echo $proy_id;?>"> <!-- proy id -->
                                    <input type="hidden" name="prod_id" id="prod_id" value="<?php echo $dato_prod->prod_id; ?>"> <!-- prod id -->
                                    <input type="hidden" name="act_id" id="act_id" value="<?php echo $dato_act->act_id; ?>"> <!-- act id -->
                                    <input type="hidden" name="ins_id" id="ins_id" value="<?php echo $ins_id ?>"> <!-- ins id -->
                                    <input type="hidden" name="insp_id" id="insp_id" value="<?php echo $insp_id ?>">
                                    <!-- <input type="hidden" name="cant_fin" id="cant_fin" value="<?php echo count($techo) ?>"> -->
                                    <input type="hidden" name="ins_tipo" id="ins_tipo" value="<?php echo $ins_tipo ?>"> <!-- ins tipo -->
                                    <input type="hidden" name="saldo_fin" id="saldo_fin" value="<?php echo $saldo_por_programar; ?>">
                                    <input type="hidden" name="gestiones" id="gestiones" value="<?php echo $gestiones ?>"> <!-- gestiones -->
                                    <input type="hidden" name="gestion" id="gestion" value="<?php echo $gestion ?>"> <!-- gestion -->
                                    <input type="hidden" name="gv" id="gv" value="<?php echo $this->session->userdata('gestion') ?>"> <!-- gestion vigente -->
                                    <input type="hidden" name="gp" id="gp" value="<?php echo $monto_programado;?>"> <!-- ptto gestion -->
                                    <?php
                                    if($ins_tipo==1)
                                    {
                                        ?>
                                        <div id="bootstrap-wizard-1" class="col-sm-9">
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>FECHA REQUERIMIENTO </b></font><font color="blue">(dd/mm/yy)</font></label>
                                                            <div class="input-group">
                                                                <input type="text" name="ins_fecha" id="ins_fecha" value="<?php echo date('d/m/Y',strtotime($insumo[0]['ins_fecha_requerimiento'])) ?>" onKeyUp="this.value=formateafecha(this.value);" placeholder="Seleccione de Requerimiento" class="form-control datepicker"  data-dateformat="dd/mm/yy" title="Seleccione fecha del Requerimiento" >
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>SALDO POR PROGRAMAR </b></font></label>
                                                            <div class="input-group">
                                                                <input class="form-control" name="saldo_programar" type="text" value="<?php echo number_format($saldo_por_programar, 2, ',', '.'); ?>" disabled="disabled">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>DETALLE </b></font></label>
                                                            <textarea name="ins_detalle" id="ins_detalle" rows="2" class="form-control" maxlength="300"><?php echo $insumo[0]['ins_detalle'];?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>CANTIDAD (meses)</b></font></label>
                                                            <input class="form-control" type="text" name="ins_cantidad" id="ins_cantidad" value="<?php echo round($insumo[0]['ins_cant_requerida'],2);?>" onblur="costo_total()" onkeypress="if (this.value.length < 7) { return numerosDecimales(event);}else{return false; }">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>COSTO UNITARIO</b></font></label>
                                                            <input class="form-control" type="text" name="ins_costo_unitario" id="ins_costo_unitario" value="<?php echo round($insumo[0]['ins_costo_unitario'],2);?>" onblur="costo_total()" onkeyup="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>COSTO TOTAL</b></font></label>
                                                            <input class="form-control" type="hidden" name="ins_costo_total_ant" id="ins_costo_total_ant" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="hidden" name="ins_costo_total" id="ins_costo_total" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="text" name="ins_costo_total2" id="ins_costo_total2" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>" disabled="true">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>
                                        </div><br>
                                        <?php
                                    }
                                    elseif($ins_tipo==2)
                                    {
                                        ?>
                                        <div id="bootstrap-wizard-1" class="col-sm-9">
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>FECHA REQUERIMIENTO </b></font><font color="blue">(dd/mm/yy)</font></label>
                                                            <div class="input-group">
                                                                <input type="text" name="ins_fecha" id="ins_fecha" value="<?php echo date('d/m/Y',strtotime($insumo[0]['ins_fecha_requerimiento'])) ?>" onKeyUp="this.value=formateafecha(this.value);" placeholder="Seleccione de Requerimiento" class="form-control datepicker"  data-dateformat="dd/mm/yy" title="Seleccione fecha del Requerimiento">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>SALDO POR PROGRAMAR </b></font></label>
                                                            <div class="input-group">
                                                                <input class="form-control" name="saldo_programar" type="text" value="<?php echo number_format($saldo_por_programar, 2, ',', '.'); ?>" disabled="disabled">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>DETALLE DEL SERVICIO</b></font></label>
                                                            <textarea name="ins_detalle" id="ins_detalle" rows="2" class="form-control" maxlength="300"><?php echo $insumo[0]['ins_detalle'];?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>CANTIDAD (meses)</b></font></label>
                                                            <input class="form-control" type="text" name="ins_cantidad" id="ins_cantidad" value="<?php echo round($insumo[0]['ins_cant_requerida'],2);?>" onblur="costo_total()" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>COSTO UNITARIO</b></font></label>
                                                            <input class="form-control" type="text" name="ins_costo_unitario" id="ins_costo_unitario" value="<?php echo round($insumo[0]['ins_costo_unitario'],2);?>" onblur="costo_total()" onkeyup="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>COSTO TOTAL</b></font></label>
                                                            <input class="form-control" type="hidden" name="ins_costo_total_ant" id="ins_costo_total_ant" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="hidden" name="ins_costo_total" id="ins_costo_total" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="text" name="ins_costo_total2" id="ins_costo_total2" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>" disabled="true">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>
                                        </div><br>
                                        <?php
                                    }
                                    elseif($ins_tipo==3)
                                    {
                                        ?>
                                        <div id="bootstrap-wizard-1" class="col-sm-9">
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>FECHA REQUERIMIENTO </b></font><font color="blue">(dd/mm/yy)</font></label>
                                                            <div class="input-group">
                                                                <input type="text" name="ins_fecha" id="ins_fecha" value="<?php echo date('d/m/Y',strtotime($insumo[0]['ins_fecha_requerimiento'])) ?>" onKeyUp="this.value=formateafecha(this.value);" placeholder="Seleccione de Requerimiento" class="form-control datepicker"  data-dateformat="dd/mm/yy" title="Seleccione fecha del Requerimiento">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>SALDO POR PROGRAMAR </b></font></label>
                                                            <div class="input-group">
                                                                <input class="form-control" name="saldo_programar" type="text" value="<?php echo number_format($saldo_por_programar, 2, ',', '.'); ?>" disabled="disabled">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>RUTA</b></font></label>
                                                            <textarea name="ins_detalle" id="ins_detalle" rows="3" class="form-control" maxlength="300"><?php echo $insumo[0]['ins_detalle'];?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>CANTIDAD (meses)</b></font></label>
                                                            <input class="form-control" type="text" name="ins_cantidad" id="ins_cantidad" value="<?php echo round($insumo[0]['ins_cant_requerida'],2);?>" onblur="costo_total()" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>COSTO UNITARIO</b></font></label>
                                                            <input class="form-control" type="text" name="ins_costo_unitario" id="ins_costo_unitario" value="<?php echo round($insumo[0]['ins_costo_unitario'],2);?>" onblur="costo_total()" onkeyup="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>COSTO TOTAL</b></font></label>
                                                            <input class="form-control" type="hidden" name="ins_costo_total_ant" id="ins_costo_total_ant" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="hidden" name="ins_costo_total" id="ins_costo_total" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="text" name="ins_costo_total2" id="ins_costo_total2" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>" disabled="true">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>

                                        </div><br>
                                        <?php
                                    }
                                    elseif($ins_tipo==4)
                                    {
                                        ?>
                                        <div id="bootstrap-wizard-1" class="col-sm-9">
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>FECHA REQUERIMIENTO </b></font><font color="blue">(dd/mm/yy)</font></label>
                                                            <div class="input-group">
                                                                <input type="text" name="ins_fecha" id="ins_fecha" value="<?php echo date('d/m/Y',strtotime($insumo[0]['ins_fecha_requerimiento'])) ?>" onKeyUp="this.value=formateafecha(this.value);" placeholder="Seleccione de Requerimiento" class="form-control datepicker"  data-dateformat="dd/mm/yy" title="Seleccione fecha del Requerimiento">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>SALDO POR PROGRAMAR </b></font></label>
                                                            <div class="input-group">
                                                                <input class="form-control" name="saldo_programar" type="text" value="<?php echo number_format($saldo_por_programar, 2, ',', '.'); ?>" disabled="disabled">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>CLASIFICACI&Oacute;N DE DESTINO</b></font></label>
                                                            <textarea name="ins_detalle" id="ins_detalle" rows="3" class="form-control" maxlength="300"><?php echo $insumo[0]['ins_detalle'];?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>D&Iacute;AS V&Iacute;ATICO</b></font></label>
                                                            <input class="form-control" type="text" name="ins_cantidad" id="ins_cantidad" value="<?php echo round($insumo[0]['ins_cant_requerida'],2);?>" onblur="costo_total()" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>V&Iacute;ATICO DIARIO</b></font></label>
                                                            <input class="form-control" type="text" name="ins_costo_unitario" id="ins_costo_unitario" value="<?php echo round($insumo[0]['ins_costo_unitario'],2);?>" onblur="costo_total()" onkeyup="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>COSTO TOTAL</b></font></label>
                                                            <input class="form-control" type="hidden" name="ins_costo_total_ant" id="ins_costo_total_ant" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="hidden" name="ins_costo_total" id="ins_costo_total" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="text" name="ins_costo_total2" id="ins_costo_total2" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>" disabled="true">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>
                                        </div><br>
                                        <?php
                                    }
                                    elseif($ins_tipo==5)
                                    {
                                        ?>
                                        <div id="bootstrap-wizard-1" class="col-sm-9">
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>FECHA REQUERIMIENTO </b></font><font color="blue">(dd/mm/yy)</font></label>
                                                            <div class="input-group">
                                                                <input type="text" name="ins_fecha" id="ins_fecha" value="<?php echo date('d/m/Y',strtotime($insumo[0]['ins_fecha_requerimiento'])) ?>" onKeyUp="this.value=formateafecha(this.value);" placeholder="Seleccione de Requerimiento" class="form-control datepicker"  data-dateformat="dd/mm/yy" title="Seleccione fecha del Requerimiento">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>SALDO POR PROGRAMAR </b></font></label>
                                                            <div class="input-group">
                                                                <input class="form-control" name="saldo_programar" type="text" value="<?php echo number_format($saldo_por_programar, 2, ',', '.'); ?>" disabled="disabled">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>DESCRIPCI&Oacute;N DE LA CONSULTORIA</b></font></label>
                                                            <textarea name="ins_detalle" id="ins_detalle" rows="2" class="form-control" maxlength="300"><?php echo $insumo[0]['ins_detalle'];?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>OBJETIVO DE LA CONSULTORIA</b></font></label>
                                                            <textarea name="objetivo" id="objetivo" rows="3" class="form-control" maxlength="300"><?php echo $insumo[0]['ins_objetivo'];?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>

                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>DURACI&Oacute;N DE LA CONSULTORIA</b></font></label>
                                                            <input class="form-control" type="text" name="ins_duracion" id="ins_duracion" value="<?php echo $insumo[0]['ins_duracion'];?>" onkeypress="if (this.value.length < 7) { return numerosDecimales(event);}else{return false; }">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>FECHA INICIO </b></font><font color="blue">(dd/mm/yy)</font></label>
                                                            <div class="input-group">
                                                                <input type="text" name="ins_i" id="ins_fi" value="<?php echo date('d/m/Y',strtotime($insumo[0]['ins_fecha_inicio'])) ?>" onKeyUp="this.value=formateafecha(this.value);" placeholder="Seleccione de Requerimiento" class="form-control datepicker"  data-dateformat="dd/mm/yy" title="Seleccione fecha del Requerimiento">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>FECHA DE CONCLUSI&Oacute;N </b></font><font color="blue">(dd/mm/yy)</font></label>
                                                            <div class="input-group">
                                                                <input type="text" name="ins_f" id="ins_f" value="<?php echo date('d/m/Y',strtotime($insumo[0]['ins_fecha_conclusion'])) ?>" onKeyUp="this.value=formateafecha(this.value);" placeholder="Seleccione de Requerimiento" class="form-control datepicker"  data-dateformat="dd/mm/yy" title="Seleccione fecha del Requerimiento">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>

                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>PRODUCTO</b></font></label>
                                                            <textarea name="ins_prod" id="ins_prod" rows="3" class="form-control" maxlength="300"><?php echo $insumo[0]['ins_productos'];?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>EVALUACI&Oacute;N</b></font></label>
                                                            <textarea name="ins_eva" id="ins_eva" rows="3" class="form-control" maxlength="300"><?php echo $insumo[0]['ins_evaluador'];?></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div><br>

                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>CANTIDAD REQUERIDA</b></font></label>
                                                            <input class="form-control" type="text" name="ins_cantidad" id="ins_cantidad" value="<?php echo round($insumo[0]['ins_cant_requerida'],2);?>" onblur="costo_total()" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>COSTO UNITARIO</b></font></label>
                                                            <input class="form-control" type="text" name="ins_costo_unitario" id="ins_costo_unitario" value="<?php echo round($insumo[0]['ins_costo_unitario'],2);?>" onblur="costo_total()" onkeyup="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>COSTO TOTAL</b></font></label>
                                                            <input class="form-control" type="hidden" name="ins_costo_total_ant" id="ins_costo_total_ant" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="hidden" name="ins_costo_total" id="ins_costo_total" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="text" name="ins_costo_total2" id="ins_costo_total2" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>" disabled="true">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>
                                        </div><br>
                                        <?php
                                    }
                                    elseif($ins_tipo==6)
                                    {
                                        ?>
                                        <div id="bootstrap-wizard-1" class="col-sm-9">
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>FECHA REQUERIMIENTO </b></font><font color="blue">(dd/mm/yy)</font></label>
                                                            <div class="input-group">
                                                                <input type="text" name="ins_fecha" id="ins_fecha" value="<?php echo date('d/m/Y',strtotime($insumo[0]['ins_fecha_requerimiento'])) ?>" onKeyUp="this.value=formateafecha(this.value);" placeholder="Seleccione de Requerimiento" class="form-control datepicker"  data-dateformat="dd/mm/yy" title="Seleccione fecha del Requerimiento">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>SALDO POR PROGRAMAR </b></font></label>
                                                            <div class="input-group">
                                                                <input class="form-control" name="saldo_programar" type="text" value="<?php echo number_format($saldo_por_programar, 2, ',', '.'); ?>" disabled="disabled">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>DURACI&Oacute;N DE LA CONSULTORIA</b></font></label>
                                                            <input class="form-control" type="text" name="ins_duracion" id="ins_duracion" value="<?php echo $insumo[0]['ins_duracion'];?>" onkeypress="if (this.value.length < 7) { return soloNumeros(event);}else{return false; }">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>

                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>FECHA INICIO </b></font><font color="blue">(dd/mm/yy)</font></label>
                                                            <div class="input-group">
                                                                <input type="text" name="ins_i" id="ins_i" value="<?php echo date('d/m/Y',strtotime($insumo[0]['ins_fecha_inicio'])) ?>" onKeyUp="this.value=formateafecha(this.value);" placeholder="Seleccione de Requerimiento" class="form-control datepicker"  data-dateformat="dd/mm/yy" title="Seleccione fecha del Requerimiento">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>FECHA DE CONCLUSI&Oacute;N </b></font><font color="blue">(dd/mm/yy)</font></label>
                                                            <div class="input-group">
                                                                <input type="text" name="ins_f" id="ins_f" value="<?php echo date('d/m/Y',strtotime($insumo[0]['ins_fecha_conclusion'])) ?>" onKeyUp="this.value=formateafecha(this.value);" placeholder="Seleccione de Requerimiento" class="form-control datepicker"  data-dateformat="dd/mm/yy" title="Seleccione fecha del Requerimiento">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>

                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>ACTIVIDADES, FUNCIONES DEL CONSULTOR</b></font></label>
                                                            <textarea name="ins_act" id="ins_act" rows="3" class="form-control" maxlength="300"><?php echo $insumo[0]['ins_actividades'];?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>PERFIL/REQUISITOS</b></font></label>
                                                            <textarea name="ins_perfil" id="ins_perfil" rows="3" class="form-control" maxlength="300"><?php echo $insumo[0]['ins_perfil'];?></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div><br>

                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>CARGO</b></font></label>
                                                            <input class="form-control" type="text" name="ins_cargo" id="ins_cargo" value="<?php echo $insumo[0]['ins_cargo'];?>" maxlength="100">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>EVALUADOR DE LA CONSULTORIA</b></font></label>
                                                            <input class="form-control" type="text" name="ins_eva" id="ins_eva" value="<?php echo $insumo[0]['ins_evaluador'];?>" maxlength="100">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"> <b>CARGO EQUIVALENTE ESCALA SALARIAL</b></font></label>
                                                            <select name="ins_car_id" id="ins_car_id" class="form-control">
                                                                <option value="">Seleccione</option>
                                                                <?php
                                                                foreach ($lista_cargo as $row) {
                                                                    if($row['car_id']==$insumo[0]['car_id'])
                                                                    {
                                                                        ?>
                                                                        <option value="<?php echo $row['car_id'] ?>" selected="true"><?php echo $row['car_cargo'] . "  -  " . $row['car_sueldo'] ?></option>
                                                                        <?php
                                                                    }
                                                                    else
                                                                    {
                                                                        ?>
                                                                        <option value="<?php echo $row['car_id'] ?>"><?php echo $row['car_cargo'] . "  -  " . $row['car_sueldo'] ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>

                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>CANTIDAD REQUERIDA</b></font></label>
                                                            <input class="form-control" type="text" name="ins_cantidad" id="ins_cantidad" value="<?php echo round($insumo[0]['ins_cant_requerida'],2);?>" onblur="costo_total()" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>COSTO UNITARIO</b></font></label>
                                                            <input class="form-control" type="text" name="ins_costo_unitario" id="ins_costo_unitario" value="<?php echo round($insumo[0]['ins_costo_unitario'],2);?>" onblur="costo_total()" onkeyup="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>COSTO TOTAL</b></font></label>
                                                            <input class="form-control" type="hidden" name="ins_costo_total_ant" id="ins_costo_total_ant" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="hidden" name="ins_costo_total" id="ins_costo_total" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="text" name="ins_costo_total2" id="ins_costo_total2" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>" disabled="true">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>

                                        </div><br>
                                        <?php
                                    }
                                    elseif($ins_tipo==7 || $ins_tipo==8 || $ins_tipo==9)
                                    {
                                        ?>
                                        <div id="bootstrap-wizard-1" class="col-sm-9">
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>FECHA REQUERIMIENTO </b></font><font color="blue">(dd/mm/yy)</font></label>
                                                            <div class="input-group">
                                                                <input type="text" name="ins_fecha" id="ins_fecha" value="<?php echo date('d/m/Y',strtotime($insumo[0]['ins_fecha_requerimiento'])) ?>" onKeyUp="this.value=formateafecha(this.value);" placeholder="Seleccione de Requerimiento" class="form-control datepicker"  data-dateformat="dd/mm/yy" title="Seleccione fecha del Requerimiento">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>SALDO POR PROGRAMAR </b></font></label>
                                                            <div class="input-group">
                                                                <input class="form-control" name="saldo_programar" type="text" value="<?php echo number_format($saldo_por_programar, 2, ',', '.'); ?>" disabled="disabled">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>DESCRIPCI&Oacute;N </b></font></label>
                                                            <textarea name="ins_detalle" id="ins_detalle" rows="2" class="form-control" maxlength="300"><?php echo $insumo[0]['ins_detalle'];?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>CANTIDAD</b></font></label>
                                                            <input class="form-control" type="text" name="ins_cantidad" id="ins_cantidad" value="<?php echo round($insumo[0]['ins_cant_requerida'],2);?>" onblur="costo_total()" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>COSTO UNITARIO</b></font></label>
                                                            <input class="form-control" type="text" name="ins_costo_unitario" id="ins_costo_unitario" value="<?php echo round($insumo[0]['ins_costo_unitario'],2);?>" onblur="costo_total()" onkeyup="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>COSTO TOTAL</b></font></label>
                                                            <input class="form-control" type="hidden" name="ins_costo_total_ant" id="ins_costo_total_ant" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="hidden" name="ins_costo_total" id="ins_costo_total" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>">
                                                            <input class="form-control" type="text" name="ins_costo_total2" id="ins_costo_total2" value="<?php echo round($insumo[0]['ins_costo_total'],2);?>" disabled="true">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label><font size="1"><b>UNIDAD DE MEDIDA</b></font></label>
                                                            <input class="form-control" type="text" name="ins_unidad_medida" id="ins_unidad_medida" maxlength="100" value="<?php echo $insumo[0]['ins_unidad_medida'];?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br>
                                        </div><br>
                                        <?php
                                    }

                                    ?>
                                    <div  class="col-sm-12">
                                        <div class="form-actions">
                                            <a href="<?php echo base_url().'index.php/prog/ins_act/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/'.$insp_id; ?>" class="btn btn-lg btn-default" title="REQUERIMIENTOS DE LA OPERACION"> CANCELAR </a>
                                            <input type="button" value="GUARDAR REQUERIMIENTO" id="btsubmit" class="btn btn-primary btn-lg" onclick="valida_envia()" title="GUARDAR REQUERIMIENTO">
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                        <!-- end widget content -->
                    </div>
                    <!-- end widget div -->
                </div>
            </div>
        </section>
        <!-- end widget grid -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN PANEL -->
<!-- PAGE FOOTER -->
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
        </div>
    </div>
</div>
<!-- END PAGE FOOTER -->
<!--================================================== -->
<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>

<script>
    if (!window.jQuery) {document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');}
</script>
<script>
    if (!window.jQuery.ui) {document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');}
</script>
<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
<script src="<?php echo base_url(); ?>mis_js/programacion/insumos/insumos_directos.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
<!-- JARVIS WIDGETS -->
<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
<!-- EASY PIE CHARTS -->
<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<!-- SPARKLINES -->
<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- browser msie issue fix -->
<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
<!-- Demo purpose only -->
<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        pageSetUp();
        $("#ins_partidas").change(function () {
            $("#ins_partidas option:selected").each(function () {
                elegido=$(this).val();
                $.post("<?php echo base_url(); ?>index.php/prog/combo_partidas", { elegido: elegido }, function(data){
                    $("#ins_partidas_dependientes").html(data);
                });
            });
        });
    })
</script>
</body>
</html>