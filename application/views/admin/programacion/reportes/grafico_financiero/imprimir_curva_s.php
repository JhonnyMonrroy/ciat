<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <link rel="STYLESHEET" href="<?php echo base_url(); ?>assets/print_static.css" type="text/css" />
    <style>
  @page {
    margin: 40px 40px;
    }
body{
        
        font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 4px 0;
        }
        header h2{
            margin: 0 0 9px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 8px;
                color: #ffffff;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 8px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .tabla {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 7px;
        width: 100%;

        }
        .tabla th {
        padding: 2px;
        font-size: 9px;
        background-color: #525659;
        background-repeat: repeat-x;
        color: #FFFFFF;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-right-style: solid;
        border-bottom-style: solid;
        border-right-color: #558FA6;
        border-bottom-color: #558FA6;
        font-family: "Trebuchet MS", Arial;
        text-transform: uppercase;
        }
        .tabla .modo1 {
        font-size: 8px;
        font-weight:bold;
       
        background-image: url(fondo_tr01.png);
        background-repeat: repeat-x;
        color: #34484E;
        font-family: "Trebuchet MS", Arial;
        }
        .tabla .modo1 td {
        padding: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-right-style: solid;
        border-bottom-style: solid;
        border-right-color: #A4C4D0;
        border-bottom-color: #A4C4D0;
        }
  </style>
    <script type="text/javascript">
      function imprimir() {
          if (window.print) {
              window.print();
          } else {
              alert("La función de impresion no esta soportada por su navegador.");
          }
      }
  </script>
</head>
<body>

<body onload="imprimir();">

<div id="section_header">
</div>
    <div class="page" style="font-size: 7pt" align="center">
      <table style="width: 90%;">
          <tr>
              <td width=20%;>
                <img src="<?php echo base_url(); ?>assets/img/logo.jpg" width="180px"/>
              </td>
              <td width=60%;>
                  <b>ENTIDAD : </b><?php echo $this->session->userdata('entidad');?><br>
                  <b>POA - PLAN OPERATIVO ANUAL : </b><?php echo $this->session->userdata("gestion")?><br>
                  <b><?php echo $this->session->userdata('sistema');?></b><br>
                  <b>GR&Aacute;FICO - CURVA S : </b> AVANCE PRESUPUESTARIO ANUAL DE LA OPERACI&Oacute;N <br>
              </td>
              <td width=20%; align="center">
                <img src="<?php echo base_url(); ?>assets/img/escudo.jpg" class="img-responsive" alt="Cinque Terre" alt="" width="80px" >
              </td>
          </tr>
      </table>
      <?php echo $dato_proyecto;?>

      <table class="change_order_items" style="width: 90%; font-size: 7pt;">
          <tbody>
            <tr>
              <td style="width: 50%;"><div id="graf" style="width: 450px; height: 320px; margin: 1 auto"></div></td>
              <td style="width: 50%;"><div id="graf_eficacia" style="width: 450px; height: 320px; margin: 1 auto"></div></td>
            </tr>
          </tbody>
        </table>
        <?php 
        echo $tabla_insumo;
        ?>
        <br><br><br><br><br>
        <div align="center">
          <table border="0" cellpadding="0" cellspacing="0" class="tabla">
              <tr class="modo1">    
                <td colspan="3" height="40"><b>FIRMAS</b></td>
              </tr>
              <tr class="modo1" align="center">    
                <td style="width: 33.3%;"><br><br><br><br>RESPONSABLE UNIDAD EJECUTORA</td>
                <td style="width: 33.3%;"><br><br><br><br>ANALISTA POA</td>
                <td style="width: 33.3%;"><br><br><br><br>ANALISTA FINANCIERO</td>
              </tr>
          </table>
        </div>
    <!--================================================== -->
    <script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
    <script type="text/javascript">
      $(function () {
          Highcharts.chart('graf_eficacia', {
              chart: {
                  type: 'column'
              },
              title: {
                  text: 'EFICACIA'
              },
              xAxis: {
                  categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'PORCENTAJES (%)'
                  },
                  stackLabels: {
                      enabled: true,
                      style: {
                          fontWeight: 'bold',
                          color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                      }
                  }
              },
              legend: {
                  align: 'right',
                  x: -30,
                  verticalAlign: 'top',
                  y: 25,
                  floating: true,
                  backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                  borderColor: '#CCC',
                  borderWidth: 1,
                  shadow: false
              },
              tooltip: {
                  headerFormat: '<b>{point.x}</b><br/>',
                  pointFormat: '{series.name}: <br/> TOTAL:   {point.y}'
              },
              plotOptions: {
                  column: {
                      stacking: 'normal',
                      dataLabels: {
                          enabled: false,
                          color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                      }
                  }
              },
              series: [{

                  name: '<b style="color: #FF0000;">MENOR A 75%</b>',
                  data: [
                  <?php 
                    for ($i=1; $i <=12 ; $i++) 
                    {
                      if($i==12){
                        ?>
                        {y: <?php echo $total[9][$i]?>, color: 'red'}
                        <?php
                      }
                      else{
                        ?>
                        {y: <?php echo $total[9][$i]?>, color: 'red'},
                        <?php
                      }
                    }
                  ?>
                 ] 
              }, {
                  name: '<b style="color: #d6d21f;">ENTRE 76% Y 90%</b>',
                  data: [ 
                    <?php 
                      for ($i=1; $i <=12 ; $i++) 
                      {
                        if($i==12){
                          ?>
                          {y: <?php echo $total[10][$i]?>, color: 'yellow'}
                          <?php
                        }
                        else{
                          ?>
                          {y: <?php echo $total[10][$i]?>, color: 'yellow'},
                          <?php
                        }
                      }
                    ?>] 
              }, {
                  name: '<b style="color: green;">MAYOR A 91%</b>',
                  data: [ 
                    <?php 
                      for ($i=1; $i <=12 ; $i++) 
                      {
                        if($i==12){
                          ?>
                          {y: <?php echo $total[11][$i]?>, color: 'green'}
                          <?php
                        }
                        else{
                          ?>
                          {y: <?php echo $total[11][$i]?>, color: 'green'},
                          <?php
                        }
                      }
                    ?>]
              }]
          });
      });
    </script>
    <script type="text/javascript">
    var chart1 = new Highcharts.Chart({
        chart: {
            renderTo: 'graf', // div contenedor
            type: 'line' // tipo de grafico
        },

        title: {
            text: 'PROGRAMACI\u00D3N Y EJECUCI\u00D3N PRESUPUESTARIA DE LA OPERACI\u00D3N', // t�tulo  del gr�fico
            x: -20 //center
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
        },
        yAxis: {
            title: {
                text: 'PORCENTAJES (%)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [
            {
                name: 'PROGRAMACIÓN ACUMULADA EN %',
                data: [
                <?php 
                  for ($i=1; $i <=12 ; $i++) 
                  {
                    if($i==12){
                      ?>
                      <?php echo $total[4][$i];?>
                      <?php
                    }
                    else{
                      ?>
                      <?php echo $total[4][$i];?>,
                      <?php
                    }
                  }
                ?>
                ]
            },
            {
                name: 'EJECUCIÓN ACUMULADA EN %',
                data: [
                <?php 
                  for ($i=1; $i <=12 ; $i++) 
                  {
                    if($i==12){
                      ?>
                      <?php echo $total[7][$i];?>
                      <?php
                    }
                    else{
                      ?>
                      <?php echo $total[7][$i];?>,
                      <?php
                    }
                  }
                ?>
                ]
            }
        ]
    });
</script>

    </body>
    </html>

