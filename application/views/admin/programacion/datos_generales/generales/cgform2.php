<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

        <title><?php echo $this->session->userdata('name')?></title>
        <meta name="description" content="">
        <meta name="author" content="">
            
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Basic Styles -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
        <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <!--///////////////css-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
        <!--//////////////fin css-->
        <style type="text/css">
            aside{background: #05678B;}
        </style>
    </head>
    <body class="">
        <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

        <!-- HEADER -->
        <header id="header">
            <div id="logo-group">
                <!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
            </div>

            <!-- pulled right: nav area -->
            <div class="pull-right">
                <!-- collapse menu button -->
                <div id="hide-menu" class="btn-header pull-right">
                    <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
                </div>
                <!-- end collapse menu -->
                <!-- logout button -->
                <div id="logout" class="btn-header transparent pull-right">
                    <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
                </div>
                <!-- end logout button -->
                <!-- search mobile button (this is hidden till mobile view port) -->
                <div id="search-mobile" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
                </div>
                <!-- end search mobile button -->
                <!-- fullscreen button -->
                <div id="fullscreen" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
                </div>
                <!-- end fullscreen button -->
            </div>
            <!-- end pulled right: nav area -->
        </header>
        <!-- END HEADER -->

        <!-- Left panel : Navigation area -->
        <aside id="left-panel">
            <!-- User info -->
            <div class="login-info">
                <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
                    <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
                    </a> 
                </span>
            </div>

            <nav>
                <ul>
                    <li>
                        <a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
                    </li>
                    <li class="text-center">
                        <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> PROGRAMACI&Oacute;N FISICA"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
                    </li>
                    <?php
                    if($nro_fase==1){
                        for($i=0;$i<count($enlaces);$i++)
                        {
                            ?>
                             <li>
                                <a href="#" >
                                    <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                                <ul >
                                <?php
                                $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                                foreach($submenu as $row) {
                                ?>
                                <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                                <?php } ?>
                                </ul>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </nav>
            <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
        </aside>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <span class="ribbon-button-alignment"> 
                    <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                        <i class="fa fa-refresh"></i>
                    </span> 
                </span>

                <!-- breadcrumb -->
                <ol class="breadcrumb">
                <?php 
                if($mod==1)
                {
                    ?>
                    <li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/prog/'.$mod.'/'.$proyecto[0]['proy_id'].'' ?>" title="DATOS GENERALES DEL PROYECTO">Datos Generales</a></li><li>Responsables</li>
                    <?php
                }
                elseif ($mod==4) 
                {
                    ?>
                    <li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/prog/'.$mod.'/'.$proyecto[0]['proy_id'].'' ?>" title="DATOS GENERALES DEL PROYECTO">Datos Generales</a></li><li>Responsables</li>    
                    <?php
                }
                ?>
                </ol>
            </div>
            <!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
				<div class="row">
					<nav role="navigation" class="navbar navbar-default navbar-inverse">
				        <div class="navbar-header">
				            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
				                <span class="sr-only">Toggle navigation</span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </button>
				        </div>
				 
				         <div id="navbarCollapse" class="collapse navbar-collapse">
				            <ul class="nav navbar-nav">
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/1' ?>"><font size="2">&nbsp;DATOS GENERALES&nbsp;</font></a></li>
				                <?php
                                    if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
                                    {
                                        ?>
                                    <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/10' ?>"><font size="2">&nbsp;METAS&nbsp;</font></a></li>
                                        <?php
                                    }
                                ?>
				                <li class="active"><a href="#"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;RESPONSABLES&nbsp;</font></a></li>
                                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/3' ?>"><font size="2">&nbsp;CLASIFICACI&Oacute;N&nbsp;</font></a></li>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/4?qRegistro='.$proyecto[0]['proy_id'].'' ?>"><font size="2">&nbsp;LOCALIZACI&Oacute;N&nbsp;</font></a></li>
				                <?php
                                    if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
                                    {
                                        ?>
                                    <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/5' ?>"><font size="2">&nbsp;OBJETIVOS&nbsp;</font></a></li>
                                        <?php
                                    }
                                ?>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/7' ?>"><font size="2">&nbsp;RESUMEN T&Eacute;CNICO&nbsp;</font></a></li>
				                
				            	<?php
                                    if($proyecto[0]['tp_id']==1){
                                      ?>
                                      <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="FASE ETAPA COMPONENTE"><font size="2">&nbsp;FASE / ETAPA&nbsp;</font></a></li>
                                      <?php
                                    }
                                    elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4) {
                                      if($mod==1)
                                      {
                                          ?>
                                          <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
                                          <?php
                                      }
                                      elseif($mod==4)
                                      {
                                          ?>
                                          <li><a href="<?php echo base_url().'index.php/admin/sgp/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
                                          <?php
                                      }
                                    }
                                  ?>
				            </ul>
				        </div>
					</nav>
				</div>

					<?php
						$attributes = array('class' => 'form-horizontal', 'id' => 'formulario','name' =>'formulario','enctype' => 'multipart/form-data');
						echo validation_errors();
						echo form_open('admin/proy/update_prog', $attributes);
					?>
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<!-- row -->
					<div class="row">
							<!-- end widget -->
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="jarviswidget jarviswidget-color-darken" >
											<header>
												<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
												<h2 class="font-md"><strong>RESPONSABLES</strong></h2>				
											</header>		
									
								                <form id="wizard-1" novalidate="novalidate" method="post">
													<div class="widget-body">
													<div class="well">
															<div class="row">
																<input class="form-control" type="hidden" name="id" value="<?php echo $proyecto[0]['proy_id'] ?>">
																<input class="form-control" type="hidden" name="mod" id="mod" value="<?php echo $mod?>">	
																	<div class="col-sm-4">
																		<div class="form-group">
																			<label><b><font size="1">T&Eacute;CNICO OPERATIVO (U.E.)</font></b></label> 
                                                                            <?php
                                                                                   if($this->session->userdata('rol_id')==1)
                                                                                    {
                                                                                        ?>
                                                                                        <select class="form-control" name="fun_id_1" id="fun_id_1">
                                                                                        <?php
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        ?>
                                                                                        <select class="form-control" name="fun_id_1" id="fun_id_1" disabled="true">
                                                                                        <?php
                                                                                    }

																                    foreach($fun1 as $row)
																                    {
																                    	if($row['fun_id']==$resp1[0]['fun_id'])
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['fun_id']?>" selected <?php if(@$_POST['pais']==$row['uni_id']){ echo "selected";} ?> ><?php echo $row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno']; ?></option>
																		                    <?php 
																                    	}
																                    	else
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['fun_id']?>" <?php if(@$_POST['pais']==$row['uni_id']){ echo "selected";} ?> ><?php echo $row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno']; ?></option>
																		                    <?php 
																                    	}
																                    }
																                ?>      
								                                                 </select>
																		</div>
																	</div>

																	<div class="col-sm-4">
																		<div class="form-group">
																			<label><b><font size="1">VALIDADOR POA</font></b></label>
								                                                    <?php 
																                    if($this->session->userdata('rol_id')==1)
                                                                                    {
                                                                                        ?>
                                                                                        <select class="form-control" name="fun_id_2" id="fun_id_2">
                                                                                        <?php
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        ?>
                                                                                        <select class="form-control" name="fun_id_2" id="fun_id_2" disabled="true">
                                                                                        <?php
                                                                                    } 
                                                                                    foreach($fun2 as $row)
																                    {
																                    	if($row['fun_id']==$resp2[0]['fun_id'])
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['fun_id']?>" selected <?php if(@$_POST['pais']==$row['uni_id']){ echo "selected";} ?> ><?php echo $row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno']; ?></option>
																		                    <?php 
																                    	}
																                    	else
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['fun_id']?>" <?php if(@$_POST['pais']==$row['uni_id']){ echo "selected";} ?> ><?php echo $row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno']; ?></option>
																		                    <?php 
																                    	}	
																                    }
																                	?>      
								                                                </select>
																		</div>
																	</div>

																	<div class="col-sm-4">
																		<div class="form-group">
																			<label><b><font size="1">VALIDADOR FINANCIERO</font></b></label>
								                                                    <?php
                                                                                    if($this->session->userdata('rol_id')==1)
                                                                                    {
                                                                                        ?>
                                                                                        <select class="form-control" name="fun_id_3" id="fun_id_3">
                                                                                        <?php
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        ?>
                                                                                        <select class="form-control" name="fun_id_3" id="fun_id_3" disabled="true">
                                                                                        <?php
                                                                                    }  
																                    foreach($fun3 as $row)
																                    {
																                    	if($row['fun_id']==$resp2[0]['fun_id'])
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['fun_id']?>" selected <?php if(@$_POST['pais']==$row['uni_id']){ echo "selected";} ?> ><?php echo $row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno']; ?></option>
																		                    <?php 
																                    	}
																                    	else
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['fun_id']?>" <?php if(@$_POST['pais']==$row['uni_id']){ echo "selected";} ?> ><?php echo $row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno']; ?></option>
																		                    <?php 
																                    	}	
																                    }
																                	?>      
								                                                </select>
																		</div>
																	</div>
																</div>
															</div><br>
																												

															
															<div class="well">
																<div class="row">
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label><b><font size="1">UNIDAD EJECUTORA</font></b></label>
                                                                                <?php
                                                                                    if($this->session->userdata('rol_id')==1)
                                                                                    {
                                                                                        ?>
                                                                                        <select class="form-control" name="uni_ejec" id="uni_ejec">
                                                                                        <?php
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        ?>
                                                                                        <select class="form-control" name="uni_ejec" id="uni_ejec" disabled="true">
                                                                                        <?php
                                                                                    } 
																                    foreach($unidad as $row)
																                    {
																                    	if($row['uni_id']==$resp1[0]['uni_ejec'])
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['uni_id']?>" selected <?php if(@$_POST['pais']==$row['uni_id']){ echo "selected";} ?> ><?php echo $row['uni_unidad']; ?></option>
																		                    <?php 
																                    	}
																                    	else
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['uni_id']?>" <?php if(@$_POST['pais']==$row['uni_id']){ echo "selected";} ?> ><?php echo $row['uni_unidad']; ?></option>
																		                    <?php 
																                    	}
																                    }
																                	?>      
								                                                </select>
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label><b><font size="1">UNIDAD RESPONSABLE</font></b></label>    
								                                                    <?php
                                                                                    if($this->session->userdata('rol_id')==1)
                                                                                    {
                                                                                        ?>
                                                                                        <select class="form-control" name="uni_ejec2" id="uni_ejec2" required>
                                                                                        <?php
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        ?>
                                                                                        <select class="form-control" name="uni_ejec2" id="uni_ejec2" disabled="true">
                                                                                        <?php
                                                                                    }  
																                    foreach($unidad as $row)
																                    {
																                    	if($row['uni_id']==$resp1[0]['uni_resp'])
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['uni_id']?>" selected <?php if(@$_POST['pais']==$row['uni_id']){ echo "selected";} ?> ><?php echo $row['uni_unidad']; ?></option>
																		                    <?php 
																                    	}
																                    	else
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['uni_id']?>" <?php if(@$_POST['pais']==$row['uni_id']){ echo "selected";} ?> ><?php echo $row['uni_unidad']; ?></option>
																		                    <?php 
																                    	}	
																                    }
																                	?>      
								                                                </select>
																		</div>
																	</div>
																</div>
															</div>
												</div>
														<div class="form-actions">
															<?php  
															if($mod==1)
															{
																?>
																<a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS PROYECTOS"> CANCELAR </a>
																<?php
															}
															elseif($mod==4)
															{
																?>
																<a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS PROYECTOS"> CANCELAR </a>
																<?php
															}
                                                            if($this->session->userdata('rol_id')==1)
                                                            {
                                                                ?>
                                                                <button type="submit" name="form" value="2" class="btn btn-primary btn-lg" title="GUARDAR Y REGISTRAR LOCALIZACION">SIGUIENTE</button>
                                                                <?php
                                                            }
															?>
															</div>
														</form>
													</div>
							</article>
					</div>
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
        <script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
        <script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
		      <script type="text/javascript">
        $(document).ready(function() {
            pageSetUp();

             $("#fun_id_1").change(function () {
                $("#fun_id_1 option:selected").each(function () {
                    elegido=$(this).val();
                    $.post("<?php echo base_url(); ?>index.php/admin/combo_fun_uni", { elegido: elegido,accion:'unidad2' }, function(data){
                        $("#uni_ejec2").html(data);
                    });     
                });
            }); 

               var $validator = $("#wizard-1").validate({
                //////////////// DATOS GENERALES
                rules: {
                  fun_id_1: {
                    required: true, ///// Tecnico operativo
                  },
                  fun_id_2: {
                    required: true, ///// validador poa
                  },
                  fun_id_3: {
                    required: true,////// validador financiero
                  },
                  uni_ejec: {
                    required: true, ///// unidad ejecutora
                  },
                  uni_ejec2: {
                    required: true, ///// unidad ejecutora
                  }

                },
                
                messages: {
                  fun_id_1: "Seleccione Tecnico operativo de planificacion",
                  fun_id_2: "Seleccione Tecnico validador POA",
                  fun_id_3: "Seleccione Tecnico validador Financiero",
                  uni_ejec: "Seleccione Unidad Ejecutora",
                  uni_ejec2: "Seleccione Unidad Responsable",

                },
                
                highlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                  if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                  } else {
                    error.insertAfter(element);
                  }
                }
              });
              
              $('#bootstrap-wizard-1').bootstrapWizard({
                'tabClass': 'form-wizard',
                'onNext': function (tab, navigation, index) {
                  var $valid = $("#wizard-1").valid();
                  if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                  } else {
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                      'complete');
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                    .html('<i class="fa fa-check"></i>');
                  }
                }
              });
              
        
            // fuelux wizard
              var wizard = $('.wizard').wizard();
              
              wizard.on('finished', function (e, data) {
                //$("#fuelux-wizard").submit();
                //console.log("submitted!");
                $.smallBox({
                  title: "Congratulations! Your form was submitted",
                  content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                  color: "#5F895F",
                  iconSmall: "fa fa-check bounce animated",
                  timeout: 4000
                });
                
              });
        })
        </script>
	</body>
</html>
