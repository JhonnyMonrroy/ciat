
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

        <title><?php echo $this->session->userdata('name')?></title>
        <meta name="description" content="">
        <meta name="author" content="">
            
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Basic Styles -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
        <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <!--///////////////css-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
        <!--//////////////fin css-->
        <style type="text/css">
            aside{background: #05678B;}
        </style>
    </head>
    <body class="">
        <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

        <!-- HEADER -->
        <header id="header">
            <div id="logo-group">
                <!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
            </div>

            <!-- pulled right: nav area -->
            <div class="pull-right">
                <!-- collapse menu button -->
                <div id="hide-menu" class="btn-header pull-right">
                    <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
                </div>
                <!-- end collapse menu -->
                <!-- logout button -->
                <div id="logout" class="btn-header transparent pull-right">
                    <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
                </div>
                <!-- end logout button -->
                <!-- search mobile button (this is hidden till mobile view port) -->
                <div id="search-mobile" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
                </div>
                <!-- end search mobile button -->
                <!-- fullscreen button -->
                <div id="fullscreen" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
                </div>
                <!-- end fullscreen button -->
            </div>
            <!-- end pulled right: nav area -->
        </header>
        <!-- END HEADER -->

        <!-- Left panel : Navigation area -->
        <aside id="left-panel">
            <!-- User info -->
            <div class="login-info">
                <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
                    <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
                    </a> 
                </span>
            </div>

            <nav>
                <ul>
                    <li>
                        <a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
                    </li>
                    <li class="text-center">
                        <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> PROGRAMACI&Oacute;N FISICA"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
                    </li>
                    <?php
                    if($nro_fase==1){
                        for($i=0;$i<count($enlaces);$i++)
                        {
                            ?>
                             <li>
                                <a href="#" >
                                    <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                                <ul >
                                <?php
                                $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                                foreach($submenu as $row) {
                                ?>
                                <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                                <?php } ?>
                                </ul>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </nav>
            <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
        </aside>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <span class="ribbon-button-alignment"> 
                    <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                        <i class="fa fa-refresh"></i>
                    </span> 
                </span>

                <!-- breadcrumb -->
                <ol class="breadcrumb">
                <?php 
                if($mod==1)
                {
                    ?>
                    <li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/prog/'.$mod.'/'.$proyecto[0]['proy_id'].'' ?>" title="DATOS GENERALES DEL PROYECTO">Datos Generales</a></li><li>Clasificaci&oacute;n</li>
                    <?php
                }
                elseif ($mod==4) 
                {
                    ?>
                    <li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/prog/'.$mod.'/'.$proyecto[0]['proy_id'].'' ?>" title="DATOS GENERALES DEL PROYECTO">Datos Generales</a></li><li>Clasificaci&oacute;n</li>    
                    <?php
                }
                ?>
                </ol>
            </div>
            <!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">

				<div class="row">
		          <nav role="navigation" class="navbar navbar-default navbar-inverse">
		                <div class="navbar-header">
		                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
		                        <span class="sr-only">Toggle navigation</span>
		                        <span class="icon-bar"></span>
		                        <span class="icon-bar"></span>
		                        <span class="icon-bar"></span>
		                    </button>
		                </div>
		         
		                <div id="navbarCollapse" class="collapse navbar-collapse">
		                    <ul class="nav navbar-nav">
		                        <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/1' ?>"><font size="2">&nbsp;DATOS GENERALES&nbsp;</font></a></li>
		                        <?php
                                    if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
                                    {
                                        ?>
                                    <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/10' ?>"><font size="2">&nbsp;METAS&nbsp;</font></a></li>
                                        <?php
                                    }
                                ?>
		                        <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/2' ?>"><font size="2">&nbsp;RESPONSABLES&nbsp;</font></a></li>
		                        <li class="active"><a href="#>"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;CLASIFICACI&Oacute;N&nbsp;</font></a></li>
		                        <li ><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/4?qRegistro='.$proyecto[0]['proy_id'].'' ?>"><font size="2">&nbsp;LOCALIZACI&Oacute;N&nbsp;</font></a></li>
		                        <?php
                                    if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
                                    {
                                        ?>
                                    <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/5' ?>"><font size="2">&nbsp;OBJETIVOS&nbsp;</font></a></li>
                                        <?php
                                    }
                                ?>
		                        <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/7' ?>"><font size="2">&nbsp;RESUMEN T&Eacute;CNICO&nbsp;</font></a></li>
		                        
			                    <?php
			                        if($proyecto[0]['tp_id']==1){
			                          ?>
			                          <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="FASE ETAPA COMPONENTE"><font size="2">&nbsp;FASE / ETAPA&nbsp;</font></a></li>
			                          <?php
			                        }
			                        elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4) {
			                          if($mod==1)
			                          {
				                          ?>
				                          <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
				                          <?php
			                          }
			                          elseif($mod==4)
			                          {
			                          	  ?>
				                          <li><a href="<?php echo base_url().'index.php/admin/sgp/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
				                          <?php
			                          }
			                        }
			                      ?>
		                    </ul>
		                </div>
		          </nav>
		        </div>
										<?php
						                  $attributes = array('class' => 'form-horizontal', 'id' => 'wizard-1','name' =>'wizard-1','enctype' => 'multipart/form-data');
						                  echo validation_errors();
						                  echo form_open('admin/proy/update_prog', $attributes);
						                ?>
				<section id="widget-grid" class="">
					<!-- row -->
					<div class="row">
							<!-- Widget ID (each widget will need unique ID)-->
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<form id="wizard-1" novalidate="novalidate" method="post">
									<input class="form-control" type="hidden" name="id" value="<?php echo $proyecto[0]['proy_id'] ?>">
									<input class="form-control" type="hidden" name="mod" value="<?php echo $mod ?>">
													<div class="jarviswidget jarviswidget-color-darken" >
														<header>
															<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
															<h2 class="font-md"><strong>CLASIFICACI&Oacute;N</strong></h2>				
														</header>
														<div>
															<!-- widget content -->
															<div class="widget-body">
																<label><b>PDES</b></label>
														<div class="well">
															<div class="row">
																<div class="col-sm-3">
																	<div class="form-group">
																		<label><font size="1"><b>PILAR</b></font></label>
																		<select class="form-control" id="pedes1" name="pedes1">
																			<?php 
																			if($proyecto[0]['pdes_id']!=0)
																			{
																				?>
																				<option value="<?php echo $pdes[0]['id1'] ?>"><?php echo $pdes[0]['id1']?> - Pilar - <?php echo $pdes[0]['pilar'] ?></option> 
					                                                         	<option value="">--------------------------------------------</option> 
																				<?php
																			}
																			else
																			{
																				?>
																				<option value="">No seleccionado </option> 
																				<?php
																			}
																			
																				$consulta1 = 'SELECT * FROM "public"."pdes" WHERE pdes_jerarquia=\'1\' AND pdes_gestion='.$this->session->userdata("gestion").' ORDER BY pdes_id ';
																				  $consulta1=$this->db->query($consulta1);
																				  $lista_pedes=$consulta1->result_array();
																				  foreach ($lista_pedes as $pedes)
																				  { ?>
																				  <option value="<?php echo $pedes['pdes_codigo']?>" <?php if(@$_POST['pais']==$pedes['pdes_id']){ echo "selected";} ?> >
																				  <?php echo $pedes['pdes_nivel'].' - '.$pedes['pdes_descripcion']?></option> 
  																			<?php } ?>     
					                                                  	</select>

																	</div>
																</div>
															
																<div class="col-sm-3">
																	<div class="form-group">
																		<label><font size="1"><b>META</b></font></label>
																		<select class="form-control" id="pedes2" name="pedes2">
																		<?php 
																			if($proyecto[0]['pdes_id']!=0)
																			{
																				?>
																				<option value="<?php echo $pdes[0]['id2'] ?>"><?php echo $pdes[0]['id2']?> - Meta - <?php echo $pdes[0]['meta'] ?></option>  
																				<?php
																			}
																			else
																			{
																				?>
																				<option value="">No seleccionado </option> 
																				<?php
																			}
																			?>
																			 
					                                                  	</select>
																	</div>
																</div>
															
																<div class="col-sm-3">
																	<div class="form-group">
																		<label><font size="1"><b>RESULTADO</b></font></label>
																		<select class="form-control" id="pedes3" name="pedes3">
																		<?php 
																			if($proyecto[0]['pdes_id']!=0)
																			{
																				?>
																				<option value="<?php echo $pdes[0]['id3'] ?>"><?php echo $pdes[0]['id3']?> - Resultado - <?php echo $pdes[0]['resultado'] ?></option>    
																				<?php
																			}
																			else
																			{
																				?>
																				<option value="">No seleccionado </option> 
																				<?php
																			}
																			?>
																		
					                                                  	</select>
																	</div>
																</div>

																<div class="col-sm-3">
																	<div class="form-group">
																		<label><font size="1"><b>ACCI&Oacute;N</b></font></label>
																		<select class="form-control" id="pedes4" name="pedes4">
																		 <?php 
																			if($proyecto[0]['pdes_id']!=0)
																			{
																				?>
																				<option value="<?php echo $pdes[0]['id4'] ?>"><?php echo $pdes[0]['id4']?> - Accion - <?php echo $pdes[0]['accion'] ?></option>   
																				<?php
																			}
																			else
																			{
																				?>
																				<option value="">No seleccionado </option> 
																				<?php
																			}
																			?>  
																		
					                                                  	</select>
																	</div>
																</div>
															</div>
														</div><br>
														<label><b>PTDI</b></label>
														<div class="well">
															<div class="row">
																<div class="col-sm-4">
																	<div class="form-group">
																		<label><font size="1"><b>COMPONENTE ESTRAT&Eacute;GICO </b></font></label>
																		<select class="select2" id="ptdi1" name="ptdi1">
																			<?php 
																			foreach ($lista_ptdi as $row) 
																			{
																				if($row['ptdi_codigo']==$ptdi[0]['id1'])
																				{
																					?>
																		            <option value="<?php echo $row['ptdi_codigo'] ?>"<?php if(@$_POST['pais']==$row['ptdi_codigo']){ echo "selected";} ?> selected><?php echo $row['ptdi_codigo'].' - '.$row['ptdi_nivel'].' - '.$row['ptdi_descripcion']?></option>  
																		            <?php
																				}
																				else
																				{
																					?>
																		            <option value="<?php echo $row['ptdi_codigo'] ?>" <?php if(@$_POST['pais']==$row['ptdi_codigo']) { echo "selected";}?>><?php echo $row['ptdi_codigo'].' - '.$row['ptdi_nivel'].' - '.$row['ptdi_descripcion']?></option>  
																		            <?php
																				}
																	            
																	        }
  																			?>    
					                                                  	</select>

																	</div>
																</div>
															
																<div class="col-sm-4">
																	<div class="form-group">
																		<label><font size="1"><b>POLITICA</b></font></label>
																		<select class="form-control" id="ptdi2" name="ptdi2">
																		<?php 
																			if($proyecto[0]['ptdi_id']!=0)
																			{
																				?>
																				<option value="<?php echo $ptdi[0]['id2'] ?>"><?php echo $ptdi[0]['id2']?> - Politica - <?php echo $ptdi[0]['meta'] ?></option>  
																				<?php
																			}
																			else
																			{
																				?>
																				<option value="">No seleccionado </option> 
																				<?php
																			}
																		?>
					                                                  	</select>
																	</div>
																</div>
															
																<div class="col-sm-4">
																	<div class="form-group">
																		<label><font size="1"><b>&Aacute;REA DE INTERVENCI&Oacute;N </b></font></label>
																		<select class="form-control" id="ptdi3" name="ptdi3">
																		<?php 
																			if($proyecto[0]['ptdi_id']!=0)
																			{
																				?>
																				<option value="<?php echo $ptdi[0]['id3'] ?>"><?php echo $ptdi[0]['id3']?> - Programa - <?php echo $ptdi[0]['resultado'] ?></option>    
																				<?php
																			}
																			else
																			{
																				?>
																				<option value="">No seleccionado </option> 
																				<?php
																			}
																			?>
																		
					                                                  	</select>
																	</div>
																</div>
															</div>
														</div><br>
														<label><b>CLASIFICADOR SECTORIAL</b></label>
														<div class="well">
															<div class="row">
																<div class="col-sm-4">
																	<div class="form-group">
																		<label><font size="1"><b>SECTOR</b></font></label>
																		<select name="clasificador1" id="clasificador1" class="select2" >
																		<?php 
																			if($proyecto[0]['codsectorial']!='00-0-00')
																			{
																				?>
																				<option value="<?php echo $codsec[0]['cod1'] ?>"><?php echo $codsec[0]['cod1'].'-'.$codsec[0]['sector'].'-'.$codsec[0]['desc1'] ?></option>	
																				<?php
																			}
																		
																			?>
																			<option value="">Seleccione Sector</option>	
																			<?php
																				$consulta1 = 'SELECT * FROM "public"."_clasificadorsectorial" WHERE nivel=\'1\' ';
																				  $consulta1=$this->db->query($consulta1);
																				  $cod_sectorial=$consulta1->result_array();
																				  foreach ($cod_sectorial as $pedes)
																				  { 
																				  	?>
																						<option value="<?php echo $pedes['codsec']?>" <?php if(@$_POST['pais']==$pedes['codsec']){ echo "selected";} ?>>
																						<?php echo $pedes['codsectorial']." - ".$pedes['codsectorialduf']." - " .$pedes['sector']?></option>
		  																			<?php
																				  	
																				  }
																			?>
																		</select>
																	</div>
																</div>

																<div class="col-sm-4">
																	<div class="form-group">
																		<label><font size="1"><b>SUB SECTOR</b></font></label>
																		<select name="clasificador2" id="clasificador2" class="form-control">
																		<?php 
																			if($proyecto[0]['codsectorial']!='00-0-00')
																			{
																				?>
																				<option value="<?php echo $codsec[0]['cod2'] ?>"><?php echo $codsec[0]['cod2'].'-'.$codsec[0]['subsector'].'-'.$codsec[0]['desc2'] ?></option>
																				<?php
																			}
																		?>
																		
																		</select>
																	</div>
																</div>

																<div class="col-sm-4">
																	<div class="form-group">
																		<label><font size="1"><b>ACTIVIDAD</b></font></label>
																		<select class="form-control" id="clasificador3" name="clasificador3">   
					                                                  	<?php 
																			if($proyecto[0]['codsectorial']!='00-0-00')
																			{
																				?>
																				<option value="<?php echo $codsec[0]['cod3'] ?>"><?php echo $codsec[0]['cod3'].'-'.$codsec[0]['actividad'].'-'.$codsec[0]['desc3'] ?></option>
																				<?php
																			}
																		?>
					                                                  	
					                                                  	</select>
																	</div>
																</div>

															</div>
														</div><br>
														<label><b>FINALIDAD FUNCI&Oacute;N</b></label>
														<div class="well">
															<div class="row">
																<div class="col-sm-4">
																	<div class="form-group">
																		<select class="select2" id="finalidad1" name="finalidad1">
					                                                        <?php 
																			if($proyecto[0]['fifu_id']!=0)
																			{
																				?>
																				<option value="<?php echo $fifu[0]['f1'] ?>"><?php echo $fifu[0]['f1'] ?> - <?php echo $fifu[0]['finalidad'] ?></option>
					                                                         	<option value="">--------------------------------------------</option> 
																				<?php
																			}
																			else
																			{
																				?>
																				<option value="">No seleccionado </option> 
																				<?php
																			}
																			
																				$consulta1 = 'SELECT * FROM "public"."_finalidadfuncion" WHERE fifu_depende=\'0\' ';
																				  $consulta1=$this->db->query($consulta1);
																				  $lista_finalidad=$consulta1->result_array();
																				  foreach ($lista_finalidad as $finalidad)
																				  { ?>
																				  <option value="<?php echo $finalidad['fifu_id']?>" <?php if(@$_POST['pais']==$finalidad['fifu_id']){ echo "selected";} ?> >
																				  <?php echo "".$finalidad['fifu_id']." - ".$finalidad['fifu_nivel']." - ".$finalidad['fifu_finalidad_funcion']?></option>
  																			<?php } ?> 

					                                                  	</select>
																	</div>
																</div>

																<div class="col-sm-4">
																	<div class="form-group">
																		<select class="form-control" id="fun" name="fun">   
					                                                  	<?php 
																			if($proyecto[0]['fifu_id']!=0)
																			{
																				?>
																				<option value="<?php echo $fifu[0]['f2'] ?>"><?php echo $fifu[0]['f2'] ?> - <?php echo $fifu[0]['funcion'] ?></option> 
																				<?php
																			}
																			else
																			{
																				?>
																				<option value="">No seleccionado </option> 
																				<?php
																			}
																			?>  

					                                                  	</select>
																	</div>
																</div>

																<div class="col-sm-4">
																	<div class="form-group">
																		<select class="form-control" id="clase" name="clase">
					                                                        <?php 
																			if($proyecto[0]['fifu_id']!=0)
																			{
																				?>
																				<option value="<?php echo $fifu[0]['f3'] ?>"><?php echo $fifu[0]['f3'] ?> - <?php echo $fifu[0]['accion'] ?></option>     
																				<?php
																			}
																			else
																			{
																				?>
																				<option value="">No seleccionado </option> 
																				<?php
																			}
																			?>  
					                                                        
					                                                  	</select>
																	</div>
																</div>

															</div>
														</div>
															</div>
															<!-- end widget content -->
														</div>
														<div class="form-actions">
																<?php  
									                              if($mod==1)
									                              {
									                                ?>
									                                <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS PROYECTOS"> CANCELAR </a>
									                                <?php
									                              }
									                              elseif($mod==4)
									                              {
									                                ?>
									                                <a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS PROYECTOS"> CANCELAR </a>
									                                <?php
									                              }
									                              ?>
																<button type="submit" name="form" value="3" class="btn btn-primary btn-lg" title="MODIFICAR Y GUARDAR CLASIFICACION">SIGUIENTE</button>
															</div>
															</div>
														</form>
													</div>

												</article>
							<!-- end widget -->
					</div>
				
					<!-- end row -->
				</section>
				
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->

				<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
				<script type="text/javascript">
		$(document).ready(function() {
			
			pageSetUp();
			$("#pedes1").change(function () {
				$("#pedes1 option:selected").each(function () {
					elegido=$(this).val();
					$.post("<?php echo base_url(); ?>index.php/admin/combo_clasificador", { elegido: elegido,accion:'pedes_2' }, function(data){
						$("#pedes2").html(data);
					});     
				});
			});
			$("#pedes2").change(function () {
				$("#pedes2 option:selected").each(function () {
					elegido=$(this).val();
					$.post("<?php echo base_url(); ?>index.php/admin/combo_clasificador", { elegido: elegido,accion:'pedes_3' }, function(data){
						$("#pedes3").html(data);
					});     
				});
			});  

			$("#pedes3").change(function () {
				$("#pedes3 option:selected").each(function () {
					elegido=$(this).val();
					$.post("<?php echo base_url(); ?>index.php/admin/combo_clasificador", { elegido: elegido,accion:'pedes_3' }, function(data){
						$("#pedes4").html(data);
					});     
				});
			}); 
			
			$("#ptdi1").change(function () {
				$("#ptdi1 option:selected").each(function () {
					elegido=$(this).val();
					$.post("<?php echo base_url(); ?>index.php/admin/combo_clasificador", { elegido: elegido,accion:'ptdi_2' }, function(data){
						$("#ptdi2").html(data);
					});     
				});
			}); 

			$("#ptdi2").change(function () {
				$("#ptdi2 option:selected").each(function () {
					elegido=$(this).val();
					$.post("<?php echo base_url(); ?>index.php/admin/combo_clasificador", { elegido: elegido,accion:'ptdi_3' }, function(data){
						$("#ptdi3").html(data);
					});     
				});
			}); 

			$("#clasificador1").change(function () {
				$("#clasificador1 option:selected").each(function () {
				elegido=$(this).val();
				$.post("<?php echo base_url(); ?>index.php/admin/combo_clasificador", { elegido: elegido,accion:'cl2' }, function(data){
				$("#clasificador2").html(data);
				});     
				});
			});

			$("#clasificador2").change(function () {
				$("#clasificador2 option:selected").each(function () {
				elegido=$(this).val();
				$.post("<?php echo base_url(); ?>index.php/admin/combo_clasificador", { elegido: elegido,accion:'cl3' }, function(data){
				$("#clasificador3").html(data);
				});     
				});
			});

			$("#finalidad1").change(function () {
				$("#finalidad1 option:selected").each(function () {
				elegido=$(this).val();
				$.post("<?php echo base_url(); ?>index.php/admin/combo_clasificador", { elegido: elegido,accion:'funcion' }, function(data){
				$("#fun").html(data);
				});     
				});
			});

			$("#fun").change(function () {
				$("#fun option:selected").each(function () {
				elegido=$(this).val();
				$.post("<?php echo base_url(); ?>index.php/admin/combo_clasificador", { elegido: elegido,accion:'clase_fn' }, function(data){
				$("#clase").html(data);
				});     
				});
			});

			//Bootstrap Wizard Validations

			   var $validator = $("#wizard-1").validate({
			    
			    rules: {
			      /////////////// CLASIFICADOR
			      pedes1: {
			        required: true, ///// Pilar 1
			      },
			      pedes2: {	
			        required: true, ////// Meta
			      },
			      pedes3: {	
			        required: true, ////// Resultado
			      },
			      pedes4: {	
			        required: true, ////// Accion
			      },
			      ptdi1: {
			        required: true, ///// Pilar 1
			      },
			      ptdi2: {	
			        required: true, ////// Meta
			      },
			      ptdi3: {	
			        required: true, ////// Resultado
			      },
			      clasificador1: {
			        required: true, ///// Clasificador 1
			      },
			      clasificador2: {	
			        required: true, ////// clasificador2
			      },
			      clasificador3: { 
			        required: true, /////// Clasificador 3
			      },
			      finalidad1: {
			        required: true, ///// Finalidad funcion 1
			      },
			      fun: {
			        required: true,////// Finalidad funcion 2
			      }
			    },
			    
			    messages: {
			      pedes1: "Seleccione Pilar",
			      pedes2: "Seleccione Meta",
			      pedes3: "Seleccione Resultado",
			      pedes4: "Seleccione Accion",

			      pedes1: "Seleccione Eje Programatica",
			      pedes2: "Seleccione Politica",
			      pedes3: "Seleccione Programa",

			      clasificador1: "Seleccione Sector",
			      clasificador2: "Seleccione Sub Sector",
			      clasificador3: "Seleccione Actividad Economica",

			      finalidad1: "Seleccione finalidad",
			      fun: "Seleccione funcion",
			    },
			    
			    highlight: function (element) {
			      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			    },
			    unhighlight: function (element) {
			      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			    },
			    errorElement: 'span',
			    errorClass: 'help-block',
			    errorPlacement: function (error, element) {
			      if (element.parent('.input-group').length) {
			        error.insertAfter(element.parent());
			      } else {
			        error.insertAfter(element);
			      }
			    }
			  });
			  
			  $('#bootstrap-wizard-1').bootstrapWizard({
			    'tabClass': 'form-wizard',
			    'onNext': function (tab, navigation, index) {
			      var $valid = $("#wizard-1").valid();
			      if (!$valid) {
			        $validator.focusInvalid();
			        return false;
			      } else {
			        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
			          'complete');
			        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
			        .html('<i class="fa fa-check"></i>');
			      }
			    }
			  });
			  
		
			// fuelux wizard
			  var wizard = $('.wizard').wizard();
			  
			  wizard.on('finished', function (e, data) {
			    //$("#fuelux-wizard").submit();
			    //console.log("submitted!");
			    $.smallBox({
			      title: "Congratulations! Your form was submitted",
			      content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
			      color: "#5F895F",
			      iconSmall: "fa fa-check bounce animated",
			      timeout: 4000
			    });
			    
			  });

		})
		</script>
	</body>
</html>
