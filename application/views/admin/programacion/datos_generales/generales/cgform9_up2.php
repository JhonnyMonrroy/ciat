<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--///////////////css-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<!--//////////////fin css-->
		<style type="text/css">
			aside{background: #05678B;}
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
					</a> 
				</span>
			</div>

			<nav>
				<ul>
					<li>
						<a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
					</li>
					<li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> PROGRAMACI&Oacute;N FISICA"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
		            </li>
					<?php
					if($nro_fase==1){
					    for($i=0;$i<count($enlaces);$i++)
					    {
					    	?>
					    	 <li>
				              	<a href="#" >
				              		<i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
				              	<ul >
				              	<?php
				                $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
					      		foreach($submenu as $row) {
				                ?>
				                <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
				                <?php } ?>
				                </ul>
				            </li>
					    	<?php
					    }
					}
					?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
				<?php 
				if($mod==1)
				{
					?>
					<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/prog/'.$mod.'/'.$proyecto[0]['proy_id'].'' ?>" title="DATOS FASE ETAPA">Datos Generales</a></li><li>Fase Etapa (Presupuesto Asignado)</li>
					<?php
				}
				elseif ($mod==4) 
				{
					?>
					<li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/prog//'.$mod.'/'.$proyecto[0]['proy_id'].'' ?>" title="DATOS FASE ETAPA">Datos Generales</a></li><li>Fase Etapa (Presupuesto Asignado)</li>	
					<?php
				}
				?>
					
				</ol>
			</div>
			<!-- END RIBBON -->
					<?php
						$attributes = array('class' => 'form-horizontal', 'id' => 'formulario','name' =>'formulario','enctype' => 'multipart/form-data');
						echo validation_errors();
						echo form_open('admin/proy/add_fe2', $attributes);
					?>
			<!-- MAIN CONTENT -->
			<div id="content">
				<div class="row">
					<nav role="navigation" class="navbar navbar-default navbar-inverse">
				        <div class="navbar-header">
				            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
				                <span class="sr-only">Toggle navigation</span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </button>
				        </div>
				 
				        <div id="navbarCollapse" class="collapse navbar-collapse">
				           <ul class="nav navbar-nav">
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/1' ?>"><font size="2">&nbsp;DATOS GENERALES&nbsp;</font></a></li>
				                <?php
		                          if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
		                          {
		                              ?>
		                          <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/10' ?>"><font size="2">&nbsp;METAS&nbsp;</font></a></li>
		                              <?php
		                          }
		                        ?>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/2' ?>"><font size="2">&nbsp;RESPONSABLES&nbsp;</font></a></li>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/3' ?>"><font size="2">&nbsp;CLASIFICACI&Oacute;N&nbsp;</font></a></li>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/4?qRegistro='.$proyecto[0]['proy_id'].'' ?>"><font size="2">&nbsp;LOCALIZACI&Oacute;N&nbsp;</font></a></li>
				                
				                <?php
                                    if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
                                    {
                                        ?>
                                    <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/5' ?>"><font size="2">&nbsp;OBJETIVOS&nbsp;</font></a></li>
                                        <?php
                                    }
                                ?>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/7' ?>"><font size="2">&nbsp;RESUMEN T&Eacute;CNICO&nbsp;</font></a></li>
				                <?php
			                        if($proyecto[0]['tp_id']==1){
			                          ?>
			                          <li><li class="active"><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="FASE ETAPA COMPONENTE"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;FASE / ETAPA&nbsp;</font></a></li>
			                          <?php
			                        }
			                        elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4) {
			                          if($mod==1)
			                          {
				                          ?>
				                          <li><li class="active"><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
				                          <?php
			                          }
			                          elseif($mod==4)
			                          {
			                          	  ?>
				                          <li><li class="active"><a href="<?php echo base_url().'index.php/admin/sgp/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
				                          <?php
			                          }
			                        }
			                      ?>
				            </ul>
				        </div>
					</nav>
				</div>	
				<div class="well">
					<u><b>PROYECTO</b></u> :		<?php echo $proyecto[0]['proy_nombre'] ?><br>
					<u><b>GESTI&Oacute;N ACTUAL</b></u> : <?php echo $this->session->userdata("gestion");?>
				</div>	
				<!-- widget grid -->
				<section id="widget-grid" class="">

					<div class="row">
							<!-- Widget ID (each widget will need unique ID)-->
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<h2 class="font-md"><strong>FASE ETAPA-PRESUPUESTO (Modificar) </strong></h2>				
									</header>
										<!-- widget content -->
									<div class="widget-body">
										<div class="row">
											<form id="formulario"  name="formulario" novalidate="novalidate" method="post">
												<input class="form-control" type="hidden" name="id_p" value="<?php echo $proyecto[0]['proy_id']?>">
												<input class="form-control" type="hidden" name="id_f" value="<?php echo $fase_proyecto[0]['id']?>">
												<input class="form-control" type="hidden" name="gest" id="gest" value="<?php echo $gestiones?>">
												<input class="form-control" type="hidden" name="gestion" id="gestion" value="<?php echo $fecha?>">
												<input class="form-control" type="hidden" name="ppt" id="ppt" value="<?php echo $fase_proyecto[0]['pfec_ptto_fase'] ?>">
												<input class="form-control" type="hidden" name="pe" id="pe" value="<?php echo $fase_proyecto[0]['pfec_ptto_fase_e'] ?>">	
												<input class="form-control" type="hidden" name="tp" id="tp" value="1">	
												<input class="form-control" type="hidden" name="mod" id="mod" value="<?php echo $mod; ?>">
												<input class="form-control" type="hidden" name="estado" id="estado" value="2"> <!-- estado MODIFICADO-->	
												<input class="form-control" type="hidden" name="montos" id="montos" value="0">
												<div id="" class="col-sm-12">
												<h2 class="alert alert-success"><center> ASIGNACI&Oacute;N DE PRESUPUESTO : Gesti&oacute;n <?php echo $fase_proyecto[0]['pfec_fecha_inicio'].' - '.$fase_proyecto[0]['pfec_fecha_fin'] ?></center></h2>
												<?php
												if($proyecto[0]['tp_id']!=1)
												{
													?>
													<div class="well">
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label><font size="1"><b>FASE/OBSERVACI&Oacute;N</b></font></label>
																	<textarea rows="3"  name="desc" id="desc" class="form-control" style="width:100%;"><?php echo $fase_proyecto[0]['descripcion']?></textarea> 
																</div>
															</div>
														</div>
													</div><br>
													<?php
												}
												?>
																<div class="well">
																	<div class="row">
																	<div class="col-sm-2">
																		<div class="form-group">
																			<label><font size="1"><b>TOTAL PRESUPUESTO </b></font></label>
																			<input class="form-control" type="text" value="<?php echo number_format($fase_proyecto[0]['pfec_ptto_fase']) ?>" disabled="true">	
																		</div>
																	</div>
																	<?php 
																			if($gestiones>=1)
																			{
																				?>
																				<div class="col-sm-2">
																					<div class="form-group">
																						<label><font size="1" color="blue"><b>GESTION <?php echo $fecha;?></b></font></label>
																						<input class="form-control" type="text" name="pt1" id="pt1" value="<?php echo round($fase_gestion1[0]['pfecg_ppto_total'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																					</div>
																				</div>

																				<?php
																				if($gestiones>=2)
																				{
																					?>
																						<div class="col-sm-2">
																							<div class="form-group">
																								<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+1;?></b></font></label>
																								<input class="form-control" type="text" name="pt2" id="pt2" value="<?php echo round($fase_gestion2[0]['pfecg_ppto_total'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
											                                                  	</select>
																							</div>
																						</div>
																					<?php
																					if($gestiones>=3)
																					{
																						?>
																							<div class="col-sm-2">
																								<div class="form-group">
																									<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+2;?></b></font></label>
																									<input class="form-control" type="text" name="pt3" id="pt3" value="<?php echo round($fase_gestion3[0]['pfecg_ppto_total'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																								</div>
																							</div>
																					
																					<?php
																						if($gestiones>=4)
																						{
																							?>
																								<div class="col-sm-2">
																									<div class="form-group">
																										<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+3;?></b></font></label>
																										<input class="form-control" type="text" name="pt4" id="pt4" value="<?php echo round($fase_gestion4[0]['pfecg_ppto_total'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																									</div>
																								</div>
																						
																						<?php
																							if($gestiones>=5)
																							{
																								?>
																									<div class="col-sm-2">
																										<div class="form-group">
																											<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+4;?></b></font></label>
																											<input class="form-control" type="text" name="pt5" id="pt5" value="<?php echo round($fase_gestion5[0]['pfecg_ppto_total'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																										</div>
																									</div>
																							
																							<?php
																								if($gestiones>=6)
																								{
																									?>
																										<div class="col-sm-2">
																											<div class="form-group">
																												<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+5;?></b></font></label>
																												<input class="form-control" type="text" name="pt6" id="pt6" value="<?php echo round($fase_gestion6[0]['pfecg_ppto_total'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																											</div>
																										</div>

																								
																								<?php
																									if($gestiones>=7)
																									{
																										?>
																											<div class="col-sm-2">
																												<div class="form-group">
																													<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+6;?></b></font></label>
																													<input class="form-control" type="text" name="pt7" id="pt7" value="<?php echo round($fase_gestion7[0]['pfecg_ppto_total'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																												</div>
																											</div>
																									
																									<?php
																										if($gestiones>=8)
																										{
																											?>
																												<div class="col-sm-2">
																													<div class="form-group">
																														<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+7;?></b></font></label>
																														<input class="form-control" type="text" name="pt8" id="pt8" value="<?php echo round($fase_gestion8[0]['pfecg_ppto_total'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																													</div>
																												</div>
																										
																										<?php
																											if($gestiones>=9)
																											{
																												?>
																													<div class="col-sm-2">
																														<div class="form-group">
																															<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+8;?></b></font></label>
																															<input class="form-control" type="text" name="pt9" id="pt9" value="<?php echo round($fase_gestion9[0]['pfecg_ppto_total'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																														</div>
																													</div>
																											
																											<?php
																												if($gestiones>=10)
																												{
																													?>
																														<div class="col-sm-2">
																															<div class="form-group">
																																<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+9;?></b></font></label>
																																<input class="form-control" type="text" name="pt10" id="pt10" value="<?php echo round($fase_gestion10[0]['pfecg_ppto_total'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																															</div>
																														</div>
																												
																												<?php
																												}
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}

																			}
																		?>
																	</div>
																</div><br>

															

																<div class="well">
																	<div class="row">
																	<div class="col-sm-2">
																		<div class="form-group">
																			<label><font size="1"><b>TOTAL EJECUTADO </b></font></label>
																			<input class="form-control" type="text" value="<?php echo number_format($fase_proyecto[0]['pfec_ptto_fase_e']) ?>" id="total" name="total" disabled="true">
																		</div>
																	</div>
																	<?php 
																			if($gestiones>=1)
																			{
																				?>
																				<div class="col-sm-2">
																					<div class="form-group">
																						<label><font size="1" color="blue"><b>GESTION <?php echo $fecha;?></b></font></label>
																						<input class="form-control" type="text" name="pte1" id="pte1" value="<?php echo round($fase_gestion1[0]['pfecg_ppto_ejecutado'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																					</div>
																				</div>

																				<?php
																				if($gestiones>=2)
																				{
																					?>
																						<div class="col-sm-2">
																							<div class="form-group">
																								<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+1;?></b></font></label>
																								<input class="form-control" type="text" name="pte2" id="pte2" value="<?php echo round($fase_gestion2[0]['pfecg_ppto_ejecutado'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																							</div>
																						</div>
																					<?php
																					if($gestiones>=3)
																					{
																						?>
																							<div class="col-sm-2">
																								<div class="form-group">
																									<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+2;?></b></font></label>
																									<input class="form-control" type="text" name="pte3" id="pte3" value="<?php echo round($fase_gestion3[0]['pfecg_ppto_ejecutado'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																								</div>
																							</div>
																					
																					<?php
																						if($gestiones>=4)
																						{
																							?>
																								<div class="col-sm-2">
																									<div class="form-group">
																										<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+3;?></b></font></label>
																										<input class="form-control" type="text" name="pte4" id="pte4" value="<?php echo round($fase_gestion4[0]['pfecg_ppto_ejecutado'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																									</div>
																								</div>
																						
																						<?php
																							if($gestiones>=5)
																							{
																								?>
																									<div class="col-sm-2">
																										<div class="form-group">
																											<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+4;?></b></font></label>
																											<input class="form-control" type="text" name="pte5" id="pte5" value="<?php echo round($fase_gestion5[0]['pfecg_ppto_ejecutado'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																										</div>
																									</div>
																							
																							<?php
																								if($gestiones>=6)
																								{
																									?>
																										<div class="col-sm-2">
																											<div class="form-group">
																												<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+5;?></b></font></label>
																												<input class="form-control" type="text" name="pte6" id="pte6" value="<?php echo round($fase_gestion6[0]['pfecg_ppto_ejecutado'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }"	>
																											</div>
																										</div>

																								
																								<?php
																									if($gestiones>=7)
																									{
																										?>
																											<div class="col-sm-2">
																												<div class="form-group">
																													<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+6;?></b></font></label>
																													<input class="form-control" type="text" name="pte7" id="pte7" value="<?php echo round($fase_gestion7[0]['pfecg_ppto_ejecutado'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																												</div>
																											</div>
																									
																									<?php
																										if($gestiones>=8)
																										{
																											?>
																												<div class="col-sm-2">
																													<div class="form-group">
																														<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+7;?></b></font></label>
																														<input class="form-control" type="text" name="pte8" id="pte8" value="<?php echo round($fase_gestion8[0]['pfecg_ppto_ejecutado'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																													</div>
																												</div>
																										
																										<?php
																											if($gestiones>=9)
																											{
																												?>
																													<div class="col-sm-2">
																														<div class="form-group">
																															<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+8;?></b></font></label>
																															<input class="form-control" type="text" name="pte9" id="pte9" value="<?php echo round($fase_gestion9[0]['pfecg_ppto_ejecutado'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																														</div>
																													</div>
																											
																											<?php
																												if($gestiones>=10)
																												{
																													?>
																														<div class="col-sm-2">
																															<div class="form-group">
																																<label><font size="1" color="blue"><b>GESTION <?php echo $fecha+9;?></b></font></label>
																																<input class="form-control" type="text" name="pte10" id="pte10" value="<?php echo round($fase_gestion10[0]['pfecg_ppto_ejecutado'],1) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }">
																															</div>
																														</div>
																												
																												<?php
																												}
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}

																			}
																		?>
																	</div>
																</div>

															</div> <!-- end well -->
															
												</div>
												<div class="form-actions">
													<a href="<?php echo site_url("admin").'/prog/update_fase/'.$mod.'/'.$fase_proyecto[0]['id'].'/'.$proyecto[0]['proy_id'].'/1'; ?>" class="btn btn-lg btn-default" title="VOLVER A MIS FASES"> ATRAS </a>
													<input type="button" value="GUARDAR" class="btn btn-primary btn-lg" onclick="valida_envia()" title="GUARDAR DATOS">
												</div>
											</form>
										</div>
				
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
					</div>
				
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<script type="text/javascript">
		//------------------------------------------
			function suma_presupuesto()
			{ 
					ptotal = parseFloat($('[name="ppt"]').val());
					if("<?php echo $gestiones?>">=1)
					{	
						a1 = parseFloat($('[name="pt1"]').val());
						$('[name="montos"]').val((a1).toFixed(2) );
						a = parseFloat($('[name="pte1"]').val());
						$('[name="total"]').val((a).toFixed(2) );
						$('[name="pe"]').val((a).toFixed(2) );

						if("<?php echo $gestiones?>">=2)
						{
							b1 = parseFloat($('[name="pt2"]').val());
							$('[name="montos"]').val((a1+b1).toFixed(2) );

							b = parseFloat($('[name="pte2"]').val());
							$('[name="total"]').val((a+b).toFixed(2) );
							$('[name="pe"]').val((a+b).toFixed(2) );

							if("<?php echo $gestiones?>">=3)
							{
								c1 = parseFloat($('[name="pt3"]').val());
								$('[name="montos"]').val((a1+b1+c1).toFixed(2) );

								c = parseFloat($('[name="pte3"]').val());
								$('[name="total"]').val((a+b+c).toFixed(2) );
								$('[name="pe"]').val((a+b+c).toFixed(2) );

								if("<?php echo $gestiones?>">=4)
								{	
									d1 = parseFloat($('[name="pt4"]').val());
									$('[name="montos"]').val((a1+b1+c1+d1).toFixed(2) );

									d = parseFloat($('[name="pte4"]').val());
									$('[name="total"]').val((a+b+c+d).toFixed(2) );
									$('[name="pe"]').val((a+b+c+d).toFixed(2) );

									if("<?php echo $gestiones?>">=5)
									{
										e1 = parseFloat($('[name="pt5"]').val());
										$('[name="montos"]').val((a1+b1+c1+d1+e1).toFixed(2) );

										e = parseFloat($('[name="pte5"]').val());
										$('[name="total"]').val((a+b+c+d+e).toFixed(2) );
										$('[name="pe"]').val((a+b+c+d+e).toFixed(2) );

										if("<?php echo $gestiones?>">=6)
										{
											f1 = parseFloat($('[name="pt6"]').val());
											$('[name="montos"]').val((a1+b1+c1+d1+e1+f1).toFixed(2) );
											f = parseFloat($('[name="pte6"]').val());
											$('[name="total"]').val((a+b+c+d+e+f).toFixed(2) );
											$('[name="pe"]').val((a+b+c+d+e+f).toFixed(2) );
											

											if("<?php echo $gestiones?>">=7)
											{
												g1 = parseFloat($('[name="pt7"]').val());
												$('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1).toFixed(2) );
												g = parseFloat($('[name="pte7"]').val());
												$('[name="total"]').val((a+b+c+d+e+f+g).toFixed(2) );
												$('[name="pe"]').val((a+b+c+d+e+f+g).toFixed(2) );
												

												if("<?php echo $gestiones?>">=8)
												{
													h1 = parseFloat($('[name="pt8"]').val());
													$('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1+h1).toFixed(2) );
													
													h = parseFloat($('[name="pte8"]').val());
													$('[name="total"]').val((a+b+c+d+e+f+g+h).toFixed(2) );
													$('[name="pe"]').val((a+b+c+d+e+f+g+h).toFixed(2) );
													
												}
											}
										}
									}
								}
							}
						}

					}

			}
		</script>
		<!--================================================== -->
		<script>
            function valida_envia()
            { 
               
                if (document.formulario.gest.value>="1") /////// 1 GESTION
                  { 
                      /*-------------------- verificando gestiones 1-----------------*/
                      if (document.formulario.pt1.value=="") /////// gestion 1
	                  { 
	                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha ?> ") 
	                      document.formulario.pt1.focus() 
	                      return 0; 
	                  }
	                  if (document.formulario.pte1.value=="") /////// gestion 1
	                  { 
	                      alert("Ingrese Campo TOTAL EJECUTADO <?php echo $fecha ?>") 
	                      document.formulario.pte1.focus() 
	                      return 0; 
	                  }
	                  /*-------------------- end verificando gestiones 1-----------------*/
	                  /*-------------------- verificando valores 1-----------------*/
	                  if(parseFloat(document.formulario.pt1.value)>parseFloat(document.formulario.ppt.value))
	                  {
	                  	  alert("Error!! PRESUPUESTO <?php echo $fecha ?>") 
	                      document.formulario.pt1.focus() 
	                      return 0; 
	                  }
	                  if(parseFloat(document.formulario.pte1.value)>parseFloat(document.formulario.pe.value))
	                  {
	                  	  alert("Error!! PRESUPUESTO EJECUTADO <?php echo $fecha ?>") 
	                      document.formulario.pte1.focus() 
	                      return 0; 
	                  }
	                  if(parseFloat(document.formulario.pte1.value)>parseFloat(document.formulario.pt1.value))
	                  {
	                  	  alert("Error!! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO <?php echo $fecha ?>") 
	                      document.formulario.pte1.focus() 
	                      return 0; 
	                  }
	                  /*-------------------- end verificando valores 1-----------------*/

	                  if (document.formulario.gest.value>="2") /////// 2 GESTION
	                  {
	                  	  /*-------------------- verificando gestiones 2-----------------*/
	                  	  if (document.formulario.pt2.value=="") /////// gestion 1
		                  { 
		                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+1 ?>") 
		                      document.formulario.pt2.focus() 
		                      return 0; 
		                  }
		                  if (document.formulario.pte2.value=="") /////// gestion 1
		                  { 
		                      alert("Ingrese Campo TOTAL EJECUTADO <?php echo $fecha+1 ?>") 
		                      document.formulario.pte2.focus() 
		                      return 0; 
		                  }
		                  /*-------------------- end verificando gestiones 2-----------------*/
		                  /*-------------------- verificando valores 2-----------------*/
		                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)>parseFloat(document.formulario.ppt.value))
		                  {
		                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+1 ?>") 
		                      document.formulario.pt2.focus() 
		                      return 0; 
		                  }
		                  if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)>parseFloat(document.formulario.pe.value))
		                  {
		                  	  alert("Error!! PRESUPUESTO EJECUTADO <?php echo $fecha+1?>") 
		                      document.formulario.pte2.focus() 
		                      return 0; 
		                  }
		                  if(parseFloat(document.formulario.pte2.value)>parseFloat(document.formulario.pt2.value))
		                  {
		                  	  alert("Error!! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO <?php echo $fecha+1 ?>") 
		                      document.formulario.pte2.focus() 
		                      return 0; 
		                  }
		                  /*-------------------- end verificando valores 2-----------------*/

			                  if (document.formulario.gest.value>="3") /////// 3 GESTION
			                  {
			                  	  /*-------------------- verificando gestiones 3-----------------*/
			                  	  if (document.formulario.pt3.value=="") /////// gestion 1
				                  { 
				                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+2 ?>") 
				                      document.formulario.pt3.focus() 
				                      return 0; 
				                  }
				                  if (document.formulario.pte3.value=="") /////// gestion 1
				                  { 
				                      alert("Ingrese Campo TOTAL EJECUTADO <?php echo $fecha+2 ?>") 
				                      document.formulario.pte3.focus() 
				                      return 0; 
				                  }
				                  /*-------------------- end verificando gestiones 3-----------------*/
				                  /*-------------------- verificando valores 3-----------------*/
				                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)>parseFloat(document.formulario.ppt.value))
				                  {
				                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+2 ?>") 
				                      document.formulario.pt3.focus() 
				                      return 0; 
				                  }
				                  if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)+parseFloat(document.formulario.pte3.value)>parseFloat(document.formulario.pe.value))
				                  {
				                  	  alert("Error!! PRESUPUESTO EJECUTADO <?php echo $fecha+2 ?>") 
				                      document.formulario.pte3.focus() 
				                      return 0; 
				                  }
				                  if(parseFloat(document.formulario.pte3.value)>parseFloat(document.formulario.pt3.value))
				                  {
				                  	  alert("Error!! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO <?php echo $fecha+2 ?>") 
				                      document.formulario.pte3.focus() 
				                      return 0; 
				                  }
				                  /*-------------------- end verificando valores 3-----------------*/

				                  	if (document.formulario.gest.value>="4") /////// 4 GESTION
					                  {
					                  	  /*-------------------- verificando gestiones 4-----------------*/
					                  	  if (document.formulario.pt4.value=="") /////// gestion 1
						                  { 
						                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+3 ?>") 
						                      document.formulario.pt4.focus() 
						                      return 0; 
						                  }
						                  if (document.formulario.pte4.value=="") /////// gestion 1
						                  { 
						                      alert("Ingrese Campo TOTAL EJECUTADO <?php echo $fecha+3 ?>") 
						                      document.formulario.pte4.focus() 
						                      return 0; 
						                  }
						                  /*--------------------end verificando gestiones 4-----------------*/
						                  /*-------------------- verificando valores 4-----------------*/
						                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)>parseFloat(document.formulario.ppt.value))
						                  {
						                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+3 ?>") 
						                      document.formulario.pt4.focus() 
						                      return 0; 
						                  }
						                  if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)+parseFloat(document.formulario.pte3.value)+parseFloat(document.formulario.pte4.value)>parseFloat(document.formulario.pe.value))
						                  {
						                  	  alert("Error!! PRESUPUESTO EJECUTADO <?php echo $fecha+3 ?>") 
						                      document.formulario.pte4.focus() 
						                      return 0; 
						                  }
						                  if(parseFloat(document.formulario.pte4.value)>parseFloat(document.formulario.pt4.value))
						                  {
						                  	  alert("Error!! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO <?php echo $fecha+3 ?>") 
						                      document.formulario.pte4.focus() 
						                      return 0; 
						                  }
						                  /*-------------------- end verificando valores 4-----------------*/

						                  	if (document.formulario.gest.value>="5") /////// 5 GESTION
							                  {
							                  	  /*-------------------- verificando gestiones 5-----------------*/
							                  	  if (document.formulario.pt5.value=="") /////// gestion 1
								                  { 
								                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+4 ?>") 
								                      document.formulario.pt5.focus() 
								                      return 0; 
								                  }
								                  if (document.formulario.pte5.value=="") /////// gestion 1
								                  { 
								                      alert("Ingrese Campo TOTAL EJECUTADO <?php echo $fecha+4 ?>") 
								                      document.formulario.pte5.focus() 
								                      return 0; 
								                  }
								                  /*-------------------- end verificando gestiones 5-----------------*/
								                  /*-------------------- verificando valores 5-----------------*/
								                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)>parseFloat(document.formulario.ppt.value))
								                  {
								                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+4 ?>") 
								                      document.formulario.pt5.focus() 
								                      return 0; 
								                  }
								                  if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)+parseFloat(document.formulario.pte3.value)+parseFloat(document.formulario.pte4.value)+parseFloat(document.formulario.pte5.value)>parseFloat(document.formulario.pe.value))
								                  {
								                  	  alert("Error!! PRESUPUESTO EJECUTADO <?php echo $fecha+4 ?>") 
								                      document.formulario.pte5.focus() 
								                      return 0; 
								                  }
								                  if(parseFloat(document.formulario.pte5.value)>parseFloat(document.formulario.pt5.value))
								                  {
								                  	  alert("Error!! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO <?php echo $fecha+4 ?>") 
								                      document.formulario.pte5.focus() 
								                      return 0; 
								                  }
								                  /*-------------------- end verificando valores 4-----------------*/

								                  	if (document.formulario.gest.value>="6") /////// 6 GESTION
									                  {
									                  	  /*-------------------- verificando gestiones 6-----------------*/
									                  	  if (document.formulario.pt6.value=="") /////// gestion 1
										                  { 
										                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+5 ?>") 
										                      document.formulario.pt6.focus() 
										                      return 0; 
										                  }
										                  if (document.formulario.pte6.value=="") /////// gestion 1
										                  { 
										                      alert("Ingrese Campo TOTAL EJECUTADO <?php echo $fecha+5 ?>") 
										                      document.formulario.pte6.focus() 
										                      return 0; 
										                  }
										                  /*--------------------end verificando gestiones 6-----------------*/
										                  /*-------------------- verificando valores 6-----------------*/
										                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)>parseFloat(document.formulario.ppt.value))
										                  {
										                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+5 ?>") 
										                      document.formulario.pt6.focus() 
										                      return 0; 
										                  }
										                  if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)+parseFloat(document.formulario.pte3.value)+parseFloat(document.formulario.pte4.value)+parseFloat(document.formulario.pte5.value)+parseFloat(document.formulario.pte6.value)>parseFloat(document.formulario.pe.value))
										                  {
										                  	  alert("Error!! PRESUPUESTO EJECUTADO <?php echo $fecha+5 ?>") 
										                      document.formulario.pte6.focus() 
										                      return 0; 
										                  }
										                  if(parseFloat(document.formulario.pte5.value)>parseFloat(document.formulario.pt5.value))
										                  {
										                  	  alert("Error!! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO <?php echo $fecha+5 ?>") 
										                      document.formulario.pte5.focus() 
										                      return 0; 
										                  }
										                  /*-------------------- end verificando valores 6-----------------*/

									                  }
									                  else{
									                  	alert("error de gestiones")
									                  }
							                  }
					                  }
			                  }
	                  }


                  }

                  if(document.formulario.montos.value<=document.formulario.ppt.value)
	                {
	                	var OK = confirm("GUARDAR INFORMACION ?");
			                if (OK) {
			                      document.formulario.submit(); 
			                    }
	                }
	                else
	                {
	                	alert('La suma de los presupuestos asignados no puede sobre pasar el monto presupuesto tota de la gestion')
	                }
             	   	
            }
          </script>
               }
               }
               	}
		
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
	</body>
</html>
