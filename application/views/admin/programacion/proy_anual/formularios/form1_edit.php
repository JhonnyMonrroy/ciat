<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
        <style type="text/css">
            aside{background: #05678B;}
        </style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                            <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                            </span>
						<i class="fa fa-angle-down"></i>
					</a>
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i><span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <?php
						if($proyecto[0]['proy_estado']==1){ ?>
							<a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
							<?php
						}
						elseif ($proyecto[0]['proy_estado']==2){ ?>
							<a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
							<?php
						}
						?>
		            </li>
                    <?php
                    if($nro_fase==1){
                        for($i=0;$i<count($enlaces);$i++){ ?>
                            <li>
                                <a href="#" >
                                    <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                                <ul>
                                    <?php
                                    $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                                    foreach($submenu as $row) {
                                        ?>
                                        <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php
                        }
                    }
                    ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<?php
					if($proyecto[0]['proy_estado']==1){ ?>
						<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>">T&eacute;cnico de Unidad Ejecutora</a></li><li>Datos Generales</li>
						<?php
					}
					elseif ($proyecto[0]['proy_estado']==2){ ?>
						<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>">T&eacute;cnico Analista POA</a></li><li>Datos Generales</li>
						<?php
					} ?>
				</ol>
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
				<section id="widget-grid" class="">
				<div class="row">
					<nav role="navigation" class="navbar navbar-default navbar-inverse">
				        <div class="navbar-header">
				            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
				                <span class="sr-only">Toggle navigation</span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </button>
				        </div>
				 
				        <div id="navbarCollapse" class="collapse navbar-collapse">
				            <ul class="nav navbar-nav">
				                <li class="active"><a href="#"><i class="glyphicon glyphicon-ok"></i>&nbsp;DATOS GENERALES&nbsp;</a></li>
								<li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/10' ?>" title="<?php echo strtoupper($titulo);?> DEL PROYECTO">&nbsp;<?php echo strtoupper($titulo);?>&nbsp;</a></li>
				                <?php
		                        if($this->session->userdata("rol_id")==1){ ?>
		                        <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/2' ?>">&nbsp;RESPONSABLES&nbsp;</a></li><?php
		                        } ?>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/3' ?>">&nbsp;CLASIFICACI&Oacute;N&nbsp;</a></li>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/4?qRegistro='.$proyecto[0]['proy_id'].'' ?>">&nbsp;LOCALIZACI&Oacute;N&nbsp;</a></li>
				                
				                <?php
		                            if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3){ ?>
		                                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/5' ?>" title="MARCO LOGICO">&nbsp;OBJETIVOS&nbsp;</a></li>
		                            <?php }
		                        ?>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/7' ?>">&nbsp;RESUMEN T&Eacute;CNICO&nbsp;</a></li>
				                <li><a href='<?php echo site_url("admin").'/proy/proyecto/'.$proyecto[0]['proy_id'].'/8'; ?>'>ANEXOS DE LA OPERACI&Oacute;N</a></li>
				            	<?php
				            		if($proyecto[0]['proy_estado']!=4){
				            			if($proyecto[0]['tp_id']==1){ ?>
				            			<li><a href='<?php echo site_url("admin").'/proy/fase_etapa/'.$proyecto[0]['proy_id']; ?>' title="FASE ETAPA COMPONENTE">&nbsp;FASE / ETAPA&nbsp;</a></li>
				            			<?php
					            		}
					            		elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4){ ?>
					            			<li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO">&nbsp;PRESUPUESTO&nbsp;</a></li>
					            			<?php
					            		}
				            		}
				            	?>
				            </ul>
				        </div>
					</nav>
				</div>

					<!-- row -->
					<div class="row">
							<!-- Widget ID (each widget will need unique ID)-->
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<form name="form_nuevo_obj" id="form_nuevo_obj" method="post" action="<?php echo site_url("") . '/programacion/proyecto/actualizar_datos'?>">
								<input type="hidden" name="id" id="id" value="<?php echo $proyecto[0]['proy_id'] ?>">
								<input type="hidden" name="form" id="form" value="1">
								<input type="hidden" name="tp_ant" id="tp_ant" value="<?php echo $proyecto[0]['tp_id'] ?>">
								<input type="hidden" name="aper_id" id="aper_id" value="<?php echo $proyecto[0]['aper_id'] ?>" >
								<input  type="hidden" name="gestion" id="gestion" value="<?php echo $this->session->userdata("gestion") ?>">
								<input  type="hidden" name="gi" id="gi" value="<?php echo $proyecto[0]['inicio'] ?>">
								<input  type="hidden" name="gf" id="gf" value="<?php echo $proyecto[0]['fin'] ?>">
								<div class="jarviswidget jarviswidget-color-darken" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<h2 class="font-md"><strong>DATOS GENERALES DE LA OPERACI&Oacute;N </strong></h2>
									</header>
										<!-- widget content -->
											<div class="widget-body">
												<div id="bootstrap-wizard-1" class="col-sm-8">
															<div class="well">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="form-group">
																			<label><b>NOMBRE DE PROYECTO, PROGRAMA Y OPERACI&Oacute;N INSTITUCIONAL </b><span class="text-info">(Obligatorio)</span></label>
																			<textarea rows="4" class="form-control" name="nom_proy" id="nom_proy" style="width:100%;" maxlength="200"><?php echo $proyecto[0]['proy_nombre'] ?></textarea>
																		</div>
																	</div>
																</div>

																<div class="row">
																	<div class="col-sm-4">
																		<div class="form-group">
																		<label><b>TIPO DE OPERACI&Oacute;N </b><span class="text-info">(Obligatorio)</span></label>
																		<select class="select2" id="tp_id2" name="tp_id2" title="Seleccione Tipo de Proyecto">
					                                                        <?php 
															                    foreach($tp_proy as $row)
															                    {
															                    	if($row['tp_id']==$proyecto[0]['tp_id']){ ?>
																	                     <option value="<?php echo $row['tp_id']; ?>" selected><?php echo $row['tp_tipo']; ?></option>
																	                    <?php 
															                    	}
															                    	else{ ?>
																	                     <option value="<?php echo $row['tp_id']; ?>"><?php echo $row['tp_tipo']; ?></option>
																	                    <?php 
															                    	}	
															                    }
															                ?>        
						                                                  	</select>
																		</div>
																	</div>
																	<div class="col-sm-4">
																		<div class="form-group">
																			<label><b>PONDERACI&Oacute;N </b><span class="text-info">(Automatico)</span></label>
																			<input class="form-control" type="number" value="<?php echo $proyecto[0]['proy_ponderacion'] ?>" disabled="true">
																			<input class="form-control" type="hidden" name="pn_cion" id="pn_cion" value="<?php echo $proyecto[0]['proy_ponderacion'] ?>" >
																		</div>
																	</div>
																	<?php 
																	if($proyecto[0]['tp_id']==1){ ?>
																		<div id="pi2">  <!-- Relativo -->
																			<div class="col-sm-4">
																				<div class="form-group"> 
																					<label><b>C&Oacute;DIGO SISIN </b><span class="text-info">(Opcional)</span></label>
																					<input class="form-control" type="text" name="cod_sisin" id="cod_sisin" value="<?php echo $proyecto[0]['proy_sisin'] ?>" data-mask="9999-99999-99999" data-mask-placeholder= "X" placeholder="Codigo Proyecto">
																				</div>
																			</div>
																		</div>
																		<div id="pr2" style="display:none;">  <!-- Relativo -->
																			<div class="col-sm-4">
																				<div class="form-group"> 
																					<label><b>COSTO TOTAL PROYECTO </b><span class="text-info">(Opcional)</span></label>
																					<?php
																					if($nro_f==0){ ?>
																						<input class="form-control" type="text" name="mtotal" id="mtotal" placeholder="0" value="0"  onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																						<?php
																					}
																					else{ ?>
																						<input class="form-control" type="text" name="mtotal" id="mtotal" placeholder="0" value="<?php echo $fase[0]['pfec_ptto_fase'] ?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																						<?php
																					}
																					?>
																					
																				</div>
																			</div>
																		</div>
																		<?php
																	}
																	else{ ?>
																		<div id="pi2" style="display:none;">  <!-- Relativo -->
																			<div class="col-sm-4">
																				<div class="form-group"> 
																					<label><b>C&Oacute;DIGO SISIN </b><span class="text-info">(Opcional)</span></label>
																					<input class="form-control" type="text" name="cod_sisin" id="cod_sisin" value="<?php echo $proyecto[0]['proy_sisin'] ?>" data-mask="9999-99999-99999" data-mask-placeholder= "X" placeholder="Codigo Proyecto">
																				</div>
																			</div>
																		</div>
																		<div id="pr2" >  <!-- Relativo -->
																			<div class="col-sm-4">
																				<div class="form-group"> 
																					<label><b>COSTO TOTAL PROGRAMA </b> <span class="text-info">(Opcional)</span></label>
																					<?php
																					if($nro_f==0) { ?>
																						<input class="form-control" type="text" name="mtotal" id="mtotal" placeholder="0" value="0"  onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" >
																						<?php
																					}
																					else{ ?>
																						<input class="form-control" type="text" name="mtotal" id="mtotal" placeholder="0" value="<?php echo $fase[0]['pfec_ptto_fase'] ?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" >
																						<?php
																					} ?>
																				</div>
																			</div>
																		</div>
																		<?php } ?>
																</div>
																<div class="row">
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label><b>FECHA INICIAL DEL PROYECTO </b><span class="text-info">(dd/mm/yyyy)</span></label>
																			<div class="input-group">
																					<input type="text" name="f_ini" id="f_ini" placeholder="Seleccione Fecha inicial" value="<?php echo date('d/m/Y',strtotime($proyecto[0]['f_inicial'])); ?>" class="form-control datepicker" data-dateformat="dd/mm/yy"
							                                                   			onKeyUp="this.value=formateafecha(this.value);">
																					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																			</div>
																		</div>
																	</div>

																	<div class="col-sm-6">
																		<div class="form-group">
																			<label><b>FECHA FINAL DEL PROYECTO </b><span class="text-info">(dd/mm/yyyy)</span></label> 
																			<div class="input-group">
																					<input type="text" name="f_final" id="f_final" placeholder="Seleccione Fecha final" value="<?php echo date('d/m/Y',strtotime($proyecto[0]['f_final'])); ?>" class="form-control datepicker" data-dateformat="dd/mm/yy"
							                                                   		onKeyUp="this.value=formateafecha(this.value);">
																					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																			</div>
																		</div>
																	</div>
																</div>
																

															</div> <!-- well -->
													</div><!-- id="bootstrap-wizard-1" class="col-sm-8" -->
													<div id="bootstrap-wizard-1" class="col-sm-4">
														<div class="well">
															<div class="row">
																<center><b>CATEGORIA PROGRAM&Aacute;TICA</b></center>
																<center><b>GESTI&Oacute;N <?php echo $this->session->userData('gestion') ?></b></center>
																<div class="col-sm-12">
																	<div class="form-group">
																		<label><b>PROGRAMA </b><span class="text-info">(Obligatorio)</span></label>
																		<input class="form-control" type="hidden" name="prog_a" id="prog_a" value="<?php echo $proyecto[0]['aper_programa'] ?>" >
																		<input class="form-control" type="hidden" name="proy_a" id="proy_a" value="<?php echo $proyecto[0]['aper_proyecto'] ?>" >
																		<input class="form-control" type="hidden" name="act_a" id="act_a" value="<?php echo $proyecto[0]['aper_actividad'] ?>" >
																		<input class="form-control" type="hidden" name="tp_id" id="tp_id" value="<?php echo $proyecto[0]['tp_id'] ?>" >

																		<select class="select2" id="prog" name="prog" title="Seleccione Tipo de Proyecto">
					                                                        <?php 
															                    foreach($programas as $row)
															                    {
															                    	if($row['aper_programa']==$proyecto[0]['aper_programa']) { ?>
																	                     <option value="<?php echo $row['aper_programa']; ?>" selected><?php echo $row['aper_programa'].' - '.$row['aper_descripcion']; ?></option>
																	                    <?php 
															                    	}
															                    	else{ ?>
																	                     <option value="<?php echo $row['aper_programa']; ?>"><?php echo $row['aper_programa'].' - '.$row['aper_descripcion']; ?></option>
																	                    <?php 
															                    	}	
															                    }
															                ?>        
					                                                  	</select>
																	</div>
																</div>
																<div class="col-sm-12">
																	<div class="form-group">
																		<label><b>PROYECTO</b></label>
																		<input class="form-control" type="text" name="proy" id="proy" value="<?php echo $proyecto[0]['aper_proyecto'] ?>" maxlength="18" onpaste="return false">
																		
																	</div>
																</div>
																<div class="col-sm-12">
																	<div class="form-group">
																		<label><b>ACTIVIDAD</b></label>
																		<input class="form-control" type="text" name="act" id="act" value="<?php echo $proyecto[0]['aper_actividad'] ?>" data-mask="999" data-mask-placeholder= "9" onpaste="return false">
																	</div>
																</div>
															</div>
														</div><br>

														<div class="well">
															<div class="row">
																<div class="col-sm-12">
																	<center><b>TIPO DE GASTO </b><span class="text-info">(Obligatorio)</span></center>
																	<div class="form-group">
																		<select class="select2" id="tg" name="tg" title="Seleccione Tipo de Gasto">
					                                                        <option value="">Seleccione</option>
					                                                        <?php 
																		        foreach($tp_gasto as $row)
																		        {
																		            if($row['tg_id']==$proyecto[0]['tg_id']){ ?>
																	                     <option value="<?php echo $row['tg_id']; ?>" selected><?php echo $row['tg_descripcion']; ?></option>
																	                    <?php 
															                    	}
															                    	else{ ?>
																	                     <option value="<?php echo $row['tg_id']; ?>"><?php echo $row['tg_descripcion']; ?></option>
																	                    <?php 
															                    	}
																		        }
																		    ?>        
					                                                  	</select>
																	</div>
																</div>
																<div class="col-sm-12">
																	<center><b>TIPO</b><span class="text-info">(Obligatorio)</span></center>
																	<div class="form-group">
																		<select class="select2" id="tp_cap" name="tp_cap" title="Seleccione Tipo">
					                                                        <option value="">Seleccione</option>
					                                                        <?php 
																		        foreach($list_cap as $row)
																		        {
																		            if($row['cp_id']==$proyecto[0]['cp_id'])
															                    	{
															                    		?>
																	                     <option value="<?php echo $row['cp_id']; ?>" selected><?php echo $row['cp_descripcion']; ?></option>
																	                    <?php 
															                    	}
															                    	else
															                    	{
															                    		?>
																	                     <option value="<?php echo $row['cp_id']; ?>"><?php echo $row['cp_descripcion']; ?></option>
																	                    <?php 
															                    	}
																		        }
																		    ?>     
					                                                  	</select>
																	</div>
																</div>

																<div class="row">
																	<div class="col-sm-12">
																		<center><b id="titulo_comp"><?php echo $comp;?></b><br> REQUIEREN DE UNIDADES EJECUTORAS ?<span class="text-info">(Obligatorio)</span></center>
																		<div class="form-group">
																			<select class="select2" id="comp" name="comp" title="Seleccione Tipo">
																				<option value="">Seleccione</option>
						                                                        <?php 
						                                                        	if($proyecto[0]['comp']==1){
						                                                        		?>
						                                                        		<option value="1" selected="true">SI</option>   
						                                                        		<option value="0">NO</option> 
						                                                        		<?php
						                                                        	}
						                                                        	else{
						                                                        		?>
						                                                        		<option value="1">SI</option>   
						                                                        		<option value="0" selected="true">NO</option> 
						                                                        		<?php
						                                                        	}
						                                                        ?>
						                                                  	</select>
																		</div>
																	</div>
																</div>	
															</div>
														</div>
													</div> <!-- id="bootstrap-wizard-1" class="col-sm-4" -->
														

												</div><!-- class="widget-body" -->
												<div class="form-actions">
													<?php
													if($proyecto[0]['proy_estado']!=4){
														if($proyecto[0]['proy_estado']==1){ ?>
														<a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS PROYECTOS"> CANCELAR </a>
														<?php
														}
														elseif ($proyecto[0]['proy_estado']==2){ ?>
															<a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS PROYECTOS"> CANCELAR </a>
															<?php
														}


	                                                    if($rol_modifica==true){?>
	                                                    <input type="button" id="enviar_obj" name="enviar_obj" value="MODIFICAR PROYECTO" class="btn btn-primary btn-lg">
	                                                    <?php }else{?>
	                                                        <a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/10' ?>" class="btn btn-primary btn-lg" title="REGISTRAR METAS"> PASAR A METAS </a>
	                                                    <?php }
													}
													?>
												</div>
										</div> <!-- class="jarviswidget jarviswidget-color-darken" -->
									</form>
								</article>
							<!-- end widget -->
					</div>
					<!-- end row -->
				</section>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer"> 
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/ejecucion/abm_ejecucion.js" type="text/javascript"></SCRIPT>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			pageSetUp();
		})
		</script>
		<script >
		$(function () {
			function reset() {
		            $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
		            alertify.set({
		                labels: {
		                    ok: "ACEPTAR",
		                    cancel: "CANCELAR"
		                },
		                delay: 5000,
		                buttonReverse: false,
		                buttonFocus: "ok"
		            });
		        }
		    $("#enviar_obj").on("click", function (e) {
		        var $validator = $("#form_nuevo_obj").validate({
		            rules: {
		                nom_proy: {
		                    required: true,
		                },
		                tp_id2: {
		                    required: true,
		                },
		                ini: {
		                    required: true,
		                },
		                fin: {
		                    required: true,
		                }
		            },
		            messages: {
		                nom_proy: {required: "Campo Requerido"},
		                tp_id2: {required: "Campo Requerido"},
		                ini: {required: "Campo Requerido"},
		                fin: {required: "Campo Requerido"}
		            },
		            highlight: function (element) {
		                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            },
		            unhighlight: function (element) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            },
		            errorElement: 'span',
		            errorClass: 'help-block',
		            errorPlacement: function (error, element) {
		                if (element.parent('.input-group').length) {
		                    error.insertAfter(element.parent());
		                } else {
		                    error.insertAfter(element);
		                }
		            }
		        });
		        var $valid = $("#form_nuevo_obj").valid();
		        if (!$valid) {
		            $validator.focusInvalid();
		        } 
		        else {
		        		
		        		if(document.getElementById("tp_id").value!=1){
		                  	if (document.getElementById("mtotal").value==""){ 
			                    alertify.alert("POR FAVOR REGISTRE EL MONTO TOTAL DEL PROGRAMA, OPERACIÓN");
			                	return false; 
			                }
		                }
		                
		                if (document.form_nuevo_obj.tg.value==""){ 
		                    alertify.alert("SELECCIONE TIPO DE GASTOS") 
		                    document.form_nuevo_obj.tg.focus() 
		                    return 0; 
		                }

		                if (document.form_nuevo_obj.tp_cap.value==""){ 
		                    alertify.alert("SELECCIONE TIPO CAPITALIZABLE") 
		                    document.form_nuevo_obj.tp_cap.focus() 
		                    return 0; 
		                }

		                if (document.form_nuevo_obj.comp.value==""){ 
		                    alertify.alert("Seleccione si los componentes de la operacion requieren Unidades Ejecutoras") 
		                    document.form_nuevo_obj.comp.focus() 
		                    return 0; 
		                }

		        		var fecha_inicial = document.getElementById("f_ini").value.split("/")  //fecha inicial
	                	var fecha_final = document.getElementById("f_final").value.split("/")  /*fecha final*/

	                	if(document.getElementById("gestion").value<parseInt(fecha_inicial[2])){
			                alertify.alert("NO PUEDE REGISTRAR PROYECTOS,PROGRAMAS,OPERACIONES DE GESTIONES POSTERIORES A LA GESTION VIGENTE DEL SISTEMA");
			                return false;
	                	}

	                	if(parseInt(fecha_final[2])<parseInt(fecha_inicial[2])){
	                		alertify.alert("ERROR !!! VERIFIQUE LAS FECHAS DE INICIO Y FINAL DEL PROYECTO,PROGRAMA, OPERACIÓN");
			                return false;
	                	}

	                	 /*------------------------- Fecha Inicial ----------------------*/
	                	if(parseInt(fecha_inicial[0])>'31' || parseInt(fecha_inicial[1])>'12' || parseInt(fecha_inicial[2])<'2008'){
	                		alertify.alert("ERROR !!! VERIFIQUE LA FECHA INICIAL REGISTRADO ");
			                return false;
	                	}
	                	/*-----------------------------------------------------------------*/
	                	/*------------------------------ Fecha Final ----------------------*/
	                	if(parseInt(fecha_final[2])>'2027' || parseInt(fecha_final[0])>'31' || parseInt(fecha_final[1])>'12'){
	                		alertify.alert("ERROR !!! VERIFIQUE LA FECHA FINAL REGISTRADO ");
			                return false;
	                	}
		            
	                	if(document.getElementById("gestion").value<parseInt(fecha_inicial[2]) || parseInt(fecha_inicial[2])<document.getElementById("gi").value || parseInt(fecha_inicial[2])>document.getElementById("gi").value){
	                		alertify.confirm("SE MODIFICARA LA GESTIÓN DE INICIO : DEL "+document.getElementById("gi").value+" A "+parseInt(fecha_inicial[2]),function(a){
                                if(a){ return true;}
                                else {return false;}
                            });
	                	}

	                	if(parseInt(fecha_final[2])<document.getElementById("gf").value || parseInt(fecha_final[2])>document.getElementById("gf").value){
	                		alertify.confirm("SE MODIFICARA LA GESTIÓN DE FINAL : DEL "+document.getElementById("gf").value+" A "+parseInt(fecha_final[2]),function(a){
	                		if(a){ return true;}
	                		else {return false;}
                            });
	                	}

	                	var aper1 = document.getElementById("prog_a").value.concat(document.getElementById("proy_a").value.concat(document.getElementById("act_a").value)); ///// apertura antiguo
	             		var aper2 = document.getElementById("prog").value.concat(document.getElementById("proy").value.concat(document.getElementById("act").value)); //// apertura nuevo

	             		if(aper1==aper2)
		             	{
		             		reset();
		             		alertify.confirm("MODIFICAR PROYECTO,PROGRAMA,OPERACI\u00D3N?", function (a) {
			                    if (a) {
			                        document.form_nuevo_obj.submit();
			                        document.getElementById('enviar_obj').disabled = true;
			                    } else {
			                        alertify.error("OPCI\u00D3N CANCELADA");
			                    }
			                });
		             	}
		             	else
		             	{
		             		var prog = document.getElementById("prog").value;
		             		var proy = document.getElementById("proy").value;
		             		var act = document.getElementById("act").value;

		             		var url = "<?php echo site_url("admin")?>/proy/verif";
								$.ajax({
									type:"post",
									url:url,
									data:{prog:prog,proy:proy,act:act},
									success:function(datos){
										
									if(datos.trim() =='true'){
										alertify.alert("LA CATEGORIA PROGRAMATICA SELECCIONADO YA SE ENCUENTRA REGISTRADO");
			                			return false;
									}else{
										reset();
										alertify.confirm("MODIFICAR PROYECTO,PROGRAMA,OPERACI\u00D3N?", function (a) {
						                    if (a) {
						                        document.form_nuevo_obj.submit();
						                        document.getElementById('enviar_obj').disabled = true;
						                    } else {
						                        alertify.error("OPCI\u00D3N CANCELADA");
						                    }
						                });
									}

								}});
		             	}

		        }
		    });
		});

		</script>
	</body>
</html>
