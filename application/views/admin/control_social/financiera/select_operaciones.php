<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <title><?php echo $this->session->userdata('name')?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <!--estiloh-->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
    <meta name="viewport" content="width=device-width">
  </head>
  <body class="">
    <header id="header">
      <!-- pulled right: nav area -->
      <div class="pull-right">
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
          <span> <a href="<?php echo site_url("user/logout_control_social"); ?>" title="SALIR DE CONTROL SOCIAL" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a></span>
        </div>
        <!-- end logout button -->
        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="EXPANDIR PANTALLA"><i class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
      </div>
      <!-- end pulled right: nav area -->
    </header>
    <!-- END HEADER -->
    <!-- MAIN PANEL -->
    <div>
      <!-- RIBBON -->
      <div id="ribbon">
        <span class="ribbon-button-alignment"> 
          <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
          </span> 
        </span>
        <!-- breadcrumb -->
        <ol class="breadcrumb">
          <li>Control Social <?php echo $this->session->userdata("gestion")?></li><li>Ejecuci&oacute;n Presupuestaria</li>
        </ol>
      </div>
      <!-- END RIBBON -->
      <!-- MAIN CONTENT -->
      <div id="content">
      <div class="well">
        <section id="widget-grid" class="">
          <!-- row -->
                    <div class="row">
            <article class="col-sm-12 col-md-2 col-lg-2">
            </article>
            <article class="col-sm-12 col-md-8 col-lg-8">
              <div class="alert alert-block alert-success" align="center">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <h4 class="alert-heading">EJECUCI&Oacute;N FINANCIERA <?php echo $this->session->userdata("gestion");?></h4>
              </div>
              <!-- Widget ID (each widget will need unique ID)-->
              <div class="jarviswidget" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false">
                <header>
                  <span class="widget-icon"> <i class="fa fa-check txt-color-green"></i> </span>
                  <h2></h2>        
                </header>
                <!-- widget div-->
                <div>
                  <!-- widget edit box -->
                  <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->
                  </div>
                  <!-- end widget edit box -->
                  <!-- widget content -->
                  <div class="widget-body no-padding">
                    <!-- Success states for elements -->
                    <form id="formulario" name="formulario" novalidate="novalidate" method="post" action="<?php echo site_url("").'/rep/valida_resfisfin'?>" class="smart-form">
                      <input type="hidden" name="tp" id="tp" value="2">
                      <header><b>SELECCIONE EL TIPO DE OPERACI&Oacute;N :</b></header>
                      <fieldset>
                        <section>
                          <div class="row">
                            <div class="col col-12">
                              <label class="checkbox state-success"><input type="checkbox" name="p1" id="p1"><i></i>PROYECTOS DE INVERSI&Oacute;N PUBLICA</label>
                            </div>
                            <div class="col col-12">
                              <label class="checkbox state-success"><input type="checkbox" name="p2" id="p2"><i></i>PROGRAMAS RECURRENTES</label>
                            </div>
                            <div class="col col-12">
                              <label class="checkbox state-success"><input type="checkbox" name="p3" id="p3"><i></i>PROGRAMAS NO RECURRENTES</label>
                            </div>
                            <div class="col col-12">
                              <label class="checkbox state-success"><input type="checkbox" name="p4" id="p4"><i></i>OPERACI&Oacute;N DE FUNCIONAMIENTO</label>
                            </div>
                          </div>
                          <div class="note note-success"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></div> 
                        </section>
                      </fieldset>
                      <footer>
                        <input type="button" value="GENERAR REPORTES" class="btn btn-primary btn-lg" onclick="valida_envia()" id="btsubmit" title="GENERAR LOS REPORTES SELECCIONADOS">
                        <a href="<?php echo base_url().'index.php/admin/control_social' ?>" class="btn btn-lg btn-default" title="Volver a Menu Principal">CANCELAR</a>
                      </footer>
                    </form>
                    <!--/ Success states for elements -->       
                  </div>
                  <!-- end widget content -->
                </div>
                <!-- end widget div -->
              </div>
              <!-- end widget -->
            </article>

          </div>
          <!-- end row -->
        </section>
        </div>
      </div>
      <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN PANEL -->

    <!-- PAGE FOOTER -->
    <div class="page-footer"> 
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
        </div>
      </div>
    </div>
    <!-- END PAGE FOOTER -->

    <!--================================================== -->
    <script>
      if (!window.jQuery) {
        document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
      }
    </script>

    <script>
      if (!window.jQuery.ui) {
        document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
      }
    </script>
    <script>
      function valida_envia()
      { 

        if(document.getElementById('p1').checked==false && document.getElementById('p2').checked==false && document.getElementById('p3').checked==false && document.getElementById('p4').checked==false)
        {
            alert('Seleccione opciones')
            return 0; 
        }
        
        document.formulario.submit();
        document.getElementById("btsubmit").value = "GENERANDO REPORTE...";
        document.getElementById("btsubmit").disabled = true;
        return true;

      }
    </script>

    <!-- IMPORTANT: APP CONFIG -->
    <script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
    <script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
    <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
    <script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
    <!-- BOOTSTRAP JS -->
    <script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
    <!-- CUSTOM NOTIFICATION -->
    <script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
    <!-- JARVIS WIDGETS -->
    <script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
    <!-- EASY PIE CHARTS -->
    <script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
    <!-- SPARKLINES -->
    <script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
    <!-- JQUERY VALIDATE -->
    <script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
    <!-- JQUERY MASKED INPUT -->
    <script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <!-- JQUERY SELECT2 INPUT -->
    <script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
    <!-- JQUERY UI + Bootstrap Slider -->
    <script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
    <!-- browser msie issue fix -->
    <script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
    <!-- FastClick: For mobile devices -->
    <script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
    <!-- Demo purpose only -->
    <script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
    <!-- MAIN APP JS FILE -->
    <script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
    <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
    <!-- Voice command : plugin -->
    <script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
  </body>
</html>
