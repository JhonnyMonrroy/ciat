<!DOCTYPE html>
<html lang="en-us" id="extr-page">
<head>
    <meta charset="utf-8">
    <title> Sistema Integrado de Planificación, Monitoreo, Seguimiento y Evaluación institucional v.1.1</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- #CSS Links -->
	<link href="https://fonts.googleapis.com/css?family=Overpass" rel="stylesheet">    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">

    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">

    <!-- #FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="<?php echo base_url(); ?>assets/image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">

    <!-- #APP SCREEN / ICONS -->
    <!-- Specifying a Webpage Icon for Web Clip
       Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/img/splash/touch-icon-ipad-retina.png">

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="<?php echo base_url(); ?>assets/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="<?php echo base_url(); ?>assets/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="<?php echo base_url(); ?>assets/img/splash/iphone.png" media="screen and (max-device-width: 320px)">
    <style>
    </style>

    <!--Verif. Nav.-->
	<script> 
	var $buoop = {
		notify:{e:-3,f:-3,o:-3,s:-3,c:-3},
		insecure:true,
		unsupported:true,
		api:5,
//		test:true,
		text:"Su navegador Web {brow_name} es demasiado antiguo. <br>Para trabajar con el <?php echo $this->session->userdata('name')?> de manera adecuada, descargue y actualice su navegador, <br> preferentemente utilice Google Chrome.  <br><a{up_but}>Actualizar</a> o <a{ignore_but}>Ignorar</a>",
	}; 
	function $buo_f(){ 
	 var e = document.createElement("script"); 
	 e.src = "//browser-update.org/update.min.js"; 
	 document.body.appendChild(e);
	};
	try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
	catch(e){window.attachEvent("onload", $buo_f)}
	</script>	

    <style type="text/css">
        input[type="text"],
        input[type="password"],
        select,
        textarea {
            height: 50px;
            margin: 1px solid #eee; !important
            padding: 0 20px;
            vertical-align: middle;
            background: #fff;
            border: 1px solid #ccc;
            font-family: 'Roboto', sans-serif;
            font-size: 16px;
            font-weight: 300;
            line-height: 40px;
            color: #888;
            -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;
            -moz-box-shadow: none; -webkit-box-shadow: none; box-shadow: none;
            -o-transition: all .3s; -moz-transition: all .3s; -webkit-transition: all .3s; -ms-transition: all .3s; transition: all .3s;
        }

        textarea.form-control {
			height:70px;
		}
		

        textarea,
        textarea.form-control {
            padding-top: 10px;
            padding-bottom: 10px;
            line-height: 30px;
        }

        input[type="text"]:focus,
        input[type="password"]:focus,
        textarea:focus,
        textarea.form-control:focus {
            outline: 0;
            background: #fff;
            border: 1px solid #999;
            -moz-box-shadow: none; -webkit-box-shadow: none; box-shadow: none;
        }

        input[type="text"]:-moz-placeholder, input[type="password"]:-moz-placeholder,
        textarea:-moz-placeholder, textarea.form-control:-moz-placeholder { color: #888; }

        input[type="text"]:-ms-input-placeholder, input[type="password"]:-ms-input-placeholder,
        textarea:-ms-input-placeholder, textarea.form-control:-ms-input-placeholder { color: #888; }

        input[type="text"]::-webkit-input-placeholder, input[type="password"]::-webkit-input-placeholder,
        textarea::-webkit-input-placeholder, textarea.form-control::-webkit-input-placeholder { color: #888; }



        body {
			font-family: 'Overpass', sans-serif;
            font-size: 1.5em;
            font-weight: 300;
            color: #888;
            line-height: 30px;
            text-align: center;
            background: #fff url("<?php echo base_url(); ?>assets/img_v1.1/gp.jpg") center top no-repeat fixed;
        }



        h1, h2 {
            margin-top: 10px;
            font-size: 38px;
            font-weight: 100;
            line-height: 50px;
        }

        h3 {
            font-size: 22px;
            font-weight: 300;
            line-height: 30px;
        }

        img { max-width: 100%; }

        ::-moz-selection { background: #de615e; color: #fff; text-shadow: none; }
        ::selection { background: #de615e; color: #fff; text-shadow: none; }




        /***** Top content *****/

        .inner-bg {
            padding: 10px 0 10px 0; /*MODIFICADO*/
        }

        .top-content .text {
            color: #fff;
        }

        .top-content .text h1 { color: #fff; }

        .top-content .description {
            margin: 20px 0 10px 0;
        }

        .top-content .description p { opacity: 0.8; }

        .top-content .description a {
            color: #fff;
        }
        .top-content .description a:hover,
        .top-content .description a:focus { border-bottom: 1px dotted #fff; }


        .form-top {
            overflow: hidden;
            padding: 0 25px 15px 25px;
            text-align: left;
        }

        .form-top-left {
            float: left;
            width: 75%;
            padding-top: 25px;
        }

        .form-top-left h3 { margin-top: 0; color: #fff; }
        .form-top-left p { opacity: 0.8; color: #fff; }

        .form-top-right {
            float: left;
            width: 25%;
            padding-top: 5px;
            font-size: 66px;
            color: #fff;
            line-height: 100px;
            text-align: right;
            opacity: 0.3;
        }

        .form-bottom {
            padding: 25px 25px 30px 25px;
            -moz-border-radius: 0 0 4px 4px; 
			-webkit-border-radius: 0 0 4px 4px; 
			border-radius: 0 0 4px 4px;
            text-align: left;
        }

        .form-bottom form textarea {
            height: 100px;
        }

        .form-bottom form button.btn {
            width: 100%;
        }

        .form-bottom form .input-error {
            border-color: #de615e;
        }

        .social-login {
            margin-top: 35px;
        }

        .social-login h3 {
            color: #fff;
        }

        .social-login-buttons {
            margin-top: 25px;
        }


        /***** Media queries *****/

        @media (min-width: 992px) and (max-width: 1199px) {}

        @media (min-width: 768px) and (max-width: 991px) {}

        @media (max-width: 767px) {

            .inner-bg { padding: 0px 0 110px 0; }

        }

        @media (max-width: 415px) {

            h1, h2 { font-size: 32px; }

        }

        #control{ background:#336699; }

		
		/* ESTILOS <?php echo $this->session->userdata('name')?> VERSION 1.1 */
		.right-corners{
            -moz-border-radius: 0 7px 7px 0; 
			-webkit-border-radius: 0 7px 7px 0; 
			border-radius: 0 7px 7px 0;
			color:#fff;
		}
		.left-corners{
            -moz-border-radius: 7px 0 0 7px; 
			-webkit-border-radius: 7px 0 0 7px; 
			border-radius: 7px 0 0 7px;
			color:#333;
		}
        .box-green {
            background-image: url('<?php echo base_url('assets/img_v1.1/ogov.jpg');?>');
/*            background: rgba(7, 144, 55, 0.7);*/
			background-repeat: no-repeat;
			background-size: 100% 100%;
			-moz-border-radius: 0 0 10px 10px; 
			-webkit-border-radius: 0 0 10px 10px; 
			border-radius: 0 0 10px 10px;
			color:#333;
        }

        .box-gray-green{
			background-repeat: no-repeat;
			background-size: 100% 100%;
            background-image: url('<?php echo base_url('assets/img_v1.1/bg-green-img.jpg');?>');
        }
		
        .box-gray{
			background: linear-gradient(to bottom, rgba(0,0,0,0.7), rgba(100, 100, 100,0.45));			
        }
        .box-green1{
            background: #009966;
			-moz-border-radius: 10px 10px 10px 10px; 
			-webkit-border-radius: 10px 10px 10px 10px; 
			border-radius: 10px 10px 10px 10px;
        }
		
		.box-green-grad{
			background: linear-gradient(to bottom, rgba(255,255,255,0.8), rgba(0,0,0,0.9));			
		}

        .box-white {
            background: rgba(255, 255, 255, 0.85);
            -moz-border-radius: 10px 10px 10px 10px; 
			-webkit-border-radius: 10px 10px 10px 10px; 
			border-radius: 10px 10px 10px 10px;
			border:1px solid #ddd;
			color:#333;
        }

		
		.bg-gray3{
			background-color:transparent;			
		}
		.no-border{
			border:0 !important;
		}
		.hr_black{
			background-color:#666; border-color:#666;
		}	
		.hr_white{
			background-color:#ccc; border-color:#ddd;
		}	
		.hr_green{
			background-color:#ccc; border-color:#074;
		}	
		.navbar-brand{
			margin-right:20px;
		}
		
		
		ul.nav li a{
			height:60px !important; line-height:30px;
			background-color:#f8f8f8;
		}

		ul.nav li:hover{
			background-color:#e7e7e7;
		}
		
		.page-footer{
            background: rgba(30, 30, 30, 0.6);
			padding:7px;
			color:#fff;
		}

		.copyleft{
		  position:relative;
		  top:-2px;
		  display:inline-block;
		  transform: rotate(180deg);
		}

.buorg {
    background-color: rgba(30,30,30,0.8) !important;   
	color:#fff !important;
	height:100%;
	padding-top:200px;
	font-size:1.5em !important;
}		
a#buorgul{
    background-color: #293 !important;   
}

.link_banner{
	color:#0a7; 
	font-style:italic;
}
.link_banner:hover{
	color:#0a7; 
	text-decoration:underline;
}

		
    </style>

</head>
<body>
   <div class="navbar navbar-default navbar-fixed-top" role="navigation" style="height:60px;">
      <div class="container text-center">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" style="height:70px; padding:5px;">
                <img src="<?php echo base_url(); ?>assets/img_v1.1/SIIPP_logo_white.png" alt="<?php echo $this->session->userdata('name')?> " style="height:85% !important;">
		  </a>
        </div>
        <div class="navbar-collapse collapse pull-right">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo site_url();?>">
				<i class="glyphicon glyphicon-home"></i>
				Inicio
			</a></li>
            <li><a href="#descripcion_siipp" title="Descripción del <?php echo $this->session->userdata('name')?>">
				<i class="glyphicon glyphicon-th"></i>
				¿Qué es el <?php echo $this->session->userdata('name')?>?
			</a></li>
            <li><a href="#descripcion_ogob" title="Ingresar a los reportes de Control Social">
				<i class="glyphicon glyphicon-share"></i>
				Gobierno abierto
			</a></li>
            <li><a href="#asistencia_siipp" title="Contactos y asistencia técnica">
				<i class="glyphicon glyphicon-question-sign"></i>
				Asistencia T&eacute;cnica
			</a></li>
            <li><a href="#ingresar" title="Ingresar al <?php echo $this->session->userdata('name')?>" data-toggle="modal" data-target="#login_siipp">
				<i class="glyphicon glyphicon-user"></i>
				Ingresar
			</a></li>


			</ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

<!-- Modal Control Social -->
<div class="modal fade" id="ctrl_social" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <img src="<?php echo base_url(); ?>assets/img_v1.1/SIIPP_logo_white.png" alt="<?php echo $this->session->userdata('name')?>" style="width:80%;">
      </div>
      <div class="modal-body lead">
        <h3 class="modal-title text-success" style="font-size:1.5em;" id="myModalLabel">
            DATOS ABIERTOS DEL C.I.A.T.
		</h3>
			<small>Consulte datos sobre proyectos y programas que el Centro de Investigación Agrícola Tropical tiene para la población.</small>
			<br>
			<hr class="hr_white">
            <form method="post" class="login-form" action="<?php echo base_url(); ?>index.php/admin/validate_invitado">
                <div class="form-group">
				<small>Los reportes se generarán a: </small>
				<br><br>
				<div class="row">
					<div class="col-md-3 col-md-offset-2">
                                <select class="form-control" id="mes" name="mes" title="Seleccione Mes" style="width:100%;">
                                    <option value="0">Seleccione un mes</option>
                                    <option value="1" <?php if(date("m") == 1) echo 'selected';?>>Enero</option>   
                                    <option value="2" <?php if(date("m") == 2) echo 'selected';?>>Febrero</option>
                                    <option value="3" <?php if(date("m") == 3) echo 'selected';?>>Marzo</option>   
                                    <option value="4" <?php if(date("m") == 4) echo 'selected';?>>Abril</option>   
                                    <option value="5" <?php if(date("m") == 5) echo 'selected';?>>Mayo</option>   
                                    <option value="6" <?php if(date("m") == 6) echo 'selected';?>>Junio</option>   
                                    <option value="7" <?php if(date("m") == 7) echo 'selected';?>>Julio</option>   
                                    <option value="8" <?php if(date("m") == 8) echo 'selected';?>>Agosto</option>
                                    <option value="9" <?php if(date("m") == 9) echo 'selected';?>>Septiembre</option>   
                                    <option value="10" <?php if(date("m") == 10) echo 'selected';?>>Octubre</option>   
                                    <option value="11" <?php if(date("m") == 11) echo 'selected';?>>Noviembre</option>
                                    <option value="12" <?php if(date("m") == 12) echo 'selected';?>>Diciembre</option>         
                                </select>
					</div>	
					<div class="col-md-2">
					<small>del</small>
					</div>	
					<div class="col-md-3">
                                <select class="form-control" id="gestion" name="gestion" title="Seleccione Gestion" style="width:100%;">
                                    <option value="0">Seleccione la Gesti&oacute;n</option>
                                    <option value="2017" <?php if(date("Y") == 2017) echo 'selected';?>>2017</option>   
                                    <option value="2018" <?php if(date("Y") == 2018) echo 'selected';?>>2018</option>
								</select>	
					</div>	
				</div>	
			<hr class="hr_white">
                    <p class="login button"> 
                        <button type="submit" class="btn btn-success btn-lg" id="control" style="width:100%;" title="INGRESAR A CONTROL SOCIAL">INGRESAR</button>
						<br>
                    </p>
						<small>Para cerrar este di&aacute;logo, presione la tecla ESC.</small>
                </div>
            </form>
      </div>
    </div>
  </div>
</div>

<br>
<!-- Modal Mailer -->
<div class="modal fade" id="mail_siipp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <img src="<?php echo base_url(); ?>assets/img_v1.1/SIIPP_logo_white.png" alt="<?php echo $this->session->userdata('name')?>" style="width:70%;">
      </div>
      <div class="modal-body">
        <h3 class="modal-title text-success" style="font-size:2em;" id="myModalLabel">
            ENV&Iacute;ANOS TU E-MAIL 
		</h3>
		<div class="text-justify" style="line-height:22px; margin-bottom:7px;">
			<ul>
				<li>
					Env&iacute;a tus consultas sobre renovación de contraseñas, reporte de 
					errores, o cualquier duda relacionada al funcionamiento del <?php echo $this->session->userdata('name')?>.
				</li>
				<li>
					Escriba sus datos en el siguiente formulario y env&iacute;e su consulta,
					el equipo de asistencia t&eacute;cnica responder&aacute; lo antes posible.
				</li>
				<li>
					Todos los campos en el formulario son requeridos.
				</li>
			</ul>
		</div>
			<?php 
				$attributes = array('class' => 'email', 'id' => 'siipp_mailer');
				echo form_open('sendmail', $attributes);
			?>
				<div class="row">
					<div class="col-md-6">
					  <div class="form-group">
						<input id="nombre" name="nombre" style="border:1px solid #ccc !important;" type="text" class="form-control" placeholder="Nombre y apellido">
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
						<input id="referencia" name="referencia" style="border:1px solid #ccc !important;" type="text" class="form-control" placeholder="Celular o email">
					  </div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
					  <div class="form-group">
						<input id="unidad" name="unidad" style="border:1px solid #ccc !important;" type="text" class="form-control" placeholder="Lugar de trabajo: p.ej. SEDCAM">
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
						<select id="asunto" name="asunto" style="border:1px solid #ccc !important; padding:15px !important; height:50px; font-size:16px;" class="form-control">
							<option value="Consulta abierta">Seleccione un Asunto</option>
							<option value="Renovacion password <?php echo $this->session->userdata('name')?>">Renovar mi contrase&ntilde;a</option>
							<option value="Consulta funcionamiento <?php echo $this->session->userdata('name')?>">Sobre el funcionamiento del <?php echo $this->session->userdata('name')?></option>
							<option value="Reporte error <?php echo $this->session->userdata('name')?>">Reportar un error del <?php echo $this->session->userdata('name')?></option>
						</select>
					  </div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
					  <div class="form-group">
						<textarea id="detalle" name="detalle" style="border:1px solid #ccc !important;" class="form-control" rows="9" placeholder="Escriba el detalle de su consulta"></textarea>				
					  </div>
					</div>
				</div>
				<div class="row text-center">
					<div class="col-md-6 col-md-offset-3">
						<div class="g-recaptcha" data-sitekey="6LdhhlMUAAAAANlrUVHCekcM2oMUH1QV_71--WdT"></div>
					</div>
				</div>
				<div class="row text-center">
					<div class="col-md-12">
					  <button type="submit" class="btn btn-primary btn-lg" style="margin-top:7px;">
						<i class="glyphicon glyphicon-envelope"></i>
						 Enviar e-mail
					  </button>
					</div>
				</div>
			</form>
      </div>
    </div>
  </div>
</div>
<!-- end mailer -->

<!-- Modal Login -->
<div class="modal fade" id="login_siipp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <img src="<?php echo base_url(); ?>assets/img_v1.1/SIIPP_logo_white.png" alt="<?php echo $this->session->userdata('name')?>" style="width:70%;">
      </div>
      <div class="modal-body">
		<h3 class="modal-title text-success" style="font-size:2em;" id="myModalLabel">
            INICIAR SESIÓN
		</h3>
		<div class="lead">
		Este formulario permite a fucionarios del CIAT acceder a sus cuentas de usuario y trabajar con el <?php echo $this->session->userdata('name')?>.
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
                        <form id="form_ingreso" role="form" action="<?php echo base_url(); ?>index.php/admin/validate" method="post" class="login-form">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="user_name" placeholder="Nombre de usuario" class="form-username form-control" id="usu" required autofocus>
                                    <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" id="password" name="password" placeholder="Contraseña" class="form-password form-control" required>
                                    <div class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                                </div>
                            </div>
							
                            <button type="submit" class="btn btn-success btn-block btn-lg">
								<i class="glyphicon glyphicon-share-alt"></i>
								Ingresar al <?php echo $this->session->userdata('name')?>
							</button>
                            <a href="#" class="btn btn-sm btn-default" style="margin-top:7px;" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#mail_siipp"> 
								<i class="glyphicon glyphicon-question-sign"></i>
								¿ No recuerdas tu contrase&ntilde;a?</a>

                        </form>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
<!-- end login -->


<div class="row box-gray-green">

		<div class="row form-box box-gray">
				<div class="col-md-12">

							<?php if($this->session->flashdata('error')!=""): ?>
							
							<p class="alert alert-danger msg_alert" style="margin-top:25px;">
                                <?php echo $this->session->flashdata('error'); ?>
							</p>
							<?php endif; ?>
				
				
					<?php if($this->session->flashdata('mensaje_mail') != ''):?>
					<br>
					<div id="mensaje_mail" class="alert alert-warning alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <strong>Mensaje: </strong> <?php echo $this->session->flashdata('mensaje_mail');?>.
					</div>
					<?php endif;?>
					<div class="row">
						<div class="col-sm-12">
							<br>
							<img src="<?php echo base_url(); ?>assets/img_v1.1/head_gobs.png" alt="Centro de Investigación Agrícola Tropical" style="width:90% !important;">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-8">
							<h2 style="color:white;">Bienvenido al <?php echo $this->session->userdata('name')?></h2>
							<hr class="hr_black">
							<div class="lead text-justify">
							<ul style="color:white;">
								<li>
									Para trabajar con el <?php echo $this->session->userdata('name')?>, debe solicitar la creación de su <i><b>cuenta de usuario</b></i> (en caso de no contar con una)
									al administrador del sistema del Centro de Investigación Agrícola Tropical.
								</li>
								<li>
									Para la asignaci&oacute;n de responsabilidad sobre proyectos o programas dentro del <?php echo $this->session->userdata('name')?>,
									<a class="link_banner" href="#asistencia_siipp" title="Click aqui para conocer al sectorialista correspondiente">
									contactarse con el sectorialista correspondiente.
									</a>
								</li>
								<li>
									Para acceder a datos destinados al control social, haga click 
									<a class="link_banner" href="#" title="INGRESO A DATOS ABIERTOS" data-toggle="modal" data-target="#ctrl_social">
									aquí.	
									</a>
								</li>
							</ul>
							</div>
						</div>
						<div class="col-sm-4">
                        <div class="text-center">
                            <h2 style="color:white;" id="ingresar">Iniciar sesi&oacute;n</h2>
						<hr class="hr_black">
                        </div>
                        <form id="form_ingreso" role="form" action="<?php echo base_url(); ?>index.php/admin/validate" method="post" class="login-form">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="user_name" placeholder="Nombre de usuario" class="form-username form-control" id="usu" required autofocus>
                                    <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" id="password" name="password" placeholder="Contraseña" class="form-password form-control" required>
                                    <div class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                                </div>
                            </div>
							
                            <button type="submit" class="btn btn-success btn-block btn-lg">
								<i class="glyphicon glyphicon-share-alt"></i>
								Ingresar al <?php echo $this->session->userdata('name')?>
							</button>
                            <a href="#" class="btn btn-sm btn-default" style="margin-top:7px;" data-toggle="modal" data-target="#mail_siipp"> 
								<i class="glyphicon glyphicon-question-sign"></i>
								¿ No recuerdas tu contrase&ntilde;a?</a>
							<br>
							<br>

                        </form>
						</div>
					</div>
				</div>
            </div>


</div>



<div class="top-content">
    <div class="inner-bg">
        <div class="container">
<!--
		<div class="row form-box box-green">
				<div class="col-md-12">

							<?php if($this->session->flashdata('error')!=""): ?>
							
							<p class="alert alert-danger msg_alert" style="margin-top:25px;">
                                <?php echo $this->session->flashdata('error'); ?>
							</p>
							<?php endif; ?>
				
				
					<?php if($this->session->flashdata('mensaje_mail') != ''):?>
					<br>
					<div id="mensaje_mail" class="alert alert-warning alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <strong>Mensaje: </strong> <?php echo $this->session->flashdata('mensaje_mail');?>.
					</div>
					<?php endif;?>
					<div class="row">
						<div class="col-sm-12">
							<br>
							<img src="<?php echo base_url(); ?>assets/img_v1.1/head_gobs.png" alt="Centro de Investigación Agrícola Tropical" style="width:90% !important;">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-8">
							<h2 style="color:white;">Bienvenido al <?php echo $this->session->userdata('name')?></h2>
							<hr class="hr_black">
							<div class="lead text-justify">
							<ul style="color:white;">
								<li>
									Para trabajar con el <?php echo $this->session->userdata('name')?>, debe solicitar la creación de su <i><b>cuenta de usuario</b></i> (en caso de no contar con una)
									a la Dirección de Control de Gestión, dependiente de la Secretar&iacute;a Departamental de Planificación del Desarrollo.
								</li>
								<li>
									Para la asignaci&oacute;n de responsabilidad sobre proyectos o programas dentro del <?php echo $this->session->userdata('name')?>,
									<a class="link_banner" href="#asistencia_siipp" title="Click aqui para conocer al sectorialista correspondiente">
									contactarse con el sectorialista correspondiente.
									</a>
								</li>
								<li>
									Para acceder a reportes destinados al control social, haga click 
									<a class="link_banner" href="#" title="INGRESO A CONTROL SOCIAL" data-toggle="modal" data-target="#ctrl_social">
									aquí.	
									</a>
								</li>
							</ul>
							</div>
						</div>
						<div class="col-sm-4">
                        <div class="text-center">
                            <h2 style="color:white;" id="ingresar">Iniciar sesi&oacute;n</h2>
						<hr class="hr_black">
                        </div>
                        <form id="form_ingreso" role="form" action="<?php echo base_url(); ?>index.php/admin/validate" method="post" class="login-form">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="user_name" placeholder="Nombre de usuario" class="form-username form-control" id="usu" required autofocus>
                                    <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" id="password" name="password" placeholder="Contraseña" class="form-password form-control" required>
                                    <div class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                                </div>
                            </div>
							
                            <button type="submit" class="btn btn-success btn-block btn-lg">
								<i class="glyphicon glyphicon-share-alt"></i>
								Ingresar al <?php echo $this->session->userdata('name')?>
							</button>
                            <a href="#" class="btn btn-sm btn-default" style="margin-top:7px;" data-toggle="modal" data-target="#mail_siipp"> 
								<i class="glyphicon glyphicon-question-sign"></i>
								¿ No recuerdas tu contrase&ntilde;a?</a>
							<br>
							<br>

                        </form>
						</div>
					</div>
				</div>
            </div>
-->

			<hr class="hr_white" id="descripcion_siipp">
			
			<div class="row box-white">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6 lead text-justify">
							<h1 class="text-success text-center">
							¿Qu&eacute; es el <?php echo $this->session->userdata('name')?>?
							</h1>
							<hr class="hr_white">
								<img class="pull-left" src="<?php echo base_url(); ?>assets/img_v1.1/siipp-incognito.png" alt="Centro de Investigación Agrícola Tropical" style="width:40%; margin:0 auto;">
								El SISTEMA INTEGRADO DE PLANIFICACIÓN, SEGUIMIENTO Y EVALUACIÓN
								denominado <?php echo $this->session->userdata('name')?>, es el Software del Centro de Investigación Agrícola Tropical
                                que permite gestionar el Plan Operativo Anual y sus respectivos
								proyectos, programas, y actividades de funcionamiento de las unidades
								funcionales.
						</div>
						<div class="col-md-6 lead text-justify">
							<h1 class="text-success text-center">
							¿Para qu&eacute; sirve?
							</h1>
							<hr class="hr_white">
								<img class="pull-left" src="<?php echo base_url(); ?>assets/img_v1.1/beneficios-siipp.png" alt="Centro de Investigación Agrícola Tropical" style="width:40%; margin:0 auto;">
								Sirve para la planificación de proyectos, programas y acciones institucionales, y
								para realizar el seguimiento y control de las operaciones programadas en el
                                Centro de Investigación Agrícola Tropical, con el fin de generar información oportuna y
								confiable para la toma de decisiones.
						</div>
					</div>
				</div>
			</div>

			<hr class="hr_white" id="descripcion_ogob">

<div class="row box-green1">
	<br>
	<div class="col-md-7">
		<div style="font-size:3.0em; line-height:1em; color:white; margin:5px; font-family: 'Overpass', sans-serif;">
		El CIAT hacia el gobierno abierto
		</div>
		<hr class="hr_white">
		
		<div style="font-size:1.8em; line-height:1.2em; color:white; margin:5px; font-family: 'Overpass', sans-serif;">
		<p class="text-justify">
		El Centro de Investigación Agrícola Tropical, con el fin de transparentar sus acciones, le brinda a la ciudadanía información sobre
		los proyectos y programas que se gestionan en la institución a través del SISTEMA INTEGRADO DE PLANIFICACIÓN, SEGUIMIENTO Y EVALUACIÓN.
		</p>
            <a class="btn btn-lg btn-default" href="#" title="Ingresar a los reportes de Control Social" data-toggle="modal" data-target="#ctrl_social">
				<i class="glyphicon glyphicon-stats"></i>
				Consulte los datos abiertos
			</a>
		
		</div>
	</div>
	<div class="col-md-5">
		<img src="<?php echo base_url('assets/img_v1.1/ogov.jpg');?>" style="width:100%;">
	</div>
</div>

			
			<hr class="hr_white">
			<div class="row box-white">
				<h1 class="text-success">
				¿Qué es el Gobierno abierto?
				</h1>
				<hr class="hr_white">
				<div class="col-md-8 text-justify lead">
					<blockquote>
						"Un Gobierno Abierto, es un gobierno que abre sus puertas al mundo, co-innova con todos, 
						especialmente con los ciudadanos; comparte recursos que anteriormente estaban celosamente 
						guardados, aprovecha el poder de la colaboración masiva y la transparencia en todas sus operaciones..."
						<footer>(Don Tapscott, 2010)</footer>		
					</blockquote>
					En este concepto, la <i>transparencia</i> es una de las bases más importantes, la cual se sostiene en el derecho constitucional
					de acceso a la información. 
					<br>
					Uno de los componentes del Gobierno Abierto, son los <b><i>Datos Abiertos</i></b>, los cuales hacen referencia a todos los datos generados
					por el sector publico que no contienen datos personales ni información de seguridad. Estos pueden ser reutilizados y redistribuidos
					por cualquier persona para cualquier proposito, sin cargos y libres de restricciones.
				</div>
				<div class="col-md-4">
                    <img src="<?php echo base_url(); ?>assets/img_v1.1/opengob.png" alt="Centro de Investigación Agrícola Tropical" style="width:100%; margin:0 auto;">
				</div>
			</div>
			<hr class="hr_white" id="descripcion_ogob">
			<div class="row box-white">
				<h1 class="text-success">
				El <?php echo $this->session->userdata('name')?> como herramienta para el Gobierno Abierto
				</h1>
				<hr class="hr_white">
				<div class="col-md-4">
                    <img src="<?php echo base_url(); ?>assets/img_v1.1/siipp_ogob.png" alt="Centro de Investigación Agrícola Tropical" style="width:100%; margin:0 auto;">
				</div>
				<div class="col-md-8 text-justify lead">
					El SISTEMA INTEGRADO DE PLANIFICACIÓN, SEGUIMIENTO Y EVALUACIÓN , tiene como una de sus finalidades <i>transparentar las acciones</i>
					llevadas a cabo por el Centro de Investigación Agrícola Tropical, con la visión de tener un Gobierno Abierto que permita
					mejorar la gestión pública.
					<br>
					Usted puede consultar datos actualizados relacionados a proyectos y programas gestionados por el Centro de Investigación Agrícola Tropical.
					<br>
					<a class="btn btn-lg btn-success" href="#" title="Ingresar a los reportes de Control Social" data-toggle="modal" data-target="#ctrl_social">
						<i class="glyphicon glyphicon-stats"></i>
						Consulte los datos abiertos
					</a>
				</div>
			</div>

			<hr class="hr_white" id="descripcion_ogob">

			<div class="row box-white">
				<h1 class="text-success" id="asistencia_siipp">
				Asistencia Técnica
				</h1>
				<hr class="hr_white">
				<div class="col-md-12 text-justify">
					<div class="lead">
					Contáctate con el personal a cargo de la implementación del <?php echo $this->session->userdata('name')?> a través del <strong>n-nnnnnn</strong> en horario de oficina.
					<br>
					Tambien puedes realizar tus consultas desde
					<a href="#" data-toggle="modal" data-target="#mail_siipp">
					 aquí
					</a>
					enviando un e-mail a 
					<a href="#" data-toggle="modal" data-target="#mail_siipp">
					correo@ciat.gob.bo
					</a>
					</div>
				</div>
			</div>

        </div>
    </div>

</div>

		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<span class="txt-color-white">
						<span class="copyleft">&copy;</span> 2017 - <span id="copy_year"></span> Centro de Investigación Agrícola Tropical - Bolivia
					</span>					
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

<!--================================================== -->
<!-- Validador Recaptcha -->
<script src='https://www.google.com/recaptcha/api.js?hl=es'></script>

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script> if (!window.jQuery) { document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');} </script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script> if (!window.jQuery.ui) { document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');} </script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>


<script type="text/javascript">
    runAllForms();
	//oculta mensaje del robot MAIL
	setTimeout(function(){$('#mensaje_mail').slideUp({duration:2000});}, 3500);
	setTimeout(function(){$('.msg_alert').slideUp({duration:2000});}, 3500);

	//Año actual del Copyright
	$('#copy_year').text((new Date()).getFullYear());        // Validation
		
    $(function() {
		
        $("#login-form").validate({
            // Rules for form validation
            rules : {
                email : {
                    required : true,
                    email : true
                },
                password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                }
            },

            // Messages for form validation
            messages : {
                email : {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
                },
                password : {
                    required : 'Please enter your password'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            }
        });
    });
</script>

</body>
</html>
