<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<link rel="STYLESHEET" href="<?php echo base_url(); ?>assets/print_static.css" type="text/css" />
  <style>
        table{
            font-size: 9px;
            width: 100%;
            background-color:#fff;
        }
  </style>
  <script type="text/javascript">
      function imprimir() {
          if (window.print) {
              window.print();
          } else {
              alert("La función de impresion no esta soportada por su navegador.");
          }
      }
  </script>
</head>
<body onload="imprimir();">

<div id="body">
<div id="section_header">
</div>
  
    <div class="page" style="font-size: 7pt" align="center">
      <table style="width: 90%;" class="header">
          <tr>
              <td width=20%; align="center">
                <img src="<?php echo base_url(); ?>assets/img/logo.jpg" class="img-responsive" alt="Cinque Terre" width="150px"/>
              </td>
              <td width=60%; class="titulo_pdf">
                  <b>ENTIDAD : </b><?php echo $this->session->userdata('entidad');?><br>
                  <b>POA - PLAN OPERATIVO ANUAL : </b><?php echo $this->session->userdata("gestion")?><br>
                  <b><?php echo $this->session->userdata('sistema');?></b><br>
                  <b>REPORTE : </b> <?php echo $title; ?>
                  (Al de <?php echo $dias.' de '.$mes.' de '.$this->session->userdata("gestion");?>) (Expresado en Bolivianos)<br>
              </td>
              <td width=20%; align="center">
                <img src="<?php echo base_url(); ?>assets/img/escudo.jpg" class="img-responsive" alt="Cinque Terre" alt="" width="90px" >
              </td>
          </tr>
      </table>

              <table class="change_order_items" style="width: 80%;" border="1">
                  <thead>
                    <tr>
                      <tr>
                      <th>MUNICIPIO</th>
                      <th>PRESUPUESTO INICIAL</th>
                      <th>MODIFICACIONES</th>
                      <th>PRESUPUESTO VIGENTE</th>
                      <th>TRANSFERENCIAS</th>
                      <th>% EJECUCI&Oacute;N</th>
                      <th>% PART</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php echo $tabla;?>
                  </tbody>
              </table>
              <table style="width: 80%;" class="change_order_items" border="1">
                <tr class="even_row">
                  <th>
                    GR&Aacute;FICO
                  </th>
                </tr>
                <tr>
                  <td>
                    <div id="container" style="width: 600px; height: 350px; margin: 1 auto"></div> 
                  </td>
                </tr>
              </table>
                      
      <div align="center">
        <table style="width: 80%;">
            <tr>    
              <td colspan="3" height="80"><b>FIRMAS</b></td>
            </tr>
            <tr>    
              <td style="width: 33.3%;">RESPONSABLE UNIDAD EJECUTORA</td>
              <td style="width: 33.3%;">ANALISTA POA</td>
              <td style="width: 33.3%;">ANALISTA FINANCIERO</td>
            </tr>
        </table>
      </div>

    </div>
  </div>
</div>
</body>
 <script>
    if (!window.jQuery) {
      document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
    }
    </script>
    <script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
    <script type="text/javascript">
      var chart;
      $(document).ready(function() {
        
        var colors = Highcharts.getOptions().colors,
          categories = [

            'PRESUPUESTO VIGENTE', 'TRANSFERENCIAS'
            ],
          name = 'Nivel de Ejecución',
          data = [{ 
              y: <?php echo $muni[$nro][5];?>,
              color: '#04B404',
            }, {
              y: <?php echo $muni[$nro][6];?>,
              color: '#04B404',
            }];
        
        function setChart(name, categories, data, color) {
          chart.xAxis[0].setCategories(categories);
          chart.series[0].remove();
          chart.addSeries({
            name: name,
            data: data,
            color: color || 'white'
          });
        }
        
        chart = new Highcharts.Chart({
          chart: {
            renderTo: 'container', 
            type: 'column'
          },
          title: {
            text: 'NIVEL DE EJECUCI\u00D3N POR TIPO DE GASTO'
          },
          subtitle: {
            text: '(Al de <?php echo $dias.' de '.$mes.' de '.$this->session->userdata("gestion");?>) (Expresado en % de Participacion)'
          },
          xAxis: {
            categories: categories              
          },
          yAxis: {
            title: {
              text: ''
            }
          },
          plotOptions: {
            column: {
              cursor: 'pointer',
              point: {
                events: {
                  click: function() {
                    var drilldown = this.drilldown;
                    if (drilldown) { // drill down
                      setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                    } else { // restore
                      setChart(name, categories, data);
                    }
                  }
                }
              },
              dataLabels: {
                enabled: true,
                color: colors[0],
                style: {
                  fontWeight: 'bold'
                },
                formatter: function() {
                  return this.y +'%';
                }
              }         
            }
          },
          tooltip: {
            formatter: function() {
              var point = this.point,
                s = this.x +':<b>'+ this.y +'% </b><br/>';
              if (point.drilldown) {
                s += ''+ point.category +' ';
              } else {
                s += '';
              }
              return s;
            }
          },
          series: [{
            name: name,
            data: data,
            color: 'white'
          }],
          exporting: {
            enabled: false
          }
        });
      });
    </script>
</html>