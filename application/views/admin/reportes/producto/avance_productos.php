<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> <?php echo $this->session->userdata('name')?> </title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">


		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		    <!--para las alertas-->
 			<!--para las alertas-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
	    <meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->

	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->

				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->

				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->

				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->

			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="REPORTES"> <span class="menu-item-parent">REPORTES</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>REportes</li><li>Seguimiento</li><li>Prog. y Ejec. del Producto de las Acciones a Nivel Insitucional</li>
				</ol>
			</div>

			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">

						<div class="alert alert-block alert-success" >
							<a class="close" data-dismiss="alert" href="#">×</a>
							<h4 class="alert-heading"><i class="glyphicon glyphicon-user"></i><?php echo " FUNCIONARIO RESPONSABLE : ".$this->session->userdata("funcionario")."   ||  "; ?><i class="glyphicon glyphicon-calendar"></i><?php echo "FECHA ACTUAL : " . date("d") . " del " . date("m") . " de " . date("Y");?></h4>
						</div>
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
				            <section id="widget-grid" class="well">
				              <div align="center">
				                <h1><b> <?php echo $this->session->userdata('entidad')?>  </b><br><small>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N DEL PRODUCTO DE LAS ACCIONES A NIVEL INSTITUCIONAL<br> AL MES DE <?php echo $mes;?>  de <?php echo $this->session->userdata("gestion");?></small></h1>
				              </div>
				            </section>
				        </article>
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
				           <section id="widget-grid" class="well">
								<center><a href="<?php echo base_url().'index.php/admin/rep/list_productos'?>" title="MIS PRODUCTOS  A NIVEL PROGRAMAS " class="btn btn-lg btn-success"><span class="glyphicon glyphicon-stats"></span> PRODUCTOS DE LA ACCI&Oacute;N<br>A NIVEL DE PROGRAMAS</a></center>
                            </section>
                        </article>

						<?php $nro=1; $a=1;
						$suma_prog=0;
						for($i=1;$i<=12;$i++){$prog_p[$i]=0; $prog_e[$i]=0;$prog_efi[$i]=0;$menor[$i]=0;$entre[$i]=0;$mayor[$i]=0;}
				        foreach($lista_aper_padres  as $rowa)
				        { 	
				        	$programa=$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'];
				        	$proyecto=$this->model_proyecto->programas_proyecto($rowa['aper_programa'],$this->session->userdata("gestion"));
				        	for($i=1;$i<=12;$i++){$proy_p[$i]=0; $proy_e[$i]=0;$proy_efi[$i]=0;}
				            foreach($proyecto  as $proy)
				            {
				            	$nro_f=$this->model_faseetapa->nro_fase($proy['proy_id']);
				            	/*======================= VERIF FASE ===================*/
				            	if($nro_f>0)
				            	{
				            	$fase = $this->model_faseetapa->get_id_fase($proy['proy_id']);
					            $componentes = $this->model_componente->componentes_id($fase[0]['id']);
					            for($i=1;$i<=12;$i++){$com_p[$i]=0; $com_e[$i]=0;}
					            /*======================= COMPONENTE ====================*/
					            foreach ($componentes as $rowc)
					            {
					              $productos = $this->model_producto->list_prod($rowc['com_id']);
					              for($i=1;$i<=12;$i++){$prod_p[$i]=0; $prod_e[$i]=0;}
					              /*======================== PRODUCTO===========================*/
					              foreach ($productos as $rowp)
					              { 
					                $meta_gest=$this->model_producto->meta_prod_gest($rowp['prod_id']);
					                if($meta_gest[0]['meta_gest']==''){$meta_gest[0]['meta_gest']='0';}
					                $programado = $this->model_producto->list_prodgest_anual($rowp['prod_id']);
                          			$ejecutado=$this->model_producto->prod_ejec_mensual($rowp['prod_id'],$this->session->userdata("gestion")); /// ejecutado
                          			/*============================= PROCESO PRODUCTO =========================*/
					              	/*================= matriz ==========*/   
					              	for($j = 1; $j<=12; $j++)
                                      {
                                        $matriz_r[1][$j]=$j;
                                        $matriz_r[2][$j]='0';
                                        $matriz_r[3][$j]='0';
                                        $matriz_r[4][$j]='0';
                                        $matriz_r[5][$j]='0';
                                        $matriz_r[6][$j]='0';
                                        $matriz_r[7][$j]='0';
                                        $matriz_r[8][$j]='0';
                                        $matriz_r[9][$j]='0';
                                        $matriz_r[10][$j]='0';
                                      }
                                      /*==================================*/ 
                                      /*========== Programado ============*/
                                      $nro=0;
                                      foreach($programado as $row)
                                      {
                                        $nro++;
                                        $matriz [1][$nro]=$row['m_id']; 
                                        $matriz [2][$nro]=$row['pg_fis'];
                                      }
                                	  /*==================================*/ 
                                	  /*=========== Ejecutado =============*/
                                      $nro_e=0;
                                      foreach($ejecutado as $row)
                                      {
                                          $nro_e++;
                                          $matriz_e [1][$nro_e]=$row['m_id'];
                                          $matriz_e [2][$nro_e]=$row['pejec_fis'];
                                          $matriz_e [3][$nro_e]=$row['pejec_fis_a'];
                                          $matriz_e [4][$nro_e]=$row['pejec_fis_b'];
                                      }
                                	   /*==================================*/
                                	   /*------- asignando en la matriz P, PA, %PA ----------*/
                                      for($i = 1 ;$i<=$nro ;$i++)
                                      {
                                        for($j = 1 ;$j<=12 ;$j++)
                                        {
                                          if($matriz[1][$i]==$matriz_r[1][$j])
                                          {
                                            $matriz_r[2][$j]=round($matriz[2][$i],1);
                                          }
                                        }
                                      }

                                      $pa=0;        
                                      for($j = 1 ;$j<=12 ;$j++){
                                        $pa=$pa+$matriz_r[2][$j];
                                        $matriz_r[3][$j]=$pa+$rowp['prod_linea_base'];
                                        if($meta_gest[0]['meta_gest']==0)
                                        {
                                        	$matriz_r[4][$j]=round((($pa+$rowp['prod_linea_base'])*100),1);
                                        }
                                        else
                                        {
                                        	$matriz_r[4][$j]=round(((($pa+$rowp['prod_linea_base'])/$meta_gest[0]['meta_gest'])*100),1);	
                                        }
                                        
                                      }

                                      if($rowp['indi_id']==1)
                                      {
                                        for($i = 1 ;$i<=$nro_e ;$i++){
                                            for($j = 1 ;$j<=12 ;$j++)
                                            {
                                                if($matriz_e[1][$i]==$matriz_r[1][$j])
                                                {
                                                    $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                                                }
                                            }
                                        }
                                      }
                                      elseif ($rowp['indi_id']==2) 
                                      {
                                        if($rowp['prod_denominador']==0)
                                        {
                                          for($i = 1 ;$i<=$nro_e ;$i++){
                                              for($j = 1 ;$j<=12 ;$j++)
                                              {
                                                  if($matriz_e[1][$i]==$matriz_r[1][$j])
                                                  {
                                                      $matriz_r[5][$j]=round($matriz_e[3][$i],1);
                                                      $matriz_r[6][$j]=round($matriz_e[4][$i],1);
                                                      $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                                                  }
                                              }
                                          }
                                        }
                                        if ($rowp['prod_denominador']==1)
                                        {
                                          for($i = 1 ;$i<=$nro_e ;$i++){
                                              for($j = 1 ;$j<=12 ;$j++)
                                              {
                                                  if($matriz_e[1][$i]==$matriz_r[1][$j])
                                                  {
                                                      $matriz_r[5][$j]=round($matriz_e[3][$i],1);
                                                      $matriz_r[6][$j]=round($matriz_e[4][$i],1);
                                                      $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                                                  }
                                              }
                                          }
                                        }
                                      /*--------------------------------------------------------*/
                                      }
                                      /*--------------------matriz E,AE,%AE gestion ------------------*/
                                      $pe=0; 
                                      for($j = 1 ;$j<=12 ;$j++){
                                        $pe=$pe+$matriz_r[7][$j];
                                        $matriz_r[8][$j]=$pe+$rowp['prod_linea_base'];
                                        if($meta_gest[0]['meta_gest']==0)
                                        {
                                        	$matriz_r[9][$j]=round((($pe+$rowp['prod_linea_base'])*100),1);
                                        }
                                        else
                                        {
                                        	$matriz_r[9][$j]=round(((($pe+$rowp['prod_linea_base'])/$meta_gest[0]['meta_gest'])*100),1);
                                        }
                                        
                                        if($matriz_r[4][$j]==0)
                                        {$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),1);}
                                        else
                                        {
                                          $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),1);
                                        }

                                     }
                                    /*============================= END PROCESO PRODUCTO =========================*/
                          			/*------------------------ sumatoria a nivel producto -------------------------------*/
                          			for($j = 1 ;$j<=12 ;$j++)
                                     {
                                     	$prod_p[$j]=round($prod_p[$j]+(($matriz_r[4][$j]*$rowp['prod_ponderacion'])/100),2);
                                     	$prod_e[$j]=round($prod_e[$j]+(($matriz_r[9][$j]*$rowp['prod_ponderacion'])/100),2);
                                     }
                          			/*-------------------------------------------------------------------------------------*/
					              }
					              /*===================END PRODUCTO ===================*/
					              	/*------------------------ sumatoria a nivel componente  -------------------------------*/
                                     for($j = 1 ;$j<=12 ;$j++)
                                     {
                                     	$com_p[$j]=round($com_p[$j]+(($prod_p[$j]*$rowc['com_ponderacion'])/100),2);
                                     	$com_e[$j]=round($com_e[$j]+(($prod_e[$j]*$rowc['com_ponderacion'])/100),2);
                                     }
                                    /*-------------------------------------------------------------------------------------*/
					             
					            }
								/*------------------------ sumatoria a nivel proyecto  -------------------------------*/
					            for($j = 1 ;$j<=12 ;$j++)
                                    {
                                     	$proy_p[$j]=round($proy_p[$j]+(($com_p[$j]*$proy['proy_ponderacion'])/100),2); //// proyectos programado
                                     	$proy_e[$j]=round($proy_e[$j]+(($com_e[$j]*$proy['proy_ponderacion'])/100),2); //// proyectos ejecutado
                                    }
                                /*-------------------------------------------------------------------------------------*/
					          }
					        	/*======================= END VERIF FASE ===================*/
					        }
					        for($j = 1 ;$j<=12 ;$j++)
                            {
                            	if($proy_p[$j]==0)
                            	$proy_efi[$j]=round($proy_e[$j],1);
                            	else
                            	$proy_efi[$j]=round((($proy_e[$j]/$proy_p[$j])*100),1);
							}
				        /*------------------------ sumatoria a nivel programa  -------------------------------*/
							for($j = 1 ;$j<=12 ;$j++)
                                {
                                    $prog_p[$j]=$prog_p[$j]+(($proy_p[$j]*$rowa['aper_ponderacion'])/100); /// Programa Programado
                                	$prog_e[$j]=$prog_e[$j]+(($proy_e[$j]*$rowa['aper_ponderacion'])/100); //// Programa Ejecutado
                                }
                        /*-------------------------------------------------------------------------------------*/
				        }
				        for($j = 1 ;$j<=12 ;$j++)
                            {
                            	if($prog_p[$j]==0)
                            	$prog_efi[$j]=round($prog_e[$j],1);
                            	else
                            	$prog_efi[$j]=round((($prog_e[$j]/$prog_p[$j])*100),1);
							}

						for($j=1;$j<=12;$j++)
						{
							if($prog_efi[$j] <=75){$menor[$j]=$prog_efi[$j];}
							if($prog_efi[$j] >= 76 && $prog_efi[$j] <= 90.9){$entre[$j]=$prog_efi[$j];}
							if($prog_efi[$j] >= 91){$mayor[$j]=$prog_efi[$j];}
						}
				    ?>
						<div class="row">
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false">
								<header>
									<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
									<h2>Programaci&oacute;n, Ejecuci&oacute;n a nivel Institucional</h2>
								</header>

								<!-- widget div-->
								<div>
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
									</div>
									<!-- end widget edit box -->
									<!-- widget content -->
									<div class="widget-body no-padding">
										<div class="col-md-12 col-sm-12 col-xs-12" id="graf" class="chart"></div>
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
						</article>
						<!-- WIDGET END -->

						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false">
								<header>
									<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
									<h2>Eficacia a nivel Instituci&oacute;n</h2>
								</header>
								<!-- widget div-->
								<div>
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
									</div>
									<!-- end widget edit box -->
									<!-- widget content -->
									<div class="widget-body no-padding">
										<div class="col-md-12 col-sm-12 col-xs-12" id="graf_eficacia" class="chart"></div>
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
						</article>
						<!-- WIDGET END -->

					
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
								<header>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>Seguimiento de Producto de las Acciones</h2>
								</header>
								<div>
									<div class="jarviswidget-editbox">
									</div>
									<!-- end widget edit box -->
									<!-- widget content -->
									<div class="widget-body">
										<p>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N DE LOS PRODUCTOS DE LA ACCI&Oacute;N</p>
										
										<div class="table-responsive">
										
											<table class="table table-bordered">
												<thead>
													<tr>
														<th></th>
														<th>Ene.</th>
														<th>Feb.</th>
														<th>Mar.</th>
														<th>Abr.</th>
														<th>May.</th>
														<th>Jun.</th>
														<th>Jul.</th>
														<th>Ago.</th>
														<th>Sep.</th>
														<th>Oct.</th>
														<th>Nov.</th>
														<th>Dic.</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td> % PROGRAMADO</td>
														<?php
														for($i=1;$i<=12;$i++)
														{
															echo "<td>".$prog_p[$i]." %</td>";
														}
														?>
													</tr>
													<tr>
														<td> % EJECUTADO</td>
														<?php
														for($i=1;$i<=12;$i++)
														{
															echo "<td>".$prog_e[$i]." %</td>";
														}
														?>
													</tr>
													<tr>
														<td>EFICACIA</td>
														<?php
														for($i=1;$i<=12;$i++)
														{
															echo "<td>".$prog_efi[$i]." %</td>";
														}
														?>
													</tr>
													
												</tbody>
											</table>
											
										</div>
									</div>
									<!-- end widget content -->
								</div>
								<div class="form-actions">
									<a href="<?php echo base_url().'index.php/admin/rep/at_acciones'?>" title="MIS ACCIONES" class="btn btn-lg btn-default">ATRAS</a>
                                </div>
							</div>

						</article>
				</div>
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
	<!-- ========================================================================================================= -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userdata('name')?> <?php echo $this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>

		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>

		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>

		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>

		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>

		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
    	<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
    	<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-3d.js"></script>
    	<script src="<?php echo base_url(); ?>assets/highcharts/js/modules/exporting.js"></script>
	    <script type="text/javascript">
	    //////////////////////////////////////////uno//////////////////

	    var chart1 = new Highcharts.Chart({
	        chart: {
	            renderTo: 'graf', // div contenedor
	            type: 'line' // tipo de grafico
	        },

	        title: {
	            text: 'PROGRAMACI\u00D3N Y EJECUCI\u00D3N F\u00CDSICA DE LOS PRODUCTOS DE LA ACCI\u00D3N', // t�tulo  del gr�fico
	            x: -20 //center
	        },
	        subtitle: {
	            text: ''
	        },
	        xAxis: {
	            categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
	        },
	        yAxis: {
	            title: {
	                text: 'PORCENTAJES (%)'
	            }
	        },
	        plotOptions: {
	            line: {
	                dataLabels: {
	                    enabled: true
	                },
	                enableMouseTracking: false
	            }
	        },
	        series: [
	            {
	                name: 'PROGRAMACIÓN ACUMULADA EN %',
	                data: [<?php echo $prog_p[1] ?>,<?php echo $prog_p[2]?>,<?php echo $prog_p[3]?>,<?php echo $prog_p[4]?>,<?php echo $prog_p[5]?>,<?php echo $prog_p[6]?>,<?php echo $prog_p[7]?>,<?php echo $prog_p[8]?>,<?php echo $prog_p[9]?>,<?php echo $prog_p[10]?>,<?php echo $prog_p[11]?>,<?php echo $prog_p[12]?>]
	            },
	            {
	                name: 'EJECUCIÓN ACUMULADA EN %',
	                data: [<?php echo $prog_e[1] ?>,<?php echo $prog_e[2]?>,<?php echo $prog_e[3]?>,<?php echo $prog_e[4]?>,<?php echo $prog_e[5]?>,<?php echo $prog_e[6]?>,<?php echo $prog_e[7]?>,<?php echo $prog_e[8]?>,<?php echo $prog_e[9]?>,<?php echo $prog_e[10]?>,<?php echo $prog_e[11]?>,<?php echo $prog_e[12]?>]
	            }
	        ]
	    });
	</script>
	<script type="text/javascript">
	    $(function () {
	        Highcharts.chart('graf_eficacia', {
	            chart: {
	                type: 'column'
	            },
	            title: {
	                text: 'EFICACIA'
	            },
	            xAxis: {
	                categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
	            },
	            yAxis: {
	                min: 0,
	                title: {
	                    text: 'PORCENTAJES (%)'
	                },
	                stackLabels: {
	                    enabled: true,
	                    style: {
	                        fontWeight: 'bold',
	                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
	                    }
	                }
	            },
	            legend: {
	                align: 'right',
	                x: -30,
	                verticalAlign: 'top',
	                y: 25,
	                floating: true,
	                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
	                borderColor: '#CCC',
	                borderWidth: 1,
	                shadow: false
	            },
	            tooltip: {
	                headerFormat: '<b>{point.x}</b><br/>',
	                pointFormat: '{series.name}: <br/> TOTAL:   {point.y}'
	            },
	            plotOptions: {
	                column: {
	                    stacking: 'normal',
	                    dataLabels: {
	                        enabled: false,
	                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
	                    }
	                }
	            },
	            series: [{

	                name: '<b style="color: #FF0000;">MENOR A 75%</b>',
	                data: [{y: <?php echo $menor[1]?>, color: 'red'},{y: <?php echo $menor[2]?>, color: 'red'},{y: <?php echo $menor[3]?>, color: 'red'},{y: <?php echo $menor[4]?>, color: 'red'},{y: <?php echo $menor[5]?>, color: 'red'},{y: <?php echo $menor[6]?>, color: 'red'},{y: <?php echo $menor[7]?>, color: 'red'},{y: <?php echo $menor[8]?>, color: 'red'},{y: <?php echo $menor[9]?>, color: 'red'},{y: <?php echo $menor[10]?>, color: 'red'},{y: <?php echo $menor[11]?>, color: 'red'},{y: <?php echo $menor[12]?>, color: 'red'}] 

	            }, {
	                name: '<b style="color: #d6d21f;">ENTRE 76% Y 90%</b>',
	                data: [{y: <?php echo $entre[1]?>, color: 'yellow'},{y: <?php echo $entre[2]?>, color: 'yellow'},{y: <?php echo $entre[3]?>, color: 'yellow'},{y: <?php echo $entre[4]?>, color: 'yellow'},{y: <?php echo $entre[5]?>, color: 'yellow'},{y: <?php echo $entre[6]?>, color: 'yellow'},{y: <?php echo $entre[7]?>, color: 'yellow'},{y: <?php echo $entre[8]?>, color: 'yellow'},{y: <?php echo $entre[9]?>, color: 'yellow'},{y: <?php echo $entre[10]?>, color: 'yellow'},{y: <?php echo $entre[11]?>, color: 'yellow'},{y: <?php echo $entre[12]?>, color: 'yellow'}] 
	            }, {
	                name: '<b style="color: green;">MAYOR A 91%</b>',
	                data: [{y: <?php echo $mayor[1]?>, color: 'green'},{y: <?php echo $mayor[2]?>, color: 'green'},{y: <?php echo $mayor[3]?>, color: 'green'},{y: <?php echo $mayor[4]?>, color: 'green'},{y: <?php echo $mayor[5]?>, color: 'green'},{y: <?php echo $mayor[6]?>, color: 'green'},{y: <?php echo $mayor[7]?>, color: 'green'},{y: <?php echo $mayor[8]?>, color: 'green'},{y: <?php echo $mayor[9]?>, color: 'green'},{y: <?php echo $mayor[10]?>, color: 'green'},{y: <?php echo $mayor[11]?>, color: 'green'},{y: <?php echo $mayor[12]?>, color: 'green'}] 
	            }]
	        });
	    });
	</script>
	</body>
</html>
