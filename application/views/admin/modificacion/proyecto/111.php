<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		    <!--para las alertas-->
 
    	<meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
          	<style>
			/*////scroll tablas/////*/
			table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
			    overflow-x: scroll;
            }
			</style>
			<?php
				$v1=$this->uri->segment(5);
				$v2=$this->uri->segment(6);
				$v3=$this->uri->segment(7);
				$id=$this->uri->segment(8);
			?>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="PROGRAMACION"> <span class="menu-item-parent">MODIFICACIONES</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Modificaciones</li><li>Modificaciones del POA</li><li>Modificaciones de la Operaci&oacute;n</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
			<div class="row">
				<!-- widget grid -->
			<article class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
				<section id="widget-grid" class="well">
					<div class="">
						<h1> CATEGORIA PROGRAM&Aacute;TICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad']?></small>
						<h1> <?php echo $titulo_proy;?> : <small><?php echo $proyecto[0]['proy_nombre']?></small></h1>
					</div>
				</section>
			</article>
			<article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<section id="widget-grid" class="well">
					<center><a href="<?php echo base_url().'index.php/admin/mod/proyecto/'.$proyecto[0]['proy_id'].'' ?>" class="btn btn-success" style="width:100%;" title="Volver atras">VOLVER ATRAS</a></center>
				</section>
			</article>

			<?php 
			if($v1==1)
			{
				?>
				<article class="col-sm-12 col-md-12 col-lg-12">
					<div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
								<h2>METAS</h2>
						</header>
						<div>
									<div class="widget-body">
										<?php 
										$componente = $this->model_componente->componentes_id($fase[0]['id']);
										foreach($componente  as $rowc)
			                            {
			                            	?>
			                            	<div class="alert alert-info">
											    <strong>COMPONENTE : <?php echo $rowc['com_componente'];?></strong>
											</div>
											
												<?php 
												$producto = $this->model_producto->list_prod($rowc['com_id']);
												$nrop=1;
												foreach($producto  as $rowp)
			                            		{	$meta_gest=$this->model_producto->meta_prod_gest($rowp['prod_id']);
			                            			$ti='';
                                                    if($rowp['indi_id']==2){ $ti='%';}
			                            			?>
			                            			<table class="table table-bordered">
													<thead>
														<tr>
															<th bgcolor="#555756" style="width:1%;"><font color="#ffffff">NRO</font></th>
															<th bgcolor="#555756" style="width:20%;"><font color="#ffffff">PRODUCTO</font></th>
															<th bgcolor="#555756" style="width:5%;"><font color="#ffffff">INDICADOR</font></th>
															<th bgcolor="#555756" style="width:5%;"><font color="#ffffff">LINEA BASE</font></th>
															<th bgcolor="#555756" style="width:5%;"><font color="#ffffff">META TOTAL</font></th>
															<th bgcolor="#555756" style="width:5%;"><font color="#ffffff">META GESTI&Oacute;N</font></th>
															<th bgcolor="#555756" style="width:5%;"><font color="#ffffff">PONDERACI&Oacute;N</font></th>
															<th bgcolor="#555756" style="width:1%;"><font color="#ffffff">MODIFICAR PRODUCTO</font></th>
														</tr>
													</thead>
			                            			<tr>
														<td>
														<?php 
														echo $nrop;
														if($id==$rowp['prod_id'])
														{
															?>
															<img src="<?php echo base_url(); ?>assets/Iconos/accept.png"/>
															<?php
														}
														?>
														</td>
														<td><?php echo $rowp['prod_producto'];?></td>
														<td><?php echo $rowp['indi_descripcion'];?></td>
														<td><?php echo $rowp['prod_linea_base'].' '.$ti;?></td>
														<td><?php echo $rowp['prod_meta'].' '.$ti;?></td>
														<td><?php echo $meta_gest[0]['meta_gest'].' '.$ti;?></td>
														<td><?php echo $rowp['prod_ponderacion'];?> %</td>
														<td>
															<center><a href="<?php echo base_url().'index.php/admin/mod/producto/'.$proyecto[0]['proy_id'].'/'.$rowp['prod_id'].'/'.$v1.'/'.$v2.'/'.$v3.'' ?>" class="btn btn-primary" title="MODIFICAR PRODUCTO">MODIFICAR PRODUCTO</a></center>
														</td>
													</tr>
													</table><br>
			                            			<?php
			                            			$nroa=1;
			                            			$actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
			                            			foreach($actividad  as $rowa)
			                            			{ 	$ti='';if($rowa['indi_id']==2){ $ti='%';}
			                            				$meta_gesta=$this->model_actividad->meta_act_gest($rowa['act_id']);
			                            				?>
			                            				<table class="table table-bordered">
															<thead>
																<tr>
																	<th style="width:1%;">NRO</th>
																	<th style="width:20%;">ACTIVIDAD</th>
																	<th style="width:5%;">INDICADOR</th>
																	<th style="width:5%;">LINEA BASE</th>
																	<th style="width:5%;">META TOTAL</th>
																	<th style="width:5%;">META GESTI&Oacute;N</th>
																	<th style="width:5%;">PONDERACI&Oacute;N</th>
																	<th style="width:1%;">MODIFICAR ACTIVIDAD</th>
																</tr>
															</thead>
					                            			<tr>
																<td>
																<?php 
																echo $nroa;
																if($id==$rowa['act_id'])
																{
																	?>
																	<img src="<?php echo base_url(); ?>assets/Iconos/accept.png"/>
																	<?php
																}
																?>
																</td>
																<td><?php echo $rowa['act_actividad'];?></td>
																<td><?php echo $rowa['indi_descripcion'];?></td>
																<td><?php echo $rowa['act_linea_base'].' '.$ti;?></td>
																<td><?php echo $rowa['act_meta'].' '.$ti;?></td>
																<td><?php echo $meta_gesta[0]['meta_gest'].' '.$ti;?></td>
																<td><?php echo $rowa['act_ponderacion'];?> %</td>
																<td>
																	<center><a href="<?php echo base_url().'index.php/admin/mod/actividad/'.$proyecto[0]['proy_id'].'/'.$rowa['act_id'].'/'.$v1.'/'.$v2.'/'.$v3.'' ?>" class="btn btn-default" title="MODIFICAR ACTIVIDAD">MODIFICAR ACTIVIDAD</a></center>
																</td>
															</tr>
														</table><br>
			                            				<?php
			                            				$nroa++;
			                            			}
			                            			$nrop++;
			                            		}
			                            }
										?>
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
					</article>
				<?php
			}
			if($v2==1)
			{ $unidad = $this->model_proyecto->responsable_proy($proyecto[0]['proy_id'],'1');
				?>
				<article class="col-sm-12 col-md-12 col-lg-12">
					<div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
							<h2>PLAZO</h2>
						</header>
						<div>
							<div class="widget-body">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width:5%;"><font size="1">FASE</font></th>
										<th style="width:5%;"><font size="1">ETAPA</font></th>
										<th style="width:10%;"><font size="1">DESCRIPCI&Oacute;N FASE </font></th>
										<th style="width:5%;"><font size="1">UNIDAD ORGANIZACIONAL</font></th>
										<th style="width:5%;"><font size="1">FECHA DE INICIO</font></th>
										<th style="width:5%;"><font size="1">FECHA DE CONCLUSI&Oacute;N</font></th>
										<th style="width:5%;"><font size="1">EJECUCI&Oacute;N</font></th>
										<th style="width:5%;"><font size="1">ANUAL/PLURIANUAL</font></th>
										<th style="width:5%;"><font size="1">MODIFICAR ACTIVIDAD</font></th>
										</tr>
								</thead>
					             	<tr>
						             	<td><?php echo $fase[0]['fase'] ?></td>
						             	<td><?php echo $fase[0]['etapa'] ?></td>
						             	<td><?php echo $fase[0]['descripcion'] ?></td>
						             	<td><?php echo $unidad[0]['uejec'] ?></td>
						             	<td><?php echo date('d-m-Y',strtotime($fase[0]['inicio'])) ?></td>
						             	<td><?php echo date('d-m-Y',strtotime($fase[0]['final'])) ?></td>
						             	<td><?php echo $this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']) ?></td>
						             	<td><?php echo $this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']) ?></td>
						             	<td>
						             		<center><a href="#" data-toggle="modal" class="btn btn-default nuevo_ff" data-target="#modal_nuevo_ff" title="MODIFICAR PLAZO DE EJECUCION FASE ACTIVA" style="width:100%;" >MODIFICAR PLAZO</a></center>
						             	</td>
					             	</tr>
					            </table>				
							</div>
						<!-- end widget content -->
						</div>
					<!-- end widget div -->
					</div>
				<!-- end widget -->
				</article>
				<?php
			}
			if($v3==1)
			{ $unidad = $this->model_proyecto->responsable_proy($proyecto[0]['proy_id'],'1');
				?>
				<article class="col-sm-12 col-md-12 col-lg-12">

					<div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
							<h2>TECHO PRESUPUESTARIO</h2>
						</header>
						<div>
							<div class="widget-body">
							<?php 
								if($id=='true')
								{
								?>
									<div class="alert alert-success">
									  <a href="#" class="alert-link">Se Modifico Correctamente el presupuesto</a>
									</div>
								<?php
								}
							?>
								
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width:5%;"><font size="1">FASE</font></th>
										<th style="width:5%;"><font size="1">ETAPA</font></th>
										<th style="width:10%;"><font size="1">DESCRIPCI&Oacute;N FASE </font></th>
										<th style="width:5%;"><font size="1">UNIDAD ORGANIZACIONAL</font></th>
										<th style="width:5%;"><font size="1">FECHA DE INICIO</font></th>
										<th style="width:5%;"><font size="1">FECHA DE CONCLUSI&Oacute;N</font></th>
										<th style="width:5%;"><font size="1">EJECUCI&Oacute;N</font></th>
										<th style="width:5%;"><font size="1">ANUAL/PLURIANUAL</font></th>
										<th style="width:1%;"></th>
										</tr>
								</thead>
					             	<tr>
						             	<td><?php echo $fase[0]['fase'] ?></td>
						             	<td><?php echo $fase[0]['etapa'] ?></td>
						             	<td><?php echo $fase[0]['descripcion'] ?></td>
						             	<td><?php echo $unidad[0]['uejec'] ?></td>
						             	<td><?php echo date('d-m-Y',strtotime($fase[0]['inicio'])) ?></td>
						             	<td><?php echo date('d-m-Y',strtotime($fase[0]['final'])) ?></td>
						             	<td><?php echo $this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']) ?></td>
						             	<td><?php echo $this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']) ?></td>
						             	<td>
						             		<center><a href="<?php echo base_url().'index.php/admin/mod/presupuesto/'.$proyecto[0]['proy_id'].'/'.$v1.'/'.$v2.'/'.$v3.'' ?>" class="btn btn-default" style="width:100%;">MODIFICAR PRESUPUESTO</a><br>
						             		<a href="<?php echo base_url().'index.php/admin/mod/techo_p/'.$proyecto[0]['proy_id'].'/'.$v1.'/'.$v2.'/'.$v3.'' ?>" class="btn btn-default" style="width:100%;">MODIFICAR TECHO</a></center>
						             	</td>
					             	</tr>
					            </table>				
							</div>
						<!-- end widget content -->
						</div>
					<!-- end widget div -->
					</div>
				<!-- end widget -->
				</article>
				<?php
			}
			?>	
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->
			</div>
		</div>
		<!-- END MAIN PANEL -->
	<div  class="modal animated fadeInDown" id="modal_nuevo_ff" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">
                        <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
                    </h4>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR PLAZO (Fase Activa)</b>
                    </h4>
                </div>
				<div class="modal-body no-padding">
					<div class="row">
						<form id="form_ff" novalidate="novalidate" method="post">
						<input type="hidden" name="id_p" id="id_p" value="<?php echo $proyecto[0]['proy_id'];?>">
						<input type="hidden" name="id_f" id="id_f" value="<?php echo $fase[0]['id'];?>">
						<input type="hidden" name="fi1" id="fi1" value="<?php echo date('d/m/Y',strtotime($fase[0]['inicio'])) ?>">
                        <input type="hidden" name="ff1" id="ff1" value="<?php echo date('d/m/Y',strtotime($fase[0]['final'])) ?>">
							<div id="bootstrap-wizard-1" class="col-sm-12">
								<div class="well">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label><b>FECHA INICIAL</b></label>
												<input type="text" name="fi2" id="fi2" value="<?php echo date('d/m/Y',strtotime($fase[0]['inicio'])) ?>" placeholder="Ingrese Fecha" class="form-control datepicker" data-dateformat="dd/mm/yy">
												<span class="input-group-btn"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label><b>FECHA FINAL</b></label>
												<input type="text" name="ff2" id="ff2" value="<?php echo date('d/m/Y',strtotime($fase[0]['final'])) ?>" placeholder="Ingrese Fecha" class="form-control datepicker" data-dateformat="dd/mm/yy">
												<span class="input-group-btn"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
									</div>
								</div> <!-- end well -->
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-sm btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="enviar_ff" id="enviar_ff" class="btn  btn-sm btn-primary">
                                <i class="fa fa-save"></i>
                                ACEPTAR
                            </button>
                        </div>
                    </div>
                </div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script type="text/javascript">
		    $(function () {
		        var id_p = '';
		        $('#modal_nuevo_ff').on('hidden.bs.modal', function () {
		            document.forms['form_ff'].reset();

		        });
		        $("#enviar_ff").on("click", function (e) {
		            //========================VALIDANDO FORMULARIO===================
		            var $validator = $("#form_ff").validate({
		                //////////////// DATOS GENERALES
		                rules: {
		                    fi2: { //// indicador
		                        required: true,
		                    },
		                    ff2: { //// indicador
		                        required: true,
		                    }
		                },
		                messages: {
		                    fi2: "Seleccione Fecha Inicial a modificar ",
		                    ff2: "Seleccione Fecha Final a modificar",
		                    
		                },
		                highlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		                },
		                unhighlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		                },
		                errorElement: 'span',
		                errorClass: 'help-block',
		                errorPlacement: function (error, element) {
		                    if (element.parent('.input-group').length) {
		                        error.insertAfter(element.parent());
		                    } else {
		                        error.insertAfter(element);
		                    }
		                }
		            });
		            var $valid = $("#form_ff").valid();
		            if (!$valid) {
		                $validator.focusInvalid();
		                //return false;
		            } else { 
		                //==========================================================
		                var id_p = document.getElementById("id_p").value;
		                var id_f = document.getElementById("id_f").value;
		                var fi1 = document.getElementById("fi1").value;
		                var ff1 = document.getElementById("ff1").value;
		                var fi2 = document.getElementById("fi2").value;
		                var ff2 = document.getElementById("ff2").value;
		                //=================== VERIFICAR SI EXISTE EL COD DE PROGRAMA ==============
		                
		                var url = "<?php echo site_url("admin")?>/mod/valida_plazo";
		                            $.ajax({
		                                type: "post",
		                                url: url,
		                                data: {
		                                    id_p: id_p,
		                                    id_f: id_f,
		                                    fi1: fi1,
		                                    ff1: ff1,
		                                    fi2: fi2,
		                                    ff2: ff2
		                                },
		                                success: function (data) {
		                                    window.location.reload(true);
		                                }
		                            });
		            }
		        });
		    });
		</script>
		<!--============================== MODIFICAR OBSERVACION =========================================-->
	</body>
</html>