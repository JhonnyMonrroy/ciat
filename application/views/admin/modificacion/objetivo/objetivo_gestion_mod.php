<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
		    <!--para las alertas-->
    	<meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
          	<style>
			/*////scroll tablas/////*/
			table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
			    overflow-x: scroll;
            }
			</style>
	</head>
	<body class="">
		<?php
$site_url = site_url("");
$rol_id = $this->session->userData('rol_id');
?>
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Expandir Pantalla"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="MODIFICACIONES"> <span class="menu-item-parent">MODIFICACIONES</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Modificaciones</li><li>Modificaciones del POA</li><li>Modificaciones de Objetivos de Gesti&oacute;n y Productos Terminales</li><li>Objetivo de Gesti&oacute;n</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="well">
					<div class="">
						<h1>
						C&Oacute;DIGO OBJETIVO: <small><?php echo $obje[0]['o_codigo'] ?></small><br>
						OBJETIVO GESTI&Oacute;N: <small><?php echo $obje[0]['o_objetivo'] ?></small>
						</h1>
					</div>
				</section>

				<article class="col-sm-12 col-md-12 col-lg-12">
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <i class="glyphicon glyphicon-pencil"></i> </span>
                        <h2>MODIFICAR ACCI&Oacute;N DE CORTO PLAZO</h2>
                    </header>
                    <div class="widget-body">
                    	<form  name="formulario" id="formulario" method="post" action="<?php echo site_url("admin").'/mod/valida_objetivo' ?>"  enctype="multipart/form-data">
											<input class="form-control" type="hidden" name="sw1" id="sw1" value="1">
											<input class="form-control" type="hidden" name="obje_id" id="obje_id" value="<?php echo $obje_id;?>">
											<input class="form-control" type="hidden" name="o_id" id="o_id" value="<?php echo $o_id;?>">
											<input class="form-control" type="hidden" name="obj1" id="obj1" value="<?php echo $obje[0]['o_objetivo'];?>">
											<input class="form-control" type="hidden" name="lb" id="lb" value="<?php echo round($obje[0]['o_linea_base'],1);?>">
											<input type="hidden" name="site_url" id="site_url" value="<?php echo site_url("")?>">

							<div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                    	<div class="col-sm-6">
                                                <div class="form-group">
                                                        <LABEL><b>Responsable</b></label>
                                                        <select name="funcionario" id="funcionario"
                                                                class="form-control"  disabled="disabled" >
                                                            <option value=""> Seleccione una opción</option>
                                                            <?php
                                                            foreach ($list_funcionario as $row) {
                                                                $dato = $row['fun_nombre'] . " " . $row['fun_paterno'] . " " . $row['fun_materno'];
                                                                if ($row['fun_id'] == $obje[0]['fun_id']) {
                                                                    echo '<option value="' . $row['fun_id'] . '" selected>' . $dato . '</option>';
                                                                } 
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <LABEL><b>Unidad Organizacional</b></label>
															<input type="text" name="uni_unidad" id="uni_unidad"
                                                                   class="form-control" disabled="disabled"
                                                                   value="<?php echo $this->session->userData('unidad') ?>">



                                                    </div>
                                                </div>

                                    </div>
                                </div>

                                <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <LABEL><b>Código</b></label>
                                                        <input value="<?php echo $obje[0]['o_codigo'] ?>"
                                                               disabled="disabled" class="form-control" type="text"
                                                               name="ocodigo" id="ocodigo">
                                                    </div>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <LABEL><b>Acci&oacute;n de Corto Plazo</b></label>
                                                        <textarea name="obj2" id="obj2" style="width:100%;"
                                                                  rows="4" class="form-control custom-scroll"
                                                                  onpaste="return false" disabled="disabled"
                                                                  onkeypress="if (this.value.length > 500){return false; }"><?php echo $obje[0]['o_objetivo'];?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                    <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <LABEL><b>Tipo Indicador? </b></label><br>
                                                                    
                                                                <select class="form-control" id="tipo_indicador" name="tipo_indicador" onChange="cambia1()" disabled="disabled">
                                                                    <option value="">Seleccione</option>
                                                                    <?php
                                                                    foreach ($list_indicador as $row) {
                                                                        if ($row['indi_id'] == $obje[0]['indi_id']) {
                                                                            echo '<option value="' . $row['indi_id'] . '" selected>' . $row['indi_descripcion'] . '</option>';
                                                                        } else {
                                                                            echo '<option value="' . $row['indi_id'] . '">' . $row['indi_descripcion'] . '</option>';
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        if ($obje[0]['indi_id'] == 1) {
                                                            ?>
                                                            <div id="caja_relativo2" name="caja_relativo2"
                                                                 style="display: none;">
                                                                <div class="col-sm-12">
                                                                    <label for=""><b>Denominador</b></label>
                                                                    <select class="form-control" id="o_denominador"
                                                                            name="o_denominador" disabled="disabled"
                                                                            onChange="denominador(this)">
                                                                        <option value="0">Variable</option>
                                                                        <option value="1">Fijo</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <div id="caja_relativo2" name="caja_relativo2">
                                                                <div class="col-sm-12">
                                                                    <label for=""><b>Denominador</b></label>
                                                                    <select class="form-control" id="o_denominador"
                                                                            name="o_denominador"
                                                                            onChange="denominador(this)">
                                                                        <?php
                                                                        if ($obje[0]['o_denominador'] == 0) {
                                                                            ?>
                                                                            <option value="0" selected>Variable
                                                                            </option>
                                                                            <option value="1">Fijo</option>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <option value="0">Variable</option>
                                                                            <option value="1" selected>Fijo</option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>

                                                    </div>


                                                </div>


                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <LABEL><b>Indicador</b></label>
                                                <textarea name="oindicador" id="oindicador" disabled="disabled" style="width:100%;"
                                                          rows="4" class="form-control custom-scroll"
                                                          onkeypress="if (this.value.length > 500){return false; }"><?php echo $obje[0]['o_indicador'] ?></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top: 10px;">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <LABEL><b>Fórmula</b></label>
                                                        <?php
                                                        if ($obje[0]['indi_id'] == 1) {
                                                            echo '<textarea name="oformula" id="oformula" onpaste="return false" disabled="disabled"
                                                          style="width:100%;"
                                                          rows="3" class="form-control custom-scroll"
                                                          onkeypress="if (this.value.length > 500){return false; }">N/A</textarea>';
                                                        } else {
                                                            echo '<textarea name="oformula" id="oformula" disabled="disabled" onpaste="return false"
                                                                        style="width:100%;"
                                                                        rows="3" class="form-control custom-scroll"
                                                                        onkeypress="if (this.value.length > 500){return false; }">' . $obje[0]['o_formula'] . '</textarea>';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="alert alert-block alert-success">
                                                        <b>La suma de la programación mensual mas la línea base debe ser igual a la meta. </b><br>
                                                        <center><b style="align-content: center">( <span style="color: #00A300">PROGRAMACION MENSUAL </span> +
                                                                <span style="color: #0000cc">LINEA BASE</span> ) =
                                                                <span style="color: #f00000"> META </span></b></center>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <LABEL><b><span style="color: #0000cc">LÍNEA BASE</span></b></label>
                                                        <input class="form-control"
                                                               value="<?php echo round($obje[0]['o_linea_base'], 1) ?>"
                                                               type="text" name="olineabase" id="olineabase" disabled="disabled"
                                                               placeholder="0"
                                                               onkeypress="if (this.value.length < 6) { return numerosDecimales(event);}else{return false; }"
                                                               onpaste="return false">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <?php
                                                        if ($obje[0]['indi_id'] == 1) {
                                                            echo '<LABEL><b><span style="color: #f00000">Meta</span></b><span id="tipo_meta"> </span></label>';
                                                        } else {
                                                            echo '<LABEL><b><span style="color: #f00000">Meta</span></b><span id="tipo_meta"> -% </span></label>';
                                                        }
                                                        ?>
                                                        <input name="met_m" id="met_m" class="form-control" disabled="disabled"
                                                               value="<?php echo round($obje[0]['o_meta'], 1) ?>"
                                                               type="text"
                                                               placeholder="0" onChange="cambia1()"
                                                               onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"
                                                               onpaste="return false">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <LABEL><b>Ponderación</b></label>
                                                        <input class="form-control" disabled="disabled"
                                                               value="<?php echo round($obje[0]['o_ponderacion'], 1) ?>"
                                                               type="text" name="oponderacion" id="oponderacion"
                                                               placeholder="0%"
                                                               onkeypress="if (this.value.length < 6) { return numerosDecimales(event);}else{return false; }"
                                                               onpaste="return false">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <LABEL><b>Fuente Verificación</b></label>
                                                <textarea name="ofuenteverificacion" id="ofuenteverificacion" disabled="disabled"
                                                          style="width:100%;"
                                                          rows="3" class="form-control custom-scroll"
                                                          onpaste="return false"
                                                          onkeypress="if (this.value.length > 500){return false; }"><?php echo $obje[0]['o_fuente_verificacion'] ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <LABEL><b>Supuesto</b></label>
                                                <textarea name="osupuesto" id="osupuesto" style="width:100%;" disabled="disabled"
                                                          rows="3" class="form-control custom-scroll"
                                                          onpaste="return false"
                                                          onkeypress="if (this.value.length > 500){return false; }"><?php echo $obje[0]['o_supuestos'] ?></textarea>
                                                    </div>
                                                </div>
                                            </div>



                            </div>


                    </div>	
                </div>
            	</article>



										<article class="col-sm-12 col-md-8 col-lg-8" style="align-content: center;">
											<div class="jarviswidget jarviswidget-color-darken" id="wid-id-2" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">

												<header>
													<h2><strong>GESTI&Oacute;N <?php echo $this->session->userdata("gestion") ?></strong></h2>		
												</header>  
												
											<?php 
												$nro=0;
												foreach($programado as $row)
								                {
								                    $nro++;
								                    $matriz [1][$nro]=$row['mes_id'];
								                    $matriz [2][$nro]=$row['opm_fis'];
								                }

								                for($j = 1 ;$j<=12 ;$j++){
								                    $matriz_r[1][$j]=$j;
								                    $matriz_r[2][$j]='0'; /// programado
								                }

								                for($i = 1 ;$i<=$nro ;$i++){
								                    for($j = 1 ;$j<=12 ;$j++)
								                    {
								                        if($matriz[1][$i]==$matriz_r[1][$j])
								                        {
								                            $matriz_r[2][$j]=round($matriz[2][$i],2);
								                        }
								                    }
								                }
								                $suma=0;

								                for($j = 1 ;$j<=12 ;$j++)
								                {
								                	$suma=$suma+$matriz_r[2][$j];
								                }
								                                
											?>
													<div class="row">
															<table class="table table-bordered table-hover" style="width:100%;" >
															    <thead>
															        <tr>
															            <th style="width:15%;"><center>ENERO</center></th>
															            <th style="width:15%;"><center>FEBRERO</center></th>
															            <th style="width:15%;"><center>MARZO</center></th>
															            <th style="width:15%;"><center>ABRIL</center></th>
															            <th style="width:15%;"><center>MAYO</center></th>
															            <th style="width:15%;"><center>JUNIO</center></th>
															        </tr>
															    </thead>
															    <tbody>
															        <tr>
															            <td><input  name="m1" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][1]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
															            <td><input  name="m2" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][2]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
															            <td><input  name="m3" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][3]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
															            <td><input  name="m4" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][4]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
															        	<td><input  name="m5" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][5]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
															            <td><input  name="m6" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][6]; ?>" onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
															        </tr>
															    </tbody>
															</table>
															<table class="table table-bordered table-hover" style="width:100%;" >
															    <thead>
															        <tr>
															            <th style="width:15%;"><center>JULIO</center></th>
															            <th style="width:15%;"><center>AGOSTO</center></th>
															            <th style="width:15%;"><center>SEPTIEMBRE</center></th>
															            <th style="width:15%;"><center>OCTUBRE</center></th>
															            <th style="width:15%;"><center>NOVIEMBRE</center></th>
															            <th style="width:15%;"><center>DICIEMBRE</center></th>
															        </tr>
															    </thead>
															    <tbody>
															        <tr>
															            <td><input  name="m7" class="form-control" type="text" style="width:100%;" value="<?php echo $matriz_r[2][7]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
															            <td><input  name="m8" class="form-control" type="text" style="width:100%;" value="<?php echo $matriz_r[2][8]; ?>" onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
															        	<td><input  name="m9" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][9]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
															            <td><input  name="m10" class="form-control" type="text" style="width:100%;" value="<?php echo $matriz_r[2][10]; ?>" onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
															            <td><input  name="m11" class="form-control" type="text" style="width:100%;" value="<?php echo $matriz_r[2][11]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
															            <td><input  name="m12" class="form-control" type="text" style="width:100%;" value="<?php echo $matriz_r[2][12]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
															        </tr>
															    </tbody>
															</table>		
														</div>

													
												<div class="col-sm-4">
												<div class="form-group">
												<label><font size="2" color="blue"><b>SUMA TOTAL DE INDICADOR</b></font></label>
												<input class="form-control" name="total" type="text" id="total" value="<?php echo round($suma,2);?>" disabled="true" >
												
												<div class="well well-sm">
	                                            <div class="row">
	                                                
	                                                <div class="col-sm-1">
	                                                    <label for=""> </label>
	                                                    <img id="load" style="display: none" src="<?php echo base_url() ?>/assets/img/loading.gif" width="30" height="30">
	                                                </div>
	                                            </div>
	                                        </div>


												
													<a href="<?php echo base_url().'index.php/admin/mod/redobj/' ?>" class="btn btn-success" title="Volver atras">ATRAS</a>
													
												
												</div>
												</div>
													<div class="modal-footer"> 
														
													</div>
											</div> <!-- div obs1 -->
												</div><!-- row -->
											</div><!-- class="jarviswidget jarviswidget-color-darken" -->
										</article>
									</form>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
		<script>
			function suma_programado()
            {
                lb = parseFloat($('[name="lb"]').val());
                m1 = parseFloat($('[name="m1"]').val());
                m2 = parseFloat($('[name="m2"]').val());
                m3 = parseFloat($('[name="m3"]').val());
                m4 = parseFloat($('[name="m4"]').val());
                m5 = parseFloat($('[name="m5"]').val());
                m6 = parseFloat($('[name="m6"]').val());
                m7 = parseFloat($('[name="m7"]').val());
                m8 = parseFloat($('[name="m8"]').val());
                m9 = parseFloat($('[name="m9"]').val());
                m10 = parseFloat($('[name="m10"]').val());
                m11 = parseFloat($('[name="m11"]').val());
                m12 = parseFloat($('[name="m12"]').val());

                $('[name="total"]').val((m1+m2+m3+m4+m5+m6+m7+m8+m9+m10+m11+m12+lb).toFixed(2) );
            }
            function cambia1()
            {
             $('#sw1').val(2);
            }
            function valida_envia()
            { 
            	suma=document.getElementById('total').value;
               
               if(parseFloat(document.formulario.met_m.value)<suma)
               {
               	alertify.error('La suma de las cifras Programadas no puede ser mayor a la Meta')
               	document.formulario.met_m.focus() 
	            return 0; 
               }
               if(parseFloat(document.formulario.met_m.value)>suma)
               {
               	alertify.error('La suma de las cifras Programadas no puede ser menor a la Meta')
               	document.formulario.met_m.focus() 
	            return 0;
               }
               if(parseFloat(document.formulario.met_m.value)==suma)
               {
               	alertify.confirm("GUARDAR MODIFICACI\u00D3N?", function (a) {
                    if (a) {
                        //============= GUARDAR DESPUES DE LA VALIDACION ===============
                        document.formulario.submit();
                    } else {
                        alertify.error("OPCI\u00D3N CANCELADA");
                    }
                });
               }

            }
        </script>
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<script> var site_url = document.getElementById("site_url").value; </script>
		<script src="<?php echo base_url(); ?>mis_js/programacion/prog_poa/abm_ogestion.js" type="text/javascript"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		
	</body>
</html>
