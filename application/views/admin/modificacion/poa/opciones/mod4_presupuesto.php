<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<style type="text/css">
			aside{background: #05678B;}
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
			</div>
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
					</a> 
				</span>
			</div>

			<nav>
				<ul>
					<li>
						<a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
					</li>
					<li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> MIS PROYECTOS"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
		            </li>
					<?php
					if($nro_fase==1){
					    for($i=0;$i<count($enlaces);$i++){ ?>
					    	 <li>
				              	<a href="#" >
				              		<i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
				              	<ul >
				              	<?php
				                $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
					      		foreach($submenu as $row) {
				                ?>
				                <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
				                <?php } ?>
				                </ul>
				            </li>
					    	<?php
					    }
					}
					?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
				<li>Mis Operaciones</li><li>Modificaci&oacute;n</li><li>F&iacute;sica</li><li>Operaci&oacute;n - Proyecto de Inversi&oacute;n</li><li>Modificar Presupuesto</li>

				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<section id="widget-grid" class="well">
					<div class="">
						<h1> 
							CATEGORIA PROGRAMATICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'].' - '.$proyecto[0]['proy_nombre']?></small><br>
							FECHA INICIO : <small><?php echo date('d/m/Y',strtotime($id_f[0]['inicio'])); ?></small> - FECHA FINAL : <small><?php echo date('d/m/Y',strtotime($id_f[0]['final'])); ?></small>
						</h1>
					</div>
				</section>
				<section id="widget-grid" class="">
						<div class="row">
							<div class="container">
								<?php 
				                  if($this->session->flashdata('success')){ ?>
				                    <div class="alert alert-success">
				                      <?php echo $this->session->flashdata('success'); ?>
				                    </div>
				                <?php }
				                    elseif($this->session->flashdata('danger')){ ?>
			                    	<div class="alert alert-danger">
				                      <?php echo $this->session->flashdata('danger'); ?>
				                    </div><?php }
				                ?>
								<?php echo $presupuesto; ?>
							</div>
						<!-- WIDGET END -->
						</div>
				</section>
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script type="text/javascript">
	    $(function () {
			$("#add_form3").on("click", function () {
		        var $valid = $("#form_nuevo3").valid();
		        if (!$valid) {
		            $validator.focusInvalid();
		        } else {
		            var ptto = document.getElementById('ptto').value;
		            var nro = document.getElementById('nro').value;
		            var suma=0;
	                for (var i = 1; i <= nro; i++) {
	                	if(document.getElementById('fgp_id'+i).value!=0){
	                		suma=parseFloat(suma)+parseFloat(document.getElementById('fgp_id'+i).value);
	                	}
	                }
	                
	                if(parseFloat(suma)<=parseFloat(ptto)){
		            	alertify.confirm("DESEA MODIFICAR EL PRESUPUESTO DE LA FASE ?", function (a) {
		                    if (a) {
		                        document.getElementById("load3").style.display = 'block';
		                        document.getElementById('add_form3').disabled = true;
		                        document.forms['form_nuevo3'].submit();
		                    } else {
		                        alertify.error("OPCI\u00D3N CANCELADA");
		                    }
		                });	
	                }
	                else{
	                	alertify.error("ERROR EN LA SUMA PROGRAMADO POR GESTIONES SUPERA AL PTTO TOTAL");
	                }
		        }
		    });
	    });

	    function suma_presupuesto(){ 
	    	ptotal = parseFloat($('[name="ptto"]').val());
	    	nro = parseFloat($('[name="nro"]').val());
	    	var suma=0;
	    	for (var i = 1; i <= nro; i++) {
            	suma=parseFloat(suma)+parseFloat($('[id="fgp_id'+i+'"]').val());
            }

	    	$('[name="saldo"]').val((ptotal-suma).toFixed(2));
	    	saldo=parseFloat($('[name="saldo"]').val());
	    	if(saldo==0){
	    		$('#tit').html('<font color="#42F990"> SIN SALDO</font>');
	    	}
	    	if(saldo>0){
	    		$('#tit').html('<font color="red"> SALDO PENDIENTE</font>');
	    	}
	    	if(saldo<0){
	    		$('#tit').html('<font color="red"> SALDO SOBREGIRADO</font>');
	    	}
	    }
		</script>
	</body>
</html>
