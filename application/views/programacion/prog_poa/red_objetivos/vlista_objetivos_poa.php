<script xmlns="http://www.w3.org/1999/html">
    function abreVentana(PDF) {
        var direccion;
        direccion = '' + PDF;
        window.open(direccion, "Reporte de Proyectos", "width=800,height=650,scrollbars=SI");
    }
</script>
<style>
    table {
        font-size: 9px;
        width: 100%;
        max-width: 1550px;;
        overflow-x: scroll;
    }

    th {
        padding: 1.4px;
        text-align: center;
        font-size: 9px;
    }

    td {
        padding: 1.4px;
        text-align: center;
        font-size: 9px;
    }
</style>
<!-- MAIN PANEL -->
<div id="main" role="main">
    <!-- RIBBON -->
    <div id="ribbon">
        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li> Programación del POA</li>
            <li><a href="<?php echo site_url("") ?>/prog/redobj">Red de Objetivos </a></li>
            <li> Acciones de Mediano Plazo</li>
        </ol>
    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <section id="widget-grid" class="well">
                    <div class="" style="font-size: 15px">
                        <b> ACCI&Oacute;N DE MEDIANO PLAZO</b> <br>
                        <b>C&oacute;digo POA: </b>
                        <small class="txt-color-blueLight"><?php echo $dato_poa[0]['poa_codigo'] ?></small>
                        <br>
                        <b>Categoría Program&aacute;tica: </b>
                        <small
                            class="txt-color-blueLight"><?php echo $dato_poa[0]['aper_programa'] . $dato_poa[0]['aper_proyecto'] . $dato_poa[0]['aper_actividad'] . " - " . $dato_poa[0]['aper_descripcion'] ?></small>
                        <br>
                        <b>Gesti&oacute;n: </b>
                        <small class="txt-color-blueLight"><?php echo $dato_poa[0]['poa_gestion'] ?></small>
                        <br>
                    </div>
                </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <section id="widget-grid" class="well">
                    <center>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                                OPCIONES
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1"
                                                           href="javascript:abreVentana('<?php echo site_url("admin") . '/objetivos_estrategicos/' . $dato_poa[0]['poa_id']; ?>');">VER
                                        REPORTE</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url("") ?>/prog/redobj">VOLVER ATRAS</a></li>
                            </ul>
                        </div>
                    </center>
                </section>
            </article>

        </div>

        <!-- <div class="row">
             <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                 <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw ">&nbsp;</i> ACCIONES DE MEDIANO PLAZO VINCULADOS AL PROGRAMA</h1>
             </div>
         </div>-->
        <!-- <div class="row">
            <div class="col-sm-1">
                <a data-toggle="modal" href="<?php /*echo site_url("") */ ?>/prog/redobj"
                   class="btn btn-labeled btn-success ">
                    <span class="btn-label"><i class="glyphicon glyphicon-arrow-left"></i></span><b>ATRAS</b></a><br>
            </div>
            <div class="col-sm-2">
                <a href="javascript:abreVentana('<?php /*echo site_url("admin") . '/objetivos_estrategicos/' . $dato_poa[0]['poa_id']; */ ?>');"
                   class="btn btn-labeled btn-danger" title="REPORTE">
                    <span class="btn-label"><i class="fa fa-file-pdf-o"></i></span><b>REPORTE</b></a><br>
            </div>
        </div>-->
        <section id="widget-grid" class="">
            <div class="row">
                <!--<article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <span class="glyphicon glyphicon-folder-close"></span> </span>

                            <h2>CATEGORIA PROGRAM&Aacute;TICA</h2>
                        </header>
                        <div class="widget-body">
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <LABEL><b>Programa </b></label>
                                        <input class="form-control" type="text" disabled="disabled"
                                               value="<?php /*echo $dato_poa[0]['aper_programa'] . $dato_poa[0]['aper_proyecto'] . $dato_poa[0]['aper_actividad'] . " - " . $dato_poa[0]['aper_descripcion'] */ ?>">
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <LABEL><b>Gesti&oacute;n </b></label>
                                        <input class="form-control" type="text" disabled="disabled"
                                               value="<?php /*echo $dato_poa[0]['poa_gestion'] */ ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>-->
                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <?php
                    if (count($lista_objetivos) == 0) {
                        ?>
                        <div class="row">
                            <div class="alert alert-block alert-warning">
                                <a class="close" data-dismiss="alert" href="#">×</a>

                                <h1 class="alert-heading"> No existen Acciones de mediano plazo cargados.</h1>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <span class="fa fa-table"></span> </span>

                            <h2>ACCIONES DE MEDIANO PLAZO</h2>
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>
                                                <center>ACCIONES DE<br>MEDIANO PLAZO</center>
                                            </th>
                                            <th> C&Oacute;DIGO</th>
                                            <th title="PLAN ESTRATÉGICO DE DESARROLLO">PDES</th>
                                            <th title="PLAN TERRITORIAL DE DESARROLLO INTEGRAL">PTDI</th>
                                            <th>ACCI&Oacute;N DE MEDIANO PLAZO</th>
                                            <th title="INDICADOR">INDICADOR DE IMPACTO</th>
                                            <th title="TIPO DE INDICADOR">TIPO DE INDICADOR</th>
                                            <th title="LÍNEA BASE">LINEA_BASE</th>
                                            <th>META</th>
                                            <th>FUENTE VERIFICACI&Oacute;N</th>
                                            <th>CRONOGRAMA DE EJECUCI&Oacute;N</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $cont_pdes = 0;
                                        $cont_ptdi = 0;
                                        foreach ($lista_objetivos as $fila) {
                                            $this->db->SELECT('o.*,f.fun_nombre,f.fun_paterno,f.fun_materno,(SELECT get_unidad(f.uni_id)) AS unidad,i.indi_abreviacion');
                                            $this->db->FROM('objetivosgestion o');
                                            $this->db->JOIN('funcionario f', 'o.fun_id = f.fun_id','INNER');
                                            $this->db->JOIN('indicador i', 'i.indi_id = o.indi_id','INNER');
                                            $this->db->WHERE('(o.o_estado = 1 OR o.o_estado = 2)');
                                            $this->db->WHERE('obje_id', $fila['obje_id']);
                                            $query = $this->db->get();
                                            $di = "";
                                            if ( $query->num_rows() > 0) {
                                                $di = "ASIGNAR ACCIONES DE CORTO PLAZO";
                                            } else {
                                                $di = "SIN ACCIONES DE CORTO PLAZO";
                                            }
                                            echo '<tr>';
                                            echo '<td ><a href="' . site_url("") . '/prog/obje_gestion/' .
                                                $dato_poa[0]['poa_id'] . '/' . $fila['obje_id'] . '"><center>
                                            <img src="' . base_url() . 'assets/ifinal/archivo.png" width="50" height="50"
                                            class="img-responsive" title="'.$di.'">
                                            REGISTRO DE ACCIONES DE CORTO PLAZO
                                            </center></a></td>';
                                            echo '<td>' . $fila['obje_codigo'] . '</td>';
                                            ?>
                                            <!-- ------------------------ GENERAR PDES ----------------------------- -->
                                            <td>
                                                <div class="buttonclick">
                                                    <div class="btnapp">
                                                        <div class="hover-btn">
                                                            <a href="#" data-toggle="modal"
                                                               data-target="#pdes<?php echo $cont_pdes; ?>"
                                                               class="btn btn-lg btn-default">
                                                                <font
                                                                    size="1"><b><?php echo $fila['pdes_pcod'] . ' <br> ' . $fila['pdes_mcod'] .
                                                                            ' <br> ' . $fila['pdes_rcod'] . ' <br> ' . $fila['pdes_acod'] ?></b></font>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div><!-- buttonclick -->
                                                <div class="modal fade bs-example-modal-lg"
                                                     id="pdes<?php echo $cont_pdes; ?>"
                                                     tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="texto"><font size="4">
                                                                    <div class="row text-center">
                                                                        <LABEL><b>PLAN ESTRATÉGICO DE
                                                                                DESARROLLO</b></LABEL>
                                                                    </div>
                                                                    <P>
                                                                        <u><b>PILAR</b></u>
                                                                        :<?php echo $fila['pdes_pcod'] . ' - ' . $fila['pdes_pilar'] ?>
                                                                        <br>
                                                                        <u><b>META</b></u>
                                                                        : <?php echo $fila['pdes_mcod'] . ' - ' . $fila['pdes_meta'] ?>
                                                                        <br>
                                                                        <u><b>RESULTADO</b></u>
                                                                        : <?php echo $fila['pdes_rcod'] . ' - ' . $fila['pdes_resultado'] ?>
                                                                        <br>
                                                                        <u><b>ACCION</b></u>
                                                                        : <?php echo $fila['pdes_acod'] . ' - ' . $fila['pdes_accion'] ?>
                                                                    </P>
                                                            </div>
                                                            </font>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <!-- ------------------------ GENERAR PTDI ----------------------------- -->
                                            <td>
                                                <div class="buttonclick">
                                                    <div class="btnapp">
                                                        <div class="hover-btn">
                                                            <a href="#" data-toggle="modal"
                                                               data-target="#ptdi<?php echo $cont_ptdi; ?>"
                                                               class="btn btn-lg btn-default">
                                                                <font
                                                                    size="1"><b><?php echo $fila['ptdi_ecod'] . ' <br> ' . $fila['ptdi_ocod'] .
                                                                            ' <br> ' . $fila['ptdi_pcod'] ?></b></font>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div><!-- buttonclick -->
                                                <div class="modal fade bs-example-modal-lg"
                                                     id="ptdi<?php echo $cont_ptdi; ?>"
                                                     tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="texto"><font size="4">
                                                                    <div class="row text-center">
                                                                        <LABEL><b>PLAN TERRITORIAL DE DESARROLLO
                                                                                INTEGRAL
                                                                            </b></LABEL>
                                                                    </div>
                                                                    <P>
                                                                        <u><b>EJE PROGRAMÁTICA</b></u>
                                                                        :<?php echo $fila['ptdi_ecod'] . ' - ' . $fila['ptdi_eje'] ?>
                                                                        <br>
                                                                        <u><b>PÓLITICA</b></u>
                                                                        : <?php echo $fila['ptdi_ocod'] . ' - ' . $fila['ptdi_politica'] ?>
                                                                        <br>
                                                                        <u><b>PROGRAMA</b></u>
                                                                        : <?php echo $fila['ptdi_pcod'] . ' - ' . $fila['ptdi_programa'] ?>
                                                                    </P>
                                                            </div>
                                                            </font>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <?php
                                            echo '<td>' . $fila['obje_objetivo'] . '</td>';
                                            echo '<td>' . $fila['obje_indicador'] . '</td>';
                                            echo '<td>' . $fila['indicador'] . '</td>';
                                            echo '<td>' . $fila['obje_linea_base'] . '</td>';
                                            echo '<td>' . $fila['obje_meta'] . '</td>';
                                            echo '<td>' . $fila['obje_fuente_verificacion'] . '</td>';
                                            echo '<td>';
                                            $indi[1] = '';
                                            $indi[2] = '%';
                                            $porc = $indi[$fila['indi_id']];
                                            //$gestion_inicial = $fila['obje_gestion_curso'];
                                            $gestion_inicial = $gestion_inicio;
                                            echo '<table style="width:100%;" class="table table-bordered">
                                                          <thead>
                                                              <tr>
                                                                  <td colspan="6" bgcolor="#2F4F4F"><center><b><font color="#ffffff" size="1">
                                                                  PROGRAMACI&Oacute;N ' . $gestion_inicial . ' - ' . ($gestion_inicial + 4) . '</font></b></center></td>
                                                              </tr>';
                                            //---------------------- CABECERA DE GESTIONES
                                            echo '<tr>';
                                            echo '<td style="width:1%;" bgcolor="#2F4F4F">
                                            <center>
                                            <button type="button" class="btn btn-primary grafico_objetivo" name="' . $fila["obje_id"] . '" id="grafico"
                                            data-toggle="modal" data-target="#modal_grafico"
                                            title="PROGRAMACION ' . $fila['obje_gestion_curso'] . ' - ' . ($gestion_inicial - 1) . '">
                                            <span class="glyphicon glyphicon-stats" aria-hidden="true">
                                            </center>
                                            </td>';
                                            for ($i = 1; $i <= 5; $i++) {
                                                echo '<td style="width:1%;" bgcolor="#2F4F4F"><center><b><font color="#ffffff" size="1">' . ($gestion_inicial++) . '</font></b></center></td>';
                                            }
                                            echo '</tr>';
                                            //---------------------- FIN DE CABECERA
                                            $obje_id = $fila['obje_id'];
                                            //---------------------- PROGRAMACION
                                            echo '<tr>';
                                            echo '<td style="width:5%;" bgcolor="#E6E6FA"><center><b><font color="#000000" size="1">P</font></b></center></td>';
                                            for ($i = 1; $i <= 5; $i++) {
                                                $puntero = 'prog' . $i;
                                                $prog_gestion = $temporalizacion[$obje_id][$puntero];
                                                echo ' <td style="width:1%;" bgcolor="#E6E6FA"><center><font color="#000000" size="1">' . round($prog_gestion, 1) . $porc . '</font></center></td>';
                                            }
                                            echo '</tr>';
                                            //--------------------- PROGRAMACION ACUMULADA
                                            echo '<tr>';
                                            echo '<td style="width:5%;" bgcolor="#E6E6FA"><center><b><font color="#000000" size="1">P.A</font></b></center></td>';
                                            for ($i = 1; $i <= 5; $i++) {
                                                $puntero_acumulado = 'p_acumulado' . $i;
                                                $prog_acumulado = $temporalizacion[$obje_id][$puntero_acumulado];
                                                echo ' <td style="width:1%;" bgcolor="#E6E6FA"><center><font color="#000000" size="1">' . round($prog_acumulado, 1) . $porc . '</font></center></td>';
                                            }
                                            echo '</tr>';
                                            //--------------------- PROGRAMACION ACUMULADA PORCENTUAL
                                            echo '<tr>';
                                            echo '<td style="width:5%;" bgcolor="#E6E6FA"><center><b><font color="#000000" size="1">%P.A</font></b></center></td>';
                                            for ($i = 1; $i <= 5; $i++) {
                                                $puntero_pa_porcentual = 'pa_porc' . $i;
                                                $pa_porcentual = $temporalizacion[$obje_id][$puntero_pa_porcentual];
                                                echo ' <td style="width:1%;" bgcolor="#E6E6FA"><center><font color="#000000" size="1">' . round($pa_porcentual, 1) . $porc . '%</font></center></td>';
                                            }
                                            echo '</tr>';
                                            echo '              </thead>
                                                        </table>';
                                            echo '</td>';


                                            echo '</tr>';
                                            $cont_pdes++;
                                            $cont_ptdi++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>

                    <!-- end widget -->
                </article>

                <!-- WIDGET END -->
            </div>
        </section>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN PANEL -->

<!-- ------------------------------- MODAL DE GRAFICO OBJETIVO ESTRATEGIO-------------- -->
<div class="modal fade bs-example-modal-lg" id="modal_grafico" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="col-md-12 des">
                <center>TEMPORALIZACI&Oacute;N</center>
            </div>
            <table class="table table-bordered" style="width:100%;">
                <tbody id="tabla_grafico">
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6 des">
                    L&Iacute;NEA BASE:<span class="badge" id="linea_base"></span>
                </div>
                <div class="col-md-6 des">
                    META: <span class="badge" id="meta"></span>
                </div>
            </div>

            <div class="row">
                <div id="" class="col-md-12">
                    <div id="grafico_objetivo" class="graf">
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>