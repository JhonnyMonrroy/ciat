 
<script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://printjs-4de6.kxcdn.com/print.min.css">
<script xmlns="http://www.w3.org/1999/html">
    function abreVentana(PDF) {
        var direccion;
        direccion = '' + PDF;
        window.open(direccion, "Reporte de Proyectos", "width=800,height=650,scrollbars=SI");
    };
</script>
<?php $site_url = site_url(""); ?>
<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Programación</li><li> Analisis de Situación</li><li>FODA</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw"></i> Analisis de Situación</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 animated fadeInRight">
                <a href="javascript:window.history.go(-1);" class="btn btn-labeled btn-success ">
                    <span class="btn-label">
                        <i class="glyphicon glyphicon-arrow-left"></i>
                    </span><b>ATRAS</b><br>
                </a>
            </div>
        </div>
        <section id="widget-grid" class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-teal" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <span class="fa fa-table"></span> </span>
                            <h2>Fortalezas, Oportunidades, Debilidades, Amenazas</h2>
                        </header>
                        <div>
                            <div class="widget-body">
                                <div class="row">
                                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                        <form class="form-horizontal" action="<?php echo $site_url . '/programacion/analisis_situacion/agrega_foda' ?>" id="form_analisis" name="form_analisis" method="post">
                                            <fieldset>
                                                <input name="poa_id" id="poa_id" type="text" value="<?php echo $poa_id?>" hidden>
                                                <legend>Agregar Nuevos Registros</legend>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2" for="prepend">Tipo De Registro</label>
                                                    <div class="col-md-10">
                                                        <div class="icon-addon addon-lg">
                                                            <select class="form-control" name="tipo_foda" id="tipo_foda">
                                                                <option value="">Seleccione Una Opción</option>
                                                                <?php
                                                                    foreach($tipo_foda as $fila){
                                                                        echo '<option value="'.$fila['tfoda_id'].'">'.$fila['tfoda_descripcion'].'</option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                            <label for="email" class="glyphicon glyphicon-search" rel="tooltip" title="Tipo"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2" for="prepend">Descripción</label>
                                                    <div class="col-md-10">
                                                        <div class="icon-addon addon-lg">
                                                            <input type="text" placeholder="Descripción" class="form-control" name="descripcion" id="descripcion">
                                                            <label for="email" class="glyphicon glyphicon-pencil" rel="tooltip" title="Descripción"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2" for="prepend">Incidencia en Objetivos</label>
                                                    <div class="col-md-10">
                                                        <div class="icon-addon addon-lg">
                                                            <input type="text" placeholder="Incidencia" class="form-control" name="incidencia" id="incidencia">
                                                            <label for="email" class="glyphicon glyphicon-pencil" rel="tooltip" title="Incidencia"></label>
                                                        </div>
                                                    </div>
                                                </div>                                        
                                            </fieldset>
                                            <button class="btn btn-success" type="reset">
                                                <i class="glyphicon glyphicon-remove-circle"></i>
                                                Limpiar
                                            </button>
                                            <button class="btn btn-primary" type="button" id="validar_form_analisis" name="validar_form_analisis">
                                                <i class="fa fa-save"></i>
                                                Registrar
                                            </button>
                                        </form>
                                    </div>
                                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                        <legend>
                                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                            Datos Registrados
                                            <span class="pull-right">
                                                <a href="javascript:abreVentana('<?php echo site_url("admin").'/reporte_analisis_situacion/'.$poa_id; ?>');" id="smart-mod-eg3" class="btn btn-success"> Generar Reporte </a>
                                            </span>
                                        </legend>
                                        <div class="widget-body no-padding">
                                            <div class="table-responsive">
                                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:100px;">Acción</th>
                                                        <th style="width:90px;">Tipo FODA</th>
                                                        <th style="width:90px;">Analisis</th>
                                                        <th>Descripción</th>
                                                        <th>Incidencia</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="bdi">
                                                        <?php 
                                                            foreach ($lista_foda as $fila) {
                                                                    echo '
                                                                        <tr id="tr'.$fila['foda_id'].'">
                                                                            <td>';?>
                                                                                <a data-toggle="modal" data-target="#modal_mod_aper" name="<?php echo $fila['foda_id'] ?>" id="enviar_mod" href="#" class="editar_anal">
                                                                                    <img src="<?php echo base_url() ?>assets/ifinal/modificar.png" rel="tooltip" title="Editar Registro">
                                                                                </a>
                                                                                <a class="del_anal" name="<?php echo $fila['foda_id'] ?>" >
                                                                                    <img src="<?php echo base_url() ?>assets/ifinal/eliminar.png" rel="tooltip" title="Eliminar Registro">
                                                                                </a>
                                                                            <?php
                                                                            echo '</td>
                                                                            <td>'.$fila['tfoda_descripcion'].'</td>
                                                                            <td>'.$fila['tfoda_analisis'].'</td>
                                                                            <td>'.$fila['foda_variables'].'</td>
                                                                            <td>'.$fila['foda_incidencia'].'</td>
                                                                        </tr>
                                                                    ';
                                                            }
                                                        ?>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
        </section>
    </div>

    <!-- ================== Modal  MODIFICAR apertura========================== -->
    <div class="modal animated fadeInDown" id="modal_mod_aper" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR REGISTRO</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="form_anal" name="form_anal" novalidate="novalidate" method="post">
                            <input type="text" name="e_foda_id" id="e_foda_id" hidden>
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">

                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <LABEL><b>Descripción</b></label>
                                                <textarea class="form-control" name="e_descripcion" id="e_descripcion"
                                                       placeholder="Ingrese la descripción" cols="30" rows="4"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <LABEL><b>Incidencia</b></label>
                                                <textarea class="form-control" name="e_incidencia" id="e_incidencia"
                                                       placeholder="Ingrese la Incidencia" cols="30" rows="4"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <LABEL><b>Unidad Organizacional</b></label>
                                                <select name="e_tipo_foda" id="e_tipo_foda" class="form-control">
                                                    <option value="">Seleccione</option>
                                                    <?php
                                                    foreach ($tipo_foda as $fila2) {
                                                        echo '<option value="'.$fila2['tfoda_id'].'">'.$fila2['tfoda_descripcion'].'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="enviar_anal" id="enviar_anal" class="btn  btn-lg btn-primary"><i class="fa fa-save"></i>
                                ACEPTAR
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
