<?php
$atras = site_url("") . '/prog/pond_p';
?>
<div id="main">
    <div>
        <ol class="breadcrumb">
            <li>Ponderaciones</li>
            <li><a href="<?php echo $atras ?>">Red de Programas</a></li>
            <li>Ponderación de Operaciones</li>
        </ol>
    </div>
    <div id="content">
        <div class="alert alert-block alert-success">
            <a class="close" data-dismiss="alert" href="#"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
            <h4 class="alert-heading"><i class="glyphicon glyphicon-ok-circle"></i>
                LA SUMA DE LAS PONDERACIONES DE LAS OPERACIONES DEBE SER IGUAL AL 100%
            </h4>
        </div>
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <section id="widget-grid" class="well">
                    <div class="" style="font-size: 15px">
                        <b>PONDERACIONES DE OPERACIONES</b> <br>
                        <label>
                            <B>
                                <U STYLE="color: #0E70CA;">CATEGORIA PROGRAMÁTICA:</U>
                                <?php
                                $titulo_aper = $dato_aper['aper_programa'].$dato_aper['aper_proyecto'].$dato_aper['aper_actividad'].' - '.
                                    $dato_aper['aper_descripcion'];
                                echo $titulo_aper;
                                ?>
                            </B>
                        </label>
                    </div>
                </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <section id="widget-grid" class="well">
                    <center>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                                OPCIONES
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="<?php echo $atras ?>">VOLVER ATRAS</a>
                                </li>
                            </ul>
                        </div>
                    </center>
                </section>
            </article>

        </div>


        <section id="widget-grid" class="">
            <div class="row">
                <div class="col-sm-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <span class="fa fa-table"></span></span>

                            <h2>LISTA DE OPERACIONES</h2>
                        </header>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php
                                if ($ponderacion == 100) {
                                    ?>
                                    <div class="alert alert-block alert-success">
                                        <a class="close" data-dismiss="alert" href="#"></a>
                                        <h4 class="alert-heading"><i
                                                class="glyphicon glyphicon-ok-circle"></i>
                                            LA SUMA TOTAL DE LAS PONDERACIONES ES IGUAL AL 100%
                                        </h4>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="alert alert-block alert-danger">
                                        <a class="close" data-dismiss="alert" href="#"></a>
                                        <h4 class="alert-heading"><i
                                                class="glyphicon glyphicon-remove-circle"></i>
                                            LA SUMA TOTAL DE LAS PONDERACIONES ES DIFERENTE DEL 100%
                                        </h4>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-10">
                                <?php
                                if(count($lista_proyecto) != 0){
                                    ?>
                                    <div class="col-sm-4">
                                        <button class="btn btn-labeled btn-primary" type="button" name="asignar_ponderacion" id="asignar_ponderacion"
                                                value="<?php echo $dato_aper['aper_id']?>">
                                            <span class="btn-label"><i class="fa fa-gear fa-2x fa-spin" aria-hidden="true"></i></span>
                                            ASIGNAR PONDERACI&Oacute;N
                                        </button>
                                    </div>
                                    <?php
                                }
                                ?>
                                <br>
                            </div>
                            <div class="col-sm-2 text-align-right">
                                <label for=""> <B><U STYLE="color: #0E70CA;">TOTAL PONDERACIONES:</U> <?php echo $ponderacion ?>%</B></label>
                            </div>
                        </div>
                        <br>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic"
                                           class="table table-striped table-bordered table-hover"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Nro</th>
                                            <th TITLE="ASIGNAR PONDERACIÓN">ASIGNAR</th>
                                            <th>CÓDIGO</th>
                                            <th>APERTURA</th>
                                            <th>NOMBRE DEL PROYECTO</th>
                                            <th>SI SIN</th>
                                            <th>TOTAL ASIGNADO</th>
                                            <th TITLE="PONDERACIÓN">PONDERACIÓN</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        echo $lista_proyecto;
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!------------ MODAL PONDERACION DE PROYECTOS     -->
<div class="modal fade bs-example-modal-lg " id="modal_ponderacion" role="dialog"data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div id="cerrar_modal">
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-times-circle" aria-hidden="true"></i></button>
                </div>
                <h4 class="modal-title text-center text-info">
                    <b><i class="glyphicon glyphicon-pencil"></i> PONDERACI&Oacute;N DE OPERACIONES</b>
                </h4>
            </div>
            <div class="modal-body no-padding">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                    <header>
                        <div class="row text-center">
                            <h2>INGRESE LOS DATOS DE LA PONDERACI&Oacute;N</h2>
                        </div>
                    </header>
                    <div class="widget-body">
                        <form id="form_add_ins" name="form_add_ins" novalidate="novalidate" method="post">
                            <div class="col-sm-12">
                                <div class="tab-content">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <LABEL><b>C&Oacute;DIGO</b></label>
                                                <input class="form-control" type="text" id="codigo" name="codigo" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <LABEL><b>NOMBRE DE PROYECTO</b></label>
                                                <textarea name="objetivo" id="objetivo" rows="3" class="form-control" disabled="disabled"
                                                          style="width:100%;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <LABEL><b>PONDERACI&Oacute;N</b></label>
                                                <input name="pond" id="pond" class="form-control"
                                                       onkeypress="if (this.value.length < 3) { return soloNumeros(event);}else{return false; }"
                                                 title="CAMPO REQUERIDO" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <ul class="pager wizard no-margin">
                                                <li class="previous disabled" style="float: left">
                                                    <input type="button" data-dismiss="modal" class="btn  btn-ms btn-danger" value="CANCELAR">
                                                </li>
                                                <li class="next" style="float: right;">
                                                    <button class="btn  btn-ms btn-primary" id="add_ponderacion" name="add_ponderacion" type="button">
                                                        GUARDAR
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>