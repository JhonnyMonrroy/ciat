<div id="main">
    <div>
        <ol class="breadcrumb">
            <li> Ponderadoraci&oacute;n Categoría Programatica</li>
            <li> Ponderaci&oacute;n de Programas</li>
        </ol>
    </div>
    <div id="content">
        <section id="widget-grid" class="">
            <div class="alert alert-block alert-success">
                <a class="close" data-dismiss="alert" href="#"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                <h4 class="alert-heading"><i class="glyphicon glyphicon-ok-circle"></i>
                    LA SUMA DE LAS PONDERACIONES DE LOS PROGRAMAS DEBE SER IGUAL AL 100%
                </h4>
            </div>
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                            <h2 class="font-md"><strong>PONDERACI&Oacute;N DE PROGRAMAS</strong></h2>
                        </header>
                        <div class="widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php
                                    if ($ponderacion == 100) {
                                        ?>
                                        <div class="alert alert-block alert-success">
                                            <a class="close" data-dismiss="alert" href="#"></a>
                                            <h4 class="alert-heading"><i
                                                    class="glyphicon glyphicon-ok-circle"></i>
                                                LA SUMA TOTAL DE LAS PONDERACIONES ES IGUAL AL 100%
                                            </h4>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="alert alert-block alert-danger">
                                            <a class="close" data-dismiss="alert" href="#"></a>
                                            <h4 class="alert-heading"><i
                                                    class="glyphicon glyphicon-remove-circle"></i>
                                                LA SUMA TOTAL DE LAS PONDERACIONES ES DIFERENTE DEL 100%
                                            </h4>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                                if(count($lista_poa) != 0){
                                   ?>
                                   <!-- <div class="row">
                                        <div class="col-sm-4">
                                            <button  class="btn btn-labeled btn-primary" type="button" name="asignar_ponderacion" id="asignar_ponderacion" >
                                                <span class="btn-label"><i class="fa fa-gear fa-2x fa-spin" aria-hidden="true"></i></span>
                                                ASIGNAR PONDERACI&Oacute;N
                                            </button>
                                        </div>
                                    </div>-->
                            <?php
                                }
                            ?>
                        </div>
                        <div class="well">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0"data-widget-editbutton="false">
                                        <header>
                                            <span class="widget-icon"> <span class="fa fa-table"></span></span>
                                            <h2>LISTA DE PROGRAMAS</h2>
                                        </header>
                                        <div class="row text-align-right">
                                            <div class="col-sm-12">
                                                <label for=""> <B><U STYLE="color: #0E70CA;">TOTAL PONDERACIONES:</U> <?php echo $ponderacion ?>
                                                        %</B></label>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="widget-body no-padding">
                                                <div class="table-responsive">
                                                    <table id="dt_basic"
                                                           class="table table-striped table-bordered table-hover"
                                                           width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Nro</th>
                                                            <th TITLE="ASIGNAR PONDERACIÓN">ASIGNAR</th>
                                                            <th>CÓDIGO</th>
                                                            <th>APERTURA PROGRAM&Aacute;TICA</th>
                                                            <th>UNIDAD ORGANIZACIONAL</th>
                                                            <th>FECHA DE CREACI&Oacute;N</th>
                                                            <th TITLE="PONDERACIÓN">PONDERACIÓN</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody id="bdi">
                                                        <?php
                                                            echo $lista_poa;
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>
<!-------------------------------------        MODAL PONDERACION DE PROGRAMAS     -->
<div class="modal fade bs-example-modal-lg " id="modal_ponderacion" role="dialog" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-times-circle" aria-hidden="true"></i></button>
                </div>
                <h4 class="modal-title text-center text-info">
                    <b><i class="glyphicon glyphicon-pencil"></i> PONDERACI&Oacute;N DE PROGRAMAS</b>
                </h4>
            </div>
            <div class="modal-body no-padding">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                    <header>
                        <div class="row text-center">
                            <h2>INGRESE LOS DATOS DE LA PONDERACI&Oacute;N</h2>
                        </div>
                    </header>
                    <div class="widget-body">
                        <form id="form_pond_prog" name="form_pond_prog" novalidate="novalidate" method="post">
                                <div class="tab-content">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <LABEL><b>C&Oacute;DIGO</b></label>
                                                <input class="form-control" type="text" id="codigo" name="codigo" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <LABEL><b>APERTURA PROGRAM&Aacute;TICA</b></label>
                                                <textarea name="apertura" id="apertura" rows="2" class="form-control" disabled="disabled"
                                                          style="width:100%;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <LABEL><b>PONDERACI&Oacute;N</b></label>
                                                <input name="pond" id="pond" class="form-control"
                                                       onkeypress="if (this.value.length < 3) { return soloNumeros(event);}else{return false; }">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <ul class="pager wizard no-margin">
                                                <li class="previous disabled" style="float: left">
                                                    <input type="button" data-dismiss="modal" class="btn  btn-ms btn-danger" value="CANCELAR">
                                                </li>
                                                <li class="next" style="float: right;">
                                                    <button class="btn  btn-ms btn-primary" id="add_pond_prog" name="add_pond_prog" type="button">
                                                        GUARDAR
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>