<?php
$atras = site_url("") . '/prog/pond_o';
?>
<div id="main" role="main">
    <ol class="breadcrumb">
        <li>Ponderadoración de objetivos</li>
        <li><a href="<?php echo $atras ?>">Red Programas</a></li>
        <li>Ponderación de acciones de corto plazo</li>
    </ol>
    <div id="content">
        <div class="alert alert-block alert-success">
            <a class="close" data-dismiss="alert" href="#"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
            <h4 class="alert-heading"><i class="glyphicon glyphicon-ok-circle"></i>
                LA SUMA DE LAS PONDERACIONES DE LAS ACCIONES DE CORTO PLAZO DEBE SER IGUAL AL 100%
            </h4>
        </div>
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <section id="widget-grid" class="well">
                    <div class="" style="font-size: 15px">
                        <b>PONDERACI&Oacute;N DE ACCIONES DE CORTO PLAZO</b> <br>
                        <b>C&oacute;DIGO POA: </b>
                        <small class="txt-color-blueLight"><?php echo $dato_poa->poa_codigo ?></small>
                        <br>
                        <b>CATEGOR&Iacute;A PROGRAM&Aacute;TICA: </b>
                        <small class="txt-color-blueLight"> <?php echo $dato_poa->aper_programa . $dato_poa->aper_proyecto .
                                $dato_poa->aper_actividad . " - " . $dato_poa->aper_descripcion ?></small>
                        <br>
                        <b>GESTI&Oacute;N: </b>
                        <small class="txt-color-blueLight"><?php echo $dato_poa->poa_gestion ?></small>
                    </div>
                </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <section id="widget-grid" class="well">
                    <center>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                                OPCIONES
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="<?php echo $atras ?>">VOLVER ATRAS</a>
                                </li>
                            </ul>
                        </div>
                    </center>
                </section>
            </article>

        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-21" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <span class="fa fa-table"></span></span>

                        <h2>ACCIONES DE CORTO PLAZO</h2>
                    </header>
                    <div class="row text-align-right">
                        <?php
                        if ($ponderacion == 100) {
                            ?>
                            <div class="alert alert-block alert-success">
                                <a class="close" data-dismiss="alert" href="#"></a>
                                <h4 class="alert-heading"><i
                                        class="glyphicon glyphicon-ok-circle"></i>
                                    LA SUMA TOTAL DE LAS PONDERACIONES ES IGUAL A 100%
                                </h4>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-block alert-danger">
                                <a class="close" data-dismiss="alert" href="#"></a>
                                <h4 class="alert-heading"><i
                                        class="glyphicon glyphicon-remove-circle"></i>
                                    LA SUMA TOTAL DE LAS PONDERACIONES ES DIFERENTE DEL 100%
                                </h4>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="col-sm-12">
                            <label>
                                <B><U STYLE="color: #0E70CA;">SUMA TOTAL PONDERACIÓN:</U> <?php echo $ponderacion ?>%</B>
                            </label>
                        </div>
                    </div>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="table-responsive">
                                <table id="dt_basic" class="table table-bordered "
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>Nro</th>
                                        <th TITLE="ASIGNAR PONDERACIÓN"><font size="1"> ASIGNAR </font></th>
                                        <th><font size="1">CÓDIGO </font></th>
                                        <th><font size="1">OBJETIVO DE GESTIÓN</font></th>
                                        <th><font size="1">RESPONSABLE</font></th>
                                        <th><font size="1">UNIDAD ORGANIZACIONAL </font></th>
                                        <th TITLE="PONDERACIÓN"><font size="1"> PONDERACIÓN </font></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    echo $lista_ogestion;
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-------------------------------------        MODAL PONDERACION DE OBJETIVO DE GESTION     -->
<div class="modal fade bs-example-modal-lg " id="modal_ponderacion" role="dialog" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div id="cerrar_modal">
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-times-circle" aria-hidden="true"></i></button>
                </div>
                <h4 class="modal-title text-center text-info">
                    <b><i class="glyphicon glyphicon-pencil"></i> PONDERACI&Oacute;N DE ACCIONES DE CORTO PLAZO</b>
                </h4>
            </div>
            <div class="modal-body no-padding">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false"
                     data-widget-deletebutton="false">
                    <header>
                        <div class="row text-center">
                            <h2>INGRESE LOS DATOS DE LA PONDERACI&Oacute;N</h2>
                        </div>
                    </header>
                    <div class="widget-body">
                        <form id="form_add_ins" name="form_add_ins" novalidate="novalidate" method="post">
                            <div class="col-sm-12">
                                <div class="tab-content">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <LABEL><b>C&Oacute;DIGO</b></label>
                                                <input class="form-control" type="text" id="codigo" name="codigo" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <LABEL><b>ACCIÓN DE CORTO PLAZO</b></label>
                                                <textarea name="objetivo" id="objetivo" rows="3" class="form-control" disabled="disabled"
                                                          style="width:100%;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <LABEL><b>PONDERACI&Oacute;N</b></label>
                                                <input name="pond" id="pond" class="form-control"
                                                       onkeypress="if (this.value.length < 3) { return soloNumeros(event);}else{return false; }">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <ul class="pager wizard no-margin">
                                                <li class="previous disabled" style="float: left">
                                                    <input type="button" data-dismiss="modal" class="btn  btn-ms btn-danger" value="CANCELAR">
                                                </li>
                                                <li class="next" style="float: right;">
                                                    <button class="btn  btn-ms btn-primary" id="add_ponderacion" name="add_ponderacion" type="button">
                                                        GUARDAR
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
