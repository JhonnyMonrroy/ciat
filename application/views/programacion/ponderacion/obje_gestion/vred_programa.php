<div id="main" role="main">
    <div>
        <ol class="breadcrumb">
            <li>Ponderadoración de objetivos</li>
            <li>Acciones de corto plazo</li>
        </ol>
    </div>
    <div id="content">
       <!-- <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-pencil-square-o fa-fw "></i>
                    RED DE PROGRAMAS - PONDERACION OBJETIVO DE GESTIÓN
                </h1>
            </div>
        </div>-->
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <span class="fa fa-table"></span> </span>
                            <h2> ACCIONES DE CORTO PLAZO - RED DE PROGRAMAS</h2>
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th title="ASIGNAR PPONDERACIÓN"><center>ASIGNAR</center></th>
                                            <th>CÓDIGO</th>
                                            <th>APERTURA PROGRAMÁTICA</th>
                                            <th>UNIDAD ORGANIZACIONAL</th>
                                            <th>FECHA DE CREACIÓN</th>
                                        </tr>
                                        </thead>
                                        <tbody >
                                        <?php
                                        echo $lista_poa;
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>




