<?php $atras = site_url() . '/rep/global' ?>
<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Reporte Gerencial</li>
            <li><a href="<?php echo $atras ?>">Reporte Global</a></li>
            <li>Reporte por Estados de Proyectos</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw ">&nbsp;</i> <b> REPORTE POR ESTADOS DE PROYECTOS </b>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <a href="<?php echo $atras ?>" class="btn btn-labeled btn-success" title="ATRAS">
                    <span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span><font size="1">ATRAS </font>
                </a>
            </div>
        </div>
        <br>
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-10">
                <header>
                    <span class="widget-icon"> <i class="fa fa-pie-chart"></i> </span>

                    <h2>NIVEL DE REPORTE</h2>
                </header>
                <div>
                    <div class="widget-body no-padding">
                        <br>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel-group smart-accordion-default" id="accordion-2">
                                    <div class="panel panel-teal">
                                        <div class="panel-heading">
                                            <h4 class="panel-title" class="collapsed">
                                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-1">
                                                    <i class="fa fa-fw fa-plus-circle  "></i><i class="fa fa-fw fa-minus-circle"></i>
                                                    ESTADO DE PROYECTOS DE INVERSIÓN PÚBLICA
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne-1" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-4" id="col_proy_inv"></div>
                                                    <div class="col-sm-1"></div>
                                                    <div class="col-sm-7 table-responsive">
                                                        <style type="text/css">
                                                            .head_pi {
                                                                background-color: #568A89;
                                                                color: #ffffff;
                                                                align-content: center;
                                                                text-align: center;
                                                                vertical-align: middle%;
                                                            }

                                                            td {
                                                                color: black;
                                                                font-weight: bold;
                                                                text-align: right;
                                                                font-size: 11px;
                                                            }
                                                        </style>
                                                        <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th class="head_pi">ESTADO DEL PROYECTO</th>
                                                                <th class="head_pi">Nro. PROY.</th>
                                                                <th class="head_pi">PPTO. INICIAL</th>
                                                                <th class="head_pi">MODIF. (Bs.)</th>
                                                                <th class="head_pi">PPTO. VIGENTE</th>
                                                                <th class="head_pi">PPTO. EJEC.</th>
                                                                <th class="head_pi">EJEC.[%]</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            echo $tabla_proy_inv;
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="panel panel-greenDark">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-1" class="collapsed">
                                                    <i class="fa fa-fw fa-plus-circle  "></i><i class="fa fa-fw fa-minus-circle  "></i>
                                                    ESTADO DE PROYECTOS DE PROGRAMAS NO RECURRENTES
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo-1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-4" id="col_prog_norec"></div>
                                                    <div class="col-sm-1"></div>
                                                    <div class="col-sm-7 table-responsive">
                                                        <style type="text/css">
                                                            .head_pnr {
                                                                background-color: #496949;
                                                                color: #ffffff;
                                                                align-content: center;
                                                                text-align: center;
                                                                vertical-align: middle%;
                                                            }
                                                        </style>
                                                        <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th class="head_pnr">ESTADO DEL PROYECTO</th>
                                                                <th class="head_pnr">Nro. PROY.</th>
                                                                <th class="head_pnr">PPTO. INICIAL</th>
                                                                <th class="head_pnr">MODIF. (Bs.)</th>
                                                                <th class="head_pnr">PPTO. VIGENTE</th>
                                                                <th class="head_pnr">PPTO. EJEC.</th>
                                                                <th class="head_pnr">EJEC.[%]</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            echo $tabla_prog_norec;
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="panel panel-orange">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-1" class="collapsed">
                                                    <i class="fa fa-fw fa-plus-circle  "></i> <i class="fa fa-fw fa-minus-circle  "></i>
                                                    PROGRAMA DE INVERSIÓN PÚBLICA
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree-1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-4" id="col_pi_pnr"></div>
                                                    <div class="col-sm-1"></div>
                                                    <div class="col-sm-7 table-responsive">
                                                        <style type="text/css">
                                                            .head_pi_pnr {
                                                                background-color: #C79121;
                                                                color: #ffffff;
                                                                align-content: center;
                                                                text-align: center;
                                                                vertical-align: middle%;
                                                            }
                                                        </style>
                                                        <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th class="head_pi_pnr">ESTADO DEL PROYECTO</th>
                                                                <th class="head_pi_pnr">Nro. PROY.</th>
                                                                <th class="head_pi_pnr">PPTO. INICIAL</th>
                                                                <th class="head_pi_pnr">MODIF. (Bs.)</th>
                                                                <th class="head_pi_pnr">PPTO. VIGENTE</th>
                                                                <th class="head_pi_pnr">PPTO. EJEC.</th>
                                                                <th class="head_pi_pnr">EJEC.[%]</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            echo $tabla_pi_pnr;
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </article>

    </div>
</div>
<!-- HIGH CHART  -->
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-more.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-3d.js"></script>
<script type="text/javascript">
    Highcharts.setOptions({
        lang: {
            contextButtonTitle: 'Opciones',
            printChart: 'Imprimir',
            downloadJPEG: 'Descargar en JPEG',
            downloadPDF: 'Descargar en PDF',
            downloadPNG: 'Descargar en PNG',
            downloadSVG: 'Descargar en SVG',
            loading: 'Cargando....'
        },
        credits: {
            enabled: false
        }
    });
    var chart1 = Highcharts.chart('col_proy_inv', {

        title: {
            text: 'EJECUCION DEL PROYECTO DE INVERSIÓN'
        },

        subtitle: {
            text: 'ESTADOS'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'PORCENTAJES (%)'
            },
            stackLabels: {
                enabled: false,

            }
        },
        xAxis: {
            categories: [<?php echo $graf_col_titulo?>]
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php echo $graf_col_pi?>],
            showInLegend: false
        }]

    });
    var chart2 = Highcharts.chart('col_prog_norec', {

        title: {
            text: 'EJECUCION DEL PROGRAMA NO RECURRENTE'
        },

        subtitle: {
            text: 'ESTADOS'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'PORCENTAJES (%)'
            },
            stackLabels: {
                enabled: false,

            }
        },
        xAxis: {
            categories: [<?php echo $graf_col_tit_prog_norec?>]
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php echo $graf_col_prog_norec?>],
            showInLegend: false
        }]

    });
    var chart3 = Highcharts.chart('col_pi_pnr', {

        title: {
            text: 'PROYECTO DE INVERSIÓN + PROGRAMA NO RECURRENTE'
        },

        subtitle: {
            text: 'ESTADOS'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'PORCENTAJES (%)'
            },
            stackLabels: {
                enabled: false,

            }
        },
        xAxis: {
            categories: [<?php echo $graf_col_titulo_pi_pnr?>]
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php echo $graf_col_pi_pnr?>],
            showInLegend: false
        }]

    });
</script>


