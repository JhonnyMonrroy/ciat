<?php
$gestion = $this->session->userData('gestion');
$mes_id = $this->session->userdata('mes');
$atras = site_url() . '/rep/ejec_pres';
?>
<style type="text/css">
    .ruta {
        text-decoration: none;
        color-link: white;
    }

    .icono {
        transition: 0.3s;
        color: white;
    }

    i:hover {

        transform: scale(1.2);
        letter-spacing: 5px;
        color: whitesmoke;
    }

    .head_proy {
        background-color: #568A89;
        color: #ffffff;
        align-content: center;
        text-align: center;
        vertical-align: middle%;
    }

    td {
        color: black;
        font-weight: bold;
        text-align: right;
    }

    .descripcion {
        text-align: left;
    }
</style>
<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Reporte Gerencial</li>
            <li>Reporte Global</li>
        </ol>
    </div>
    <div id="content">
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-lg-offset-1 col-md-offset-1">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <span class="fa fa-table"></span> </span>

                            <h2> REPORTE GLOBAL</h2>
                        </header>

                        <div>
                            <div class="widget-body no-padding">
                                <div class="row text-center">
                                    <h3><b>Seleccione Tipo de Proyecto:</b></h3>
                                </div>

                                <div class="row well">
                                    <form class="smart-form">
                                        <div class="col-sm-3">
                                            <label class="checkbox state-error">
                                                <input type="checkbox" onclick="tipo_proyecto(this)" name="proy_inversion" id="proy_inversion" checked="checked">
                                                <i></i><b>Proyectode Inversión </b></label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="checkbox state-error">
                                                <input type="checkbox" onclick="tipo_proyecto(this)" name="prog_recurrente" id="prog_recurrente" checked="checked">
                                                <i></i><b>Programa Recurrente </b></label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="checkbox state-error">
                                                <input type="checkbox" onclick="tipo_proyecto(this)" name="prog_no_recurrente" id="prog_no_recurrente" checked="checked">
                                                <i></i><b>Programa no Recurrente </b></label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="checkbox state-error">
                                                <input type="checkbox" onclick="tipo_proyecto(this)" name="accion_func" id="accion_func" checked="checked">
                                                <i></i><b>Acción de funcionamiento </b></label>
                                        </div>
                                    </form>
                                </div>
                                <br>
                                <style type="text/css">
                                    .head_proy {
                                        background-color: #3A3633;
                                        color: #ffffff;
                                        align-content: center;
                                        text-align: center;
                                        vertical-align: middle%;
                                    }

                                    td {
                                        color: black;
                                        font-weight: bold;
                                        text-align: right;
                                    }

                                    .descripcion {
                                        text-align: left;
                                    }
                                </style>
                                <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="head_proy" style="vertical-align: middle">TIPO DE PROYECTO</th>
                                        <th colspan="4" class="head_proy">CANTIDAD</th>
                                        <th colspan="4" class="head_proy">PRESUPUESTO</th>
                                        <th colspan="3" class="head_proy">EJECUCIÓN FINANCIERA</th>
                                    </tr>
                                    <tr>
                                        <th class="head_proy">INV</th>
                                        <th class="head_proy">PINV</th>
                                        <th class="head_proy">EJEC</th>
                                        <th class="head_proy">TOTAL</th>
                                        <th class="head_proy">PPTO. INICIAL</th>
                                        <th class="head_proy">MODIF. (Bs.)</th>
                                        <th class="head_proy">PPTO. VIGENTE</th>
                                        <th class="head_proy">[%]</th>
                                        <th class="head_proy">PPTO. EJEC.</th>
                                        <th class="head_proy">SALDO</th>
                                        <th class="head_proy">EJEC[%]</th>
                                    </tr>

                                    </thead>
                                    <tbody id="tabla">
                                    <?php
                                    echo $tabla;
                                    ?>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="well well-sm col-sm-6 animated rotateInDownLeft">
                                        <div class="well well-sm bg-color-pinkDark txt-color-white text-center">
                                            <a href="<?php echo site_url() . '/rep/global/unidad' ?>" class="ruta">
                                                <h5 style="font-weight: bold;font-style: italic;color: white">POR UNIDAD EJECUTORA</h5>
                                                <i class="fa fa-pie-chart fa-5x icono" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="well well-sm col-sm-6 animated rotateInDownRight">
                                        <div class="well well-sm bg-color-teal txt-color-white text-center">
                                            <a href="<?php echo site_url() . '/rep/global/estado' ?>" class="ruta">
                                                <h5 style="font-weight: bold;font-style: italic;color: white">POR ESTADO DE PROYECTOS</h5>
                                                <i class="fa fa-line-chart fa-5x icono" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="well well-sm col-sm-6 animated bounceInLeft">
                                        <div class="well well-sm bg-color-blue txt-color-white text-center">
                                            <a href="<?php echo site_url() . '/rep/global/eje_prog' ?>" class="ruta">
                                                <h5 style="font-weight: bold;font-style: italic;color: white">POR EJE PROGRAMÁTICO</h5>
                                                <i class="fa fa-area-chart fa-5x icono" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="well well-sm col-sm-6 animated bounceInRight">
                                        <div class="well well-sm bg-color-redLight txt-color-white text-center">
                                            <a href="<?php echo site_url() . '/rep/global/region' ?>" class="ruta">
                                                <h5 style="font-weight: bold;font-style: italic;color: white">POR REGIÓN</h5>
                                                <i class="fa fa-bar-chart fa-5x icono" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="well well-sm col-sm-6 animated rotateInUpLeft">
                                        <div class="well well-sm bg-color-yellow txt-color-white text-center">
                                            <a href="<?php echo site_url() . '/rep/global/provincia' ?>" class="ruta">
                                                <h5 style="font-weight: bold;font-style: italic;color: white">POR PROVINCIA</h5>
                                                <i class="fa fa-signal fa-5x icono" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="well well-sm col-sm-6 animated rotateInUpRight">
                                        <div class="well well-sm bg-color-orangeDark txt-color-white text-center">
                                            <a href="<?php echo site_url() . '/rep/global/municipio' ?>" class="ruta">
                                                <h5 style="font-weight: bold;font-style: italic;color: white">POR MUNICIPIO</h5>
                                                <i class="fa fa-pie-chart fa-5x icono" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </article>
            </div>
    </div>

</div><!-- main -->
<!--<script src="<?php /*echo base_url(); */ ?>assets/highcharts/js/highcharts.js"></script>
<script src="<?php /*echo base_url(); */ ?>assets/highcharts/js/highcharts-more.js"></script>
<script src="<?php /*echo base_url(); */ ?>assets/highcharts/js/modules/exporting.js"></script>
<script src="<?php /*echo base_url(); */ ?>assets/highcharts/js/highcharts-3d.js"></script>-->
<script type="text/javascript">
    function tipo_proyecto(a) {
        //a.checked='checked';
        var proy_inv = document.getElementById("proy_inversion").checked;
        var prog_rec = document.getElementById("prog_recurrente").checked;
        var prog_no_rec = document.getElementById("prog_no_recurrente").checked;
        var accion_fun = document.getElementById("accion_func").checked;
        var url = site_url + "/reportes/creporte_global/get_financiamiento";
        var request;
        if (request) {
            request.abort();
        }
        request = $.ajax({
            url: url,
            type: "POST",
            dataType: 'json',
            data: {"proy_inv": proy_inv, "prog_rec": prog_rec, "prog_no_rec": prog_no_rec, "accion_fun": accion_fun}
        });
        request.done(function (response, textStatus, jqXHR) {
            $("#tabla").html(response.tabla);

        });
        request.fail(function (jqXHR, textStatus, thrown) {
            console.log("ERROR: " + textStatus);
        });
        request.always(function () {
            //console.log("termino la ejecuicion de ajax");
        });
        e.preventDefault();


    }

</script>
