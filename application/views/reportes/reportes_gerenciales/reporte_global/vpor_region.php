<?php $atras = site_url() . '/rep/global' ?>
<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Reporte Gerencial</li>
            <li><a href="<?php echo $atras ?>">Reporte Global</a></li>
            <li>Reporte por Región</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw ">&nbsp;</i><b>REPORTE POR REGIÓN</b>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <a href="<?php echo $atras ?>" class="btn btn-labeled btn-success" title="ATRAS">
                    <span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span><font size="1">ATRAS </font>
                </a>
            </div>
        </div>
        <br>
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-10">
                <header>
                    <span class="widget-icon"> <i class="fa fa-pie-chart"></i> </span>

                    <h2>NIVEL DE REPORTE</h2>
                </header>
                <div>
                    <div class="widget-body no-padding">
                        <br>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel-group smart-accordion-default" id="accordion-2">
                                    <div class="panel panel-teal">
                                        <div class="panel-heading">
                                            <h4 class="panel-title" class="collapsed">
                                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-1">
                                                    <i class="fa fa-fw fa-plus-circle  "></i><i class="fa fa-fw fa-minus-circle"></i>
                                                    PROYECTO DE INVERSIÓN PÚBLICA
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne-1" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div id="pie_proy_inv"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="well well-sm col-sm-12 table-responsive">
                                                        <style type="text/css">
                                                            .head_pi {
                                                                background-color: #568A89;
                                                                color: #ffffff;
                                                                align-content: center;
                                                                text-align: center;
                                                                vertical-align: middle%;
                                                            }

                                                            td {
                                                                color: black;
                                                                font-weight: bold;
                                                                text-align: right;
                                                            }
                                                        </style>
                                                        <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th class="head_pi">REGIÓN</th>
                                                                <th class="head_pi">INV.</th>
                                                                <th class="head_pi">P.INV.</th>
                                                                <th class="head_pi">TOTAL</th>
                                                                <th class="head_pi">PPTO. VIGENTE</th>
                                                                <th class="head_pi">[%]</th>
                                                                <th class="head_pi">PPTO. EJEC.</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                                echo $tabla_proy_inv;
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="panel panel-greenDark">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-1" class="collapsed">
                                                    <i class="fa fa-fw fa-plus-circle  "></i><i class="fa fa-fw fa-minus-circle  "></i>
                                                    PROGRAMAS NO RECURRENTES
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo-1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div id="pie_prog_norec"></div>
                                                    </div>
                                                    <div class="well well-sm  col-sm-6 table-responsive">
                                                        <style type="text/css">
                                                            .head_pnr {
                                                                background-color: #496949;
                                                                color: #ffffff;
                                                                align-content: center;
                                                                text-align: center;
                                                                vertical-align: middle%;
                                                            }
                                                        </style>
                                                        <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th class="head_pnr">REGIÓN</th>
                                                                <th class="head_pnr">P-NR</th>
                                                                <th class="head_pnr">P-R</th>
                                                                <th class="head_pnr">TOTAL</th>
                                                                <th class="head_pnr">PPTO. VIGENTE</th>
                                                                <th class="head_pnr">[%]</th>
                                                                <th class="head_pnr">PPTO. EJEC.</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                                echo $tabla_prog_norec;
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="panel panel-pinkDark">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-1" class="collapsed">
                                                    <i class="fa fa-fw fa-plus-circle  "></i> <i class="fa fa-fw fa-minus-circle  "></i>
                                                    PROGRAMA DE INVERSIÓN PÚBLICA (Proyectos de Inversión + Programas no Recurrentes)
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree-1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div id="pie_pi_pnr"></div>
                                                    </div>
                                                    <div class="well well-sm col-sm-6 table-responsive">
                                                        <style type="text/css">
                                                            .head_pi_pnr {
                                                                background-color: #A8829F;
                                                                color: #ffffff;
                                                                align-content: center;
                                                                text-align: center;
                                                                vertical-align: middle%;
                                                            }
                                                        </style>
                                                        <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th class="head_pi_pnr">REGIÓN</th>
                                                                <th class="head_pi_pnr">INV.</th>
                                                                <th class="head_pi_pnr">P-NR</th>
                                                                <th class="head_pi_pnr">TOTAL</th>
                                                                <th class="head_pi_pnr">PPTO. VIGENTE</th>
                                                                <th class="head_pi_pnr">[%]</th>
                                                                <th class="head_pi_pnr">PPTO. EJEC.</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                                echo $tabla_pi_pnr;
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </article>


    </div>
</div>
<!-- HIGH CHART  -->
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-more.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-3d.js"></script>
<script type="text/javascript">
    Highcharts.setOptions({
        lang: {
            contextButtonTitle: 'Opciones',
            printChart: 'Imprimir',
            downloadJPEG: 'Descargar en JPEG',
            downloadPDF: 'Descargar en PDF',
            downloadPNG: 'Descargar en PNG',
            downloadSVG: 'Descargar en SVG',
            loading: 'Cargando....'
        },
        credits: {
            enabled: false
        }
    });
    //inversion  publica
    Highcharts.chart('pie_proy_inv', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'PROYECTO DE INVERSIÓN PUBLICA'
        },
        subtitle: {
            text: 'REGIÓN'
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Total[%] :',
            data: [
                <?php echo $pie_proy_inv?>
            ]
        }]
    });
    Highcharts.chart('pie_prog_norec', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'PROGRAMA NO RECURRENTE'
        },
        subtitle: {
            text: 'REGIÓN'
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Total[%] :',
            data: [
                <?php echo $pie_prog_norec?>
            ]
        }]
    });
    Highcharts.chart('pie_pi_pnr', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'PROYECTO DE INVERSIÓN PÚBLICA'
        },
        subtitle: {
            text: 'MUNICIPIO'
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Total[%] :',
            data: [
                <?php echo $pie_prog_norec?>
            ]
        }]
    });
</script>


