<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Presupuesto</li>
            <li><a href="<?php echo $atras?>">Lista de Programas</a></li>
            <li>Lista de Proyectos</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><span class="widget-icon"><i class="fa fa-bar-chart" aria-hidden="true">&nbsp;</i></span>
                    <b>LISTA DE ACCIONES</b>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <a href="<?php echo $atras ?>" class="btn btn-labeled btn-success" title="ATRAS">
                    <span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span><font size="1">ATRAS </font>
                </a>
            </div>
        </div>
        <br>
        <div class="row">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-10">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-list-ol" aria-hidden="true"></i></span>

                        <h2>PROYECTOS</h2>
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="table-responsive">
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                    <tr>
                                        <th>NRO.</th>
                                        <th title="PRESUPUESTO">PRESUPUESTO<br>PROGRAMADO</th>
                                        <th>CATEGORIA PROGRAMATICA</th>
                                        <th>NOMBRE DE LA ACCI&Oacute;N</th>
                                        <th>UNIDAD RESPONSABLE</th>
                                        <th>TIPO</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $cont = 1;
                                    $ruta_img = base_url() . 'assets/ifinal/grafico5.png';
                                    foreach ($lista_proy as $item) {
                                        echo '<tr>';
                                        echo '<td>' . $cont . '</td>';
                                        echo '<td>
                                                  <a href="' . $ruta . $item['proy_id'] . '/' . $item['tipo_ejec'] . '" title="PRSUPUESTO PROGRAMADO">
                                                        <img src="' . $ruta_img . '"  class="img-responsive" title="PRSUPUESTO PROGRAMADO">
							                      </a>
			                            	  </td>';
                                        echo '<td>' . $item['programatica'] . '</td>';
                                        echo '<td>' . $item['proy_nombre'] . '</td>';
                                        echo '<td>' . $item['uni_unidad'] . '</td>';
                                        echo '<td>' . $item['tp_tipo'] . '</td>';
                                        echo '</tr>';
                                        $cont++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
</div>


