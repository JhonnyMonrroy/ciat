<script xmlns="http://www.w3.org/1999/html">
    function abreVentana(PDF) {
        var direccion;
        direccion = '' + PDF;
        window.open(direccion, "Reporte de Proyectos", "width=800,height=650,scrollbars=SI");
    }
</script>
<div id="main" role="main">
    <div>
        <ol class="breadcrumb">
            <li>Frm. POA Programación</li><li> Operaciones</li>
        </ol>
    </div>
    <div id="content">
        <section id="widget-grid" class="">
			<div class="row">
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="jarviswidget jarviswidget-color-teal" id="wid-id-0" data-widget-editbutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-table"></i></span>
						</header>
						<div>
							<div class="widget-body no-padding">
								<div class="table-responsive">
									<table id="dt_basic"  class="table table-striped table-bordered table-hover">
                                        <thead style="width:100%;">
                                            <th style="width:5%;" > AO </th>
                                                <th style="width:15%;">CÓDIGO</th>
                                                <th style="width:70%;">APERTURA PROGRAMÁTICA</th>
                                            <th style="width:10%;">FECHA DE CREACIÓN</th>
                                        </thead>
										<tbody>
                                            <?php
                                                foreach ($operaciones as $fila) {
                                                    echo '<tr style="width:100%;">';
                                                    echo'<td>';
                                                        ?>
                                                            <a href="javascript:abreVentana('<?php echo site_url("admin").'/reporte_operaciones/'.$fila['aper_id']; ?>');" title="REPORTE">
                                                                <img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/>
                                                            </a>
                                                        <?php
                                                        '</td>';
                                                    echo '
                                                            <td>'.$fila['poa_codigo'].'</td>
                                                            <td>'.$fila['aper_programa'].$fila['aper_proyecto'].$fila['aper_actividad']." - ".$fila['aper_descripcion'].'</td>
                                                            <td>'.$fila['poa_fecha_creacion'].'</td>
                                                          </tr>';
                                                }
                                            ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

				</article>
			</div>
		</section>
    </div>
</div>